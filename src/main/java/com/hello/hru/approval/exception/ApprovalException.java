package com.hello.hru.approval.exception;

public class ApprovalException extends Exception{
	
	public ApprovalException(String msg) {
		super(msg);
	}

}
