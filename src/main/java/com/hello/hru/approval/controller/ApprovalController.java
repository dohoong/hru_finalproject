package com.hello.hru.approval.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.approval.exception.ApprovalException;
import com.hello.hru.approval.model.service.ApprovalService;
import com.hello.hru.approval.model.vo.Alarm;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.approval.model.vo.Schedule;
import com.hello.hru.attendance.model.service.AttendanceService;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.common.ApprovalLine;
import com.hello.hru.common.PageInfo;
import com.hello.hru.common.Pagination;
import com.hello.hru.humanResource.model.service.EvaluationService;
import com.hello.hru.humanResource.model.service.HumanResourceService;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.Movement;
import com.hello.hru.humanResource.model.vo.TeamPlanDetail;
import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.service.MemberService;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.TemporaryMember;
import com.hello.hru.webSocket.websocketHandler.EchoHandler;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
public class ApprovalController {
	@Autowired
	private ApprovalService as;
	@Autowired
	private AuthorityService aus;
	@Autowired
	private MemberService ms;
	@Autowired
	private HumanResourceService hrs;
	
	@PostMapping("selectOneBasic.app")
	public ModelAndView selectOneBasic(HttpServletRequest request, ModelAndView mv) {
		ApprovalBasic appBasic = null;
		
		int approvalCode = Integer.parseInt(request.getParameter("approvalCode"));
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		System.out.println(mid);
		try {
			appBasic = as.selectOneBasic(approvalCode, mid);
			
			
			
			mv.addObject("appBasic", appBasic);
			
		} catch (ApprovalException e) {
			mv.addObject("msg", "실패!");
		}
		mv.setViewName("jsonView");

		return mv;
	}
	
	@RequestMapping("showApprovalLine.app")
	public String selectListApproval(Model model) {
		
		ArrayList<ApprovalBasic> ablist = as.selectListApprovalLine();
		
		model.addAttribute("ablist", ablist);
		
		return "system/approvalLine";
	}
	
	@RequestMapping("updateApprovalLine.app")
	public String updateApprovalLine(ApprovalBasic ab, Model model) {
		
		//System.out.println("ablist : " + ab.getAblist());
		ArrayList<ApprovalBasic> ablist2 = ab.getAblist();
		
		for(int i=0; i<ablist2.size(); i++) {
			if(ablist2.get(i).getStatus() != null) {
				ablist2.get(i).setStatus("Y");
			} else { //사용하지 않을 경우!
				ablist2.get(i).setStatus("N");
				ablist2.get(i).setFirstApproval(0);
				ablist2.get(i).setSecondApproval(0);
				ablist2.get(i).setThirdApproval(0);
			}
			
						
		}
		
		
		int result = as.updateApprovalLine(ablist2);
		
		//System.out.println("Result : " +result);
		
		return "redirect:/showApprovalLine.app";
	}
	
	@RequestMapping("showApprovalWaiting.app")
	public String showAppWaiting(Model model, HttpServletRequest request) {
		int  mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		

		String authority = aus.checkAuthority(mid, 4);
		
		if(authority.equals("Y")) {
			ArrayList<HashMap<String, String>> tlist = aus.checkAuthorityTeam(mid, 4);
			//System.out.println(tlist);
			

			ArrayList<HashMap<String, Object>> alist = as.selectListApproval(mid);
			
			model.addAttribute("tlist", tlist);
			model.addAttribute("alist", alist);
			
			return "leader/authorizationWaiting";

		} else {
			return "common/noAuthority";
		}
		
	}
	
	//결재내역 자세히보는 모달창(ajax)
	@RequestMapping("showApprovalDetail.app")
	public ModelAndView showApprovalDetail(HttpServletRequest request, ModelAndView mv) {
		
		int approvalId = Integer.parseInt(request.getParameter("approvalId"));
		
		//System.out.println("approvalId : " + approvalId);
		
		Approval app = as.selectApproval(approvalId);
		
		if(app.getApprovalCode()==1) { //신입사원 입사건일 경우!
			
			HashMap<String, Object> tempMember = ms.selectTempMember(approvalId);
			mv.addObject("tempMember", tempMember);
		} else if(app.getApprovalCode()==8){ //휴가신청 건일 경우!
			HashMap<String, Object> vhmap = as.selectVacation(approvalId);
			mv.addObject("vhmap", vhmap);
		} else if(app.getApprovalCode()==7) { //야특근 신청
			HashMap<String, Object> spemap = as.selectSpecialAttendance(approvalId);
			mv.addObject("smap", spemap);
			
		} else if(app.getApprovalCode()==4) { //인사평가 계획
			ArrayList<HashMap<String, Object>> evalist = as.selectEvaluation(approvalId);
			mv.addObject("evalist", evalist);
			
		} else if(app.getApprovalCode()==3) { //인사발령 건
			HashMap<String, Object> movhmap = as.selectMovement(approvalId);
			//System.out.println(movhmap);
			Movement m = new Movement();
			m.setApprovalId((Integer) movhmap.get("approvalId"));
			m.setMid((Integer) movhmap.get("mid"));
			HashMap<String, Object> movhmap0 = as.selectMovementBefore(m);
			mv.addObject("movMap", movhmap);
			mv.addObject("movMap0", movhmap0);
		} else if(app.getApprovalCode()==2) { //인사계획
			HashMap<String, Object> hrplan = hrs.selectHRPlanHis(app.getApprovalId());
			if(hrplan!=null) {
				ArrayList<TeamPlanDetail> tplist = hrs.selectNextTeamPlanHis((Integer)hrplan.get("hrplanId"));
				hrplan.put("tplist", tplist);
				mv.addObject("hrplan", hrplan);
			}
			
		} else if(app.getApprovalCode()==5) { //월근태 마감
			ArrayList<HashMap<String, Object>> monlist = as.selectMonthAttList(approvalId);
			mv.addObject("monlist", monlist);
		} else if(app.getApprovalCode()==6) { //월급여 마감
			
		} else if(app.getApprovalCode()==9) { //퇴직 신청
			
		} else if(app.getApprovalCode()==10) { //인사평가 마감
			Evaluation evaluation = as.selectEvaluationByApp(app.getApprovalId());
			mv.addObject("evaluation", evaluation);
			
		} else if(app.getApprovalCode()==11) { //휴가 취소
			
		} else if(app.getApprovalCode()==12) { //제증명 신청
			
		}
		
		mv.addObject("app", app);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("updateSuccess.app")
	public String updateSuccessApproval(HttpServletRequest request) {
		int  mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		ArrayList<ApprovalHistory> ahlist = new ArrayList<>();
		
		String[] apphisId = request.getParameterValues("apphisId");
		String[] finalYn = request.getParameterValues("finalYn");
		String[] midArr = request.getParameterValues("mid");
		String[] result = request.getParameterValues("result");
		int approvalId = Integer.parseInt(request.getParameter("approvalId"));
		
		//System.out.println("approvalId : " +approvalId);
		
		for(int i=0; i<apphisId.length; i++) {
			ApprovalHistory apphis = new ApprovalHistory();
			
			apphis.setApphisId(Integer.parseInt(apphisId[i]));
			apphis.setFinalYn(finalYn[i]);
			apphis.setMid(Integer.parseInt(midArr[i]));
			apphis.setResult(Integer.parseInt(result[i]));
			apphis.setApprovalId(approvalId);
			if(!result[i].equals("3") && !result[i].equals("4")) {
				ahlist.add(apphis);				
			}
		}
		
		int result1 = as.updateSuccessApp(ahlist);
		
		return "redirect:/showApprovalWaiting.app";
	}
	
	@RequestMapping("updateRefuse.app")
	public String updateRefuseApproval(HttpServletRequest request) {
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		int approvalId = Integer.parseInt(request.getParameter("num"));
		String reason = request.getParameter("reason");
		
		//System.out.println("approvalId : " +approvalId + ", reason " + reason);
		
		ApprovalHistory ah = new ApprovalHistory();
		ah.setApprovalId(approvalId);
		ah.setMid(mid);
		ah.setResult(4);
		ah.setRefuseReason(reason);
		
		int result = as.updateRefuseApp(ah);
		
		//System.out.println("result : " + result);
		return "redirect:/showApprovalWaiting.app";
	}

	@RequestMapping("showApprovalList.app")
	public String showApprovalList(HttpServletRequest request, Model model) {
		int  mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		String authority = aus.checkAuthority(mid, 4);
		if(authority.equals("Y")) {
			ArrayList<HashMap<String, String>> tlist = aus.checkAuthorityTeam(mid, 4);
			
			
			//currentPage에 대한 처리
			int currentPage = 1;
			
			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			int listCount = as.getListCount(mid);
			
			//System.out.println("listCount : " + listCount);
			
			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
			
			ArrayList<HashMap<String, Object>> allist = as.selectListAllApproval(mid, pi);
			
			//System.out.println("allist : " +allist);
			//System.out.println("tlist : " + tlist);
			//System.out.println("pi : " + pi);
			
			
			model.addAttribute("tlist", tlist);
			model.addAttribute("allist", allist);
			model.addAttribute("pi", pi);
			
			return "leader/authorizationList";

		} else {
			return "common/noAuthority";
		}
		
	}
	
	@RequestMapping("search.app")
	public ModelAndView searchApplication(HttpServletRequest request, ModelAndView mv) {
		int  mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		String name = request.getParameter("name");
		int team = Integer.parseInt(request.getParameter("team"));
		java.sql.Date date = null;
		if(request.getParameter("date")!=null && !request.getParameter("date").equals("")) {
			date = java.sql.Date.valueOf(request.getParameter("date"));
		}
		String title = request.getParameter("title");

		HashMap<String, Object> hmap = new HashMap<>();
		hmap.put("name", name);
		hmap.put("team", team);
		hmap.put("date", date);
		hmap.put("title", title);
		hmap.put("mid", mid);
		
		//System.out.println(hmap);

		//currentPage에 대한 처리
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int listCount = as.getListCountSearch(hmap);
		//System.out.println("카운트 : " + listCount);

		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		ArrayList<HashMap<String, Object>> slist = as.searchApprovalList(hmap, pi);
		
		
		//System.out.println(slist);
		
		
		mv.addObject("slist", slist);
		mv.addObject("pi", pi);
		
		mv.setViewName("jsonView");
		return mv;
	}
	

	@RequestMapping("showApprovalRequestList.app")
	public String showApprovalRequestList(HttpServletRequest request, Model model) {
		int  mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		//currentPage에 대한 처리
		int currentPage = 1;

		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int listCount = as.getRequestListCount(mid);

		//System.out.println("listCount : " + listCount);

		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

		ArrayList<HashMap<String, Object>> allist = as.selectListAllApprovalRequest(mid, pi);

		model.addAttribute("allist", allist);
		model.addAttribute("pi", pi);

		return "member/approvalList";
	}
	
	@RequestMapping("searchRequest.app")
	public ModelAndView searchRequestApproval(HttpServletRequest request, ModelAndView mv) {
		int  mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		java.sql.Date date = null;
		if(request.getParameter("date")!=null && !request.getParameter("date").equals("")) {
			date = java.sql.Date.valueOf(request.getParameter("date"));
		}
		String title = request.getParameter("title");

		HashMap<String, Object> hmap = new HashMap<>();
		hmap.put("date", date);
		hmap.put("title", title);
		hmap.put("mid", mid);
		
		//System.out.println(hmap);

		//currentPage에 대한 처리
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int listCount = as.getListRequestCountSearch(hmap);
		//System.out.println("카운트 : " + listCount);

		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		ArrayList<HashMap<String, Object>> slist = as.searchRequestApprovalList(hmap, pi);
		
		
		//System.out.println(slist);
		
		
		mv.addObject("slist", slist);
		mv.addObject("pi", pi);
		
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	
	@RequestMapping("test.app")
	public String alarmTestFunction() {
		Alarm al = new ApprovalLine().makingAlarm("전자결재 : " + "test title", 6, 2);
		//ad.insertAlarm(sqlSession, al); //알람내역을 인서트
		//System.out.println("alarm: " + al);
		
		return "redirect:/showExample.temp";
	}
	
	@RequestMapping("selectListAlarm.app")
	public ModelAndView selectListAlarm(ModelAndView mv, HttpServletRequest request) {
		int  mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		ArrayList<Alarm> alist = as.selectAlarm(mid);
		
		
		mv.addObject("alist", alist);
		mv.setViewName("jsonView");
		return mv;
	}
	@RequestMapping("updateAlarm.app")
	public ModelAndView updateAlarm(ModelAndView mv, HttpServletRequest request) {
		int alarmId = Integer.parseInt(request.getParameter("alarmId"));
		int alarmCnt = Integer.parseInt(request.getParameter("cnt"));
		System.out.println("alarmCnt : " +alarmCnt);
		int result = as.updateAlarm(alarmId);
		
		request.getSession().setAttribute("alarmCnt", --alarmCnt);
		
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("alarmCountUp.app")
	public ModelAndView alarmCountUp(HttpServletRequest request, ModelAndView mv) {
		int alarmCnt = Integer.parseInt(request.getParameter("cnt"));
		request.getSession().setAttribute("alarmCnt", ++alarmCnt);
		int alarmId = Integer.parseInt(request.getParameter("alarmId"));
		
		Alarm alarm = as.selectAlarmOne(alarmId);
		
		mv.addObject("alarm", alarm);
		mv.setViewName("jsonView");
		return mv;
	}

	//	@RequestMapping("sendMessage.app")
//	public ModelAndView insertMessage(ModelAndView mv, HttpServletRequest request) {
//		int  smid = 0;
//		if(request.getSession().getAttribute("loginUser") != null) {
//			smid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
//		}
//		String content = request.getParameter("content");
//		int tmid = Integer.parseInt(request.getParameter("tmid"));
//		
//		Alarm alarm = new Alarm();
//		Alarm al = new ApprovalLine().makingAlarm("쪽지 : " + content, smid, tmid);
//		
//		Alarm resultAlarm = as.insertAlarm(al);
//		
//		
//		
//		return mv;
//	}
	
	@PostMapping("selectCalendarAll.app")
	public ModelAndView selectCalendarAll(ModelAndView mv, HttpServletRequest request) {
		//System.out.println(request.getParameter("start"));
		//System.out.println(request.getParameter("end"));
		HashMap<String, Object> rangeMap = new HashMap<>();
		rangeMap.put("start", request.getParameter("start"));
		rangeMap.put("end", request.getParameter("end"));
		
		ArrayList<Schedule> schlist = as.selectScheduleAll(rangeMap);
		//System.out.println(schlist);
		JSONArray jarr = new JSONArray();
		for(int i=0; i<schlist.size(); i++) {
			JSONObject jobj = new JSONObject();
			jobj.put("code", schlist.get(i).getScheduleCode());
			jobj.put("start", schlist.get(i).getStartDate());
			jobj.put("end", schlist.get(i).getEndDate());
			jobj.put("title", schlist.get(i).getScheduleTitle());
			jobj.put("category", schlist.get(i).getCategory());
			if(schlist.get(i).getCategory()==1) {
				jobj.put("color", "#3f64a8");
			} else if(schlist.get(i).getCategory()==2) {
				jobj.put("color", "#4ba83f");
			} else {
				jobj.put("color", "#a83f49");
			}
			jobj.put("content", schlist.get(i).getScheduleContent());
			jobj.put("allDay",true);
			jarr.add(jobj);
		}
		//System.out.println(jarr);
		
		mv.addObject("jarr", jarr);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("updateSchedule.app")
	public ModelAndView updateScheduleByMove(HttpServletRequest request, ModelAndView mv) {
		Schedule sch = new Schedule();
		sch.setScheduleCode(Integer.parseInt(request.getParameter("scheduleCode")));
		sch.setStartDate(request.getParameter("startDate"));
		
		if(request.getParameter("endDate").equals("Invalid date")||request.getParameter("endDate").equals(request.getParameter("startDate"))) {
			sch.setEndDate(null);			
		} else {
			sch.setEndDate(request.getParameter("endDate"));			
		}
		
		//System.out.println(sch);
		int result = as.updateScheduleByMove(sch);
		
		
		mv.addObject("sch", sch);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("deleteSchedule.app")
	public String deleteSchedule(HttpServletRequest request) {
		int scheduleCode = Integer.parseInt(request.getParameter("scheduleCode"));
		System.out.println("code :" + scheduleCode);
		int result = as.deleteSchedule(scheduleCode);
		
		return "redirect:/showCalendarAdministration.temp";
	}
	
	@RequestMapping("addAndChangeSch.app")
	public String addAndChangeSch(Schedule sch) {
		
		System.out.println("*****일정 변경/추가 : " + sch);
		
		if(sch.getEndDate().equals(sch.getStartDate())) {
			sch.setEndDate(null);			
		}
		int result = 0;
		if(sch.getScheduleCode() == 0) { //추가
			result = as.insertSchedule(sch);
		} else { //변경
			result = as.updateSchedule(sch);
		}
		return "redirect:/showCalendarAdministration.temp";
	}
	
	@RequestMapping("showOrganizationChart.app")
	public String showOrganization(Model model) {
		//사업장을 가져온다.
		ArrayList<HashMap<String, Object>> glist = as.selectGroupListforOrg();
		//System.out.println("glist : " +glist);
		//팀을 가져온다.
		ArrayList<HashMap<String, Object>> tlist = as.selectTeamListforOrg();
		//System.out.println("tlist : " +tlist);
		//사람을 가져온다.
		HashMap<Integer, ArrayList<String>> peopleMap = as.selectPeopleListforOrg(tlist);
		//System.out.println("peopleMap : " + peopleMap);
		
		JSONObject job = new JSONObject();
		job.put("id", "1");
		job.put("name", "CEO");
		job.put("title", "김진호");
		
		//second Children : 부서
		JSONArray jarr2 = new JSONArray();
		for(int i=0; i<tlist.size(); i++) {
			JSONObject tempObj = new JSONObject();
			tempObj.put("id", tlist.get(i).get("teamCode"));
			tempObj.put("name", tlist.get(i).get("teamName"));
			tempObj.put("groupCode", tlist.get(i).get("groupCode"));
			tempObj.put("className", "third-level");
			
			String peopleStr = "<i class='chess queen yellow icon'></i>" + tlist.get(i).get("mName") + "<br>";
			ArrayList<String> peoples = peopleMap.get(tlist.get(i).get("teamCode"));
			//System.out.println(peoples);
			for(int j=0; j<peoples.size(); j++) {
				//System.out.println(peoples.get(j));
				peopleStr += peoples.get(j) + "<br>";
			}
			tempObj.put("title", peopleStr);
			jarr2.add(tempObj);
		}
		
		
		//first Children : 사업장
		JSONArray jarr1 = new JSONArray();
		for(int i=0; i<glist.size(); i++) {
			JSONObject tempObj = new JSONObject();
			tempObj.put("id", glist.get(i).get("groupCode"));
			tempObj.put("name", glist.get(i).get("groupName"));
			tempObj.put("title", glist.get(i).get("ceo"));
			tempObj.put("className", "middle-level");

			JSONArray jarr11 = new JSONArray();
			for(int j=0; j<jarr2.size(); j++) {
				JSONObject temp2 = (JSONObject) jarr2.get(j);
				if(temp2.get("groupCode") == glist.get(i).get("groupCode")) {
					temp2.remove("groupCode");
					jarr11.add(temp2);
					
				}
			}
			tempObj.put("children", jarr11);
			jarr1.add(tempObj);
		}
		
		job.put("children", jarr1);
		
		//System.out.println("JSON :::::: " + job);
		model.addAttribute("job", job);
		return "humanResource/organizationChart";
	}
}













