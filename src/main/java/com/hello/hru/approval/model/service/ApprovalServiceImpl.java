package com.hello.hru.approval.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.approval.exception.ApprovalException;
import com.hello.hru.approval.model.dao.ApprovalDao;
import com.hello.hru.approval.model.vo.Alarm;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.approval.model.vo.Schedule;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.PageInfo;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.Movement;
import com.hello.hru.webSocket.websocketHandler.EchoHandler;

@Service
public class ApprovalServiceImpl implements ApprovalService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private ApprovalDao ad;

	@Override
	public ApprovalBasic selectOneBasic(int approvalCode, int mid) throws ApprovalException {
		
		ApprovalBasic ab = ad.selectOneBasic(sqlSession, approvalCode);
		
		if(ab.getStatus().equals("Y")) {
			ArrayList<HashMap<Integer, String>> mlist = new ArrayList<>();
			
			
			if(ab.getFirstApproval() != 0) {
				HashMap<Integer, String> memberMap = null;
				
				
				switch(ab.getFirstApproval()) {
					case 1 : memberMap = ad.selectAccounter(sqlSession, mid); break;
					case 2 : memberMap = ad.selectHRer(sqlSession, mid); break;
					case 3 : memberMap = ad.selectLeader(sqlSession, mid); break;
					case 4 : memberMap = ad.selectHRLeader(sqlSession); break;
					case 5 : memberMap = ad.selectCEO(sqlSession); break;
				}
				
				mlist.add(memberMap);
				
			}
			if(ab.getSecondApproval() != 0) {
				HashMap<Integer, String> memberMap = null;
				
				
				switch(ab.getSecondApproval()) {
					case 1 : memberMap = ad.selectAccounter(sqlSession, mid); break;
					case 2 : memberMap = ad.selectHRer(sqlSession, mid); break;
					case 3 : memberMap = ad.selectLeader(sqlSession, mid); break;
					case 4 : memberMap = ad.selectHRLeader(sqlSession); break;
					case 5 : memberMap = ad.selectCEO(sqlSession); break;
				}
				
				mlist.add(memberMap);
				
				
			}
			if(ab.getThirdApproval() != 0) {
				HashMap<Integer, String> memberMap = null;
				
				
				switch(ab.getThirdApproval()) {
					case 1 : memberMap = ad.selectAccounter(sqlSession, mid); break;
					case 2 : memberMap = ad.selectHRer(sqlSession, mid); break;
					case 3 : memberMap = ad.selectLeader(sqlSession, mid); break;
					case 4 : memberMap = ad.selectHRLeader(sqlSession); break;
					case 5 : memberMap = ad.selectCEO(sqlSession); break;
				}
				
				mlist.add(memberMap);
				
			}
			
			ab.setMlist(mlist);
			
		}
		
		return ab;
	}

	@Override
	public ArrayList<ApprovalBasic> selectListApprovalLine() {
		
		ArrayList<ApprovalBasic> ablist = ad.selectListApprovalLine(sqlSession);
		//System.out.println("Service ablist : " + ablist);
		
		return ablist;
	}

	@Override
	public int updateApprovalLine(ArrayList<ApprovalBasic> ablist2) {
		
		int result = 0;
		for(int i=0; i<ablist2.size(); i++) {
			//System.out.println("Service : " + ablist2.get(i));
			result += ad.updateApprovalLine(sqlSession, ablist2.get(i));
		}
		
		
		return result;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListApproval(int mid) {
		
		ArrayList<HashMap<String, Object>> alist = ad.selectListApproval(sqlSession, mid);
		//System.out.println("Service alist : " +alist);
		
		return alist;
	}

	@Override
	public Approval selectApproval(int approvalId) {
		
		Approval app = ad.selectApproval(sqlSession, approvalId);
		ArrayList<ApprovalHistory> ahlist = ad.selectListAppHis(sqlSession, approvalId);
		app.setAhlist(ahlist);
		Attachment attach = ad.selectAttachApproval(sqlSession, approvalId);
		app.setAttach(attach);
		
		return app;
	}

	@Override
	public int updateSuccessApp(ArrayList<ApprovalHistory> ahlist) {
		int result = 0;
		for(int i=0; i<ahlist.size(); i++) {
			if(ahlist.get(i).getFinalYn().equals("N") && ahlist.get(i).getResult()==1) { //마지막결재X && 대기중인 결재!
				//2차까지+1차결재, 3차까지+1차결재, 3차까지+2차결재
				//result : 3(승인)
				ahlist.get(i).setResult(3);
				result += ad.updateSuccessAppNotFinal(sqlSession, ahlist.get(i));
				
				//(approval) status : 2(진행중)
				result += ad.updateApprovalStatus2(sqlSession, ahlist.get(i).getApprovalId());
				
			} else if(ahlist.get(i).getFinalYn().equals("Y") && ahlist.get(i).getResult()==1){ //마지막결재ㅇ && 대기중인 결재!!
				//결재끝!(1차까지+1차결재, 2차까지+2차결재, 3차까지+3차결재)
				//result : 3(승인)
				ahlist.get(i).setResult(3);
				result += ad.updateSuccessAppNotFinal(sqlSession, ahlist.get(i));
				
				//(approval) status : 3(완료)
				result += ad.updateApprovalStatus3(sqlSession, ahlist.get(i).getApprovalId());
				
			} else if(ahlist.get(i).getFinalYn().equals("N") && ahlist.get(i).getResult()==2){ //마지막결재X && 예약중인 결재!!
				//3차까지+2차결재
				//result : 1(대기)
				ahlist.get(i).setResult(1);
				result += ad.updateWaitingApp(sqlSession, ahlist.get(i));
				
			} else { //마지막결재o && 예약중인 결재
				if(ahlist.size()==2) { //2차까지+2차결재(1차결재중), 3차까지+3차결재(2차결재중)
					//result : 1(대기)
					ahlist.get(i).setResult(1);
					result += ad.updateWaitingApp(sqlSession, ahlist.get(i));
					
				} else { // 3차까지+3차결재(1차결재중)
					//result : 2(예약, 그대로!)
				}
				
			}
		}
		
		
		return result;
	}

	@Override
	public HashMap<String, Object> selectVacation(int approvalId) {
		HashMap<String, Object> vhmap = null;
		
		vhmap = ad.selectVacation(sqlSession, approvalId);
		
		return vhmap;
	}

	@Override
	public int updateRefuseApp(ApprovalHistory ah) {
		int result = 0;
		
		int result1 = ad.updateRefuseApp(sqlSession, ah);
		int result2 = 0;
		if(result1>0) {
			result2 = ad.updateApprovalStatus4(sqlSession, ah.getApprovalId());
		}
		
		if(result1>0 && result2>0) {
			result = 1;
		}
		
		return result;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListAllApproval(int mid, PageInfo pi) {
		return ad.selectListAllApproval(sqlSession, mid, pi);
	}

	@Override
	public int getListCount(int mid) {
		return ad.getListCount(sqlSession, mid);
	}

	@Override
	public int getListCountSearch(HashMap<String, Object> hmap) {
		return ad.getListCountSearch(sqlSession, hmap);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchApprovalList(HashMap<String, Object> hmap, PageInfo pi) {
		return ad.searchApprovalList(sqlSession, hmap, pi);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListAllApprovalRequest(int mid, PageInfo pi) {
		return ad.selectListAllApprovalRequest(sqlSession, mid, pi);
	}

	@Override
	public int getRequestListCount(int mid) {
		return ad.getRequestListCount(sqlSession, mid);
	}

	@Override
	public HashMap<String, Object> selectMovement(int approvalId) {
		return ad.selectMovement(sqlSession, approvalId);
	}

	@Override
	public HashMap<String, Object> selectMovementBefore(Movement m) {
		return ad.selectMovementBefore(sqlSession, m);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectEvaluation(int approvalId) {
		return ad.selectEvaluation(sqlSession, approvalId);
	}

	@Override
	public HashMap<String, Object> selectSpecialAttendance(int approvalId) {
		return ad.selectSpecialAttendance(sqlSession, approvalId);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchRequestApprovalList(HashMap<String, Object> hmap, PageInfo pi) {
		return ad.searchRequestApprovalList(sqlSession, hmap, pi);
	}

	@Override
	public int getListRequestCountSearch(HashMap<String, Object> hmap) {
		return ad.getListRequestCountSearch(sqlSession, hmap);
	}

	@Override
	public Attachment selectAttachment(int attid) {
		return ad.selectattachment(sqlSession, attid);
	}

	@Override
	public ArrayList<Alarm> selectAlarm(int mid) {
		return ad.selectAlarmList(sqlSession, mid);
	}

	@Override
	public int updateAlarm(int alarmId) {
		return ad.updateAlarm(sqlSession, alarmId);
	}

	@Override
	public Alarm insertAlarm(Alarm al) {
		
		Alarm resultAlarm = ad.insertAlarm(sqlSession, al);
		
		return resultAlarm;
	}

	@Override
	public Alarm selectAlarmOne(int alarmId) {
		return ad.selectAlarmOne(sqlSession, alarmId);
	}

	@Override
	public ArrayList<Schedule> selectScheduleAll(HashMap<String, Object> rangeMap) {
		return ad.selectScheduleAll(sqlSession, rangeMap);
	}

	@Override
	public int updateScheduleByMove(Schedule sch) {
		return ad.updateScheduleByMove(sqlSession, sch);
	}

	@Override
	public int deleteSchedule(int scheduleCode) {
		return ad.deleteSchedule(sqlSession, scheduleCode);
	}

	@Override
	public int insertSchedule(Schedule sch) {
		return ad.insertSchedule(sqlSession, sch);
	}

	@Override
	public int updateSchedule(Schedule sch) {
		return ad.updateSchedule(sqlSession, sch);
	}

	@Override
	public Evaluation selectEvaluationByApp(int approvalId) {
		Evaluation evaluation = ad.selectEvaluationOne(sqlSession, approvalId);
		
		evaluation.setEdlist(ad.selectEvaluationDetailList(sqlSession, evaluation.getEvaluationId()));
		
		
		return evaluation;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectMonthAttList(int approvalId) {
		return ad.selectMonthAttList(sqlSession, approvalId);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectGroupListforOrg() {
		return ad.selectGroupListforOrg(sqlSession);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectTeamListforOrg() {
		return ad.selectTeamListforOrg(sqlSession);
	}

	@Override
	public HashMap<Integer, ArrayList<String>> selectPeopleListforOrg(ArrayList<HashMap<String, Object>> tlist) {
		HashMap<Integer, ArrayList<String>> peopleMap = new HashMap<Integer, ArrayList<String>>();
		for(int i=0;i<tlist.size();i++) {
			ArrayList<String> templist = ad.selectPeopleListforOrg(sqlSession, (Integer)tlist.get(i).get("teamCode"));
			
			peopleMap.put((Integer)tlist.get(i).get("teamCode"), templist);
		}
		
		return peopleMap;
	}
	
	
}















