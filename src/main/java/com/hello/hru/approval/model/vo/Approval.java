package com.hello.hru.approval.model.vo;

import java.sql.Timestamp;
import java.util.ArrayList;
import com.hello.hru.common.Attachment;

public class Approval {
	private int approvalId;
	private int approvalCode;
	private int mid;
	private String mName;
	private Timestamp approvalDate;
	private int status;
	private String approvalTitle;
	private ArrayList<ApprovalHistory> ahlist;
	private Attachment attach;
	
	public Approval() {}
	

	public Approval(int approvalId, int approvalCode, int mid, String mName, Timestamp approvalDate, int status,
			String approvalTitle, ArrayList<ApprovalHistory> ahlist, Attachment attach) {
		super();
		this.approvalId = approvalId;
		this.approvalCode = approvalCode;
		this.mid = mid;
		this.mName = mName;
		this.approvalDate = approvalDate;
		this.status = status;
		this.approvalTitle = approvalTitle;
		this.ahlist = ahlist;
		this.attach = attach;
	}


	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public int getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(int approvalCode) {
		this.approvalCode = approvalCode;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public Timestamp getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Timestamp approvalDate) {
		this.approvalDate = approvalDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getApprovalTitle() {
		return approvalTitle;
	}

	public void setApprovalTitle(String approvalTitle) {
		this.approvalTitle = approvalTitle;
	}

	public ArrayList<ApprovalHistory> getAhlist() {
		return ahlist;
	}

	public void setAhlist(ArrayList<ApprovalHistory> ahlist) {
		this.ahlist = ahlist;
	}
	
	
	
	public String getmName() {
		return mName;
	}



	public void setmName(String mName) {
		this.mName = mName;
	}

	
	

	public Attachment getAttach() {
		return attach;
	}


	public void setAttach(Attachment attach) {
		this.attach = attach;
	}


	@Override
	public String toString() {
		return "Approval [approvalId=" + approvalId + ", approvalCode=" + approvalCode + ", mid=" + mid + ", mName="
				+ mName + ", approvalDate=" + approvalDate + ", status=" + status + ", approvalTitle=" + approvalTitle
				+ ", ahlist=" + ahlist + ", attach=" + attach + "]";
	}

	
	
}
