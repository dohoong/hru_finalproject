package com.hello.hru.approval.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.approval.exception.ApprovalException;
import com.hello.hru.approval.model.vo.Alarm;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.approval.model.vo.Schedule;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.PageInfo;
import com.hello.hru.common.model.vo.ExcelMember;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.EvaluationDetail;
import com.hello.hru.humanResource.model.vo.Movement;

public interface ApprovalDao {

	ApprovalBasic selectOneBasic(SqlSessionTemplate sqlSession, int approvalCode) throws ApprovalException;

	HashMap<Integer, String> selectAccounter(SqlSessionTemplate sqlSession, int mid);

	HashMap<Integer, String> selectHRer(SqlSessionTemplate sqlSession, int mid);

	HashMap<Integer, String> selectLeader(SqlSessionTemplate sqlSession, int mid);

	HashMap<Integer, String> selectHRLeader(SqlSessionTemplate sqlSession);

	HashMap<Integer, String> selectCEO(SqlSessionTemplate sqlSession);

	int insertApproval(SqlSessionTemplate sqlSession, Approval app);

	int insertApprovalHistory(SqlSessionTemplate sqlSession, ApprovalHistory apphis);

	ArrayList<ApprovalBasic> selectListApprovalLine(SqlSessionTemplate sqlSession);

	int updateApprovalLine(SqlSessionTemplate sqlSession, ApprovalBasic approvalBasic);

	ArrayList<HashMap<String, Object>> selectListApproval(SqlSessionTemplate sqlSession, int mid);

	Approval selectApproval(SqlSessionTemplate sqlSession, int approvalId);

	ArrayList<ApprovalHistory> selectListAppHis(SqlSessionTemplate sqlSession, int approvalId);

	Attachment selectAttachApproval(SqlSessionTemplate sqlSession, int approvalId);

	int updateSuccessAppNotFinal(SqlSessionTemplate sqlSession, ApprovalHistory approvalHistory);

	int updateWaitingApp(SqlSessionTemplate sqlSession, ApprovalHistory approvalHistory);

	int updateApprovalStatus2(SqlSessionTemplate sqlSession, int approvalId);

	int updateApprovalStatus3(SqlSessionTemplate sqlSession, int approvalId);

	HashMap<String, Object> selectVacation(SqlSessionTemplate sqlSession, int approvalId);

	int updateRefuseApp(SqlSessionTemplate sqlSession, ApprovalHistory ah);

	int updateApprovalStatus4(SqlSessionTemplate sqlSession, int approvalId);

	ArrayList<HashMap<String, Object>> selectListAllApproval(SqlSessionTemplate sqlSession, int mid, PageInfo pi);

	int getListCount(SqlSessionTemplate sqlSession, int mid);

	int getListCountSearch(SqlSessionTemplate sqlSession, HashMap<String, Object> hmap);

	ArrayList<HashMap<String, Object>> searchApprovalList(SqlSessionTemplate sqlSession, HashMap<String, Object> hmap,
			PageInfo pi);

	ArrayList<HashMap<String, Object>> selectListAllApprovalRequest(SqlSessionTemplate sqlSession, int mid,
			PageInfo pi);

	int getRequestListCount(SqlSessionTemplate sqlSession, int mid);

	HashMap<String, Object> selectMovement(SqlSessionTemplate sqlSession, int approvalId);

	HashMap<String, Object> selectMovementBefore(SqlSessionTemplate sqlSession, Movement m);

	ArrayList<HashMap<String, Object>> selectEvaluation(SqlSessionTemplate sqlSession, int approvalId);

	HashMap<String, Object> selectSpecialAttendance(SqlSessionTemplate sqlSession, int approvalId);

	ArrayList<HashMap<String, Object>> searchRequestApprovalList(SqlSessionTemplate sqlSession,
			HashMap<String, Object> hmap, PageInfo pi);

	int getListRequestCountSearch(SqlSessionTemplate sqlSession, HashMap<String, Object> hmap);

	int insertMembers(SqlSessionTemplate sqlSession, ExcelMember excelMember);

	Attachment selectattachment(SqlSessionTemplate sqlSession, int attid);

	Alarm insertAlarm(SqlSessionTemplate sqlSession, Alarm al);

	ArrayList<Alarm> selectAlarmList(SqlSessionTemplate sqlSession, int mid);

	int updateAlarm(SqlSessionTemplate sqlSession, int alarmId);

	Alarm selectAlarmOne(SqlSessionTemplate sqlSession, int alarmId);

	ArrayList<Schedule> selectScheduleAll(SqlSessionTemplate sqlSession, HashMap<String, Object> rangeMap);

	int updateScheduleByMove(SqlSessionTemplate sqlSession, Schedule sch);

	int deleteSchedule(SqlSessionTemplate sqlSession, int scheduleCode);

	int insertSchedule(SqlSessionTemplate sqlSession, Schedule sch);

	int updateSchedule(SqlSessionTemplate sqlSession, Schedule sch);

	Evaluation selectEvaluationOne(SqlSessionTemplate sqlSession, int approvalId);

	ArrayList<EvaluationDetail> selectEvaluationDetailList(SqlSessionTemplate sqlSession, int evaluationId);

	ArrayList<HashMap<String, Object>> selectMonthAttList(SqlSessionTemplate sqlSession, int approvalId);

	ArrayList<HashMap<String, Object>> selectGroupListforOrg(SqlSessionTemplate sqlSession);

	ArrayList<String> selectPeopleListforOrg(SqlSessionTemplate sqlSession, int teamCode);

	ArrayList<HashMap<String, Object>> selectTeamListforOrg(SqlSessionTemplate sqlSession);

}
