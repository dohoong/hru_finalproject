package com.hello.hru.approval.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.approval.exception.ApprovalException;
import com.hello.hru.approval.model.vo.Alarm;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.approval.model.vo.Schedule;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.PageInfo;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.Movement;

public interface ApprovalService {

	ApprovalBasic selectOneBasic(int approvalCode, int mid) throws ApprovalException;

	ArrayList<ApprovalBasic> selectListApprovalLine();

	int updateApprovalLine(ArrayList<ApprovalBasic> ablist2);

	ArrayList<HashMap<String, Object>> selectListApproval(int mid);

	Approval selectApproval(int approvalId);

	int updateSuccessApp(ArrayList<ApprovalHistory> ahlist);

	HashMap<String, Object> selectVacation(int approvalId);

	int updateRefuseApp(ApprovalHistory ah);

	ArrayList<HashMap<String, Object>> selectListAllApproval(int mid, PageInfo pi);

	int getListCount(int mid);

	int getListCountSearch(HashMap<String, Object> hmap);

	ArrayList<HashMap<String, Object>> searchApprovalList(HashMap<String, Object> hmap, PageInfo pi);

	ArrayList<HashMap<String, Object>> selectListAllApprovalRequest(int mid, PageInfo pi);

	int getRequestListCount(int mid);

	HashMap<String, Object> selectMovement(int approvalId);

	HashMap<String, Object> selectMovementBefore(Movement m);

	ArrayList<HashMap<String, Object>> selectEvaluation(int approvalId);

	HashMap<String, Object> selectSpecialAttendance(int approvalId);

	ArrayList<HashMap<String, Object>> searchRequestApprovalList(HashMap<String, Object> hmap, PageInfo pi);

	int getListRequestCountSearch(HashMap<String, Object> hmap);

	Attachment selectAttachment(int attid);

	ArrayList<Alarm> selectAlarm(int mid);

	int updateAlarm(int alarmId);

	Alarm insertAlarm(Alarm al);

	Alarm selectAlarmOne(int alarmId);

	ArrayList<Schedule> selectScheduleAll(HashMap<String, Object> rangeMap);

	int updateScheduleByMove(Schedule sch);

	int deleteSchedule(int scheduleCode);

	int insertSchedule(Schedule sch);

	int updateSchedule(Schedule sch);

	Evaluation selectEvaluationByApp(int approvalId);

	ArrayList<HashMap<String, Object>> selectMonthAttList(int approvalId);

	ArrayList<HashMap<String, Object>> selectGroupListforOrg();

	ArrayList<HashMap<String, Object>> selectTeamListforOrg();

	HashMap<Integer, ArrayList<String>> selectPeopleListforOrg(ArrayList<HashMap<String, Object>> tlist);

}
