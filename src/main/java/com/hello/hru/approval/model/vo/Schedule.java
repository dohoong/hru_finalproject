package com.hello.hru.approval.model.vo;

public class Schedule {
	private int scheduleCode;
	private String startDate;
	private String endDate;
	private String scheduleTitle;
	private int category;
	private String scheduleContent;
	
	public Schedule() {}

	public Schedule(int scheduleCode, String startDate, String endDate, String scheduleTitle, int category,
			String scheduleContent) {
		super();
		this.scheduleCode = scheduleCode;
		this.startDate = startDate;
		this.endDate = endDate;
		this.scheduleTitle = scheduleTitle;
		this.category = category;
		this.scheduleContent = scheduleContent;
	}

	public int getScheduleCode() {
		return scheduleCode;
	}

	public void setScheduleCode(int scheduleCode) {
		this.scheduleCode = scheduleCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getScheduleTitle() {
		return scheduleTitle;
	}

	public void setScheduleTitle(String scheduleTitle) {
		this.scheduleTitle = scheduleTitle;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getScheduleContent() {
		return scheduleContent;
	}

	public void setScheduleContent(String scheduleContent) {
		this.scheduleContent = scheduleContent;
	}

	@Override
	public String toString() {
		return "Schedule [scheduleCode=" + scheduleCode + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", scheduleTitle=" + scheduleTitle + ", category=" + category + ", scheduleContent=" + scheduleContent
				+ "]";
	}
	
	
}
