package com.hello.hru.approval.model.vo;

import java.sql.Date;
import java.sql.Timestamp;

public class ApprovalHistory {
	private int apphisId;
	private int result;
	private Timestamp apphisDate;
	private int approvalId;
	private String refuseReason;
	private int mid;
	private String mName;
	private String finalYn;
	
	public ApprovalHistory() {}
	
	

	public ApprovalHistory(int apphisId, int result, Timestamp apphisDate, int approvalId, String refuseReason, int mid,
			String mName, String finalYn) {
		super();
		this.apphisId = apphisId;
		this.result = result;
		this.apphisDate = apphisDate;
		this.approvalId = approvalId;
		this.refuseReason = refuseReason;
		this.mid = mid;
		this.mName = mName;
		this.finalYn = finalYn;
	}



	public int getApphisId() {
		return apphisId;
	}

	public void setApphisId(int apphisId) {
		this.apphisId = apphisId;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public Timestamp getApphisDate() {
		return apphisDate;
	}

	public void setApphisDate(Timestamp apphisDate) {
		this.apphisDate = apphisDate;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getFinalYn() {
		return finalYn;
	}

	public void setFinalYn(String finalYn) {
		this.finalYn = finalYn;
	}
	
	
	
	public String getmName() {
		return mName;
	}



	public void setmName(String mName) {
		this.mName = mName;
	}



	@Override
	public String toString() {
		return "ApprovalHistory [apphisId=" + apphisId + ", result=" + result + ", apphisDate=" + apphisDate
				+ ", approvalId=" + approvalId + ", refuseReason=" + refuseReason + ", mid=" + mid + ", mName=" + mName
				+ ", finalYn=" + finalYn + "]";
	}
	
	
}
