package com.hello.hru.approval.model.vo;

import java.util.ArrayList;
import java.util.HashMap;

public class ApprovalBasic {
	private int approvalCode;
	private int firstApproval;
	private int secondApproval;
	private int thirdApproval;
	private int approvalType;
	private String approvalName;
	private String status;
	private ArrayList<ApprovalBasic> ablist;
	private ArrayList<HashMap<Integer, String>> mlist;
	
	public ApprovalBasic() {}
	
	

	public ApprovalBasic(int approvalCode, int firstApproval, int secondApproval, int thirdApproval, int approvalType,
			String approvalName, String status, ArrayList<ApprovalBasic> ablist,
			ArrayList<HashMap<Integer, String>> mlist) {
		super();
		this.approvalCode = approvalCode;
		this.firstApproval = firstApproval;
		this.secondApproval = secondApproval;
		this.thirdApproval = thirdApproval;
		this.approvalType = approvalType;
		this.approvalName = approvalName;
		this.status = status;
		this.ablist = ablist;
		this.mlist = mlist;
	}



	public int getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(int approvalCode) {
		this.approvalCode = approvalCode;
	}

	public int getFirstApproval() {
		return firstApproval;
	}

	public void setFirstApproval(int firstApproval) {
		this.firstApproval = firstApproval;
	}

	public int getSecondApproval() {
		return secondApproval;
	}

	public void setSecondApproval(int secondApproval) {
		this.secondApproval = secondApproval;
	}

	public int getThirdApproval() {
		return thirdApproval;
	}

	public void setThirdApproval(int thirdApproval) {
		this.thirdApproval = thirdApproval;
	}

	public int getApprovalType() {
		return approvalType;
	}

	public void setApprovalType(int approvalType) {
		this.approvalType = approvalType;
	}

	public String getApprovalName() {
		return approvalName;
	}

	public void setApprovalName(String approvalName) {
		this.approvalName = approvalName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<HashMap<Integer, String>> getMlist() {
		return mlist;
	}

	public void setMlist(ArrayList<HashMap<Integer, String>> mlist) {
		this.mlist = mlist;
	}
	

	public ArrayList<ApprovalBasic> getAblist() {
		return ablist;
	}



	public void setAblist(ArrayList<ApprovalBasic> ablist) {
		this.ablist = ablist;
	}



	@Override
	public String toString() {
		return "ApprovalBasic [approvalCode=" + approvalCode + ", firstApproval=" + firstApproval + ", secondApproval="
				+ secondApproval + ", thirdApproval=" + thirdApproval + ", approvalType=" + approvalType
				+ ", approvalName=" + approvalName + ", status=" + status + ", ablist=" + ablist + ", mlist=" + mlist
				+ "]";
	}

	
	
}
