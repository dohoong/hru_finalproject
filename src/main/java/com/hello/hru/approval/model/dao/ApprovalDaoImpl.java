package com.hello.hru.approval.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.approval.exception.ApprovalException;
import com.hello.hru.approval.model.vo.Alarm;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.approval.model.vo.Schedule;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.PageInfo;
import com.hello.hru.common.model.vo.ExcelMember;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.EvaluationDetail;
import com.hello.hru.humanResource.model.vo.Movement;

@Repository
public class ApprovalDaoImpl implements ApprovalDao{

	@Override
	public ApprovalBasic selectOneBasic(SqlSessionTemplate sqlSession, int approvalCode) throws ApprovalException {
		
		ApprovalBasic ab = sqlSession.selectOne("Approval.selectOneBasic", approvalCode);
		
		if(ab == null) {
			throw new ApprovalException("결재내역이 존재하지 않습니다!");
		}
		
		return ab;
	}

	@Override
	public HashMap<Integer, String> selectAccounter(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Approval.selectAccounter", mid);
	}

	@Override
	public HashMap<Integer, String> selectHRer(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Approval.selectHRer", mid);
	}

	@Override
	public HashMap<Integer, String> selectLeader(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Approval.selectLeader", mid);
	}

	@Override
	public HashMap<Integer, String> selectHRLeader(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Approval.selectHRLeader");
	}

	@Override
	public HashMap<Integer, String> selectCEO(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Approval.selectCEO");
	}
	
	@Override
	public int insertApproval(SqlSessionTemplate sqlSession, Approval app) {
		sqlSession.insert("Approval.insertApproval", app);
		return app.getApprovalId();
	}

	@Override
	public int insertApprovalHistory(SqlSessionTemplate sqlSession, ApprovalHistory apphis) {
		System.out.println("history :" +apphis);
		return sqlSession.insert("Approval.insertApprovalHistory", apphis);
	}

	@Override
	public ArrayList<ApprovalBasic> selectListApprovalLine(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Approval.selectListApprovalLine");
	}

	@Override
	public int updateApprovalLine(SqlSessionTemplate sqlSession, ApprovalBasic approvalBasic) {
		return sqlSession.update("Approval.updateApprovalLine", approvalBasic);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListApproval(SqlSessionTemplate sqlSession, int mid) {
		return (ArrayList)sqlSession.selectList("Approval.selectListApproval", mid);
	}

	@Override
	public Approval selectApproval(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.selectOne("Approval.selectOneApproval", approvalId);
	}

	@Override
	public ArrayList<ApprovalHistory> selectListAppHis(SqlSessionTemplate sqlSession, int approvalId) {
		return (ArrayList)sqlSession.selectList("Approval.selectApprovalHistory", approvalId);
	}

	@Override
	public Attachment selectAttachApproval(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.selectOne("Approval.selectAttachApproval", approvalId);
	}

	@Override
	public int updateSuccessAppNotFinal(SqlSessionTemplate sqlSession, ApprovalHistory approvalHistory) {
		return sqlSession.update("Approval.updateSuccessNotFinal", approvalHistory);
	}

	@Override
	public int updateWaitingApp(SqlSessionTemplate sqlSession, ApprovalHistory approvalHistory) {
		return sqlSession.update("Approval.updateWaitingApp", approvalHistory);
	}

	@Override
	public int updateApprovalStatus2(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.update("Approval.updateAppStatus2", approvalId);
	}

	@Override
	public int updateApprovalStatus3(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.update("Approval.updateAppStatus3", approvalId);
	}

	@Override
	public HashMap<String, Object> selectVacation(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.selectOne("Approval.selectOneVacation", approvalId);
	}

	@Override
	public int updateRefuseApp(SqlSessionTemplate sqlSession, ApprovalHistory ah) {
		return sqlSession.update("Approval.updateRefuseApp", ah);
	}

	@Override
	public int updateApprovalStatus4(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.update("Approval.updateAppStatus4", approvalId);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListAllApproval(SqlSessionTemplate sqlSession, int mid, PageInfo pi) {
		
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return (ArrayList)sqlSession.selectList("Approval.selectListAllApproval", mid, rowBounds);
	}

	@Override
	public int getListCount(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Approval.getListCount", mid);
	}

	@Override
	public int getListCountSearch(SqlSessionTemplate sqlSession, HashMap<String, Object> hmap) {
		return sqlSession.selectOne("Approval.getListCountSearch", hmap);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchApprovalList(SqlSessionTemplate sqlSession,
			HashMap<String, Object> hmap, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return (ArrayList)sqlSession.selectList("Approval.selectApprovalList", hmap, rowBounds);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListAllApprovalRequest(SqlSessionTemplate sqlSession, int mid, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return (ArrayList)sqlSession.selectList("Approval.selectListAllApprovalRequest", mid, rowBounds);
	}

	@Override
	public int getRequestListCount(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Approval.getRequestListCount", mid);
	}

	@Override
	public HashMap<String, Object> selectMovement(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.selectOne("Approval.selectMovement", approvalId);
	}

	@Override
	public HashMap<String, Object> selectMovementBefore(SqlSessionTemplate sqlSession, Movement m) {
		return sqlSession.selectOne("Approval.selectMovementBefore", m);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectEvaluation(SqlSessionTemplate sqlSession, int approvalId) {
		return (ArrayList)sqlSession.selectList("Approval.selectEvaluation", approvalId);
	}

	@Override
	public HashMap<String, Object> selectSpecialAttendance(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.selectOne("Approval.selectSpecialAttendance", approvalId);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchRequestApprovalList(SqlSessionTemplate sqlSession,
			HashMap<String, Object> hmap, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return (ArrayList)sqlSession.selectList("Approval.searchRequestList", hmap, rowBounds);
	}

	@Override
	public int getListRequestCountSearch(SqlSessionTemplate sqlSession, HashMap<String, Object> hmap) {
		return sqlSession.selectOne("Approval.getListRequestCountSearch", hmap);
	}

	@Override
	public int insertMembers(SqlSessionTemplate sqlSession, ExcelMember excelMember) {
		return sqlSession.insert("Approval.insertExcelMember", excelMember);
	}

	@Override
	public Attachment selectattachment(SqlSessionTemplate sqlSession, int attid) {
		return sqlSession.selectOne("Approval.selectAttachment", attid);
	}

	@Override
	public Alarm insertAlarm(SqlSessionTemplate sqlSession, Alarm al) {
		sqlSession.insert("Approval.insertAlarm", al);
		return al;
	}

	@Override
	public ArrayList<Alarm> selectAlarmList(SqlSessionTemplate sqlSession, int mid) {
		return (ArrayList) sqlSession.selectList("Approval.selectAlarmList", mid);
	}

	@Override
	public int updateAlarm(SqlSessionTemplate sqlSession, int alarmId) {
		return sqlSession.update("Approval.updateAlarm", alarmId);
	}

	@Override
	public Alarm selectAlarmOne(SqlSessionTemplate sqlSession, int alarmId) {
		return sqlSession.selectOne("Approval.selectAlarmOne", alarmId);
	}

	@Override
	public ArrayList<Schedule> selectScheduleAll(SqlSessionTemplate sqlSession, HashMap<String, Object> rangeMap) {
		return (ArrayList)sqlSession.selectList("Approval.selectScheduleAll", rangeMap);
	}

	@Override
	public int updateScheduleByMove(SqlSessionTemplate sqlSession, Schedule sch) {
		return sqlSession.update("Approval.updateScheduleByMove", sch);
	}

	@Override
	public int deleteSchedule(SqlSessionTemplate sqlSession, int scheduleCode) {
		return sqlSession.delete("Approval.deleteScheduleWrite", scheduleCode);
	}

	@Override
	public int insertSchedule(SqlSessionTemplate sqlSession, Schedule sch) {
		return sqlSession.insert("Approval.insertScheduleWrite", sch);
	}

	@Override
	public int updateSchedule(SqlSessionTemplate sqlSession, Schedule sch) {
		return sqlSession.update("Approval.updateScheduleWrite", sch);
	}

	@Override
	public Evaluation selectEvaluationOne(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.selectOne("Approval.selectEvaluationOne", approvalId);
	}

	@Override
	public ArrayList<EvaluationDetail> selectEvaluationDetailList(SqlSessionTemplate sqlSession, int evaluationId) {
		return (ArrayList)sqlSession.selectList("Approval.selectEvaluationDetailList", evaluationId);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectMonthAttList(SqlSessionTemplate sqlSession, int approvalId) {
		return (ArrayList)sqlSession.selectList("Approval.selectMonthAttList", approvalId);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectGroupListforOrg(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Approval.selectGroupListforOrg");
	}

	@Override
	public ArrayList<String> selectPeopleListforOrg(SqlSessionTemplate sqlSession, int teamCode) {
		return (ArrayList)sqlSession.selectList("Approval.selectPeopleListforOrg", teamCode);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectTeamListforOrg(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Approval.selectTeamListforOrg");
	}


}
