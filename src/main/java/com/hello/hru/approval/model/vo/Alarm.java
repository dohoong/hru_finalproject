package com.hello.hru.approval.model.vo;

public class Alarm {
	private int alarmId;
	private String content;
	private String readYn;
	private int sendMid;
	private int takeMid;
	private String alarmDate;
	
	public Alarm() {}

	

	public Alarm(int alarmId, String content, String readYn, int sendMid, int takeMid, String alarmDate) {
		super();
		this.alarmId = alarmId;
		this.content = content;
		this.readYn = readYn;
		this.sendMid = sendMid;
		this.takeMid = takeMid;
		this.alarmDate = alarmDate;
	}



	public int getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(int alarmId) {
		this.alarmId = alarmId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReadYn() {
		return readYn;
	}

	public void setReadYn(String readYn) {
		this.readYn = readYn;
	}

	public int getSendMid() {
		return sendMid;
	}

	public void setSendMid(int sendMid) {
		this.sendMid = sendMid;
	}

	public int getTakeMid() {
		return takeMid;
	}

	public void setTakeMid(int takeMid) {
		this.takeMid = takeMid;
	}
	
	
	
	public String getAlarmDate() {
		return alarmDate;
	}



	public void setAlarmDate(String alarmDate) {
		this.alarmDate = alarmDate;
	}



	@Override
	public String toString() {
		return "Alarm [alarmId=" + alarmId + ", content=" + content + ", readYn=" + readYn + ", sendMid=" + sendMid
				+ ", takeMid=" + takeMid + ", alarmDate=" + alarmDate + "]";
	}

	
	
	
}
