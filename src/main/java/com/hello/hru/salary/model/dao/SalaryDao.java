package com.hello.hru.salary.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.common.model.vo.Search;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.SearchMember;
import com.hello.hru.salary.model.vo.AccountStandard;
import com.hello.hru.salary.model.vo.MemberBasicInfo;
import com.hello.hru.salary.model.vo.MonthSalary;
import com.hello.hru.salary.model.vo.MonthSalaryDetail;
import com.hello.hru.salary.model.vo.SalaryStatus;
import com.hello.hru.salary.model.vo.SaveSalary;
import com.hello.hru.salary.model.vo.Standard;
import com.hello.hru.salary.model.vo.StandardInformation;
import com.hello.hru.salary.model.vo.TaxBasic;
import com.hello.hru.system.model.vo.Team;

public interface SalaryDao {
	//급여기준
	ArrayList<Standard> selectSalaryStandard(SqlSessionTemplate sqlSession);
	//근태기준
	ArrayList<Standard> selectAttendanceStandard(SqlSessionTemplate sqlSession);
	//기본급,상여금
	ArrayList<AccountStandard> selectAccountStandard(SqlSessionTemplate sqlSession);
	//비과세 항목
	ArrayList<AccountStandard> selectNoneAccountStandard(SqlSessionTemplate sqlSession);
	//연장근무수당 항목
	ArrayList<AccountStandard> selectExtensionAccountStandard(SqlSessionTemplate sqlSession);
	//4대보험 요율
	ArrayList<AccountStandard> selectInsuranceAccountStandard(SqlSessionTemplate sqlSession);
	//기준정보 업데이트
	int updateStandardInfofirst(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoSecond(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoThree(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoFour(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoFive(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoSix(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoSeven(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoEight(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoNine(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoTen(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoEleven(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	int updateStandardInfoTwelve(SqlSessionTemplate sqlSession, StandardInformation stdinfo);
	//급여정보 업데이트
	int updateSalaryInfoTwo(SqlSessionTemplate sqlSession, AccountStandard as);
	int updateSalaryInfoOne(SqlSessionTemplate sqlSession, AccountStandard as);
	//비과세정보 
	int insertNonTaxAdd(SqlSessionTemplate sqlSession, AccountStandard accstd);
	int updateNoneTax(SqlSessionTemplate sqlSession, AccountStandard accstd);
	int deleteNoneTax(SqlSessionTemplate sqlSession, AccountStandard accstd);
	//연장근무수당
	int insertExtentsionAdd(SqlSessionTemplate sqlSession, AccountStandard accstd);
	int updateExtensionUdpate(SqlSessionTemplate sqlSession, AccountStandard accstd);
	int deleteExtentsionDelete(SqlSessionTemplate sqlSession, AccountStandard accstd);
	//4대보험
	int updateFourInsurance(SqlSessionTemplate sqlSession, AccountStandard as);
	
	//급여정보-페이지이동
	ArrayList<Team> selectTeamList(SqlSessionTemplate sqlSession);
	//급여정보-급여정보-검색결과
	ArrayList<SearchMember> searchMember(SqlSessionTemplate sqlSession, Search search);
	//급여정보-급여정보-사원기본정보 검색
	HashMap<String, Object> selectMemberBasicInfo(int mid, SqlSessionTemplate sqlSession);
	//급여정보-급여정보-사원가족정보 검색
	ArrayList<HashMap<String, Object>> selectMemberFailyInfo(SqlSessionTemplate sqlSession, int mid);
	//급여정보-급여정보-사원급여정보 검색
	HashMap<String, Object> selectMemberSalaryInfo(SqlSessionTemplate sqlSession, int mid);
	//급여정보-급여정보-사원기본급여정보 업데이트 - 1
	int updateMemberBasicInfoOne(SqlSessionTemplate sqlSession, MemberBasicInfo mbInfo);
	//급여정보-급여정보-사원기본급여정보 업데이트 - 2
	int updateMemberBasicInfoTwo(SqlSessionTemplate sqlSession, MemberBasicInfo mbInfo);
	//급여정보-급여정보-사원기본급여정보 업데이트 - 3
	int updateMemberBasicInfoThree(SqlSessionTemplate sqlSession, MemberBasicInfo mbInfo);
	//급여정보-급여입력-팀 검색 - 1 - 사원 MID목록 가져오기
	ArrayList<Member> selectsearchMidList(Search search, SqlSessionTemplate sqlSession);
	//급여정보-급여입력-팀 검색 - 2 -팀 월근태 현황 가져오기
	ArrayList<MonthAttendance> selectMonthlyList(Search search, SqlSessionTemplate sqlSession);
	//급여정보-급여입력-팀 검색 - 3 - 전체 급여기준 가져오기
	ArrayList<AccountStandard> selectAllAccountStandard(SqlSessionTemplate sqlSession);
	//급여정보-급여입력-회원 월 급여 정보 확인 - 4 - 
	MonthSalary selectMonthSalary(SqlSessionTemplate sqlSession, Search search);
	//급여정보-급여입력-회원 월급여지급 테이블 인서트 - 5 -
	int insertMonthSalary(SqlSessionTemplate sqlSession, MonthSalary ms);
	//급여정보 - 급여입력 - 회원 상세월급여지급 테이블 인서트 - 6 - 
	int insertMonthSalaryDetail(SqlSessionTemplate sqlSession, MonthSalaryDetail msd);
	//급여정보 - 급여입력 - 회원 월급 , 부양가족 , 자녀수에 따른 간이세액표 금액 - 7 -
	int selectTaxBasic(SqlSessionTemplate sqlSession, TaxBasic tbasic);
	//급여정보 - 급여입력 - 검색한 부서의 사원 명단 - 8 -
	ArrayList<SearchMember> searchMonthSalaryTeam(SqlSessionTemplate sqlSession, Search search);
	//급여정보-급여입력- 사원 지급/공제내역 조회
	ArrayList<HashMap<String, Object>> selectMemberMonthSalary(SqlSessionTemplate sqlSession, Search search);
	//급여정보-급여입력- 사원 지급/공제내역 조회 -2-
	ArrayList<HashMap<String, Integer>> selectMemberMonthSalaryTwo(SqlSessionTemplate sqlSession, Search search);
	//급여정보-급여입력- 사원 지급/공제내역 업데이트 -1-
	int updateMemberMonthSalary(SqlSessionTemplate sqlSession, MonthSalary ms);
	//급여정보-급여입력- 사원 지급/공제내역 업데이트 -2-
	int updateMemberMonthSalaryDetail(SqlSessionTemplate sqlSession, SaveSalary save);
	//급여정보-급여입력- 사원 지급/공제내역 업데이트 -3-
	int updateMemberMonthSalaryDetailTwo(SqlSessionTemplate sqlSession, SaveSalary save);
	//급여정보-급여입력- 팀 지급/공제내역 조회
	ArrayList<HashMap<String, Object>> searchTeamSalary(SqlSessionTemplate sqlSession, Search search);
	//급여관리-급여입력 - 팀 지급/공제내역 월급여 아이디 리스트 검색
	ArrayList<Integer> selectSalaryIdList(SqlSessionTemplate sqlSession, Search search);
	//급여관리-급여입력 - 팀 지급/공제내역 전자결재 월 급여 업데이트
	int updateMonthSalaryApproval(SqlSessionTemplate sqlSession, MonthSalary ms);
	//급여관리-급여지급현황-사원 검색
	ArrayList<SearchMember> selectMonthlyWageMember(SqlSessionTemplate sqlSession, Search search);
	//급여관리-급여지급현황-사원 년도 지급현황 
	ArrayList<SalaryStatus> selectMembeYearSalary(SqlSessionTemplate sqlSession, Search search);
	//급여관리-이메일전송-검색 결과 출력(사원 리스트 )
	ArrayList<HashMap<String, Object>> searchMonthEmailSalary(Search search, SqlSessionTemplate sqlSession);
	//급여관리-이메일전송-사원한달급여목록검색
	ArrayList<HashMap<String, Object>> selectMonthSalary(Search search, SqlSessionTemplate sqlSession);
	//사원 - 셀프서비스 - 퇴직신청 (전자결재 - 퇴직금)
	int insertQuitMember(SqlSessionTemplate sqlSession, HashMap<String, Object> mInfo);
	//사원 - 셀프서비스 - 퇴직신청 (전자결재 - 퇴직금 상세 )
	int insertQuitHistoryMember(SqlSessionTemplate sqlSession, HashMap<String, Object> mInfo);
	//사원 - 퇴직금산정  - 사원 검색
	ArrayList<HashMap<String, Object>> selectQuitMemberList(Search search, SqlSessionTemplate sqlSession);
	//급여임력 - 퇴직금 산정 - 마감 - 1
	int updateQuitEnd(SqlSessionTemplate sqlSession, HashMap<String, Object> sList);
	//급여임력 - 퇴직금 산정 - 마감 - 2
	int updateQuitEndDetail(SqlSessionTemplate sqlSession, HashMap<String, Object> sList);
	//급여입력 - 퇴직사원 관리 - 검색결과
	ArrayList<HashMap<String, Object>> selectRetirementMember(SqlSessionTemplate sqlSession, Search search);
	//급여입력 - 퇴직정산 - 계산하기
	HashMap<String, Object> selectMemberQuitBasic(SqlSessionTemplate sqlSession, int mid);
	//급여입력 - 퇴직사원 관리 - 마감  -3 
	int updateMemberQuitInfo(SqlSessionTemplate sqlSession, HashMap<String, Object> sList);
	//급여입력 - 퇴직사원 관리 - 신고내역 
	ArrayList<HashMap<String, Object>> selectRetirementMemberInfo(SqlSessionTemplate sqlSession, Search search);
	
	
	
	

}
 