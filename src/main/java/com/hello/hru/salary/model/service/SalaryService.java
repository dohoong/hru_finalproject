package com.hello.hru.salary.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.model.vo.Search;
import com.hello.hru.member.model.vo.SearchMember;
import com.hello.hru.salary.model.vo.AccountStandard;
import com.hello.hru.salary.model.vo.MemberBasicInfo;
import com.hello.hru.salary.model.vo.SalaryStatus;
import com.hello.hru.salary.model.vo.SaveSalary;
import com.hello.hru.salary.model.vo.StandardInformation;
import com.hello.hru.system.model.vo.Team;


public interface SalaryService {
	//급여/근태기준 정보 불러오기
	HashMap<String, Object> selectsMap();
	//기준정보 업데이트
	int updateStandardInfo(StandardInformation stdinfo);
	//급여정보 업데이트
	int updateSalaryInfo(AccountStandard accstd);
	//비과세항목 추가
	int insertNonTaxAdd(AccountStandard accstd);
	//비과세항목 수정(업데이트)
	int updateNoneTax(AccountStandard accstd);
	//비과세항목 삭제 (삭제)
	int deleteNoneTax(AccountStandard accstd);
	//연장 근무수당 추가
	int insertExtensionAdd(AccountStandard accstd);
	//연장 근무수당 수정
	int updateExtensionUpdate(AccountStandard accstd);
	//연장 근무수당 삭제
	int deleteExtensionDelete(AccountStandard accstd);
	//4대보험 업데이트
	int updateFourInsurance(AccountStandard accstd);
	//급여관리-급여관리-급여기본정보 페이지 이동
	ArrayList<Team> selectTeamList();
	//급여관리-급여관리-검색결과
	ArrayList<SearchMember> searchMember(Search search);
	//급여관리-급여관리-사원 급여기본정보 가져오기
	HashMap<String, Object> selectMemberBasicInfo(int mid);
	//급여관리-급여관리-사원 부양가족 정보 가져오기
	ArrayList<HashMap<String, Object>> selectMemberFailyInfo(int mid);
	//급여관리-급여관리-사원 급여정보 가져오기
	HashMap<String, Object> selectMemberSalaryInfo(int mid);
	//급여관리-급여관리-사원급여 기본정보 업데이트
	int updateMemberBasicInfo(MemberBasicInfo mbInfo);
	//급여관리-급여입력-사원 검색 (월급여테이블 , 지금상세테이블 데이터 인서트 , 최종 검색사원 리턴)
	ArrayList<SearchMember> searchInsertSalary(Search search);
	//급여관리-급여입력-사원 지급/공제내역 검색
	ArrayList<HashMap<String, Object>> selectMemberMonthSalary(Search search);
	//급여관리-급여입력-사원 지급/공제내역 검색 -2- 
	ArrayList<HashMap<String, Integer>> selectMemberMonthSalaryTwo(Search search);
	//급여관리-급여입력-사원 지급/공제내역 업데이트
	int updateMemberMonthSalary(SaveSalary save);
	//급여관리-급여입력-팀 지급/공제내역 검색
	ArrayList<HashMap<String, Object>> searchTeamSalary(Search search);
	//급여관리-급여입력-팀 지급/공제내역 전자결재
	int insertMonthSalary(Approval app, Attachment att, ArrayList<Integer> salaryId);
	//급여관리-급여입력 - 팀 지급/공제내역 월급여 아이디 리스트 검색
	ArrayList<Integer> selectSalaryIdList(Search search);
	//급여관리-급여지급현황-사원 검색
	ArrayList<SearchMember> selectMonthlyWageMember(Search search);
	//급여관리-급여지급현황-사원 년도 급여현황 가져오기
	HashMap<String, ArrayList<SalaryStatus>> selectMemberYearSalary(Search search);
	//급여관리-이메일전송- 검색 결과 (사원 리스트 )
	ArrayList<HashMap<String, Object>> searchMonthEmailSalary(Search search);
	//급여관리-이메일전송-사원월급여목록	
	ArrayList<HashMap<String, Object>> selectMonthSalary(Search search);
	//사원 - 퇴직신청 - 전자결재 (결제요청)
	int insertQuitSalary(Approval app, Search search);
	//사원 - 퇴직금산정  - 사원 검색
	ArrayList<HashMap<String, Object>> selectQuitMemberList(Search search);
	//급여임력 - 퇴직금 산정 - 마감
	int updateQuitEnd(HashMap<String, Object> sList);
	//급여입력 - 퇴직사원 관리 - 검색결과 
	ArrayList<HashMap<String, Object>> selectRetirementMember(Search search);
	//급여입력 - 퇴직금 산정 - 계산하기
	HashMap<String, Object> selectMemberQuitBasic(int mid);
	//급여입력 - 퇴직금 산정 - 신고내역
	ArrayList<HashMap<String, Object>> selectRetirementMemberInfo(Search search);
	
	
	
}
 