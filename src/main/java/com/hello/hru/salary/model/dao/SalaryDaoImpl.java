package com.hello.hru.salary.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.common.model.vo.Search;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.SearchMember;
import com.hello.hru.salary.model.vo.AccountStandard;
import com.hello.hru.salary.model.vo.MemberBasicInfo;
import com.hello.hru.salary.model.vo.MonthSalary;
import com.hello.hru.salary.model.vo.MonthSalaryDetail;
import com.hello.hru.salary.model.vo.SalaryStatus;
import com.hello.hru.salary.model.vo.SaveSalary;
import com.hello.hru.salary.model.vo.Standard;
import com.hello.hru.salary.model.vo.StandardInformation;
import com.hello.hru.salary.model.vo.TaxBasic;
import com.hello.hru.system.model.vo.Team;

@Repository
public class SalaryDaoImpl implements SalaryDao{
	//급여기준
	@Override
	public ArrayList<Standard> selectSalaryStandard(SqlSessionTemplate sqlSession) {
		//System.out.println((ArrayList) sqlSession.selectList("Salary.selectSalaryStandard"));
		return (ArrayList) sqlSession.selectList("Salary.selectSalaryStandard");
	}
	//근태기준
	@Override
	public ArrayList<Standard> selectAttendanceStandard(SqlSessionTemplate sqlSession) {
		//System.out.println((ArrayList) sqlSession.selectList("Salary.selectAttendanceStandard"));
		return (ArrayList) sqlSession.selectList("Salary.selectAttendanceStandard");
	}
	//기본급 , 상여금
	@Override
	public ArrayList<AccountStandard> selectAccountStandard(SqlSessionTemplate sqlSession) {
		//System.out.println((ArrayList) sqlSession.selectList("Salary.selectAccountStandard"));
		return (ArrayList) sqlSession.selectList("Salary.selectAccountStandard");
	}
	//비과세 항목
	@Override
	public ArrayList<AccountStandard> selectNoneAccountStandard(SqlSessionTemplate sqlSession) {
		//System.out.println((ArrayList) sqlSession.selectList("Salary.selectNoneAccountStandard"));
		return (ArrayList) sqlSession.selectList("Salary.selectNoneAccountStandard");
	}
	//연장근무수당 항목
	@Override
	public ArrayList<AccountStandard> selectExtensionAccountStandard(SqlSessionTemplate sqlSession) {
		//System.out.println((ArrayList) sqlSession.selectList("Salary.selectExtensionAccountStandard"));
		return (ArrayList) sqlSession.selectList("Salary.selectExtensionAccountStandard");
	}
	//4대보험 요율
	@Override
	public ArrayList<AccountStandard> selectInsuranceAccountStandard(SqlSessionTemplate sqlSession) {
		//System.out.println((ArrayList) sqlSession.selectList("Salary.selectInsuranceAccountStandard"));
		return (ArrayList) sqlSession.selectList("Salary.selectInsuranceAccountStandard");
	}
	//급여/근태기준 업데이트
	@Override
	public int updateStandardInfofirst(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfofirst", stdinfo);
	}
	@Override
	public int updateStandardInfoSecond(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoSecond", stdinfo);
	}
	@Override
	public int updateStandardInfoThree(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoThree", stdinfo);
	}
	@Override
	public int updateStandardInfoFour(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoFour", stdinfo);
	}
	@Override
	public int updateStandardInfoFive(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoFive", stdinfo);
	}
	@Override
	public int updateStandardInfoSix(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoSix", stdinfo);
	}
	@Override
	public int updateStandardInfoSeven(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoSeven", stdinfo);
	}
	@Override
	public int updateStandardInfoEight(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoEight", stdinfo);
	}
	@Override
	public int updateStandardInfoNine(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoNine", stdinfo);
	}
	@Override
	public int updateStandardInfoTen(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoTen", stdinfo);
	}
	@Override
	public int updateStandardInfoEleven(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoEleven", stdinfo);
	}
	@Override
	public int updateStandardInfoTwelve(SqlSessionTemplate sqlSession, StandardInformation stdinfo) {
		return sqlSession.update("Salary.updateStandardInfoTwelve", stdinfo);
	}
	//기본급/상여금 업데이트
	@Override
	public int updateSalaryInfoOne(SqlSessionTemplate sqlSession, AccountStandard as) {
		return sqlSession.update("Salary.updateSalaryInfoOne" , as);
	}
	@Override
	public int updateSalaryInfoTwo(SqlSessionTemplate sqlSession, AccountStandard as) {
		return sqlSession.update("Salary.updateSalaryInfoTwo" , as);
	}
	//비과세항목 추가
	@Override
	public int insertNonTaxAdd(SqlSessionTemplate sqlSession, AccountStandard accstd) {
		return sqlSession.insert("Salary.insertNonTaxAdd", accstd);
	}
	//비과세항목 업데이트
	@Override
	public int updateNoneTax(SqlSessionTemplate sqlSession, AccountStandard accstd) {
		return sqlSession.update("Salary.updateNoneTax" , accstd);
	}
	//비과세항목 삭제
	@Override
	public int deleteNoneTax(SqlSessionTemplate sqlSession, AccountStandard accstd) {
		return sqlSession.delete("Salary.deleteNoneTax" ,accstd);
	}
	//연장근무수당 추가
	@Override
	public int insertExtentsionAdd(SqlSessionTemplate sqlSession, AccountStandard accstd) {
		return sqlSession.insert("Salary.insertExtentsionAdd" , accstd);
	}
	@Override
	public int updateExtensionUdpate(SqlSessionTemplate sqlSession, AccountStandard accstd) {
		return sqlSession.update("Salary.updateExtension" , accstd);
	}
	@Override
	public int deleteExtentsionDelete(SqlSessionTemplate sqlSession, AccountStandard accstd) {
		return sqlSession.delete("Salary.deleteExtentsionDelete" ,accstd);
	}
	@Override
	public int updateFourInsurance(SqlSessionTemplate sqlSession, AccountStandard as) {
		return sqlSession.update("Salary.updateFourInsurance" , as);
	}
	//급여정보-급여정보-급여기본페이지 이동
	@Override
	public ArrayList<Team> selectTeamList(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Salary.selectTeamList");
	}
	//급여정보-급여정보-검색결과
	@Override
	public ArrayList<SearchMember> searchMember(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList) sqlSession.selectList("Salary.searchMemberList" , search);
	}
	//급여정보-급여정보-사원검색결과
	@Override
	public HashMap<String, Object> selectMemberBasicInfo(int mid, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Salary.selectMemberBasicInfo", mid);
	}
	//급여정보-급여정보-사원가족정보검색
	@Override
	public ArrayList<HashMap<String, Object>> selectMemberFailyInfo(SqlSessionTemplate sqlSession, int mid) {
		return (ArrayList) sqlSession.selectList("Salary.selectMemberFailyInfo" , mid);
	}
	@Override
	public HashMap<String, Object> selectMemberSalaryInfo(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Salary.selectMemberSalaryInfo" , mid);
	}
	//급여정보-급여정보-사원급여기본정보 -1
	@Override
	public int updateMemberBasicInfoOne(SqlSessionTemplate sqlSession, MemberBasicInfo mbInfo) {
		return sqlSession.update("Salary.updateMemberBasicInfoOne" , mbInfo);
	}
	//급여정보-급여정보-사원급여기본정보 -2
	@Override
	public int updateMemberBasicInfoTwo(SqlSessionTemplate sqlSession, MemberBasicInfo mbInfo) {
		return sqlSession.update("Salary.updateMemberBasicInfoTwo" , mbInfo);
	}
	//급여정보-급여정보-사원급여기본정보 -3
	@Override
	public int updateMemberBasicInfoThree(SqlSessionTemplate sqlSession, MemberBasicInfo mbInfo) {
		return sqlSession.update("Salary.updateMemberBasicInfoThree" , mbInfo);
	}
	
	
	//<급여입력 시작>
	//급여정보 - 급여입력 - 팀 검색 - 1 - 사원 MID목록 가져오기
	//조건 : 검색한 팀명 , 검색한 날짜에 재직중이였던 사원이어야함
	@Override
	public ArrayList<Member> selectsearchMidList(Search search, SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Salary.selectsearchMidList",search);
	}
	//급여정보-급여입력-팀 검색 - 2 -팀 월근태 현황 가져오기
	@Override
	public ArrayList<MonthAttendance> selectMonthlyList(Search search, SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Salary.selectMonthAttendanceList" ,search);
	}
	//급여정보-급여입력-팀 검색 - 3 - 전체 급여기준 가져오기
	@Override
	public ArrayList<AccountStandard> selectAllAccountStandard(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Salary.selectAllAccountStandard");
	}
	//급여정보-급여입력-회원 월 급여 정보 확인 - 4 - 
	@Override
	public MonthSalary selectMonthSalary(SqlSessionTemplate sqlSession, Search search) {
		return sqlSession.selectOne("Salary.selectMonthSalary" , search);
	}
	//급여정보-급여입력-회원 월급여지급 테이블 인서트 - 5 -
	@Override
	public int insertMonthSalary(SqlSessionTemplate sqlSession, MonthSalary ms) {
		return sqlSession.insert("Salary.insertMonthSalary", ms);
	}
	//급여정보 - 급여입력 - 회원 상세월급여지급 테이블 인서트 - 6 - 
	@Override
	public int insertMonthSalaryDetail(SqlSessionTemplate sqlSession, MonthSalaryDetail msd) {
		return sqlSession.insert("Salary.insertMonthSalaryDetail" , msd);
	}
	//급여정보 - 급여입력 - 회원 월급 , 부양가족 , 자녀수에 따른 간이세액표 금액 
	@Override
	public int selectTaxBasic(SqlSessionTemplate sqlSession, TaxBasic tbasic) {
		return sqlSession.selectOne("Salary.selectTaxBasic" , tbasic);
	}
	//급여정보 - 급여입력 - 검색한 부서의 사원 명단 
	@Override
	public ArrayList<SearchMember> searchMonthSalaryTeam(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList) sqlSession.selectList("Salary.searchMonthSalaryTeam" , search);
	}
	//급여정보-급여입력- 사원 지급/공제내역 조회
	@Override
	public ArrayList<HashMap<String, Object>> selectMemberMonthSalary(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList) sqlSession.selectList("Salary.selectMemberMonthSalary" , search);
	}
	@Override
	public ArrayList<HashMap<String, Integer>> selectMemberMonthSalaryTwo(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList) sqlSession.selectList("Salary.selectMemberMonthSalaryTwo" , search);
	}
	//급여정보-급여입력- 사원 지급/공제내역 업데이트 -1-
	@Override
	public int updateMemberMonthSalary(SqlSessionTemplate sqlSession, MonthSalary ms) {
		return sqlSession.update("Salary.updateMemberMonthSalary" , ms);
	}
	
	
	//급여정보-급여입력- 사원 지급/공제내역 업데이트 -2-
	@Override
	public int updateMemberMonthSalaryDetail(SqlSessionTemplate sqlSession, SaveSalary save) {
		return sqlSession.update("Salary.updateMemberMonthSalaryDetailOne" , save);
	}
	//급여정보-급여입력- 사원 지급/공제내역 업데이트 -3-
	@Override
	public int updateMemberMonthSalaryDetailTwo(SqlSessionTemplate sqlSession, SaveSalary save) {
		return sqlSession.update("Salary.updateMemberMonthSalaryDetailTwo" , save);
	}
	//급여정보-급여입력- 팀 지급/공제내역 조회
	@Override
	public ArrayList<HashMap<String, Object>> searchTeamSalary(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList) sqlSession.selectList("Salary.searchTeamSalary" , search);
	}
	//급여관리-급여입력 - 팀 지급/공제내역 월급여 아이디 리스트 검색
	@Override
	public ArrayList<Integer> selectSalaryIdList(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList)sqlSession.selectList("Salary.selectSalaryIdList",search);
	}
	//급여관리-급여입력 - 팀 지급/공제내역 전자결재 월 급여 업데이트
	@Override
	public int updateMonthSalaryApproval(SqlSessionTemplate sqlSession, MonthSalary ms) {
		return sqlSession.update("Salary.updateMonthSalaryApproval" , ms);
	}
	//급여관리-급여지급현황-사원 검색
	@Override
	public ArrayList<SearchMember> selectMonthlyWageMember(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList)sqlSession.selectList("Salary.selectMonthlyWageMember" , search);
	}
	//급여관리-급여지급현황-사원 년도 지급현황 
	@Override
	public ArrayList<SalaryStatus> selectMembeYearSalary(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList) sqlSession.selectList("Salary.selectMemberYearSalary", search);
	}
	//급여관리-급여지급현황-사원 검색 리스트 
	@Override
	public ArrayList<HashMap<String, Object>> searchMonthEmailSalary(Search search, SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Salary.searchMonthEmailSalary" ,search);
	}
	@Override
	public ArrayList<HashMap<String, Object>> selectMonthSalary(Search search, SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Salary.selectMonthSalaryEmail" , search);
	}
	//사원 - 셀프서비스 - 퇴직신청 (전자결재 - 퇴직금)
	@Override
	public int insertQuitMember(SqlSessionTemplate sqlSession, HashMap<String, Object> mInfo) {
		return sqlSession.insert("Salary.insertQuitMember" , mInfo);
	}
	//사원 - 셀프서비스 - 퇴직신청 (전자결재 - 퇴직금)
	@Override
	public int insertQuitHistoryMember(SqlSessionTemplate sqlSession, HashMap<String, Object> mInfo) {
		return sqlSession.insert("Salary.insertQuitHistoryMember" , mInfo);
	}
	//사원 - 퇴직금산정  - 사원 검색
	@Override
	public ArrayList<HashMap<String, Object>> selectQuitMemberList(Search search, SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Salary.selectQuitMemberList" , search);
	}
	//급여임력 - 퇴직금 산정 - 마감 -1
	@Override
	public int updateQuitEnd(SqlSessionTemplate sqlSession, HashMap<String, Object> sList) {
		return sqlSession.update("Salary.updateQuitEnd" , sList);
	}
	//급여임력 - 퇴직금 산정 - 마감 -2
	@Override
	public int updateQuitEndDetail(SqlSessionTemplate sqlSession, HashMap<String, Object> sList) {
		return sqlSession.update("Salary.updateQuitDetailEnd" , sList);
	}
	//급여입력 - 퇴직사원 관리 - 검색결과
	@Override
	public ArrayList<HashMap<String, Object>> selectRetirementMember(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList) sqlSession.selectList("Salary.selectRetirementMember" , search);
	}
	//급여입력 - 퇴직금 정산 - 계산하기
	@Override
	public HashMap<String, Object> selectMemberQuitBasic(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Salary.selectMemberQuitBasic" , mid);
	}
	//급여입력 - 퇴직정산 - 마감  -3 
	@Override
	public int updateMemberQuitInfo(SqlSessionTemplate sqlSession, HashMap<String, Object> sList) {
		return sqlSession.update("Salary.updateMemberQuitInfo" , sList);
	}
	//퇴직사원 관리 - 신고내역
	@Override
	public ArrayList<HashMap<String, Object>> selectRetirementMemberInfo(SqlSessionTemplate sqlSession, Search search) {
		return (ArrayList)sqlSession.selectList("Salary.selectRetirementMemberInfo" , search);
	}
	
	
	
	
	
	
}
