package com.hello.hru.salary.model.vo;

/**
 * @author Lee
 *
 */
public class Standard {
	private int stdinfocode;
	private String stdinfoname;
	private String content;
	private int category;
	
	public Standard(int stdinfocode, String stdinfoname, String content, int category) {
		super();
		this.stdinfocode = stdinfocode;
		this.stdinfoname = stdinfoname;
		this.content = content; 
		this.category = category;
	}
	public Standard() {
		super();
	}
	public int getStdinfocode() {
		return stdinfocode;
	}
	public void setStdinfocode(int stdinfocode) {
		this.stdinfocode = stdinfocode;
	}
	public String getStdinfoname() {
		return stdinfoname;
	}
	public void setStdinfoname(String stdinfoname) {
		this.stdinfoname = stdinfoname;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "SalaryStandard [stdinfocode=" + stdinfocode + ", stdinfoname=" + stdinfoname + ", content=" + content
				+ ", category=" + category + "]";
	}
	

}
