package com.hello.hru.salary.model.service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.approval.model.dao.ApprovalDao;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.model.vo.Search;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.SearchMember;
import com.hello.hru.salary.model.dao.SalaryDao;
import com.hello.hru.salary.model.vo.AccountStandard;
import com.hello.hru.salary.model.vo.MemberBasicInfo;
import com.hello.hru.salary.model.vo.MonthSalary;
import com.hello.hru.salary.model.vo.MonthSalaryDetail;
import com.hello.hru.salary.model.vo.SalaryStatus;
import com.hello.hru.salary.model.vo.SaveSalary;
import com.hello.hru.salary.model.vo.Standard;
import com.hello.hru.salary.model.vo.StandardInformation;
import com.hello.hru.salary.model.vo.TaxBasic;
import com.hello.hru.system.model.vo.Team;

 
@Service
public class SalaryServiceImpl implements SalaryService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private SalaryDao sd;
	@Autowired
	private ApprovalDao ad;
	
	
	
	@Override
	public HashMap<String, Object> selectsMap() {
		HashMap<String, Object> sMap = new HashMap<String, Object>();
		//급여기준
		ArrayList<Standard> slistOne = sd.selectSalaryStandard(sqlSession);
		//근태기준
		ArrayList<Standard> slistTwo = sd.selectAttendanceStandard(sqlSession);
		//기본급,상여급기준
		ArrayList<AccountStandard> aslistOne = sd.selectAccountStandard(sqlSession);
		//비과세 항목
		ArrayList<AccountStandard> aslistTwo = sd.selectNoneAccountStandard(sqlSession);
		//연장근무수당 항목
		ArrayList<AccountStandard> aslistThree = sd.selectExtensionAccountStandard(sqlSession);
		//4대보험 요율 항목
		ArrayList<AccountStandard> aslistFour = sd.selectInsuranceAccountStandard(sqlSession);
		
		sMap.put("slistOne", slistOne);
		sMap.put("slistTwo", slistTwo);
		sMap.put("aslistOne", aslistOne);
		sMap.put("aslistTwo", aslistTwo);
		sMap.put("aslistThree", aslistThree);
		sMap.put("aslistFour", aslistFour);
		
		return sMap;
	}
	//급여/근태기준 수정
	@Override
	public int updateStandardInfo(StandardInformation stdinfo) {
		int result = 0;
		int resultOne = 0;
		int resultTwo = 0;
		int resultThree = 0;
		int resultFour = 0;
		int resultFive = 0;
		int resultSix = 0;
		int resultSeven = 0;
		int resultEight = 0;
		int resultNine = 0;
		int resultTen = 0;
		int resultEleven = 0;
		int resultTwelve = 0;
		
		resultOne = sd.updateStandardInfofirst(sqlSession , stdinfo);
		
		if(resultOne > 0) {
			resultTwo = sd.updateStandardInfoSecond(sqlSession,stdinfo);
			if(resultTwo > 0) {
				resultThree = sd.updateStandardInfoThree(sqlSession, stdinfo);
				if(resultThree > 0) {
					resultFour = sd.updateStandardInfoFour(sqlSession, stdinfo);
					if(resultFour > 0) {
						resultFive = sd.updateStandardInfoFive(sqlSession, stdinfo);
						if(resultFive >0 ) {
							resultSix = sd.updateStandardInfoSix(sqlSession, stdinfo);
							if(resultSix > 0) {
								resultSeven = sd.updateStandardInfoSeven(sqlSession, stdinfo);
								if(resultSeven > 0) {
									resultEight = sd.updateStandardInfoEight(sqlSession, stdinfo);
									if(resultEight > 0) {
										resultNine = sd.updateStandardInfoNine(sqlSession, stdinfo);
										if(resultNine >0) {
											resultTen = sd.updateStandardInfoTen(sqlSession, stdinfo);
											if(resultTen > 0 ) {
												resultEleven = sd.updateStandardInfoEleven(sqlSession, stdinfo);
												if(resultEleven > 0) {
													resultTwelve = sd.updateStandardInfoTwelve(sqlSession, stdinfo);
													if(resultEleven > 0) {
														result = 1;
													}else {
														result = 0;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return result;
	}
	//급여기준 수정
	@Override
	public int updateSalaryInfo(AccountStandard accstd) {
		int result = 0;
		int resultOne = 0;
		int resultTwo = 0;
		
		for(int i=0 ; i<accstd.getNameASList().size(); i++) {
			if(i != accstd.getNameASList().size()-1) {
				AccountStandard as = new AccountStandard();
				as.setStdpercent(accstd.getNameASList().get(i).getStdpercent());
				as.setCountNumber(i+1);
				
				sd.updateSalaryInfoOne(sqlSession , as);
				resultOne++;
			}else if(i == accstd.getNameASList().size()-1){
				AccountStandard as = new AccountStandard();
				as.setAccountstd(accstd.getNameASList().get(i).getAccountstd());
				as.setCountNumber(i+1);
				
				sd.updateSalaryInfoTwo(sqlSession , as);
				resultTwo++;
			}
		}
		if(resultOne + resultTwo >= 5 ) {
			result = 1;
		}else {
			result = 0;
		}
		return result;
	}
	//비과세 항목 추가
	@Override
	public int insertNonTaxAdd(AccountStandard accstd) {
		int result = 0;
		int resultOne = 0;
		
		resultOne = sd.insertNonTaxAdd(sqlSession , accstd);
		
		if(resultOne > 0) {
			result = 1;
		}else {
			result = 0;
		}
		return result;
	}
	//비과세 항목 수정
	@Override
	public int updateNoneTax(AccountStandard accstd) {
		int result = 0;
		int resultOne = 0;
		
		resultOne = sd.updateNoneTax(sqlSession , accstd);
		
		if(resultOne > 0) {
			result = 1;
		}else {
			result = 0;
		}
		return result;
	}
	//비과세항목 삭제
	@Override
	public int deleteNoneTax(AccountStandard accstd) {
		int result = 0;
		int resultOne = 0;
		
		resultOne = sd.deleteNoneTax(sqlSession , accstd);
		
		if(resultOne>0) {
			result = 1;
		}else {
			result =0;
		}
		return result;
	}
	//연장근무수당 추가 
	@Override
	public int insertExtensionAdd(AccountStandard accstd) {
		int result = 0 ;
		int resultOne = 0;
		
		resultOne = sd.insertExtentsionAdd(sqlSession , accstd);
		
		if(resultOne > 0) {
			result = 1;
		}else {
			result = 0;
		}
		return result;
	}
	//연장근무수당 수정 
	@Override
	public int updateExtensionUpdate(AccountStandard accstd) {
		int result = 0;
		int resultOne = 0;
		
		resultOne = sd.updateExtensionUdpate(sqlSession , accstd);
		
		if(resultOne >0) {
			result = 1;
		}else {
			result =0;
		}
		return result;
	}
	//연장근무수당 삭제
	@Override
	public int deleteExtensionDelete(AccountStandard accstd) {
		int result = 0;
		int resultOne = 0;
		
		resultOne = sd.deleteExtentsionDelete(sqlSession , accstd);
		if(resultOne > 0 ) {
			result=1;
		}else {
			result=0;
		}
		
		return result;
	}
	//4대보험 정보 업데이트
	@Override
	public int updateFourInsurance(AccountStandard accstd) {
		int result = 0;
		int resultOne = 0;
		
		for(int i=0 ; i<accstd.getNameASList().size(); i++) {
				AccountStandard as = new AccountStandard();
				as.setStdpercent(accstd.getNameASList().get(i).getStdpercent());
				as.setCountNumber(i+13);
				sd.updateFourInsurance(sqlSession , as);
				resultOne++;
		}
		if(resultOne  >= 8 ) {
			result = 1;
		}else {
			result = 0;
		}
		return result;
	}
	//급여정보-급여정보-급여기본정보페이지이동
	@Override
	public ArrayList<Team> selectTeamList() {
		
		ArrayList<Team> tList = sd.selectTeamList(sqlSession);
		
		return tList;
	}
	//급여정보-급여정보-검색결과
	@Override
	public ArrayList<SearchMember> searchMember(Search search) {
		
		ArrayList<SearchMember> mList = sd.searchMember(sqlSession , search);
		
		return mList;
	}
	//급여정보-급여정보-사원기본정보 검색
	@Override
	public HashMap<String, Object> selectMemberBasicInfo(int mid) {
		
		HashMap<String, Object> sMap = sd.selectMemberBasicInfo(mid , sqlSession);
		
		return sMap;
	}
	//급여정보-급여정보-사원가족정보 검색
	@Override
	public ArrayList<HashMap<String, Object>> selectMemberFailyInfo(int mid) {
		
		ArrayList<HashMap<String,Object>> fList = sd.selectMemberFailyInfo(sqlSession , mid);
		
		return fList;
	}
	//급여정보-급여정보-사원급여정보검색
	@Override
	public HashMap<String, Object> selectMemberSalaryInfo(int mid) {
		
		HashMap<String, Object> sMap = sd.selectMemberSalaryInfo(sqlSession , mid);
		
		return sMap;
	}
	//급여정보-급여정보-사원급여정보업데이트
	@Override
	public int updateMemberBasicInfo(MemberBasicInfo mbInfo) {
		int result = 0;
		int resultOne = sd.updateMemberBasicInfoOne(sqlSession , mbInfo);
		if(resultOne > 0) {
			int resultTwo = sd.updateMemberBasicInfoTwo(sqlSession , mbInfo);
			if(resultTwo > 0) {
				int resultThree = sd.updateMemberBasicInfoThree(sqlSession, mbInfo);
				if(resultThree >0) {
					result = 1;
				}
			}else {
				result = 0;
			}
		}else {
			result = 0;
		}
		
		return result;
	}
	//급여관리-급여입력-사원 검색 (월급여테이블 , 지금상세테이블 데이터 인서트 , 최종 검색사원 리턴)
	/* searchResult에서 해줘야할 기능
	1. 월급여테이블 , 지급상세테이블 데이터 인서트
	   - 월급여테이블을 Insert하기 위해서는 해당 부서의 팀원들의 memberId가 필요 -> 월 근태 조회해야함 단 승인 여부가 y인 친구들
	   - 해당 사원들의 월근태현황을 모두 가져온다.
	   - 급여기준 테이블의 급여기준을 가져온다.
	   - 월근태현황과 급여기준을 통해 월급여테이블에 데이터를 인서트한다.
	   - 인서트를 완료하면 해당 월급여테이블의 지급상세테이블에 데이터들을 인서트한다.
	   - 이와 같은 로직을 부서테이블을 반복한다.   -> 완료 / 2020-01-31
	2. 사원 목록 조회
	   - 인서트가 완료되면 해당 검색 조건에 맞는 사원들을 조회한다.
	*/
	@Override
	public ArrayList<SearchMember> searchInsertSalary(Search search) {
		//리턴할 검색 부서 명단 초기화
		ArrayList<SearchMember> mList = null;
		//검색한 조건 (부서)의 팀원 사번 가져오기
		ArrayList<Member> midList = sd.selectsearchMidList(search , sqlSession);
		//System.out.println("midList: " + midList);
		//System.out.println("search :" + search);
		//팀의 사번을 담은 midList가 null이 아니라면 (검색에 일치하는 팀원이 한명이라도 있으면)
		//검색조건 : 월 근태가 있고 , 월근태 마감이 'Y' , 검색 날짜와 검색 팀에 속한 사원들의 MIDLIST
		if(midList != null) {
			//'월 근태현황 초기화'
			ArrayList<MonthAttendance> maList = null; 
			//전체 급여기준을 가져온다.
			ArrayList<AccountStandard> as = sd.selectAllAccountStandard(sqlSession);
			System.out.println("급여기준:" + as);
			
			for(int i=0; i<midList.size(); i++) {
				int mid = midList.get(i).getMid();
				//한 사원의 '월 근태 현황' SELECT
				Search search2 = new Search();
				int searchDay = search.getSearchTeamCode();
				
				search2.setMid(mid);
				search2.setSearchTeamCode(searchDay);
				
				
				maList = sd.selectMonthlyList(search2, sqlSession);
				//System.out.println(mid +"사원의 월 근태 현황 : " + maList);
				//한 사원의 '가족정보' SELECT
				ArrayList<HashMap<String,Object>> fList = sd.selectMemberFailyInfo(sqlSession , mid);
				//System.out.println( "가족정보 리스트 크기 : " + fList.size());
				//System.out.println(mid + "사원의 가족정보 : " + fList);
				
				int FamilyCount = 0;
				int KidsCount = 0;
				int resultFamilyCount = 0;
				//부양가족 , 20세미만 자녀 Count 
				//가족 목록이 한명 이상 존재할 떄
				if(fList.size()!=0) {
					for(int a=0; a<fList.size(); a++) {
						int FamilyAge = 0;
						String myYear = "";
						String ssn2 = ((String)fList.get(a).get("fSsn")).substring(7,8);
						String year ="";
						String month ="";
						String day ="";
						
						SimpleDateFormat formatY = new SimpleDateFormat("yyyy" , Locale.KOREA);
						SimpleDateFormat formatM = new SimpleDateFormat("MM" , Locale.KOREA);
						SimpleDateFormat formatD = new SimpleDateFormat("dd" , Locale.KOREA);
						year = formatY.format(new java.util.Date());
						month = formatM.format(new java.util.Date());
						day = formatD.format(new java.util.Date());
						
						if(ssn2.equals("1") || ssn2.equals("2")) {
							myYear ="19"+ ((String)fList.get(a).get("fSsn")).substring(0,2);
						}else {
							myYear = "20"+ ((String)fList.get(a).get("fSsn")).substring(0,2);
						}
						
						if(Integer.parseInt(month) > Integer.parseInt( ((String)fList.get(a).get("fSsn")).substring(2,4) )   ) {
							FamilyAge = Integer.parseInt(year) - Integer.parseInt(myYear);
						}else if(Integer.parseInt(month) == Integer.parseInt( ((String)fList.get(a).get("fSsn")).substring(2,4) )  ) {
							if(Integer.parseInt(day) > Integer.parseInt( ((String)fList.get(a).get("fSsn")).substring(4,6)  )) {
								FamilyAge = Integer.parseInt(year) - Integer.parseInt(myYear);
							}else {
								FamilyAge = Integer.parseInt(year) - (Integer.parseInt(myYear)+1);
							}
						}else {
							FamilyAge = Integer.parseInt(year) - (Integer.parseInt(myYear) +1);
						}
						if(FamilyAge < 20 ) {
							KidsCount++;
						}
						FamilyCount++;
					}
					//자기자신도 포함
					FamilyCount += 1;
				//가족 목록이 한명도 없을 떄 => 자기 자신은 포함해야함
				}else {
					FamilyCount += 1;
				}
				resultFamilyCount = FamilyCount + KidsCount;
				//System.out.println("F : " + resultFamilyCount);
				//한 사원의 '급여정보' SELECT (은행 , 급여액 , 등등 )
				HashMap<String, Object> mSalary = sd.selectMemberSalaryInfo(sqlSession, mid);
				//System.out.println(mid + "사원의 급여정보 : " + mSalary);
				
				search.setMid(mid);
				//사원의 '월 급여' SELECT
				MonthSalary salary = sd.selectMonthSalary(sqlSession ,search);
				int plusSalaryCategory = 0;
				if(as.get(4).getAccountstd().equals("매달지급")) {
					plusSalaryCategory = 12;
				}else if(as.get(4).getAccountstd().equals("짝수달지급") || as.get(4).getAccountstd().equals("홀수달지급")) {
					plusSalaryCategory = 6;
				}
				
				//사원별 매달 기본급 ( 
				double basicSalary = (Double.parseDouble((String) mSalary.get("amount")) / (as.get(0).getStdpercent() + as.get(1).getStdpercent()) * 100);
				//사원별 매달 상여급
				double plusSalary = (basicSalary * (as.get(1).getStdpercent()/ 100 )) / plusSalaryCategory;
				//사원별 월 급여액 (비과세 및 학자금 제외 ) 
				double MonthSalary = basicSalary + plusSalary;
				//기본급 + 상여급 + 식대
				double Total = basicSalary + plusSalary+ as.get(5).getAccountMoney();
				
				//사원별 통상임금
				double AvgSalary = Total / 209;
				System.out.println( mid + " 사원의통상임금 : " +  AvgSalary);
				//연장수당
				int plusWork =  (int) ((maList.get(0).getWorkMore() * (AvgSalary *((as.get(8).getStdpercent()+100)/100)))/10)*10;
				//야간수당
				int nightWork = (int) ((maList.get(0).getWorkNight() * (AvgSalary * ((as.get(9).getStdpercent())/100)))/10)*10;
				//휴일수당
				int holiyWork = (int) ((maList.get(0).getWorkHolidayOne() * (AvgSalary * ((as.get(10).getStdpercent()+100)/100)))/10)*10
						+(int) ((maList.get(0).getWorkHolidayTwo() * (AvgSalary * ((as.get(11).getStdpercent()+150)/100)))/10)*10 ;
				double middelSalary = 0;
				//급여합계 ( 기본급 + 상여금 + 연장수당 + 야간수당 + 휴일수당 )
				middelSalary = basicSalary + plusSalary + plusWork + nightWork + holiyWork;
				
				//System.out.println("급여합계 : " + middelSalary);
				//한 사원의 월급여가 NULL이면 (검색이 이루어지지 않았다. == 월급여 ,상세급여 테이블이 생성되지 않았다.) 
				//전체급여기준에 맞춰서 SALARY TABLE과  SALARY_DETAIL TABLE에 INSERT한다.
				if(salary == null) {
					//System.out.println(mid + "회원의 월급여 내역이 없습니다.");
					//한 사원의 지급항목 계
					double giveSalaryTotal = Total
							+ (int) ((maList.get(0).getWorkMore() * (AvgSalary *((as.get(8).getStdpercent()+100)/100)))/10)*10 
							+ (int) ((maList.get(0).getWorkNight() * (AvgSalary * ((as.get(9).getStdpercent())/100)))/10)*10
							+ (int) ((maList.get(0).getWorkHolidayOne() * (AvgSalary * ((as.get(10).getStdpercent()+100)/100)))/10)*10
							+(int) ((maList.get(0).getWorkHolidayTwo() * (AvgSalary * ((as.get(11).getStdpercent()+150)/100)))/10)*10;
							
					//System.out.println("지급항목 계 : " + giveSalaryTotal);
					
					//국민연금기준 상한가와 하한가 존재 
					double nationalPerson = 0;
					if(middelSalary <= 4680000  && middelSalary >= 310000) {
						nationalPerson = (int)((middelSalary * (as.get(13).getStdpercent()/100))/10)*10;
					}else if(basicSalary > 4680000) {
						nationalPerson = 218700;
					}else if(basicSalary < 310000) {
						nationalPerson = 13950;
					}
					// 소득세 , 지방소득세 추출 로직 시작
					int TaxBasic = 0;
					//월급여 , 부양가족수 , 20세 미만 자녀수
					TaxBasic tbasic = new TaxBasic();
					tbasic.setMonthSalary(MonthSalary);
					tbasic.setFamilyCount(FamilyCount);
					tbasic.setKidsCount(KidsCount);
					tbasic.setResultFamilyCount(resultFamilyCount);
					
					TaxBasic = sd.selectTaxBasic(sqlSession, tbasic);
					// 소득세 , 지방소득세 추출 로직 종료
					
					//한 사원의 공제항목 계
					double DeductionTotal = 
							(int)((middelSalary * (as.get(15).getStdpercent()/100))/10)*10 // 건강보험요율
							+ (int)(((middelSalary * (as.get(15).getStdpercent() / 100)) * (as.get(16).getStdpercent() / 100))/10)*10	 //장기요양보험요율
							+ (int)((middelSalary * (as.get(18).getStdpercent()/100))/10)*10 //고용보험요율
							+ nationalPerson									// 국민연금
							+ TaxBasic											// 소득세
							+ (TaxBasic / 100) * 10;							// 지방소득세
					
					//System.out.println("공제항목 계 : " + DeductionTotal);
					
					int resultSalary = (int) (giveSalaryTotal - DeductionTotal);
					
					resultSalary = (resultSalary/10) * 10;
					
					//System.out.println("실지급 금액 : " + resultSalary);
					
					
					//현재 선택한 달의 10일 자 날짜 ( ORACLE에서 한달을 더해줘야함 )
					String yearDate = search.getSearchDate().substring(0,4);
					String monthDate = search.getSearchDate().substring(4);
					String payDate = yearDate + "-" + monthDate + "-" + "10";
					//String -> Date 
					Date paymentDate = Date.valueOf(payDate);
					//넘겨줄 월 급여 객체 생성
					MonthSalary ms = new MonthSalary();
					
					//월급여지급아이디 : 시퀀스
					//승인여부 : DEFUALT
					//APPROVAL_YN : DEFAULT
					//APPROVAL_ID : DEFAULT
					ms.setMid(search.getMid());				//해당사원아이디 (int) 
					ms.setStdDate(search.getSearchDate());	//기준년월 ( string )
					ms.setSalaryDate(paymentDate);			//지급일 ( Date )
					ms.setSalaryMoney(resultSalary);		//실지급액
					
					//System.out.println("넘겨야할 ms객체 : " + ms);
					
					int MonthResult = sd.insertMonthSalary(sqlSession , ms);
					
					if(MonthResult > 0) {
						//급여명세서 지급항목 14가지
						
						int count = 0; 
						for(int j=0; j < 14; j++) {
							MonthSalaryDetail msd = new MonthSalaryDetail();
							switch(j) {
							case 0 :
								//기본급
								msd.setAccountCode(1);
								msd.setSdetailMoney(((int)basicSalary /10) * 10 );
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultOne = sd.insertMonthSalaryDetail(sqlSession , msd);
								if(resultOne >0) {
									count++;
								}
								break;
							case 1 :
								//정기상여 (매달)
								msd.setAccountCode(2);
								msd.setSdetailMoney(((int) plusSalary / 10) * 10);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultTwo = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultTwo > 0) {
									count++;
								}
								break;
							case 2 :
								//시점상여 ( 설날 ,추석 ) 
								//pass -> 0원 입력
								msd.setAccountCode(3);
								msd.setSdetailMoney(0);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultThree = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultThree > 0) {
									count++;
								}
								break;
							case 3 :
								//차량보조
								//추가 기입사항 -> 처음은 0원 입력
								msd.setAccountCode(7);
								msd.setSdetailMoney(0);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultFour = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultFour > 0) {
									count++;
								}
								break;
							case 4 :
								//식대
								//전원 100,000원 지급
								msd.setAccountCode(6);
								msd.setSdetailMoney(100000);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultFive = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultFive > 0) {
									count++;
								}
								break;
								
							case 5 :
								//잔업수당 (연장수당)
								msd.setAccountCode(9);
								msd.setSdetailMoney(  (int) ((maList.get(0).getWorkMore() * (AvgSalary *((as.get(8).getStdpercent()+100)/100)))/10)*10  );
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultSix = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultSix > 0) {
									count++;
								}
								break;
							case 6 :
								//야간수당
								msd.setAccountCode(10);
								msd.setSdetailMoney(  (int) ((maList.get(0).getWorkNight() * (AvgSalary * ((as.get(9).getStdpercent())/100)))/10)*10  );
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultSeven = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultSeven >0) {
									count++;
								}
								break;
							case 7 : 
								//휴일근무수당
								msd.setAccountCode(11);
								msd.setSdetailMoney
									(	
										(int) ((maList.get(0).getWorkHolidayOne() * (AvgSalary * ((as.get(10).getStdpercent()+100)/100)))/10)*10
										+(int) ((maList.get(0).getWorkHolidayTwo() * (AvgSalary * ((as.get(11).getStdpercent()+150)/100)))/10)*10 
									);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultEight = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultEight > 0) {
									count++;
								}
								break;
							case 8 :
								//국민연금
								msd.setAccountCode(14);
								msd.setSdetailMoney
									(
										(int)((middelSalary * (as.get(13).getStdpercent()/100))/10)*10
									);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultNine = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultNine > 0) {
									count++;
								}
								break;
							case 9 : 
								//건강보험
								msd.setAccountCode(16);
								msd.setSdetailMoney
									(
										(int)((middelSalary * (as.get(15).getStdpercent()/100))/10)*10
									);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultTen = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultTen > 0) {
									count++;
								}
								break;
							case 10 : 
								//고용보험
								msd.setAccountCode(19);
								msd.setSdetailMoney
									(
										(int)((middelSalary * (as.get(18).getStdpercent()/100))/10)*10
									);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultEleven = sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultEleven > 0) {
									count++;
								}
								break;
							case 11 : 
								//장기요양보험
								msd.setAccountCode(17);
								msd.setSdetailMoney
									(
											(int)(((middelSalary * (as.get(15).getStdpercent() / 100)) * (as.get(16).getStdpercent() / 100))/10)*10	
									);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultTwelve= sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultTwelve > 0) {
									count++;
								}
								break;
							case 12 : 
								//소득세
								msd.setAccountCode(21);
								msd.setSdetailMoney(TaxBasic);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultThirteen= sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultThirteen > 0) {
									count++;
								}
								
								break;
							case 13 : 
								//지방소득세
								msd.setAccountCode(22);
								msd.setSdetailMoney((TaxBasic / 100) * 10);
								msd.setMid(mid);
								msd.setSearchDate(search.getSearchDate());
								int resultFourteen= sd.insertMonthSalaryDetail(sqlSession, msd);
								if(resultFourteen > 0) {
									count++;
								}
								break;
							}
						}
						
					}//if(MonthResult > 0)일때 종료  -> salary테이블에 인서트가 완료되었을 때 ,
				}
			}// for문 종료 ( 사원별 for문 로직 종료 ) 
			mList = sd.searchMonthSalaryTeam(sqlSession , search);
			return mList;
		}else {
			//검색조건에 부합하지 않으므로 초기화된 null == mList 리턴
			return mList;
		}
	}
	//급여관리-급여입력-사원 지급/공제내역 조회
	@Override
	public ArrayList<HashMap<String, Object>> selectMemberMonthSalary(Search search) {
		ArrayList<HashMap<String, Object>> msList = sd.selectMemberMonthSalary(sqlSession , search);
		return msList;
	}
	//급여관리-급여입력-사원 지급/공제내역 조회 -2- 
	@Override
	public ArrayList<HashMap<String, Integer>> selectMemberMonthSalaryTwo(Search search) {
		ArrayList<HashMap<String, Integer>> atList = sd.selectMemberMonthSalaryTwo(sqlSession , search);
		return atList;
	}
	//급여관리-급여입력-사원 지급/공제내역 업데이트	
	@Override
	public int updateMemberMonthSalary(SaveSalary save) {
		
		//SALARY 테이블 실 지급액 UPDATE 
		MonthSalary ms = new MonthSalary();
		
		ms.setMid(save.getMid());
		ms.setSalaryMoney(save.getTotal());
		ms.setStdDate(save.getSearchDay());
		
		int result =0 ;
		
		int resultOne = sd.updateMemberMonthSalary(sqlSession , ms);
		//SALARY_DETAIL 테이블 비과세 목록 UPDATE
		if(resultOne > 0) {
			int resultTwo = sd.updateMemberMonthSalaryDetail(sqlSession , save);
			if(resultTwo > 0) {
				int resultThree = sd.updateMemberMonthSalaryDetailTwo(sqlSession , save);
				if(resultThree > 0) {
					result  = 1;
				}else {
					result = 0;
				}
			}
		}
		return result;
	}
	//급여관리-급여입력-팀 지급/공제내역 조회
	@Override
	public ArrayList<HashMap<String, Object>> searchTeamSalary(Search search) {
		ArrayList<HashMap<String, Object>> stList = sd.searchTeamSalary(sqlSession , search);
		return stList;
	}
	//급여관리-급여입력-팀 -지금/공제내역 월지급아이디 리스트 조회
	@Override
	public ArrayList<Integer> selectSalaryIdList(Search search) {
		ArrayList<Integer> salaryIdList = sd.selectSalaryIdList(sqlSession , search);
		return salaryIdList;
	}
	//급여관리-급여입력-팀 지급/공제내역 전자결재
	@Override
	public int insertMonthSalary(Approval app, Attachment att, ArrayList<Integer> salaryId) {
		int result = 0;
		
		MonthSalary ms = new MonthSalary();
		
		int result1 = ad.insertApproval(sqlSession, app);
		System.out.println("resultOne : "+result1);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					System.out.println("Service Apphis : " +apphis);
					result12 = ad.insertApprovalHistory(sqlSession, apphis);
					System.out.println(result12);
				}
				ms.setApprovalYN("N"); // n
			} else {
				result12 = 1;
				ms.setApprovalYN("Y"); //결재가 필요없을 경우라면, 카테고리를 3:승인으로 바꿈! =>y 
			}
		}
		//전자결재 아이디 한개로 통합 (팀단위 전자결재)
		ms.setApprovalId(result1);
		
		int count = 0;
		int result2 = 0;
		//월급여 아이디의 숫자만큼 반복 ( update 해줘야함 ) 
		for(int j=0; j<salaryId.size(); j++) {
			
			ms.setSalaryId(salaryId.get(j));
			
			count += sd.updateMonthSalaryApproval(sqlSession , ms);
		}
		System.out.println("count : " + count);
		
		if(count == salaryId.size()) {
			result2 = 1;
		}
		
		//첨부파일 인서트
		//int result3 = 0;
		
		
		if(result12 > 0 && result2>0 ) {
			result = 1;
		}else {
			result = 0;
		}
		
		return result;
	}
	//급여관리-급여지급현황-사원 검색
	@Override
	public ArrayList<SearchMember> selectMonthlyWageMember(Search search) {
		ArrayList<SearchMember> mList = sd.selectMonthlyWageMember(sqlSession , search);
		return mList;
	}
	//급여관리-급여지급현황-년간 사원 급여지급현황
	@Override
	public HashMap<String, ArrayList<SalaryStatus>> selectMemberYearSalary(Search search) {
		
		HashMap<String, ArrayList<SalaryStatus>> myList =new HashMap<>();
		int[] accountCode = {1,2,3,6,7,9,10,11,14,16,17,19,21,22};
		for(int i=0; i<accountCode.length; i++) {
			
			search.setSearchAccountCode(accountCode[i]);
			ArrayList<SalaryStatus> SalaryList = sd.selectMembeYearSalary(sqlSession,search);
			
			//System.out.println("SalayList : " + SalaryList );
			
			myList.put("myList"+i, SalaryList);
			
		}
		
		
		return myList;
	}
	//급여관리-이메일전송-검색결과확인 (사원리스트)
	@Override
	public ArrayList<HashMap<String, Object>> searchMonthEmailSalary(Search search) {
		ArrayList<HashMap<String, Object>> mList = sd.searchMonthEmailSalary(search , sqlSession);
		return mList;
	}
	//급여관리-이메일전송-급여목록확인
	@Override
	public ArrayList<HashMap<String, Object>> selectMonthSalary(Search search) {
		ArrayList<HashMap<String, Object>> sList = sd.selectMonthSalary(search , sqlSession);
		return sList;
	}
	//사원 - 퇴직신청 - 전자결재 ( 결재요청 ) 
	@Override
	public int insertQuitSalary(Approval app, Search search) {
		int result = 0;
		
		Date quitDate =Date.valueOf(search.getSearchDate());
		int mid = search.getMid();
		//전자결재 ID ( APPROVAL_ID)
		int result1 = ad.insertApproval(sqlSession, app);
		HashMap<String, Object> mInfo = new HashMap<String, Object>();
		
		//해당 회원의 MID , APPID , QUITDATE를 담은 HashMap
		mInfo.put("mid", mid);
		mInfo.put("appId",result1);
		mInfo.put("quitDate", quitDate);
		
		System.out.println(result1);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					System.out.println("Service Apphis : " +apphis);
					result12 = ad.insertApprovalHistory(sqlSession, apphis);
					System.out.println(result12);	
				}
				mInfo.put("approvalYN", "N");
			} else {
				result12 = 1;
				mInfo.put("approvalYN", "Y");
			}
		}
		
		int result2 = 0;
		int result3 = 0;
	
		result2 = sd.insertQuitMember(sqlSession , mInfo);
		
		System.out.println("result2 : " + result2);
		
		if(result2 > 0) {
			System.out.println("mInfo : " + mInfo);
			result3 = sd.insertQuitHistoryMember(sqlSession ,mInfo);
			if(result3 > 0) {
				result = 1;
			}else {
				result = 0 ;
			}
		}
		System.out.println("result  : " + result);
		return result;
	}
	//사원 - 퇴직금산정  - 사원 검색
	@Override
	public ArrayList<HashMap<String, Object>> selectQuitMemberList(Search search) {
		ArrayList<HashMap<String, Object>> qList = sd.selectQuitMemberList(search , sqlSession);
		return qList ;
	}
	//급여임력 - 퇴직금 산정 - 마감
	@Override
	public int updateQuitEnd(HashMap<String, Object> sList) {
		int result = 0;
		int result1 = sd.updateQuitEnd(sqlSession , sList);
		
		if(result1 > 0) {
			int result2 = sd.updateQuitEndDetail(sqlSession , sList);
			if(result2 > 0) {
				int result3 = sd.updateMemberQuitInfo(sqlSession , sList);
				if(result3 > 0) {
					result = 1;
				}else {
					result = 0;
				}
			}
		}
		return result;
	}
	//급여입력 - 퇴직사원 관리 - 검색결과 
	@Override
	public ArrayList<HashMap<String, Object>> selectRetirementMember(Search search) {
		ArrayList<HashMap<String, Object>> mList = sd.selectRetirementMember(sqlSession , search);
		return mList;
	}
	//급여입력 - 퇴직금정산 - 계산하기
	@Override
	public HashMap<String, Object> selectMemberQuitBasic(int mid) {
		HashMap<String, Object> qList = sd.selectMemberQuitBasic(sqlSession , mid);
		return qList;
	}
	//퇴직 사원관리 - 신고내역
	@Override
	public ArrayList<HashMap<String, Object>> selectRetirementMemberInfo(Search search) {
		ArrayList<HashMap<String, Object>> mList = sd.selectRetirementMemberInfo(sqlSession , search);
		return mList;
	}
	
	
}
