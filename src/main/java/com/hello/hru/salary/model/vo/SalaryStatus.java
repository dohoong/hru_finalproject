package com.hello.hru.salary.model.vo;

public class SalaryStatus {
	private int salaryId;		//급여 id
	private int sdetailMoney;	//급여액	
	private int accountCode;	//급여기준코드
	private int stdDate;		//검색날짜
	public SalaryStatus(int salaryId, int sdetailMoney, int accountCode, int stdDate) {
		super();
		this.salaryId = salaryId;
		this.sdetailMoney = sdetailMoney;
		this.accountCode = accountCode;
		this.stdDate = stdDate;
	}
	public SalaryStatus() {
		super();
	}
	public int getSalaryId() {
		return salaryId;
	}
	public void setSalaryId(int salaryId) {
		this.salaryId = salaryId;
	}
	public int getSdetailMoney() {
		return sdetailMoney;
	}
	public void setSdetailMoney(int sdetailMoney) {
		this.sdetailMoney = sdetailMoney;
	}
	public int getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(int accountCode) {
		this.accountCode = accountCode;
	}
	public int getStdDate() {
		return stdDate;
	}
	public void setStdDate(int stdDate) {
		this.stdDate = stdDate;
	}
	@Override
	public String toString() {
		return "SalaryStatus [salaryId=" + salaryId + ", sdetailMoney=" + sdetailMoney + ", accountCode=" + accountCode
				+ ", stdDate=" + stdDate + "]";
	}
	
	
	
	
}
