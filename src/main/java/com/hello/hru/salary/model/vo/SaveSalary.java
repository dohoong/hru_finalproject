package com.hello.hru.salary.model.vo;

public class SaveSalary {
	
	private int	mealId;		//식대ID
	private int carId;		//차량유지비ID
	private int mid;		//사원번호
	private int meal;		//식대
	private int car;		//차량유지비
	private int total;		//총합
	private String searchDay;//검색 날짜
	public SaveSalary(int mealId, int carId, int mid, int meal, int car, int total, String searchDay) {
		super();
		this.mealId = mealId;
		this.carId = carId;
		this.mid = mid;
		this.meal = meal;
		this.car = car;
		this.total = total;
		this.searchDay = searchDay;
	}
	public SaveSalary() {
		super();
	}
	public int getMealId() {
		return mealId;
	}
	public void setMealId(int mealId) {
		this.mealId = mealId;
	}
	public int getCarId() {
		return carId;
	}
	public void setCarId(int carId) {
		this.carId = carId;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getMeal() {
		return meal;
	}
	public void setMeal(int meal) {
		this.meal = meal;
	}
	public int getCar() {
		return car;
	}
	public void setCar(int car) {
		this.car = car;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getSearchDay() {
		return searchDay;
	}
	public void setSearchDay(String searchDay) {
		this.searchDay = searchDay;
	}
	@Override
	public String toString() {
		return "SaveSalary [mealId=" + mealId + ", carId=" + carId + ", mid=" + mid + ", meal=" + meal + ", car=" + car
				+ ", total=" + total + ", searchDay=" + searchDay + "]";
	}
	
	
	
	
	
	
}
