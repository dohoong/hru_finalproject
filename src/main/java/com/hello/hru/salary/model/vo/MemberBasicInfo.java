package com.hello.hru.salary.model.vo;

import java.sql.Date;

public class MemberBasicInfo {
	private int mid;
	private String name;
	private Date enterDay;
	private String phone;
	private String email;
	private String bank;
	private String accountNumber;
	private String accountName;
	
	public MemberBasicInfo() {
		super();
	}

	public MemberBasicInfo(int mid, String name, Date enterDay, String phone, String email, String bank,
			String accountNumber, String accountName) {
		super();
		this.mid = mid;
		this.name = name;
		this.enterDay = enterDay;
		this.phone = phone;
		this.email = email;
		this.bank = bank;
		this.accountNumber = accountNumber;
		this.accountName = accountName;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getEnterDay() {
		return enterDay;
	}

	public void setEnterDay(Date enterDay) {
		this.enterDay = enterDay;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Override
	public String toString() {
		return "MemberBaiscInfo [mid=" + mid + ", name=" + name + ", enterDay=" + enterDay + ", phone=" + phone
				+ ", email=" + email + ", bank=" + bank + ", accountNumber=" + accountNumber + ", accountName="
				+ accountName + "]";
	}
	
	
	
}
