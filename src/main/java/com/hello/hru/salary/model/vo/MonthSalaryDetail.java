package com.hello.hru.salary.model.vo;

public class MonthSalaryDetail {
	private int sdetailMoney;	//금액
	private int accountCode;	//지급항목 (급여코드)
	private int salaryId;		//월급여지급 ID (SALARY테이블)
	private int mid;			//MID
	private String searchDate;  //검색년월
	
	
	public MonthSalaryDetail() {
		super();
	}
	public MonthSalaryDetail(int sdetailMoney, int accountCode, int salaryId, int mid, String searchDate) {
		super();
		this.sdetailMoney = sdetailMoney;
		this.accountCode = accountCode;
		this.salaryId = salaryId;
		this.mid = mid;
		this.searchDate = searchDate;
	}
	public int getSdetailMoney() {
		return sdetailMoney;
	}
	public void setSdetailMoney(int sdetailMoney) {
		this.sdetailMoney = sdetailMoney;
	}
	public int getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(int accountCode) {
		this.accountCode = accountCode;
	}
	public int getSalaryId() {
		return salaryId;
	}
	public void setSalaryId(int salaryId) {
		this.salaryId = salaryId;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getSearchDate() {
		return searchDate;
	}
	public void setSearchDate(String searchDate) {
		this.searchDate = searchDate;
	}
	@Override
	public String toString() {
		return "MonthSalaryDetail [sdetailMoney=" + sdetailMoney + ", accountCode=" + accountCode + ", salaryId="
				+ salaryId + ", mid=" + mid + ", searchDate=" + searchDate + "]";
	}
	
	
	
	
	
	
}
