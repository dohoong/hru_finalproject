package com.hello.hru.salary.model.vo;

import java.sql.Date;

public class MonthSalary {
	private int salaryId;		//월급여지급ID
	private String stdDate;		//기준년월
	private Date salaryDate;	//지급일
	private String approvalYN;	//승인여부
	private int approvalId;		//전자결제ID
	private int salaryMoney;	//실지급액
	private int mid;			//사원번호
	
	
	public int getSalaryId() {
		return salaryId;
	}
	public void setSalaryId(int salaryId) {
		this.salaryId = salaryId;
	}
	public String getStdDate() {
		return stdDate;
	}
	public void setStdDate(String stdDate) {
		this.stdDate = stdDate;
	}
	public Date getSalaryDate() {
		return salaryDate;
	}
	public void setSalaryDate(Date salaryDate) {
		this.salaryDate = salaryDate;
	}
	public String getApprovalYN() {
		return approvalYN;
	}
	public void setApprovalYN(String approvalYN) {
		this.approvalYN = approvalYN;
	}
	public int getApprovalId() {
		return approvalId;
	}
	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}
	public int getSalaryMoney() {
		return salaryMoney;
	}
	public void setSalaryMoney(int salaryMoney) {
		this.salaryMoney = salaryMoney;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public MonthSalary(int salaryId, String stdDate, Date salaryDate, String approvalYN, int approvalId, int salaryMoney,
			int mid) {
		super();
		this.salaryId = salaryId;
		this.stdDate = stdDate;
		this.salaryDate = salaryDate;
		this.approvalYN = approvalYN;
		this.approvalId = approvalId;
		this.salaryMoney = salaryMoney;
		this.mid = mid;
	}
	public MonthSalary() {
		super();
	}
	@Override
	public String toString() {
		return "MonthSalary [salaryId=" + salaryId + ", stdDate=" + stdDate + ", salaryDate=" + salaryDate
				+ ", approvalYN=" + approvalYN + ", approvalId=" + approvalId + ", salaryMoney=" + salaryMoney
				+ ", mid=" + mid + "]";
	}
	
	
	
	
	
}
