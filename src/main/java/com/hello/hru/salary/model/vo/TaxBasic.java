package com.hello.hru.salary.model.vo;

public class TaxBasic {
	private double MonthSalary; 	//월급
	private int FamilyCount;	//부양가족수
	private int KidsCount;		//20세 미만 자녀 수 
	private int resultFamilyCount; //최종 공제 가족 수
	public TaxBasic(double monthSalary, int familyCount, int kidsCount, int resultFamilyCount) {
		super();
		MonthSalary = monthSalary;
		FamilyCount = familyCount;
		KidsCount = kidsCount;
		this.resultFamilyCount = resultFamilyCount;
	}
	public double getMonthSalary() {
		return MonthSalary;
	}
	public void setMonthSalary(double monthSalary) {
		MonthSalary = monthSalary;
	}
	public int getFamilyCount() {
		return FamilyCount;
	}
	public void setFamilyCount(int familyCount) {
		FamilyCount = familyCount;
	}
	public int getKidsCount() {
		return KidsCount;
	}
	public void setKidsCount(int kidsCount) {
		KidsCount = kidsCount;
	}
	public int getResultFamilyCount() {
		return resultFamilyCount;
	}
	public void setResultFamilyCount(int resultFamilyCount) {
		this.resultFamilyCount = resultFamilyCount;
	}
	public TaxBasic() {
		super();
	}
	@Override
	public String toString() {
		return "TaxBasic [MonthSalary=" + MonthSalary + ", FamilyCount=" + FamilyCount + ", KidsCount=" + KidsCount
				+ ", resultFamilyCount=" + resultFamilyCount + "]";
	}
	
	
	
}
