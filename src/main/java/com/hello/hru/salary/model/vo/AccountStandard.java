package com.hello.hru.salary.model.vo;

import java.util.ArrayList;

public class AccountStandard {
	private int accountCode;
	private String accountName;
	private int category;
	private double stdpercent;
	private int accountMoney;
	private String accountstd;
	private int countNumber;
	private ArrayList<AccountStandard> nameASList;
	
	public AccountStandard() {
		super();
	}

	public AccountStandard(int accountCode, String accountName, int category, double stdpercent, int accountMoney,
			String accountstd, int countNumber, ArrayList<AccountStandard> nameASList) {
		super();
		this.accountCode = accountCode;
		this.accountName = accountName;
		this.category = category;
		this.stdpercent = stdpercent;
		this.accountMoney = accountMoney;
		this.accountstd = accountstd;
		this.countNumber = countNumber;
		this.nameASList = nameASList;
	}


	public int getAccountCode() {
		return accountCode;
	}


	public void setAccountCode(int accountCode) {
		this.accountCode = accountCode;
	}


	public String getAccountName() {
		return accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public int getCategory() {
		return category;
	}


	public void setCategory(int category) {
		this.category = category;
	}


	public double getStdpercent() {
		return stdpercent;
	}


	public void setStdpercent(double stdpercent) {
		this.stdpercent = stdpercent;
	}


	public int getAccountMoney() {
		return accountMoney;
	}


	public void setAccountMoney(int accountMoney) {
		this.accountMoney = accountMoney;
	}


	public String getAccountstd() {
		return accountstd;
	}


	public void setAccountstd(String accountstd) {
		this.accountstd = accountstd;
	}


	public int getCountNumber() {
		return countNumber;
	}


	public void setCountNumber(int countNumber) {
		this.countNumber = countNumber;
	}


	public ArrayList<AccountStandard> getNameASList() {
		return nameASList;
	}


	public void setNameASList(ArrayList<AccountStandard> nameASList) {
		this.nameASList = nameASList;
	}


	@Override
	public String toString() {
		return "AccountStandard [accountCode=" + accountCode + ", accountName=" + accountName + ", category=" + category
				+ ", stdpercent=" + stdpercent + ", accountMoney=" + accountMoney + ", accountstd=" + accountstd
				+ ", countNumber=" + countNumber + ", nameASList=" + nameASList + ", getAccountCode()="
				+ getAccountCode() + ", getAccountName()=" + getAccountName() + ", getCategory()=" + getCategory()
				+ ", getStdpercent()=" + getStdpercent() + ", getAccountMoney()=" + getAccountMoney()
				+ ", getAccountstd()=" + getAccountstd() + ", getCountNumber()=" + getCountNumber()
				+ ", getNameASList()=" + getNameASList() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	
	
	
}
