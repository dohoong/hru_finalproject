package com.hello.hru.salary.model.vo;

public class StandardInformation {

	private int salaryStandard;   //급여 기준 (1: 연봉제 ,2: 월급제 ,3: 시급제 )
	private String giveMoneyDate; //급여 지급 일자  
	private int accountStandard;  //근태 기준 일자 ( 1: 월말 , 2 : 월초)
	private int saturdaySelect;   //토요근무산정방법 ( 1: 무급휴무일 , 2: 정상근무)
	private String workingstart;  //근무시작시간
	private String workingend;	  //근무종료시간
	private String workingtime;   //근무 전체시간--
	private String extensionstart;//연장근무시작시간
	private String extensionend;  //연장근무종료시간
	private String extensiontime; //연장근무전체시간--
	private String breakstart;    //휴계시작시간
	private String breakend;      //휴계종료시간
	private String breaktime;     //휴계전체시간--
	private String nightstart;	  //야간근로시작시간
	private String nightend;      //야간근로종료시간
	private String nighttime;     //야간근로 전체시간--
	private String saturdayworkstart;  //토요근무시작시간
	private String saturdayworkend;	   //토요근무종료시간
	private String saturdayworktime;   //토요근무전체시간--
	private String saturdayextensionstart; //토요연장근무시작시간
	private String saturdayextensionend;   //토요연장근무종료시간
	private String saturdayextensiontime;  //토요연장근무 전체시간 --
	private String saturdaybreakstart; //토요휴계시작시간
	private String saturdaybreakend;   //토요휴계종료시간
	private String saturdaybreaktime;  //토요휴계전체시간--
	private String saturdaynightstart; //토요야간시작시간
	private String saturdaynightend;   //토요야간종료시간
	private String saturdaynighttime;  //토요야간 전체시간--
	
	
	
	public StandardInformation() {
		super();
	}
	public StandardInformation(int salaryStandard, String giveMoneyDate, int accountStandard, int saturdaySelect,
			String workingstart, String workingend, String workingtime, String extensionstart, String extensionend,
			String extensiontime, String breakstart, String breakend, String breaktime, String nightstart,
			String nightend, String nighttime, String saturdayworkstart, String saturdayworkend,
			String saturdayworktime, String saturdayextensionstart, String saturdayextensionend,
			String saturdayextensiontime, String saturdaybreakstart, String saturdaybreakend, String saturdaybreaktime,
			String saturdaynightstart, String saturdaynightend, String saturdaynighttime) {
		super();
		this.salaryStandard = salaryStandard;
		this.giveMoneyDate = giveMoneyDate;
		this.accountStandard = accountStandard;
		this.saturdaySelect = saturdaySelect;
		this.workingstart = workingstart;
		this.workingend = workingend;
		this.workingtime = workingtime;
		this.extensionstart = extensionstart;
		this.extensionend = extensionend;
		this.extensiontime = extensiontime;
		this.breakstart = breakstart;
		this.breakend = breakend;
		this.breaktime = breaktime;
		this.nightstart = nightstart;
		this.nightend = nightend;
		this.nighttime = nighttime;
		this.saturdayworkstart = saturdayworkstart;
		this.saturdayworkend = saturdayworkend;
		this.saturdayworktime = saturdayworktime;
		this.saturdayextensionstart = saturdayextensionstart;
		this.saturdayextensionend = saturdayextensionend;
		this.saturdayextensiontime = saturdayextensiontime;
		this.saturdaybreakstart = saturdaybreakstart;
		this.saturdaybreakend = saturdaybreakend;
		this.saturdaybreaktime = saturdaybreaktime;
		this.saturdaynightstart = saturdaynightstart;
		this.saturdaynightend = saturdaynightend;
		this.saturdaynighttime = saturdaynighttime;
	}
	public int getSalaryStandard() {
		return salaryStandard;
	}
	public void setSalaryStandard(int salaryStandard) {
		this.salaryStandard = salaryStandard;
	}
	public String getGiveMoneyDate() {
		return giveMoneyDate;
	}
	public void setGiveMoneyDate(String giveMoneyDate) {
		this.giveMoneyDate = giveMoneyDate;
	}
	public int getAccountStandard() {
		return accountStandard;
	}
	public void setAccountStandard(int accountStandard) {
		this.accountStandard = accountStandard;
	}
	public int getSaturdaySelect() {
		return saturdaySelect;
	}
	public void setSaturdaySelect(int saturdaySelect) {
		this.saturdaySelect = saturdaySelect;
	}
	public String getWorkingstart() {
		return workingstart;
	}
	public void setWorkingstart(String workingstart) {
		this.workingstart = workingstart;
	}
	public String getWorkingend() {
		return workingend;
	}
	public void setWorkingend(String workingend) {
		this.workingend = workingend;
	}
	public String getWorkingtime() {
		return workingtime;
	}
	public void setWorkingtime(String workingtime) {
		this.workingtime = workingtime;
	}
	public String getExtensionstart() {
		return extensionstart;
	}
	public void setExtensionstart(String extensionstart) {
		this.extensionstart = extensionstart;
	}
	public String getExtensionend() {
		return extensionend;
	}
	public void setExtensionend(String extensionend) {
		this.extensionend = extensionend;
	}
	public String getExtensiontime() {
		return extensiontime;
	}
	public void setExtensiontime(String extensiontime) {
		this.extensiontime = extensiontime;
	}
	public String getBreakstart() {
		return breakstart;
	}
	public void setBreakstart(String breakstart) {
		this.breakstart = breakstart;
	}
	public String getBreakend() {
		return breakend;
	}
	public void setBreakend(String breakend) {
		this.breakend = breakend;
	}
	public String getBreaktime() {
		return breaktime;
	}
	public void setBreaktime(String breaktime) {
		this.breaktime = breaktime;
	}
	public String getNightstart() {
		return nightstart;
	}
	public void setNightstart(String nightstart) {
		this.nightstart = nightstart;
	}
	public String getNightend() {
		return nightend;
	}
	public void setNightend(String nightend) {
		this.nightend = nightend;
	}
	public String getNighttime() {
		return nighttime;
	}
	public void setNighttime(String nighttime) {
		this.nighttime = nighttime;
	}
	public String getSaturdayworkstart() {
		return saturdayworkstart;
	}
	public void setSaturdayworkstart(String saturdayworkstart) {
		this.saturdayworkstart = saturdayworkstart;
	}
	public String getSaturdayworkend() {
		return saturdayworkend;
	}
	public void setSaturdayworkend(String saturdayworkend) {
		this.saturdayworkend = saturdayworkend;
	}
	public String getSaturdayworktime() {
		return saturdayworktime;
	}
	public void setSaturdayworktime(String saturdayworktime) {
		this.saturdayworktime = saturdayworktime;
	}
	public String getSaturdayextensionstart() {
		return saturdayextensionstart;
	}
	public void setSaturdayextensionstart(String saturdayextensionstart) {
		this.saturdayextensionstart = saturdayextensionstart;
	}
	public String getSaturdayextensionend() {
		return saturdayextensionend;
	}
	public void setSaturdayextensionend(String saturdayextensionend) {
		this.saturdayextensionend = saturdayextensionend;
	}
	public String getSaturdayextensiontime() {
		return saturdayextensiontime;
	}
	public void setSaturdayextensiontime(String saturdayextensiontime) {
		this.saturdayextensiontime = saturdayextensiontime;
	}
	public String getSaturdaybreakstart() {
		return saturdaybreakstart;
	}
	public void setSaturdaybreakstart(String saturdaybreakstart) {
		this.saturdaybreakstart = saturdaybreakstart;
	}
	public String getSaturdaybreakend() {
		return saturdaybreakend;
	}
	public void setSaturdaybreakend(String saturdaybreakend) {
		this.saturdaybreakend = saturdaybreakend;
	}
	public String getSaturdaybreaktime() {
		return saturdaybreaktime;
	}
	public void setSaturdaybreaktime(String saturdaybreaktime) {
		this.saturdaybreaktime = saturdaybreaktime;
	}
	public String getSaturdaynightstart() {
		return saturdaynightstart;
	}
	public void setSaturdaynightstart(String saturdaynightstart) {
		this.saturdaynightstart = saturdaynightstart;
	}
	public String getSaturdaynightend() {
		return saturdaynightend;
	}
	public void setSaturdaynightend(String saturdaynightend) {
		this.saturdaynightend = saturdaynightend;
	}
	public String getSaturdaynighttime() {
		return saturdaynighttime;
	}
	public void setSaturdaynighttime(String saturdaynighttime) {
		this.saturdaynighttime = saturdaynighttime;
	}
	@Override
	public String toString() {
		return "StandardInformation [salaryStandard=" + salaryStandard + ", giveMoneyDate=" + giveMoneyDate
				+ ", accountStandard=" + accountStandard + ", saturdaySelect=" + saturdaySelect + ", workingstart="
				+ workingstart + ", workingend=" + workingend + ", workingtime=" + workingtime + ", extensionstart="
				+ extensionstart + ", extensionend=" + extensionend + ", extensiontime=" + extensiontime
				+ ", breakstart=" + breakstart + ", breakend=" + breakend + ", breaktime=" + breaktime + ", nightstart="
				+ nightstart + ", nightend=" + nightend + ", nighttime=" + nighttime + ", saturdayworkstart="
				+ saturdayworkstart + ", saturdayworkend=" + saturdayworkend + ", saturdayworktime=" + saturdayworktime
				+ ", saturdayextensionstart=" + saturdayextensionstart + ", saturdayextensionend="
				+ saturdayextensionend + ", saturdayextensiontime=" + saturdayextensiontime + ", saturdaybreakstart="
				+ saturdaybreakstart + ", saturdaybreakend=" + saturdaybreakend + ", saturdaybreaktime="
				+ saturdaybreaktime + ", saturdaynightstart=" + saturdaynightstart + ", saturdaynightend="
				+ saturdaynightend + ", saturdaynighttime=" + saturdaynighttime + "]";
	}
	
	

	
}
