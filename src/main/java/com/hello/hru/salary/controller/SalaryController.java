package com.hello.hru.salary.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.common.ApprovalLine;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.CommonsUtils;
import com.hello.hru.common.model.vo.Search;
import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.service.MemberService;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.SearchMember;
import com.hello.hru.salary.model.service.SalaryService;
import com.hello.hru.salary.model.vo.AccountStandard;
import com.hello.hru.salary.model.vo.MemberBasicInfo;
import com.hello.hru.salary.model.vo.SalaryStatus;
import com.hello.hru.salary.model.vo.SaveSalary;
import com.hello.hru.salary.model.vo.StandardInformation;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
public class SalaryController { 

	@Autowired
	private SalaryService ss;
	@Autowired
	private AuthorityService aus;
	@Autowired
	private MemberService ms;
	
	
	//급여입력 - 퇴직사원관리 - 신고여부 
	@PostMapping("selectRetirementMemberInfo.salary")
	public ModelAndView selectRetirementMemberInfo(ModelAndView mv , HttpServletRequest request) {
		
		String sDate = request.getParameter("resultDate");
		String teamName = request.getParameter("teamName");
		
		Date quitDate = Date.valueOf(sDate);
		
		Search search = new Search();
		search.setSearchTeamName(teamName);
		search.setSearchMonthDate(quitDate);
		
		ArrayList<HashMap<String, Object>> mList = ss.selectRetirementMemberInfo(search);
		
		mv.addObject("mList", mList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	
	//급여입력 - 퇴직사원 검색
	@PostMapping("selectRetirementMember.salary")
	public ModelAndView selectRetirementMember(ModelAndView mv , HttpServletRequest request) {
		
		String teamName = request.getParameter("teamName");
		String resultDate = request.getParameter("resultDate");
		
		Date quitDate = Date.valueOf(resultDate);
		
		Search search = new Search();
		
		search.setSearchTeamName(teamName);
		search.setSearchMonthDate(quitDate);
		
		ArrayList<HashMap<String, Object>> mList = ss.selectRetirementMember(search);
		
		
		mv.addObject("mList", mList);
		mv.setViewName("jsonView");
		return mv;
		
	}
	
	
	//급여입력 - 퇴직사원 관리
	@RequestMapping("showRetiringMember.salary")
	public String showRetiringMember(HttpServletRequest request , Model model) {
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) { 
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		ArrayList<HashMap<String, String>> alist = aus.checkAuthorityTeam(mid, 3);

		if(alist != null) {
			model.addAttribute("tlist" , alist);
			return "salary/retiringMember";
		}else {
			model.addAttribute("msg" , "검색리스트 조회 실패");
			return "common/errorPage";
		}
		
	}
	
	
	//급여임력 - 퇴직금 산정 - 마감
	@GetMapping("updateQuitEnd.salary")
	public String updateQuitEnd(HttpServletRequest request , Model model) {
		int mid = Integer.parseInt(request.getParameter("mid"));
		int realResult= Integer.parseInt(request.getParameter("realResult"));
		int quitResult =Integer.parseInt(request.getParameter("quitResult"));
		String rDate = request.getParameter("quitDate");
		HashMap<String, Object> sList = new HashMap<String, Object>();
		
		Date quitDate = Date.valueOf(rDate);
		
		sList.put("mid", mid);
		sList.put("realResult",realResult);
		sList.put("quitResult", quitResult);
		sList.put("quitDate" , quitDate);
		
		int result = ss.updateQuitEnd(sList);
		
		if(result > 0) {
			return "redirect:/quitMoneyResult.salary";
		}else {
			model.addAttribute("msg" , "퇴직 마감 실패!!");
			return "common/errorPage";
		}
		
	}
	
	//급여입력-퇴직금정산-퇴직금 ( + 보수월액 추가 ) 
		@PostMapping("selectMemberQuitMoneySalary.salary")
		public ModelAndView selectMemberQuitMoney(ModelAndView mv , HttpServletRequest request) {
			
			int mid = Integer.parseInt(request.getParameter("mid"));
			//사원 정보
			HashMap<String , Object> qList = ms.selectMemberQuitBasic(mid);
			
			//3개월 임금총액 계산내역 
			ArrayList<HashMap<String, Object>> tMoneyList = ms.selectTotalMoneyList(mid);
			//연차수당 , 상여금
			HashMap<String, Object> plusSalary = ms.selectPlusSalaryList(mid);
			System.out.println("3개월 임금총액 : " + tMoneyList);
			System.out.println("연차수당 , 상여금  : "+ plusSalary);
			//평균임금
			
			int avgMoney = 0;
			int avgDate = 0;
			for(int i =0; i<tMoneyList.size(); i++) {
				//기본급 + 수당
				avgMoney += (int) tMoneyList.get(i).get("money");
				avgMoney += (int) tMoneyList.get(i).get("plus");
				//3개월 날짜 계산
				avgDate += (int) tMoneyList.get(i).get("minus");
			}
			// + 연차수당
			avgMoney +=  ((int)plusSalary.get("vacation")) * 0.25;
			// + 상여금
			avgMoney += ((int)plusSalary.get("plus")) * 0.25;
			
			
			//퇴직금 계산
			HashMap<String, Object> qSalary = new HashMap<String, Object>();
			//평균임금
			int resultAvg = avgMoney / avgDate;
			//1일 통상임금
			int ordinarySalary = (int) ((double) plusSalary.get("avg") * 8);
			//퇴직금
			double quitSalary = 0;
			//평균임금이 통상임금보다 높을 때
			if(resultAvg > ordinarySalary) {
				quitSalary = Math.round(((int)qList.get("employeeDate") / (double) 365) * 30 * resultAvg);
			}
			//통상임금이 평균임금보다 높을 때
			else {
				quitSalary = Math.round(((int)qList.get("employeeDate") / (double) 365) * 30 * ordinarySalary);
			}
			
			
			System.out.println("1일 평균임금 : " + resultAvg);
			System.out.println("1시간통상임금 :" + plusSalary.get("avg"));
			System.out.println("1일통산임금 : " + ordinarySalary);
			System.out.println("퇴직금 : " + quitSalary);
			
			//1일 평균임금 , 1일통상임금 , 퇴직금
			qSalary.put("avgSalary", resultAvg);
			qSalary.put("ordinarySalary" ,ordinarySalary);
			qSalary.put("quitSalary" , quitSalary);
			
			
			//퇴직금 계산
			mv.addObject("qSalary" , qSalary);
			//연차수당 , 상여금
			mv.addObject("plusSalary", plusSalary);
			//3개월 임금총액 계산내역 
			mv.addObject("tMoneyList", tMoneyList);
			mv.setViewName("jsonView");
			return mv;
		}
	
	//퇴직금산정 - 사원 클릭 (사원 기본정보 )
	@PostMapping("selectMemberInfo.salary")
	public ModelAndView memberInfomation(ModelAndView mv , HttpServletRequest request) {
		int mid = Integer.parseInt(request.getParameter("mid"));
		
		HashMap<String , Object> qList = ss.selectMemberQuitBasic(mid);
		
		System.out.println(qList);
		
		mv.addObject("qList", qList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//퇴직금산정 - 사원 검색 
	@PostMapping("selectQuitMemberList.salary")
	public ModelAndView quitMemberList(ModelAndView mv , HttpServletRequest request) {
	
		String teamName = request.getParameter("teamName");
		String resultDate = request.getParameter("resultDate");
		String name = request.getParameter("name");

		Date searchDate = Date.valueOf(resultDate);

		Search search = new Search();

		search.setSearchContent(name);
		search.setSearchMonthDate(searchDate);
		search.setSearchTeamName(teamName);

		ArrayList<HashMap<String, Object>> qList = ss.selectQuitMemberList(search);
		
		mv.addObject("qList", qList);
		mv.setViewName("jsonView");
		return mv;
		
	}
	
	//사원 - 퇴직신청 - 전자결재 
	@PostMapping("memberQuitApproval.salary")
	public String quitApproval(Model model , ApprovalBasic ab , HttpServletRequest request) {
		
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		Approval app = new ApprovalLine().makingApprovalLine(9, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		
		String dateof = request.getParameter("quitDate");
		
		System.out.println("date : " +dateof);
		
		Search search = new Search();
		search.setMid(mid);
		search.setSearchDate(dateof);
		
		int result = ss.insertQuitSalary(app , search);
		
		if(result > 0) {
			return "redirect:/showInfoQuitMoney.me";
		}else {
			model.addAttribute("msg", "전자결재 실패입니다.");
			return "common/errorPage";
		}
	}
	
	
	
	//급여관리-급여관리-퇴직금정산
	@RequestMapping("quitMoneyResult.salary")
	public String quitMoneyResult(Model model , HttpServletRequest request) {
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) { 
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		ArrayList<HashMap<String, String>> alist = aus.checkAuthorityTeam(mid, 3);

		if(alist != null) {
			model.addAttribute("tlist" , alist);
			return "salary/quitMoneyResult";
		}else {
			model.addAttribute("msg" , "퇴직금 정산 조회 실패");
			return "common/errorPage";
		}
	}

	
	//급여관리-이메일발송-이메일발송
	@PostMapping("sendEmail.salary")
	public ModelAndView sendEmail(ModelAndView mv , @RequestParam String jsonData) throws AddressException, MessagingException {
		
		final String user = "hrforyou0214@gmail.com";
		final String password = "qorwp18660!";

		Properties prop = new Properties(); 
		prop.put("mail.smtp.host", "smtp.gmail.com"); 
		prop.put("mail.smtp.port", 465); 
		prop.put("mail.smtp.auth", "true"); 
		prop.put("mail.smtp.ssl.enable", "true"); 
		prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		Session session = Session.getDefaultInstance(prop , new javax.mail.Authenticator() {
			String un = user;
			String pw = password;
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication(user, password);
			}
		});
		session.setDebug(true);

		Message mimeMessage = new MimeMessage(session);
		mimeMessage.setFrom(new InternetAddress("hrforyou0214@gmail.com"));

		JSONArray array = JSONArray.fromObject(jsonData);

		//사원 한명 한명에대해 이메일 전송이 이루어져야한다.
		for(int i=0; i<array.size(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			int mid =Integer.parseInt(jsonObject.getString("mid"));
			Date searchDate = Date.valueOf(jsonObject.getString("giveDate"));
			String email = jsonObject.getString("email");
			String searchDay = searchDate.toString();
			String teamName = jsonObject.getString("teamName");
			String name = jsonObject.getString("name");

			String[] day = searchDay.split("-");
			
			String resultDay = day[0]+"년 "+day[1]+"월 ";

			Search search = new Search();

			search.setMid(mid);
			search.setSearchMonthDate(searchDate);
			//선택한 사원 한명의 급여 내역 조회
			ArrayList<HashMap<String, Object>> sList = ss.selectMonthSalary(search);

			ArrayList<Integer> salaryList = new ArrayList<Integer>();
			int giveTotal = 0;
			int taxTotal = 0;
			int resultSalary = 0;
			for(int j=0; j < 8; j++) {
				salaryList.add((Integer) sList.get(j).get("sdetailMoney"));
				giveTotal += (int) sList.get(j).get("sdetailMoney");
			}
			for(int j=8; j < 14; j++) {
				salaryList.add((Integer) sList.get(j).get("sdetailMoney"));
				taxTotal += (int) sList.get(j).get("sdetailMoney");
			}
			resultSalary = giveTotal - taxTotal;

			salaryList.add(giveTotal);
			salaryList.add(taxTotal);
			salaryList.add(resultSalary);
			
			String form = "<div style='width: 800px;  border: 1px solid gray; padding: 30px;'>\r\n" + 
					"    <div style='width: 100%'>\r\n" + 
					"        <div class='header'>\r\n" + 
					"            <div style='display: inline-block; width: 48%; vertical-align: top;'>\r\n" + 
					"                <h2 style='color: black;'>"+resultDay +" 급여명세서</h2>\r\n" + 
					"            </div>\r\n" + 
					" \r\n" + 
					"            <div style='display: inline-block; width: 48%; margin-top: 20px; margin-left: 10px; '>\r\n" + 
					"                <label style='font-size: medium; font-weight:bold; float: right; padding-top: 7px;'>지급일 : " +searchDay + "</label>\r\n" + 
					"            </div>\r\n" + 
					"        </div>\r\n" + 
					" \r\n" + 
					"    </div>\r\n" + 
					"    <div class='image content' style='display: inline-block; width: 100%;'>\r\n" + 
					"        <div class='hru segment' id='memberInfo' style='padding-top:0px;padding-bottom:0px;margin-bottom:20px;padding:10px;'>\r\n" + 
					"            <table id='memberTable'><tr>\r\n" + 
					"					 <td>성명 : </td>\r\n" + 
					"                    <td>"+ name+ "</td>\r\n" + 
					"                    <td style='width: 40px;'></td>\r\n" + 
					"                    <td style='width: 40px;'></td>\r\n" + 
					"                    <td>부서 : </td>\r\n" + 
					"                    <td>"+teamName +"</td>\r\n" + 
					"                </tr></table>\r\n" + 
					"</div>\r\n" + 
					"        <div class='hru segment' style='margin-bottom: 20px;\r\n" + 
					"        padding: 10px;'>\r\n" + 
					"            <table class='hru board2' border='1' id='memberTable' style='width: 100%;\r\n" + 
					"        border-collapse: collapse;\r\n" + 
					"        border-top: 2px solid #d4d4d4;\r\n" + 
					"        border-right: 2px solid white;\r\n" + 
					"        border-left: 2px solid white;\r\n" + 
					"        border-bottom: 2px solid #ccc;\r\n" + 
					"        font-size: 1.1em;'>\r\n" + 
					"<thead><tr style='width: 100%;'>\r\n" + 
					"<th style='width:200px;height:35px;text-align:center;border-top:1px solid #eee;border-bottom:1px solid #eee;background:#E4EAF0;color:#0F4C81;'>지급항목</th>\r\n" + 
					"                        <th style='height: 35px;\r\n" + 
					"        text-align: center;\r\n" + 
					"        border-top: 1px solid #eee;\r\n" + 
					"        border-bottom: 1px solid #eee;\r\n" + 
					" \r\n" + 
					"        background: #E4EAF0;\r\n" + 
					"        color: #0F4C81;'>지급액</th>\r\n" + 
					"                        <th style='width:200px;height:35px;text-align:center;border-top:1px solid #eee;border-bottom:1px solid #eee;background:#E4EAF0;color:#0F4C81;'>공제항목</th>\r\n" + 
					"                        <th style='height: 35px;\r\n" + 
					"        text-align: center;\r\n" + 
					"        border-top: 1px solid #eee;\r\n" + 
					"        border-bottom: 1px solid #eee;\r\n" + 
					" \r\n" + 
					"        background: #E4EAF0;\r\n" + 
					"        color: #0F4C81;'>공제액</th>\r\n" + 
					"                    </tr></thead>\r\n" + 
					"<tbody>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>기본급</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(0))+"</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>국민연금</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(8))+"</td>\r\n" + 
					"                    </tr>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>상여</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(1))+"</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>건강보험</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(9))+"</td>\r\n" + 
					"                    </tr>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>연차수당</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(2))+"</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>장기요양보험</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(10))+"</td>\r\n" + 
					"                    </tr>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>차량보조</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(3))+"</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>고용보험</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(11))+"</td>\r\n" + 
					"                    </tr>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>식대</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(4))+"</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>소득세</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(12))+"</td>\r\n" + 
					"                    </tr>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>잔업수당</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(5))+"</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>지방 소득세</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(13))+"</td>\r\n" + 
					"                    </tr>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>야근수당</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(6))+"</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'></td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'></td>\r\n" + 
					"                    </tr>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>휴일수당</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(7))+"</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'></td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'></td>\r\n" + 
					"                    </tr>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td style='font-weight:bold;border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>지급항목계</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(14))+"</td>\r\n" + 
					"                        <td style='font-weight:bold;border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>공제항목계</td>\r\n" + 
					"                        <td style='border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(15))+"</td>\r\n" + 
					"                    </tr>\r\n" + 
					"<tr style='width: 100%;'>\r\n" + 
					"<td colspan='3' style='font-weight:bold;background:#e0e0e0;border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'><label style='padding-right: 10px;'>실지급금액계</label></td>\r\n" + 
					"                        <td style='font-weight:bold; border-top:1px solid #eee;border-bottom:1px solid #eee;text-align:center;height:50px;width:200px;'>"+toNumFormat(salaryList.get(16))+"</td>\r\n" + 
					"                    </tr>\r\n" + 
					"</tbody>\r\n" + 
					"</table>\r\n" + 
					"<label class='bottomText' style='float:left;margin:1% auto;'>귀하의 노고에 감사드립니다.</label>\r\n" + 
					"            <lable class='bottomText' style='float:right;margin:1% auto;'>오렌지레드 컴퍼니</lable>\r\n" + 
					"</div>\r\n" + 
					"    </div>\r\n" + 
					"</div>";			
			//수신자셋팅
			mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(email));

			mimeMessage.setSubject(resultDay+" " + teamName + " " + name +"님의 급여명세서입니다.");
			mimeMessage.setContent(form, "text/html; charset=utf-8");
			
			Transport.send(mimeMessage);
		}

		mv.addObject("result", 1);
		mv.setViewName("jsonView");
		return mv;
	}
	 public static String toNumFormat(int num) {
		  DecimalFormat df = new DecimalFormat("#,###");
		  return df.format(num);
	}


	//급여관리-이메일발송-검색결과 (사원검색)
	@PostMapping("selectMonthSalaryEmail.salary")
	public ModelAndView selectMonthSalaryEmailMember(ModelAndView mv , HttpServletRequest request) {

		String teamName = request.getParameter("teamName");
		String resultDate = request.getParameter("resultDate");
		String name = request.getParameter("name");

		Date searchDate = Date.valueOf(resultDate);

		Search search = new Search();

		search.setSearchContent(name);
		search.setSearchMonthDate(searchDate);
		search.setSearchTeamName(teamName);

		ArrayList<HashMap<String, Object>> mList = ss.searchMonthEmailSalary(search);

		mv.addObject("mList", mList);
		mv.setViewName("jsonView");
		return mv;
	}



	//급여관리-이메일발송-페이지 이동
	@RequestMapping("showWageStatusAndEmail.salary")
	public String showWageStatusAndEmail(Model model , HttpServletRequest request) {
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) { 
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		ArrayList<HashMap<String, String>> alist = aus.checkAuthorityTeam(mid, 3);

		if(alist != null) {
			model.addAttribute("tlist" , alist);
			return "salary/wageStatusAndEmail";
		}else {
			model.addAttribute("msg" , "이메일 발송 페이지 조회 실패");
			return "common/errorPage";
		}
	}


	//급여관리-급여지급현황-사원의 1년 급여지급 내역 조회
	@PostMapping("selectMemberYearSalary.salary")
	public ModelAndView selectMemberYearSalary(ModelAndView mv , HttpServletRequest request) {

		int searchDate = Integer.parseInt(request.getParameter("searchDay"));
		int mid = Integer.parseInt(request.getParameter("mid"));

		Search search = new Search();
		search.setMid(mid);
		search.setSearchDay(searchDate);

		HashMap<String, ArrayList<SalaryStatus>> myList = ss.selectMemberYearSalary(search);
		System.out.println("myList : " + myList);

		mv.addObject("myList" , myList);
		mv.setViewName("jsonView");
		return mv;
	}

	//급여관리-급여지급현황-조건에 맞는 사원 검색
	@PostMapping("selectMonthlyWageMember.salary")
	public ModelAndView selectMonthlyWageMember(ModelAndView mv , HttpServletRequest request) {

		String teamName = request.getParameter("teamName");

		Search search = new Search();

		search.setSearchTeamName(teamName);

		ArrayList<SearchMember> mList = ss.selectMonthlyWageMember(search);

		mv.addObject("mList" , mList);
		mv.setViewName("jsonView");
		return mv;
	}

	//급여관리-급여입력-입력페이지 검색 결과
	@RequestMapping("showMonthlyWageStatus.salary")
	public String showMonthlyWageStatus(Model model , HttpServletRequest request) {
		//권한에 따른 검색 팀 리스트
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) { 
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		ArrayList<HashMap<String, String>> alist = aus.checkAuthorityTeam(mid, 3);

		if(alist != null) {
			model.addAttribute("tlist" , alist);
			return "salary/monthlyWageStatus";
		}else {
			model.addAttribute("msg" , "급여 입력 부서 리스트 조회 실패");
			return "common/errorPage";
		}
	}



	//급여관리 - 급여입력 - 팀 월 급여 전자 결재
	@PostMapping("MonthSalaryApproval.salary")
	public String endSalary(Model model, ApprovalBasic ab, HttpServletRequest request, @RequestParam(name="appFile") MultipartFile file) {

		int teamCode = Integer.parseInt(request.getParameter("MonthSalaryTeamCode"));
		String searchDate = request.getParameter("MonthSalarySearchDate");

		Search search = new Search();

		search.setSearchTeamCode(teamCode);
		search.setSearchDate(searchDate);
		//사원 목록 id 조회	
		ArrayList<Integer> salaryId = ss.selectSalaryIdList(search);
		System.out.println("salaryId : " + salaryId);

		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		System.out.println("ApprovalBasic : " + ab);


		Approval app = new ApprovalLine().makingApprovalLine(6, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		System.out.println(app);

		//파일에 대한 정보
		String root = request.getSession().getServletContext().getRealPath("resources");

		String os = System.getProperty("os.name").toLowerCase();
		//System.out.println(os);
		String sl = "";
		if(os.equals("mac os x")) {
			sl = "/";
		} else {
			sl = "\\";
		}

		String filePath = root + sl + "uploadFiles";
		Attachment att = new Attachment();
		try {
			if(!file.getOriginalFilename().equals("")) {
				String originFileName = file.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();


				att.setOriginName(originFileName);
				att.setChangeName(changeName);
				att.setPath(filePath);
				att.setCategory(6);
				att.setFileLevel(0);

				file.transferTo(new File(filePath + sl + changeName + ext));
			}
			int result = ss.insertMonthSalary(app, att , salaryId);

			if(result > 0) {
				return "redirect:/showinsertWage.salary";
			}else {
				model.addAttribute("msg", "전자결재 실패입니다.");

				return "common/errorPage";

			}


		}catch(IllegalStateException | IOException e) {
			model.addAttribute("msg", "파일업로드에 실패하였습니다.");

			return "common/errorPage";
		}

	}
	//급여관리-급여입력 - 전체 급여 확인
	@PostMapping("searchTeamSalary.salary")
	public ModelAndView searchTeamSalary(ModelAndView mv , HttpServletRequest request) {

		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		String searchDay = request.getParameter("resultDay");

		Search search =new Search();

		search.setSearchTeamCode(teamCode);
		search.setSearchDate(searchDay);

		ArrayList<HashMap<String, Object>> stList = ss.searchTeamSalary(search);

		System.out.println("stList : " + stList);

		mv.addObject("stList" , stList);
		mv.setViewName("jsonView");
		return mv;
	}

	//급여관리-급여입력-사원 지급/공제내역 업데이트
	@PostMapping("updateMemberMonthSalary.salary")
	public ModelAndView updateMemberMonthSalary(ModelAndView mv , HttpServletRequest request) {

		int mid = Integer.parseInt(request.getParameter("mid"));				//사원 id
		int meal = Integer.parseInt(request.getParameter("mealSalary"));		//식대
		int carSalary = Integer.parseInt(request.getParameter("carSalary"));	//차량유지비
		int membertotal = Integer.parseInt(request.getParameter("membertotal"));//실지급액
		int carId = Integer.parseInt(request.getParameter("carId"));			//상세급여 ID (차량유지비)
		int mealId = Integer.parseInt(request.getParameter("mealId"));			//상세급여 ID (식대)
		String searchDay = request.getParameter("resultDay");

		SaveSalary save = new SaveSalary();

		save.setMid(mid);
		save.setCar(carSalary);
		save.setMeal(meal);
		save.setTotal(membertotal);
		save.setSearchDay(searchDay);
		save.setCarId(carId);
		save.setMealId(mealId);

		int result = ss.updateMemberMonthSalary(save);

		if(result>0) {
			mv.addObject("result" , 1);
			mv.setViewName("jsonView");
		}

		return mv;
	}



	//급여관리-급여입력-사원 지급/공제내역 조회
	@PostMapping("selectMemberMonthSalary.salary")
	public ModelAndView selectMemberMonthSalary(ModelAndView mv , HttpServletRequest request) {

		int mid = Integer.parseInt(request.getParameter("mid"));
		int resultDate = Integer.parseInt(request.getParameter("resultDay"));
		String resultDay = request.getParameter("resultDay");

		//System.out.println(resultDay);

		Search search = new Search();

		search.setMid(mid);
		search.setSearchContent(resultDay);
		//지금만 팀코드에 insert
		search.setSearchTeamCode(resultDate);
		ArrayList<HashMap<String, Object>> msList = ss.selectMemberMonthSalary(search);
		System.out.println("msList : " + msList);
		ArrayList<HashMap<String, Integer>> atList = ss.selectMemberMonthSalaryTwo(search);
		System.out.println("atList : " + atList);
		System.out.println(msList);
		mv.addObject("atList",atList);
		mv.addObject("msList",msList);
		mv.setViewName("jsonView");
		return mv;
	}

	//급여관리-급여입력-검색 결과
	@RequestMapping("searchInsertSalary.salary")
	public ModelAndView searchInsertSalary(ModelAndView mv , HttpServletRequest request) {

		String resultDay = request.getParameter("resultDay");
		String searchTeamName = request.getParameter("searchTeamName");
		int resultDate = Integer.parseInt(request.getParameter("resultDay"));

		//검색 날짜 , 검색 부서이름 객체 저장
		Search search = new Search();
		search.setSearchDate(resultDay);
		search.setSearchTeamName(searchTeamName);
		search.setSearchTeamCode(resultDate);


		ArrayList<SearchMember> mList = ss.searchInsertSalary(search);
		//System.out.println("컨트롤러 !!!!!! mList !!!!!! : " + mList);
		mv.addObject("mList" , mList);
		mv.setViewName("jsonView");

		return mv;
	}

	//급여관리-급여입력-입력페이지 검색 결과
	@RequestMapping("showinsertWage.salary")
	public String showinsertWage(Model model , HttpServletRequest request) {
		//권한에 따른 검색 팀 리스트
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) { 
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		ArrayList<HashMap<String, String>> alist = aus.checkAuthorityTeam(mid, 3);

		if(alist != null) {
			model.addAttribute("tlist" , alist);
			return "salary/wageinsert";
		}else {
			model.addAttribute("msg" , "급여 입력 부서 리스트 조회 실패");
			return "common/errorPage";
		}
	}


	//급여관리-급여관리-검색사원기본정보 업데이트
	@PostMapping("UpdateMemberBasicInfo.salary")
	public String updateMemberBasicInfo(MemberBasicInfo mbInfo , Model model) {

		//System.out.println(mbInfo);

		int result = ss.updateMemberBasicInfo(mbInfo);

		if(result > 0) {
			return "redirect:/showWageBasicInformation.salary";
		}else {
			model.addAttribute("msg", "업데이트 에러");
			return "common/errorPage";
		}
	}


	//급여관리-급여관리-사원급여정보 검색
	@PostMapping("selectMemberSalaryInfo.salary")
	public ModelAndView selectMemberSalaryInfo(ModelAndView mv , HttpServletRequest request) {
		int mid = Integer.parseInt(request.getParameter("mid"));

		HashMap<String, Object> sMap = ss.selectMemberSalaryInfo(mid);

		//System.out.println(sMap);

		mv.addObject("sMap" , sMap);
		mv.setViewName("jsonView");

		return mv;
	}

	//급여관리-급여관리-사원부양가족 검색
	@PostMapping("selectMemberFamilyInfo.salary")
	public ModelAndView selectMemberFamilyInfo(ModelAndView mv , HttpServletRequest request) {
		int mid = Integer.parseInt(request.getParameter("mid"));

		ArrayList<HashMap<String, Object>> fList = ss.selectMemberFailyInfo(mid);

		//System.out.println(fList);

		mv.addObject("fList",fList);
		mv.setViewName("jsonView");

		return mv;
	}


	//급여관리-급여관리-사원 클릭 결과
	@PostMapping("selectMemberBasicInfo.salary")
	public ModelAndView selectMemberBasicInfo(ModelAndView mv , HttpServletRequest request) {
		int mid = Integer.parseInt(request.getParameter("mid"));

		HashMap<String, Object> bMap = ss.selectMemberBasicInfo(mid);

		//System.out.println(bMap);

		mv.addObject("bMap" , bMap);
		mv.setViewName("jsonView");

		return mv;
	}

	//급여관리-급여관리-급여기본페이지 검색 결과
	@PostMapping("searchResult.salary")
	public ModelAndView searchResultMemberList(ModelAndView mv , HttpServletRequest request) {
		Search search = new Search();

		String searchTeamName = request.getParameter("searchTeamName");
		String searchContent = request.getParameter("searchContent");
		search.setSearchTeamName(searchTeamName);
		search.setSearchContent(searchContent);

		ArrayList<SearchMember> mList = ss.searchMember(search);

		//System.out.println(mList);

		mv.addObject("mList" , mList);
		mv.setViewName("jsonView");

		return mv; 
	}


	//급여관리-급여관리-급여기본정보페이지 이동
	@RequestMapping("showWageBasicInformation.salary")
	public String showWageBasicInformation(Model model , HttpServletRequest request) {
		//권한에 따른 검색 팀 리스트
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) { 
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		ArrayList<HashMap<String, String>> alist = aus.checkAuthorityTeam(mid, 3);

		//System.out.println("alist : " + alist);


		//ArrayList<Team> tlist = ss.selectTeamList();
		if(alist != null) {
			model.addAttribute("tlist" , alist);
			return "salary/wagebasicinformation";
		}else {
			model.addAttribute("msg" , "부서 리스트 조회 실패");
			return "common/errorPage";
		}
	}



	//급여/근태정보 셀렉트
	@RequestMapping("showSalaryAttitudeAdministration.salary")
	public String SelectSalaryAttendanceStandard(Model model) {

		HashMap<String, Object> sMap = ss.selectsMap();

		model.addAttribute("sMap", sMap);

		return "system/salaryAttitudeAdministration";
	}
	//급여/근태기준 - 급여기준 , 근태기준 업데이트
	@PostMapping("standardInfo.salary")
	public String standardInfo(Model model , StandardInformation stdinfo , HttpServletRequest request) {
		int num = 0;
		stdinfo.setWorkingtime(stdinfo.getWorkingstart() + "~" + stdinfo.getWorkingend());
		stdinfo.setExtensiontime(stdinfo.getExtensionstart()+"~"+stdinfo.getExtensionend());
		stdinfo.setBreaktime(stdinfo.getBreakstart()+"~"+stdinfo.getBreakend());
		stdinfo.setNighttime(stdinfo.getNightstart()+"~"+stdinfo.getNightend());

		stdinfo.setSaturdayworktime(stdinfo.getSaturdayworkstart()+"~"+stdinfo.getSaturdayworkend());
		stdinfo.setSaturdayextensiontime(stdinfo.getSaturdayextensionstart() + "~"+stdinfo.getSaturdayextensionend());
		stdinfo.setSaturdaybreaktime(stdinfo.getSaturdaybreakstart()+"~"+stdinfo.getSaturdaybreakend());
		stdinfo.setSaturdaynighttime(stdinfo.getSaturdaynightstart()+"~"+stdinfo.getSaturdaynightend());

		int result = ss.updateStandardInfo(stdinfo);

		if(result > 0) {
			return "redirect:/showSalaryAttitudeAdministration.salary";
		}else {
			model.addAttribute("msg", "업데이트 에러");
			return "common/errorPage";
		}
	}
	//급여정보 - 기본급 /상여금 업데이트
	@PostMapping("SalaryInfo.salary")
	public String salaryInfo(Model model , AccountStandard accstd , HttpServletRequest request) {

		int result = ss.updateSalaryInfo(accstd);

		if(result > 0) {
			return "redirect:/showSalaryAttitudeAdministration.salary";
		}else {
			model.addAttribute("msg", "급여업데이트에러");
			return "common/errorPage";
		}

	}
	//비과세항목 추가 - 저장 
	@PostMapping("SalaryNoneTaxAdd.salary")
	public String SalaryNoneTaxAdd( AccountStandard accstd , Model model  ) {
		int result = ss.insertNonTaxAdd(accstd);

		if(result >0) {
			return "redirect:/showSalaryAttitudeAdministration.salary";
		}else {
			model.addAttribute("msg", "비과세항목 추가 에러");
			return "common/errorPage";
		}
	}
	//비과세항목 업데이트 - 저장
	@PostMapping("SalaryNoneTaxUpdate.salary")
	public String SalaryNoneTaxUpdate(Model model , AccountStandard accstd , HttpServletRequest request) {
		int result = ss.updateNoneTax(accstd);

		if(result > 0) {
			return "redirect:/showSalaryAttitudeAdministration.salary";
		}else {
			model.addAttribute("msg", "비과세항목 업데이트 에러");
			return "common/errorPage";
		}
	}
	//비과세항목 삭제 -삭제
	@PostMapping("SalaryNoneTaxDelete.salary")
	public String SalaryNoneTaxDelete(Model model , AccountStandard accstd , HttpServletRequest request) {

		int result = ss.deleteNoneTax(accstd);
		if(result > 0) {
			return "redirect:/showSalaryAttitudeAdministration.salary";
		}else {
			model.addAttribute("msg" , "비과세항목 삭제 에러");
			return "common/errorPage";
		}
	}
	//연장근무수당 추가
	@PostMapping("ExtensionAdd.salary")
	public String SalaryExtensionAdd( AccountStandard accstd , Model model) {

		int result = ss.insertExtensionAdd(accstd);

		if(result > 0) {
			return "redirect:/showSalaryAttitudeAdministration.salary";
		}else {
			model.addAttribute("msg" , "연장근무수당 추가 에러");
			return "common/errorPage";
		}
	}
	@PostMapping("SalaryExtensionUpdate.salary")
	public String SalaryExtensionUpdate(AccountStandard accstd , Model model) {

		int result = ss.updateExtensionUpdate(accstd);

		if(result > 0) {
			return "redirect:/showSalaryAttitudeAdministration.salary";
		}else {
			model.addAttribute("msg", "연장근무 수정 에러");
			return "common/errorPage";
		}
	}
	@PostMapping("SalaryExtensionDelete.salary")
	public String SalaryExtensionDelete(AccountStandard accstd , Model model) {

		int result = ss.deleteExtensionDelete(accstd);

		if(result>0) {
			return "redirect:/showSalaryAttitudeAdministration.salary";
		}else {
			model.addAttribute("msg", "연장근무 삭제 에러");
			return "common/errorPage";
		}
	}
	@PostMapping("fourInsuranceUpdate.salary")
	public String FourInsuranceUpdate(AccountStandard accstd , Model model) {

		int result = ss.updateFourInsurance(accstd);

		if(result > 0) {
			return "redirect:/showSalaryAttitudeAdministration.salary";
		}else {
			model.addAttribute("msg" , "4대보험 갱신 실패");
			return "common/errorPage";
		}

	}
}
