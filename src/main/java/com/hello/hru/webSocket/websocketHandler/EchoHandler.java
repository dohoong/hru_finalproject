package com.hello.hru.webSocket.websocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.hello.hru.approval.model.service.ApprovalService;
import com.hello.hru.approval.model.vo.Alarm;
import com.hello.hru.common.ApprovalLine;


public class EchoHandler extends TextWebSocketHandler {
	@Autowired
	private ApprovalService as;
	
	
	private List<WebSocketSession> sessions = new ArrayList<WebSocketSession>();
    //private Set<WebSocketSession> sessions = Collections.synchronizedSet(new HashSet<WebSocketSession>());
    
    public void noticeAlarm(Alarm alarm){
    	
    	Iterator<WebSocketSession> iterator = sessions.iterator();
    	System.out.println(alarm);
    	while(iterator.hasNext()) {
    		WebSocketSession session = iterator.next();
    		try {
    			//클라이언트에게 전송할 메시지를 TextMessage객체로 만든다.
    			TextMessage message = new TextMessage(alarm.getContent() + "(" + alarm.getAlarmDate() + ")");
    			session.sendMessage(message);
    		} catch(IOException e) {
    			//전송중에 에러가 발생하는 해당 클라이언트와 통신을 담당하는 websocketsession 객체를 삭제한다.
    			iterator.remove();
    		}
    	}
    }
    
    
	// 클라이언트와 연결 이후에 실행되는 메서드
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessions.add(session);
		System.out.println("연결성공 : "+ session.getId() + "/ run Time : " + new Date());
	}
     
	// 클라이언트가 서버로 메시지를 전송했을 때 실행되는 메서드
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(message.getPayload());
		JSONObject jobj = (JSONObject) obj;
		System.out.println(message.getPayload());
		String smid = (String)jobj.get("smid");
		String tmid = (String)jobj.get("tmid");
		Alarm alarm = new ApprovalLine().makingAlarm((String)jobj.get("content"), Integer.parseInt(smid), Integer.parseInt(tmid));
		Alarm resultAlarm = as.insertAlarm(alarm);
		
		if(resultAlarm.getAlarmId()>0) {
			System.out.println("메시지 전송 성공! : " + resultAlarm);
			JSONObject robj = new JSONObject();
			robj.put("alarmId", resultAlarm.getAlarmId());
			robj.put("content", resultAlarm.getContent());
			robj.put("smid", resultAlarm.getSendMid());
			robj.put("tmid", resultAlarm.getTakeMid());
			robj.put("alarmDate", resultAlarm.getAlarmDate());
			
			for (WebSocketSession sess : sessions) {
				sess.sendMessage(new TextMessage(robj.toJSONString()));
			}
		}
		
//		System.out.println(("{}로 부터 {} 받음"+ session.getId()+ message.getPayload()));

	}
     
      // 클라이언트와 연결을 끊었을 때 실행되는 메소드
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessions.remove(session);
		System.out.println(("{} 연결 끊김"+ session.getId()));
	}  
}
 
