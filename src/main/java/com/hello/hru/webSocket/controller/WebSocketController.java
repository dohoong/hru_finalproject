package com.hello.hru.webSocket.controller;

import javax.websocket.server.ServerEndpoint;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
//@ServerEndpoint(value="/echo.so")
public class WebSocketController {
	@RequestMapping("showWebSocket.so")
	public String showWebSocket() {
		return "test/websocket-echo";
	}
}
