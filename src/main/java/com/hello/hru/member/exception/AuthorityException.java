package com.hello.hru.member.exception;

public class AuthorityException extends Exception{
	
	public AuthorityException(String msg) {
		super(msg);
	}

}
