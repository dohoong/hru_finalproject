package com.hello.hru.member.exception;

public class LoginException extends Exception{
	public LoginException(String msg) {
		super(msg);
	}
}
