package com.hello.hru.member.model.vo;

public class AuthorityMember {
	private int mid;
	private int authorityId;
	private int mauthorityId;
	private int teamCode;
	private String useYn;
	
	public AuthorityMember() {}

	public AuthorityMember(int mid, int authorityId, int mauthorityId, int teamCode, String useYn) {
		super();
		this.mid = mid;
		this.authorityId = authorityId;
		this.mauthorityId = mauthorityId;
		this.teamCode = teamCode;
		this.useYn = useYn;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getAuthorityId() {
		return authorityId;
	}

	public void setAuthorityId(int authorityId) {
		this.authorityId = authorityId;
	}

	public int getMauthorityId() {
		return mauthorityId;
	}

	public void setMauthorityId(int mauthorityId) {
		this.mauthorityId = mauthorityId;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	@Override
	public String toString() {
		return "AuthorityMember [mid=" + mid + ", authorityId=" + authorityId + ", mauthorityId=" + mauthorityId
				+ ", teamCode=" + teamCode + ", useYn=" + useYn + "]";
	}
	
	
}
