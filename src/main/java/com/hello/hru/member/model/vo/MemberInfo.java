package com.hello.hru.member.model.vo;

public class MemberInfo {
	private int mid;
	private String mName;
	private String email;
	private String enterDate;
	private String outDate;
	private int salaryType;
	private int salaryAmount;
	private String ssn;
	private String korean;
	private String country;
	private String phone;
	private String address;
	private int workStatus;
	private String teamName;
	private String groupName;
	private String jobName;
	private String enterName;
	private String responsibilityName;
	private String positionName;
	private String ppName;
	private int attachmentId;
	private String changeName;
	
	public MemberInfo() {}

	public MemberInfo(int mid, String mName, String email, String enterDate, String outDate, int salaryType,
			int salaryAmount, String ssn, String korean, String country, String phone, String address, int workStatus,
			String teamName, String groupName, String jobName, String enterName, String responsibilityName,
			String positionName, String ppName, int attachmentId, String changeName) {
		super();
		this.mid = mid;
		this.mName = mName;
		this.email = email;
		this.enterDate = enterDate;
		this.outDate = outDate;
		this.salaryType = salaryType;
		this.salaryAmount = salaryAmount;
		this.ssn = ssn;
		this.korean = korean;
		this.country = country;
		this.phone = phone;
		this.address = address;
		this.workStatus = workStatus;
		this.teamName = teamName;
		this.groupName = groupName;
		this.jobName = jobName;
		this.enterName = enterName;
		this.responsibilityName = responsibilityName;
		this.positionName = positionName;
		this.ppName = ppName;
		this.attachmentId = attachmentId;
		this.changeName = changeName;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEnterDate() {
		return enterDate;
	}

	public void setEnterDate(String enterDate) {
		this.enterDate = enterDate;
	}

	public String getOutDate() {
		return outDate;
	}

	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}

	public int getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(int salaryType) {
		this.salaryType = salaryType;
	}

	public int getSalaryAmount() {
		return salaryAmount;
	}

	public void setSalaryAmount(int salaryAmount) {
		this.salaryAmount = salaryAmount;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getKorean() {
		return korean;
	}

	public void setKorean(String korean) {
		this.korean = korean;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getWorkStatus() {
		return workStatus;
	}

	public void setWorkStatus(int workStatus) {
		this.workStatus = workStatus;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getEnterName() {
		return enterName;
	}

	public void setEnterName(String enterName) {
		this.enterName = enterName;
	}

	public String getResponsibilityName() {
		return responsibilityName;
	}

	public void setResponsibilityName(String responsibilityName) {
		this.responsibilityName = responsibilityName;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getPpName() {
		return ppName;
	}

	public void setPpName(String ppName) {
		this.ppName = ppName;
	}

	public int getattachmentId() {
		return attachmentId;
	}

	public void setattachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	@Override
	public String toString() {
		return "MemberInfo [mid=" + mid + ", mName=" + mName + ", email=" + email + ", enterDate=" + enterDate
				+ ", outDate=" + outDate + ", salaryType=" + salaryType + ", salaryAmount=" + salaryAmount + ", ssn="
				+ ssn + ", korean=" + korean + ", country=" + country + ", phone=" + phone + ", address=" + address
				+ ", workStatus=" + workStatus + ", teamName=" + teamName + ", groupName=" + groupName + ", jobName="
				+ jobName + ", enterName=" + enterName + ", responsibilityName=" + responsibilityName
				+ ", positionName=" + positionName + ", ppName=" + ppName + ", attachmentId=" + attachmentId + ", changeName="
				+ changeName + "]";
	}

}
