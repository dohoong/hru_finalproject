package com.hello.hru.member.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.hello.hru.member.model.vo.Authority;


public interface AuthorityService {

	ArrayList<Authority> selectListAuthority();

	int updateAuthority(ArrayList<Authority> list);
	
	String checkAuthority(int mid, int menuCode);
	
	ArrayList<HashMap<String, String>> checkAuthorityTeam(int mid, int menuCode);

	ArrayList<HashMap<String, Object>> selectListAuthMember();

	ArrayList<HashMap<String, Object>> selectListHrMember();

	int insertMemberAuthority(int teamCode, int hrmid, int acmid);

	int insertAuthorityNewMember(int mid, int type);

	ArrayList<HashMap<String, Object>> selectListTeamList();

	ArrayList<HashMap<String, Object>> selectCountList();

	ArrayList<HashMap<String, Object>> searchTeamList(int category, String text);

	ArrayList<HashMap<String, Object>> searchGlist();

	ArrayList<HashMap<String, Object>> selectLeaderTeam();

	ArrayList<HashMap<String, Object>> selectTmember(int mid1);

	




	
	
}
