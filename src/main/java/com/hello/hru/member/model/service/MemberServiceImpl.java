package com.hello.hru.member.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.hello.hru.approval.model.dao.ApprovalDao;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.common.Attachment;
import com.hello.hru.member.exception.LoginException;
import com.hello.hru.member.model.dao.MemberDao;
import com.hello.hru.member.model.vo.HrInfo;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberAdd;
import com.hello.hru.member.model.vo.MemberFamily;
import com.hello.hru.member.model.vo.MemberHrInfo;
import com.hello.hru.member.model.vo.MemberInfo;
import com.hello.hru.member.model.vo.TemporaryMember;
import com.hello.hru.salary.model.dao.SalaryDao;
import com.hello.hru.salary.model.vo.AccountStandard;
import com.hello.hru.system.model.vo.Team;

@Service
public class MemberServiceImpl implements MemberService {
	@Autowired
	private MemberDao md;
	@Autowired
	private ApprovalDao ad;
	@Autowired
	private SalaryDao sd;
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Override
	public Member loginMember(Member m) throws LoginException {
		Member loginUser = null;
		
		String encPassword = md.selectEncPassword(sqlSession, m);
		String initPass = "$2a$10$PqZbjF20jYv1V/Dfdlnsv.kPbfAOh4oifep7RduNcqs7DvuwsvAr."; //초기비밀번호
		//System.out.println(encPassword);
		
		if(passwordEncoder.matches(m.getmPwd(), encPassword)) {
			loginUser = md.selectMember(sqlSession, m);
			MemberHrInfo mhi = md.selectMemberHrInfo(sqlSession, loginUser.getMid());
			if(md.selectProfileAtt(sqlSession, loginUser.getMid()) != null) {
				mhi.setAtt(md.selectProfileAtt(sqlSession, loginUser.getMid()));
			}
			//System.out.println("service member HR info : " +mhi);
			if(passwordEncoder.matches(m.getmPwd(), initPass)) { //초기비밀번호랑 같으면,
				mhi.setInitYn(true);
			} else {
				mhi.setInitYn(false);
			}
			
			if(mhi != null) {
				loginUser.setMhi(mhi);
			}
		} else {
			throw new LoginException("로그인 실패!");
		}
		
		return loginUser;
	}

	@Override
	public int insertMember(TemporaryMember m, Approval app, ArrayList<Attachment> attlist) {
		int result = 0;
		
		int result1 = ad.insertApproval(sqlSession, app);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					//System.out.println("Service Apphis : " +apphis);
					result12 = ad.insertApprovalHistory(sqlSession, apphis);
				}
				m.setCategory(1);
			} else {
				result12 = 1;
				m.setCategory(3); //결재가 필요없을 경우라면, 카테고리를 3:승인으로 바꿈!
			}
		}
		m.setApprovalId(result1);
		int result2 = md.insertMember(sqlSession, m);
		
		int mid=0;
		if(m.getCategory()==3) { //결재가 필요없어서, 바로 승인나는 경우!
			mid = md.selectMemberMid(sqlSession, m);
		}
		
		// 임시사원 첨부파일 인서트!
		int result3 = 0;
		for(int i=0; i<attlist.size(); i++) {
			if(attlist.get(i).getCategory()==5) {
				if(mid == 0) { //임시사원으로 등록할 때,
					attlist.get(i).setTempMid(result2);
				} else { //결재없이 바로 임시사원에서 사원으로 등록할 때,
					attlist.get(i).setMid(mid);
				}
			} else {
				attlist.get(i).setApprovalId(result1);
			}
			result3 += md.insertAttachment(sqlSession, attlist.get(i));
		}
		
		//트랜젝션 처리!
		if(result12>0 && result2 >0 && result3>0) {
			result=1;
		} else {
			result=0;
		}
		
		return result;
		
	}


	@Override
	public HrInfo selectHRInformation() {
		HrInfo hi = new HrInfo();
		
		ArrayList<HashMap<String, String>> tlist = md.selectListTeam(sqlSession);
		hi.setTlist(tlist);
		
		ArrayList<HashMap<String, String>> glist = md.selectListGroup(sqlSession);
		hi.setGlist(glist);
		
		ArrayList<HashMap<String, String>> jlist = md.selectListJob(sqlSession);
		hi.setJlist(jlist);
		
		ArrayList<HashMap<String, String>> plist = md.selectListPosition(sqlSession);
		hi.setPlist(plist);
		
		ArrayList<HashMap<String, String>> pplist = md.selectListPrePosition(sqlSession);
		hi.setPplist(pplist);

		ArrayList<HashMap<String, String>> rlist = md.selectListResponsibility(sqlSession);
		hi.setRlist(rlist);
		
		ArrayList<HashMap<String, String>> elist = md.selectListEnterCategory(sqlSession);
		hi.setElist(elist);
		
		//System.out.println(hi);
		
		return hi;
	}

	@Override
	public HashMap<String, Object> selectTempMember(int approvalId) {
		HashMap<String, Object> tempMember = md.selectTempMember(sqlSession, approvalId);
		
		return tempMember;
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchMember(int category, String text) {
		
		ArrayList<HashMap<String, Object>> mlist = null;
		if(category==1) { //사원명 검색
			mlist = md.searchMemberName(sqlSession, text);
			
		} else { //부서명 검색
			mlist = md.searchMemberTeam(sqlSession, text);
		}
		
		
		return mlist;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListNewMember() {
		return md.selectListNewMember(sqlSession);
	}

	@Override
	public int updateInsuranceYN(HashMap<String, Object> mMap) {
		return md.updateInsuranceYN(sqlSession, mMap);
	}

	@Override
	public int updateBankInformation(HashMap<String, Object> bimap) {
		return md.updateBankInformation(sqlSession, bimap);
	}

	//사원 인적사항 조회
	@Override
	public MemberInfo selectMemberInfo(int mid) {
		return md.selectMemberInfo(sqlSession, mid);
	}

	//사원 가족사항 조회
	@Override
	public ArrayList<MemberFamily> selectMemberFamily(int mid) {
		return md.selectMemberFamily(sqlSession, mid);
	}

	@Override
	public int updateMemberInfo(MemberInfo mi, Attachment att, int attId) {
		int result = 0;
		int result1 = 0;
		int result2 = 0;
		int result3 = 0;
		int result4 = 0;
		
		if(att.getChangeName() != null) {
			result1 = md.insertAttachment(sqlSession, att);
		}
		result2 = md.updateMember(sqlSession, mi);
		result3 = md.updateMemberDetail(sqlSession, mi);
		
		if(result1 > 0 && result2 > 0 && result3 > 0) {
			result = 1;
		}
		
		if(attId > 0) {
			result4 = md.updateProfileAtt(sqlSession, attId);
			
			if(result4 == 0) {
				result = 0;
			}
		}
		
		return result;
	}

	@Override
	public int updatePassword(Member m) {
		return md.updatePassword(sqlSession, m);
	}

	@Override
	public int deleteFamily(int fid) {
		return md.deleteFamily(sqlSession, fid);
	}

	@Override
	public int insertFamily(MemberFamily mf) {
		return md.insertFamily(sqlSession, mf);
	}

	@Override
	public int updateFamily(ArrayList<MemberFamily> updateMfl) {
		int result = 0;
		for(MemberFamily mf : updateMfl) {
			result += md.updateFamily(sqlSession, mf); 
		}
		return result;
	}

	@Override
	public ArrayList<MemberAdd> selectMemberAdd(int mid) {
		return md.selectMemberAdd(sqlSession, mid);
	}

	@Override
	public int insertMemberAdd(MemberAdd ma) {
		return md.insertMemberAdd(sqlSession, ma);
	}

	@Override
	public int deleteMemberAdd(int maddId) {
		return md.deleteMemberAdd(sqlSession, maddId);
	}

	@Override
	public int updateMemberAdd(ArrayList<MemberAdd> updateList) {
		int result = 0;
		for(MemberAdd ma : updateList) {
			result += md.updateMemberAdd(sqlSession, ma);
		}
		return result;
	}
	//사원-셀프서비스-급여관리 (검색하기)
	@Override
	public ArrayList<HashMap<String, Object>> selectMemberSalary(HashMap<String, Object> search) {
		ArrayList<HashMap<String, Object>> sList = md.selectMemberSalary(sqlSession , search);
		return sList;
	}
	//사원-셀프서비스-급여관리 (급여상세목록 Modal 확인)
	@Override
	public ArrayList<HashMap<String, Object>> selectDateSalary(HashMap<String, Object> choice) {
		ArrayList<HashMap<String, Object>> sdList = md.selectDateSalary(sqlSession , choice);
		return sdList;
	}
	//사원-셀프서비스-급여관리(계좌관리)
	@Override
	public HashMap<String, Object> selectMemberSalaryAccount(int mid) {
		
		HashMap<String, Object> bList = md.selectMemberSalaryAccount(sqlSession , mid);
		return bList;
	}
	//사원-셀프서비스-퇴직금조회(페이지이동)
	@Override
	public HashMap<String, Object> selectMemberQuitBasic(int mid) {
		HashMap<String, Object> qList = md.selectMemberQuitBasic(sqlSession , mid);
		return qList;
		
	}
	//사원-셀프서비스-퇴직금계산(퇴직전 3개월 임금총액)
	@Override
	public ArrayList<HashMap<String, Object>> selectTotalMoneyList(int mid) {
		
		ArrayList<HashMap<String, Object>> mtList = new ArrayList<HashMap<String,Object>>();
		for(int i=2; i<5; i++) {
			HashMap<String, Object> search = new HashMap<String, Object>();
			search.put("mid", mid);
			search.put("i" , i);
			HashMap<String, Object> list = md.selectTotalMoneyList(search , sqlSession);
			int plusSalary = md.selectMonthPlusSalary(search , sqlSession);
			System.out.println(plusSalary);
			if(list != null) {
				list.put("plus", plusSalary);
			}
			mtList.add(list);
		}
		//System.out.println(mtList);
		return mtList;
	}
	
	//사원-셀프서비스-퇴직금계산(퇴직전 3개월 임금총액 - 연차수당 , 상여금)
	@Override
	public HashMap<String, Object> selectPlusSalaryList(int mid) {
		HashMap<String, Object> plusList = new HashMap<String, Object>();
		
		ArrayList<AccountStandard> as = sd.selectAllAccountStandard(sqlSession);
		
		//한 사원의 '급여정보' SELECT (은행 , 급여액 , 등등 )
		HashMap<String, Object> mSalary = sd.selectMemberSalaryInfo(sqlSession, mid);
		int plusSalaryCategory = 0;
		if(as.get(4).getAccountstd().equals("매달지급")) {
			plusSalaryCategory = 12;
		}else if(as.get(4).getAccountstd().equals("짝수달지급") || as.get(4).getAccountstd().equals("홀수달지급")) {
			plusSalaryCategory = 6;
		}
		//사원별 매달 기본급 ( 
		double basicSalary = (Double.parseDouble((String) mSalary.get("amount")) / (as.get(0).getStdpercent() + as.get(1).getStdpercent()) * 100);
		//사원별 매달 상여급
		double plusSalary = (basicSalary * (as.get(1).getStdpercent()/ 100 )) / plusSalaryCategory;
		double Total = basicSalary + plusSalary+ as.get(5).getAccountMoney();
		//사원별 통상임금
		double AvgSalary = Total / 209;
		HashMap<String, Object> search = new HashMap<String, Object>();
		search.put("mid", mid);
		search.put("avg" ,AvgSalary );
		
		plusList = md.selectPlusSalaryList(sqlSession , search);
		//System.out.println(plusList);
		//통상임금 추가
		plusList.put("avg", AvgSalary);
		return plusList;
	}

	@Override
	public HashMap<String, Object> selectYearEndBasic() {
		return md.selectYearEndBasic(sqlSession);
	}

	@Override
	public int insertYearEndInfo(HashMap<String, Object> yeMap) {
		return md.insertYearEndInfo(sqlSession, yeMap);
	}

	@Override
	public HashMap<String, Object> selectMemberYearEnd(int mid) {
		HashMap<String, Object> yearEnd = md.selectMemberYearEnd(sqlSession, mid);
		if(yearEnd==null) {
			md.insertMemberYearEnd(sqlSession, mid);
			yearEnd = md.selectMemberYearEnd(sqlSession, mid);
		}
		ArrayList<Attachment> attlist = md.selectAttachment(sqlSession, (Integer)yearEnd.get("yearendId"));
		yearEnd.put("attlist", attlist);
		
		return yearEnd;
	}

	@Override
	public int insertAttachmentYearEnd(Attachment att, Attachment att2) {
		int result = 0;
		if(att != null) {
			result += md.insertAttachmentYearEnd(sqlSession, att);
		}
		if(att2 != null) {
			result += md.insertAttachmentYearEnd(sqlSession, att2);
		}
		
		return result;
	}

	@Override
	public int updateAttachmentforYE(int attachmentId) {
		return md.updateAttachmentforYE(sqlSession, attachmentId);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectYearEndDocument(ArrayList<HashMap<String, String>> tlist) {
		ArrayList<HashMap<String, Object>> yeDoclist = new ArrayList<HashMap<String, Object>>();
		
		for(int i=0; i<tlist.size(); i++) {
			ArrayList<HashMap<String, Object>> templist = md.selectYearEndDocument(sqlSession, Integer.parseInt(tlist.get(i).get("teamCode")));
			
			for(int j=0; j<templist.size(); j++) {
				yeDoclist.add(templist.get(j));
			}
		}
		
		return yeDoclist;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectYearEndEtc(ArrayList<HashMap<String, String>> tlist) {
		ArrayList<HashMap<String, Object>> yeDoclist = new ArrayList<HashMap<String, Object>>();
		
		for(int i=0; i<tlist.size(); i++) {
			ArrayList<HashMap<String, Object>> templist = md.selectYearEndEtc(sqlSession, Integer.parseInt(tlist.get(i).get("teamCode")));
			
			for(int j=0; j<templist.size(); j++) {
				yeDoclist.add(templist.get(j));
			}
		}
		
		return yeDoclist;
    }
    
    @Override
	public ArrayList<HashMap<String, Object>> selectMemberList(ArrayList<HashMap<String, String>> tllist) {
		ArrayList<HashMap<String, Object>> memlist = new ArrayList<HashMap<String, Object>>();
		
		for(int i=0; i<tllist.size(); i++) {
			ArrayList<HashMap<String, Object>> templist = md.selectMemberList(sqlSession, Integer.parseInt(tllist.get(0).get("teamCode")));
			
			for(int j=0; j<templist.size(); j++) {
				memlist.add(templist.get(j));
			}
		}
		
		return memlist;
	}


    //사원-셀프서비스-퇴직금조회(퇴직신청)
	@Override
	public HashMap<String, Object> selectMemberQuitInfo(int mid) {
		
		HashMap<String, Object> mList = md.selectMemberQuitInfo(sqlSession , mid);
		
		return mList;
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchYearEnd(HashMap<String, Object> sMap) {
		return md.searchYearEnd(sqlSession, sMap);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectSearchTeamMember(HashMap<String, Object> search) {
		ArrayList<HashMap<String, Object>> ha = md.selectSearchTeamMember(sqlSession,search);
		return ha;
	}
	
}








