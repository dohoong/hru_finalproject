package com.hello.hru.member.model.vo;

public class MemberAdd {
	private int maddId;
	private int category;
	private String maddTitle;
	private String startDate;
	private String endDate;
	private String maddContent;
	private int mid;
	
	public MemberAdd () {}

	public MemberAdd(int maddId, int category, String maddTitle, String maddContent, String startDate, String endDate,
			int mid) {
		super();
		this.maddId = maddId;
		this.category = category;
		this.maddTitle = maddTitle;
		this.startDate = startDate;
		this.endDate = endDate;
		this.maddContent = maddContent;
		this.mid = mid;
	}

	public int getMaddId() {
		return maddId;
	}

	public void setMaddId(int maddId) {
		this.maddId = maddId;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getMaddTitle() {
		return maddTitle;
	}

	public void setMaddTitle(String maddTitle) {
		this.maddTitle = maddTitle;
	}

	public String getMaddContent() {
		return maddContent;
	}

	public void setMaddContent(String maddContent) {
		this.maddContent = maddContent;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	@Override
	public String toString() {
		return "MemberAdd [maddId=" + maddId + ", category=" + category + ", maddTitle=" + maddTitle + ", maddContent="
				+ maddContent + ", startDate=" + startDate + ", endDate=" + endDate + ", mid=" + mid + "]";
	}
	
}
