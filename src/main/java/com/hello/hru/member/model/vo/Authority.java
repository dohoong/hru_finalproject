package com.hello.hru.member.model.vo;

import java.util.ArrayList;

public class Authority {
	private int authorityId;
	private int menuCode;
	private String menuName;
	private int category;
	private String useYn;
	private ArrayList<Authority> list;
	
	public Authority() {}

	

	public Authority(int authorityId, int menuCode, String menuName, int category, String useYn,
			ArrayList<Authority> list) {
		super();
		this.authorityId = authorityId;
		this.menuCode = menuCode;
		this.menuName = menuName;
		this.category = category;
		this.useYn = useYn;
		this.list = list;
	}

	

	public int getAuthorityId() {
		return authorityId;
	}



	public void setAuthorityId(int authorityId) {
		this.authorityId = authorityId;
	}



	public int getMenuCode() {
		return menuCode;
	}



	public void setMenuCode(int menuCode) {
		this.menuCode = menuCode;
	}



	public String getMenuName() {
		return menuName;
	}



	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}



	public int getCategory() {
		return category;
	}



	public void setCategory(int category) {
		this.category = category;
	}



	public String getUseYn() {
		return useYn;
	}



	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}



	public ArrayList<Authority> getList() {
		return list;
	}



	public void setList(ArrayList<Authority> list) {
		this.list = list;
	}



	@Override
	public String toString() {
		return "Authority [authorityId=" + authorityId + ", menuCode=" + menuCode + ", menuName=" + menuName
				+ ", category=" + category + ", useYn=" + useYn + ", list=" + list + "]";
	}


	
	
}
