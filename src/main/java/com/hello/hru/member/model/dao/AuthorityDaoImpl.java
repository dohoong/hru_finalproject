package com.hello.hru.member.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.member.model.vo.Authority;
import com.hello.hru.member.model.vo.AuthorityMember;

@Repository
public class AuthorityDaoImpl implements AuthorityDao{

	@Override
	public ArrayList<Authority> selectListAuthority(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Authority.selectListAuthority");
	}

	@Override
	public ArrayList<Integer> searchHrLeaderList(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Authority.searchListHrLeader");
	}

	@Override
	public ArrayList<HashMap<Integer, Integer>> searchHrList(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Authority.searchListHrList");
	}

	@Override
	public ArrayList<HashMap<Integer, Integer>> searchAccounterList(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Authority.searchAccounterList");
	}

	@Override
	public ArrayList<Integer> searchLeaderList(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Authority.searchLeaderList");
	}

	@Override
	public ArrayList<Integer> searchAll(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Authority.searchAll");
	}

	@Override
	public int insertAuthority(SqlSessionTemplate sqlSession, HashMap<String, Integer> hmap) {
		return sqlSession.insert("Authority.insertAuthority", hmap);
	}

	@Override
	public int updateAuthority(SqlSessionTemplate sqlSession, Authority authority) {
		return sqlSession.update("Authority.updateAuthority", authority);
	}

	@Override
	public int updateAuthMember(SqlSessionTemplate sqlSession, int authorityId) {
		return sqlSession.update("Authority.updateAuthMember", authorityId);
	}

	@Override
	public int checkAuthority(SqlSessionTemplate sqlSession, HashMap<String, Integer> hmap) {
		return sqlSession.selectOne("Authority.checkAuthority", hmap);
	}

	@Override
	public ArrayList<HashMap<String, String>> selectListAuthorityTeam(SqlSessionTemplate sqlSession, HashMap<String, Integer> hmap) {
		return (ArrayList)sqlSession.selectList("Authority.checkAuthorityTeam", hmap);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListAuthMember(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Authority.selectListAuthMember");
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListHrMember(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Authority.selectListHrMember");
	}

	@Override
	public ArrayList<Integer> selectListAuthorityCategory(SqlSessionTemplate sqlSession, Authority auth) {
		return (ArrayList)sqlSession.selectList("Authority.selectListAuthorityCategory", auth);
	}

	@Override
	public int updateAuthorityTeam(SqlSessionTemplate sqlSession, int teamCode) {
		return sqlSession.update("Authority.updateAuthorityTeam", teamCode);
	}

	@Override
	public int insertAuthorityMember(SqlSessionTemplate sqlSession, AuthorityMember aum) {
		return sqlSession.insert("Authority.insertAuthorityMember", aum);
	}

	@Override
	public ArrayList<Integer> selectListAuthority2(SqlSessionTemplate sqlSession, int type) {
		return (ArrayList)sqlSession.selectList("Authority.selectListAuthority2", type);
	}

	@Override
	public int insertAuthorityMember2(SqlSessionTemplate sqlSession, AuthorityMember authm) {
		return sqlSession.insert("Authority.insertAuthorityMember2", authm);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListTeamList(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Authority.selectListTeamList");
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectCountList(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Authority.selectCountList");
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchTeamName(SqlSessionTemplate sqlSession, String text) {

		return (ArrayList)sqlSession.selectList("Authority.searchTeamName");
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchTeamCode(SqlSessionTemplate sqlSession, String text) {
		return (ArrayList)sqlSession.selectList("Authority.searchTeamCode");
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchGroupName(SqlSessionTemplate sqlSession, String text) {
		return (ArrayList)sqlSession.selectList("Authority.searchGroupName");
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchGlist(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Authority.searchGlist");
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectLeaderTeam(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Authority.selectLeaderTeam");
	}

    @Override
	public int updateAuthMemberDelete(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.update("Authority.updateAuthMemberDelete", mid);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectTmember(SqlSessionTemplate sqlSession, int mid1) {
		return (ArrayList)sqlSession.selectList("Authority.selectTmember",mid1);
	}


}
