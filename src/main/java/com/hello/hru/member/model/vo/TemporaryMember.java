package com.hello.hru.member.model.vo;

import java.sql.Date;

public class TemporaryMember {
	private int tempMid;
	private String tempName;
	private String tempEmail;
	private Date tempDate;
	private String tempKorean;
	private String tempCountry;
	private String tempSsn;
	private int approvalId;
	private int enterCode;
	private int salaryType;
	private int salaryAmount;
	private int teamCode;
	private int jobCode;
	private int positionCode;
	private int responsibilityCode;
	private int category;
	
	public TemporaryMember() {}

	public TemporaryMember(int tempMid, String tempName, String tempEmail, Date tempDate, String tempKorean,
			String tempCountry, String tempSsn, int approvalId, int enterCode, int salaryType, int salaryAmount,
			int teamCode, int jobCode, int positionCode, int responsibilityCode, int category) {
		super();
		this.tempMid = tempMid;
		this.tempName = tempName;
		this.tempEmail = tempEmail;
		this.tempDate = tempDate;
		this.tempKorean = tempKorean;
		this.tempCountry = tempCountry;
		this.tempSsn = tempSsn;
		this.approvalId = approvalId;
		this.enterCode = enterCode;
		this.salaryType = salaryType;
		this.salaryAmount = salaryAmount;
		this.teamCode = teamCode;
		this.jobCode = jobCode;
		this.positionCode = positionCode;
		this.responsibilityCode = responsibilityCode;
		this.category = category;
	}

	public int getTempMid() {
		return tempMid;
	}

	public void setTempMid(int tempMid) {
		this.tempMid = tempMid;
	}

	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	public String getTempEmail() {
		return tempEmail;
	}

	public void setTempEmail(String tempEmail) {
		this.tempEmail = tempEmail;
	}

	public Date getTempDate() {
		return tempDate;
	}

	public void setTempDate(Date tempDate) {
		this.tempDate = tempDate;
	}

	public String getTempKorean() {
		return tempKorean;
	}

	public void setTempKorean(String tempKorean) {
		this.tempKorean = tempKorean;
	}

	public String getTempCountry() {
		return tempCountry;
	}

	public void setTempCountry(String tempCountry) {
		this.tempCountry = tempCountry;
	}

	public String getTempSsn() {
		return tempSsn;
	}

	public void setTempSsn(String tempSsn) {
		this.tempSsn = tempSsn;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public int getEnterCode() {
		return enterCode;
	}

	public void setEnterCode(int enterCode) {
		this.enterCode = enterCode;
	}

	public int getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(int salaryType) {
		this.salaryType = salaryType;
	}

	public int getSalaryAmount() {
		return salaryAmount;
	}

	public void setSalaryAmount(int salaryAmount) {
		this.salaryAmount = salaryAmount;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	public int getJobCode() {
		return jobCode;
	}

	public void setJobCode(int jobCode) {
		this.jobCode = jobCode;
	}

	public int getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(int positionCode) {
		this.positionCode = positionCode;
	}

	public int getResponsibilityCode() {
		return responsibilityCode;
	}

	public void setResponsibilityCode(int responsibilityCode) {
		this.responsibilityCode = responsibilityCode;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "TemporaryMember [tempMid=" + tempMid + ", tempName=" + tempName + ", tempEmail=" + tempEmail
				+ ", tempDate=" + tempDate + ", tempKorean=" + tempKorean + ", tempCountry=" + tempCountry
				+ ", tempSsn=" + tempSsn + ", approvalId=" + approvalId + ", enterCode=" + enterCode + ", salaryType="
				+ salaryType + ", salaryAmount=" + salaryAmount + ", teamCode=" + teamCode + ", jobCode=" + jobCode
				+ ", positionCode=" + positionCode + ", responsibilityCode=" + responsibilityCode + ", category="
				+ category + "]";
	}
	
	
}
