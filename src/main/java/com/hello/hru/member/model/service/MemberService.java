package com.hello.hru.member.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.common.Attachment;
import com.hello.hru.member.exception.LoginException;
import com.hello.hru.member.model.vo.Authority;
import com.hello.hru.member.model.vo.HrInfo;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberAdd;
import com.hello.hru.member.model.vo.MemberFamily;
import com.hello.hru.member.model.vo.MemberInfo;
import com.hello.hru.member.model.vo.TemporaryMember;
import com.hello.hru.system.model.vo.Team;

public interface MemberService {
	
	Member loginMember(Member m) throws LoginException;
 
	int insertMember(TemporaryMember m, Approval app, ArrayList<Attachment> attlist);

	HrInfo selectHRInformation();

	HashMap<String, Object> selectTempMember(int approvalId);

	ArrayList<HashMap<String, Object>> searchMember(int category, String text);

	ArrayList<HashMap<String, Object>> selectListNewMember();

	int updateInsuranceYN(HashMap<String, Object> mMap);

	int updateBankInformation(HashMap<String, Object> bimap);

	MemberInfo selectMemberInfo(int mid);

	ArrayList<MemberFamily> selectMemberFamily(int mid);

	int updateMemberInfo(MemberInfo mi, Attachment att, int attId);

	int updatePassword(Member m);

	int deleteFamily(int fid);

	int insertFamily(MemberFamily mf);

	int updateFamily(ArrayList<MemberFamily> updateMfl);

	ArrayList<MemberAdd> selectMemberAdd(int mid);

	int insertMemberAdd(MemberAdd ma);

	int deleteMemberAdd(int maddId);

	int updateMemberAdd(ArrayList<MemberAdd> updateList);
	//사원-셀프서비스-급여관리 (검색하기)
	ArrayList<HashMap<String, Object>> selectMemberSalary(HashMap<String, Object> search);
	//사원-셀프서비스-급여관리 (급여상세목록 Modal 확인 )
	ArrayList<HashMap<String, Object>> selectDateSalary(HashMap<String, Object> choice);
	//사원-셀프서비스-급여관리 (계좌관리 탭 확인 )
	HashMap<String, Object> selectMemberSalaryAccount(int mid);
	//사원-셀프서비스-퇴직금조회(페이지이동)
	HashMap<String, Object> selectMemberQuitBasic(int mid);
	//사원-셀프서비스-퇴직금계산(퇴직전 3개월 임금총액 - 상여금 제외)
	ArrayList<HashMap<String, Object>> selectTotalMoneyList(int mid);
	//사원-셀프서비스-퇴직금계산(퇴직전 3개월 임금총액 - 연차수당 , 상여금)
	HashMap<String, Object> selectPlusSalaryList(int mid);
	//사원-셀프서비스-퇴직신청 (퇴직신청 ) 
	HashMap<String, Object> selectMemberQuitInfo(int mid);

	HashMap<String, Object> selectYearEndBasic();

	int insertYearEndInfo(HashMap<String, Object> yeMap);

	HashMap<String, Object> selectMemberYearEnd(int mid);

	int insertAttachmentYearEnd(Attachment att, Attachment att2);

	int updateAttachmentforYE(int attachmentId); 

	ArrayList<HashMap<String, Object>> selectYearEndDocument(ArrayList<HashMap<String, String>> tlist);

	ArrayList<HashMap<String, Object>> selectYearEndEtc(ArrayList<HashMap<String, String>> tlist);

	ArrayList<HashMap<String, Object>> searchYearEnd(HashMap<String, Object> sMap);

	ArrayList<HashMap<String, Object>> selectMemberList(ArrayList<HashMap<String, String>> tllist);

	ArrayList<HashMap<String, Object>> selectSearchTeamMember(HashMap<String, Object> search);




	




	

}
