package com.hello.hru.member.model.vo;

public class MemberFamily {
	private int fid;
	private int fCategory;
	private String fName;
	private String fSsn;
	private String togetherYN;
	private String deductionYN;
	private String insuranceYN;
	private int mid;
	
	public MemberFamily () {}

	public MemberFamily(int fid, int fCategory, String fName, String fSsn, String togetherYN, String deductionYN,
			String insuranceYN, int mid) {
		super();
		this.fid = fid;
		this.fCategory = fCategory;
		this.fName = fName;
		this.fSsn = fSsn;
		this.togetherYN = togetherYN;
		this.deductionYN = deductionYN;
		this.insuranceYN = insuranceYN;
		this.mid = mid;
	}

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public int getfCategory() {
		return fCategory;
	}

	public void setfCategory(int fCategory) {
		this.fCategory = fCategory;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getfSsn() {
		return fSsn;
	}

	public void setfSsn(String fSsn) {
		this.fSsn = fSsn;
	}

	public String getTogetherYN() {
		return togetherYN;
	}

	public void setTogetherYN(String togetherYN) {
		this.togetherYN = togetherYN;
	}

	public String getDeductionYN() {
		return deductionYN;
	}

	public void setDeductionYN(String deductionYN) {
		this.deductionYN = deductionYN;
	}

	public String getInsuranceYN() {
		return insuranceYN;
	}

	public void setInsuranceYN(String insuranceYN) {
		this.insuranceYN = insuranceYN;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	@Override
	public String toString() {
		return "MemberFamily [fid=" + fid + ", fCategory=" + fCategory + ", fName=" + fName + ", fSsn=" + fSsn
				+ ", togetherYN=" + togetherYN + ", deductionYN=" + deductionYN + ", insuranceYN=" + insuranceYN
				+ ", mid=" + mid + "]";
	}
	
	
	
}
