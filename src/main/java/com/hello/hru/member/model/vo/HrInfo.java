package com.hello.hru.member.model.vo;

import java.util.ArrayList;
import java.util.HashMap;

public class HrInfo {
	private ArrayList<HashMap<String, String>> tlist;
	private ArrayList<HashMap<String, String>> glist;
	private ArrayList<HashMap<String, String>> jlist;
	private ArrayList<HashMap<String, String>> plist;
	private ArrayList<HashMap<String, String>> pplist;
	private ArrayList<HashMap<String, String>> rlist;
	private ArrayList<HashMap<String, String>> elist;
	
	public HrInfo() {}

	public HrInfo(ArrayList<HashMap<String, String>> tlist, ArrayList<HashMap<String, String>> glist,
			ArrayList<HashMap<String, String>> jlist, ArrayList<HashMap<String, String>> plist,
			ArrayList<HashMap<String, String>> pplist, ArrayList<HashMap<String, String>> rlist,
			ArrayList<HashMap<String, String>> elist) {
		super();
		this.tlist = tlist;
		this.glist = glist;
		this.jlist = jlist;
		this.plist = plist;
		this.pplist = pplist;
		this.rlist = rlist;
		this.elist = elist;
	}

	public ArrayList<HashMap<String, String>> getTlist() {
		return tlist;
	}

	public void setTlist(ArrayList<HashMap<String, String>> tlist) {
		this.tlist = tlist;
	}

	public ArrayList<HashMap<String, String>> getGlist() {
		return glist;
	}

	public void setGlist(ArrayList<HashMap<String, String>> glist) {
		this.glist = glist;
	}

	public ArrayList<HashMap<String, String>> getJlist() {
		return jlist;
	}

	public void setJlist(ArrayList<HashMap<String, String>> jlist) {
		this.jlist = jlist;
	}

	public ArrayList<HashMap<String, String>> getPlist() {
		return plist;
	}

	public void setPlist(ArrayList<HashMap<String, String>> plist) {
		this.plist = plist;
	}

	public ArrayList<HashMap<String, String>> getPplist() {
		return pplist;
	}

	public void setPplist(ArrayList<HashMap<String, String>> pplist) {
		this.pplist = pplist;
	}

	public ArrayList<HashMap<String, String>> getRlist() {
		return rlist;
	}

	public void setRlist(ArrayList<HashMap<String, String>> rlist) {
		this.rlist = rlist;
	}

	public ArrayList<HashMap<String, String>> getElist() {
		return elist;
	}

	public void setElist(ArrayList<HashMap<String, String>> elist) {
		this.elist = elist;
	}

	@Override
	public String toString() {
		return "HrInfo [tlist=" + tlist + ", glist=" + glist + ", jlist=" + jlist + ", plist=" + plist + ", pplist="
				+ pplist + ", rlist=" + rlist + ", elist=" + elist + "]";
	}
	
	
	
}
