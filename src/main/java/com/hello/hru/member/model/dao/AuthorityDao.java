package com.hello.hru.member.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.member.model.vo.Authority;
import com.hello.hru.member.model.vo.AuthorityMember;

public interface AuthorityDao {

	ArrayList<Authority> selectListAuthority(SqlSessionTemplate sqlSession);

	ArrayList<Integer> searchHrLeaderList(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<Integer, Integer>> searchHrList(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<Integer, Integer>> searchAccounterList(SqlSessionTemplate sqlSession);

	ArrayList<Integer> searchLeaderList(SqlSessionTemplate sqlSession);

	ArrayList<Integer> searchAll(SqlSessionTemplate sqlSession);

	int insertAuthority(SqlSessionTemplate sqlSession, HashMap<String, Integer> hmap);

	int updateAuthority(SqlSessionTemplate sqlSession, Authority authority);

	int updateAuthMember(SqlSessionTemplate sqlSession, int authorityId);

	int checkAuthority(SqlSessionTemplate sqlSession, HashMap<String, Integer> hmap);

	ArrayList<HashMap<String, String>> selectListAuthorityTeam(SqlSessionTemplate sqlSession, HashMap<String, Integer> hmap);

	ArrayList<HashMap<String, Object>> selectListAuthMember(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, Object>> selectListHrMember(SqlSessionTemplate sqlSession);

	ArrayList<Integer> selectListAuthorityCategory(SqlSessionTemplate sqlSession, Authority auth);

	int updateAuthorityTeam(SqlSessionTemplate sqlSession, int teamCode);

	int insertAuthorityMember(SqlSessionTemplate sqlSession, AuthorityMember aum);

	ArrayList<Integer> selectListAuthority2(SqlSessionTemplate sqlSession, int type);

	int insertAuthorityMember2(SqlSessionTemplate sqlSession, AuthorityMember authm);
	
	//
	ArrayList<HashMap<String, Object>> selectListTeamList(SqlSessionTemplate sqlSession);
	
	ArrayList<HashMap<String, Object>> selectCountList(SqlSessionTemplate sqlSession);
	  
	ArrayList<HashMap<String, Object>> searchTeamName(SqlSessionTemplate sqlSession, String text);

	ArrayList<HashMap<String, Object>> searchTeamCode(SqlSessionTemplate sqlSession, String text);

	ArrayList<HashMap<String, Object>> searchGroupName(SqlSessionTemplate sqlSession, String text);

	ArrayList<HashMap<String, Object>> searchGlist(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, Object>> selectLeaderTeam(SqlSessionTemplate sqlSession);

	int updateAuthMemberDelete(SqlSessionTemplate sqlSession, int mid);
	
	ArrayList<HashMap<String, Object>> selectTmember(SqlSessionTemplate sqlSession, int mid1);




}
