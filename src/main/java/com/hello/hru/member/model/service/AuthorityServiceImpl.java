package com.hello.hru.member.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.member.model.dao.AuthorityDao;
import com.hello.hru.member.model.dao.MemberDao;
import com.hello.hru.member.model.vo.Authority;
import com.hello.hru.member.model.vo.AuthorityMember;
import com.hello.hru.member.model.vo.HrInfo;
import com.hello.hru.member.model.vo.MemberHrInfo;

@Service
public class AuthorityServiceImpl implements AuthorityService{
	@Autowired
	private AuthorityDao ad;
	@Autowired
	private MemberDao md;
	

	@Autowired
	private SqlSessionTemplate sqlSession;
	

	@Override
	public ArrayList<Authority> selectListAuthority() {
		return ad.selectListAuthority(sqlSession);
	}

	@Override
	public int updateAuthority(ArrayList<Authority> list) {
		int result = 0;
		
		ArrayList<Authority> prelist = ad.selectListAuthority(sqlSession);
		int result1 = 1;
		int result2 = 1;
		
		for(int i=0; i<prelist.size(); i++) {
			if(!prelist.get(i).getUseYn().equals(list.get(i).getUseYn())) { //변경된 값만!
				result2 = ad.updateAuthority(sqlSession, list.get(i));
				result1 = 0;
				//System.out.println("변경됐넹?! :" + list.get(i).getAuthorityId());
				
				if(list.get(i).getUseYn().equals("Y")) {	//권한이 추가되었을 때,
					if(list.get(i).getCategory()==1) { // 인사부서장의 권한이 변경되었을 때,
						ArrayList<Integer> hrleaders = ad.searchHrLeaderList(sqlSession);
						
						if(hrleaders != null) {
							for(int j=0; j<hrleaders.size();j++) {
								HashMap<String, Integer> hrleader = new HashMap<>();
								hrleader.put("mid", hrleaders.get(j));
								hrleader.put("authorityId", list.get(i).getAuthorityId());
								
								result1 += ad.insertAuthority(sqlSession, hrleader);
							}
						}
					} else if(list.get(i).getCategory()==2) { //인사담당자의 권한이 변경되었을 때,
						ArrayList<HashMap<Integer, Integer>> hrlist = ad.searchHrList(sqlSession);
						
						if(hrlist != null) {
							for(int j=0; j<hrlist.size(); j++) {
								HashMap<String, Integer> hr = new HashMap<>();
								hr.put("mid", hrlist.get(j).get("mid"));
								hr.put("authorityId", list.get(i).getAuthorityId());
								hr.put("teamCode", hrlist.get(j).get("teamCode"));
								
								result1 += ad.insertAuthority(sqlSession, hr);
							}
						}
						
					} else if(list.get(i).getCategory()==3) { // 급여단당자 권한이 변경되었을 때,
						ArrayList<HashMap<Integer, Integer>> accounterList = ad.searchAccounterList(sqlSession);
						
						if(accounterList != null) {
							for(int j=0; j<accounterList.size(); j++) {
								HashMap<String, Integer> account = new HashMap<>();
								account.put("mid", accounterList.get(j).get("mid"));
								account.put("authorityId", list.get(i).getAuthorityId());
								account.put("teamCode", accounterList.get(j).get("teamCode"));
								
								result1 += ad.insertAuthority(sqlSession, account);
							}
						}
						
						
					} else if(list.get(i).getCategory()==4) { //조직책임자 권한이 변경되었을 때,
						ArrayList<Integer> leaderList = ad.searchLeaderList(sqlSession);
						
						if(leaderList != null) {
							for(int j=0; j<leaderList.size(); j++) {
								HashMap<String, Integer> leader = new HashMap<>();
								leader.put("mid", leaderList.get(j));
								leader.put("authorityId", list.get(i).getAuthorityId());
								
								result1 += ad.insertAuthority(sqlSession, leader);
							}
						}
						
					} else { //사원권한이 변경되었을 때,
						ArrayList<Integer> mList = ad.searchAll(sqlSession);
						
						if(mList != null) {
							for(int j=0; j<mList.size(); j++) {
								HashMap<String, Integer> mhmap = new HashMap<>();
								mhmap.put("mid", mList.get(j));
								mhmap.put("authorityId", list.get(i).getAuthorityId());
								
								result1 += ad.insertAuthority(sqlSession, mhmap);
							}
						}
						
					}
				} else { //권한이 삭제되었을 때,
					result1 += ad.updateAuthMember(sqlSession, list.get(i).getAuthorityId());
				}
			} 
		}
		
		if(result1>0 && result2>0) {
			result = 1;
		}
		
		return result;
		
	}

	@Override
	public String checkAuthority(int mid, int menuCode) {
		//1. 사원에 대한 권한이 있는지
		//boolean authorityYn = ad.checkAuthority(sqlSession, mid, menuCode);
		String result = "N";
		if(mid == 1 || menuCode == 5) {
			result = "Y"; //대표자(CEO)이거나, 사원메뉴에 대한 권한조회라면, 무조건 YES!
		} else {
			
			HashMap<String, Integer> hmap = new HashMap<>();
			hmap.put("mid", mid);
			hmap.put("menuCode", menuCode);
			
			int num = ad.checkAuthority(sqlSession, hmap);
			
			if(num>0) {
				result = "Y";
			} else {
				result = "N";
			}
		}
		return result;
	}
	
	
	@Override
	public ArrayList<HashMap<String, String>> checkAuthorityTeam(int mid, int menuCode) {
		HrInfo hi = null;
		ArrayList<HashMap<String, String>> tlist = null;
		HashMap<String, String> mhrMap = md.selectMemberHR(sqlSession, mid); //해당 회원의 HR정보를 가져오기

		
		// CEO인지 판단하고
		if(mid == 1) {
			tlist = md.selectListTeam(sqlSession);
			
		} else if(mhrMap.get("rCode").equals("2")) {	//조직책임자일 경우
			if(mhrMap.get("teamCode").equals("2") && menuCode!=4) { //인사팀장일 때,
				tlist = md.selectListTeam(sqlSession);
			} else { // 조직책임자이거나 인사팀장이 조직책임자의 메뉴를 접근했을 때,
				tlist = new ArrayList<>();
				HashMap<String, String> tempMap = new HashMap<>();
				tempMap.put("teamCode", mhrMap.get("teamCode"));
				tempMap.put("teamName", mhrMap.get("teamName"));
				tempMap.put("groupCode", mhrMap.get("groupCode"));
				tempMap.put("groupName", mhrMap.get("groupName"));
				
				tlist.add(tempMap);
			}
		} else { // 일반 사원(조직책임자X)의 경우,
			//권한을 조회해야 한다.
			HashMap<String, Integer> hmap = new HashMap<>();
			hmap.put("mid", mid);
			hmap.put("menuCode", menuCode);
			tlist = ad.selectListAuthorityTeam(sqlSession, hmap);
		}
		
		//System.out.println("Service tlist : " + tlist);
		
		return tlist;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListAuthMember() {
		return ad.selectListAuthMember(sqlSession);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListHrMember() {
		return ad.selectListHrMember(sqlSession);
	}

	@Override
	public int insertMemberAuthority(int teamCode, int hrmid, int acmid) {
		int result = 0;

		Authority auth2 = new Authority();
		auth2.setCategory(2);
		Authority auth3 = new Authority();
		auth3.setCategory(3);
		
		//인사담당자에게 주어진 권한 조회
		ArrayList<Integer> authHRlist = ad.selectListAuthorityCategory(sqlSession, auth2);
		//회계담당자에게 주어진 권한 조회
		ArrayList<Integer> authAClist = ad.selectListAuthorityCategory(sqlSession, auth3);
		
		//이미 있는 권한 삭제
		int result1 = ad.updateAuthorityTeam(sqlSession, teamCode);
		
		int result2 = 0;
		int result3 = 0;
		
		//새로 주려는 권한 인서트
		for(int i=0; i<authHRlist.size(); i++) { //인사담당자에 대한 권한!
			AuthorityMember aum = new AuthorityMember();
			aum.setAuthorityId(authHRlist.get(i));
			aum.setMid(hrmid);
			aum.setTeamCode(teamCode);
			
			result2 += ad.insertAuthorityMember(sqlSession, aum);
		}
		for(int j=0; j<authAClist.size(); j++) { //회계담당자에 대한 권한!
			AuthorityMember aum2 = new AuthorityMember();
			aum2.setAuthorityId(authAClist.get(j));
			aum2.setMid(acmid);
			aum2.setTeamCode(teamCode);
			
			result3 += ad.insertAuthorityMember(sqlSession, aum2);
		}
		
		if(result1>0 && result2>0 && result3>0) {
			result = 1;
		}
		
		
		
		return result;
	}

	@Override
	public int insertAuthorityNewMember(int mid, int type) {
		int result=0;
		
		//해당 카테고리에 대한 권한 조회
		ArrayList<Integer> aulist = ad.selectListAuthority2(sqlSession, type);
		if(type==1) {
			for(int i=0; i<aulist.size();i++) { //인사팀장에 대한 권한임!
				AuthorityMember authm = new AuthorityMember();
				authm.setAuthorityId(aulist.get(i));
				authm.setMid(mid);
				result += ad.insertAuthorityMember2(sqlSession,authm);
			}
			
		} else {
			for(int i=0; i<aulist.size(); i++) { //인사부서 혹은 조직책임자에 대한 권한임!
				if(aulist.get(i)<25) {
					AuthorityMember authm = new AuthorityMember();
					authm.setAuthorityId(aulist.get(i));
					authm.setMid(mid);
					result += ad.insertAuthorityMember2(sqlSession,authm);
				}
			}
		}
		
		
		
		return result;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListTeamList(){
		return ad.selectListTeamList(sqlSession);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectCountList() {
		return ad.selectCountList(sqlSession);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchTeamList(int category, String text) {
		
		ArrayList<HashMap<String, Object>> tlist = null;
		
		if(category == 1) {
			tlist = ad.searchTeamName(sqlSession,text);
		}else if(category == 2) {
			tlist = ad.searchTeamCode(sqlSession,text);
		}else {
			tlist = ad.searchGroupName(sqlSession,text);
		}
				
				
		return null;
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchGlist() {
		
		return ad.searchGlist(sqlSession);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectLeaderTeam() {

		return ad.selectLeaderTeam(sqlSession);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectTmember(int mid1) {
		
		return ad.selectTmember(sqlSession,mid1);
	}




	
	
}















