package com.hello.hru.member.model.vo;

import java.sql.Date;

public class Member {
	
	private int mid;
	private String mPwd;
	private String mName;
	private String email;
	private Date enterDate;
	private String status;
	private Date outDate;
	private MemberHrInfo mhi;
	
	
	public Member() {}

	public Member(int mid, String mPwd, String mName, String email, Date enterDate, String status, Date outDate,
			MemberHrInfo mhi) {
		super();
		this.mid = mid;
		this.mPwd = mPwd;
		this.mName = mName;
		this.email = email;
		this.enterDate = enterDate;
		this.status = status;
		this.outDate = outDate;
		this.mhi = mhi;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getmPwd() {
		return mPwd;
	}

	public void setmPwd(String mPwd) {
		this.mPwd = mPwd;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEnterDate() {
		return enterDate;
	}

	public void setEnterDate(Date enterDate) {
		this.enterDate = enterDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getOutDate() {
		return outDate;
	}

	public void setOutDate(Date outDate) {
		this.outDate = outDate;
	}
	
	

	public MemberHrInfo getMhi() {
		return mhi;
	}



	public void setMhi(MemberHrInfo mhi) {
		this.mhi = mhi;
	}

	@Override
	public String toString() {
		return "Member [mid=" + mid + ", mPwd=" + mPwd + ", mName=" + mName + ", email=" + email + ", enterDate="
				+ enterDate + ", status=" + status + ", outDate=" + outDate + ", mhi=" + mhi + "]";
	}

	
	
	
	
}
