package com.hello.hru.member.model.dao;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.common.Attachment;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberAdd;
import com.hello.hru.member.model.vo.MemberFamily;
import com.hello.hru.member.model.vo.MemberHrInfo;
import com.hello.hru.member.model.vo.MemberInfo;
import com.hello.hru.member.model.vo.TemporaryMember;
import com.hello.hru.system.model.vo.Team;

@Repository
public class MemberDaoImpl implements MemberDao{

	@Override
	public String selectEncPassword(SqlSessionTemplate sqlSession, Member m) {
		return sqlSession.selectOne("Member.selectPwd", m.getMid());
	}

	@Override
	public Member selectMember(SqlSessionTemplate sqlSession, Member m) {
		return sqlSession.selectOne("Member.selectLoginUser", m);
	}

	@Override
	public int insertMember(SqlSessionTemplate sqlSession, TemporaryMember m) {
		
		sqlSession.insert("Member.insertTempMember", m);
		
		return m.getTempMid();
	}

	@Override
	public int insertAttachment(SqlSessionTemplate sqlSession, Attachment attachment) {
		return sqlSession.insert("Member.insertAttachment", attachment);
	}

	@Override
	public MemberHrInfo selectMemberHrInfo(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Member.selectMemberHrInfo", mid);
	}

	@Override
	public HashMap<String, String> selectMemberHR(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Member.selectMemberHR", mid);
	}



	@Override
	public ArrayList<HashMap<String, String>> selectListTeam(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Member.selectListTeam");
	}

	@Override
	public ArrayList<HashMap<String, String>> selectListGroup(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Member.selectListGroup");
	}

	@Override
	public ArrayList<HashMap<String, String>> selectListJob(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Member.selectListJob");
	}

	@Override
	public ArrayList<HashMap<String, String>> selectListPosition(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Member.selectListPosition");
	}

	@Override
	public ArrayList<HashMap<String, String>> selectListPrePosition(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Member.selectListPrePosition");
	}

	@Override
	public ArrayList<HashMap<String, String>> selectListResponsibility(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Member.selectListResponsibility");
	}

	@Override
	public ArrayList<HashMap<String, String>> selectListEnterCategory(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Member.selectListEnterCategory");
	}

	@Override
	public int selectMemberMid(SqlSessionTemplate sqlSession, TemporaryMember m) {
		return sqlSession.selectOne("Member.selectMemberMid", m);
	}

	@Override
	public HashMap<String, Object> selectTempMember(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.selectOne("Member.selectTempMember", approvalId);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchMemberName(SqlSessionTemplate sqlSession, String text) {
		return (ArrayList)sqlSession.selectList("Member.searchMemberName", text);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchMemberTeam(SqlSessionTemplate sqlSession, String text) {
		return (ArrayList)sqlSession.selectList("Member.searchMemberTeam", text);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListNewMember(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Member.selectListNewMember");
	}

	@Override
	public int updateInsuranceYN(SqlSessionTemplate sqlSession, HashMap<String, Object> mMap) {
		return sqlSession.update("Member.updateInsuranceYN", mMap);
	}

	@Override
	public int updateBankInformation(SqlSessionTemplate sqlSession, HashMap<String, Object> bimap) {
		return sqlSession.update("Member.updateBankInformation", bimap);
	}

	@Override
	public MemberInfo selectMemberInfo(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Member.selectMemberInfo", mid);
	}

	@Override
	public ArrayList<MemberFamily> selectMemberFamily(SqlSessionTemplate sqlSession, int mid) {
		return (ArrayList)sqlSession.selectList("Member.selectMemberFamily", mid);
	}

	@Override
	public int updateMember(SqlSessionTemplate sqlSession, MemberInfo mi) {
		return sqlSession.update("Member.updateMember", mi);
	}

	@Override
	public int updateMemberDetail(SqlSessionTemplate sqlSession, MemberInfo mi) {
		return sqlSession.update("Member.updateMemberDetail", mi);
	}

	@Override
	public int updateProfileAtt(SqlSessionTemplate sqlSession, int attId) {
		return sqlSession.update("Member.updateProfileAtt", attId);
	}

	@Override
	public Attachment selectProfileAtt(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Member.selectProfileAtt", mid);
	}

	@Override
	public int updatePassword(SqlSessionTemplate sqlSession, Member m) {
		return sqlSession.update("Member.updatePassword", m);
	}

	@Override
	public int deleteFamily(SqlSessionTemplate sqlSession, int fid) {
		return sqlSession.delete("Member.deleteFamily", fid);
	}

	@Override
	public int insertFamily(SqlSessionTemplate sqlSession, MemberFamily mf) {
		return sqlSession.insert("Member.insertFamily", mf);
	}

	@Override
	public int updateFamily(SqlSessionTemplate sqlSession, MemberFamily mf) {
		return sqlSession.update("Member.updateFamily", mf);
	}

	@Override
	public ArrayList<MemberAdd> selectMemberAdd(SqlSessionTemplate sqlSession, int mid) {
		return (ArrayList)sqlSession.selectList("Member.selectMemberAdd", mid);
	}

	@Override
	public int insertMemberAdd(SqlSessionTemplate sqlSession, MemberAdd ma) {
		return sqlSession.insert("Member.insertMemberAdd", ma);
	}

	@Override
	public int deleteMemberAdd(SqlSessionTemplate sqlSession, int maddId) {
		return sqlSession.delete("Member.deleteMemberAdd", maddId);
	}

	@Override
	public int updateMemberAdd(SqlSessionTemplate sqlSession,MemberAdd ma) {
		return sqlSession.update("Member.updateMemberAdd", ma);
	}

	//사원-셀프서비스-급여관리 (검색하기)
	@Override
	public ArrayList<HashMap<String, Object>> selectMemberSalary(SqlSessionTemplate sqlSession,
			HashMap<String, Object> search) {
		return (ArrayList) sqlSession.selectList("Member.selectMemberSalary" , search);
	}
	//사원-셀프서비스-급여관리(급여상세목록 Modal 확인 )
	@Override
	public ArrayList<HashMap<String, Object>> selectDateSalary(SqlSessionTemplate sqlSession,
			HashMap<String, Object> choice) {
		return (ArrayList) sqlSession.selectList("Member.selectDateSalary" , choice);
	}
	//사원-셀프서비스-급여관리(계좌관리)
	@Override
	public HashMap<String, Object> selectMemberSalaryAccount(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Member.selectMemberSalaryAccount", mid);
	}
	//사원-셀프서비스-퇴직금조회(페이지이동)
	@Override
	public HashMap<String, Object> selectMemberQuitBasic(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Member.selectMemberQuitBasic" , mid);
	}
	//사원-셀프서비스-퇴직금계산(퇴직전 3개월 임금총액)
	@Override
	public HashMap<String, Object> selectTotalMoneyList(HashMap<String, Object> search, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Member.selectTotalMoneyList", search);
	}
	//사원-셀프서비스-퇴직금계산(퇴직전 3개월 추가 수당 )
	@Override
	public int selectMonthPlusSalary(HashMap<String, Object> search, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Member.selectPlusMoney", search);
	}
	//사원-셀프서비스-퇴직금계산(연차수당 , 상여금)
	@Override
	public HashMap<String, Object> selectPlusSalaryList(SqlSessionTemplate sqlSession, HashMap<String, Object> search) {
		return sqlSession.selectOne("Member.selectPlusSalaryList" , search);
	}

	@Override
	public HashMap<String, Object> selectYearEndBasic(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Member.selectYearEndBasic");
	}

	@Override
	public int insertYearEndInfo(SqlSessionTemplate sqlSession, HashMap<String, Object> yeMap) {
		return sqlSession.insert("Member.insertYearEndBasic", yeMap);
	}

	@Override
	public HashMap<String, Object> selectMemberYearEnd(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Member.selectMemberYearEnd", mid);
	}

	@Override
	public int insertMemberYearEnd(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.insert("Member.insertMemberYearEnd", mid);
	}

	@Override
	public int insertAttachmentYearEnd(SqlSessionTemplate sqlSession, Attachment att) {
		return sqlSession.insert("Member.insertAttYearEnd", att);
	}

	@Override
	public ArrayList<Attachment> selectAttachment(SqlSessionTemplate sqlSession, int yearendId) {
		return (ArrayList)sqlSession.selectList("Member.selectYearEndAttach", yearendId);
	}

	@Override
	public int updateAttachmentforYE(SqlSessionTemplate sqlSession, int attachmentId) {
		return sqlSession.update("Member.updateAttachmentforYE", attachmentId);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectYearEndDocument(SqlSessionTemplate sqlSession, int teamCode) {
		return (ArrayList)sqlSession.selectList("Member.selectYearEndDocument", teamCode);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectYearEndEtc(SqlSessionTemplate sqlSession, int teamCode) {
		return (ArrayList)sqlSession.selectList("Member.selectYearEndEtc", teamCode);
    }

	@Override
	public ArrayList<HashMap<String, Object>> selectMemberList(SqlSessionTemplate sqlSession, int teamCode) {
		return (ArrayList)sqlSession.selectList("Member.selectMemberList", teamCode);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchLeaderMName(SqlSessionTemplate sqlSession, String text) {
		
		return (ArrayList)sqlSession.selectList("Member.searchLeaderMNaem",text);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchLeaderMPP(SqlSessionTemplate sqlSession, String text) {
		return (ArrayList)sqlSession.selectList("Member.searchLeaderMPP",text);
	}

	//사원-셀프서비스-퇴직금조회 (퇴직신청)
	@Override
	public HashMap<String, Object> selectMemberQuitInfo(SqlSessionTemplate sqlSession, int mid) {
		
		return sqlSession.selectOne("Member.selectMemberQuitInfo" , mid);
    }
    
	@Override
	public ArrayList<HashMap<String, Object>> searchYearEnd(SqlSessionTemplate sqlSession,
			HashMap<String, Object> sMap) {
		return (ArrayList)sqlSession.selectList("Member.searchYearEnd", sMap);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectSearchTeamMember(SqlSessionTemplate sqlSession, HashMap<String, Object> search) {
		ArrayList<HashMap<String, Object>> g = (ArrayList)sqlSession.selectList("Member.selectSearchTeamMember", search);
		
		return g;
	}

}
