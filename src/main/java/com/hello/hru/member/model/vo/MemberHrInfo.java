package com.hello.hru.member.model.vo;

import com.hello.hru.common.Attachment;

public class MemberHrInfo {
	private int mid;
	private String tname;
	private String gname;
	private String pname;
	private String ppname;
	private String jname;
	private String ename;
	private String rname;
	private Attachment att;
	private boolean initYn;
	
	public MemberHrInfo() {}
	
	public MemberHrInfo(int mid, String tname, String gname, String pname, String ppname, String jname, String ename,
			String rname, Attachment att, boolean initYn) {
		super();
		this.mid = mid;
		this.tname = tname;
		this.gname = gname;
		this.pname = pname;
		this.ppname = ppname;
		this.jname = jname;
		this.ename = ename;
		this.rname = rname;
		this.att = att;
		this.initYn = initYn;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getTname() {
		return tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	public String getGname() {
		return gname;
	}

	public void setGname(String gname) {
		this.gname = gname;
	}

	public String getJname() {
		return jname;
	}

	public void setJname(String jname) {
		this.jname = jname;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public Attachment getAtt() {
		return att;
	}

	public void setAtt(Attachment att) {
		this.att = att;
	}
	
	

	public String getPname() {
		return pname;
	}



	public void setPname(String pname) {
		this.pname = pname;
	}

	

	public String getPpname() {
		return ppname;
	}



	public void setPpname(String ppname) {
		this.ppname = ppname;
	}
	
	

	public boolean isInitYn() {
		return initYn;
	}

	public void setInitYn(boolean initYn) {
		this.initYn = initYn;
	}

	@Override
	public String toString() {
		return "MemberHrInfo [mid=" + mid + ", tname=" + tname + ", gname=" + gname + ", pname=" + pname + ", ppname="
				+ ppname + ", jname=" + jname + ", ename=" + ename + ", rname=" + rname + ", att=" + att + ", initYn="
				+ initYn + "]";
	}
	
	

}
