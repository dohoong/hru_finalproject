package com.hello.hru.member.model.dao;

import java.util.HashMap;
import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.common.Attachment;
import com.hello.hru.member.model.vo.Authority;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberAdd;
import com.hello.hru.member.model.vo.MemberFamily;
import com.hello.hru.member.model.vo.MemberHrInfo;
import com.hello.hru.member.model.vo.MemberInfo;
import com.hello.hru.member.model.vo.TemporaryMember;
import com.hello.hru.system.model.vo.Team;

public interface MemberDao {

	String selectEncPassword(SqlSessionTemplate sqlSession, Member m);

	Member selectMember(SqlSessionTemplate sqlSession, Member m);

	int insertMember(SqlSessionTemplate sqlSession, TemporaryMember m);


	int insertAttachment(SqlSessionTemplate sqlSession, Attachment attachment);

	MemberHrInfo selectMemberHrInfo(SqlSessionTemplate sqlSession, int mid);
	

	ArrayList<HashMap<String, String>> selectListTeam(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, String>> selectListGroup(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, String>> selectListJob(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, String>> selectListPosition(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, String>> selectListPrePosition(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, String>> selectListResponsibility(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, String>> selectListEnterCategory(SqlSessionTemplate sqlSession);

	HashMap<String, String> selectMemberHR(SqlSessionTemplate sqlSession, int mid);

	int selectMemberMid(SqlSessionTemplate sqlSession, TemporaryMember m);

	HashMap<String, Object> selectTempMember(SqlSessionTemplate sqlSession, int approvalId);

	ArrayList<HashMap<String, Object>> searchMemberName(SqlSessionTemplate sqlSession, String text);

	ArrayList<HashMap<String, Object>> searchMemberTeam(SqlSessionTemplate sqlSession, String text);

	ArrayList<HashMap<String, Object>> selectListNewMember(SqlSessionTemplate sqlSession);

	int updateInsuranceYN(SqlSessionTemplate sqlSession, HashMap<String, Object> mMap);

	int updateBankInformation(SqlSessionTemplate sqlSession, HashMap<String, Object> bimap);
	


	MemberInfo selectMemberInfo(SqlSessionTemplate sqlSession, int mid);

	ArrayList<MemberFamily> selectMemberFamily(SqlSessionTemplate sqlSession, int mid);

	int updateMember(SqlSessionTemplate sqlSession, MemberInfo mi);

	int updateMemberDetail(SqlSessionTemplate sqlSession, MemberInfo mi);

	int updateProfileAtt(SqlSessionTemplate sqlSession, int attId);

	Attachment selectProfileAtt(SqlSessionTemplate sqlSession, int mid);

	int updatePassword(SqlSessionTemplate sqlSession, Member m);

	int deleteFamily(SqlSessionTemplate sqlSession, int fid);

	int insertFamily(SqlSessionTemplate sqlSession, MemberFamily mf);

	int updateFamily(SqlSessionTemplate sqlSession, MemberFamily mf);

	ArrayList<MemberAdd> selectMemberAdd(SqlSessionTemplate sqlSession, int mid);

	int insertMemberAdd(SqlSessionTemplate sqlSession, MemberAdd ma);

	int deleteMemberAdd(SqlSessionTemplate sqlSession, int maddId);

	int updateMemberAdd(SqlSessionTemplate sqlSession, MemberAdd ma);
	//사원-셀프서비스-급여관리 (검색하기)
	ArrayList<HashMap<String, Object>> selectMemberSalary(SqlSessionTemplate sqlSession,
			HashMap<String, Object> search);
	//사원-셀프서비스-급여관리(급여상세목록 Modal 확인 )
	ArrayList<HashMap<String, Object>> selectDateSalary(SqlSessionTemplate sqlSession, HashMap<String, Object> choice);
	//사원-셀프서비스-급여관리(계좌관리)
	HashMap<String, Object> selectMemberSalaryAccount(SqlSessionTemplate sqlSession, int mid);
	//사원-셀프서비스-퇴직금조회(페이지이동)
	HashMap<String, Object> selectMemberQuitBasic(SqlSessionTemplate sqlSession, int mid);
	//사원-셀프서비스-퇴직금계산(퇴직전 3개월 임금총액)
	HashMap<String, Object> selectTotalMoneyList(HashMap<String, Object> search, SqlSessionTemplate sqlSession);
	//사원-셀프서비스-퇴직금계산(퇴직전 3개월 추가 수당 )
	int selectMonthPlusSalary(HashMap<String, Object> search, SqlSessionTemplate sqlSession);
	//사원-셀프서비스-퇴직금계산(연차수당 , 상여금)
	HashMap<String, Object> selectPlusSalaryList(SqlSessionTemplate sqlSession, HashMap<String, Object> search);

	HashMap<String, Object> selectYearEndBasic(SqlSessionTemplate sqlSession);

	int insertYearEndInfo(SqlSessionTemplate sqlSession, HashMap<String, Object> yeMap);

	HashMap<String, Object> selectMemberYearEnd(SqlSessionTemplate sqlSession, int mid);

	int insertMemberYearEnd(SqlSessionTemplate sqlSession, int mid);

	int insertAttachmentYearEnd(SqlSessionTemplate sqlSession, Attachment att);

	ArrayList<Attachment> selectAttachment(SqlSessionTemplate sqlSession, int yearendId);

	int updateAttachmentforYE(SqlSessionTemplate sqlSession, int attachmentId);

	ArrayList<HashMap<String, Object>> selectYearEndDocument(SqlSessionTemplate sqlSession, int teamCode);

	ArrayList<HashMap<String, Object>> selectYearEndEtc(SqlSessionTemplate sqlSession, int teamCode);

	ArrayList<HashMap<String, Object>> searchYearEnd(SqlSessionTemplate sqlSession, HashMap<String, Object> sMap);

	ArrayList<HashMap<String, Object>> selectMemberList(SqlSessionTemplate sqlSession, int teamCode);
	//사원-셀프서비스-퇴직금조회 (퇴직신청)
	HashMap<String, Object> selectMemberQuitInfo(SqlSessionTemplate sqlSession, int mid);

	ArrayList<HashMap<String, Object>> searchLeaderMName(SqlSessionTemplate sqlSession, String text);

	ArrayList<HashMap<String, Object>> searchLeaderMPP(SqlSessionTemplate sqlSession, String text);

	ArrayList<HashMap<String, Object>> selectSearchTeamMember(SqlSessionTemplate sqlSession,HashMap<String, Object> search);










}
