package com.hello.hru.member.model.vo;

public class SearchMember {
	private int mid;
	private String mName;
	private String positionName;
	private String teamName;
	
	public SearchMember(int mid, String mName, String positionName, String teamName) {
		super();
		this.mid = mid;
		this.mName = mName;
		this.positionName = positionName;
		this.teamName = teamName;
	}

	public SearchMember() {
		super();
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	@Override
	public String toString() {
		return "SearchMember [mid=" + mid + ", mName=" + mName + ", positionName=" + positionName + ", teamName="
				+ teamName + "]";
	}
	
	
	
	
	
}
