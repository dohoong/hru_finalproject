package com.hello.hru.member.controller;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.service.MemberService;
import com.hello.hru.member.model.vo.Authority;
import com.hello.hru.member.model.vo.HrInfo;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.system.model.vo.Team;

@Controller
public class AuthorityController {
	@Autowired
	private AuthorityService as;
	@Autowired
	private MemberService ms;
	
	@RequestMapping("selectList.au")
	public String selectListAuthority(Model model) {
		//System.out.println("권한조회!");
		
		ArrayList<Authority> alist = as.selectListAuthority();
		
		model.addAttribute("alist", alist);
		
		return "system/authorityAdministration";
	}
	
	@RequestMapping("update.au")
	public String updateAuthority(Authority auth) {
		//ModelAndView mv = new ModelAndView();
		
		
		for(int i=0; i<auth.getList().size(); i++) {
			if(auth.getList().get(i).getAuthorityId()>=21 && auth.getList().get(i).getAuthorityId()<=25) {
				//사원에 대한 권한은 뺏을 수 없음!
				auth.getList().get(i).setUseYn("Y");
			}
			
			if(auth.getList().get(i).getUseYn() != null) {
				auth.getList().get(i).setUseYn("Y");
			} else {
				auth.getList().get(i).setUseYn("N");
			}
			
			//System.out.println(auth.getList().get(i));
		}
		
		as.updateAuthority(auth.getList());
		
		
		return "redirect:/selectList.au";
	}
	
	
	@RequestMapping("checkMember.au")
	public ModelAndView checkAuthority(HttpServletRequest request, ModelAndView mv) {
		
		//System.out.println("menu id : " + request.getParameter("id"));
		
		String id = request.getParameter("id");
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		int menuCode = 0;
		String authorityYn = "";
		switch(id) {
			case "system" : menuCode=1; break;
			case "hr" : menuCode=2; break;
			case "account" : menuCode=3; break;
			case "leader" : menuCode=4; break;
			case "member" : menuCode=5; break;
			default : authorityYn="N";
		}
		//System.out.println(menuCode);
		authorityYn = as.checkAuthority(mid, menuCode);
		
		mv.addObject("authorityYn", authorityYn);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	@RequestMapping("showAuthorityMember.au")
	public String showAuthorityMgt(HttpServletRequest request, Model model) {
	
		int mid=0;
		if(request.getSession().getAttribute("loginUser")!=null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		String authority = as.checkAuthority(mid, 4);
		if(authority.equals("Y")) {
			
			ArrayList<HashMap<String, Object>> aulist = as.selectListAuthMember();
			ArrayList<HashMap<String, Object>> mlist = as.selectListHrMember();
			
			//System.out.println(aulist);
			model.addAttribute("aulist", aulist);
			model.addAttribute("mlist", mlist);
			
			return "humanResource/authorityManagement";

		} else {
			return "common/noAuthority";
		}
	}
	
	@RequestMapping("showTeamList.au")
	public String showTeamList(HttpServletRequest request, Model model) {		
			int mid =0;
			
			if(request.getSession().getAttribute("loginUser") !=null) {
				mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
			}
			
			
			String auto = as.checkAuthority(mid, 12);
			
			if(auto.equals("Y")) {
				ArrayList<HashMap<String, String>> aulist =as.checkAuthorityTeam(mid, 12);
				ArrayList<HashMap<String, Object>> tllist = as.selectListTeamList();
				
				System.out.println("tllist" + tllist);
				System.out.println("aulist" + aulist);
				model.addAttribute("aulist",aulist);
				model.addAttribute("tllist", tllist);
				
				
				return"humanResource/teamList";
			
			}else {
				return "common/errorPage";
			}
			
	}
	@RequestMapping("searchteamList.au")
	public String searchteamList(HttpServletRequest request, Model model) {
		
		int category = Integer.parseInt(request.getParameter("searchTeamGroup"));
		String text = request.getParameter("text");
		
		System.out.println("category :" + category);
		System.out.println("text" + text);
		
		
		ArrayList<HashMap<String, Object>> teamlist = as.searchTeamList(category,text);
		
		System.out.println(teamlist);
		
		model.addAttribute("teamlist", teamlist);
		
		
		return "redirect:/showTeamList.au";
	}
	

	@RequestMapping("search.me")
	public ModelAndView searchMember(HttpServletRequest request, ModelAndView mv) {
		
		int category = Integer.parseInt(request.getParameter("category"));
		String text = request.getParameter("text");
		
		ArrayList<HashMap<String, Object>> mlist = ms.searchMember(category, text);
		
		//System.out.println("mlist : " +mlist);
		
		mv.addObject("mlist", mlist);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("insertMember.au")
	public String insertMemberAuthority(HttpServletRequest request) {
		
		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		int hrmid = Integer.parseInt(request.getParameter("hrmid"));
		int acmid = Integer.parseInt(request.getParameter("acmid"));
		
		//System.out.println("teamCode : " +teamCode);
		//System.out.println("hrmid : " + hrmid);
		//System.out.println("acmid : " + acmid);
		
		int result = as.insertMemberAuthority(teamCode, hrmid, acmid);
		
		
		return "redirect:/showAuthorityMember.au";
	}
	

	@RequestMapping("insertAuthorityMember.au")
	public String insertAuthorityMember(HttpServletRequest request) {
		int mid = Integer.parseInt(request.getParameter("mid"));
		int type = Integer.parseInt(request.getParameter("type"));
		
		
		int result = as.insertAuthorityNewMember(mid, type);
		
		return "redirect:/showNewMemberManagement.me";
	}
	
	
	
	@RequestMapping("searchTeamMember.au")
	public String searchTeamMember(HttpServletRequest request, Model model) {
		
		HashMap<String, Object> search = new HashMap<String, Object>();
		
		search.put("category", Integer.parseInt(request.getParameter("category")));
		search.put("searchVal",request.getParameter("searchVal"));
		search.put("teamCode", Integer.parseInt(request.getParameter("teamCode")));
		System.out.println(search);
		
		ArrayList<HashMap<String, Object>> memlist = ms.selectSearchTeamMember(search);

		
		model.addAttribute("memlist",memlist);
		
		
		return "leader/organizationManagement";
	}
	
	
	
}














