package com.hello.hru.member.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.member.model.service.MemberService;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberAdd;
import com.hello.hru.member.model.vo.MemberFamily;

@Controller
public class MemberInfoController {
	
	@Autowired
	private MemberService ms;
	
	
	//사원-셀프서비스-퇴직신청 (사원 정보가져오기)
	@PostMapping("selectMemberQuitInfo.me")
	public ModelAndView selectMemberQuitInfo(ModelAndView mv , HttpServletRequest request) {
		
		int mid = Integer.parseInt(request.getParameter("mid"));
		
		HashMap<String, Object> mList = ms.selectMemberQuitInfo(mid);
		
		mv.addObject("mList", mList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	
	//사원-셀프서비스-퇴직금 ( + 보수월액 추가 ) 
	@PostMapping("selectMemberQuitMoney.me")
	public ModelAndView selectMemberQuitMoney(ModelAndView mv , HttpServletRequest request) {
		
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		//사원 정보
		HashMap<String , Object> qList = ms.selectMemberQuitBasic(mid);
		
		//3개월 임금총액 계산내역 
		ArrayList<HashMap<String, Object>> tMoneyList = ms.selectTotalMoneyList(mid);
		//연차수당 , 상여금
		HashMap<String, Object> plusSalary = ms.selectPlusSalaryList(mid);
		System.out.println("3개월 임금총액 : " + tMoneyList);
		System.out.println("연차수당 , 상여금  : "+ plusSalary);
		//평균임금
		
		int avgMoney = 0;
		int avgDate = 0;
		for(int i =0; i<tMoneyList.size(); i++) {
			//기본급 + 수당
			avgMoney += (int) tMoneyList.get(i).get("money");
			avgMoney += (int) tMoneyList.get(i).get("plus");
			//3개월 날짜 계산
			avgDate += (int) tMoneyList.get(i).get("minus");
		}
		// + 연차수당
		avgMoney +=  ((int)plusSalary.get("vacation")) * 0.25;
		// + 상여금
		avgMoney += ((int)plusSalary.get("plus")) * 0.25;
		
		
		//퇴직금 계산
		HashMap<String, Object> qSalary = new HashMap<String, Object>();
		//평균임금
		int resultAvg = avgMoney / avgDate;
		//1일 통상임금
		int ordinarySalary = (int) ((double) plusSalary.get("avg") * 8);
		//퇴직금
		double quitSalary = 0;
		//평균임금이 통상임금보다 높을 때
		if(resultAvg > ordinarySalary) {
			quitSalary = Math.round(((int)qList.get("employeeDate") / (double) 365) * 30 * resultAvg);
		}
		//통상임금이 평균임금보다 높을 때
		else {
			quitSalary = Math.round(((int)qList.get("employeeDate") / (double) 365) * 30 * ordinarySalary);
		}
		
		
		System.out.println("1일 평균임금 : " + resultAvg);
		System.out.println("1시간통상임금 :" + plusSalary.get("avg"));
		System.out.println("1일통산임금 : " + ordinarySalary);
		System.out.println("퇴직금 : " + quitSalary);
		
		//1일 평균임금 , 1일통상임금 , 퇴직금
		qSalary.put("avgSalary", resultAvg);
		qSalary.put("ordinarySalary" ,ordinarySalary);
		qSalary.put("quitSalary" , quitSalary);
		
		
		//퇴직금 계산
		mv.addObject("qSalary" , qSalary);
		//연차수당 , 상여금
		mv.addObject("plusSalary", plusSalary);
		//3개월 임금총액 계산내역 
		mv.addObject("tMoneyList", tMoneyList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//사원-셀프서비스-퇴직금조회 (화면이동)
	@RequestMapping("showInfoQuitMoney.me")
	public String showInfoQuitMoney(Model model , HttpServletRequest request) {
		
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		
		HashMap<String , Object> qList = ms.selectMemberQuitBasic(mid);
		
		System.out.println(qList);
		if(qList !=null) {
			model.addAttribute("qList" , qList);
			return "member/infoQuitMoney";
		}else {
			model.addAttribute("msg" , "퇴직금조회실패");
			return "common/errorPage";
		}
		
	}
	
	//사원-셀프서비스-급여관리(사원 계좌 목록 select )
	@PostMapping("selectMemberSalary.me")
	public ModelAndView selectMemberSalaryAccount(HttpServletRequest request , ModelAndView mv) {
		
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		
		HashMap<String, Object> bList = ms.selectMemberSalaryAccount(mid);
		
		mv.addObject("bList", bList);
		mv.setViewName("jsonView");
		return mv;
	}
	//사원-셀프서비스-급여관리(급여상세목록 Modal 확인 )
	@PostMapping("selectDateSalary.me")
	public ModelAndView selectDateSalary(HttpServletRequest request , ModelAndView mv) {
		
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		String date = request.getParameter("date");
		
		Date selectDate = Date.valueOf(date);
		
		HashMap<String , Object> choice = new HashMap<String, Object>();
		choice.put("mid", mid);
		choice.put("date" , date);
		
		ArrayList<HashMap<String, Object>> sdList = ms.selectDateSalary(choice);
		
		System.out.println(sdList);
		
		mv.addObject("sdList", sdList);
		mv.setViewName("jsonView");
		return mv;
		
	}
	
	//사원-셀프서비스-급여관리(검색하기)
	@PostMapping("selectSalaryList.me")
	public ModelAndView selectMemberSalary(HttpServletRequest request ,  ModelAndView mv) {
		
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		String date1 = request.getParameter("searchDate1");
		String date2= request.getParameter("searchDate2");
		
		Date searchDate1 = Date.valueOf(date1);
		Date searchDate2 = Date.valueOf(date2);
		
		
		HashMap<String, Object> search = new HashMap<String, Object>();
		search.put("mid", mid);
		search.put("searchDate1" , searchDate1);
		search.put("searchDate2" , searchDate2);
		
		System.out.println(search);
		ArrayList<HashMap<String, Object>> sList = ms.selectMemberSalary(search);
		
		
		System.out.println(sList);
		
		mv.addObject("sList", sList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("updateFamily.me")
	public ModelAndView updateFamily(HttpServletRequest request, MemberFamily mf, ModelAndView mv) {
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		int result = 0;
		ArrayList<MemberFamily> updateMfl = new ArrayList<> ();
		
		String[] fid = request.getParameterValues("fid");
		String[] fname = request.getParameterValues("fname");
		String[] fCategory = request.getParameterValues("fCategory");
		String[] fssn1 = request.getParameterValues("fssn1");
		String[] fssn2 = request.getParameterValues("fssn2");
		String[] together = request.getParameterValues("together");
		String[] deduction = request.getParameterValues("deduction");
		String[] insurance = request.getParameterValues("insurance");
		
		if(fid != null) {
			for(int i = 0; i<fid.length; i++) {
				mf.setFid(Integer.parseInt(fid[i]));
				mf.setfName(fname[i]);
				mf.setfCategory(Integer.parseInt(fCategory[i]));
				mf.setfSsn(fssn1[i] + "-" + fssn2[i]);
				mf.setTogetherYN(together[i]);
				mf.setDeductionYN(deduction[i]);
				mf.setInsuranceYN(insurance[i]);
				
				updateMfl.add(mf);
			}
			result = ms.updateFamily(updateMfl);
		}
		
		ArrayList<MemberFamily> mfList = ms.selectMemberFamily(mid);
		
		mv.addObject("mfList", mfList);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	@RequestMapping("addFamily.me")
	public ModelAndView insertFamily(HttpServletRequest request, MemberFamily mf, ModelAndView mv) {
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		int result = 0;
		
		mf.setfName(request.getParameter("name"));
		mf.setfCategory(Integer.parseInt(request.getParameter("category")));
		mf.setfSsn(request.getParameter("ssn1") + "-" + request.getParameter("ssn2"));
		mf.setMid(mid);
		if(request.getParameter("together") != null) {
			mf.setTogetherYN("Y");
		}else {
			mf.setTogetherYN("N");
		}
		if(request.getParameter("deduction") != null) {
			mf.setDeductionYN("Y");
		}else {
			mf.setDeductionYN("N");
		}
		if(request.getParameter("insurance") != null) {
			mf.setInsuranceYN("Y");
		}else {
			mf.setInsuranceYN("N");
		}
		
		result = ms.insertFamily(mf);
		
		ArrayList<MemberFamily> mfList = ms.selectMemberFamily(mid);
		
		mv.addObject("mfList", mfList);
		mv.setViewName("jsonView");
		 
		return mv;
	}
	
	@RequestMapping("removeFamily.me")
	public ModelAndView deleteFamily(HttpServletRequest request, ModelAndView mv) {
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		int result = 0;
		result = ms.deleteFamily(Integer.parseInt(request.getParameter("fid")));
		if(result > 0) {
			//System.out.println("삭제완료");
		}else {
			//System.out.println("삭제실패");
		}
		
		ArrayList<MemberFamily> mfList = ms.selectMemberFamily(mid);
		
		mv.addObject("mfList", mfList);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	@RequestMapping("showInfoCareer.me")
	public String selectMemberAdd(HttpServletRequest request, Model m) {
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		ArrayList<MemberAdd> maList = new ArrayList<>();
		ArrayList<MemberAdd> eduList = new ArrayList<>();
		ArrayList<MemberAdd> careerList = new ArrayList<>();
		ArrayList<MemberAdd> cerfiList = new ArrayList<>();
		ArrayList<MemberAdd> languageList = new ArrayList<>();
		
		maList = ms.selectMemberAdd(mid);
		
		if(maList.size() > 0) {
			for(MemberAdd ma : maList) {
				switch(ma.getCategory()) {
					case 1: eduList.add(ma); break;
					case 2: careerList.add(ma); break;
					case 3: cerfiList.add(ma); break;
					case 4: languageList.add(ma); break;
				}
			}
		}
		
		m.addAttribute("eduList", eduList);
		m.addAttribute("careerList", careerList);
		m.addAttribute("cerfiList", cerfiList);
		m.addAttribute("languageList", languageList);

		return "member/infoCareer";
	}
	
	@RequestMapping("insertMemberAdd.me")
	public ModelAndView insertMemberAdd(HttpServletRequest request, MemberAdd ma, ModelAndView mv) {
		ArrayList<MemberAdd> maList = new ArrayList<>();
		ArrayList<MemberAdd> eduList = new ArrayList<>();
		ArrayList<MemberAdd> careerList = new ArrayList<>();
		ArrayList<MemberAdd> cerfiList = new ArrayList<>();
		ArrayList<MemberAdd> languageList = new ArrayList<>();
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		int result = 0;
		
		ma.setMid(mid);
		ma.setCategory(Integer.parseInt(request.getParameter("category")));
		ma.setMaddTitle(request.getParameter("maddTitle"));
		ma.setMaddContent(request.getParameter("maddContent"));
		ma.setStartDate(request.getParameter("startYear") + "-" + request.getParameter("startMonth"));
		ma.setEndDate(request.getParameter("endYear") + "-" + request.getParameter("endMonth"));
		
		result = ms.insertMemberAdd(ma);
		
		
		maList = ms.selectMemberAdd(mid);
		
		if(maList.size() > 0) {
			for(MemberAdd m : maList) {
				switch(m.getCategory()) {
					case 1: eduList.add(m); break;
					case 2: careerList.add(m); break;
					case 3: cerfiList.add(m); break;
					case 4: languageList.add(m); break;
				}
			}
		}
	
		mv.addObject("eduList", eduList);
		mv.addObject("careerList", careerList);
		mv.addObject("cerfiList", cerfiList);
		mv.addObject("languageList", languageList);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	@RequestMapping("deleteMemberAdd.me")
	public ModelAndView deleteMemberAdd(HttpServletRequest request, ModelAndView mv) {
		ArrayList<MemberAdd> maList = new ArrayList<>();
		ArrayList<MemberAdd> eduList = new ArrayList<>();
		ArrayList<MemberAdd> careerList = new ArrayList<>();
		ArrayList<MemberAdd> cerfiList = new ArrayList<>();
		ArrayList<MemberAdd> languageList = new ArrayList<>();
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		int result = 0;
		int maddId = 0;
		
		maddId = Integer.parseInt(request.getParameter("maddId"));

		result = ms.deleteMemberAdd(maddId);
		
		maList = ms.selectMemberAdd(mid);
		
		if(maList.size() > 0) {
			for(MemberAdd m : maList) {
				switch(m.getCategory()) {
					case 1: eduList.add(m); break;
					case 2: careerList.add(m); break;
					case 3: cerfiList.add(m); break;
					case 4: languageList.add(m); break;
				}
			}
		}
	
		mv.addObject("eduList", eduList);
		mv.addObject("careerList", careerList);
		mv.addObject("cerfiList", cerfiList);
		mv.addObject("languageList", languageList);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	@RequestMapping("updateMemberAdd.me")
	public ModelAndView updateMemberAdd(HttpServletRequest request, MemberAdd ma, ModelAndView mv) {
		ArrayList<MemberAdd> maList = new ArrayList<>();
		ArrayList<MemberAdd> updateList = new ArrayList<>();
		ArrayList<MemberAdd> eduList = new ArrayList<>();
		ArrayList<MemberAdd> careerList = new ArrayList<>();
		ArrayList<MemberAdd> cerfiList = new ArrayList<>();
		ArrayList<MemberAdd> languageList = new ArrayList<>();
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		int result = 0;

		String[] maddId = request.getParameterValues("maddId");
		String[] title = request.getParameterValues("title");
		String[] content = request.getParameterValues("content");
		String[] startYear = request.getParameterValues("startYear");
		String[] startMonth = request.getParameterValues("startMonth");
		String[] endYear = request.getParameterValues("endYear");
		String[] endMonth = request.getParameterValues("endMonth");
		
		if(maddId != null) {
			for(int i = 0; i<maddId.length; i++) {
				ma.setMaddId(Integer.parseInt(maddId[i]));
				ma.setMaddTitle(title[i]);
				ma.setMaddContent(content[i]);
				ma.setStartDate(startYear[i] + "-" + startMonth[i]);
				ma.setEndDate(endYear[i] + "-" + endMonth[i]);
				
				updateList.add(ma);
			}
			result = ms.updateMemberAdd(updateList);
		}
		
		maList = ms.selectMemberAdd(mid);
		
		if(maList.size() > 0) {
			for(MemberAdd m : maList) {
				switch(m.getCategory()) {
					case 1: eduList.add(m); break;
					case 2: careerList.add(m); break;
					case 3: cerfiList.add(m); break;
					case 4: languageList.add(m); break;
				}
			}
		}
	
		mv.addObject("eduList", eduList);
		mv.addObject("careerList", careerList);
		mv.addObject("cerfiList", cerfiList);
		mv.addObject("languageList", languageList);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	@PostMapping("searchYE.me")
	public ModelAndView searchYearendDocu(HttpServletRequest request, ModelAndView mv) {
		int year = Integer.parseInt(request.getParameter("year"));
		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		String mName = request.getParameter("mName");
		int type = Integer.parseInt(request.getParameter("type"));
		
		HashMap<String, Object> sMap = new HashMap<>();
		sMap.put("year", year);
		sMap.put("teamCode", teamCode);
		sMap.put("mName", mName);
		sMap.put("type", type);
		System.out.println("Controller :::: " + sMap);

		ArrayList<HashMap<String, Object>> yelist = ms.searchYearEnd(sMap);
		
		mv.addObject("yelist", yelist);
		mv.setViewName("jsonView");
		return mv;
		
	}
}
