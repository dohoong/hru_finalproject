package com.hello.hru.member.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import org.apache.tools.ant.taskdefs.condition.Http;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.approval.model.service.ApprovalService;
import com.hello.hru.approval.model.vo.Alarm;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.common.ApprovalLine;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.CommonsUtils;
import com.hello.hru.common.PageInfo;
import com.hello.hru.common.Pagination;
import com.hello.hru.common.model.service.CheckService;
import com.hello.hru.member.exception.LoginException;
import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.service.MemberService;
import com.hello.hru.member.model.vo.HrInfo;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberFamily;
import com.hello.hru.member.model.vo.MemberInfo;
import com.hello.hru.member.model.vo.TemporaryMember;
import com.hello.hru.system.model.vo.Team;

@SessionAttributes("loginUser")
@Controller
public class MemberController {
	///
	 // private final MemberService ms;
	 // 
	 // public MemberController(MemberService ms) { this.ms = ms; }
	 ///
	@Autowired
	private MemberService ms;
	@Autowired
	private AuthorityService as;
	@Autowired
	private CheckService cs;
	@Autowired
	private ApprovalService aps;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Scheduled(cron = "0 30 19 ? * *")
	public void checkForBatch() {
		Date now = new Date();
		System.out.println("스케쥴러 작동 : " + now);
		cs.callBack();
	}

	@RequestMapping("login.me")
	public String loginCheck(Member m, Model model, HttpServletRequest request) {
		Member loginUser;
		try {
			loginUser = ms.loginMember(m);
			ArrayList<Alarm> alist = aps.selectAlarm(m.getMid());
			model.addAttribute("loginUser", loginUser);
			model.addAttribute("alist", alist);

			PageInfo pi = Pagination.getPageInfo(1, 10);
			ArrayList<HashMap<String, Object>> applist = aps.selectListAllApprovalRequest(m.getMid(), pi);
			model.addAttribute("applist", applist);

			request.getSession().setAttribute("alarmCnt", alist.size());
			if (loginUser.getMhi().isInitYn()) { // 초기비밀번호일 때,
				return "redirect:/showInfoMember.me";
			} else { // 초기비밀번호 아닐때
				return "main/main";
			}

		} catch (LoginException e) {
			model.addAttribute("msg", e.getMessage());

			return "common/errorPage";
		}
	}

	@RequestMapping("logout.me")
	public String logout(SessionStatus status) {
		status.setComplete();

		return "redirect:index.jsp";
	}

	@PostMapping("insert.me")
	public String insertMember(Model model, TemporaryMember m, ApprovalBasic ab, HttpServletRequest request,
			@RequestParam(name = "contractFile") MultipartFile file,
			@RequestParam(name = "resumeFile") MultipartFile file2,
			@RequestParam(name = "attestedFile") MultipartFile file3,
			@RequestParam(name = "appFile") MultipartFile appFile) {

		m.setTempDate(java.sql.Date.valueOf(request.getParameter("startDate")));
		String ssn1 = request.getParameter("tempSsn");
		String ssn2 = request.getParameter("tempSsn2");
		m.setTempSsn(ssn1 + "-" + ssn2);

		// System.out.println("Controller member : " + m);

		// 전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		// System.out.println("ApprovalBasic : " + ab);

		Approval app = new ApprovalLine().makingApprovalLine(1, mid, title, ab.getFirstApproval(),
				ab.getSecondApproval(), ab.getThirdApproval());
		// System.out.println(app);

		// 파일에 대한 정보
		String root = request.getSession().getServletContext().getRealPath("resources");

		String os = System.getProperty("os.name").toLowerCase();
		System.out.println(os);
		String sl = "";
		if (os.equals("mac os x")) {
			sl = "/";
		} else {
			sl = "\\";
		}

		// 첨부파일
		ArrayList<Attachment> attlist = new ArrayList<>();

		String filePath = root + sl + "uploadFiles";
		try {
			if (!file.getOriginalFilename().equals("")) {
				String originFileName = file.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();

				Attachment att = new Attachment();
				att.setOriginName(originFileName);
				att.setChangeName(changeName);
				att.setPath(filePath);
				att.setCategory(5);
				att.setFileLevel(2);

				attlist.add(att);
				file.transferTo(new File(filePath + sl + changeName + ext));
			}
			if (!file2.getOriginalFilename().equals("")) {
				String originFileName = file2.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();

				Attachment att = new Attachment();
				att.setOriginName(originFileName);
				att.setChangeName(changeName);
				att.setPath(filePath);
				att.setCategory(5);
				att.setFileLevel(3);

				attlist.add(att);
				file2.transferTo(new File(filePath + sl + changeName + ext));
			}
			if (!file3.getOriginalFilename().equals("")) {
				String originFileName = file3.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();

				Attachment att = new Attachment();
				att.setOriginName(originFileName);
				att.setChangeName(changeName);
				att.setPath(filePath);
				att.setCategory(5);
				att.setFileLevel(4);

				attlist.add(att);
				file3.transferTo(new File(filePath + sl + changeName + ext));
			}
			if (!appFile.getOriginalFilename().equals("")) { // 전자결재 첨부파일!
				String originFileName = appFile.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();

				Attachment att = new Attachment();
				att.setOriginName(originFileName);
				att.setChangeName(changeName);
				att.setPath(filePath);
				att.setCategory(6);
				att.setFileLevel(0);

				attlist.add(att);
				appFile.transferTo(new File(filePath + sl + changeName + ext));
			}

			ms.insertMember(m, app, attlist);

			return "redirect:/showMemberInsert.me";

		} catch (IllegalStateException | IOException e) {
			// new File(filePath + sl + changeName + ext).delete();

			model.addAttribute("msg", "파일업로드에 실패하였습니다.");

			return "common/errorPage";
		}

	}

	@RequestMapping("showMemberInsert.me")
	public String showMember(Model model, HttpServletRequest request) {

		int mid = 0;
		if (request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		}

		String authority = as.checkAuthority(mid, 10);

		// System.out.println("권한은 있나요?? " + authority);

		if (authority.equals("Y")) {
			HrInfo hi = ms.selectHRInformation();

			// 권한을 가지고 있는 팀, 사업장 코드와 이름을 가져오기!
			ArrayList<HashMap<String, String>> tlist = as.checkAuthorityTeam(mid, 10);

			hi.setTlist(new ArrayList<HashMap<String, String>>());
			hi.setGlist(new ArrayList<HashMap<String, String>>());
			for (int j = 0; j < tlist.size(); j++) {
				HashMap<String, String> hmap2 = new HashMap<>();
				hi.getTlist().add(tlist.get(j));

				hmap2.put("groupCode", tlist.get(j).get("groupCode"));
				hmap2.put("groupName", tlist.get(j).get("groupName"));
				if (j > 0) {
					for (int k = 0; k < hi.getGlist().size(); k++) {
						if (!hmap2.get("groupCode").equals(hi.getGlist().get(k).get("groupCode"))) {
							hi.getGlist().add(hmap2);
						}
					}
				} else {
					hi.getGlist().add(hmap2);
				}
			}

			model.addAttribute("hi", hi);

			return "humanResource/memberInsert";
		} else {

			return "common/noAuthority";

		}
	}

	@GetMapping("showNewMemberManagement.me")
	public String showNewMemberManagement(Model model) {

		// System.out.println("신입사원 조회합시다!");

		ArrayList<HashMap<String, Object>> mlist = ms.selectListNewMember();
		// System.out.println(mlist);

		model.addAttribute("mlist", mlist);

		return "humanResource/newMemberManagement";
	}

	@GetMapping("updateInsurance.me")
	public String updateInsuranceYN(HttpServletRequest request) {
		String insYn = request.getParameter("insYn");
		//System.out.println("insYn : " + insYn);
		int mid = Integer.parseInt(request.getParameter("mid"));
		HashMap<String, Object> mMap = new HashMap<>();
		mMap.put("mid", mid);
		if(insYn.equals("true")) {
			mMap.put("insYn", "Y");
		} else {
			mMap.put("insYn", "N");
		}
		//System.out.println(mMap);
		int result = ms.updateInsuranceYN(mMap);
		
		return "redirect:/showNewMemberManagement.me";
	}

	@PostMapping("updateBankInfo.me")
	public String updateBankInfo(HttpServletRequest request) {

		int mid = Integer.parseInt(request.getParameter("mid"));
		int bankNum = Integer.parseInt(request.getParameter("bankNum"));
		String bankName = "";
		switch (bankNum) {
		case 0:
			bankName = "신한은행";
			break;
		case 1:
			bankName = "우리은행";
			break;
		case 2:
			bankName = "국민은행";
			break;
		case 3:
			bankName = "하나은행";
			break;
		case 4:
			bankName = "카카오뱅크";
			break;
		}

		String accountNum = request.getParameter("accountNumber");

		HashMap<String, Object> bimap = new HashMap<>();
		bimap.put("mid", mid);
		bimap.put("bankName", bankName);
		bimap.put("accountNum", accountNum);

		int result = ms.updateBankInformation(bimap);

		return "redirect:/showNewMemberManagement.me";
	}

	@RequestMapping("showInfoMember.me")
	public String showInfoMember(HttpServletRequest request, Model m) {
		if (request.getSession().getAttribute("loginUser") != null) {
			int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();

			MemberInfo mi = ms.selectMemberInfo(mid);
			ArrayList<MemberFamily> mfList = ms.selectMemberFamily(mid);

			m.addAttribute("mi", mi);
			m.addAttribute("mfList", mfList);
		}
		return "member/infoMember";
	}

	@RequestMapping("updateMemberInfo.me")
	public String updateMemberInfo(HttpServletRequest request, MemberInfo mi,
			@RequestParam(name = "imgFile") MultipartFile file) {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		int mid = Integer.parseInt(request.getParameter("mid"));
		int attId = 0;
		if (request.getParameter("attId") != null) {
			attId = Integer.parseInt(request.getParameter("attId"));
		}

		mi.setMid(mid);
		mi.setmName(request.getParameter("name"));
		mi.setSsn(request.getParameter("ssn1") + "-" + request.getParameter("ssn2"));
		mi.setPhone(request.getParameter("phone1") + "-" + request.getParameter("phone2") + "-"
				+ request.getParameter("phone3"));
		mi.setEmail(request.getParameter("email"));
		mi.setKorean(request.getParameter("korean"));
		mi.setCountry(request.getParameter("country"));
		mi.setAddress(request.getParameter("address"));

		String root = request.getSession().getServletContext().getRealPath("resources");

		String os = System.getProperty("os.name").toLowerCase();
		System.out.println(os);
		String sl = "";
		if (os.equals("mac os x")) {
			sl = "/";
		} else {
			sl = "\\";
		}
		// 첨부파일
		Attachment att = new Attachment();

		String filePath = root + sl + "uploadFiles";
		try {
			if (!file.getOriginalFilename().equals("")) {
				String originFileName = file.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();

				att.setMid(mid);
				att.setOriginName(originFileName);
				att.setChangeName(changeName + ext);
				att.setPath(filePath);
				att.setCategory(4);
				att.setFileLevel(0);

				file.transferTo(new File(filePath + sl + changeName + ext));
				loginUser.getMhi().setAtt(att);
				request.getSession().setAttribute("loginUser", loginUser);
			}

			ms.updateMemberInfo(mi, att, attId);

			return "redirect:/showInfoMember.me";

		} catch (IllegalStateException | IOException e) {

			System.out.println("파일업로드에 실패하였습니다.");

			return "redirect:/showInfoMember.me";
		}
	}
	
	
	
	@RequestMapping("showOrganizationManagement.me")
	public String showOrganizationManagement(HttpServletRequest request, Model model) {
		int mid = 0;
		if (request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		}
		
		String authority = as.checkAuthority(mid, 4);
		
		if (authority.equals("Y")) {
			ArrayList<HashMap<String, String>> tllist = as.checkAuthorityTeam(mid, 4);
			ArrayList<HashMap<String, Object>> memlist = ms.selectMemberList(tllist);

			System.out.println(tllist);
			System.out.println(memlist);
			/// model.addAttribute("Mlist",Mlist); ///
			model.addAttribute("tllist", tllist);
			model.addAttribute("memlist", memlist);
			return "leader/organizationManagement";
		} else {
			return "common/errorPage";
		}

	}

	@PostMapping("updatePassword.me")
	public String updatePassword(HttpServletRequest request, Member m) {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		int result = 0;

		m.setMid(((Member) request.getSession().getAttribute("loginUser")).getMid());
		m.setmPwd(passwordEncoder.encode(request.getParameter("pass1")));

		loginUser.getMhi().setInitYn(false);
		request.getSession().setAttribute("loginUser", loginUser);

		result = ms.updatePassword(m);

		if (result > 0) {
			return "redirect:/showInfoMember.me";
		} else {
			return "common/errorPage";
		}

	}

	@RequestMapping(value = "fileDownload.me", method = RequestMethod.GET)
	public ModelAndView documentDown(HttpServletRequest request) {

		int attid = Integer.parseInt(request.getParameter("attid"));

		Attachment attachment = aps.selectAttachment(attid);

		return new ModelAndView("downloadView", "attachment", attachment);
	}

	@RequestMapping("showMemberView.me")
	public String showMemberView(Model model, HttpServletRequest request) {
		int mid = 0;
		if (request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		}
		ArrayList<Alarm> alist = aps.selectAlarm(mid);
		// model.addAttribute("loginUser", loginUser);
		model.addAttribute("alist", alist);

		PageInfo pi = Pagination.getPageInfo(1, 10);
		ArrayList<HashMap<String, Object>> applist = aps.selectListAllApprovalRequest(mid, pi);
		model.addAttribute("applist", applist);

		request.getSession().setAttribute("alarmCnt", alist.size());
		return "main/main";
	}

	@GetMapping("showYearEndAccount.me")
	public String showYearEndAccount(Model model) {

		HashMap<String, Object> yeMap = ms.selectYearEndBasic();

		model.addAttribute("yeMap", yeMap);
		return "account/yearEndAccount";
	}

	@GetMapping("showYearEndDocument.me")
	public String showYearEndDocument(Model model, HttpServletRequest request) {
		int mid = 0;
		if (request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		}
		String authority = as.checkAuthority(mid, 3);
		// System.out.println("권한은 있나요?? " + authority);

		ArrayList<HashMap<String, Object>> yeDoculist = null;
		if (authority.equals("Y")) {
			// 권한을 가지고 있는 팀, 사업장 코드와 이름을 가져오기!
			ArrayList<HashMap<String, String>> tlist = as.checkAuthorityTeam(mid, 3);
			model.addAttribute("tlist", tlist);
			yeDoculist = ms.selectYearEndDocument(tlist);
		}

		model.addAttribute("yeDoculist", yeDoculist);
		return "account/yearEndDocument";
	}

	@GetMapping("showYearEndEtcDocument.me")
	public String showYearEndEtcDocument(Model model, HttpServletRequest request) {
		int mid = 0;
		if (request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		}
		String authority = as.checkAuthority(mid, 3);
		// System.out.println("권한은 있나요?? " + authority);
		ArrayList<HashMap<String, Object>> yeEtclist = null;
		if (authority.equals("Y")) {
			ArrayList<HashMap<String, String>> tlist = as.checkAuthorityTeam(mid, 3);
			model.addAttribute("tlist", tlist);
			yeEtclist = ms.selectYearEndEtc(tlist);

		}

		model.addAttribute("yeEtclist", yeEtclist);
		return "account/yearEndEtcDocument";
	}

	@PostMapping("storeYearEnd.me")
	public String storeyearEnd(HttpServletRequest request) {
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String documentStart = request.getParameter("documentStart");
		String documentEnd = request.getParameter("documentEnd");
		HashMap<String, Object> yeMap = new HashMap<>();
		yeMap.put("startDate", startDate);
		yeMap.put("endDate", endDate);
		yeMap.put("documentStart", documentStart);
		yeMap.put("documentEnd", documentEnd);

		int result = ms.insertYearEndInfo(yeMap);

		return "redirect:/showYearEndAccount.me";
	}

	@GetMapping("showYearEndCalculator.me")
	public String showYearEndCalculator(Model model, HttpServletRequest request) {
		int mid = 0;
		if (request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		}

		HashMap<String, Object> yeMap = ms.selectYearEndBasic();
		HashMap<String, Object> yearEnd = ms.selectMemberYearEnd(mid);

		model.addAttribute("yearEnd", yearEnd);
		model.addAttribute("yeMap", yeMap);
		System.out.println("yearEnd : " + yearEnd);
		return "member/yearEndCalculator";
	}

	@PostMapping("insertDocument.me")
	public String insertDocument(HttpServletRequest request, @RequestParam(name = "inputFile") MultipartFile file,
			@RequestParam(name = "inputFile2") MultipartFile file2) {
		int yearendId = Integer.parseInt(request.getParameter("yearendId"));

		System.out.println("yearendId : " + yearendId);
		try {
			String root = request.getSession().getServletContext().getRealPath("resources");

			String os = System.getProperty("os.name").toLowerCase();
			System.out.println(os);
			String sl = "";
			if (os.equals("mac os x")) {
				sl = "/";
			} else {
				sl = "\\";
			}
			// 첨부파일
			Attachment att = null;
			Attachment att2 = null;

			String filePath = root + sl + "uploadFiles";

			if (!file.getOriginalFilename().equals("")) {
				att = new Attachment();
				String originFileName = file.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();

				att.setOriginName(originFileName);
				att.setChangeName(changeName);
				att.setPath(filePath);
				att.setCategory(1);
				att.setFileLevel(1);
				att.setYearendId(yearendId);
				file.transferTo(new File(filePath + sl + changeName + ext));
			}
			if (!file2.getOriginalFilename().equals("")) {
				att2 = new Attachment();
				String originFileName = file2.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();

				att2.setOriginName(originFileName);
				att2.setChangeName(changeName);
				att2.setPath(filePath);
				att2.setCategory(1);
				att2.setFileLevel(0);
				att2.setYearendId(yearendId);
				file2.transferTo(new File(filePath + sl + changeName + ext));
			}

			System.out.println("::::::::::::::::::::::::::::::::::::::::::::");
			System.out.println("att1 : " + att);
			System.out.println("att2 : " + att2);
			int result = ms.insertAttachmentYearEnd(att, att2);

		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}

		return "redirect:/showYearEndCalculator.me";
	}

	@RequestMapping("deleteAttforYE.me")
	public String deleteAttachmentforYE(HttpServletRequest request, Model model) {
		int attachmentId = Integer.parseInt(request.getParameter("attachmentId"));

		int result = ms.updateAttachmentforYE(attachmentId);

		return "redirect:/showYearEndCalculator.me";
	}

}
