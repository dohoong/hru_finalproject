package com.hello.hru.board.model.service;

import java.util.ArrayList;

import com.hello.hru.board.exception.ApplyException;
import com.hello.hru.board.exception.DeleteException;
import com.hello.hru.board.exception.FilterException;
import com.hello.hru.board.exception.InsertException;
import com.hello.hru.board.exception.ReplyException;
import com.hello.hru.board.exception.SearchException;
import com.hello.hru.board.exception.SelectException;
import com.hello.hru.board.exception.SelectOneException;
import com.hello.hru.board.exception.UpdateException;
import com.hello.hru.board.model.vo.Proposal;
import com.hello.hru.board.model.vo.ProposalComment;
import com.hello.hru.board.model.vo.SearchCondition;
import com.hello.hru.board.model.vo.SelectOneSocialBoard;
import com.hello.hru.board.model.vo.SelectOneSocialBoard2;
import com.hello.hru.board.model.vo.SelectOneSocialBoard3;
import com.hello.hru.board.model.vo.SelectSocialMember;
import com.hello.hru.board.model.vo.SocialBoard;
import com.hello.hru.board.model.vo.SocialMember;
import com.hello.hru.common.PageInfo;

public interface BoardService {

	ArrayList<SocialBoard> selectSocailList(SocialBoard sb, PageInfo pi) throws SelectException;

	int insertSocialList(SocialBoard sb) throws InsertException;

	ArrayList<SelectSocialMember> selectSocialListInsert(SelectSocialMember ssm) throws SelectException;

	ArrayList<SelectOneSocialBoard> selectOneSocialList(int friendid, SelectOneSocialBoard ssb) throws SelectOneException;

	ArrayList<SelectOneSocialBoard2> selectOneSocialList2(SelectOneSocialBoard2 ssb2) throws SelectOneException;

	ArrayList<SelectOneSocialBoard3> selectOneSocialList3(SelectOneSocialBoard3 ssb3) throws SelectOneException;

	int applySocialList(SocialMember sm) throws ApplyException;

	ArrayList<SocialBoard> selectSocialListforWriter(SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SelectOneSocialBoard> selectOneSocialListforWriter(int friendid, SelectOneSocialBoard ssb) throws SelectOneException;

	ArrayList<SelectOneSocialBoard2> selectOneSocialListforWriter2(SelectOneSocialBoard2 ssb2) throws SelectOneException;

	int updateInfo(SelectOneSocialBoard ssb) throws UpdateException;

	int deleteInfo(int friendid) throws DeleteException;

	ArrayList<SocialBoard> searchSocialList(SearchCondition sc, PageInfo pi) throws SearchException;

	ArrayList<SocialBoard> searchSocialListforWriter(SearchCondition sc, PageInfo pi) throws SearchException;

	ArrayList<SocialBoard> filterSocialList(SearchCondition sc, PageInfo pi) throws FilterException;
	
	ArrayList<SocialBoard> filterSocialListforWriter(SearchCondition sc, PageInfo pi) throws FilterException;

	ArrayList<SocialBoard> selectSocailList2(SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SocialBoard> selectSocailList3(SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SocialBoard> selectSocailList4(SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SocialBoard> selectSocailList5(SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SocialBoard> selectSocailList6(SocialBoard sb, PageInfo pi) throws SelectException;

	int getListCount() throws SelectException;

	int getListCount2() throws SelectException;

	int getListCount3() throws SelectException;

	int getListCount4() throws SelectException;

	int getListCount5() throws SelectException;

	int getListCount6() throws SelectException;

	int getListCountforWriter(SocialBoard sb) throws SelectException;

	int getListCountforSearch(SearchCondition sc) throws SearchException;
 
	int getListCountforFilter(SearchCondition sc) throws FilterException;

	int getListCountforSearchWriter(SearchCondition sc) throws SearchException;

	int getListCountforFilterWriter(SearchCondition sc) throws FilterException;

	int updatePlusInfo(SelectOneSocialBoard2 ssb2) throws UpdateException;

	//ArrayList<SocialBoard> filterSocialListAllforWriter(SearchCondition sc) throws SelectException;

	ArrayList<Proposal> selectPbList(PageInfo pi) throws SelectException;

	int getListCountforPb() throws SelectException;

	ArrayList<Proposal> selectOnePbList(Proposal p) throws SelectOneException;

	Proposal selectOnePbListwithComment(Proposal p) throws SelectOneException;

	ArrayList<ProposalComment> selectOnePbListwithComment2(ProposalComment pc) throws SelectOneException;

	ArrayList<Proposal> selectInsertPbList(Proposal p) throws SelectException;

	int insertPbList(Proposal p) throws InsertException;

	int updateInfoforPb(Proposal p) throws UpdateException;

	int deleteInfoforPb(int proposalid) throws DeleteException;

	int getListCountPbforSearch(SearchCondition sc) throws SearchException;

	ArrayList<Proposal> searchPbList(SearchCondition sc, PageInfo pi) throws SearchException;

	int insertReply(ProposalComment pc) throws ReplyException;

	int updateReply(ProposalComment pc) throws UpdateException;

	int deleteReply(int procommentid) throws DeleteException;

	int cancelApply(SelectOneSocialBoard2 ssb2) throws UpdateException;


}
