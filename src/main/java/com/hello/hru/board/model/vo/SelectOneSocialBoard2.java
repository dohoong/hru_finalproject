package com.hello.hru.board.model.vo;

public class SelectOneSocialBoard2 implements java.io.Serializable{
	
	private int fmemberid;
	private int mid;
	private int friendid;
	private String phone;
	private String email;
	private String mname;
	private String ssn;
	private String plusinfo;
	private String applyyn;
	
	public SelectOneSocialBoard2() {
		
	}

	public SelectOneSocialBoard2(int fmemberid, int mid, int friendid, String phone, String email, String mname,
			String ssn, String plusinfo, String applyyn) {
		super();
		this.fmemberid = fmemberid;
		this.mid = mid;
		this.friendid = friendid;
		this.phone = phone;
		this.email = email;
		this.mname = mname;
		this.ssn = ssn;
		this.plusinfo = plusinfo;
		this.applyyn = applyyn;
	}

	public int getFmemberid() {
		return fmemberid;
	}

	public void setFmemberid(int fmemberid) {
		this.fmemberid = fmemberid;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getFriendid() {
		return friendid;
	}

	public void setFriendid(int friendid) {
		this.friendid = friendid;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getPlusinfo() {
		return plusinfo;
	}

	public void setPlusinfo(String plusinfo) {
		this.plusinfo = plusinfo;
	}

	public String getApplyyn() {
		return applyyn;
	}

	public void setApplyyn(String applyyn) {
		this.applyyn = applyyn;
	}

	@Override
	public String toString() {
		return "SelectOneSocialBoard2 [fmemberid=" + fmemberid + ", mid=" + mid + ", friendid=" + friendid + ", phone="
				+ phone + ", email=" + email + ", mname=" + mname + ", ssn=" + ssn + ", plusinfo=" + plusinfo
				+ ", applyyn=" + applyyn + "]";
	}

	
	
	
}
