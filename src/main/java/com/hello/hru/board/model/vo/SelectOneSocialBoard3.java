package com.hello.hru.board.model.vo;

public class SelectOneSocialBoard3 implements java.io.Serializable{
	private int mid;
	private String mname;
	private String phone;
	private String ssn;
	private String email;
	private String teamname;

	public SelectOneSocialBoard3() {

	}

	public SelectOneSocialBoard3(int mid, String mname, String phone, String ssn, String email, String teamname) {
		super();
		this.mid = mid;
		this.mname = mname;
		this.phone = phone;
		this.ssn = ssn;
		this.email = email;
		this.teamname = teamname;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTeamname() {
		return teamname;
	}

	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}

	@Override
	public String toString() {
		return "SelectOneSocialBoard3 [mid=" + mid + ", mname=" + mname + ", phone=" + phone + ", ssn=" + ssn
				+ ", email=" + email + ", teamname=" + teamname + "]";
	}
	
}
