package com.hello.hru.board.model.vo;

import java.sql.Date;

public class SocialMember implements java.io.Serializable{
	private int fmemberid;
	private int friendid;
	private String plusinfo;
	private String applyyn;
	private int mid;
	private Date applydate;
	
	public SocialMember() {
		
	}	
	
	public SocialMember(int fmemberid, int friendid, String plusinfo, String applyyn, int mid, Date applydate) {
		super();
		this.fmemberid = fmemberid;
		this.friendid = friendid;
		this.plusinfo = plusinfo;
		this.applyyn = applyyn;
		this.mid = mid;
		this.applydate = applydate;
	}



	public int getFmemberid() {
		return fmemberid;
	}

	public void setFmemberid(int fmemberid) {
		this.fmemberid = fmemberid;
	}

	public int getFriendid() {
		return friendid;
	}

	public void setFriendid(int friendid) {
		this.friendid = friendid;
	}

	public String getPlusinfo() {
		return plusinfo;
	}

	public void setPlusinfo(String plusinfo) {
		this.plusinfo = plusinfo;
	}

	public String getApplyyn() {
		return applyyn;
	}

	public void setApplyyn(String applyyn) {
		this.applyyn = applyyn;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public Date getApplydate() {
		return applydate;
	}

	public void setApplydate(Date applydate) {
		this.applydate = applydate;
	}



	@Override
	public String toString() {
		return "SocialMember [fmemberid=" + fmemberid + ", friendid=" + friendid + ", plusinfo=" + plusinfo
				+ ", applyyn=" + applyyn + ", mid=" + mid + ", applydate=" + applydate + "]";
	}
	
	
	
}
