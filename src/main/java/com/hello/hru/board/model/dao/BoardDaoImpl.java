package com.hello.hru.board.model.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.board.exception.ApplyException;
import com.hello.hru.board.exception.DeleteException;
import com.hello.hru.board.exception.FilterException;
import com.hello.hru.board.exception.InsertException;
import com.hello.hru.board.exception.ReplyException;
import com.hello.hru.board.exception.SearchException;
import com.hello.hru.board.exception.SelectException;
import com.hello.hru.board.exception.SelectOneException;
import com.hello.hru.board.exception.UpdateException;
import com.hello.hru.board.model.vo.Proposal;
import com.hello.hru.board.model.vo.ProposalComment;
import com.hello.hru.board.model.vo.SearchCondition;
import com.hello.hru.board.model.vo.SelectOneSocialBoard;
import com.hello.hru.board.model.vo.SelectOneSocialBoard2;
import com.hello.hru.board.model.vo.SelectOneSocialBoard3;
import com.hello.hru.board.model.vo.SelectSocialMember;
import com.hello.hru.board.model.vo.SocialBoard;
import com.hello.hru.board.model.vo.SocialMember;
import com.hello.hru.common.PageInfo;

@Repository
public class BoardDaoImpl implements BoardDao {

	@Override
	public ArrayList<SocialBoard> selectSocialList(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi)
			throws SelectException {

		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.selectSocialList", sb, rowBounds);
	}

	@Override
	public int insertSocialList(SqlSessionTemplate sqlSession, SocialBoard sb) throws InsertException {
		return sqlSession.insert("Board.insertSocialList", sb);
	}

	@Override
	public ArrayList<SelectSocialMember> selectSocialListInsert(SqlSessionTemplate sqlSession, SelectSocialMember ssm)
			throws SelectException {
		return (ArrayList) sqlSession.selectList("Board.selectSocialListInsert", ssm);

	}

	@Override
	public ArrayList<SelectOneSocialBoard> selectOneSocialList(SqlSessionTemplate sqlSession, int friendid,
			SelectOneSocialBoard ssb) throws SelectOneException {

		return (ArrayList) sqlSession.selectList("Board.selectOneSocialList", friendid);

	}

	@Override
	public ArrayList<SelectOneSocialBoard2> selectOneSocialList2(SqlSessionTemplate sqlSession,
			SelectOneSocialBoard2 ssb2) throws SelectOneException {
		return (ArrayList) sqlSession.selectList("Board.selectOneSocialList2", ssb2);
	}

	@Override
	public ArrayList<SelectOneSocialBoard3> selectOneSocialList3(SqlSessionTemplate sqlSession,
			SelectOneSocialBoard3 ssb3) throws SelectOneException {
		return (ArrayList) sqlSession.selectList("Board.selectOneSocialList3", ssb3);
	}

	@Override
	public int updateCount(SqlSessionTemplate sqlSession, int friendid) throws SelectOneException {

		return sqlSession.update("Board.updateCount", friendid);
	}

	@Override
	public int applySocialList(SqlSessionTemplate sqlSession, SocialMember sm) throws ApplyException {

		return sqlSession.insert("Board.applySocialList", sm);
	}

	@Override
	public ArrayList<SocialBoard> selectSocialListforWriter(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi)
			throws SelectException {

		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.selectSocialListforWriter", sb, rowBounds);
	}

	@Override
	public ArrayList<SelectOneSocialBoard> selectOneSocialListforWriter(SqlSessionTemplate sqlSession, int friendid)
			throws SelectOneException {
		return (ArrayList) sqlSession.selectList("Board.selectOneSocialListforWriter", friendid);

	}

	@Override
	public int updateCountforWriter(SqlSessionTemplate sqlSession, int friendid) throws SelectOneException {
		return sqlSession.update("Board.updateCountforWriter", friendid);

	}

	@Override
	public ArrayList<SelectOneSocialBoard2> selectOneSocialListforWriter2(SqlSessionTemplate sqlSession,
			SelectOneSocialBoard2 ssb2) throws SelectOneException {
		return (ArrayList) sqlSession.selectList("Board.selectOneSocialListforWriter2", ssb2);
	}

	@Override
	public int updateInfo(SqlSessionTemplate sqlSession, SelectOneSocialBoard ssb) throws UpdateException {
		return sqlSession.update("Board.updateInfo", ssb);
	}

	@Override
	public int deleteInfo(SqlSessionTemplate sqlSession, int friendid) throws DeleteException {
		return sqlSession.update("Board.deleteInfo", friendid);
	}

	@Override
	public ArrayList<SocialBoard> searchSocialList(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi)
			throws SearchException {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.searchSocialList", sc, rowBounds);
	}

	@Override
	public ArrayList<SocialBoard> searchSocialListforWriter(SqlSessionTemplate sqlSession, SearchCondition sc,
			PageInfo pi) throws SearchException {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return (ArrayList) sqlSession.selectList("Board.searchSocialListforWriter", sc, rowBounds);
	}

	@Override
	public ArrayList<SocialBoard> filterSocialList(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi)
			throws FilterException {

		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.filterSocialList", sc, rowBounds);
	}

	@Override
	public ArrayList<SocialBoard> filterSocialListforWriter(SqlSessionTemplate sqlSession, SearchCondition sc,
			PageInfo pi) throws FilterException {

		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.filterSocialListforWriter", sc, rowBounds);
	}

	@Override
	public ArrayList<SocialBoard> selectSocialList2(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi)
			throws SelectException {

		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.selectSocialList2", sb, rowBounds);

	}

	@Override
	public ArrayList<SocialBoard> selectSocialList3(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi)
			throws SelectException {

		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.selectSocialList3", sb, rowBounds);
	}

	@Override
	public ArrayList<SocialBoard> selectSocialList4(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi)
			throws SelectException {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.selectSocialList4", sb, rowBounds);
	}

	@Override
	public ArrayList<SocialBoard> selectSocialList5(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi)
			throws SelectException {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.selectSocialList5", sb, rowBounds);
	}

	@Override
	public ArrayList<SocialBoard> selectSocialList6(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi)
			throws SelectException {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.selectSocialList6", sb, rowBounds);
	}

	// @Override
	// public ArrayList selectSocialListAll(SqlSessionTemplate sqlSession,
	// SocialBoard sb) throws SelectException {
	// return (ArrayList) sqlSession.selectList("Board.selectSocialListAll", sb);
	// }

	@Override
	public int getListCount(SqlSessionTemplate sqlSession) throws SelectException {
		return sqlSession.selectOne("Board.getListCount");
	}

	@Override
	public int getListCount2(SqlSessionTemplate sqlSession) throws SelectException {
		return sqlSession.selectOne("Board.getListCount2");
	}

	@Override
	public int getListCount3(SqlSessionTemplate sqlSession) throws SelectException {
		return sqlSession.selectOne("Board.getListCount3");
	}

	@Override
	public int getListCount4(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Board.getListCount4");
	}

	@Override
	public int getListCount5(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Board.getListCount5");
	}

	@Override
	public int getListCount6(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Board.getListCount6");
	}

	@Override
	public int getListCountforWriter(SqlSessionTemplate sqlSession, SocialBoard sb) throws SelectException {
		return sqlSession.selectOne("Board.getListCountforWriter", sb);
	}

	@Override
	public int getListCountforSearch(SqlSessionTemplate sqlSession, SearchCondition sc) throws SearchException {
		return sqlSession.selectOne("Board.getListCountforSearch", sc);
	}

	@Override
	public int getListCountforFilter(SqlSessionTemplate sqlSession, SearchCondition sc) throws FilterException {

		return sqlSession.selectOne("Board.getListCountforFilter", sc);

	}

	@Override
	public int getListCountforSearchWriter(SqlSessionTemplate sqlSession, SearchCondition sc) throws SearchException {

		return sqlSession.selectOne("Board.getListCountforSearchWriter", sc);

	}

	@Override
	public int getListCountforFilterWriter(SqlSessionTemplate sqlSession, SearchCondition sc) throws FilterException {

		return sqlSession.selectOne("Board.getListCountforFilterWriter", sc);
	}

	@Override
	public int updatePlusInfo(SqlSessionTemplate sqlSession, SelectOneSocialBoard2 ssb2) throws UpdateException {
		
		return sqlSession.update("Board.updatePlusInfo", ssb2);

	}

	@Override
	public ArrayList<Proposal> selectPbList(PageInfo pi, SqlSessionTemplate sqlSession) throws SelectException {

		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());		

		return (ArrayList) sqlSession.selectList("Board.selectPbList", null,rowBounds);
	}

	@Override
	public int getListCountforPb(SqlSessionTemplate sqlSession) {

		return sqlSession.selectOne("Board.getListCountforPb");
	}

	@Override
	public Proposal selectOnePbListwithComment(SqlSessionTemplate sqlSession, Proposal p)
			throws SelectOneException {
		return sqlSession.selectOne("Board.selectOnePbListwithComment", p);

	}

	@Override
	public ArrayList<ProposalComment> selectOnePbListwithComment2(SqlSessionTemplate sqlSession, ProposalComment pc)
			throws SelectOneException {
		return (ArrayList) sqlSession.selectList("Board.selectOnePbListwithComment2", pc);

	}

	@Override
	public ArrayList<Proposal> selectOnePbList(SqlSessionTemplate sqlSession, Proposal p)
			throws SelectOneException {

		return (ArrayList) sqlSession.selectList("Board.selectOnePbList", p);
	}

	@Override
	public ArrayList<Proposal> selectInsertPbList(SqlSessionTemplate sqlSession, Proposal p) throws SelectException {

		return (ArrayList) sqlSession.selectList("Board.selectInsertPbList", p);

	}

	@Override
	public int insertPbList(SqlSessionTemplate sqlSession, Proposal p) throws InsertException {

		return sqlSession.insert("Board.insertPbList", p);

	}

	@Override
	public int updateCountforPb(SqlSessionTemplate sqlSession, Proposal p) throws SelectOneException {
		return sqlSession.update("Board.updateCountforPb", p);	
	}

	@Override
	public int updateInfoforPb(SqlSessionTemplate sqlSession, Proposal p) throws UpdateException {

		return sqlSession.update("Board.updateInfoforPb", p);
	}

	@Override
	public int deleteInfoforPb(SqlSessionTemplate sqlSession, int proposalid) throws DeleteException {

		return sqlSession.update("Board.deleteInfoforPb", proposalid);
	}

	@Override
	public int getListCountPbforSearch(SqlSessionTemplate sqlSession, SearchCondition sc) throws SearchException {
		return sqlSession.selectOne("Board.getListCountPbforSearch", sc);

	}

	@Override
	public ArrayList<Proposal> searchPbList(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi)
			throws SearchException {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();

		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return (ArrayList) sqlSession.selectList("Board.searchPbList", sc, rowBounds);
	}

	@Override
	public int insertReply(SqlSessionTemplate sqlSession, ProposalComment pc) throws ReplyException{
		
		return sqlSession.insert("Board.insertReply", pc);

	}

	@Override
	public int updateReply(SqlSessionTemplate sqlSession, ProposalComment pc) throws UpdateException {

		return sqlSession.update("Board.updateReply", pc);
	}

	@Override
	public int deleteReply(SqlSessionTemplate sqlSession, int procommentid) throws DeleteException {
		
		return sqlSession.update("Board.deleteReply", procommentid);
	}

	@Override
	public int cancelApply(SqlSessionTemplate sqlSession, SelectOneSocialBoard2 ssb2) throws UpdateException {
		
		return sqlSession.update("Board.cancelApply", ssb2);
	}

	// @Override
	// public ArrayList<SocialBoard> filterSocialListAllforWriter(SqlSessionTemplate
	// sqlSession, SearchCondition sc)
	// throws SelectException {
	// return (ArrayList)sqlSession.selectList("Board.filterSocialListAllforWriter",
	// sc);
	//
	// }
}
