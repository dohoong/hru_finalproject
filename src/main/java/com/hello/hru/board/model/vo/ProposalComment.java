package com.hello.hru.board.model.vo;

import java.sql.Date;

public class ProposalComment implements java.io.Serializable{

	private int mid;
	private int procommentid;
	private int proposalid;
	private String content2;
	private String mname2;
	private int dept;
	private String removeyn2;
	private Date createdate2;
	private int orgcommentid;

	public ProposalComment() {

	}

	public ProposalComment(int mid, int procommentid, int proposalid, String content2, String mname2, int dept,
			String removeyn2, Date createdate2, int orgcommentid) {
		super();
		this.mid = mid;
		this.procommentid = procommentid;
		this.proposalid = proposalid;
		this.content2 = content2;
		this.mname2 = mname2;
		this.dept = dept;
		this.removeyn2 = removeyn2;
		this.createdate2 = createdate2;
		this.orgcommentid = orgcommentid;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getProcommentid() {
		return procommentid;
	}

	public void setProcommentid(int procommentid) {
		this.procommentid = procommentid;
	}

	public int getProposalid() {
		return proposalid;
	}

	public void setProposalid(int proposalid) {
		this.proposalid = proposalid;
	}

	public String getContent2() {
		return content2;
	}

	public void setContent2(String content2) {
		this.content2 = content2;
	}

	public String getMname2() {
		return mname2;
	}

	public void setMname2(String mname2) {
		this.mname2 = mname2;
	}

	public int getDept() {
		return dept;
	}

	public void setDept(int dept) {
		this.dept = dept;
	}

	public String getRemoveyn2() {
		return removeyn2;
	}

	public void setRemoveyn2(String removeyn2) {
		this.removeyn2 = removeyn2;
	}

	public Date getCreatedate2() {
		return createdate2;
	}

	public void setCreatedate2(Date createdate2) {
		this.createdate2 = createdate2;
	}

	public int getOrgcommentid() {
		return orgcommentid;
	}

	public void setOrgcommentid(int orgcommentid) {
		this.orgcommentid = orgcommentid;
	}

	@Override
	public String toString() {
		return "ProposalComment [mid=" + mid + ", procommentid=" + procommentid + ", proposalid=" + proposalid
				+ ", content2=" + content2 + ", mname2=" + mname2 + ", dept=" + dept + ", removeyn2=" + removeyn2
				+ ", createdate2=" + createdate2 + ", orgcommentid=" + orgcommentid + "]";
	}

	
	
}
