package com.hello.hru.board.model.vo;

public class SelectSocialMember implements java.io.Serializable{
	
	private int mid;
	private String mname;
	private String email;
	private String phone;
	private String teamname;
	private String ssn;
	
	
	public SelectSocialMember() {
		
	}

	public SelectSocialMember(int mid, String mname, String email, String phone, String teamname, String ssn) {
		super();
		this.mid = mid;
		this.mname = mname;
		this.email = email;
		this.phone = phone;
		this.teamname = teamname;
		this.ssn = ssn;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTeamname() {
		return teamname;
	}

	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	@Override
	public String toString() {
		return "SelectSocialMember [mid=" + mid + ", mname=" + mname + ", email=" + email + ", phone=" + phone
				+ ", teamname=" + teamname + ", ssn=" + ssn + "]";
	}	
}
