package com.hello.hru.board.model.vo;

public class SearchCondition implements java.io.Serializable {

	private int mid;
	private String mname;
	private String title;
	private int category;

	public SearchCondition() {

	}

	public SearchCondition(int mid, String mname, String title, int category) {
		super();
		this.mid = mid;
		this.mname = mname;
		this.title = title;
		this.category = category;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "SearchCondition [mid=" + mid + ", mname=" + mname + ", title=" + title + ", category=" + category + "]";
	}

	
}
