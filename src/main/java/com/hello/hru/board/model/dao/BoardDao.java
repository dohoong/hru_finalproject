package com.hello.hru.board.model.dao;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.board.exception.ApplyException;
import com.hello.hru.board.exception.DeleteException;
import com.hello.hru.board.exception.FilterException;
import com.hello.hru.board.exception.InsertException;
import com.hello.hru.board.exception.ReplyException;
import com.hello.hru.board.exception.SearchException;
import com.hello.hru.board.exception.SelectException;
import com.hello.hru.board.exception.SelectOneException;
import com.hello.hru.board.exception.UpdateException;
import com.hello.hru.board.model.vo.Proposal;
import com.hello.hru.board.model.vo.ProposalComment;
import com.hello.hru.board.model.vo.SearchCondition;
import com.hello.hru.board.model.vo.SelectOneSocialBoard;
import com.hello.hru.board.model.vo.SelectOneSocialBoard2;
import com.hello.hru.board.model.vo.SelectOneSocialBoard3;
import com.hello.hru.board.model.vo.SelectSocialMember;
import com.hello.hru.board.model.vo.SocialBoard;
import com.hello.hru.board.model.vo.SocialMember;
import com.hello.hru.common.PageInfo;

public interface BoardDao{

	ArrayList<SocialBoard> selectSocialList(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi) throws SelectException;

	int insertSocialList(SqlSessionTemplate sqlSession, SocialBoard sb) throws InsertException;

	ArrayList<SelectSocialMember> selectSocialListInsert(SqlSessionTemplate sqlSession, SelectSocialMember ssm) throws SelectException;

	ArrayList<SelectOneSocialBoard> selectOneSocialList(SqlSessionTemplate sqlSession, int friendid, SelectOneSocialBoard ssb) throws SelectOneException;

	ArrayList<SelectOneSocialBoard2> selectOneSocialList2(SqlSessionTemplate sqlSession, SelectOneSocialBoard2 ssb2) throws SelectOneException;

	ArrayList<SelectOneSocialBoard3> selectOneSocialList3(SqlSessionTemplate sqlSession, SelectOneSocialBoard3 ssb3) throws SelectOneException;

	int updateCount(SqlSessionTemplate sqlSession, int friendid) throws SelectOneException;

	int applySocialList(SqlSessionTemplate sqlSession, SocialMember sm) throws ApplyException;

	ArrayList<SocialBoard> selectSocialListforWriter(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SelectOneSocialBoard> selectOneSocialListforWriter(SqlSessionTemplate sqlSession, int friendid) throws SelectOneException;

	int updateCountforWriter(SqlSessionTemplate sqlSession, int friendid) throws SelectOneException;

	ArrayList<SelectOneSocialBoard2> selectOneSocialListforWriter2(SqlSessionTemplate sqlSession,
			SelectOneSocialBoard2 ssb2) throws SelectOneException;

	int updateInfo(SqlSessionTemplate sqlSession, SelectOneSocialBoard ssb) throws UpdateException;

	int deleteInfo(SqlSessionTemplate sqlSession, int friendid) throws DeleteException;

	ArrayList<SocialBoard> searchSocialList(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi) throws SearchException;

	ArrayList<SocialBoard> searchSocialListforWriter(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi) throws SearchException;

	ArrayList<SocialBoard> filterSocialListforWriter(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi) throws FilterException;

	ArrayList<SocialBoard> filterSocialList(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi) throws FilterException;

	ArrayList<SocialBoard> selectSocialList2(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SocialBoard> selectSocialList3(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SocialBoard> selectSocialList4(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SocialBoard> selectSocialList5(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi) throws SelectException;

	ArrayList<SocialBoard> selectSocialList6(SqlSessionTemplate sqlSession, SocialBoard sb, PageInfo pi) throws SelectException;

	//ArrayList<SocialBoard> selectSocialListAll(SqlSessionTemplate sqlSession, SocialBoard sb) throws SelectException;

	int getListCount(SqlSessionTemplate sqlSession) throws SelectException;

	int getListCount2(SqlSessionTemplate sqlSession) throws SelectException;

	int getListCount3(SqlSessionTemplate sqlSession) throws SelectException;

	int getListCount4(SqlSessionTemplate sqlSession) throws SelectException;

	int getListCount5(SqlSessionTemplate sqlSession) throws SelectException;

	int getListCount6(SqlSessionTemplate sqlSession) throws SelectException;

	int getListCountforWriter(SqlSessionTemplate sqlSession, SocialBoard sb) throws SelectException;

	int getListCountforSearch(SqlSessionTemplate sqlSession, SearchCondition sc) throws SearchException;

	int getListCountforFilter(SqlSessionTemplate sqlSession, SearchCondition sc) throws FilterException;

	int getListCountforSearchWriter(SqlSessionTemplate sqlSession, SearchCondition sc) throws SearchException;

	int getListCountforFilterWriter(SqlSessionTemplate sqlSession, SearchCondition sc) throws FilterException;

	int updatePlusInfo(SqlSessionTemplate sqlSession, SelectOneSocialBoard2 ssb2) throws UpdateException;

	ArrayList<Proposal> selectPbList(PageInfo pi, SqlSessionTemplate sqlSession) throws SelectException;

	int getListCountforPb(SqlSessionTemplate sqlSession);

	ArrayList<Proposal> selectOnePbList(SqlSessionTemplate sqlSession, Proposal p) throws SelectOneException;

	Proposal selectOnePbListwithComment(SqlSessionTemplate sqlSession, Proposal p) throws SelectOneException;

	ArrayList<ProposalComment> selectOnePbListwithComment2(SqlSessionTemplate sqlSession, ProposalComment pc) throws SelectOneException;

	ArrayList<Proposal> selectInsertPbList(SqlSessionTemplate sqlSession, Proposal p) throws SelectException;

	int insertPbList(SqlSessionTemplate sqlSession, Proposal p) throws InsertException;

	int updateCountforPb(SqlSessionTemplate sqlSession, Proposal p) throws SelectOneException;

	int updateInfoforPb(SqlSessionTemplate sqlSession, Proposal p) throws UpdateException;

	int deleteInfoforPb(SqlSessionTemplate sqlSession, int proposalid) throws DeleteException;

	int getListCountPbforSearch(SqlSessionTemplate sqlSession, SearchCondition sc) throws SearchException;

	ArrayList<Proposal> searchPbList(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi) throws SearchException;

	int insertReply(SqlSessionTemplate sqlSession, ProposalComment pc) throws ReplyException;

	int updateReply(SqlSessionTemplate sqlSession, ProposalComment pc) throws UpdateException;

	int deleteReply(SqlSessionTemplate sqlSession, int procommentid) throws DeleteException;

	int cancelApply(SqlSessionTemplate sqlSession, SelectOneSocialBoard2 ssb2) throws UpdateException;

	//ArrayList<SocialBoard> filterSocialListAllforWriter(SqlSessionTemplate sqlSession, SearchCondition sc) throws SelectException;
 


}
