package com.hello.hru.board.model.vo;

import java.sql.Date;

public class SocialBoard implements java.io.Serializable{

	private int friendid;
	private String title;
	private Date meetingdate;
	private Date meetenddate;
	private String mname;
	private int meetperson;
	private String content;
	private String removeyn;
	private int category;
	private int mid;
	private int count;

	public SocialBoard() {

	}


	public SocialBoard(int friendid, String title, Date meetingdate, Date meetenddate, String mname, int meetperson,
			String content, String removeyn, int category, int mid, int count) {
		super();
		this.friendid = friendid;
		this.title = title;
		this.meetingdate = meetingdate;
		this.meetenddate = meetenddate;
		this.mname = mname;
		this.meetperson = meetperson;
		this.content = content;
		this.removeyn = removeyn;
		this.category = category;
		this.mid = mid;
		this.count = count;
	}
	
	public int getFriendid() {
		return friendid;
	}


	public void setFriendid(int friendid) {
		this.friendid = friendid;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Date getMeetingdate() {
		return meetingdate;
	}


	public void setMeetingdate(Date meetingdate) {
		this.meetingdate = meetingdate;
	}


	public Date getMeetenddate() {
		return meetenddate;
	}


	public void setMeetenddate(Date meetenddate) {
		this.meetenddate = meetenddate;
	}


	public String getMname() {
		return mname;
	}


	public void setMname(String mname) {
		this.mname = mname;
	}


	public int getMeetperson() {
		return meetperson;
	}


	public void setMeetperson(int meetperson) {
		this.meetperson = meetperson;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getRemoveyn() {
		return removeyn;
	}


	public void setRemoveyn(String removeyn) {
		this.removeyn = removeyn;
	}


	public int getCategory() {
		return category;
	}


	public void setCategory(int category) {
		this.category = category;
	}


	public int getMid() {
		return mid;
	}


	public void setMid(int mid) {
		this.mid = mid;
	}


	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}


	@Override
	public String toString() {
		return "SocialBoard [friendid=" + friendid + ", title=" + title + ", meetingdate=" + meetingdate
				+ ", meetenddate=" + meetenddate + ", mname=" + mname + ", meetperson=" + meetperson + ", content="
				+ content + ", removeyn=" + removeyn + ", category=" + category + ", mid=" + mid + ", count=" + count
				+ "]";
	}




}
