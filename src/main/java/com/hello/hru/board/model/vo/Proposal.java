package com.hello.hru.board.model.vo;

import java.sql.Date;
import java.util.ArrayList;

public class Proposal implements java.io.Serializable{

	private int proposalid;
	private String mname;
	private Date createdate;
	private String title;
	private String content;
	private String removeyn;
	private int mid;
	private int count;
	
	private ArrayList<ProposalComment> replyList;


	public Proposal() {

	}


	public Proposal(int proposalid, String mname, Date createdate, String title, String content, String removeyn,
			int mid, int count, ArrayList<ProposalComment> replyList) {
		super();
		this.proposalid = proposalid;
		this.mname = mname;
		this.createdate = createdate;
		this.title = title;
		this.content = content;
		this.removeyn = removeyn;
		this.mid = mid;
		this.count = count;
		this.replyList = replyList;
	}


	public int getProposalid() {
		return proposalid;
	}


	public void setProposalid(int proposalid) {
		this.proposalid = proposalid;
	}


	public String getMname() {
		return mname;
	}


	public void setMname(String mname) {
		this.mname = mname;
	}


	public Date getCreatedate() {
		return createdate;
	}


	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getRemoveyn() {
		return removeyn;
	}


	public void setRemoveyn(String removeyn) {
		this.removeyn = removeyn;
	}


	public int getMid() {
		return mid;
	}


	public void setMid(int mid) {
		this.mid = mid;
	}


	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}


	public ArrayList<ProposalComment> getReplyList() {
		return replyList;
	}


	public void setReplyList(ArrayList<ProposalComment> replyList) {
		this.replyList = replyList;
	}


	@Override
	public String toString() {
		return "Proposal [proposalid=" + proposalid + ", mname=" + mname + ", createdate=" + createdate + ", title="
				+ title + ", content=" + content + ", removeyn=" + removeyn + ", mid=" + mid + ", count=" + count
				+ ", replyList=" + replyList + "]";
	}



}
