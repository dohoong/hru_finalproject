package com.hello.hru.board.model.vo;

import java.sql.Date;

public class SelectOneSocialBoard implements java.io.Serializable{

	private int friendid;
	private String title;
	private Date meetingdate;
	private Date meetenddate;
	private int category;
	private int meetperson;
	private String content;
	private String mname;
	private String phone;
	private String email;
	private String teamname;
	private String ssn;
	private int count;
	
	public SelectOneSocialBoard() {
		
	}

	public SelectOneSocialBoard(int friendid, String title, Date meetingdate, Date meetenddate, int category,
			int meetperson, String content, String mname, String phone, String email, String teamname, String ssn,
			int count) {
		super();
		this.friendid = friendid;
		this.title = title;
		this.meetingdate = meetingdate;
		this.meetenddate = meetenddate;
		this.category = category;
		this.meetperson = meetperson;
		this.content = content;
		this.mname = mname;
		this.phone = phone;
		this.email = email;
		this.teamname = teamname;
		this.ssn = ssn;
		this.count = count;
	}

	public int getFriendid() {
		return friendid;
	}

	public void setFriendid(int friendid) {
		this.friendid = friendid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getMeetingdate() {
		return meetingdate;
	}

	public void setMeetingdate(Date meetingdate) {
		this.meetingdate = meetingdate;
	}

	public Date getMeetenddate() {
		return meetenddate;
	}

	public void setMeetenddate(Date meetenddate) {
		this.meetenddate = meetenddate;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getMeetperson() {
		return meetperson;
	}

	public void setMeetperson(int meetperson) {
		this.meetperson = meetperson;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTeamname() {
		return teamname;
	}

	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "SelectOneSocialBoard [friendid=" + friendid + ", title=" + title + ", meetingdate=" + meetingdate
				+ ", meetenddate=" + meetenddate + ", category=" + category + ", meetperson=" + meetperson
				+ ", content=" + content + ", mname=" + mname + ", phone=" + phone + ", email=" + email + ", teamname="
				+ teamname + ", ssn=" + ssn + ", count=" + count + "]";
	}

	
	
}
