package com.hello.hru.board.model.service;

import java.util.ArrayList;

import javax.annotation.processing.FilerException;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.board.exception.ApplyException;
import com.hello.hru.board.exception.DeleteException;
import com.hello.hru.board.exception.FilterException;
import com.hello.hru.board.exception.InsertException;
import com.hello.hru.board.exception.ReplyException;
import com.hello.hru.board.exception.SearchException;
import com.hello.hru.board.exception.SelectException;
import com.hello.hru.board.exception.SelectOneException;
import com.hello.hru.board.exception.UpdateException;
import com.hello.hru.board.model.dao.BoardDao;
import com.hello.hru.board.model.vo.Proposal;
import com.hello.hru.board.model.vo.ProposalComment;
import com.hello.hru.board.model.vo.SearchCondition;
import com.hello.hru.board.model.vo.SelectOneSocialBoard;
import com.hello.hru.board.model.vo.SelectOneSocialBoard2;
import com.hello.hru.board.model.vo.SelectOneSocialBoard3;
import com.hello.hru.board.model.vo.SelectSocialMember;
import com.hello.hru.board.model.vo.SocialBoard;
import com.hello.hru.board.model.vo.SocialMember;
import com.hello.hru.common.PageInfo;

@Service
public class BoardServiceImpl implements BoardService {

	@Autowired
	private BoardDao bd;

	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	public ArrayList<SocialBoard> selectSocailList(SocialBoard sb, PageInfo pi) throws SelectException {

		return (ArrayList) bd.selectSocialList(sqlSession, sb, pi);
	}

	@Override
	public int insertSocialList(SocialBoard sb) throws InsertException {
		return bd.insertSocialList(sqlSession, sb);
	}

	@Override
	public ArrayList<SelectSocialMember> selectSocialListInsert(SelectSocialMember ssm) throws SelectException {

		return (ArrayList<SelectSocialMember>) bd.selectSocialListInsert(sqlSession, ssm);
	}

	@Override
	public ArrayList<SelectOneSocialBoard> selectOneSocialList(int friendid, SelectOneSocialBoard ssb)
			throws SelectOneException {

		int result = bd.updateCount(sqlSession, friendid);

		if (result > 0) {
			bd.selectOneSocialList(sqlSession, friendid, ssb);
		}

		return (ArrayList<SelectOneSocialBoard>) bd.selectOneSocialList(sqlSession, friendid, ssb);
	}

	@Override
	public ArrayList<SelectOneSocialBoard2> selectOneSocialList2(SelectOneSocialBoard2 ssb2) throws SelectOneException {

		return (ArrayList<SelectOneSocialBoard2>) bd.selectOneSocialList2(sqlSession, ssb2);

	}

	@Override
	public ArrayList<SelectOneSocialBoard3> selectOneSocialList3(SelectOneSocialBoard3 ssb3) throws SelectOneException {
		return (ArrayList<SelectOneSocialBoard3>) bd.selectOneSocialList3(sqlSession, ssb3);

	}

	@Override
	public int applySocialList(SocialMember sm) throws ApplyException {
		return bd.applySocialList(sqlSession, sm);
	}

	@Override
	public ArrayList<SocialBoard> selectSocialListforWriter(SocialBoard sb, PageInfo pi) throws SelectException {
		return (ArrayList<SocialBoard>) bd.selectSocialListforWriter(sqlSession, sb, pi);
	}

	@Override
	public ArrayList<SelectOneSocialBoard> selectOneSocialListforWriter(int friendid, SelectOneSocialBoard ssb)
			throws SelectOneException {

		int result = bd.updateCountforWriter(sqlSession, friendid);

		if (result > 0) {
			bd.selectOneSocialListforWriter(sqlSession, friendid);
		}

		return (ArrayList<SelectOneSocialBoard>) bd.selectOneSocialListforWriter(sqlSession, friendid);
	}

	@Override
	public ArrayList<SelectOneSocialBoard2> selectOneSocialListforWriter2(SelectOneSocialBoard2 ssb2)
			throws SelectOneException {
		return (ArrayList<SelectOneSocialBoard2>) bd.selectOneSocialListforWriter2(sqlSession, ssb2);

	}

	@Override
	public int updateInfo(SelectOneSocialBoard ssb) throws UpdateException {

		return bd.updateInfo(sqlSession, ssb);

	}

	@Override
	public int deleteInfo(int friendid) throws DeleteException {

		return bd.deleteInfo(sqlSession, friendid);
	}

	@Override
	public ArrayList<SocialBoard> searchSocialList(SearchCondition sc, PageInfo pi) throws SearchException {

		return (ArrayList<SocialBoard>) bd.searchSocialList(sqlSession, sc, pi);
	}

	@Override
	public ArrayList<SocialBoard> searchSocialListforWriter(SearchCondition sc, PageInfo pi) throws SearchException {
		return (ArrayList<SocialBoard>) bd.searchSocialListforWriter(sqlSession, sc, pi);
	}

	@Override
	public ArrayList<SocialBoard> filterSocialList(SearchCondition sc, PageInfo pi) throws FilterException {
		return (ArrayList<SocialBoard>) bd.filterSocialList(sqlSession, sc, pi);
	}

	@Override
	public ArrayList<SocialBoard> filterSocialListforWriter(SearchCondition sc, PageInfo pi) throws FilterException {
		return (ArrayList<SocialBoard>) bd.filterSocialListforWriter(sqlSession, sc, pi);
	}

	@Override
	public ArrayList<SocialBoard> selectSocailList2(SocialBoard sb, PageInfo pi) throws SelectException {

		return (ArrayList) bd.selectSocialList2(sqlSession, sb, pi);
	}

	@Override
	public ArrayList<SocialBoard> selectSocailList3(SocialBoard sb, PageInfo pi) throws SelectException {
		return (ArrayList) bd.selectSocialList3(sqlSession, sb, pi);

	}

	@Override
	public ArrayList<SocialBoard> selectSocailList4(SocialBoard sb, PageInfo pi) throws SelectException {

		return (ArrayList) bd.selectSocialList4(sqlSession, sb, pi);

	}

	@Override
	public ArrayList<SocialBoard> selectSocailList5(SocialBoard sb, PageInfo pi) throws SelectException {

		return (ArrayList) bd.selectSocialList5(sqlSession, sb, pi);

	}

	@Override
	public ArrayList<SocialBoard> selectSocailList6(SocialBoard sb, PageInfo pi) throws SelectException {

		return (ArrayList) bd.selectSocialList6(sqlSession, sb, pi);

	}

	@Override
	public int getListCount() throws SelectException {

		return bd.getListCount(sqlSession);
	}

	@Override
	public int getListCount2() throws SelectException {
		return bd.getListCount2(sqlSession);
	}

	@Override
	public int getListCount3() throws SelectException {
		return bd.getListCount3(sqlSession);
	}

	@Override
	public int getListCount4() throws SelectException {
		return bd.getListCount4(sqlSession);
	}

	@Override
	public int getListCount5() throws SelectException {
		return bd.getListCount5(sqlSession);
	}

	@Override
	public int getListCount6() throws SelectException {
		return bd.getListCount6(sqlSession);
	}

	@Override
	public int getListCountforWriter(SocialBoard sb) throws SelectException {
		return bd.getListCountforWriter(sqlSession, sb);
	}

	@Override
	public int getListCountforSearch(SearchCondition sc) throws SearchException {
		return bd.getListCountforSearch(sqlSession, sc);
	}

	@Override
	public int getListCountforFilter(SearchCondition sc) throws FilterException {
		return bd.getListCountforFilter(sqlSession, sc);

	}

	@Override
	public int getListCountforSearchWriter(SearchCondition sc) throws SearchException {
		return bd.getListCountforSearchWriter(sqlSession, sc);

	}

	@Override
	public int getListCountforFilterWriter(SearchCondition sc) throws FilterException {
		return bd.getListCountforFilterWriter(sqlSession, sc);

	}

	@Override
	public int updatePlusInfo(SelectOneSocialBoard2 ssb2) throws UpdateException {

		return bd.updatePlusInfo(sqlSession, ssb2);

	}

	@Override
	public ArrayList<Proposal> selectPbList(PageInfo pi) throws SelectException {
		return (ArrayList)bd.selectPbList(pi ,sqlSession);
	}

	@Override
	public int getListCountforPb(){
		return bd.getListCountforPb(sqlSession);
	}

	@Override
	public Proposal selectOnePbListwithComment(Proposal p) throws SelectOneException {
		int result = bd.updateCountforPb(sqlSession, p);

		if (result > 0) {
			bd.selectOnePbListwithComment(sqlSession, p);
		}


		return bd.selectOnePbListwithComment(sqlSession, p);
	}

	@Override
	public ArrayList<ProposalComment> selectOnePbListwithComment2(ProposalComment pc) throws SelectOneException {
		return (ArrayList<ProposalComment>)bd.selectOnePbListwithComment2(sqlSession, pc);
	}

	@Override
	public ArrayList<Proposal> selectOnePbList(Proposal p) throws SelectOneException {
		return (ArrayList<Proposal>)bd.selectOnePbList(sqlSession, p);
	}

	@Override
	public ArrayList<Proposal> selectInsertPbList(Proposal p) throws SelectException {
		return (ArrayList<Proposal>) bd.selectInsertPbList(sqlSession, p);
	}

	@Override
	public int insertPbList(Proposal p) throws InsertException {

		return bd.insertPbList(sqlSession, p);
	}

	@Override
	public int updateInfoforPb(Proposal p) throws UpdateException {

		return bd.updateInfoforPb(sqlSession, p);
	}

	@Override
	public int deleteInfoforPb(int proposalid) throws DeleteException {

		return bd.deleteInfoforPb(sqlSession, proposalid);
	}

	@Override
	public int getListCountPbforSearch(SearchCondition sc) throws SearchException {

		return bd.getListCountPbforSearch(sqlSession, sc);
	}

	@Override
	public ArrayList<Proposal> searchPbList(SearchCondition sc, PageInfo pi) throws SearchException {

		return (ArrayList<Proposal>)bd.searchPbList(sqlSession, sc, pi);
	}

	@Override
	public int insertReply(ProposalComment pc) throws ReplyException{
		
		return bd.insertReply(sqlSession, pc);

	}

	@Override
	public int updateReply(ProposalComment pc) throws UpdateException {
		
		return bd.updateReply(sqlSession, pc);
	}

	@Override
	public int deleteReply(int procommentid) throws DeleteException {
		
		return bd.deleteReply(sqlSession, procommentid);
	}

	@Override
	public int cancelApply(SelectOneSocialBoard2 ssb2) throws UpdateException {
		return bd.cancelApply(sqlSession, ssb2);
	}


	// @Override
	// public ArrayList<SocialBoard> selectSocailListAll(SocialBoard sb) throws
	// SelectException {
	//
	// return (ArrayList) bd.selectSocialListAll(sqlSession, sb);
	//
	// }

	// @Override
	// public ArrayList<SocialBoard> filterSocialListAllforWriter(SearchCondition
	// sc) throws SelectException {
	//
	//
	// return (ArrayList) bd.filterSocialListAllforWriter(sqlSession, sc);
	//
	//
	// }

}
