package com.hello.hru.board.exception;

public class FilterException extends Exception{
	public FilterException(String msg) {
		super(msg);
	}
}
