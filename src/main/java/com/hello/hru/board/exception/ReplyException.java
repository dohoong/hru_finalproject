package com.hello.hru.board.exception;

public class ReplyException extends Exception{
	public ReplyException(String msg) {
		super(msg);
	}

}
