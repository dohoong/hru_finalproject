package com.hello.hru.board.exception;

public class DeleteException extends Exception{
	public DeleteException(String msg) {
		super(msg);
	}
}
