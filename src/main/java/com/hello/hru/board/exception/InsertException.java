package com.hello.hru.board.exception;

public class InsertException extends Exception{
	public InsertException(String msg) {
		super(msg);
	}
}
