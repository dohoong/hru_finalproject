package com.hello.hru.board.exception;

public class SelectOneException extends Exception{
	public SelectOneException(String msg) {
		super(msg);
	}
}
