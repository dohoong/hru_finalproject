package com.hello.hru.board.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hello.hru.board.exception.ApplyException;
import com.hello.hru.board.exception.DeleteException;
import com.hello.hru.board.exception.FilterException;
import com.hello.hru.board.exception.InsertException;
import com.hello.hru.board.exception.ReplyException;
import com.hello.hru.board.exception.SearchException;
import com.hello.hru.board.exception.SelectException;
import com.hello.hru.board.exception.SelectOneException;
import com.hello.hru.board.exception.UpdateException;
import com.hello.hru.board.model.service.BoardService;
import com.hello.hru.board.model.vo.Proposal;
import com.hello.hru.board.model.vo.ProposalComment;
import com.hello.hru.board.model.vo.SearchCondition;
import com.hello.hru.board.model.vo.SelectOneSocialBoard;
import com.hello.hru.board.model.vo.SelectOneSocialBoard2;
import com.hello.hru.board.model.vo.SelectOneSocialBoard3;
import com.hello.hru.board.model.vo.SelectSocialMember;
import com.hello.hru.board.model.vo.SocialBoard;
import com.hello.hru.board.model.vo.SocialMember;
import com.hello.hru.common.PageInfo;
import com.hello.hru.common.Pagination;
import com.hello.hru.member.model.vo.Member;

@Controller
public class BoardController {

	@Autowired
	private BoardService bs;

	// 친목 도모 게시판 조회
	@RequestMapping("selectSocializingList.bo")
	public String selectSocialList(SocialBoard sb, Model model, HttpServletRequest request) {

		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		try {
			int listCount = bs.getListCount();

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<SocialBoard> list = bs.selectSocailList(sb, pi);

			model.addAttribute("category", list.get(0).getCategory());
			model.addAttribute("list", list);
			model.addAttribute("pi", pi);

			return "member/socializingBoard/socializingList";

		} catch (SelectException e) {
			model.addAttribute("msg", "조회 실패!!");
			return "common/errorPage";
		}
	}

	// 친목 도모 게시판 조회
	@RequestMapping("selectSocializingList2.bo")
	public String selectSocialList2(HttpServletRequest request, SocialBoard sb, Model model) {
		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		try {
			int listCount = bs.getListCount2();

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<SocialBoard> list = bs.selectSocailList2(sb, pi);

			model.addAttribute("list", list);
			model.addAttribute("pi", pi);
			model.addAttribute("category", list.get(0).getCategory());

			return "member/socializingBoard/socializingList";

		} catch (SelectException e) {
			model.addAttribute("msg", "조회 실패!!");
			return "common/errorPage";
		}
	}

	// 친목 도모 게시판 조회
	@RequestMapping("selectSocializingList3.bo")
	public String selectSocialList3(HttpServletRequest request, SocialBoard sb, Model model) {
		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		try {
			int listCount = bs.getListCount3();

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<SocialBoard> list = bs.selectSocailList3(sb, pi);
			model.addAttribute("list", list);
			model.addAttribute("pi", pi);
			model.addAttribute("category", list.get(0).getCategory());

			return "member/socializingBoard/socializingList";

		} catch (SelectException e) {
			model.addAttribute("msg", "조회 실패!!");
			return "common/errorPage";
		}
	}

	// 친목 도모 게시판 조회
	@RequestMapping("selectSocializingList4.bo")
	public String selectSocialList4(HttpServletRequest request, SocialBoard sb, Model model) {
		int currentPage = 1;


		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		try {
			int listCount = bs.getListCount4();

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<SocialBoard> list = bs.selectSocailList4(sb, pi);

			model.addAttribute("list", list);
			model.addAttribute("pi", pi);
			model.addAttribute("category", list.get(0).getCategory());

			return "member/socializingBoard/socializingList";

		} catch (SelectException e) {
			model.addAttribute("msg", "조회 실패!!");
			return "common/errorPage";
		}
	}

	// 친목 도모 게시판 조회
	@RequestMapping("selectSocializingList5.bo")
	public String selectSocialList5(HttpServletRequest request, SocialBoard sb, Model model) {
		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		try {
			int listCount = bs.getListCount5();

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<SocialBoard> list = bs.selectSocailList5(sb, pi);
			model.addAttribute("list", list);
			model.addAttribute("pi", pi);
			model.addAttribute("category", list.get(0).getCategory());

			return "member/socializingBoard/socializingList";

		} catch (SelectException e) {
			model.addAttribute("msg", "조회 실패!!");
			return "common/errorPage";
		}
	}

	// 친목 도모 게시판 조회
	@RequestMapping("selectSocializingList6.bo")
	public String selectSocialList6(HttpServletRequest request, SocialBoard sb, Model model) {

		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		try {
			int listCount = bs.getListCount6();

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<SocialBoard> list = bs.selectSocailList6(sb, pi);
			model.addAttribute("list", list);
			model.addAttribute("pi", pi);
			model.addAttribute("category", list.get(0).getCategory());

			return "member/socializingBoard/socializingList";

		} catch (SelectException e) {
			model.addAttribute("msg", "조회 실패!!");
			return "common/errorPage";
		}
	}

	// 친목 도모 게시판 작성 페이지로 넘기기
	@RequestMapping("selectSocialListInsert.bo")
	public String selectSocialListInsert(SelectSocialMember ssm, Model model, HttpServletRequest request) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		ssm.setMid(mid);

		try {
			ArrayList<SelectSocialMember> list = bs.selectSocialListInsert(ssm);
			model.addAttribute("list", list);
			model.addAttribute("mid", mid);
			return "member/socializingBoard/socializingListInsert";

		} catch (SelectException e) {
			model.addAttribute("msg", "조회 실패!!");
			return "common/errorPage";
		}

	}

	// 친목 도모 게시판 내용 작성
	@RequestMapping("insertSocialList.bo")
	public String insertSocializingList(Model model, SocialBoard sb) {

		try {
			bs.insertSocialList(sb);
			return "redirect:selectSocializingList.bo";

		} catch (InsertException e) {
			model.addAttribute("msg", "insert 실패!!");
			return "common/errorPage";
		}

	}

	// 사원 친목 도모 게시판 글쓴이용 메인
	@RequestMapping("selectSocialListforWriterMain.bo")
	public String SelectSocialListforWriterMain(HttpServletRequest request, Model model, SocialBoard sb) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		sb.setMid(mid);

		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		try {
			int listCount = bs.getListCountforWriter(sb);
			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<SocialBoard> list = bs.selectSocialListforWriter(sb, pi);
			model.addAttribute("list", list);
			model.addAttribute("pi", pi);

			return "member/socializingBoard/socializingListforWriterMain";

		} catch (SelectException e) {
			model.addAttribute("msg", "조회 실패!!");
			return "common/errorPage";
		}

	}

	// ajax로 console에 카테고리 값 받아오기
	@PostMapping("selInsertSocial.bo")
	public ModelAndView selInsertSocial(ModelAndView mv, String sel) {

		System.out.println(sel);

		mv.addObject("sel", sel);
		mv.setViewName("jsonView");

		return mv;
	}

	// 친목 도모 게시판 상세 조회(신청 페이지로 이동)
	@RequestMapping("selectOneSocialListApply.bo")
	public String selectOneSocialListApply(Model model, int friendid, SelectOneSocialBoard ssb,
			SelectOneSocialBoard2 ssb2, HttpServletRequest request, SelectOneSocialBoard3 ssb3) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		ssb3.setMid(mid);

		try {
			ArrayList<SelectOneSocialBoard> list1 = bs.selectOneSocialList(friendid, ssb);

			ArrayList<SelectOneSocialBoard2> list2 = bs.selectOneSocialList2(ssb2);

			ArrayList<SelectOneSocialBoard3> list3 = bs.selectOneSocialList3(ssb3);

			model.addAttribute("friendid", friendid);
			model.addAttribute("mid", mid);
			model.addAttribute("list1", list1);
			model.addAttribute("list2", list2);
			model.addAttribute("list3", list3);

			return "member/socializingBoard/socializingListApplyVolunteer";

		} catch (SelectOneException e) {
			model.addAttribute("msg", "조회 실패!!");

			return "common/errorPage";
		}

	}

	// 친목 도모 게시판 신청자 신청하기
	@RequestMapping("applySocialList.bo")
	public String applySocialList(Model model, SocialMember sm, RedirectAttributes rttr) {

		try {
			bs.applySocialList(sm);

			rttr.addAttribute("friendid", sm.getFriendid());

			return "redirect:selectOneSocialListApply.bo";
		} catch (ApplyException e) {

			model.addAttribute("msg", "신청 실패!!");

			return "common/errorPage";
		}
	}

	// 친목 도모 게시판 글쓴이용 상세 조회
	@RequestMapping("selectOneSocialListforWriter.bo")
	public String selectOneSocialListforWriter(Model model, SelectOneSocialBoard ssb, SelectOneSocialBoard2 ssb2,
			int friendid) {

		try {
			ArrayList<SelectOneSocialBoard> list1 = bs.selectOneSocialListforWriter(friendid, ssb);

			ArrayList<SelectOneSocialBoard2> list2 = bs.selectOneSocialListforWriter2(ssb2);

			model.addAttribute("list1", list1);
			model.addAttribute("list2", list2);

			return "member/socializingBoard/socializingListforWriterVolunteer";

		} catch (SelectOneException e) {
			model.addAttribute("msg", "상세 조회 실패!!!");

			return "common/errorPage";
		}

	}

	// 글쓴이용 상세 조회에서 글 수정
	@RequestMapping("infoUpdate.bo")
	public String updateInfo(SelectOneSocialBoard ssb, Model model) {

		try {
			bs.updateInfo(ssb);
			return "redirect: selectSocialListforWriterMain.bo";

		} catch (UpdateException e) {
			model.addAttribute("msg", "수정 실패!!");

			return "common/errorPage";
		}
	}

	// 글쓴이 상세 조회에서 글 삭제
	@RequestMapping("infoDelete.bo")
	public String deleteInfo(Model model, int friendid) {

		try {
			bs.deleteInfo(friendid);

			return "redirect: selectSocialListforWriterMain.bo";

		} catch (DeleteException e) {
			model.addAttribute("msg", "삭제 실패!!");

			return "common/errorPage";
		}
	}

	// 친목 도모 게시판 리스트에서 검색하기
	@RequestMapping("searchSocialList.bo")
	public String searchSocialList(HttpServletRequest request, String searchCondition, String searchValue,
			SearchCondition sc, Model model, int category) {

		sc.setCategory(category);

		if (searchCondition.equals("writer")) {
			sc.setMname(searchValue);
		} else {
			sc.setTitle(searchValue);
		}

		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		try {
			int listCount = bs.getListCountforSearch(sc);

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
			ArrayList<SocialBoard> list = bs.searchSocialList(sc, pi);

			model.addAttribute("list", list);
			model.addAttribute("pi", pi);

			return "member/socializingBoard/socializingListforSearch";

		} catch (SearchException e) {
			model.addAttribute("msg", "검색 실패!!");

			return "common/errorPage";
		}

	}

	// 친목 도모 게시판 글쓴이용 리스트에서 검색하기
	@RequestMapping("searchSocialListforWriter.bo")
	public String searchSocialListforWriter(HttpServletRequest request, String searchCondition, String searchValue,
			SearchCondition sc, Model model) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		sc.setMid(mid);

		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		if (searchCondition.equals("title")) {
			sc.setTitle(searchValue);
		}

		try {
			int listCount = bs.getListCountforSearchWriter(sc);

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<SocialBoard> list = bs.searchSocialListforWriter(sc, pi);

			model.addAttribute("list", list);
			model.addAttribute("pi", pi);
			model.addAttribute("category", list.get(0).getCategory());

			return "member/socializingBoard/socializingListforWriterMain";

		} catch (SearchException e) {
			model.addAttribute("msg", "검색 실패!!");
			return "common/errorPage";
		}

	}

	// 친목 도모 게시판 리스트 필터링
	@RequestMapping("filterSocialList.bo")
	public String filterSocialList(HttpServletRequest request, int category, SearchCondition sc, Model model) {

		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		if (category == 1) {
			sc.setCategory(category);
		} else if (category == 2) {
			sc.setCategory(category);
		} else if (category == 3) {
			sc.setCategory(category);
		} else if (category == 4) {
			sc.setCategory(category);
		} else if (category == 5) {
			sc.setCategory(category);
		} else {
			sc.setCategory(category);
		}

		try {
			int listCount = bs.getListCountforFilter(sc);

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<SocialBoard> list = bs.filterSocialList(sc, pi);

			model.addAttribute("list", list);
			model.addAttribute("pi", pi);
			model.addAttribute("category", sc.getCategory());

			return "member/socializingBoard/socializingList";
		} catch (FilterException e) {
			model.addAttribute("msg", "필터링 실패!!");
			return "common/errorPage";
		}

	}

	// 친목 도모 게시판 글쓴이용 필터링
	@RequestMapping("filterSocialListforWriter.bo")
	public String filterSocialListforWriter(HttpServletRequest request, int filterCondition, SearchCondition sc,
			Model model) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		sc.setMid(mid);

		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		if (filterCondition == 1) {
			sc.setCategory(filterCondition);
		} else if (filterCondition == 2) {
			sc.setCategory(filterCondition);
		} else if (filterCondition == 3) {
			sc.setCategory(filterCondition);
		} else if (filterCondition == 4) {
			sc.setCategory(filterCondition);
		} else if (filterCondition == 5) {
			sc.setCategory(filterCondition);
		} else if (filterCondition == 6) {
			sc.setCategory(filterCondition);
		}

		try {
			int listCount = bs.getListCountforFilterWriter(sc);

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
			ArrayList<SocialBoard> list = bs.filterSocialListforWriter(sc, pi);

			model.addAttribute("list", list);
			model.addAttribute("pi", pi);

			return "member/socializingBoard/socializingListforWriterMain";
		} catch (FilterException e) {
			model.addAttribute("msg", "필터링 실패!!");
			return "common/errorPage";
		}

	}


	// @RequestMapping("filterSocialListAllforWriter.bo")
	// public String filterSocialListAllforWriter(SearchCondition sc, Model model) {
	//
	// try {
	// ArrayList<SocialBoard> list = bs.filterSocialListAllforWriter(sc);
	// model.addAttribute("list", list);
	//
	// return "member/socializingBoard/socializingList";
	// } catch (SelectException e) {
	// model.addAttribute("msg", "조회 실패!!");
	// return "common/errorPage";
	// }
	// }

	// 건의사항 게시판 리스트 조회
	@RequestMapping("selectPbList.bo")
	public String selectPbList(HttpServletRequest request, Proposal p, Model model) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		p.setMid(mid);

		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		try {

			int listCount = bs.getListCountforPb();

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<Proposal> list = bs.selectPbList(pi);

			model.addAttribute("mid", mid);
			model.addAttribute("pi", pi);
			model.addAttribute("list", list);

			return "member/proposalBoard/pbList";
		} catch (SelectException e) {

			model.addAttribute("msg", "조회 실패!!");

			return "common/errorPage";
		}

	}

	// 건의 사항 게시판 댓글 페이지 상세조회
	@RequestMapping("selectOnePbListwithComment.bo")
	public String selectOnePbListwithComment(HttpServletRequest request, Proposal p, ProposalComment pc, Model model,
			int proposalid) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String mname = ((Member) (request.getSession().getAttribute("loginUser"))).getmName();
		pc.setMid(mid);
		pc.setMname2(mname);
		
		try {
			p = bs.selectOnePbListwithComment(p);
			ArrayList<ProposalComment> list2 = bs.selectOnePbListwithComment2(pc);
						
			System.out.println(list2);
			System.out.println(pc.getMname2());
			model.addAttribute("mname", pc.getMname2());
			model.addAttribute("replyList", p.getReplyList());
			model.addAttribute("p", p);
			model.addAttribute("pc", pc);
			model.addAttribute("list2", list2);
			model.addAttribute("mid", mid);

			return "member/proposalBoard/detailpbListwithComment";

		} catch (SelectOneException e) {
			model.addAttribute("msg", "댓글 페이지 상세 조회 실패!!");

			return "common/errorPage";
		}
	}

	// 건의 사항 게시판 상세조회
	@RequestMapping("selectOnePbList.bo")
	public String selectOnePbList(HttpServletRequest request, Proposal p, Model model) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		p.setMid(mid);

		try {
			ArrayList<Proposal> list = bs.selectOnePbList(p);

			model.addAttribute("mid", mid);
			model.addAttribute("list", list);

			return "member/proposalBoard/detailpbList";

		} catch (SelectOneException e) {
			model.addAttribute("msg", "상세 조회 실패!!");

			return "common/errorPage";
		}

	}

	// 건의 사항 게시판 작성 페이지 조회
	@RequestMapping("selectInsertPbList.bo")
	public String selectInsertPbList(HttpServletRequest request, Model model, Proposal p) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		p.setMid(mid);

		try {
			ArrayList<Proposal> list = bs.selectInsertPbList(p);
			model.addAttribute("list", list);
			model.addAttribute("mid", mid);
			return "member/proposalBoard/insertPbList";

		} catch (SelectException e) {
			model.addAttribute("msg", "조회 실패!!");
			return "common/errorPage";
		}
	}

	// 건의 사항 게시판 작성
	@RequestMapping("insertPbList.bo")
	public String insertPbList(HttpServletRequest request, Model model, Proposal p) {
		
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		p.setMid(mid);

		try {
			bs.insertPbList(p);
			
			
			return "redirect:selectPbList.bo";

		} catch (InsertException e) {

			model.addAttribute("msg", "insert 실패!!");
			return "common/errorPage";
		}
	}

	// 건의 사항 게시판 수정
	@RequestMapping("infoUpdateforPb.bo")
	public String infoUpdateforPb(Model model, Proposal p) {

		try {
			bs.updateInfoforPb(p);

			return "redirect: selectPbList.bo";

		} catch (UpdateException e) {
			model.addAttribute("msg", "수정 실패!!");

			return "common/errorPage";
		}

	}

	// 건의 사항 게시판 삭제
	@RequestMapping("infoDeleteforPb.bo")
	public String infoDeleteforPb(Model model, int proposalid) {

		try {
			bs.deleteInfoforPb(proposalid);

			return "redirect: selectPbList.bo";
		} catch (DeleteException e) {
			model.addAttribute("msg", "삭제 실패!!");

			return "common/errorPage";
		}
	}

	// 건의 사항 게시판 검색 리스트 조회
	@RequestMapping("searchPbList.bo")
	public String searchPbList(HttpServletRequest request, String searchCondition, String searchValue,
			SearchCondition sc, Model model) {
		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		if (searchCondition.equals("writer")) {
			sc.setMname(searchValue);
		} else {
			sc.setTitle(searchValue);
		}

		try {
			int listCount = bs.getListCountPbforSearch(sc);

			PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

			ArrayList<Proposal> list = bs.searchPbList(sc, pi);

			model.addAttribute("list", list);
			model.addAttribute("pi", pi);

			return "member/proposalBoard/pbList";
		} catch (SearchException e) {
			model.addAttribute("msg", "검색 실패!!");

			return "common/errorPage";
		}
	}

	// 건의 사항 게시판 댓글 달기
	@RequestMapping("insertReply.bo")
	public String insertReply(HttpServletRequest request, Model model, Member m, ProposalComment pc, Proposal p,
			RedirectAttributes rttr) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String mname = ((Member) (request.getSession().getAttribute("loginUser"))).getmName();
		pc.setMid(mid);
		pc.setMname2(mname);

		try {
			bs.insertReply(pc);
			
			System.out.println("pc : " + pc);
			rttr.addAttribute("mname", pc.getMname2());			
			rttr.addAttribute("proposalid", p.getProposalid());
			rttr.addAttribute("mid", p.getMid());

			return "redirect:selectOnePbListwithComment.bo";
		} catch (ReplyException e) {
			model.addAttribute("msg", "댓글 달기 실패!!");

			return "common/errorPage";
		}
	}

	// 건의 사항 게시판 게시글 댓글 수정
	@RequestMapping("replyUpdate.bo")
	public String updateReply(HttpServletRequest request, Model model, ProposalComment pc, Proposal p,
			RedirectAttributes rttr) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		pc.setMid(mid);

		try {
			bs.updateReply(pc);

			rttr.addAttribute("procommentid", pc.getProcommentid());
			rttr.addAttribute("proposalid", p.getProposalid());
			rttr.addAttribute("mid", mid);

			return "redirect:selectOnePbListwithComment.bo";

		} catch (UpdateException e) {

			model.addAttribute("msg", "수정 실패!!");
			return "common/errorPage";
		}
	}

	//건의 사항 게시판  게시글 댓글 삭제
	@RequestMapping("replyDelete.bo")
	public String replyDelete(HttpServletRequest request, Proposal p, Model model, int procommentid, ProposalComment pc,
			RedirectAttributes rttr) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		pc.setMid(mid);

		try {
			bs.deleteReply(procommentid);

			rttr.addAttribute("procommentid", procommentid);
			rttr.addAttribute("proposalid", p.getProposalid());
			rttr.addAttribute("mid", mid);

			return "redirect:selectOnePbListwithComment.bo";
		} catch (DeleteException e) {
			model.addAttribute("msg", "삭제 실패!!");
			return "common/errorPage";
		}


	}

	@RequestMapping("updatePlusInfo.bo")
	public String updatePlusInfo(HttpServletRequest request, SelectOneSocialBoard2 ssb2, SelectOneSocialBoard3 ssb3,
			String updatePlusInfo, Model model, RedirectAttributes rttr) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		ssb2.setMid(mid);

		ssb2.setPlusinfo(updatePlusInfo);

		try {
			bs.updatePlusInfo(ssb2);

			rttr.addAttribute("friendid", ssb2.getFriendid());

			return "redirect: selectOneSocialListApply.bo";

		} catch (UpdateException e) {

			model.addAttribute("msg", "수정 실패!!");
			return "common/errorPage";
		}

	}

	@RequestMapping("cancelApply.bo")
	public String cancelApply(HttpServletRequest request, RedirectAttributes rttr, SelectOneSocialBoard2 ssb2, int fmemberid, Model model, int friendid) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		ssb2.setMid(mid);
		ssb2.setFmemberid(fmemberid);

		try {			
			bs.cancelApply(ssb2);

			rttr.addAttribute("friendid", friendid);
			rttr.addAttribute("fmemberid", fmemberid);

			return "redirect:selectOneSocialListApply.bo";

		} catch (UpdateException e) {
			model.addAttribute("msg", "신청 취소 실패!!");
			return "common/errorPage";
		}	


	}
}