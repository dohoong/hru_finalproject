package com.hello.hru.humanResource.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.common.ApprovalLine;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.CommonsUtils;
import com.hello.hru.humanResource.model.service.EvaluationService;
import com.hello.hru.humanResource.model.service.MovementService;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.Movement;
import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.service.MemberService;
import com.hello.hru.member.model.vo.HrInfo;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberHrInfo;

@Controller
public class MovementController {
	@Autowired
	private AuthorityService as;
	@Autowired
	private MovementService ms;
	@Autowired
	private MemberService mes;
	@Autowired
	private EvaluationService es;

	@GetMapping("showMemberMovement.mov")
	public String showMemberMovement(HttpServletRequest request, Model model) {
		//System.out.println("인사이동을 해봅시다. 하핳!");

		int mid=0;
		if(request.getSession().getAttribute("loginUser")!=null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		
		String authority = as.checkAuthority(mid, 36);
		if(authority.equals("Y")) {
			HrInfo hi = mes.selectHRInformation();
			
			//권한을 가지고 있는 팀, 사업장 코드와 이름을 가져오기!
			ArrayList<HashMap<String, String>> tlist = as.checkAuthorityTeam(mid, 10);
			
			
			hi.setTlist(new ArrayList<HashMap<String, String>>());
			hi.setGlist(new ArrayList<HashMap<String, String>>());
			for(int j=0; j<tlist.size(); j++) {
				HashMap<String, String> hmap2 = new HashMap<>();
				hi.getTlist().add(tlist.get(j));
				
				hmap2.put("groupCode", tlist.get(j).get("groupCode"));
				hmap2.put("groupName", tlist.get(j).get("groupName"));
				if(j>0) {
					
					int toggle = 0;
					for(int k=0; k<hi.getGlist().size(); k++) {
						if(hmap2.get("groupCode").equals(hi.getGlist().get(k).get("groupCode"))) {
							toggle = 1;
						}
					}
					if(toggle==0) {
						hi.getGlist().add(hmap2);
					}
				} else {
					hi.getGlist().add(hmap2);
				}
			}
			
			model.addAttribute("hi", hi);
			
			ArrayList<String> yaerlist = ms.selectListYearMov();
			//System.out.println(yaerlist);
			ArrayList<Movement> movlist = ms.selectListAllMovement();
			//System.out.println(movlist);
			
			model.addAttribute("yearlist", yaerlist);
			model.addAttribute("movlist", movlist);
			
			return "humanResource/memberMovement";
			
		} else {
			return "common/noAuthority";
		}
		
	}
	
	@PostMapping("showMemberInfo.mov")
	public ModelAndView showMemberInformation(HttpServletRequest request, ModelAndView mv) {
		
		int mid = Integer.parseInt(request.getParameter("mid"));
		
		MemberHrInfo mhi = ms.selectMemberInformation(mid);
		
		mv.addObject("mhi", mhi);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("insert.mov")
	public String insertMovement(Movement mov, HttpServletRequest request, @RequestParam(name="appFile") MultipartFile appFile, Model model) {
		//System.out.println(mov);
		//System.out.println(request.getParameter("title"));
		if(request.getParameter("title").equals("")) {
			//System.out.println("여기는 그냥 저장!");
			int result = 0;
			if(mov.getMovementId()==0) {
				result = ms.insertMovement(mov);
			} else {
				result = ms.updateMovement(mov);
			}
		} else {
			//System.out.println("여기는 결재를 같이하는 저장!");
			int result = 0;

			//전자결재를 위한 정보
			int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
			String title = request.getParameter("title");
			//System.out.println("ApprovalBasic : " + ab);
			int firstApproval = 0;
			int secondApproval = 0;
			int thirdApproval = 0;
			
			if(request.getParameter("firstApproval")!=null) {
				firstApproval = Integer.parseInt(request.getParameter("firstApproval"));				
			}
			if(request.getParameter("secondApproval")!=null) {
				secondApproval = Integer.parseInt(request.getParameter("secondApproval"));				
			}
			if(request.getParameter("thirdApproval")!=null) {
				thirdApproval = Integer.parseInt(request.getParameter("thirdApproval"));				
			}
			
			
			Approval app = new ApprovalLine().makingApprovalLine(3, mid, title, firstApproval, secondApproval, thirdApproval);
			//System.out.println(app);
			
			//파일에 대한 정보
			String root = request.getSession().getServletContext().getRealPath("resources");
			
			String os = System.getProperty("os.name").toLowerCase();
			//System.out.println(os);
			String sl = "";
			if(os.equals("mac os x")) {
				sl = "/";
			} else {
				sl = "\\";
			}
			
			try {
				String filePath = root + sl + "uploadFiles";
				Attachment att = null;

				if(!appFile.getOriginalFilename().equals("")) { //전자결재 첨부파일!
					att = new Attachment();
					String originFileName = appFile.getOriginalFilename();
					String ext = originFileName.substring(originFileName.lastIndexOf("."));
					String changeName = CommonsUtils.getRandomString();
					
					att.setOriginName(originFileName);
					att.setChangeName(changeName);
					att.setPath(filePath);
					att.setCategory(6);
					att.setFileLevel(0);
					
					appFile.transferTo(new File(filePath + sl + changeName + ext));
				}
					
					if(mov.getMovementId()==0) {
						//System.out.println("인서트+결재");
						result = ms.insertMovementWithApproval(mov, app, att);
					} else {
						//System.out.println("업데이트+결재");
						result = ms.updateMovementWithApproval(mov, app, att);
					}
					
			} catch(IllegalStateException | IOException e) {
				//new File(filePath + sl + changeName + ext).delete();
				
				model.addAttribute("msg", "파일업로드에 실패하였습니다.");
				
				return "common/errorPage";
			}
			
		}
		
		return "redirect:/showMemberMovement.mov";
	}
	
	@PostMapping("selectMemMovInfo.mov")
	public ModelAndView selectMemberMovementInfo(HttpServletRequest request, ModelAndView mv) {
		
		int mid = Integer.parseInt(request.getParameter("mid"));
		int movementId = Integer.parseInt(request.getParameter("movementId"));
		
		
		HashMap<String, Object> mMap = ms.selectMemberMovement(mid, movementId);
		
		
		mv.addObject("mMap", mMap);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@GetMapping("searchYear.mov")
	public ModelAndView selectYearList(HttpServletRequest request, ModelAndView mv) {
		
		String year = request.getParameter("year");
		
		
		ArrayList<Movement> movlist = ms.selectListYearMovement(year);
		
		mv.addObject("movlist", movlist);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("showInfoPersonalHR.mov")
	public String selectInfoPersonalHR(HttpServletRequest request, Model m) {
		ArrayList<Movement> hrList = new ArrayList<>();
		ArrayList<Movement> prList = new ArrayList<>();
		ArrayList<Evaluation> evalList = new ArrayList<>();
		
		int mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		int result = 0;
		
		ArrayList<Movement> mList = ms.selectMMList(mid);
		
		if(mList.size() > 0) {
			for(Movement mml : mList) {
				if(mml.getCategory() < 5) {
					hrList.add(mml);
				}else {
					prList.add(mml);
				}
			}
		}
		
		evalList = es.selectMemberEval(mid);
		
		m.addAttribute("evalList", evalList);
		m.addAttribute("hrList", hrList);
		m.addAttribute("prList", prList);
		
		return "member/infoPersonalHR";
	}
	
	@RequestMapping("updateEvalPersonal.mov")
	public String updateEvalPersonal(HttpServletRequest request) {
		ArrayList<HashMap<String, String>> edMap = new ArrayList<>();
		int result = 0;
		
		String[] id = request.getParameterValues("evalId");
		String[] ropinion = request.getParameterValues("ropinion");
		
		if(id.length > 0) {
			for(int i = 0; i < id.length; i++) {
				HashMap<String, String> hm = new HashMap<>();
				
				hm.put("edId", id[i]);
				hm.put("ropinion", ropinion[i]);
				
				edMap.add(hm);
			}
			
			result = es.updateEvaluationDetailPersonal(edMap);
		}
		
		if(result > 0) {
			return "성공";
		}else {
			return "실패";
		}
		
	}
}











