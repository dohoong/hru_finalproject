package com.hello.hru.humanResource.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.common.ApprovalLine;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.CommonsUtils;
import com.hello.hru.common.PageInfo;
import com.hello.hru.common.Pagination;
import com.hello.hru.humanResource.model.service.EvaluationService;
import com.hello.hru.humanResource.model.service.HumanResourceService;
import com.hello.hru.humanResource.model.service.MovementService;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.GroupAndTeam;
import com.hello.hru.humanResource.model.vo.Movement;
import com.hello.hru.humanResource.model.vo.TeamPlanDetail;
import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.service.MemberService;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberAdd;
import com.hello.hru.member.model.vo.MemberFamily;
import com.hello.hru.member.model.vo.MemberInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
public class HumanResourceController {
	
	@Autowired
	private HumanResourceService hrs;
	@Autowired
	private AuthorityService as;
	@Autowired
	private MemberService ms;
	@Autowired
	private MovementService mms;
	@Autowired
	private EvaluationService es;
	
	@RequestMapping("showEvaluateMember.hr")
	public String selectTeamList(HttpServletRequest request,Model model) {
		int mid=0;
		if(request.getSession().getAttribute("loginUser")!=null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		
		String authority = as.checkAuthority(mid, 35);
		if(authority.equals("Y")) {
			
			ArrayList<HashMap<String, String>> tlist = as.checkAuthorityTeam(mid, 35);
			ArrayList<HashMap<String, String>> glist = new ArrayList<HashMap<String, String>>();
			
			
			for(HashMap<String, String> e : tlist) {
				int gCode = Integer.parseInt(e.get("groupCode"));
				
				int count = 0;
				for(HashMap<String, String> g : glist) {
					if(Integer.parseInt(g.get("groupCode")) == gCode) {
						count++;
					}
				}
				if(count == 0) {
					glist.add(e);
				}
			}
			model.addAttribute("glist", glist);
			model.addAttribute("tlist", tlist);
			
			return "humanResource/evaluateMember";
		} else {
			return "common/noAuthority";
		}

	}
	
	@RequestMapping("showEvaluateList.hr")
	public ModelAndView selectOneEval(HttpServletRequest request, EvaluationBasic eb, ModelAndView mv) {
		eb.setTeamCode(Integer.parseInt(request.getParameter("teamCode")));
		eb.setEvalYear(Integer.parseInt(request.getParameter("year")));
		ArrayList<EvaluationBasic> evalList = hrs.selectOneEval(eb);
		
		//System.out.println("컨트롤러 - " + evalList);
		mv.addObject("evalList", evalList);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	@RequestMapping("insertEvaluateList.hr")
	public ModelAndView insertEvaluateList(HttpServletRequest request, ModelAndView mv) {
		int result = 0;
		int resultD = 0;
		ArrayList<EvaluationBasic> evalList = new ArrayList<> ();
		
		String[] evalCode = request.getParameterValues("evalCode");
		String[] typeB = request.getParameterValues("typeB"); 
		String[] nameB = request.getParameterValues("nameB");
		String[] weightB = request.getParameterValues("weightB");
		String[] contentB = request.getParameterValues("contentB");
		String[] typeO = request.getParameterValues("typeO");
		String[] nameO = request.getParameterValues("nameO");
		String[] weightO = request.getParameterValues("weightO");
		String[] contentOA = request.getParameterValues("contentOA");
		String[] contentOB = request.getParameterValues("contentOB");
		String[] contentOC = request.getParameterValues("contentOC");
		String[] contentOD = request.getParameterValues("contentOD");
		String[] contentOE = request.getParameterValues("contentOE");
		
		if(evalCode != null) {
			resultD = hrs.deleteEval(evalCode);
		}

		if(typeB != null) {
			for(int i = 0; i<typeB.length; i++) {
				EvaluationBasic eb = new EvaluationBasic();
				eb.setType(Integer.parseInt(typeB[i]));
				eb.setEvalName(nameB[i]);
				eb.setEvalContent(contentB[i]);
				eb.setEvalWeight(Integer.parseInt(weightB[i]));
				eb.setCategory(1);
				eb.setTeamCode(Integer.parseInt(request.getParameter("teamCode")));
				eb.setEvalYear(Integer.parseInt(request.getParameter("evalYear")));
				
				evalList.add(eb);
			}
		}
		if(typeO != null) {
			for(int i = 0; i<typeO.length; i++) {
				EvaluationBasic eb = new EvaluationBasic();
				eb.setType(Integer.parseInt(typeO[i]));
				eb.setEvalName(nameO[i]);
				String ContentO = contentOA[i] + "#" + contentOB[i] + "#" + contentOC[i] + "#" + contentOD[i] + "#" + contentOE[i];
				eb.setEvalContent(ContentO);
				eb.setEvalWeight(Integer.parseInt(weightO[i]));
				eb.setCategory(2);
				eb.setTeamCode(Integer.parseInt(request.getParameter("teamCode")));
				eb.setEvalYear(Integer.parseInt(request.getParameter("evalYear")));
				
				evalList.add(eb);
			}
		}
		System.out.println(evalList);
		result = hrs.insertEval(evalList);
		
		mv.addObject("text", "저장됬습니다.");
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	@RequestMapping("insertEvalApproval.hr")
	public ModelAndView insertEvalApproval(HttpServletRequest request, ApprovalBasic ab, ModelAndView mv) {
		int result = 0;
		int resultD = 0;
		int resultErr = 0;
		ArrayList<EvaluationBasic> evalList = new ArrayList<> ();
		
		String[] evalCode = request.getParameterValues("evalCode");
		String[] typeB = request.getParameterValues("typeB"); 
		String[] nameB = request.getParameterValues("nameB");
		String[] weightB = request.getParameterValues("weightB");
		String[] contentB = request.getParameterValues("contentB");
		String[] typeO = request.getParameterValues("typeO");
		String[] nameO = request.getParameterValues("nameO");
		String[] weightO = request.getParameterValues("weightO");
		String[] contentOA = request.getParameterValues("contentOA");
		String[] contentOB = request.getParameterValues("contentOB");
		String[] contentOC = request.getParameterValues("contentOC");
		String[] contentOD = request.getParameterValues("contentOD");
		String[] contentOE = request.getParameterValues("contentOE");
		
		if(evalCode != null) {
			for(int i = 0; i<evalCode.length; i++) {
				int result1 = hrs.deleteEval(evalCode);
				if(result1 > 0) {
					resultD++;
				} else {
					resultErr++;
				}
			}
		}
		if(typeB != null) {
			for(int i = 0; i<typeB.length; i++) {
				EvaluationBasic eb = new EvaluationBasic();
				eb.setType(Integer.parseInt(typeB[i]));
				eb.setEvalName(nameB[i]);
				eb.setEvalContent(contentB[i]);
				eb.setEvalWeight(Integer.parseInt(weightB[i]));
				eb.setCategory(1);
				eb.setTeamCode(Integer.parseInt(request.getParameter("teamCode")));
				eb.setEvalYear(Integer.parseInt(request.getParameter("evalYear")));
				
				evalList.add(eb);
			}
		}
		if(typeO != null) {
			for(int i = 0; i<typeO.length; i++) {
				EvaluationBasic eb = new EvaluationBasic();
				eb.setType(Integer.parseInt(typeO[i]));
				eb.setEvalName(nameO[i]);
				String ContentO = contentOA[i] + "#" + contentOB[i] + "#" + contentOC[i] + "#" + contentOD[i] + "#" + contentOE[i];
				eb.setEvalContent(ContentO);
				eb.setEvalWeight(Integer.parseInt(weightO[i]));
				eb.setCategory(2);
				eb.setTeamCode(Integer.parseInt(request.getParameter("teamCode")));
				eb.setEvalYear(Integer.parseInt(request.getParameter("evalYear")));
				
				evalList.add(eb);
			}
			
		}
		

		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		System.out.println("ApprovalBasic : " + ab);
		
		Approval app = new ApprovalLine().makingApprovalLine(4, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		
		result = hrs.insertEvalApp(evalList, app);
		
		if(resultErr > 0) {
			String fail = resultErr + "개의 항목 수정에 실패했습니다.";
			mv.addObject("text", fail);
			mv.addObject("icon", "error");
		} else {
			String success = "성공했습니다.";
			mv.addObject("text", success);
			mv.addObject("icon", "success");
		}
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	@RequestMapping("showEnterpriseMemberMain.hr")
	public String showEnterpriseMemberMain(HttpServletRequest request, Model model) {
		int mid=0;
		if(request.getSession().getAttribute("loginUser")!=null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		
		String authority = as.checkAuthority(mid, 34);
		if(authority.equals("Y")) {
			ArrayList<HashMap<String, Object>> teamList = hrs.selectTeamPlanList();
			ArrayList<TeamPlanDetail> tplist = hrs.selectNextTeamPlan();
			HashMap<String, Object> hrplan = hrs.selectHRPlan();
			if(hrplan==null) {
				hrs.insertHRPlan();
				hrplan = hrs.selectHRPlan();
			}
			model.addAttribute("hrplan", hrplan);
			model.addAttribute("tlist", teamList);
			model.addAttribute("tplist", tplist);
			
			return "humanResource/enterpriseMember";
		} else {
			return "redirect:/showEvaluateMember.hr";
		}
	}
	
	@RequestMapping("showEnterpriseMember.hr")
	public String showEnterpriseMember(HttpServletRequest request, Model model) {
		int mid=0;
		if(request.getSession().getAttribute("loginUser")!=null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		
		
		String authority = as.checkAuthority(mid, 34);
		if(authority.equals("Y")) {
			ArrayList<HashMap<String, Object>> teamList = hrs.selectTeamPlanList();
			ArrayList<TeamPlanDetail> tplist = hrs.selectNextTeamPlan();
			HashMap<String, Object> hrplan = hrs.selectHRPlan();
			if(hrplan==null) {
				hrs.insertHRPlan();
				hrplan = hrs.selectHRPlan();
			}
			model.addAttribute("hrplan", hrplan);
			model.addAttribute("tlist", teamList);
			model.addAttribute("tplist", tplist);
			
			return "humanResource/enterpriseMember";
		}else {
			return "common/noAuthority";
		}
		
	}
	
	@RequestMapping("selectDetailPlan.hr")
	public ModelAndView selectDetailPlan(HttpServletRequest request, ModelAndView mv) {
		
		int pdetailId = Integer.parseInt(request.getParameter("pdetailId"));
		
		TeamPlanDetail tpd = hrs.selectDetailTeamPlan(pdetailId);
		
		mv.addObject("tpd", tpd);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("updateHRPlan.hr")
	public String updateHRPlan(TeamPlanDetail tpdetail) {
		System.out.println("******Controller : " + tpdetail);
		
		int result = 0;
		if(tpdetail.getPdetailId()==0) { //인서트 해야함
			result = hrs.insertHRPlanDetail(tpdetail);
			
		} else { //업데이트 해야함
			result = hrs.updateHRPlanDetail(tpdetail);	
		}
		
		
		return "redirect:/showEnterpriseMember.hr";
	}
	
	@RequestMapping("deleteHRPlan.hr")
	public String deleteHRPlan(HttpServletRequest request) {
		int pdetailId = Integer.parseInt(request.getParameter("num"));
		
		int result = hrs.deleteHRPlanDetail(pdetailId);
		
		return "redirect:/showEnterpriseMember.hr";
	}
	
	@RequestMapping("approvalHRPlan.hr")
	public String updateHRPlan(Model model, ApprovalBasic ab, HttpServletRequest request, @RequestParam(name="appFile") MultipartFile appFile) {
		
		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		//System.out.println("ApprovalBasic : " + ab);
		int hrplanId = Integer.parseInt(request.getParameter("hrplanId"));
		System.out.println("hrplanId : "+ hrplanId);
				
		Approval app = new ApprovalLine().makingApprovalLine(2, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		System.out.println(app);
				
		//파일에 대한 정보
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String os = System.getProperty("os.name").toLowerCase();
		//System.out.println(os);
		String sl = "";
		if(os.equals("mac os x")) {
			sl = "/";
		} else {
			sl = "\\";
		}
		
		try {
			String filePath = root + sl + "uploadFiles";
			Attachment att = null;

			if(!appFile.getOriginalFilename().equals("")) { //전자결재 첨부파일!
				att = new Attachment();
				String originFileName = appFile.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();
				
				att.setOriginName(originFileName);
				att.setChangeName(changeName);
				att.setPath(filePath);
				att.setCategory(6);
				att.setFileLevel(0);
				
				appFile.transferTo(new File(filePath + sl + changeName + ext));
			}
			
			
			int result = hrs.updateHRPlan(hrplanId, app, att);
			
			
			return "redirect:/showEnterpriseMember.hr";

				
		} catch(IllegalStateException | IOException e) {
			//new File(filePath + sl + changeName + ext).delete();
			
			model.addAttribute("msg", "파일업로드에 실패하였습니다.");
			
			return "common/errorPage";
		}
	}
	
	@RequestMapping("insertOneTeam.hr")
	public ModelAndView insertOneTeam(HttpServletRequest request, ModelAndView mv) {
		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		int peopleNum = Integer.parseInt(request.getParameter("peopleNum"));
		int lmid = Integer.parseInt(request.getParameter("lmid"));
		
		TeamPlanDetail tpd = new TeamPlanDetail();
		tpd.setTeamCode(teamCode);
		tpd.setPeopleNum(peopleNum);
		tpd.setMid(lmid);
		
		
		int pdetailId = hrs.insertOneTeamPlanDefault(tpd);
		
		mv.addObject("pdetailId", pdetailId);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("deleteOneTeam.hr")
	public ModelAndView deleteOneTeam(HttpServletRequest request, ModelAndView mv) {
		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		
		int result = hrs.deleteOneTeam(teamCode);
		
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("insertAllTeamPlan.hr")
	public ModelAndView insertAllTeamPlan(@RequestParam String jsonData, ModelAndView mv) {
		//System.out.println(jsonData);
		JSONArray array = JSONArray.fromObject(jsonData);

		ArrayList<TeamPlanDetail> tplist = new ArrayList<>();
		for(int i=0; i<array.size(); i++) {
			 TeamPlanDetail tpd = new TeamPlanDetail();
			 JSONArray innerArr = JSONArray.fromObject(array.get(i));
			 
			 tpd.setTeamCode(innerArr.getInt(0));
			 tpd.setPeopleNum(innerArr.getInt(4));
			 tpd.setMid(innerArr.getInt(5));
			 tplist.add(tpd);
		}
		
		ArrayList<Integer> idlist = hrs.insertAllTeamPlan(tplist);
		mv.addObject("idlist", idlist);
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("deleteAllTeamPlan.hr")
	public ModelAndView deleteAllTeamPlan(@RequestParam String jsonData, ModelAndView mv) {
		System.out.println(jsonData);
		JSONArray array = JSONArray.fromObject(jsonData);
		
		int result = hrs.deleteAllTeamPlan(array);
		
		if(result>0) {
			mv.setViewName("jsonView");
		}
		return mv;
	}
	
	@RequestMapping("showMemberList.hr")
	public String showApprovalList(HttpServletRequest request, Model model) {
		//currentPage에 대한 처리
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int listCount = hrs.getListCount();
		
		//System.out.println("listCount : " + listCount);
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		ArrayList<HashMap<String, Object>> allist = hrs.selectListAllMember(pi);
		
		//System.out.println("allist : " +allist);
		//System.out.println("pi : " + pi);
			
		model.addAttribute("allist", allist);
		model.addAttribute("pi", pi);
			
		return "humanResource/memberList";
	}
	
	@RequestMapping("showMemberDetail.hr")
	public ModelAndView showMemberDetail(HttpServletRequest request, ModelAndView mv) {
		int mid = 0;
		int fmid = Integer.parseInt(request.getParameter("fmid"));
		int count = 0;
		ArrayList<HashMap<String, String>> tlist = new ArrayList<>();

		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		String authority = as.checkAuthority(mid, 11);
		if(authority.equals("Y")) {
			tlist = as.checkAuthorityTeam(mid, 11);
			//System.out.println(tlist);
			
			HashMap<String, Object> fCode = hrs.getTeamCode(fmid);
			//System.out.println(fCode);
			
			for(HashMap<String, String> t : tlist) {
				int oCode = Integer.parseInt(t.get("teamCode"));
				int pCode = (int) fCode.get("teamCode");
				if(oCode == pCode) {
					count++;
				} 
			}
		}
		if(count > 0) {
			mv.addObject("access", "Y");
		}else{
			mv.addObject("access", "N");
		}
		mv.setViewName("jsonView");
		
		return mv;
	}

	
	
	@RequestMapping("memberInfo.hr")
	public String memberInfo(HttpServletRequest request, Model m) {
		int mid = 0;
		ArrayList<MemberAdd> maList = new ArrayList<>();
		ArrayList<MemberAdd> eduList = new ArrayList<>();
		ArrayList<MemberAdd> careerList = new ArrayList<>();
		ArrayList<MemberAdd> cerfiList = new ArrayList<>();
		ArrayList<MemberAdd> languageList = new ArrayList<>();
		ArrayList<MemberFamily> mfList = new ArrayList<>();
		ArrayList<Movement> hrList = new ArrayList<>();
		ArrayList<Movement> prList = new ArrayList<>();
		ArrayList<Evaluation> evalList = new ArrayList<>();
		ArrayList<Movement> mList = new ArrayList<>();
		
		MemberInfo mi = new MemberInfo();
		mid = Integer.parseInt(request.getParameter("mid"));
		mList = mms.selectMMList(mid);
		
		if(mList.size() > 0) {
			for(Movement mml : mList) {
				if(mml.getCategory() < 5) {
					hrList.add(mml);
				}else {
					prList.add(mml);
				}
			}
		}
		
		mi = ms.selectMemberInfo(mid);
		mfList = ms.selectMemberFamily(mid);
		maList = ms.selectMemberAdd(mid);
		evalList = es.selectMemberEval(mid);
		
		if(maList.size() > 0) {
			for(MemberAdd ma : maList) {
				switch(ma.getCategory()) {
					case 1: eduList.add(ma); break;
					case 2: careerList.add(ma); break;
					case 3: cerfiList.add(ma); break;
					case 4: languageList.add(ma); break;
				}
			}
		}
		
		m.addAttribute("mi", mi);
		m.addAttribute("mfList", mfList);
		m.addAttribute("eduList", eduList);
		m.addAttribute("careerList", careerList);
		m.addAttribute("cerfiList", cerfiList);
		m.addAttribute("languageList", languageList);
		m.addAttribute("evalList", evalList);
		m.addAttribute("hrList", hrList);
		m.addAttribute("prList", prList);
		
		return "humanResource/memberDetail";
	}
	
	@RequestMapping("showSearchList.hr")
	public String showSearchList(HttpServletRequest request, Model model) {
		HashMap<String, Object> search = new HashMap<>();
		search.put("category", Integer.parseInt(request.getParameter("category")));
		search.put("serchVal", request.getParameter("searchVal"));
		//currentPage에 대한 처리
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		int listCount = hrs.getSearchCount(search);
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		ArrayList<HashMap<String, Object>> allist = hrs.selectSearchMember(search, pi);
		
		model.addAttribute("allist", allist);
		model.addAttribute("pi", pi);
			
		return "humanResource/memberList";
	}
}











