package com.hello.hru.humanResource.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.common.ApprovalLine;
import com.hello.hru.humanResource.model.service.EvaluationService;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.EvaluationDetail;
import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.vo.Member;

@Controller
public class EvaluationController {
	@Autowired
	private AuthorityService aus;
	@Autowired
	private EvaluationService es;
	
	@RequestMapping("showEvaluationList.eval")
	public String showEvaluationList(HttpServletRequest request, Model model) {
		int  mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		

		String authority = aus.checkAuthority(mid, 4);
		
		if(authority.equals("Y")) {
			ArrayList<HashMap<String, String>> tlist = aus.checkAuthorityTeam(mid, 4);
			ArrayList<HashMap<String, Object>> plist = es.selectPositionList();
			model.addAttribute("plist", plist);
			//System.out.println("tlist : " +tlist);
			//System.out.println("plist : " +plist);
			
			ArrayList<EvaluationBasic> eblist = es.selectEvaluationBasic(tlist);
			//System.out.println("eblist : " + eblist);
			if(eblist.size()< 1) {
				//인사평가 기본정보가 없을 때!
				model.addAttribute("msg", "인사평가 기본정보가 생성되지 않았습니다.");
			} else {
				//인사평가 기본정보가 있을 때!
				//System.out.println(eblist);
				ArrayList<Evaluation> evalist = es.selectEvaluationList(tlist, eblist);
				//System.out.println("Controller : " + evalist);
				if(evalist.size()<1) {
					//EVALUATION이 아직 생성 전일 때,
					evalist = es.insertEvaluations(tlist, eblist);
				}
				//System.out.println(evalist);
				model.addAttribute("evalist", evalist);
			}
		}
		
		return "leader/evaluationList";
	}
	
	@PostMapping("selectEvaluationDetail.eval")
	public ModelAndView selectEvaluationDetail(HttpServletRequest request, ModelAndView mv) {
		int evaluationId = Integer.parseInt(request.getParameter("evaluationId"));
		Evaluation eval = es.selectevaluation(evaluationId);
		ArrayList<EvaluationDetail> edlist = es.selectEvaluationDetail(evaluationId);
		//System.out.println("Controller1:::::" + edlist);
		if(edlist.size() < 1) {
			//자세한 정보가 없을 때, 새로 인서트 + 셀렉트
			edlist = es.insertEvaluationDetail(evaluationId);
			//System.out.println("Contoller2::::::" + edlist);
		}
		
		mv.addObject("eval", eval);
		mv.addObject("edlist", edlist);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("updateEvalPlan.eval")
	public String updateEvaluationPlan(Evaluation evaluation) {
		System.out.println(evaluation);
		int result = es.updateEvaluationPlan(evaluation);
		
		
		return "redirect:/showEvaluationList.eval";
	}
	
	@PostMapping("updateEvalwithApp.eval")
	public String updateEvaluationWithApp(Evaluation evaluation, ApprovalBasic ab, HttpServletRequest request) {
		//System.out.println("Controller : " +ed.getEdlist());
		//System.out.println("Evaluation : " + evaluation);
//		for(int i=0; i<evaluation.getEdlist().size();i++) {
//			System.out.println(i + ": " + evaluation.getEdlist().get(i));
//		}
		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
				
				
		Approval app = new ApprovalLine().makingApprovalLine(10, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		//System.out.println("Approval : " + app);
		
		int result = es.updateEvaluationwithApp(evaluation, app);
		
		
		
		return "redirect:/showEvaluationList.eval";
	}
	
	@PostMapping("search.eval")
	public ModelAndView searchEvaluation(HttpServletRequest request, ModelAndView mv) {
		int  mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		ArrayList<HashMap<String, String>> tlist = aus.checkAuthorityTeam(mid, 4);

		String mName = request.getParameter("mName");
		int year = Integer.parseInt(request.getParameter("year"));
		int ppCode = Integer.parseInt(request.getParameter("ppCode"));
		
		HashMap<String, Object> searchMap = new HashMap<>();
		searchMap.put("mName", mName);
		searchMap.put("year", year);
		searchMap.put("ppCode", ppCode);
		
		
		ArrayList<Evaluation> evalist = es.searchEvaluation(searchMap, tlist);
		
		
		mv.addObject("evalist", evalist);
		mv.setViewName("jsonView");
		return mv;
	}
}








