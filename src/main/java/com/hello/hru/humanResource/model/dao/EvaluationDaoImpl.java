package com.hello.hru.humanResource.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.EvaluationDetail;

@Repository
public class EvaluationDaoImpl implements EvaluationDao{

	@Override
	public ArrayList<EvaluationBasic> selectEvaluationBasic(SqlSessionTemplate sqlSession, int teamCode) {
		return (ArrayList)sqlSession.selectList("HumanResource.selectEvalBasicListTeam", teamCode);
	}

	@Override
	public ArrayList<Evaluation> selectEvaluationList(SqlSessionTemplate sqlSession, int teamCode) {
		return (ArrayList)sqlSession.selectList("HumanResource.selectEvaluationListByTeam", teamCode);
	}

	@Override
	public ArrayList<Integer> selectMidByTeam(SqlSessionTemplate sqlSession, int teamCode) {
		return (ArrayList)sqlSession.selectList("HumanResource.selectListMidByTeam", teamCode);
	}

	@Override
	public int insertEvaluation(SqlSessionTemplate sqlSession, int mid) {
		Evaluation eval = new Evaluation();
		eval.setMid(mid);
		sqlSession.insert("HumanResource.insertEvaluation", eval);
		
		return eval.getEvaluationId();
	}

	@Override
	public Evaluation selectEvaluation(SqlSessionTemplate sqlSession, int evaluationId) {
		return sqlSession.selectOne("HumanResource.selectEvaluation", evaluationId);
	}

	@Override
	public ArrayList<EvaluationDetail> selectEvaluationDetail(SqlSessionTemplate sqlSession, int evaluationId) {
		return (ArrayList)sqlSession.selectList("HumanResource.selectEvaluationDetail", evaluationId);
	}

	@Override
	public int selectTeamByEvaluation(SqlSessionTemplate sqlSession, int evaluationId) {
		return sqlSession.selectOne("HumanResource.selectMidByEvaluation", evaluationId);
	}

	@Override
	public int insertEvaluationDetail(SqlSessionTemplate sqlSession, EvaluationDetail eDetail) {
		sqlSession.insert("HumanResource.insertEvaluationDetail", eDetail);
		
		return eDetail.getEdId();
	}

	@Override
	public EvaluationDetail selectEvaluationDetailByEdid(SqlSessionTemplate sqlSession, int edId) {
		return sqlSession.selectOne("HumanResource.selectEvaluationDetailByEdid", edId);
	}

	@Override
	public int updateEvaluationPlan(SqlSessionTemplate sqlSession, EvaluationDetail evaluationDetail) {
		return sqlSession.update("HumanResource.updateEvalDetailPlan", evaluationDetail);
	}

	@Override
	public ArrayList<Evaluation> selectMemberEval(SqlSessionTemplate sqlSession, int mid) {
		return (ArrayList)sqlSession.selectList("HumanResource.selectMemberEval", mid);
	}

	@Override
	public int updateEDPersonal(SqlSessionTemplate sqlSession, EvaluationDetail evalDetail) {
		return sqlSession.update("HumanResource.updateEDPersonal", evalDetail);
	}
	
	@Override
	public int updateEvaluationAppYN(SqlSessionTemplate sqlSession, Evaluation evaluation) {
		return sqlSession.update("HumanResource.updateEvaluationAppYn", evaluation);
	}

	@Override
	public int updateEvaluationScore(SqlSessionTemplate sqlSession, EvaluationDetail evaluationDetail) {
		return sqlSession.update("HumanResource.updateEvaluationScore", evaluationDetail);
	}

	@Override
	public int updateEvaluationWithApp(SqlSessionTemplate sqlSession, Evaluation evaluation) {
		return sqlSession.update("HumanResource.updateEvaluationWithApp", evaluation);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectPositionList(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("HumanResource.selectPositionList");
	}

	@Override
	public ArrayList<Evaluation> searchEvaluation(SqlSessionTemplate sqlSession, HashMap<String, Object> searchMap) {
		return (ArrayList)sqlSession.selectList("HumanResource.searchEvaluation", searchMap);
	}

}
