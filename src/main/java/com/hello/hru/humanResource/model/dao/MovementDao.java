package com.hello.hru.humanResource.model.dao;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.humanResource.model.vo.Movement;
import com.hello.hru.member.model.vo.MemberHrInfo;

public interface MovementDao {

	ArrayList<Movement> selectListAllMovement(SqlSessionTemplate sqlSession);

	MemberHrInfo selectMemberInformation(SqlSessionTemplate sqlSession, int mid);

	int insertMovement(SqlSessionTemplate sqlSession, Movement mov);

	Movement selectMemberMovement(SqlSessionTemplate sqlSession, int movementId);

	ArrayList<String> selectListYearMov(SqlSessionTemplate sqlSession);

	int updateMovement(SqlSessionTemplate sqlSession, Movement mov);

	int insertMovementWithApproval(SqlSessionTemplate sqlSession, Movement mov);

	int updateMovementWithApproval(SqlSessionTemplate sqlSession, Movement mov);

	ArrayList<Movement> selectListOneDay(SqlSessionTemplate sqlSession, String daystr);

	int updateHRInfo(SqlSessionTemplate sqlSession, Movement movement);

	int updateMovementState(SqlSessionTemplate sqlSession, Movement movement);

	ArrayList<Movement> selectListYearMovement(SqlSessionTemplate sqlSession, String year);

	ArrayList<Movement> selectMMList(SqlSessionTemplate sqlSession, int mid);

}
