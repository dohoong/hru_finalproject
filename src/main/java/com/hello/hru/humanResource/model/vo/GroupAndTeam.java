package com.hello.hru.humanResource.model.vo;

import java.util.ArrayList;
import java.util.HashMap;

public class GroupAndTeam {
	private ArrayList<HashMap<String, String>> tlist;
	private ArrayList<HashMap<String, String>> glist;
	
	public GroupAndTeam () {}

	public GroupAndTeam(ArrayList<HashMap<String, String>> tlist, ArrayList<HashMap<String, String>> glist) {
		super();
		this.tlist = tlist;
		this.glist = glist;
	}

	public ArrayList<HashMap<String, String>> getTlist() {
		return tlist;
	}

	public void setTlist(ArrayList<HashMap<String, String>> tlist) {
		this.tlist = tlist;
	}

	public ArrayList<HashMap<String, String>> getGlist() {
		return glist;
	}

	public void setGlist(ArrayList<HashMap<String, String>> glist) {
		this.glist = glist;
	}

	@Override
	public String toString() {
		return "GroupAndTeam [tlist=" + tlist + ", glist=" + glist + "]";
	}
	
}
