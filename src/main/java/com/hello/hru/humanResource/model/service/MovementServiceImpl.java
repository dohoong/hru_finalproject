package com.hello.hru.humanResource.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.approval.model.dao.ApprovalDao;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.common.Attachment;
import com.hello.hru.humanResource.model.dao.MovementDao;
import com.hello.hru.humanResource.model.vo.Movement;
import com.hello.hru.member.model.dao.MemberDao;
import com.hello.hru.member.model.vo.MemberHrInfo;

@Service
public class MovementServiceImpl implements MovementService{
	@Autowired
	private MovementDao md;
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private ApprovalDao ad;
	@Autowired
	private MemberDao med;

	@Override
	public ArrayList<Movement> selectListAllMovement() {
		return md.selectListAllMovement(sqlSession);
	}

	@Override
	public MemberHrInfo selectMemberInformation(int mid) {
		return md.selectMemberInformation(sqlSession, mid);
	}

	@Override
	public int insertMovement(Movement mov) {
		return md.insertMovement(sqlSession, mov);
	}

	@Override
	public HashMap<String, Object> selectMemberMovement(int mid, int movementId) {
		HashMap<String, Object> mMap = new HashMap<String, Object>();
		
		//해당 회원의 정보
		MemberHrInfo mhi = md.selectMemberInformation(sqlSession, mid);
		mMap.put("mhi", mhi);
		
		//이동하려는 정보
		Movement mov = md.selectMemberMovement(sqlSession, movementId);
		mMap.put("mov", mov);
		
		return mMap;
	}

	@Override
	public ArrayList<String> selectListYearMov() {
		return md.selectListYearMov(sqlSession);
	}

	@Override
	public int updateMovement(Movement mov) {
		return md.updateMovement(sqlSession, mov);
	}

	@Override
	public int insertMovementWithApproval(Movement mov, Approval app, Attachment att) {
		int result = 0;
		
		int result1 = ad.insertApproval(sqlSession, app);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					//System.out.println("Service Apphis : " +apphis);
					result12 = ad.insertApprovalHistory(sqlSession, apphis);
				}
				mov.setApprovalYn("N");
			} else {
				result12 = 1;
				mov.setApprovalYn("Y"); //결재가 필요없을 경우라면, Y:승인으로 바꿈!
			}
		}
		
		//전자결재있는 인서트!
		mov.setApprovalId(result1);
		int result2 = md.insertMovementWithApproval(sqlSession, mov);
		
		int result3 = 0;
		if(att != null) {
			//첨부파일 인서트!!
			att.setApprovalId(result1);
			result3 = med.insertAttachment(sqlSession, att);
		}
		
		if(result1>0 && result2>0 && result3>0) {
			result = 1;
		}
		return result;
	}

	@Override
	public int updateMovementWithApproval(Movement mov, Approval app, Attachment att) {
		int result = 0;
		
		int result1 = ad.insertApproval(sqlSession, app);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					System.out.println("Service Apphis : " +apphis);
					result12 = ad.insertApprovalHistory(sqlSession, apphis);
				}
				mov.setApprovalYn("N");
			} else {
				result12 = 1;
				mov.setApprovalYn("Y"); //결재가 필요없을 경우라면, Y:승인으로 바꿈!
			}
		}
		
		//전자결재있는 업데이트
		mov.setApprovalId(result1);
		System.out.println("Service mov : " + mov);
		int result2 = md.updateMovementWithApproval(sqlSession, mov);
		
		int result3 = 0;
		if(att != null) {
			//첨부파일 인서트!!
			att.setApprovalId(result1);
			result3 = med.insertAttachment(sqlSession, att);
		}
		
		if(result1>0 && result2>0 && result3>0) {
			result = 1;
		}
		return result;
	}

	@Override
	public ArrayList<Movement> selectListYearMovement(String year) {
		return md.selectListYearMovement(sqlSession, year);
	}

	@Override
	public ArrayList<Movement> selectMMList(int mid) {
		return md.selectMMList(sqlSession, mid);
	}

}
