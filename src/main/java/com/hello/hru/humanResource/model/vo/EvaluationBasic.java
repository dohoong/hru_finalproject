package com.hello.hru.humanResource.model.vo;

public class EvaluationBasic {
	private int evalCode;
	private String evalName;
	private String evalContent;
	private int evalWeight;
	private int category;
	private int type;
	private int evalYear;
	private String approvalYN;
	private int approvalId;
	private int teamCode;
	
	public EvaluationBasic () {}

	public EvaluationBasic(int evalCode, String evalName, int evalYear, String evalContent, int evalWeight,
			int category, int type, int approvalId, String approvalYN, int teamCode) {
		super();
		this.evalCode = evalCode;
		this.evalName = evalName;
		this.evalContent = evalContent;
		this.evalWeight = evalWeight;
		this.category = category;
		this.type = type;
		this.evalYear = evalYear;
		this.approvalYN = approvalYN;
		this.approvalId = approvalId;
		this.teamCode = teamCode;
	}

	public int getEvalCode() {
		return evalCode;
	}

	public void setEvalCode(int evalCode) {
		this.evalCode = evalCode;
	}

	public String getEvalName() {
		return evalName;
	}

	public void setEvalName(String evalName) {
		this.evalName = evalName;
	}

	public int getEvalYear() {
		return evalYear;
	}

	public void setEvalYear(int evalYear) {
		this.evalYear = evalYear;
	}

	public String getEvalContent() {
		return evalContent;
	}

	public void setEvalContent(String evalContent) {
		this.evalContent = evalContent;
	}

	public int getEvalWeight() {
		return evalWeight;
	}

	public void setEvalWeight(int evalWeight) {
		this.evalWeight = evalWeight;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public String getApprovalYN() {
		return approvalYN;
	}

	public void setApprovalYN(String approvalYN) {
		this.approvalYN = approvalYN;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	@Override
	public String toString() {
		return "EvaluationBasic [evalCode=" + evalCode + ", evalName=" + evalName + ", evalYear=" + evalYear
				+ ", evalContent=" + evalContent + ", evalWeight=" + evalWeight + ", category=" + category + ", type="
				+ type + ", approvalId=" + approvalId + ", approvalYN=" + approvalYN + ", teamCode=" + teamCode + "]";
	}

	
}
