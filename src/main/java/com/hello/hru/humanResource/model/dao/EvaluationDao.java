package com.hello.hru.humanResource.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.EvaluationDetail;

public interface EvaluationDao {

	ArrayList<EvaluationBasic> selectEvaluationBasic(SqlSessionTemplate sqlSession, int teamCode);

	ArrayList<Evaluation> selectEvaluationList(SqlSessionTemplate sqlSession, int teamCode);

	ArrayList<Integer> selectMidByTeam(SqlSessionTemplate sqlSession, int teamCode);

	int insertEvaluation(SqlSessionTemplate sqlSession, int mid);

	Evaluation selectEvaluation(SqlSessionTemplate sqlSession, int evaluationId);

	ArrayList<EvaluationDetail> selectEvaluationDetail(SqlSessionTemplate sqlSession, int evaluationId);

	int selectTeamByEvaluation(SqlSessionTemplate sqlSession, int evaluationId);

	int insertEvaluationDetail(SqlSessionTemplate sqlSession, EvaluationDetail evaluationDetail);

	EvaluationDetail selectEvaluationDetailByEdid(SqlSessionTemplate sqlSession, int edId);

	int updateEvaluationPlan(SqlSessionTemplate sqlSession, EvaluationDetail evaluationDetail);

	ArrayList<Evaluation> selectMemberEval(SqlSessionTemplate sqlSession, int mid);

	int updateEDPersonal(SqlSessionTemplate sqlSession, EvaluationDetail evalDetail);

	int updateEvaluationAppYN(SqlSessionTemplate sqlSession, Evaluation evaluation);

	int updateEvaluationScore(SqlSessionTemplate sqlSession, EvaluationDetail evaluationDetail);

	int updateEvaluationWithApp(SqlSessionTemplate sqlSession, Evaluation evaluation);

	ArrayList<HashMap<String, Object>> selectPositionList(SqlSessionTemplate sqlSession);

	ArrayList<Evaluation> searchEvaluation(SqlSessionTemplate sqlSession, HashMap<String, Object> searchMap);

}
