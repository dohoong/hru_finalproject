package com.hello.hru.humanResource.model.vo;

public class Movement {
	private int movementId;
	private int mid;
	private String mName;
	private int category;
	private String moveDate;
	private String requestDate;
	private String reason;
	private int teamCode;
	private String teamName;
	private int jobCode;
	private String jobName;
	private String approvalYn;
	private int rCode;
	private String rName;
	private int approvalId;
	private int positionCode;
	private String positionName;
	private int ppCode;
	private int enterCode;
	private int groupCode;
	private String groupName;
	
	public Movement() {}

	public Movement(int movementId, int mid, String mName, int category, String moveDate, String requestDate,
			String reason, int teamCode, String teamName, int jobCode, String jobName, String approvalYn, int rCode,
			String rName, int approvalId, int positionCode, String positionName, int ppCode, int enterCode,
			int groupCode, String groupName) {
		super();
		this.movementId = movementId;
		this.mid = mid;
		this.mName = mName;
		this.category = category;
		this.moveDate = moveDate;
		this.requestDate = requestDate;
		this.reason = reason;
		this.teamCode = teamCode;
		this.teamName = teamName;
		this.jobCode = jobCode;
		this.jobName = jobName;
		this.approvalYn = approvalYn;
		this.rCode = rCode;
		this.rName = rName;
		this.approvalId = approvalId;
		this.positionCode = positionCode;
		this.positionName = positionName;
		this.ppCode = ppCode;
		this.enterCode = enterCode;
		this.groupCode = groupCode;
		this.groupName = groupName;
	}

	public int getMovementId() {
		return movementId;
	}

	public void setMovementId(int movementId) {
		this.movementId = movementId;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getMoveDate() {
		return moveDate;
	}

	public void setMoveDate(String moveDate) {
		this.moveDate = moveDate;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getJobCode() {
		return jobCode;
	}

	public void setJobCode(int jobCode) {
		this.jobCode = jobCode;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getApprovalYn() {
		return approvalYn;
	}

	public void setApprovalYn(String approvalYn) {
		this.approvalYn = approvalYn;
	}

	public int getrCode() {
		return rCode;
	}

	public void setrCode(int rCode) {
		this.rCode = rCode;
	}

	public String getrName() {
		return rName;
	}

	public void setrName(String rName) {
		this.rName = rName;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public int getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(int positionCode) {
		this.positionCode = positionCode;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public int getPpCode() {
		return ppCode;
	}

	public void setPpCode(int ppCode) {
		this.ppCode = ppCode;
	}

	public int getEnterCode() {
		return enterCode;
	}

	public void setEnterCode(int enterCode) {
		this.enterCode = enterCode;
	}

	public int getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(int groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public String toString() {
		return "Movement [movementId=" + movementId + ", mid=" + mid + ", mName=" + mName + ", category=" + category
				+ ", moveDate=" + moveDate + ", requestDate=" + requestDate + ", reason=" + reason + ", teamCode="
				+ teamCode + ", teamName=" + teamName + ", jobCode=" + jobCode + ", jobName=" + jobName
				+ ", approvalYn=" + approvalYn + ", rCode=" + rCode + ", rName=" + rName + ", approvalId=" + approvalId
				+ ", positionCode=" + positionCode + ", positionName=" + positionName + ", ppCode=" + ppCode
				+ ", enterCode=" + enterCode + ", groupCode=" + groupCode + ", groupName=" + groupName + "]";
	}

}
