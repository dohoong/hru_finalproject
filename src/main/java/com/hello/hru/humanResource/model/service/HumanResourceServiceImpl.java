package com.hello.hru.humanResource.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.approval.model.dao.ApprovalDao;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.PageInfo;
import com.hello.hru.humanResource.model.dao.HumanResourceDao;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.GroupAndTeam;
import com.hello.hru.humanResource.model.vo.HRPlan;
import com.hello.hru.humanResource.model.vo.TeamPlanDetail;
import com.hello.hru.member.model.dao.MemberDao;

import net.sf.json.JSONArray;

@Service
public class HumanResourceServiceImpl implements HumanResourceService {

	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private HumanResourceDao hrd;
	@Autowired
	private ApprovalDao ad;
	@Autowired
	private MemberDao med;


	//사업장 및 부서 조회
	@Override
	public GroupAndTeam selectTeamList() {
		GroupAndTeam gt = new GroupAndTeam();
		
		ArrayList<HashMap<String, String>> tlist = hrd.selectListTeam(sqlSession);
		gt.setTlist(tlist);
		
		ArrayList<HashMap<String, String>> glist = hrd.selectListGroup(sqlSession);
		gt.setGlist(glist);
		
		return gt;
	}

	//해당년도 부서 인사평가기록 조회
	@Override
	public ArrayList<EvaluationBasic> selectOneEval(EvaluationBasic eb) {
		ArrayList<EvaluationBasic> evalList = new ArrayList<>();
		
		evalList = hrd.selectOneEval(sqlSession, eb);
		
		return evalList;
	}

	//평가 추가
	@Override
	public int insertEval(ArrayList<EvaluationBasic> evalList) {
		int result = 0;
		for(EvaluationBasic eb : evalList) {			
			result += hrd.insertEval(sqlSession, eb);
		}
		
		return result;
	}
	
	//평가 추가(전자결재)
	@Override
	public int insertEvalApp(ArrayList<EvaluationBasic> evalList, Approval app) {
		int result = 0;

		int result1 = ad.insertApproval(sqlSession, app);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					result12 = ad.insertApprovalHistory(sqlSession, apphis);
				}
				for(EvaluationBasic eb : evalList) {
					eb.setApprovalYN("N");
					eb.setApprovalId(result1);
					result += hrd.insertEvalApp(sqlSession, eb);
				}
			} else {
				result12 = 1;
				for(EvaluationBasic eb : evalList) {
					eb.setApprovalYN("Y");
					eb.setApprovalId(result1);
					result += hrd.insertEvalApp(sqlSession, eb);
				}
			}
		}
		
		
		
		return result;
	}
	
	//평가 삭제
	@Override
	public int deleteEval(String[] evalCode) {
		int result = 0;
		
		for(String ec : evalCode) {			
			result += hrd.deleteEval(sqlSession, Integer.parseInt(ec));
		}
		
		return result;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectTeamPlanList() {
		return hrd.selectTeamPlanList(sqlSession);
	}

	@Override
	public ArrayList<TeamPlanDetail> selectNextTeamPlan() {
		return hrd.selectNextTeamPlan(sqlSession);
	}

	@Override
	public TeamPlanDetail selectDetailTeamPlan(int pdetailId) {
		return hrd.selectTeamPlanDetail(sqlSession, pdetailId);
	}

	@Override
	public int insertHRPlanDetail(TeamPlanDetail tpdetail) {
		return hrd.insertHRPlanDetail(sqlSession, tpdetail);
	}

	@Override
	public int updateHRPlanDetail(TeamPlanDetail tpdetail) {
		return hrd.updateHRPlanDetail(sqlSession, tpdetail);
	}

	@Override
	public int selectHRPlanId() {
		return hrd.selectHRPlanId(sqlSession);
	}

	@Override
	public int insertHRPlan() {
		return hrd.insertHRPlan(sqlSession);
	}

	@Override
	public int deleteHRPlanDetail(int pdetailId) {
		return hrd.deleteHRPlan(sqlSession, pdetailId);
	}

	@Override
	public HashMap<String, Object> selectHRPlan() {
		return hrd.selectHRPlan(sqlSession);
	}

	@Override
	public int updateHRPlan(int hrplanId, Approval app, Attachment att) {
		int result = 0;
		HRPlan hrplan = new HRPlan();
		hrplan.setHrplanId(hrplanId);
		
		int result1 = ad.insertApproval(sqlSession, app);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					//System.out.println("Service Apphis : " +apphis);
					result12 = ad.insertApprovalHistory(sqlSession, apphis);
				}
				hrplan.setApprovalYn("N");
			} else {
				result12 = 1;
				hrplan.setApprovalYn("Y");
			}
		}
		
		//업데이트!
		hrplan.setApprovalId(result1);
		int result2 = hrd.updateHRPlanApproval(sqlSession, hrplan);
		
		int result3 = 0;
		if(att != null) {
			//첨부파일 인서트!!
			att.setApprovalId(result1);
			result3 = med.insertAttachment(sqlSession, att);
		}
		
		if(result1>0 && result2>0 && result3>0) {
			result = 1;
		}
		return result;
	}

	@Override
	public int insertOneTeamPlanDefault(TeamPlanDetail tpd) {
		return hrd.insertOneTeamPlanDefault(sqlSession, tpd);
	}

	@Override
	public int deleteOneTeam(int teamCode) {
		return hrd.deleteOneTeam(sqlSession, teamCode);
	}

	@Override
	public ArrayList<Integer> insertAllTeamPlan(ArrayList<TeamPlanDetail> tplist) {
		
		ArrayList<Integer> idlist = new ArrayList<>();
		for(int i=0; i<tplist.size();i++) {
			int result = hrd.insertOneTeamPlanDefault(sqlSession, tplist.get(i));
			
			idlist.add(result);
		}
		return idlist;
	}

	@Override
	public int deleteAllTeamPlan(JSONArray array) {
		int result = 0;
		for(int i=0; i<array.size();i++) {
			result += hrd.deleteOneTeam(sqlSession, array.getInt(i));
		}
		
		return result;
	}

	@Override
	public HashMap<String, Object> selectHRPlanHis(int approvalId) {
		return hrd.selectHRPlanHis(sqlSession, approvalId);
	}

	@Override
	public ArrayList<TeamPlanDetail> selectNextTeamPlanHis(int hrplanId) {
		return hrd.selectNextTeamPlanHis(sqlSession, hrplanId);
	}

	@Override
	public int getListCount() {
		return hrd.getListCount(sqlSession);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListAllMember(PageInfo pi) {
		return hrd.selectListAllMember(sqlSession, pi);
	}

	@Override
	public HashMap<String, Object> getTeamCode(int fmid) {
		return hrd.getTeamCode(sqlSession, fmid);
	}

	@Override
	public int getSearchCount(HashMap<String, Object> search) {
		return hrd.getSearchCount(sqlSession, search);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectSearchMember(HashMap<String, Object> search, PageInfo pi) {
		return hrd.selectSearchMember(sqlSession, search, pi);
	}



	

}
