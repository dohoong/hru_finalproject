package com.hello.hru.humanResource.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.common.PageInfo;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.HRPlan;
import com.hello.hru.humanResource.model.vo.TeamPlanDetail;

@Repository
public class HumanResourceDaoImpl implements HumanResourceDao{
	

	@Override
	public ArrayList<HashMap<String, String>> selectListTeam(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Member.selectListTeam");
	}

	@Override
	public ArrayList<HashMap<String, String>> selectListGroup(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Member.selectListGroup");
	}

	@Override
	public ArrayList<EvaluationBasic> selectOneEval(SqlSessionTemplate sqlSession, EvaluationBasic eb) {
		return (ArrayList) sqlSession.selectList("HumanResource.selectOneEval", eb);
	}

	@Override
	public int insertEval(SqlSessionTemplate sqlSession, EvaluationBasic eb) {
		return sqlSession.insert("HumanResource.insertEval", eb);
	}

	@Override
	public int insertEvalApp(SqlSessionTemplate sqlSession, EvaluationBasic eb) {
		return sqlSession.insert("HumanResource.insertEvalApp", eb);
	}
	
	@Override
	public int deleteEval(SqlSessionTemplate sqlSession, int evalCode) {
		return sqlSession.delete("HumanResource.deleteEval", evalCode);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectTeamPlanList(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("HumanResource.selectTeamPlanList");
	}

	@Override
	public ArrayList<TeamPlanDetail> selectNextTeamPlan(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("HumanResource.selectNextTeamPlan");
	}

	@Override
	public TeamPlanDetail selectTeamPlanDetail(SqlSessionTemplate sqlSession, int pdetailId) {
		return sqlSession.selectOne("HumanResource.selectTeamPlanDetail", pdetailId);
	}

	@Override
	public int selectHRPlanId(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("HumanResource.selectHRPlanId");
	}

	@Override
	public int insertHRPlan(SqlSessionTemplate sqlSession) {
		return sqlSession.insert("HumanResource.insertHRPlan");
	}

	@Override
	public int insertHRPlanDetail(SqlSessionTemplate sqlSession, TeamPlanDetail tpdetail) {
		return sqlSession.insert("HumanResource.insertHRPlanDetail", tpdetail);
	}

	@Override
	public int updateHRPlanDetail(SqlSessionTemplate sqlSession, TeamPlanDetail tpdetail) {
		return sqlSession.update("HumanResource.updateHRPlanDetail", tpdetail);
	}

	@Override
	public int deleteHRPlan(SqlSessionTemplate sqlSession, int pdetailId) {
		return sqlSession.delete("HumanResource.deleteHRPlanDetail", pdetailId);
	}

	@Override
	public HashMap<String, Object> selectHRPlan(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("HumanResource.selectHRPlan");
	}
	
	@Override
	public int updateHRPlanApproval(SqlSessionTemplate sqlSession, HRPlan hrplan) {
		return sqlSession.update("HumanResource.updateHRPlanApproval", hrplan);
		
	}

	@Override
	public int insertOneTeamPlanDefault(SqlSessionTemplate sqlSession, TeamPlanDetail tpd) {
		sqlSession.insert("HumanResource.insertOneTeamDefault", tpd);
		return tpd.getPdetailId();
	}

	@Override
	public int deleteOneTeam(SqlSessionTemplate sqlSession, int teamCode) {
		return sqlSession.delete("HumanResource.deleteOneTeam", teamCode);
	}

	@Override
	public HashMap<String, Object> selectHRPlanHis(SqlSessionTemplate sqlSession, int approvalId) {
		return sqlSession.selectOne("HumanResource.selectOneByAppId", approvalId);
	}

	@Override
	public ArrayList<TeamPlanDetail> selectNextTeamPlanHis(SqlSessionTemplate sqlSession, int hrplanId) {
		return (ArrayList) sqlSession.selectList("HumanResource.selectListByhrplanId", hrplanId);
	}

	@Override
	public int getListCount(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("HumanResource.getListCount");
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectListAllMember(SqlSessionTemplate sqlSession, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return (ArrayList)sqlSession.selectList("HumanResource.selectListAllMember", null ,rowBounds);
	}

	@Override
	public HashMap<String, Object> getTeamCode(SqlSessionTemplate sqlSession, int fmid) {
		return sqlSession.selectOne("HumanResource.getTeamCode", fmid);
	}

	@Override
	public int getSearchCount(SqlSessionTemplate sqlSession, HashMap<String, Object> search) {
		return sqlSession.selectOne("HumanResource.getSearchCount", search);
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectSearchMember(SqlSessionTemplate sqlSession, HashMap<String, Object> search, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return (ArrayList)sqlSession.selectList("HumanResource.selectListSearchMember", search ,rowBounds);
	}


}
