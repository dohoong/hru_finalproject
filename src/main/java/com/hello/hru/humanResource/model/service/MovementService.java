package com.hello.hru.humanResource.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.common.Attachment;
import com.hello.hru.humanResource.model.vo.Movement;
import com.hello.hru.member.model.vo.MemberHrInfo;

public interface MovementService {

	ArrayList<Movement> selectListAllMovement();

	MemberHrInfo selectMemberInformation(int mid);

	int insertMovement(Movement mov);

	HashMap<String, Object> selectMemberMovement(int mid, int movementId);

	ArrayList<String> selectListYearMov();

	int updateMovement(Movement mov);

	int insertMovementWithApproval(Movement mov, Approval app, Attachment att);

	int updateMovementWithApproval(Movement mov, Approval app, Attachment att);

	ArrayList<Movement> selectListYearMovement(String year);

	ArrayList<Movement> selectMMList(int mid);

}
