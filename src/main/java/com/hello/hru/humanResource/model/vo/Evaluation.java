package com.hello.hru.humanResource.model.vo;

import java.util.ArrayList;

public class Evaluation {
	private int evaluationId;
	private int mid;
	private String mName;
	private String ppName;
	private String approvalYn;
	private double totalScore;
	private int approvalId;
	private int evaluationYear;
	private String levelupYn;
	private double salaryRate;
	private ArrayList<EvaluationDetail> edlist;
	
	public Evaluation() {}
	
	



	public Evaluation(int evaluationId, int mid, String mName, String ppName, String approvalYn, double totalScore,
			int approvalId, int evaluationYear, String levelupYn, double salaryRate,
			ArrayList<EvaluationDetail> edlist) {
		super();
		this.evaluationId = evaluationId;
		this.mid = mid;
		this.mName = mName;
		this.ppName = ppName;
		this.approvalYn = approvalYn;
		this.totalScore = totalScore;
		this.approvalId = approvalId;
		this.evaluationYear = evaluationYear;
		this.levelupYn = levelupYn;
		this.salaryRate = salaryRate;
		this.edlist = edlist;
	}





	public int getEvaluationId() {
		return evaluationId;
	}

	public void setEvaluationId(int evaluationId) {
		this.evaluationId = evaluationId;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getApprovalYn() {
		return approvalYn;
	}

	public void setApprovalYn(String approvalYn) {
		this.approvalYn = approvalYn;
	}

	public double getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(double totalScore) {
		this.totalScore = totalScore;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public int getEvaluationYear() {
		return evaluationYear;
	}

	public void setEvaluationYear(int evaluationYear) {
		this.evaluationYear = evaluationYear;
	}

	public String getLevelupYn() {
		return levelupYn;
	}

	public void setLevelupYn(String levelupYn) {
		this.levelupYn = levelupYn;
	}

	public double getSalaryRate() {
		return salaryRate;
	}

	public void setSalaryRate(double salaryRate) {
		this.salaryRate = salaryRate;
	}
	
	
	
	public ArrayList<EvaluationDetail> getEdlist() {
		return edlist;
	}



	public void setEdlist(ArrayList<EvaluationDetail> edlist) {
		this.edlist = edlist;
	}

	public String getmName() {
		return mName;
	}





	public void setmName(String mName) {
		this.mName = mName;
	}





	public String getPpName() {
		return ppName;
	}





	public void setPpName(String ppName) {
		this.ppName = ppName;
	}





	@Override
	public String toString() {
		return "Evaluation [evaluationId=" + evaluationId + ", mid=" + mid + ", mName=" + mName + ", ppName=" + ppName
				+ ", approvalYn=" + approvalYn + ", totalScore=" + totalScore + ", approvalId=" + approvalId
				+ ", evaluationYear=" + evaluationYear + ", levelupYn=" + levelupYn + ", salaryRate=" + salaryRate
				+ ", edlist=" + edlist + "]";
	}
	

	
}
