package com.hello.hru.humanResource.model.vo;

import java.util.ArrayList;

public class EvaluationDetail {
	private int edId;
	private String edContent;
	private String ropinion;
	private String gopinion;
	private int evalScore;
	private int evaluationId;
	private int evalCode;
	private String evalName;
	private String evalContent;
	private int evalWeight;
	private int category;
	private int type;
	private ArrayList<EvaluationDetail> edlist;
	
	public EvaluationDetail() {}

	public EvaluationDetail(int edId, String edContent, String ropinion, String gopinion, int evalScore,
			int evaluationId, int evalCode, String evalName, String evalContent, int evalWeight, int category, int type,
			ArrayList<EvaluationDetail> edlist) {
		super();
		this.edId = edId;
		this.edContent = edContent;
		this.ropinion = ropinion;
		this.gopinion = gopinion;
		this.evalScore = evalScore;
		this.evaluationId = evaluationId;
		this.evalCode = evalCode;
		this.evalName = evalName;
		this.evalContent = evalContent;
		this.evalWeight = evalWeight;
		this.category = category;
		this.type = type;
		this.edlist = edlist;
	}


	public int getEdId() {
		return edId;
	}

	public void setEdId(int edId) {
		this.edId = edId;
	}

	public String getEdContent() {
		return edContent;
	}

	public void setEdContent(String edContent) {
		this.edContent = edContent;
	}

	public String getRopinion() {
		return ropinion;
	}

	public void setRopinion(String ropinion) {
		this.ropinion = ropinion;
	}

	public String getGopinion() {
		return gopinion;
	}

	public void setGopinion(String gopinion) {
		this.gopinion = gopinion;
	}

	public int getEvalScore() {
		return evalScore;
	}

	public void setEvalScore(int evalScore) {
		this.evalScore = evalScore;
	}

	public int getEvaluationId() {
		return evaluationId;
	}

	public void setEvaluationId(int evaluationId) {
		this.evaluationId = evaluationId;
	}

	public int getEvalCode() {
		return evalCode;
	}

	public void setEvalCode(int evalCode) {
		this.evalCode = evalCode;
	}
	

	public String getEvalName() {
		return evalName;
	}



	public void setEvalName(String evalName) {
		this.evalName = evalName;
	}



	public String getEvalContent() {
		return evalContent;
	}



	public void setEvalContent(String evalContent) {
		this.evalContent = evalContent;
	}



	public int getEvalWeight() {
		return evalWeight;
	}



	public void setEvalWeight(int evalWeight) {
		this.evalWeight = evalWeight;
	}



	public int getCategory() {
		return category;
	}



	public void setCategory(int category) {
		this.category = category;
	}



	public int getType() {
		return type;
	}



	public void setType(int type) {
		this.type = type;
	}
	
	public ArrayList<EvaluationDetail> getEdlist() {
		return edlist;
	}

	public void setEdlist(ArrayList<EvaluationDetail> edlist) {
		this.edlist = edlist;
	}

	@Override
	public String toString() {
		return "EvaluationDetail [edId=" + edId + ", edContent=" + edContent + ", ropinion=" + ropinion + ", gopinion="
				+ gopinion + ", evalScore=" + evalScore + ", evaluationId=" + evaluationId + ", evalCode=" + evalCode
				+ ", evalName=" + evalName + ", evalContent=" + evalContent + ", evalWeight=" + evalWeight
				+ ", category=" + category + ", type=" + type + ", edlist=" + edlist + "]";
	}

	
	
}
