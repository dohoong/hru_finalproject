package com.hello.hru.humanResource.model.vo;

public class TeamPlanDetail {
	private int pdetailId;
	private int hrplanId;
	private int mid;
	private String lname;
	private int teamCode;
	private String teamName;
	private String gName;
	private int peopleNum;
	private int addNum;
	private int outNum;
	private int currentNum;
	private String detail;
	
	public TeamPlanDetail() {}

	

	public TeamPlanDetail(int pdetailId, int hrplanId, int mid, String lname, int teamCode, String teamName,
			String gName, int peopleNum, int addNum, int outNum, int currentNum, String detail) {
		super();
		this.pdetailId = pdetailId;
		this.hrplanId = hrplanId;
		this.mid = mid;
		this.lname = lname;
		this.teamCode = teamCode;
		this.teamName = teamName;
		this.gName = gName;
		this.peopleNum = peopleNum;
		this.addNum = addNum;
		this.outNum = outNum;
		this.currentNum = currentNum;
		this.detail = detail;
	}



	public int getPdetailId() {
		return pdetailId;
	}

	public void setPdetailId(int pdetailId) {
		this.pdetailId = pdetailId;
	}

	public int getHrplanId() {
		return hrplanId;
	}

	public void setHrplanId(int hrplanId) {
		this.hrplanId = hrplanId;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getgName() {
		return gName;
	}

	public void setgName(String gName) {
		this.gName = gName;
	}

	public int getPeopleNum() {
		return peopleNum;
	}

	public void setPeopleNum(int peopleNum) {
		this.peopleNum = peopleNum;
	}

	public int getAddNum() {
		return addNum;
	}

	public void setAddNum(int addNum) {
		this.addNum = addNum;
	}

	public int getOutNum() {
		return outNum;
	}

	public void setOutNum(int outNum) {
		this.outNum = outNum;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	

	public int getCurrentNum() {
		return currentNum;
	}



	public void setCurrentNum(int currentNum) {
		this.currentNum = currentNum;
	}



	@Override
	public String toString() {
		return "TeamPlanDetail [pdetailId=" + pdetailId + ", hrplanId=" + hrplanId + ", mid=" + mid + ", lname=" + lname
				+ ", teamCode=" + teamCode + ", teamName=" + teamName + ", gName=" + gName + ", peopleNum=" + peopleNum
				+ ", addNum=" + addNum + ", outNum=" + outNum + ", currentNum=" + currentNum + ", detail=" + detail
				+ "]";
	}
	
	
}
