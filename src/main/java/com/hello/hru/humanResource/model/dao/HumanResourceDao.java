package com.hello.hru.humanResource.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.common.PageInfo;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.HRPlan;
import com.hello.hru.humanResource.model.vo.TeamPlanDetail;

public interface HumanResourceDao {
	
	ArrayList<EvaluationBasic> selectOneEval(SqlSessionTemplate sqlSession, EvaluationBasic eb);

	ArrayList<HashMap<String, String>> selectListTeam(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, String>> selectListGroup(SqlSessionTemplate sqlSession);

	int insertEval(SqlSessionTemplate sqlSession, EvaluationBasic eb);
	
	int insertEvalApp(SqlSessionTemplate sqlSession, EvaluationBasic eb);

	int deleteEval(SqlSessionTemplate sqlSession, int evalCode);

	ArrayList<HashMap<String, Object>> selectTeamPlanList(SqlSessionTemplate sqlSession);

	ArrayList<TeamPlanDetail> selectNextTeamPlan(SqlSessionTemplate sqlSession);

	TeamPlanDetail selectTeamPlanDetail(SqlSessionTemplate sqlSession, int pdetailId);

	int selectHRPlanId(SqlSessionTemplate sqlSession);

	int insertHRPlan(SqlSessionTemplate sqlSession);

	int insertHRPlanDetail(SqlSessionTemplate sqlSession, TeamPlanDetail tpdetail);

	int updateHRPlanDetail(SqlSessionTemplate sqlSession, TeamPlanDetail tpdetail);

	int deleteHRPlan(SqlSessionTemplate sqlSession, int pdetailId);

	HashMap<String, Object> selectHRPlan(SqlSessionTemplate sqlSession);

	int updateHRPlanApproval(SqlSessionTemplate sqlSession, HRPlan hrplan);

	int insertOneTeamPlanDefault(SqlSessionTemplate sqlSession, TeamPlanDetail tpd);

	int deleteOneTeam(SqlSessionTemplate sqlSession, int teamCode);

	HashMap<String, Object> selectHRPlanHis(SqlSessionTemplate sqlSession, int approvalId);

	ArrayList<TeamPlanDetail> selectNextTeamPlanHis(SqlSessionTemplate sqlSession, int hrplanId);

	int getListCount(SqlSessionTemplate sqlSession);

	ArrayList<HashMap<String, Object>> selectListAllMember(SqlSessionTemplate sqlSession, PageInfo pi);

	HashMap<String, Object> getTeamCode(SqlSessionTemplate sqlSession, int fmid);

	int getSearchCount(SqlSessionTemplate sqlSession, HashMap<String, Object> search);

	ArrayList<HashMap<String, Object>> selectSearchMember(SqlSessionTemplate sqlSession, HashMap<String, Object> search, PageInfo pi);


}
