package com.hello.hru.humanResource.model.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.PageInfo;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.GroupAndTeam;
import com.hello.hru.humanResource.model.vo.TeamPlanDetail;

import net.sf.json.JSONArray;

public interface HumanResourceService {

	ArrayList<EvaluationBasic> selectOneEval(EvaluationBasic eb);

	GroupAndTeam selectTeamList();

	int insertEval(ArrayList<EvaluationBasic> evalList);

	int insertEvalApp(ArrayList<EvaluationBasic> evalList, Approval app);

	int deleteEval(String[] evalCode);

	ArrayList<HashMap<String, Object>> selectTeamPlanList();

	ArrayList<TeamPlanDetail> selectNextTeamPlan();

	TeamPlanDetail selectDetailTeamPlan(int pdetailId);

	int insertHRPlanDetail(TeamPlanDetail tpdetail);

	int updateHRPlanDetail(TeamPlanDetail tpdetail);

	int selectHRPlanId();

	int insertHRPlan();

	int deleteHRPlanDetail(int pdetailId);

	HashMap<String, Object> selectHRPlan();

	int updateHRPlan(int hrplanId, Approval app, Attachment att);

	int insertOneTeamPlanDefault(TeamPlanDetail tpd);

	int deleteOneTeam(int teamCode);

	ArrayList<Integer> insertAllTeamPlan(ArrayList<TeamPlanDetail> tplist);

	int deleteAllTeamPlan(JSONArray array);

	HashMap<String, Object> selectHRPlanHis(int approvalId);

	ArrayList<TeamPlanDetail> selectNextTeamPlanHis(int hrplanId);

	int getListCount();

	ArrayList<HashMap<String, Object>> selectListAllMember(PageInfo pi);

	HashMap<String, Object> getTeamCode(int fmid);

	int getSearchCount(HashMap<String, Object> search);

	ArrayList<HashMap<String, Object>> selectSearchMember(HashMap<String, Object> search, PageInfo pi);

}
