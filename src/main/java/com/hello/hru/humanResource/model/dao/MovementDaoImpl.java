package com.hello.hru.humanResource.model.dao;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.humanResource.model.vo.Movement;
import com.hello.hru.member.model.vo.MemberHrInfo;

@Repository
public class MovementDaoImpl implements MovementDao{

	@Override
	public ArrayList<Movement> selectListAllMovement(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Movement.selectAllMovement");
	}

	@Override
	public MemberHrInfo selectMemberInformation(SqlSessionTemplate sqlSession, int mid) {
		return sqlSession.selectOne("Movement.selectMember", mid);
	}

	@Override
	public int insertMovement(SqlSessionTemplate sqlSession, Movement mov) {
		return sqlSession.insert("Movement.insertMovement", mov);
	}

	@Override
	public Movement selectMemberMovement(SqlSessionTemplate sqlSession, int movementId) {
		return sqlSession.selectOne("Movement.selectMovement", movementId);
	}

	@Override
	public ArrayList<String> selectListYearMov(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Movement.selectListYear");
	}

	@Override
	public int updateMovement(SqlSessionTemplate sqlSession, Movement mov) {
		return sqlSession.update("Movement.updateMovement", mov);
	}

	@Override
	public int insertMovementWithApproval(SqlSessionTemplate sqlSession, Movement mov) {
		return sqlSession.insert("Movement.insertMovementWithApproval", mov);
	}

	@Override
	public int updateMovementWithApproval(SqlSessionTemplate sqlSession, Movement mov) {
		return sqlSession.update("Movement.updateMovementWithApproval", mov);
	}

	@Override
	public ArrayList<Movement> selectListOneDay(SqlSessionTemplate sqlSession, String daystr) {
		return (ArrayList)sqlSession.selectList("Movement.selectListOneDay", daystr);
	}

	@Override
	public int updateHRInfo(SqlSessionTemplate sqlSession, Movement movement) {
		return sqlSession.update("Movement.updateHRInfo", movement);
	}

	@Override
	public int updateMovementState(SqlSessionTemplate sqlSession, Movement movement) {
		return sqlSession.update("Movement.updateMoveStatus", movement);
		
	}

	@Override
	public ArrayList<Movement> selectListYearMovement(SqlSessionTemplate sqlSession, String year) {
		return (ArrayList)sqlSession.selectList("Movement.selectListYearMovement", year);
	}

	@Override
	public ArrayList<Movement> selectMMList(SqlSessionTemplate sqlSession, int mid) {
		return (ArrayList)sqlSession.selectList("Movement.selectMemberMovement", mid);
	}

}
