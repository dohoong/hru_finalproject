package com.hello.hru.humanResource.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.approval.model.dao.ApprovalDao;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.humanResource.model.dao.EvaluationDao;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.EvaluationDetail;
import com.hello.hru.humanResource.model.vo.Movement;

@Service
public class EvaluationServiceImpl implements EvaluationService{

	@Autowired
	private EvaluationDao ed;
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private ApprovalDao ad;
	
	@Override
	public ArrayList<EvaluationBasic> selectEvaluationBasic(ArrayList<HashMap<String, String>> tlist) {
		ArrayList<EvaluationBasic> eblist = new ArrayList<>();
		
		for(int i=0; i<tlist.size(); i++) {
			ArrayList<EvaluationBasic> eblistTemp = ed.selectEvaluationBasic(sqlSession, Integer.parseInt(tlist.get(i).get("teamCode")));
			if(eblistTemp !=null) {
				for(int j=0; j<eblistTemp.size(); j++) {
					eblist.add(eblistTemp.get(j));
				}
			}
		}
		return eblist;
	}

	@Override
	public ArrayList<Evaluation> selectEvaluationList(ArrayList<HashMap<String, String>> tlist,
			ArrayList<EvaluationBasic> eblist) {

		ArrayList<Evaluation> evalist = new ArrayList<>();
		for(int i=0; i<tlist.size(); i++) {
			ArrayList<Evaluation> evalistTemp = ed.selectEvaluationList(sqlSession, Integer.parseInt(tlist.get(i).get("teamCode")));
			if(evalistTemp!=null) {
				for(int j=0; j<evalistTemp.size(); j++) {
					evalist.add(evalistTemp.get(j));
				}				
			}
		}
		
		return evalist;
	}

	@Override
	public ArrayList<Evaluation> insertEvaluations(ArrayList<HashMap<String, String>> tlist,
			ArrayList<EvaluationBasic> eblist) {
		
		ArrayList<Evaluation> evalist = new ArrayList<>();
		
		ArrayList<Integer> midlist = new ArrayList<>();
		for(int i=0; i<tlist.size(); i++) { //팀에 속한 사람들을 모두 가져옴!
			ArrayList<Integer> midlistTemp = ed.selectMidByTeam(sqlSession, Integer.parseInt(tlist.get(i).get("teamCode")));
			for(int j=0; j<midlistTemp.size(); j++) {
				midlist.add(midlistTemp.get(j));
			}
		}
		
		for(int i=0; i<midlist.size(); i++) {
			Evaluation eval = null;
			int evaluationId = ed.insertEvaluation(sqlSession, midlist.get(i));
			eval = ed.selectEvaluation(sqlSession, evaluationId);
			evalist.add(eval);
		}
		
		return evalist;
	}

	@Override
	public ArrayList<EvaluationDetail> selectEvaluationDetail(int evaluationId) {
		return ed.selectEvaluationDetail(sqlSession, evaluationId);
	}

	@Override
	public ArrayList<EvaluationDetail> insertEvaluationDetail(int evaluationId) {
		int teamCode = ed.selectTeamByEvaluation(sqlSession, evaluationId);
		ArrayList<EvaluationBasic> eblist = ed.selectEvaluationBasic(sqlSession, teamCode); //해당팀에 대한 올해 평가내역(베이직)
		ArrayList<EvaluationDetail> edlist = new ArrayList<>();
		
		for(int i=0; i<eblist.size(); i++) {
			EvaluationDetail edetail = new EvaluationDetail();
			edetail.setEvalCode(eblist.get(i).getEvalCode());
			edetail.setEvaluationId(evaluationId);
			
			int edId = ed.insertEvaluationDetail(sqlSession, edetail);
			edetail = ed.selectEvaluationDetailByEdid(sqlSession, edId);
			
			edlist.add(edetail);
		}
		
		
		return edlist;
	}

	@Override
	public Evaluation selectevaluation(int evaluationId) {
		return ed.selectEvaluation(sqlSession, evaluationId);
	}

	@Override
	public int updateEvaluationPlan(Evaluation evaluation) {
		int result = 0;
		for(int i=0; i<evaluation.getEdlist().size(); i++) {
			result += ed.updateEvaluationPlan(sqlSession, evaluation.getEdlist().get(i));
		}
		
		if(result>0) {
			result += ed.updateEvaluationAppYN(sqlSession, evaluation);
		}
		
		return result;
	}

	@Override
	public ArrayList<Evaluation> selectMemberEval(int mid) {
		return ed.selectMemberEval(sqlSession, mid);
	}

	@Override
	public int updateEvaluationDetailPersonal(ArrayList<HashMap<String, String>> edMap) {
		int result = 0;
		for(int i = 0; i < edMap.size(); i++) {
			EvaluationDetail e = new EvaluationDetail();
			
			e.setRopinion(edMap.get(i).get("ropinion"));
			e.setEdId(Integer.parseInt(edMap.get(i).get("edId")));
			
			result += ed.updateEDPersonal(sqlSession, e);
		}
		return result;
	}

	@Override
	public int updateEvaluationwithApp(Evaluation evaluation, Approval app) {
		int result = 0;
		//evaluation detail을 업데이트(gopinion, evalScore)
		int result1 = 0;
		for(int i=0; i<evaluation.getEdlist().size(); i++) {
			result1 += ed.updateEvaluationScore(sqlSession, evaluation.getEdlist().get(i));
		}
		//성공했을 시에 approval을 업데이트(Approval + ApprovalHistory)
		int result2 = 0;
		if(result1>0) {
			int approvalId = ad.insertApproval(sqlSession, app);
			if(approvalId>0) {
				if(app.getAhlist() != null) {
					for(int i=0; i<app.getAhlist().size(); i++) {
						ApprovalHistory apphis = app.getAhlist().get(i);
						apphis.setApprovalId(approvalId);
						//System.out.println("Service Apphis : " +apphis);
						result2 = ad.insertApprovalHistory(sqlSession, apphis);
					}
					evaluation.setApprovalYn("P");
				} else {
					result2 = 1;
					evaluation.setApprovalYn("Y"); //결재가 필요없을 경우라면, 카테고리를 Y로 바꿈!
				}
			}
			evaluation.setApprovalId(approvalId);
			
		}
		
		
		//성공했을 때, approvalId를 받아와서 evaluation에 업데이트 (totalScore, approvalId, approvalYn, levelupYn
		if(result2 > 0) {
			result = ed.updateEvaluationWithApp(sqlSession, evaluation);
		}
		
		
		return result;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectPositionList() {
		return ed.selectPositionList(sqlSession);
	}

	@Override
	public ArrayList<Evaluation> searchEvaluation(HashMap<String, Object> searchMap, ArrayList<HashMap<String, String>> tlist) {
		
		ArrayList<Evaluation> evalist = new ArrayList<>(); 
		for(int i=0; i<tlist.size(); i++) {
			if(searchMap.get("teamCode") != null) {
				searchMap.remove("teamCode");
			}
			searchMap.put("teamCode", tlist.get(i).get("teamCode"));
			ArrayList<Evaluation> templist = ed.searchEvaluation(sqlSession, searchMap);
			
			for(int j=0; j<templist.size(); j++) {
				evalist.add(templist.get(j));
			}
		}
		
		return evalist;
	}

}











