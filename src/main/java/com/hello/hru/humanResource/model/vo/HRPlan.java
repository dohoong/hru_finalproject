package com.hello.hru.humanResource.model.vo;

public class HRPlan {
	private int hrplanId;
	private int approvalId;
	private String approvalYn;
	private int hrplanYear;
	
	public HRPlan() {}

	public HRPlan(int hrplanId, int approvalId, String approvalYn, int hrplanYear) {
		super();
		this.hrplanId = hrplanId;
		this.approvalId = approvalId;
		this.approvalYn = approvalYn;
		this.hrplanYear = hrplanYear;
	}

	public int getHrplanId() {
		return hrplanId;
	}

	public void setHrplanId(int hrplanId) {
		this.hrplanId = hrplanId;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public String getApprovalYn() {
		return approvalYn;
	}

	public void setApprovalYn(String approvalYn) {
		this.approvalYn = approvalYn;
	}

	public int getHrplanYear() {
		return hrplanYear;
	}

	public void setHrplanYear(int hrplanYear) {
		this.hrplanYear = hrplanYear;
	}

	@Override
	public String toString() {
		return "HRPlan [hrplanId=" + hrplanId + ", approvalId=" + approvalId + ", approvalYn=" + approvalYn
				+ ", hrplanYear=" + hrplanYear + "]";
	}
	
	
}
