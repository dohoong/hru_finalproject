package com.hello.hru.humanResource.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.humanResource.model.vo.Evaluation;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.EvaluationDetail;

public interface EvaluationService {

	ArrayList<EvaluationBasic> selectEvaluationBasic(ArrayList<HashMap<String, String>> tlist);

	ArrayList<Evaluation> selectEvaluationList(ArrayList<HashMap<String, String>> tlist,
			ArrayList<EvaluationBasic> eblist);

	ArrayList<Evaluation> insertEvaluations(ArrayList<HashMap<String, String>> tlist,
			ArrayList<EvaluationBasic> eblist);

	ArrayList<EvaluationDetail> selectEvaluationDetail(int evaluationId);

	ArrayList<EvaluationDetail> insertEvaluationDetail(int evaluationId);

	Evaluation selectevaluation(int evaluationId);

	int updateEvaluationPlan(Evaluation evaluation);

	int updateEvaluationwithApp(Evaluation evaluation, Approval app);

	ArrayList<HashMap<String, Object>> selectPositionList();

	ArrayList<Evaluation> searchEvaluation(HashMap<String, Object> searchMap, ArrayList<HashMap<String, String>> tlist);

	ArrayList<Evaluation> selectMemberEval(int mid);

	int updateEvaluationDetailPersonal(ArrayList<HashMap<String, String>> edMap);

}
