package com.hello.hru.humanResource.exception;

public class HumanResourceException extends Exception {
	
	public HumanResourceException(String msg) {
		super(msg);
	}
}
