package com.hello.hru.attendance.controller;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.attendance.model.service.AttendanceService;
import com.hello.hru.attendance.model.vo.AnnualVacation;
import com.hello.hru.attendance.model.vo.Certification;
import com.hello.hru.attendance.model.vo.DayAttendance;
import com.hello.hru.attendance.model.vo.MemberAnnualVacation;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.MonthStatus;
import com.hello.hru.attendance.model.vo.OneDaySearch;
import com.hello.hru.attendance.model.vo.SpecialAttendance;
import com.hello.hru.attendance.model.vo.SpecialAttendanceSearch;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.attendance.model.vo.VacationBasic;
import com.hello.hru.attendance.model.vo.VacationRequest;
import com.hello.hru.common.ApprovalLine;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.CommonsUtils;
import com.hello.hru.humanResource.model.vo.EvaluationBasic;
import com.hello.hru.humanResource.model.vo.GroupAndTeam;
import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.service.MemberService;
import com.hello.hru.member.model.vo.HrInfo;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberHrInfo;
import com.hello.hru.member.model.vo.SearchMember;
import com.hello.hru.member.model.vo.TemporaryMember;
import com.hello.hru.system.model.service.SystemService;

@Controller
public class AttendanceController {
	
	@Autowired
	private AttendanceService as;
	
	@Autowired
	private AuthorityService aus;
	
	@Autowired
	private MemberService ms;
	
	@Autowired
	private SystemService ss;

	//기준정보-휴가관리 페이지 뷰
	@RequestMapping("showVacationAdministration.att")
	public String showVacationAdminisatration(Model model) {
		HashMap<String, Object> vMap = as.selectvMap();
		model.addAttribute("vMap", vMap);
		return "system/vacationAdministration";
	}
	
	//기준정보-휴가항목 설정 INSERT
	@RequestMapping("vacationBasicInsert.att")
	public String vacationBasicUpdate(Model model, VacationBasic vb) {
			if(vb.getCategory()==1) {
				vb.setSalaryYN("Y");
			}else if(vb.getCategory()==2) {
				vb.setSalaryYN("N");
			}else if(vb.getCategory()==3) {
				vb.setSalaryYN("Y");
			}
			int result = as.insertVacationBasic(vb);
		return  "redirect:/showVacationAdministration.att";
	}
	
	//기준정보 - 휴가항목 UPDATE
	@RequestMapping("vacationBasicUpdate.att")
	public String vacationBasic(VacationBasic vb) {
		if(vb.getCategory()==1) {
			vb.setSalaryYN("Y");
		}else if(vb.getCategory()==2) {
			vb.setSalaryYN("N");
		}else if(vb.getCategory()==3) {
			vb.setSalaryYN("Y");
		}
		int result = as.updateVacationBasic(vb);
		return  "redirect:/showVacationAdministration.att";
	}
	
	//기준정보 - 휴가항목 DELETE
	@RequestMapping("vacationBasicDelete.att")
	public String vacationBasicDelete(VacationBasic vb) {
		if(vb.getCategory()==1) {
			vb.setSalaryYN("Y");
		}else if(vb.getCategory()==2) {
			vb.setSalaryYN("N");
		}else if(vb.getCategory()==3) {
			vb.setSalaryYN("Y");
		}
		
		int result = as.deleteVacationBasic(vb);
		return "redirect:/showVacationAdministration.att";
	}
	
	//기준정보-연차부여일수 추가,수정
	@RequestMapping("annualVacationUpdate.att")
	public String annualVacationUpdate(Model model, AnnualVacation av) {
		int result = 0;
		//연차기준년수 check
		AnnualVacation aVacation = as.checkAvStd(av.getAnnualStd());
		if(aVacation == null) {
			//연차부여일수 INSERT
			result = as.insertAnnualVacation(av);
		}else {
			//연차부여일수 UPDATE
			System.out.println("update");
			aVacation.setVacationNum(av.getVacationNum());
			result = as.updateAnnualVacation(aVacation);
		}
		return "redirect:/showVacationAdministration.att";
	}
	
	//기준정보-연차부여일수 삭제
	@RequestMapping("annualVacationDelete.att")
	public String annualVacationDelete(AnnualVacation av) {
		int result = as.deleteAnnualVacation(av);
		return "redirect:/showVacationAdministration.att";
	}
	
	//사원 근태관리 페이지 뷰
	@RequestMapping("showInfoAttendance.att")
	public String showInfoAttendance(Model model, HttpServletRequest request) {
		int mId = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		Member m = new Member();
		m.setMid(mId);
		//ip대역폭 사용유무 
		int useYN = ss.ipUseCheck();	//사용 1, 미사용 2
		System.out.println("ip대역폭 사용 유무  : " + useYN);
		int ipCheck = 0; //일치여부 일치1, 미일치2
		int ipUse = 0;
		String ip = null;
		if(useYN == 1) {
			//ip사용
			ip = request.getHeader("X-FORWARDED-FOR");
			if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
				ip = request.getHeader("Proxy-Client-IP");
			}
			if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
				ip = request.getHeader("WL-Proxy-Client-IP");
			}
			if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
				ip = request.getHeader("HTTP_CLIENT_IP");
			}
			if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
				ip = request.getHeader("HTTP_X_FORWARDED_FOR");
			}
			if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
				ip = request.getRemoteAddr();
			}
			if("0:0:0:0:0:0:0:1".equals(ip)) {
				ip = "127.0.0.1";
			}
			String[] ipAddressTemp = ip.split("\\.");
			long ipAddressLoing = (Long.parseLong(ipAddressTemp[0])<<24)+
					(Long.parseLong(ipAddressTemp[1])<<16)+(Long.parseLong(ipAddressTemp[2])<<8)+
					(Long.parseLong(ipAddressTemp[3]));
			//ip일치 확인
			ipCheck = as.ipCheck(ipAddressLoing);
			if(ipCheck >0) {
				ipUse = 1;
			}else {
				ipUse = 0;
			}
		}
		
		//시간 check
		SimpleDateFormat format_second = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat format_date = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat format_hour = new SimpleDateFormat("HH");
		Calendar time = Calendar.getInstance();
		String now = format_second.format(time.getTime());
		String stdTime = format_date.format(time.getTime()) + " 07:00:00";
		int nowHour = Integer.parseInt(format_hour.format(time.getTime()));
		HashMap<String, Object> aMap = new HashMap<String, Object>();
		if(nowHour >= 7) {
			//7시 이후
			aMap = as.selectDayAttendance(mId);
		}else {
			//7시 이전
			aMap = as.selectbeforeAttendance(mId);
		}
		
		//사원정보 가져오기
		MemberHrInfo mhi = as.selectMemberInfo(mId);
		aMap.put("mhi", mhi);
		aMap.put("useYN",useYN);
		aMap.put("ipUse", ipUse);
		aMap.put("ip", ip);
		model.addAttribute("aMap", aMap);
		return "member/infoAttendance";
	}
	
	//사원 일일근태 - 출근버튼 클릭
	@RequestMapping("dayStart.att")
	public ModelAndView dayStartAttendance(ModelAndView mv, Model model, HttpServletRequest request) {
		int mId = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		//일근태 check
		DayAttendance da = as.dayAttendanceCheck(mId);
		if(da == null) {
			//일근태 insert
			as.insertDayAttendance(mId);
			int result = 1;
			mv.addObject("result", result); 
			mv.setViewName("jsonView"); 
			return mv;
		}else {
			model.addAttribute("msg", "이미 출근신청을 했음");
			return mv;
		}
	}
	
	//사원 일일근태 - 퇴근버튼 클릭
	@RequestMapping("dayEnd.att")
	public ModelAndView dayEndAttendance(ModelAndView mv,Model model, HttpServletRequest request) {
		int mId = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		DayAttendance da = new DayAttendance();
		da.setmId(mId);
			//시간 check
			SimpleDateFormat format_hour = new SimpleDateFormat("HH");
			Calendar time = Calendar.getInstance();
			int nowHour = Integer.parseInt(format_hour.format(time.getTime()));
			if(nowHour >= 7) {
				//7시 이후
				System.out.println(as.updateDayAttendance(da));
				
			}else {
				//7시 이전
				as.updateBeforeAttendance(mId);
			}
			
			int result = 1;
			mv.addObject("result", result); 
			mv.setViewName("jsonView"); 
			return mv;
	}
	
	//휴가관리 페이지 뷰(연차 불러오기)
	@RequestMapping("showInfoVacation.att")
	public String showInfoVacation(Model model, HttpServletRequest request) {
		//휴가정보
		int mId = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String mName = ((Member) (request.getSession().getAttribute("loginUser"))).getmName();
		Date enterDate = ((Member) (request.getSession().getAttribute("loginUser"))).getEnterDate();
		MemberAnnualVacation mv = new MemberAnnualVacation();
		mv.setmId(mId);
		mv.setmName(mName);
		mv.setEnterDate(enterDate);
		MemberAnnualVacation mav = new MemberAnnualVacation();
		mav = as.selectMemberAnnualVacation(mv);
		model.addAttribute("mav", mav);
		
		//휴가 신청 이력
		ArrayList<VacationRequest> vrList = as.selectVacationList(mId);
		HashMap<String, Object> vMap = new HashMap<String, Object>();
		vMap.put("mav", mav);
		vMap.put("vrList", vrList);
		model.addAttribute("vMap", vMap);
		return "member/infoVacation";
	}
	
	
	//휴가신청 전자결재
	@RequestMapping("insertVacationRequest.att")
	public ModelAndView insertVacationRequest(Vacation v, ApprovalBasic ab, HttpServletRequest request, ModelAndView mav) {
		
		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		v.setmId(mid);
		
		int result = 0;
		
			//휴가신청 날짜 중복확인
			v.setmId(mid);
			ArrayList<Vacation> check = as.selectDuplicateDate(v);
			System.out.println("중복확인 : " + check);
			
			if(check.size() == 0) { 
				 System.out.println("휴가 신청 가능"); 
				 v.setmId(mid);
				 //남은 연차 확인
				 if(v.getvCode()!= 1) {
					 //연차 사용 x
					 Approval app = new ApprovalLine().makingApprovalLine(8, mid, title,ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
					 System.out.println(app); 
					 //휴가신청테이블 insert 
					 as.insertVacationRequest(v, app);
					 result = 3;
				 }else {
					 //남은 연차 확인
					 int remainV = as.checkRemainV(v);
					 if(remainV > 0) {
						 //연차 사용 가능
						 Approval app = new ApprovalLine().makingApprovalLine(8, mid, title,ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
						 System.out.println(app); 
						 //휴가신청테이블 insert 
						 as.insertVacationRequest(v, app);
						 result = 3;
					 }else {
						 //연차 사용 횟수 초과
						//남은 연차 없음
						result = 1;
					 }
				
			  }
			}else { 
				System.out.println("휴가 날짜 중복"); 
				result = 2; 
			}
		
		mav.addObject("result", result); 
		mav.setViewName("jsonView"); 
		return mav;	
		//return "redirect:/showInfoVacation.att";
	}
	
	//야특근 전자결재
	@RequestMapping("insertSpecialAttendance.att")
	public ModelAndView insertSpecialAttendance(SpecialAttendance sa, ApprovalBasic ab, HttpServletRequest request, ModelAndView mv) {
		System.out.println("야특근 신청 controller sa : " + sa);
		
		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		System.out.println("ApprovalBasic : " + ab);
		sa.setmId(mid);
		
		Approval app = new ApprovalLine().makingApprovalLine(7, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		System.out.println(app);		
		//야특근 테이블 insert
		int result = as.insertSpecialVacation(sa, app);
		
		mv.addObject("result", result); 
		mv.setViewName("jsonView"); 
		return mv;
		 
		/* return "redirect:/showInfoAttendance.att"; */
	}
	
	//제증명 관리 페이지 뷰
	@RequestMapping("showCertificateManagement.att")
	public String showCertificateManagement(Model model, HttpServletRequest request){
		
		HashMap<String, Object> cfMap = new HashMap<String, Object>();
		cfMap.put("mName", ((Member) (request.getSession().getAttribute("loginUser"))).getmName());
		cfMap.put("mId", ((Member) (request.getSession().getAttribute("loginUser"))).getMid());
		cfMap.put("tName", ((Member) (request.getSession().getAttribute("loginUser"))).getMhi().getTname());
		cfMap.put("gName", ((Member) (request.getSession().getAttribute("loginUser"))).getMhi().getGname());
		cfMap.put("ppName", ((Member) (request.getSession().getAttribute("loginUser"))).getMhi().getPpname());
		
		//제증명 신청 이력
		int mId = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		ArrayList<Certification> cList = as.selectCertification(mId);
		cfMap.put("cList", cList);
		
		model.addAttribute("cfMap", cfMap);
		
		return "member/certificateManagement";
	}
	
	//제증명 전자결재
	@RequestMapping("insertCertification.att")
	public String insertCertification(Model model, Certification cf, ApprovalBasic ab, HttpServletRequest request) {
		
		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		cf.setmId(mid);
		
		Approval app = new ApprovalLine().makingApprovalLine(12, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		
		//제증명신청 테이블 insert
		as.insertCertification(cf, app);
		return "redirect:/showCertificateManagement.att";
	}
	
	//일일 근태관리 뷰
	@RequestMapping("showOneDayAttend.att")
	public String showOneDayAttend(Model model, HttpServletRequest request) {
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		String authority = aus.checkAuthority(mid, 3);
	      if(authority.equals("Y")) {
	         
	         ArrayList<HashMap<String, String>> tlist = aus.checkAuthorityTeam(mid, 3);
	         ArrayList<HashMap<String, String>> glist = new ArrayList<HashMap<String, String>>();
	         
	         for(HashMap<String, String> e : tlist) {
	            int gCode = Integer.parseInt(e.get("groupCode"));
	            
	            int count = 0;
	            for(HashMap<String, String> g : glist) {
	               if(Integer.parseInt(g.get("groupCode")) == gCode) {
	                  count++;
	               }
	            }
	            if(count == 0) {
	               glist.add(e);
	            }
	         }
	         model.addAttribute("glist", glist);
	         model.addAttribute("tlist", tlist);
	         
	         return "account/oneDayAttend";

	      } else {
	         return "common/noAuthority";
	      }
	}
	
	//일일 근태관리 검색
	@RequestMapping("selectDayAttendance.att")
	public ModelAndView selectDayAttendance(HttpServletRequest request, OneDaySearch ods, ModelAndView mv) {
		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		String searchDate = (request.getParameter("oneDay"));
		ods.setSearchDate(searchDate);
		ods.setTeamCode(teamCode);
		ArrayList<OneDaySearch> oList = as.selectOneDayWork(ods);
		
		mv.addObject("oList", oList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//기타 근무조회 뷰
	@RequestMapping("showAttendSearch.att")
	public String showAttendSearch(Model model, HttpServletRequest request) {
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		String authority = aus.checkAuthority(mid, 3);
	      if(authority.equals("Y")) {
	         
	         ArrayList<HashMap<String, String>> tlist = aus.checkAuthorityTeam(mid, 3);
	         ArrayList<HashMap<String, String>> glist = new ArrayList<HashMap<String, String>>();
	         
	         
	         for(HashMap<String, String> e : tlist) {
	            int gCode = Integer.parseInt(e.get("groupCode"));
	            
	            int count = 0;
	            for(HashMap<String, String> g : glist) {
	               if(Integer.parseInt(g.get("groupCode")) == gCode) {
	                  count++;
	               }
	            }
	            if(count == 0) {
	               glist.add(e);
	            }
	         }
	         model.addAttribute("glist", glist);
	         model.addAttribute("tlist", tlist);
	         
	         return "account/attendSearch";
	      } else {
	         return "common/noAuthority";
	      }
		
	}
	
	//기타 근무조회 select
	@RequestMapping("selectSpecialAttendance.att")
	public ModelAndView selectSpecialAttendance(HttpServletRequest request, SpecialAttendanceSearch sas, ModelAndView mv) {
		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		int groupSelect = Integer.parseInt(request.getParameter("groupSelect"));
		String monthSelect = (request.getParameter("monthSelect"));
		sas.setTeamCode(teamCode);
		sas.setDattDate(monthSelect);
		ArrayList<SpecialAttendanceSearch> saList = as.selectSpecialAttendanceSearch(sas);
		mv.addObject("saList", saList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//휴가신청 취소 전자결재
	@RequestMapping("vacationCancel.att")
	public String vacationCancel(Model model, Vacation v, ApprovalBasic ab, HttpServletRequest request) {
		
		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		v.setmId(mid);
				
		Approval app = new ApprovalLine().makingApprovalLine(11, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		//휴가신청 불러오기 & vacationCode 2로 변경
		v.setApprovalCode(app.getApprovalCode());
		as.vacationCancle(v, app);
		return "redirect:/showInfoVacation.att";
	}
	
	//사월 월근태 뷰
	@RequestMapping("showAttendanceManage.att")
	public String showAttendanceManage(Model model, HttpServletRequest request) {
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		String authority = aus.checkAuthority(mid, 3);
	      if(authority.equals("Y")) {
	         
	         ArrayList<HashMap<String, String>> tlist = aus.checkAuthorityTeam(mid, 3);
	         ArrayList<HashMap<String, String>> glist = new ArrayList<HashMap<String, String>>();
	         
	         
	         for(HashMap<String, String> e : tlist) {
	            int gCode = Integer.parseInt(e.get("groupCode"));
	            
	            int count = 0;
	            for(HashMap<String, String> g : glist) {
	               if(Integer.parseInt(g.get("groupCode")) == gCode) {
	                  count++;
	               }
	            }
	            if(count == 0) {
	               glist.add(e);
	            }
	         }
	         model.addAttribute("glist", glist);
	         model.addAttribute("tlist", tlist);
	         
	         return "account/attendanceManage";
	      } else {
	         return "common/noAuthority";
	      }
	}
	
	//월근태 사원 정보 검색 ajax
	@RequestMapping("selectGroupMember.att")
	public ModelAndView selectGroupMember(HttpServletRequest request, ModelAndView mv) {
		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		int groupSelect = Integer.parseInt(request.getParameter("groupSelect"));
		String monthSelect = (request.getParameter("monthSelect"));
		HashMap<String, Object> map = new HashMap<>();
		map.put("teamCode", teamCode);
		map.put("monthSelect", monthSelect);
		
		//현재달 & 검색 달 확인
		SimpleDateFormat monthF =  new SimpleDateFormat("YYYYMM");
		Calendar now = Calendar.getInstance();
		String month = monthF.format(now.getTime());
		
		int nowYear = Integer.parseInt(month.substring(0, 4));
		int nowMonth = Integer.parseInt(month.substring(4, 6));
		int searchYear = Integer.parseInt(monthSelect.substring(0, 4));
		int searchMonth = Integer.parseInt(monthSelect.substring(5, 7));
		int gap = (nowYear-searchYear)*12 +(nowMonth-searchMonth);
		String[] mArray = monthSelect.split("-");
		int selectMonth = Integer.parseInt(mArray[0]+mArray[1]);
 		map.put("selectMonth", selectMonth);
		
		int result = 0;
		ArrayList<SearchMember> smList =  null;
		ArrayList<SearchMember> mtCheck = null;
		if(gap <= 0) {
			result = 1;
			System.out.println("월 마감이 되지 않음");
		}else if(gap >= 2) {
			//월 근태 테이블 확인
			mtCheck = as.monthTableCheck(map);
			System.out.println("mtCheck : " + mtCheck);
			if(mtCheck.isEmpty()) {
				//정보 없음
				System.out.println("월근태 정보 없음");
				result = 2;
			}else {
				System.out.println("월근태 정보 있음");
				smList =  as.selectMonthMember(map);
			}
		}else if(gap == 1) {
			//전달 월근태 테이블 생성 확인
			mtCheck = as.monthTableCheck(map);
			System.out.println("mtChekc : " + mtCheck);
			if(mtCheck.isEmpty()) {
				//정보 없음 -> 월근태 테이블 생성
				smList =  as.selectMonthMember(map);
				System.out.println("월근태 생성 smList : " + smList);
				as.insertMonthTable(smList, selectMonth, monthSelect);
			}else {
				System.out.println("전달 월근태 정보 있음");
				smList =  as.selectMonthMember(map);
			}
		}
		
		//월 근태 사원정보 불러오기
		mv.addObject("result", result);
		mv.addObject("smList", smList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//사원 월근태 가져오기
	@RequestMapping("selectMemberMA.att")
	public ModelAndView selectMemberMA(HttpServletRequest request, ModelAndView mv) {
		String smonth = request.getParameter("month");
		int mid = Integer.parseInt(request.getParameter("mid"));
		String years = smonth.substring(0, 4);
		String months = smonth.substring(5, 7);
		int month = Integer.parseInt(years+months);
		MonthAttendance m = new MonthAttendance();
		m.setMattDate(month);
		m.setMid(mid);
		MonthAttendance ma = as.selectMemberMA(m);
		
		mv.addObject("ma", ma);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//월근태 수정
	@RequestMapping("modifyMonthAttendnace.att")
	public ModelAndView modifyMonthAttendnace(HttpServletRequest request, MonthAttendance ma, ModelAndView mv) {
		int result = as.updateMonthAttendance(ma);
		mv.addObject("result", result);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//월근태 수정 approval check
	@RequestMapping("maApprovalCheck.att")
	public ModelAndView maApprovalCheck(HttpServletRequest request, ModelAndView mv) {
		int month = Integer.parseInt(request.getParameter("mon"));
		int mid = Integer.parseInt(request.getParameter("mid"));
		HashMap<String, Object> hmap = new HashMap<>();
		hmap.put("month", month);
		hmap.put("mid", mid);
		int result = as.maApprovalCheck(hmap);
		mv.addObject("result", result);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//사원 월근태 현황 검색
	@RequestMapping("selectMemberMAstatus.att")
	public ModelAndView selectMemberMAstatus (HttpServletRequest request, ModelAndView mv) {
		String month = request.getParameter("maDate");
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		//월근태 현황(day)
		ArrayList<MonthStatus> msList = as.selectMemberMAstatus(month, mid);
		//월 근태 테이블
		MonthAttendance ma = as.selectMemberma(mid, month);
		//사원정보 가져오기
		MemberHrInfo mhi = as.selectMemberInfo(mid);
		mv.addObject("mhi", mhi);
		mv.addObject("msList", msList);
		mv.addObject("ma", ma);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//근태관리 - 월근태관리 페이지뷰
	@RequestMapping("showMonthAttend.att")
	public String showMonthAttend(Model model, HttpServletRequest request) {
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		String authority = aus.checkAuthority(mid, 3);
	      if(authority.equals("Y")) {
	         
	         ArrayList<HashMap<String, String>> tlist = aus.checkAuthorityTeam(mid, 3);
	         ArrayList<HashMap<String, String>> glist = new ArrayList<HashMap<String, String>>();
	         
	         
	         for(HashMap<String, String> e : tlist) {
	            int gCode = Integer.parseInt(e.get("groupCode"));
	            
	            int count = 0;
	            for(HashMap<String, String> g : glist) {
	               if(Integer.parseInt(g.get("groupCode")) == gCode) {
	                  count++;
	               }
	            }
	            if(count == 0) {
	               glist.add(e);
	            }
	         }
	         model.addAttribute("glist", glist);
	         model.addAttribute("tlist", tlist);
	         
	         return "account/monthAttend";
	      } else {
	         return "common/noAuthority";
	      }
	}
	
	//근태관리 - 월근태관리(부서별 월근태현황 불러오기 ajax)
	@RequestMapping("selectTeamMonthAttendance.att")
	public ModelAndView selectTeamMonthAttendance(HttpServletRequest request, ModelAndView mv) {
		String month = request.getParameter("monthSelect");
		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		HashMap<String, Object> hmap = new HashMap<>();
		
		hmap = as.selectTeamMonthAttendance(month, teamCode);
		mv.addObject("hmap", hmap);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//상단바 출퇴근 버튼
	@RequestMapping("commuteCheck.att")
	public ModelAndView commuteCheck(HttpServletRequest request, ModelAndView mv) {
		int mid = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();			
		}
		int result = 0; //출근 1/퇴근2
		
		//일근태 check
		DayAttendance da = as.dayAttendanceCheck(mid);
		if(da == null) {
			//일근태 insert : 출근
			as.insertDayAttendance(mid);
			result = 1;
		}else {
			//퇴근
			DayAttendance das = new DayAttendance();
			das.setmId(mid);
				//시간 check
				SimpleDateFormat format_hour = new SimpleDateFormat("HH");
				Calendar time = Calendar.getInstance();
				int nowHour = Integer.parseInt(format_hour.format(time.getTime()));
				if(nowHour >= 7) {
					//7시 이후
					as.updateDayAttendance(das);
					
				}else {
					//7시 이전
					as.updateBeforeAttendance(mid);
				}
			result = 2;
		}
		mv.addObject("result", result);
		mv.setViewName("jsonView");
		return mv;
	}
	
	//월근태마감 전자결재
	@RequestMapping("insertMonthAttendanceRequest.att")
	public ModelAndView insertMonthAttendanceRequest(HttpServletRequest request, ModelAndView mv) {
		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		
		
		String title = request.getParameter("title");
		String monthSelect = request.getParameter("month");
		String[] mArray = monthSelect.split("-");
		int month = Integer.parseInt(mArray[0]+mArray[1]);
		int teamCode = Integer.parseInt(request.getParameter("teamCode"));
		int firstApp = Integer.parseInt(request.getParameter("firstApp"));
		int secondApp = Integer.parseInt(request.getParameter("secondApp"));
		int thirdApp = Integer.parseInt(request.getParameter("thirdApp"));
				
		ApprovalBasic ab = new ApprovalBasic();
		ab.setFirstApproval(firstApp);
		ab.setSecondApproval(secondApp);
		ab.setThirdApproval(thirdApp);
		
		Approval app = new ApprovalLine().makingApprovalLine(5, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		//해당 부서 근태리스트
		ArrayList<MonthAttendance> maList = as.selectMAClose(month, teamCode);
		
		//월근태 테이블 approvalID update
		int result = as.updateMAapprovalId(maList, app);
		mv.addObject("result", result); 
		mv.setViewName("jsonView"); 
		return mv;
	}
	
}


































