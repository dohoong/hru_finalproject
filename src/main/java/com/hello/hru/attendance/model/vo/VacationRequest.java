package com.hello.hru.attendance.model.vo;

import java.sql.Date;

public class VacationRequest {
	private int vacationId;
	private int mId;
	private int category;
	private int vacationCode;
	private Date approvalDate;
	private Date vacationStart;
	private Date vacationEnd;
	private int vacationNum;
	private String approvalYN;
	private String reason;
	private int approvalCode;
	
	public VacationRequest() {}

	public VacationRequest(int vacationId, int mId, int category, int vacationCode, Date approvalDate,
			Date vacationStart, Date vacationEnd, int vacationNum, String approvalYN, String reason, int approvalCode) {
		super();
		this.vacationId = vacationId;
		this.mId = mId;
		this.category = category;
		this.vacationCode = vacationCode;
		this.approvalDate = approvalDate;
		this.vacationStart = vacationStart;
		this.vacationEnd = vacationEnd;
		this.vacationNum = vacationNum;
		this.approvalYN = approvalYN;
		this.reason = reason;
		this.approvalCode = approvalCode;
	}

	public int getVacationId() {
		return vacationId;
	}

	public void setVacationId(int vacationId) {
		this.vacationId = vacationId;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getVacationCode() {
		return vacationCode;
	}

	public void setVacationCode(int vacationCode) {
		this.vacationCode = vacationCode;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public Date getVacationStart() {
		return vacationStart;
	}

	public void setVacationStart(Date vacationStart) {
		this.vacationStart = vacationStart;
	}

	public Date getVacationEnd() {
		return vacationEnd;
	}

	public void setVacationEnd(Date vacationEnd) {
		this.vacationEnd = vacationEnd;
	}

	public int getVacationNum() {
		return vacationNum;
	}

	public void setVacationNum(int vacationNum) {
		this.vacationNum = vacationNum;
	}

	public String getApprovalYN() {
		return approvalYN;
	}

	public void setApprovalYN(String approvalYN) {
		this.approvalYN = approvalYN;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(int approvalCode) {
		this.approvalCode = approvalCode;
	}

	@Override
	public String toString() {
		return "VacationRequest [vacationId=" + vacationId + ", mId=" + mId + ", category=" + category
				+ ", vacationCode=" + vacationCode + ", approvalDate=" + approvalDate + ", vacationStart="
				+ vacationStart + ", vacationEnd=" + vacationEnd + ", vacationNum=" + vacationNum + ", approvalYN="
				+ approvalYN + ", reason=" + reason + ", approvalCode=" + approvalCode + "]";
	}

	
	
	
}
