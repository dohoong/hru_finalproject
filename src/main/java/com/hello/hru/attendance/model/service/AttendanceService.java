package com.hello.hru.attendance.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.attendance.model.vo.AnnualVacation;
import com.hello.hru.attendance.model.vo.Certification;
import com.hello.hru.attendance.model.vo.DayAttendance;
import com.hello.hru.attendance.model.vo.MemberAnnualVacation;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.MonthStatus;
import com.hello.hru.attendance.model.vo.OneDaySearch;
import com.hello.hru.attendance.model.vo.SpecialAttendance;
import com.hello.hru.attendance.model.vo.SpecialAttendanceSearch;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.attendance.model.vo.VacationBasic;
import com.hello.hru.attendance.model.vo.VacationRequest;
import com.hello.hru.humanResource.model.vo.GroupAndTeam;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberHrInfo;
import com.hello.hru.member.model.vo.SearchMember;

public interface AttendanceService {

	HashMap<String, Object> selectvMap();
	
	//VacationBasic INSERT
	int insertVacationBasic(VacationBasic vb);

	int insertAnnualVacation(AnnualVacation av);
	//휴가항목 UPDATE
	int updateVacationBasic(VacationBasic vb);
	//휴가항목 DELETE
	int deleteVacationBasic(VacationBasic vb);
	//연차 기준년수 check
	AnnualVacation checkAvStd(int annualStd);
	//연차부여일수 UPDATE
	int updateAnnualVacation(AnnualVacation aVacation);
	//연차부여일수 DELETE
	int deleteAnnualVacation(AnnualVacation av);

	//사원 근태관리 페이지 뷰
	HashMap<String, Object> selectDayAttendance(int mId);
	//일근태 DB확인
	DayAttendance dayAttendanceCheck(int mId);
	//일근태 insert
	int insertDayAttendance(int mId);
	//일근태 update - 7시 이후
	int updateDayAttendance(DayAttendance da);
	//일근태 7시 이전일때
	HashMap<String, Object> selectbeforeAttendance(int mId);
	//일근태 update - 7시 이전
	int updateBeforeAttendance(int mId);
	//휴가관리 페이지 뷰(연차 불러오기)
	MemberAnnualVacation selectMemberAnnualVacation(MemberAnnualVacation mv);
	//휴가신청 전자결재
	void insertVacationRequest(Vacation v, Approval app);
	//휴가신청 이력 list
	ArrayList<VacationRequest> selectVacationList(int mId);
	//휴가신청 불러오기 & vacationCode 2로 변경
	void vacationCancle(Vacation v, Approval app);
	//휴가신청 날짜 중복확인
	ArrayList<Vacation> selectDuplicateDate(Vacation v);
	//남은 연차 확인
	int checkRemainV(Vacation v);
	
	
	//야특근 전자결재
	int insertSpecialVacation(SpecialAttendance sa, Approval app);
	//제증명신청 전자결재
	void insertCertification(Certification cf, Approval app);
	//제증명 신청 이력
	ArrayList<Certification> selectCertification(int mId);

	//사업장&부서 불러오기
	GroupAndTeam selectGAT();
	//일일 근태관리 검색
	ArrayList<OneDaySearch> selectOneDayWork(OneDaySearch ods);
	//기타 근무조회 select
	ArrayList<SpecialAttendanceSearch> selectSpecialAttendanceSearch(SpecialAttendanceSearch sas);

	//월 근태 사원정보 불러오기
	ArrayList<SearchMember> selectMonthMember(HashMap<String, Object> map);
	//월 근태 테이블 확인
	ArrayList<SearchMember> monthTableCheck(HashMap<String, Object> map);
	//월근태 테이블 생성
	void insertMonthTable(ArrayList<SearchMember> smList, int selectMonth, String monthSelect);
	//사원 월근태 가져오기
	MonthAttendance selectMemberMA(MonthAttendance m);
	//월근태 수정
	int updateMonthAttendance(MonthAttendance ma);
	//월근태 수정 approval check
	int maApprovalCheck(HashMap<String, Object> hmap);
	//사원 월근태 현황 검색
	ArrayList<MonthStatus> selectMemberMAstatus(String month, int mid);
	//사원정보 가져오기
	MemberHrInfo selectMemberInfo(int mid);
	//월 근태 테이블
	MonthAttendance selectMemberma(int mid, String month);
	//부서별 월근태현황 불러오기
	HashMap<String, Object> selectTeamMonthAttendance(String month, int teamCode);
	
	//해당 부서 근태리스트
	ArrayList<MonthAttendance> selectMAClose(int month, int teamCode);
	//월근태 테이블 approvalID update
	int updateMAapprovalId(ArrayList<MonthAttendance> maList, Approval app);
	//ip일치 확인
	int ipCheck(long ipAddressLoing);


	
	
	
	
	



}
