package com.hello.hru.attendance.model.vo;

public class SpecialAttendanceSearch {
	private int mId;
	private String mName;
	private String position;
	private String team;
	private int category;
	private String dattDate;
	private String startTime;
	private String endTime;
	private int teamCode;
	
	public SpecialAttendanceSearch() {}

	public SpecialAttendanceSearch(int mId, String mName, String position, String team, int category, String dattDate,
			String startTime, String endTime, int teamCode) {
		super();
		this.mId = mId;
		this.mName = mName;
		this.position = position;
		this.team = team;
		this.category = category;
		this.dattDate = dattDate;
		this.startTime = startTime;
		this.endTime = endTime;
		this.teamCode = teamCode;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getDattDate() {
		return dattDate;
	}

	public void setDattDate(String dattDate) {
		this.dattDate = dattDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	@Override
	public String toString() {
		return "SpecialAttendanceSearch [mId=" + mId + ", mName=" + mName + ", position=" + position + ", team=" + team
				+ ", category=" + category + ", dattDate=" + dattDate + ", startTime=" + startTime + ", endTime="
				+ endTime + ", teamCode=" + teamCode + "]";
	}

	
	
	
}
