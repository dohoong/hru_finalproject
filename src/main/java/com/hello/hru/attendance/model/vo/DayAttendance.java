package com.hello.hru.attendance.model.vo;

import java.sql.Date;

public class DayAttendance {
	private int dattId;
	private Date dattDate;
	private String startDate;
	private String endDate;
	private int mId;
	private int sattId;
	
	public DayAttendance() {}

	public DayAttendance(int dattId, Date dattDate, String startDate, String endDate, int mId, int sattId) {
		super();
		this.dattId = dattId;
		this.dattDate = dattDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.mId = mId;
		this.sattId = sattId;
	}

	public int getDattId() {
		return dattId;
	}

	public void setDattId(int dattId) {
		this.dattId = dattId;
	}

	public Date getDattDate() {
		return dattDate;
	}

	public void setDattDate(Date dattDate) {
		this.dattDate = dattDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public int getSattId() {
		return sattId;
	}

	public void setSattId(int satttId) {
		this.sattId = satttId;
	}

	@Override
	public String toString() {
		return "DayAttendance [dattId=" + dattId + ", dattDate=" + dattDate + ", startDate=" + startDate + ", endDate="
				+ endDate + ", mId=" + mId + ", sattId=" + sattId + "]";
	}
	
	
}
