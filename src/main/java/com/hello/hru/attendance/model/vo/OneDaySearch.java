package com.hello.hru.attendance.model.vo;

import java.sql.Date;

public class OneDaySearch {
	private int teamCode;
	private String searchDate;
	private int mId;
	private String mName;
	private String position;
	private String team;
	private String workDate;
	private String startTime;
	private String endTime;
	private int extendTime;
	private int nightTime;
	private int weekendTime;
	private int totalTime;
	
	public OneDaySearch() {}

	public OneDaySearch(int teamCode, String searchDate, int mId, String mName, String position, String team,
			String workDate, String startTime, String endTime, int extendTime, int nightTime, int weekendTime,
			int totalTime) {
		super();
		this.teamCode = teamCode;
		this.searchDate = searchDate;
		this.mId = mId;
		this.mName = mName;
		this.position = position;
		this.team = team;
		this.workDate = workDate;
		this.startTime = startTime;
		this.endTime = endTime;
		this.extendTime = extendTime;
		this.nightTime = nightTime;
		this.weekendTime = weekendTime;
		this.totalTime = totalTime;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	public String getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(String searchDate) {
		this.searchDate = searchDate;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getWorkDate() {
		return workDate;
	}

	public void setWorkDate(String workDate) {
		this.workDate = workDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getExtendTime() {
		return extendTime;
	}

	public void setExtendTime(int extendTime) {
		this.extendTime = extendTime;
	}

	public int getNightTime() {
		return nightTime;
	}

	public void setNightTime(int nightTime) {
		this.nightTime = nightTime;
	}

	public int getWeekendTime() {
		return weekendTime;
	}

	public void setWeekendTime(int weekendTime) {
		this.weekendTime = weekendTime;
	}

	public int getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(int totalTime) {
		this.totalTime = totalTime;
	}

	@Override
	public String toString() {
		return "OneDaySearch [teamCode=" + teamCode + ", searchDate=" + searchDate + ", mId=" + mId + ", mName=" + mName
				+ ", position=" + position + ", team=" + team + ", workDate=" + workDate + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", extendTime=" + extendTime + ", nightTime=" + nightTime + ", weekendTime="
				+ weekendTime + ", totalTime=" + totalTime + "]";
	}

	
}
