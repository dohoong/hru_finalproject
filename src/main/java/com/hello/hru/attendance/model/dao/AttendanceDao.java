package com.hello.hru.attendance.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.attendance.model.vo.AnnualVacation;
import com.hello.hru.attendance.model.vo.Certification;
import com.hello.hru.attendance.model.vo.DayAttendance;
import com.hello.hru.attendance.model.vo.MemberAnnualVacation;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.MonthStatus;
import com.hello.hru.attendance.model.vo.OneDaySearch;
import com.hello.hru.attendance.model.vo.SpecialAttendance;
import com.hello.hru.attendance.model.vo.SpecialAttendanceSearch;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.attendance.model.vo.VacationBasic;
import com.hello.hru.attendance.model.vo.VacationRequest;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberHrInfo;
import com.hello.hru.member.model.vo.SearchMember;
import com.hello.hru.system.model.vo.IP;

public interface AttendanceDao {

	ArrayList<VacationBasic> selectvList(SqlSessionTemplate sqlSession);

	ArrayList<AnnualVacation> selectaList(SqlSessionTemplate sqlSession);
	//휴가항목 INSERT
	int insertVacationBasic(VacationBasic vb, SqlSessionTemplate sqlSession);
	//휴가항목 UPDATE
	int updateVacationBasic(VacationBasic vb, SqlSessionTemplate sqlSession);
	//휴가항목 DELETE
	int deleteVacationBasic(VacationBasic vb, SqlSessionTemplate sqlSession);
	//연차부여일수 INSERT
	int insertAnnualVacation(AnnualVacation av, SqlSessionTemplate sqlSession);
	//연차 기준년수 check
	AnnualVacation checkAvStd(int annualStd, SqlSessionTemplate sqlSession);
	//연차부여일수 UPDATE
	int updateAnnualVacation(AnnualVacation aVacation, SqlSessionTemplate sqlSession);
	//연차부여일수 DELETE
	int deleteAnnualVacation(AnnualVacation av, SqlSessionTemplate sqlSession);

	//로그인유저 정보 불러오기
	Member selectMember(Member m, SqlSessionTemplate sqlSession);
	//일근태 불러오기 - 7시 이후
	DayAttendance selectDayAttendance(int mId, SqlSessionTemplate sqlSession);
	//일근태 7시 이전일때
	DayAttendance selectbeforeAttendance(int mId, SqlSessionTemplate sqlSession);
	//일근태 DB확인
	DayAttendance dayAttendanceCheck(int mId, SqlSessionTemplate sqlSession);
	
	//일근태 insert
	int insertDayAttendance(int mId, SqlSessionTemplate sqlSession);
	//일근태 update - 7시 이후
	int updateDayAttendance(DayAttendance da, SqlSessionTemplate sqlSession);
	//일근태 update - 7시 이전
	int updateBeforeAttendance(int mId, SqlSessionTemplate sqlSession);
	
	//연차 발생일수 set
	int selectMemberAnnualNum(MemberAnnualVacation mv, SqlSessionTemplate sqlSession);
	//남은 연차일수 set
	int selectRemainVacation(MemberAnnualVacation mv, SqlSessionTemplate sqlSession);
	//조정연차 일수
	int selectAddVacation(MemberAnnualVacation mv, SqlSessionTemplate sqlSession);
	
	//휴가테이블 INSERT
	int insertVacationRequest(Vacation v, SqlSessionTemplate sqlSession);
	//휴가신청 이력 list
	ArrayList<VacationRequest> selectVacationList(int mId, SqlSessionTemplate sqlSession);
	//휴가신청 불러오기 & vacationCode 2로 변경
	Vacation selectVacation(Vacation v, SqlSessionTemplate sqlSession);
	//휴가신청 날짜 중복확인
	ArrayList<Vacation> selectDuplicateDate(Vacation v, SqlSessionTemplate sqlSession);
	//남은 연차일수 불러오기
	int selectRemainV(Vacation v, SqlSessionTemplate sqlSession);
		
	//야특근 테이블 INSERT
	int insertSpecialAttendance(SpecialAttendance sa, SqlSessionTemplate sqlSession);
	//일근태 sattid UPDATE
	int updateSattId(SpecialAttendance sa, SqlSessionTemplate sqlSession);
	//야특근 테이블 INSERT
	int insertCertification(Certification cf, SqlSessionTemplate sqlSession);
	//야특근 중복 확인
	ArrayList<SpecialAttendance> selectSAcheck(SpecialAttendance sa, SqlSessionTemplate sqlSession);
	//주 야특근 시간 가져오기
	int selectSpecialHour(HashMap<String, Object> hMap, SqlSessionTemplate sqlSession);
	
	//제증명 신청 이력
	ArrayList<Certification> selectCertification(int mId, SqlSessionTemplate sqlSession);

	//일일 근태관리 검색
	ArrayList<OneDaySearch> selectOneDayWork(OneDaySearch ods, SqlSessionTemplate sqlSession);
	//기타 근무조회 select
	ArrayList<SpecialAttendanceSearch> selectSpecialAttendanceSearch(SpecialAttendanceSearch sas, SqlSessionTemplate sqlSession);

	//월 근태 사원정보 불러오기
	ArrayList<SearchMember> selectMonthMember(HashMap<String, Object> map, SqlSessionTemplate sqlSession);
	//월 근태 테이블 확인
	ArrayList<SearchMember> monthTableCheck(HashMap<String, Object> map, SqlSessionTemplate sqlSession);
	//평일 근무일수 가져오기
	int selectWeekendNum(HashMap<String, Object> mList, SqlSessionTemplate sqlSession);
	//휴일 근무일수 가져오기
	int selecHolidayNum(HashMap<String, Object> mList, SqlSessionTemplate sqlSession);
	//연차 사용일수
	ArrayList<Vacation> selectVacationNum(HashMap<String, Object> mList, SqlSessionTemplate sqlSession);
	//연장 근로시간 가져오기
	int selectWorkMore(HashMap<String, Object> mList, SqlSessionTemplate sqlSession);
	//야간 근로시간 가져오기
	int selectWorkNight(HashMap<String, Object> mList, SqlSessionTemplate sqlSession);
	//휴일근로 전체시간 가져오기
	ArrayList<Integer> selectHoliTotal(HashMap<String, Object> mList, SqlSessionTemplate sqlSession);
	//월 근태 insert
	int insertMonthAttendance(MonthAttendance ma, SqlSessionTemplate sqlSession);
	//사원 월근태 가져오기
	MonthAttendance selectMemberMA(MonthAttendance m, SqlSessionTemplate sqlSession);
	//월근태 수정
	int updateMonthAttendance(MonthAttendance ma, SqlSessionTemplate sqlSession);
	//월근태 수정 approval check//월근태 수정 approval check
	String maApprovalCheck(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession);
	//월근태 status
	ArrayList<MonthStatus> selectMemberMAstatus(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession);
	//휴가 리스트 불러오기
	ArrayList<MonthStatus> selectMonthVacationNum(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession);
	//사원정보 가져오기
	MemberHrInfo selectMemberInfo(int mid, SqlSessionTemplate sqlSession);
	//월 근태 테이블
	MonthAttendance selectMemberma(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession);
	//부서별 월근태현황 불러오기
	ArrayList<MonthAttendance> selectTeamMonthAttendance(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession);
	//부서 사원정보 LIST불러오기
	ArrayList<SearchMember> selectTeamHfInfo(HashMap<String, Object> tMap, SqlSessionTemplate sqlSession);

	//해당 부서 근태리스트
	ArrayList<MonthAttendance> selectMAClose(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession);
	//월근태 전자결재 id update
	int updateMAapprovalId(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession);
	//db에서 ip리스트 불러오기
	ArrayList<IP> selectIPlIST(SqlSessionTemplate sqlSession);
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	

}
