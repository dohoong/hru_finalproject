package com.hello.hru.attendance.model.service;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.approval.model.dao.ApprovalDao;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.attendance.model.dao.AttendanceDao;
import com.hello.hru.attendance.model.vo.AnnualVacation;
import com.hello.hru.attendance.model.vo.Certification;
import com.hello.hru.attendance.model.vo.DayAttendance;
import com.hello.hru.attendance.model.vo.MemberAnnualVacation;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.MonthStatus;
import com.hello.hru.attendance.model.vo.OneDaySearch;
import com.hello.hru.attendance.model.vo.SpecialAttendance;
import com.hello.hru.attendance.model.vo.SpecialAttendanceSearch;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.attendance.model.vo.VacationBasic;
import com.hello.hru.attendance.model.vo.VacationRequest;
import com.hello.hru.humanResource.model.dao.HumanResourceDao;
import com.hello.hru.humanResource.model.vo.GroupAndTeam;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberHrInfo;
import com.hello.hru.member.model.vo.SearchMember;
import com.hello.hru.system.model.vo.IP;

@Service
public class AttendanceServiceImpl implements AttendanceService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private AttendanceDao ad;
	@Autowired
	private ApprovalDao atd;
	@Autowired
	private HumanResourceDao hrd;
	
	//기준정보 휴가관리 select
	@Override
	public HashMap<String, Object> selectvMap() {
		ArrayList<VacationBasic> vList = ad.selectvList(sqlSession);
		ArrayList<AnnualVacation> aList = ad.selectaList(sqlSession);
		HashMap<String, Object> vMap = new HashMap<String, Object>();
		vMap.put("vList", vList);
		vMap.put("aList", aList);
		
		return vMap;
	}
	
	//기준정보 휴가관리 INSERT
	@Override
	public int insertVacationBasic(VacationBasic vb) {
		return ad.insertVacationBasic(vb, sqlSession);
	}
	
	//휴가항목 UPDATE
	@Override
	public int updateVacationBasic(VacationBasic vb) {
		return ad.updateVacationBasic(vb, sqlSession);
	}
	
	//휴가항목 DELETE
	@Override
	public int deleteVacationBasic(VacationBasic vb) {
		return ad.deleteVacationBasic(vb, sqlSession);
	}
	
	//기준정보 연차부여일수 INSERT
	@Override
	public int insertAnnualVacation(AnnualVacation av) {
		return ad.insertAnnualVacation(av, sqlSession);
	}

	//연차 기준년수 check
	@Override
	public AnnualVacation checkAvStd(int annualStd) {
		return ad.checkAvStd(annualStd, sqlSession);
	}
	//연차부여일수 UPDATE
	@Override
	public int updateAnnualVacation(AnnualVacation aVacation) {
		return ad.updateAnnualVacation(aVacation, sqlSession);
	}
	//연차부여일수 DELETE
	@Override
	public int deleteAnnualVacation(AnnualVacation av) {
		return ad.deleteAnnualVacation(av, sqlSession);
	}
	//사원 근태관리 페이지 뷰 7시 이후
	@Override
	public HashMap<String, Object> selectDayAttendance(int mId) {
		Member m = new Member();
		m.setMid(mId);
		m = ad.selectMember(m, sqlSession);
		DayAttendance da = ad.selectDayAttendance(mId, sqlSession);
		HashMap<String, Object> daMap = new HashMap<String, Object>();
		daMap.put("m", m);
		daMap.put("da", da);
		return daMap;
	}
	//일근태 7시 이전일때
	@Override
	public HashMap<String, Object> selectbeforeAttendance(int mId) {
		Member m = new Member();
		m.setMid(mId);
		m = ad.selectMember(m, sqlSession);
		DayAttendance da = ad.selectbeforeAttendance(mId, sqlSession);
		HashMap<String, Object> daMap = new HashMap<String, Object>();
		daMap.put("m", m);
		daMap.put("da", da);
		return daMap;
	}

	//일근태 DB확인
	@Override
	public DayAttendance dayAttendanceCheck(int mId) {
		return ad.dayAttendanceCheck(mId, sqlSession);
	}
	//일근태 insert
	@Override
	public int insertDayAttendance(int mId) {
		return ad.insertDayAttendance(mId, sqlSession);
	}
	//일근태 update - 7시 이후
	@Override
	public int updateDayAttendance(DayAttendance da) {
		return ad.updateDayAttendance(da, sqlSession);
	}
	//일근태 update - 7시 이전
	@Override
	public int updateBeforeAttendance(int mId) {
		return ad.updateBeforeAttendance(mId, sqlSession);
	}
	//휴가관리 페이지 뷰(연차 불러오기)
	@Override
	public MemberAnnualVacation selectMemberAnnualVacation(MemberAnnualVacation mv) {
		//연차 기준 시작일, 마지막일 set
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
		Calendar cal = Calendar.getInstance();
		int year = Integer.parseInt(yearFormat.format(cal.getTime()));
		int enterYear = Integer.parseInt(yearFormat.format(mv.getEnterDate()));
		int month = Integer.parseInt(monthFormat.format(cal.getTime()));
		String yearStart = year + "-01-01";
		String yearEnd = year + "-12-31";
		mv.setYearStart(yearStart);
		mv.setYearEnd(yearEnd);
		int enterMonth = Integer.parseInt(monthFormat.format(mv.getEnterDate()));
		//연차 발생일수 set
		
		int avNum = 0;
		if(year == enterYear) {
			//1년 미만
			//입사 달 
			avNum = 12 - enterMonth;
			mv.setAvNum(avNum);
		}else {
			//1년 이상
			avNum = ad.selectMemberAnnualNum(mv, sqlSession);
			mv.setAvNum(avNum);
		}
		//남은 연차일수 set
		int restNum = ad.selectRemainVacation(mv, sqlSession);
		mv.setRestNum(restNum);
		int useNum = avNum - restNum;
		mv.setUseNum(useNum);
		//조정연차 일수
		int addNum = ad.selectAddVacation(mv, sqlSession);
		mv.setAddNum(addNum);
		return mv;
	}
	//휴가신청 전자전재
	@Override
	public void insertVacationRequest(Vacation v, Approval app) {
		int result1 = atd.insertApproval(sqlSession, app);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					System.out.println("Service Apphis : " +apphis);
					result12 = atd.insertApprovalHistory(sqlSession, apphis);
				}
				v.setApprovalYN("N");
			} else {
				result12 = 1;
				v.setApprovalYN("Y"); //결재가 필요없을 경우라면, 카테고리를 3:승인으로 바꿈!
			}
		}
		v.setApprovalId(result1);
		//휴가 구분 set
		v.setCategory(1);
		//휴가테이블 INSERT
		int result2 = ad.insertVacationRequest(v, sqlSession);
	}
	
	//휴가신청 이력 list
	@Override
	public ArrayList<VacationRequest> selectVacationList(int mId) {
		return ad.selectVacationList(mId, sqlSession);
	}
	
	//야특근 전자결재
	@Override
	public int insertSpecialVacation(SpecialAttendance sa, Approval app) {
		int result = 0;
		int result1 = atd.insertApproval(sqlSession, app);
		int result12 = 0;
		
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					System.out.println("Service Apphis : " +apphis);
					result12 = atd.insertApprovalHistory(sqlSession, apphis);
				}
				sa.setApprovalYN("N");
			} else {
				result12 = 1;
				sa.setApprovalYN("Y"); //결재가 필요없을 경우라면, 카테고리를 3:승인으로 바꿈!
			}
			
		}
		sa.setApprovalId(result1);
		
		//시작날짜 set
		String startDate = sa.getSattDate() + " " + sa.getStartTime();
		sa.setStartDate(startDate);
		System.out.println("endDate : " + sa.getEndDate());
		String endDate = sa.getEndDate() + " " + sa.getEndTime();
		sa.setEndDate(endDate);
		System.out.println("야특근 신청 service: " + sa);
		//야특근 중복 확인
		ArrayList<SpecialAttendance> sList = ad.selectSAcheck(sa, sqlSession);
		if(sList.size() > 0) {
			System.out.println("야특근 중복됨!");
			result = 1;
			return result;
		}else {
			System.err.println("야특근 중복 아님!");
			//주 12시간 확인
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				java.util.Date nDate = formatter.parse(sa.getSattDate());
				Calendar c = Calendar.getInstance();
				c.setTime(nDate);
				c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
				formatter.format(c.getTime());
				System.out.println("service 월요일 formatter : " +  formatter.format(c.getTime()));
				Calendar s = Calendar.getInstance();
				s.setTime(nDate);
				s.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
				s.add(s.DATE, 7);
				formatter.format(s.getTime());
				System.out.println("service 일요일 formatter : " +  formatter.format(s.getTime()));
				//주 야특근 시간 가져오기
				HashMap<String, Object> hMap = new HashMap<>();
				hMap.put("monday", formatter.format(c.getTime()));
				hMap.put("sunday", formatter.format(s.getTime()));
				hMap.put("sa", sa);
				int workHour = ad.selectSpecialHour(hMap, sqlSession);
				SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				java.util.Date start = timeFormat.parse(sa.getStartDate());
				java.util.Date end = timeFormat.parse(sa.getEndDate());
				long diff = end.getTime() - start.getTime();
				int gapHour = (int)diff/3600000;
				int totalHour = workHour + gapHour;
				if(totalHour > 12) {
					//야특근 시간 합 12시간 초과
					result = 2;
					return result;
				} else {
					//야특근 신청 가능
					//insert
					if(sa.getCategory()==1) { 
						//야특근 테이블 INSERT -> 일근태 테이블 UPDATE 
						int sattId = ad.insertSpecialAttendance(sa, sqlSession); 
						sa.setSattId(sattId);
						//일근태 sattid UPDATE
						ad.updateSattId(sa, sqlSession); 
						result = 3; 
					}else { 
						//야특근 테이블 INSERT 
						ad.insertSpecialAttendance(sa, sqlSession); 
						result = 3;
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
		}
		
		return result;
	}
	
	//제증명신청 전자결재
	@Override
	public void insertCertification(Certification cf, Approval app) {
		int result1 = atd.insertApproval(sqlSession, app);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					System.out.println("Service Apphis : " +apphis);
					result12 = atd.insertApprovalHistory(sqlSession, apphis);
				}
				cf.setApprovalYN("N");
			} else {
				result12 = 1;
				cf.setApprovalYN("Y"); //결재가 필요없을 경우라면, 카테고리를 3:승인으로 바꿈!
			}
			
		}
		cf.setApprovalId(result1);
		
		//야특근 테이블 INSERT
		ad.insertCertification(cf, sqlSession);
	}
	//제증명 신청 이력
	@Override
	public ArrayList<Certification> selectCertification(int mId) {
		return ad.selectCertification(mId, sqlSession);
	}

	//사업장&부서 불러오기
	@Override
	public GroupAndTeam selectGAT() {
		GroupAndTeam gt = new GroupAndTeam();
		
		ArrayList<HashMap<String, String>> tlist = hrd.selectListTeam(sqlSession);
		gt.setTlist(tlist);
		
		ArrayList<HashMap<String, String>> glist = hrd.selectListGroup(sqlSession);
		gt.setGlist(glist);
		
		return gt;
	}
	//일일 근태관리 검색
	@Override
	public ArrayList<OneDaySearch> selectOneDayWork(OneDaySearch ods) {
		return ad.selectOneDayWork(ods, sqlSession);
	}
	//기타 근무조회 select
	@Override
	public ArrayList<SpecialAttendanceSearch> selectSpecialAttendanceSearch(SpecialAttendanceSearch sas) {
		return ad.selectSpecialAttendanceSearch(sas, sqlSession);
	}
	
	//휴가신청 불러오기 & vacationCode 2로 변경
	@Override
	public void vacationCancle(Vacation v, Approval app) {
		
		Vacation vacation = ad.selectVacation(v, sqlSession);
		System.out.println(vacation);
		
		vacation.setCategory(2);
		
		int result1 = atd.insertApproval(sqlSession, app);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					System.out.println("Service Apphis : " +apphis);
					result12 = atd.insertApprovalHistory(sqlSession, apphis);
				}
				vacation.setApprovalYN("N");
			} else {
				result12 = 1;
				vacation.setApprovalYN("Y"); //결재가 필요없을 경우라면, 카테고리를 3:승인으로 바꿈!
			}
			
		}
		vacation.setApprovalId(result1);
		
		String[] vStart = vacation.getvStart().split(" ");
		vacation.setvStart(vStart[0]);
		//휴가 취소 insert
		ad.insertVacationRequest(vacation, sqlSession);
		
		
	}
	//휴가신청 날짜 중복확인
	@Override
	public ArrayList<Vacation> selectDuplicateDate(Vacation v) {
		return ad.selectDuplicateDate(v, sqlSession);
	}

	//연차 확인
	@Override
	public int checkRemainV(Vacation v) {
		//남은 연차 일수 불러오기
		int remainV = ad.selectRemainV(v, sqlSession);
		int usable =  remainV - v.getvNum();
		return usable;
	}
	
	//월 근태 사원정보 불러오기
	@Override
	public ArrayList<SearchMember> selectMonthMember(HashMap<String, Object> map) {
		return ad.selectMonthMember(map, sqlSession);
	}
	//월 근태 테이블 확인
	@Override
	public ArrayList<SearchMember> monthTableCheck(HashMap<String, Object> map) {
		return ad.monthTableCheck(map, sqlSession);
	}
	//월근태 테이블 생성
	@Override
	public void insertMonthTable(ArrayList<SearchMember> smList, int selectMonth, String monthSelect) {
		for(int i = 0; i < smList.size(); i ++) {
			HashMap<String, Object> mList = new HashMap<>();
			mList.put("mId", smList.get(i).getMid());
			mList.put("monthSelect", monthSelect);
			//평일 근무일수 가져오기
			int weekendNum = ad.selectWeekendNum(mList, sqlSession);
			//휴일 근무일수 가져오기
			int holidayNum = ad.selecHolidayNum(mList, sqlSession);
			//연차 사용일수
			ArrayList<Vacation> vList = ad.selectVacationNum(mList, sqlSession);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat monthFormat = new SimpleDateFormat("yyy-mm");
			int vacationNum = 0;
			try {
				vacationNum = 0;
				for(int c = 0; c < vList.size(); c++){
					int vNum = vList.get(c).getvNum();
					for(int v = 0; v < vNum; v++) {
						java.util.Date nDate =  dateFormat.parse(vList.get(c).getvStart());
						java.util.Date mDate = monthFormat.parse(vList.get(c).getvStart());
						Calendar cal = Calendar.getInstance();
						Calendar mon = Calendar.getInstance();
						cal.setTime(nDate);
						String vMonth = monthFormat.format(mDate);
						cal.add(Calendar.DATE, v);
						mon.setTime(mDate);
						int dayNum = cal.get(Calendar.DAY_OF_WEEK);
						if((dayNum != 6 || dayNum != 7) & (vMonth.equals(monthSelect))) {
							vacationNum ++;
						}
					}
				}
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			//연장 근로시간 가져오기
			int workMore = ad.selectWorkMore(mList, sqlSession);
			//야간 근로시간 가져오기
			int workNight = ad.selectWorkNight(mList, sqlSession);
			//휴일근로 전체시간 가져오기
			ArrayList<Integer> holiTotal = ad.selectHoliTotal(mList, sqlSession);
			int holi1 = 0;
			int holi2 = 0;
			int ctn = 0;
			if(holiTotal.isEmpty()) {
				holi1 = 0;
				holi2 = 0;
			}else {
				for(int t = 0; t < holiTotal.size(); t++) {
					if(holiTotal.get(t) <= 8) {
						//holi1
						holi1 += holiTotal.get(t);
					}else if(holiTotal.get(t) > 8) {
						//holi2
						holi2 += (holiTotal.get(t) - 8);
						ctn ++;
					}
				}
				
			}
			int workHolidayOne = (ctn*8) + holi1;
			int workHolidayTwo = holi2;
			MonthAttendance ma = new MonthAttendance();
			ma.setMid(smList.get(i).getMid());
			ma.setMattDate(selectMonth);
			ma.setWeekdayNum(weekendNum);
			ma.setHolidayNum(holidayNum);
			ma.setVacationNum(vacationNum);
			ma.setWorkMore(workMore);
			ma.setWorkNight(workNight);
			ma.setWorkHolidayOne(workHolidayOne);
			ma.setWorkHolidayTwo(workHolidayTwo);
			
			//월 근태 insert
			int insertN = ad.insertMonthAttendance(ma, sqlSession);
			
		}
	}
	//사원 월근태 가져오기
	@Override
	public MonthAttendance selectMemberMA(MonthAttendance m) {
		return ad.selectMemberMA(m, sqlSession);
	}
	//월근태 수정
	@Override
	public int updateMonthAttendance(MonthAttendance ma) {
		return ad.updateMonthAttendance(ma, sqlSession);
	}
	//월근태 수정 approval check
	@Override
	public int maApprovalCheck(HashMap<String, Object> hmap) {
		String approvalYN = ad.maApprovalCheck(hmap, sqlSession);
		int result = 0;
		if(approvalYN.equals("Y")) {
			result = 1;
		}else {
			result = 2;
		}
		return result;
	}
	
	//사원 월근태 현황 검색
	@Override
	public ArrayList<MonthStatus> selectMemberMAstatus(String month, int mid) {
		//해당 달 마지막날 구하기
		String years = month.substring(0, 4);
		String months = month.substring(5, 7);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		int endDay = 0;
		ArrayList<MonthStatus> msList = new ArrayList<>();
		ArrayList<MonthStatus> dbList = new ArrayList<>();
		try {
			java.util.Date searchM = format.parse(years+months+"01");
			Calendar cal = Calendar.getInstance();
			cal.setTime(searchM);
			
			endDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		for(int i = 1; i <= endDay; i++) {
			MonthStatus ms = new MonthStatus();
			ms.setDay(i);
			msList.add(ms);
		}
		
		//주말 체크
		String holyCheck = years + months;
		for(int i = 0; i < msList.size(); i++) {
			String weekendCheck = holyCheck + msList.get(i).getDay();
			java.util.Date holiCheck = format.parse(weekendCheck);
			cal.setTime(holiCheck);
			int dayNum = cal.get(Calendar.DAY_OF_WEEK);
			if(dayNum == 1 || dayNum == 7) {
				msList.get(i).setWeekend(1);
			}
		}
		
		//츌근 상태 변경 해주기
		HashMap<String, Object> mMap = new HashMap<>();
		mMap.put("mid", mid);
		mMap.put("month", month);
		dbList = ad.selectMemberMAstatus(mMap, sqlSession);
		
		 for(int i = 0; i < msList.size(); i++) { 
			 	for(int d = 0; d<dbList.size(); d++) { 
			 		if(msList.get(i).getDay()==dbList.get(d).getDay()) {
			 			msList.get(i).setStatus(dbList.get(d).getStatus()); 
			 		} 
			 	}
		 }
		 
		//휴가 리스트 불러오기
		ArrayList<MonthStatus> vList = ad.selectMonthVacationNum(mMap, sqlSession);
		int vNum = 0;
		for(int i = 0; i < msList.size(); i++) {
			for(int v = 0; v < vList.size(); v++) {
				if(msList.get(i).getDay() == vList.get(v).getDay()) {
					vNum = vList.get(v).getWeekend();
				}
			}
			int dayNum = cal.get(Calendar.DAY_OF_WEEK);
			if((dayNum != 1 || dayNum != 7) && (vNum > 0)) {
				msList.get(i).setStatus(4);
				vNum--;
			}
		}
		
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return msList;
	}

	//사원정보 가져오기
	@Override
	public MemberHrInfo selectMemberInfo(int mid) {
		return ad.selectMemberInfo(mid, sqlSession);
	}
	//월 근태 테이블
	@Override
	public MonthAttendance selectMemberma(int mid, String month) {
		String smonth = month.substring(0, 4)+month.substring(5, 7);
		int imonth = Integer.parseInt(smonth);
		HashMap<String, Object> hmap = new HashMap<>();
		hmap.put("mid", mid);
		hmap.put("month", imonth);
		MonthAttendance ma = ad.selectMemberma(hmap, sqlSession);
		return ma;
	}
	//부서별 월근태현황 불러오기
	@Override
	public HashMap<String, Object> selectTeamMonthAttendance(String month, int teamCode) {
		//부서 사원정보 LIST불러오기
		HashMap<String, Object> tMap = new HashMap<>();
		tMap.put("month", month);
		tMap.put("teamCode", teamCode);
		ArrayList<SearchMember> mhList = ad.selectTeamHfInfo(tMap, sqlSession);
		String smonth = month.substring(0, 4)+month.substring(5, 7);
		int imonth = Integer.parseInt(smonth);
		HashMap<String, Object> hmap = new HashMap<>();
		//사원별 월 근태현황 
		//해당 달 마지막날 구하기
		String years = month.substring(0, 4);
		String months = month.substring(5, 7);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		int endDay = 0;
		
		ArrayList<ArrayList<MonthStatus>> tList = new ArrayList<>();
		
		for(int i = 0; i < mhList.size(); i++) {
			ArrayList<MonthStatus> msList = new ArrayList<>();
			ArrayList<MonthStatus> dbList = new ArrayList<>();
			try {
				java.util.Date searchM = format.parse(years+months+"01");
				Calendar cal = Calendar.getInstance();
				cal.setTime(searchM);
				endDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
				System.out.println("endDay" + endDay);
			
			for(int m = 1; m <= endDay; m++) {
				MonthStatus ms = new MonthStatus();
				ms.setDay(m);
				msList.add(ms);
			}
			
			//주말 체크
			String holyCheck = years + months;
			for(int m = 0; m < msList.size(); m++) {
				String weekendCheck = holyCheck + msList.get(m).getDay();
				java.util.Date holiCheck = format.parse(weekendCheck);
				cal.setTime(holiCheck);
				int dayNum = cal.get(Calendar.DAY_OF_WEEK);
				if(dayNum == 1 || dayNum == 7) {
					msList.get(m).setWeekend(1);
				}
			}

			HashMap<String, Object> mMap = new HashMap<>();
			mMap.put("mid", mhList.get(i).getMid());
			mMap.put("month", month);
			dbList = ad.selectMemberMAstatus(mMap, sqlSession);
			
			 for(int j = 0; j < msList.size(); j++) { 
				 	for(int d = 0; d<dbList.size(); d++) { 
				 		if(msList.get(j).getDay()==dbList.get(d).getDay()) {
				 			msList.get(j).setStatus(dbList.get(d).getStatus()); 
				 		} 
				 	}
			 }
			 
			//휴가 리스트 불러오기
			ArrayList<MonthStatus> vList = ad.selectMonthVacationNum(mMap, sqlSession);
			int vNum = 0;
			for(int j = 0; j < msList.size(); j++) {
				for(int v = 0; v < vList.size(); v++) {
					if(msList.get(j).getDay() == vList.get(v).getDay()) {
						vNum = vList.get(v).getWeekend();
					}
				}
				int dayNum = cal.get(Calendar.DAY_OF_WEEK);
				if((dayNum != 1 || dayNum != 7) && (vNum > 0)) {
					msList.get(j).setStatus(4);
					vNum--;
				}
					
			}


			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			tList.add(msList);
		}
		hmap.put("mhList", mhList);
		hmap.put("tList", tList);
		return hmap;
	}

	//해당 부서 근태리스트
	@Override
	public ArrayList<MonthAttendance> selectMAClose(int month, int teamCode) {
		HashMap<String, Object> hmap = new HashMap<>();
		hmap.put("month", month);
		hmap.put("teamCode", teamCode);
		return ad.selectMAClose(hmap, sqlSession);
	}
	//월근태 테이블 approvalID update
	@Override
	public int updateMAapprovalId(ArrayList<MonthAttendance> maList, Approval app) {
		int result = 0;
		int result1 = atd.insertApproval(sqlSession, app);
		int result12 = 0;
		if(result1>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(result1);
					result12 = atd.insertApprovalHistory(sqlSession, apphis);
				}
				for(int i = 0; i < maList.size(); i++) {
					maList.get(i).setApprovalYn("N");
				}
			} else {
				result12 = 1;
				//결재가 필요없을 경우라면, 카테고리를 3:승인으로 바꿈!
				for(int i = 0; i < maList.size(); i++) {
					maList.get(i).setApprovalYn("Y");
				}
			}
			
		}
		int month = maList.get(0).getMattDate();
		String approvalYN = maList.get(0).getApprovalYn();
		HashMap<String, Object> hmap = new HashMap<>();
		hmap.put("month", month);
		hmap.put("result1", result1);
		hmap.put("approvalYN", approvalYN);
		int updateCtn = 0;
		for(int i = 0; i < maList.size(); i++) {
			int mid = maList.get(i).getMid();
			hmap.put("mid", mid);
			//월근태 전자결재 id update
			updateCtn = ad.updateMAapprovalId(hmap, sqlSession);
		}
		//sa.setApprovalId(result1);
		
		return updateCtn;
	}

	//IP일치여부 확인
	@Override
	public int ipCheck(long ipAddressLoing) {
		//db에서 ip리스트 불러오기
		ArrayList<IP> ipList = ad.selectIPlIST(sqlSession);

		//일치 1 , 불일치2
		int check = 0;
		if(ipList != null && !ipList.isEmpty()) {
			for(int i = 0; i < ipList.size(); i++) {
				String[] sbArray = ipList.get(i).getStartBand().split("\\.");
				String[] ebArray = ipList.get(i).getEndBand().split("\\.");
				long sbLong = (Long.parseLong(sbArray[0])<<24)+
						(Long.parseLong(sbArray[1])<<16)+(Long.parseLong(sbArray[2])<<8)+
						(Long.parseLong(sbArray[3]));
				long ebLong = (Long.parseLong(ebArray[0])<<24)+
						(Long.parseLong(ebArray[1])<<16)+(Long.parseLong(ebArray[2])<<8)+
						(Long.parseLong(ebArray[3]));
				
				if(ipAddressLoing >= sbLong && ipAddressLoing <= ebLong) {
					check++;
				}
			}
		}
		System.out.println("ip일치여부 check : " + check);
		
		return check;
	}




	



}
