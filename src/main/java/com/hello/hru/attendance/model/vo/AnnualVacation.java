package com.hello.hru.attendance.model.vo;

public class AnnualVacation {
	private int annualCode;
	private int annualStd;
	private int vacationNum;
	private int vacationCode;
	
	public AnnualVacation() {}

	public AnnualVacation(int annualCode, int annualStd, int vacationNum, int vacationCode) {
		super();
		this.annualCode = annualCode;
		this.annualStd = annualStd;
		this.vacationNum = vacationNum;
		this.vacationCode = vacationCode;
	}

	public int getAnnualCode() {
		return annualCode;
	}

	public void setAnnualCode(int annualCode) {
		this.annualCode = annualCode;
	}

	public int getAnnualStd() {
		return annualStd;
	}

	public void setAnnualStd(int annualStd) {
		this.annualStd = annualStd;
	}

	public int getVacationNum() {
		return vacationNum;
	}

	public void setVacationNum(int vacationNum) {
		this.vacationNum = vacationNum;
	}

	public int getVacationCode() {
		return vacationCode;
	}

	public void setVacationCode(int vacationCode) {
		this.vacationCode = vacationCode;
	}

	@Override
	public String toString() {
		return "AnnualVacation [annualCode=" + annualCode + ", annualStd=" + annualStd + ", vacationNum=" + vacationNum
				+ ", vacationCode=" + vacationCode + "]";
	}
	
	
}
