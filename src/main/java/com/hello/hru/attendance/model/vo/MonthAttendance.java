package com.hello.hru.attendance.model.vo;

public class MonthAttendance {
	private int mattId;			//월근태 ID
	private int mid;			//사원번호
	private int mattDate;		//근무년월 (INT)
	private int weekdayNum;		//평일근무일수
	private int holidayNum;		//휴일근무일수
	private int vacationNum;	//연차사용일수
	private int noattNum;		//결근일수
	private int workMore;		//연장근로일수
	private int workNight;		//야간근로시간
	private int workHolidayOne;	//휴일근로시간 1
	private int workHolidayTwo;	//휴일근로시간 2
	private int approvalId;		//전자결재 ID
	private String approvalYn;	//승인여부
	
	public MonthAttendance(int mattId, int mid, int mattDate, int weekdayNum, int holidayNum, int vacationNum,
			int noattNum, int workMore, int workNight, int workHolidayOne, int workHolidayTwo, int approvalId,
			String approvalYn) {
		super();
		this.mattId = mattId;
		this.mid = mid;
		this.mattDate = mattDate;
		this.weekdayNum = weekdayNum;
		this.holidayNum = holidayNum;
		this.vacationNum = vacationNum;
		this.noattNum = noattNum;
		this.workMore = workMore;
		this.workNight = workNight;
		this.workHolidayOne = workHolidayOne;
		this.workHolidayTwo = workHolidayTwo;
		this.approvalId = approvalId;
		this.approvalYn = approvalYn;
	}

	public MonthAttendance() {
		super();
	}

	public int getMattId() {
		return mattId;
	}

	public void setMattId(int mattId) {
		this.mattId = mattId;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getMattDate() {
		return mattDate;
	}

	public void setMattDate(int mattDate) {
		this.mattDate = mattDate;
	}

	public int getWeekdayNum() {
		return weekdayNum;
	}

	public void setWeekdayNum(int weekdayNum) {
		this.weekdayNum = weekdayNum;
	}

	public int getHolidayNum() {
		return holidayNum;
	}

	public void setHolidayNum(int holidayNum) {
		this.holidayNum = holidayNum;
	}

	public int getVacationNum() {
		return vacationNum;
	}

	public void setVacationNum(int vacationNum) {
		this.vacationNum = vacationNum;
	}

	public int getNoattNum() {
		return noattNum;
	}

	public void setNoattNum(int noattNum) {
		this.noattNum = noattNum;
	}

	public int getWorkMore() {
		return workMore;
	}

	public void setWorkMore(int workMore) {
		this.workMore = workMore;
	}

	public int getWorkNight() {
		return workNight;
	}

	public void setWorkNight(int workNight) {
		this.workNight = workNight;
	}

	public int getWorkHolidayOne() {
		return workHolidayOne;
	}

	public void setWorkHolidayOne(int workHolidayOne) {
		this.workHolidayOne = workHolidayOne;
	}

	public int getWorkHolidayTwo() {
		return workHolidayTwo;
	}

	public void setWorkHolidayTwo(int workHolidayTwo) {
		this.workHolidayTwo = workHolidayTwo;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public String getApprovalYn() {
		return approvalYn;
	}

	public void setApprovalYn(String approvalYn) {
		this.approvalYn = approvalYn;
	}

	@Override
	public String toString() {
		return "MonthAttendance [mattId=" + mattId + ", mid=" + mid + ", mattDate=" + mattDate + ", weekdayNum="
				+ weekdayNum + ", holidayNum=" + holidayNum + ", vacationNum=" + vacationNum + ", noattNum=" + noattNum
				+ ", workMore=" + workMore + ", workNight=" + workNight + ", workHolidayOne=" + workHolidayOne
				+ ", workHolidayTwo=" + workHolidayTwo + ", approvalId=" + approvalId + ", approvalYn=" + approvalYn
				+ "]";
	}
	
	
}
