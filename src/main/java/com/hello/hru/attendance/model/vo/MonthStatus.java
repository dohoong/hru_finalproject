package com.hello.hru.attendance.model.vo;

public class MonthStatus {
	private int day;
	private int status;	//출근 1, 지각2, 야특근3, 휴가 4
	private int weekend; 	//평일 0, 주말1
	
	public MonthStatus() {}

	public MonthStatus(int day, int status, int weekend) {
		super();
		this.day = day;
		this.status = status;
		this.weekend = weekend;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getWeekend() {
		return weekend;
	}

	public void setWeekend(int weekend) {
		this.weekend = weekend;
	}

	@Override
	public String toString() {
		return "MonthStatus [day=" + day + ", status=" + status + ", weekend=" + weekend + "]";
	}
	
	
}
