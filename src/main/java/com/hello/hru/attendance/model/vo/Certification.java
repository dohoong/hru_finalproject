package com.hello.hru.attendance.model.vo;

import java.sql.Date;

public class Certification {
	private int mId;
	private int creaId;
	private Date requestDate;
	private String usage;
	private int category;
	private int approvalId;
	private String approvalYN;
	
	public Certification() {}

	public Certification(int mId, int creaId, Date requestDate, String usage, int category, int approvalId,
			String approvalYN) {
		super();
		this.mId = mId;
		this.creaId = creaId;
		this.requestDate = requestDate;
		this.usage = usage;
		this.category = category;
		this.approvalId = approvalId;
		this.approvalYN = approvalYN;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public int getCreaId() {
		return creaId;
	}

	public void setCreaId(int creaId) {
		this.creaId = creaId;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public String getApprovalYN() {
		return approvalYN;
	}

	public void setApprovalYN(String approvalYN) {
		this.approvalYN = approvalYN;
	}

	@Override
	public String toString() {
		return "Certification [mId=" + mId + ", creaId=" + creaId + ", requestDate=" + requestDate + ", usage=" + usage
				+ ", category=" + category + ", approvalId=" + approvalId + ", approvalYN=" + approvalYN + "]";
	}
	
	
}
