package com.hello.hru.attendance.model.vo;

public class VacationBasic {
	private int vacationCode;
	private String vacationName;
	private int category;
	private String useYN;
	private String salaryYN;
	
	private VacationBasic() {}

	public VacationBasic(int vacationCode, String vacationName, int category, String useYN, String salaryYN) {
		super();
		this.vacationCode = vacationCode;
		this.vacationName = vacationName;
		this.category = category;
		this.useYN = useYN;
		this.salaryYN = salaryYN;
	}

	public int getVacationCode() {
		return vacationCode;
	}

	public void setVacationCode(int vacationCode) {
		this.vacationCode = vacationCode;
	}

	public String getVacationName() {
		return vacationName;
	}

	public void setVacationName(String vacationName) {
		this.vacationName = vacationName;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getUseYN() {
		return useYN;
	}

	public void setUserYN(String useYN) {
		this.useYN = useYN;
	}

	public String getSalaryYN() {
		return salaryYN;
	}

	public void setSalaryYN(String salaryYN) {
		this.salaryYN = salaryYN;
	}

	@Override
	public String toString() {
		return "VacationBasic [vacationCode=" + vacationCode + ", vacationName=" + vacationName + ", category="
				+ category + ", useYN=" + useYN + ", salaryYN=" + salaryYN + "]";
	}
	
	
}
