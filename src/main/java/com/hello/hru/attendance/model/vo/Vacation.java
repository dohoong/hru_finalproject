package com.hello.hru.attendance.model.vo;

import java.sql.Date;

public class Vacation {
	private int vacationId;
	private int mId;
	private int approvalId;
	private String approvalYN;
	private int vCode;
	private String vStart;
	private int vNum;
	private String reason;
	private int category;
	private int approvalCode;
	
	public Vacation() {}

	public Vacation(int vacationId, int mId, int approvalId, String approvalYN, int vCode, String vStart, int vNum,
			String reason, int category, int approvalCode) {
		super();
		this.vacationId = vacationId;
		this.mId = mId;
		this.approvalId = approvalId;
		this.approvalYN = approvalYN;
		this.vCode = vCode;
		this.vStart = vStart;
		this.vNum = vNum;
		this.reason = reason;
		this.category = category;
		this.approvalCode = approvalCode;
	}

	public int getVacationId() {
		return vacationId;
	}

	public void setVacationId(int vacationId) {
		this.vacationId = vacationId;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public String getApprovalYN() {
		return approvalYN;
	}

	public void setApprovalYN(String approvalYN) {
		this.approvalYN = approvalYN;
	}

	public int getvCode() {
		return vCode;
	}

	public void setvCode(int vCode) {
		this.vCode = vCode;
	}

	public String getvStart() {
		return vStart;
	}

	public void setvStart(String vStart) {
		this.vStart = vStart;
	}

	public int getvNum() {
		return vNum;
	}

	public void setvNum(int vNum) {
		this.vNum = vNum;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(int approvalCode) {
		this.approvalCode = approvalCode;
	}

	@Override
	public String toString() {
		return "Vacation [vacationId=" + vacationId + ", mId=" + mId + ", approvalId=" + approvalId + ", approvalYN="
				+ approvalYN + ", vCode=" + vCode + ", vStart=" + vStart + ", vNum=" + vNum + ", reason=" + reason
				+ ", category=" + category + ", approvalCode=" + approvalCode + "]";
	}

	
	
	
}
