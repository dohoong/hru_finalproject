package com.hello.hru.attendance.model.vo;

import java.sql.Date;

public class MemberAnnualVacation {
	private int mId;
	private String mName;
	private Date enterDate;
	private String yearStart;	//연차기간 시작일
	private String yearEnd;	//연차기간 마지막일
	private int avNum;		//발생일수
	private int addNum;		//조정연차일수
	private int useNum;		//사용일수
	private int restNum;	//잔여일수
	
	public MemberAnnualVacation() {}

	public MemberAnnualVacation(int mId, String mName, Date enterDate, String yearStart, String yearEnd, int avNum,
			int addNum, int useNum, int restNum) {
		super();
		this.mId = mId;
		this.mName = mName;
		this.enterDate = enterDate;
		this.yearStart = yearStart;
		this.yearEnd = yearEnd;
		this.avNum = avNum;
		this.addNum = addNum;
		this.useNum = useNum;
		this.restNum = restNum;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public Date getEnterDate() {
		return enterDate;
	}

	public void setEnterDate(Date enterDate) {
		this.enterDate = enterDate;
	}

	public String getYearStart() {
		return yearStart;
	}

	public void setYearStart(String yearStart) {
		this.yearStart = yearStart;
	}

	public String getYearEnd() {
		return yearEnd;
	}

	public void setYearEnd(String yearEnd) {
		this.yearEnd = yearEnd;
	}

	public int getAvNum() {
		return avNum;
	}

	public void setAvNum(int avNum) {
		this.avNum = avNum;
	}

	public int getAddNum() {
		return addNum;
	}

	public void setAddNum(int addNum) {
		this.addNum = addNum;
	}

	public int getUseNum() {
		return useNum;
	}

	public void setUseNum(int useNum) {
		this.useNum = useNum;
	}

	public int getRestNum() {
		return restNum;
	}

	public void setRestNum(int restNum) {
		this.restNum = restNum;
	}

	@Override
	public String toString() {
		return "MemberAnnualVacation [mId=" + mId + ", mName=" + mName + ", enterDate=" + enterDate + ", yearStart="
				+ yearStart + ", yearEnd=" + yearEnd + ", avNum=" + avNum + ", addNum=" + addNum + ", useNum=" + useNum
				+ ", restNum=" + restNum + "]";
	}

	
	
}
