package com.hello.hru.attendance.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.hello.hru.attendance.model.vo.AnnualVacation;
import com.hello.hru.attendance.model.vo.Certification;
import com.hello.hru.attendance.model.vo.DayAttendance;
import com.hello.hru.attendance.model.vo.MemberAnnualVacation;
import com.hello.hru.attendance.model.vo.MonthAttendance;
import com.hello.hru.attendance.model.vo.MonthStatus;
import com.hello.hru.attendance.model.vo.OneDaySearch;
import com.hello.hru.attendance.model.vo.SpecialAttendance;
import com.hello.hru.attendance.model.vo.SpecialAttendanceSearch;
import com.hello.hru.attendance.model.vo.Vacation;
import com.hello.hru.attendance.model.vo.VacationBasic;
import com.hello.hru.attendance.model.vo.VacationRequest;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.member.model.vo.MemberHrInfo;
import com.hello.hru.member.model.vo.SearchMember;
import com.hello.hru.system.model.vo.IP;

@Repository
public class AttendanceDaoImpl implements AttendanceDao{

	//기준정보 휴가관리 휴가항목설정 - select
	@Override
	public ArrayList<VacationBasic> selectvList(SqlSessionTemplate sqlSession) {
		ArrayList<VacationBasic> vList = (ArrayList)sqlSession.selectList("Attendance.selectVacationBasic");
		return vList;
	}
	//기준정보 휴가관리 연차부여일수 - select
	@Override
	public ArrayList<AnnualVacation> selectaList(SqlSessionTemplate sqlSession) {
		ArrayList<AnnualVacation> aList = (ArrayList)sqlSession.selectList("Attendance.selectAnnualVacation");
		return aList;
	}
	//기준정보 휴가관리 휴가항목 INSERT
	@Override
	public int insertVacationBasic(VacationBasic vb, SqlSessionTemplate sqlSession) {
		return sqlSession.insert("Attendance.insertVacationBasic", vb);
	}
	//기준정보 휴가관리 휴가항목 UPDATE
	@Override
	public int updateVacationBasic(VacationBasic vb, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Attendance.updateVacationBasic", vb);
	}
	//기준정보 휴가관리 휴가항목 DELETE
	@Override
	public int deleteVacationBasic(VacationBasic vb, SqlSessionTemplate sqlSession) {
		return sqlSession.delete("Attendance.deleteVacationBasic", vb);
	}
	
	//기준정보 연차부여일수 INSERT
	@Override
	public int insertAnnualVacation(AnnualVacation av, SqlSessionTemplate sqlSession) {
		return sqlSession.insert("Attendance.insertAnnualVacation", av);
	}
	//연차 기준년수 check
	@Override
	public AnnualVacation checkAvStd(int annualStd, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectAnnualStd", annualStd);
	}
	//연차부여일수 UPDATE
	@Override
	public int updateAnnualVacation(AnnualVacation aVacation, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Attendance.updateAnnualVacation", aVacation);
	}
	//연차부여일수 DELETE
	@Override
	public int deleteAnnualVacation(AnnualVacation av, SqlSessionTemplate sqlSession) {
		return sqlSession.delete("Attendance.deleteAnnualVacation", av);
	}
	//로그인유저 정보 불러오기
	@Override
	public Member selectMember(Member m, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Member.selectLoginUser", m);
	}
	//일근태 불러오기
	@Override
	public DayAttendance selectDayAttendance(int mId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectDayAttendance", mId);
	}
	//일근태 7시 이전일때
	@Override
	public DayAttendance selectbeforeAttendance(int mId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectbeforeAttendance", mId);
		}
	//일근태 DB확인
	@Override
	public DayAttendance dayAttendanceCheck(int mId, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectDayAttendance", mId);
	}
	//일근태 insert
	@Override
	public int insertDayAttendance(int mId, SqlSessionTemplate sqlSession) {
		return sqlSession.insert("Attendance.insertDayAttendance", mId);
	}
	//일근태 update - 7시 이후
	@Override
	public int updateDayAttendance(DayAttendance da, SqlSessionTemplate sqlSession) {
		sqlSession.update("Attendance.updateDayAttendance", da);
		return da.getDattId();
	}
	//일근태 update - 7시 이전
	@Override
	public int updateBeforeAttendance(int mId, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Attendance.updateBeforeAttendance", mId);
	}
	
	//연차 발생일수 set
	@Override
	public int selectMemberAnnualNum(MemberAnnualVacation mv, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectMemberAnnualNum", mv);
	}
	//남은 연차일수 set
	@Override
	public int selectRemainVacation(MemberAnnualVacation mv, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectRemainVacation", mv);
	}
	//조정연차 일수
	@Override
	public int selectAddVacation(MemberAnnualVacation mv, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectAddVacation", mv);
	}
	
	//휴가테이블 INSERT
	@Override
	public int insertVacationRequest(Vacation v, SqlSessionTemplate sqlSession) {
		return sqlSession.insert("Attendance.insertVacationRequest", v);
	}
	//휴가신청 이력 list
	@Override
	public ArrayList<VacationRequest> selectVacationList(int mId, SqlSessionTemplate sqlSession) {
		ArrayList<VacationRequest> vlist = (ArrayList) sqlSession.selectList("Attendance.selectVacationList", mId);
		return vlist;
	}
	//야특근 테이블 INSERT
	@Override
	public int insertSpecialAttendance(SpecialAttendance sa, SqlSessionTemplate sqlSession) {
		sqlSession.insert("Attendance.insertSpecialAttendance", sa);
		return sa.getSattId();
	}
	//일근태 sattid UPDATE
	@Override
	public int updateSattId(SpecialAttendance sa, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Attendance.updateSattId", sa);
	}
	//야특근 테이블 INSERT
	@Override
	public int insertCertification(Certification cf, SqlSessionTemplate sqlSession) {
		return sqlSession.insert("Attendance.insertCertification", cf);
	}
	//제증명 신청 이력
	@Override
	public ArrayList<Certification> selectCertification(int mId, SqlSessionTemplate sqlSession) {
		 ArrayList<Certification> cLsit = (ArrayList)sqlSession.selectList("Attendance.selectCertificationList", mId);
		 return cLsit;
	}
	//일일 근태관리 검색
	@Override
	public ArrayList<OneDaySearch> selectOneDayWork(OneDaySearch ods, SqlSessionTemplate sqlSession) {
		ArrayList<OneDaySearch> oList = (ArrayList)sqlSession.selectList("Attendance.selectOneDayWork", ods);
		return oList;
	}
	//기타 근무조회 select
	@Override
	public ArrayList<SpecialAttendanceSearch> selectSpecialAttendanceSearch(SpecialAttendanceSearch sas, SqlSessionTemplate sqlSession) {
		ArrayList<SpecialAttendanceSearch> saList = (ArrayList) sqlSession.selectList("Attendance.selectSpecialAttendanceSearch", sas);
		return saList;
	}
	
	//휴가신청 불러오기 & vacationCode 2로 변경
	@Override
	public Vacation selectVacation(Vacation v, SqlSessionTemplate sqlSession) {
		Vacation vacation = (Vacation) sqlSession.selectOne("Attendance.selectVacation", v.getVacationId());
		return vacation; 
	}
	//휴가신청 날짜 중복확인
	@Override
	public ArrayList<Vacation> selectDuplicateDate(Vacation v, SqlSessionTemplate sqlSession) {
		ArrayList<Vacation> check = (ArrayList) sqlSession.selectList("Attendance.selectDuplicateDate", v);
		return check;
	}
	//야특근 중복 확인
	@Override
	public ArrayList<SpecialAttendance> selectSAcheck(SpecialAttendance sa, SqlSessionTemplate sqlSession) {
		ArrayList<SpecialAttendance> sList = (ArrayList) sqlSession.selectList("Attendance.selectSAcheck", sa);
		return sList;
	}
	//주 야특근 시간 가져오기
	@Override
	public int selectSpecialHour(HashMap<String, Object> hMap, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectSpecialHour", hMap);
	}
	//남은 연차일수 불러오기
	@Override
	public int selectRemainV(Vacation v, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectRemainV", v);
	}
	//월 근태 사원정보 불러오기
	@Override
	public ArrayList<SearchMember> selectMonthMember(HashMap<String, Object> map, SqlSessionTemplate sqlSession) {
		ArrayList<SearchMember> smList = (ArrayList)sqlSession.selectList("Attendance.selectMonthMember", map);
		return smList;
	}
	//월 근태 테이블 확인
	@Override
	public ArrayList<SearchMember> monthTableCheck(HashMap<String, Object> map, SqlSessionTemplate sqlSession) {
		ArrayList<SearchMember> mtCheck = (ArrayList)sqlSession.selectList("Attendance.monthTableCheck", map);
		return mtCheck;
	}
	//평일 근무일수 가져오기
	@Override
	public int selectWeekendNum(HashMap<String, Object> mList, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectWeekendNum", mList);
	}
	//휴일 근무일수 가져오기
	@Override
	public int selecHolidayNum(HashMap<String, Object> mList, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectHolidayNum", mList);
	}
	//연차 사용일수
	@Override
	public ArrayList<Vacation> selectVacationNum(HashMap<String, Object> mList, SqlSessionTemplate sqlSession) {
		ArrayList<Vacation> vList = (ArrayList) sqlSession.selectList("Attendance.selectVacationNum", mList);
		return vList;
	}
	//연장 근로시간 가져오기
	@Override
	public int selectWorkMore(HashMap<String, Object> mList, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectWorkMore", mList);
	}
	//야간 근로시간 가져오기
	@Override
	public int selectWorkNight(HashMap<String, Object> mList, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectWorkNight", mList);
	}
	//휴일근로 전체시간 가져오기
	@Override
	public ArrayList<Integer> selectHoliTotal(HashMap<String, Object> mList, SqlSessionTemplate sqlSession) {
		ArrayList<Integer> hTotal = (ArrayList) sqlSession.selectList("Attendance.selectHoliTotal", mList);
		return hTotal;
	}
	//월 근태 insert
	@Override
	public int insertMonthAttendance(MonthAttendance ma, SqlSessionTemplate sqlSession) {
		return sqlSession.insert("Attendance.insertMonthAttendance", ma);
	}
	//사원 월근태 가져오기
	@Override
	public MonthAttendance selectMemberMA(MonthAttendance m, SqlSessionTemplate sqlSession) {
		MonthAttendance ma = sqlSession.selectOne("Attendance.selectMemberMA", m);
		return ma;
	}
	//월근태 수정
	@Override
	public int updateMonthAttendance(MonthAttendance ma, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Attendance.updateMonthAttendance", ma);
	}
	//월근태 수정 approval check
	@Override
	public String maApprovalCheck(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.maApprovalCheck", hmap);
	}
	//월근태 status
	@Override
	public ArrayList<MonthStatus> selectMemberMAstatus(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession) {
		ArrayList<MonthStatus> dbList = (ArrayList) sqlSession.selectList("Attendance.selectMemberMAstatus", hmap);
		return dbList;
	}
	//휴가 리스트 불러오기
	@Override
	public ArrayList<MonthStatus> selectMonthVacationNum(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession) {
		ArrayList<MonthStatus> vList = (ArrayList) sqlSession.selectList("Attendance.selectMonthVacationNum", hmap);
		return vList;
	}
	//사원정보 가져오기
	@Override
	public MemberHrInfo selectMemberInfo(int mid, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Member.selectMemberHrInfo", mid);
	}
	//월 근태 테이블
	@Override
	public MonthAttendance selectMemberma(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Attendance.selectMemberma", hmap);
	}
	//부서별 월근태현황 불러오기
	@Override
	public ArrayList<MonthAttendance> selectTeamMonthAttendance(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession) {
		ArrayList<MonthAttendance> maList = (ArrayList) sqlSession.selectList("Attendance.selectTeamMonthAttendance", hmap);
		return maList;
	}
	//부서 사원정보 LIST불러오기
	@Override
	public ArrayList<SearchMember> selectTeamHfInfo(HashMap<String, Object> tMap, SqlSessionTemplate sqlSession) {
		ArrayList<SearchMember> mhList = (ArrayList) sqlSession.selectList("Attendance.selectTeamHfInfo", tMap);
		return mhList;
	}
	//해당 부서 근태리스트
	@Override
	public ArrayList<MonthAttendance> selectMAClose(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("Attendance.selectMAClose", hmap);
	}
	//월근태 전자결재 id update
	@Override
	public int updateMAapprovalId(HashMap<String, Object> hmap, SqlSessionTemplate sqlSession) {
		return sqlSession.update("Attendance.updateMAapprovalId", hmap);
	}
	//db에서 ip리스트 불러오기
	@Override
	public ArrayList<IP> selectIPlIST(SqlSessionTemplate sqlSession) {
		return (ArrayList)sqlSession.selectList("Attendance.selectIPlIST");
	}
	
	
	



}
