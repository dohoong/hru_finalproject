package com.hello.hru.attendance.model.vo;

import java.sql.Date;
import java.sql.Timestamp;

public class SpecialAttendance {
	private int mId;
	private int sattId;
	private String startDate;
	private String startTime;
	private String endDate;
	private String endTime;
	private String approvalYN;
	private int approvalId;
	private int category;
	private String sattDate;
	
	public SpecialAttendance() {}

	public SpecialAttendance(int mId, int sattId, String startDate, String startTime, String endDate, String endTime,
			String approvalYN, int approvalId, int category, String sattDate) {
		super();
		this.mId = mId;
		this.sattId = sattId;
		this.startDate = startDate;
		this.startTime = startTime;
		this.endDate = endDate;
		this.endTime = endTime;
		this.approvalYN = approvalYN;
		this.approvalId = approvalId;
		this.category = category;
		this.sattDate = sattDate;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public int getSattId() {
		return sattId;
	}

	public void setSattId(int sattId) {
		this.sattId = sattId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getApprovalYN() {
		return approvalYN;
	}

	public void setApprovalYN(String approvalYN) {
		this.approvalYN = approvalYN;
	}

	public int getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getSattDate() {
		return sattDate;
	}

	public void setSattDate(String sattDate) {
		this.sattDate = sattDate;
	}

	@Override
	public String toString() {
		return "SpecialAttendance [mId=" + mId + ", sattId=" + sattId + ", startDate=" + startDate + ", startTime="
				+ startTime + ", endDate=" + endDate + ", endTime=" + endTime + ", approvalYN=" + approvalYN
				+ ", approvalId=" + approvalId + ", category=" + category + ", sattDate=" + sattDate + "]";
	}


	
	
}
