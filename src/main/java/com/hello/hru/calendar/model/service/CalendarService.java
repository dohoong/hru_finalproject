package com.hello.hru.calendar.model.service;

import java.util.ArrayList; 

import com.hello.hru.calendar.exception.InsertExceptionforCalendar;
import com.hello.hru.calendar.exception.SelectExceptionforCalendar;
import com.hello.hru.calendar.model.vo.Mschedule;

public interface CalendarService {

	ArrayList<Mschedule> selectEmpCalendarMain(Mschedule ms) throws SelectExceptionforCalendar;

	int insertSchedule(Mschedule ms) throws InsertExceptionforCalendar;

}
