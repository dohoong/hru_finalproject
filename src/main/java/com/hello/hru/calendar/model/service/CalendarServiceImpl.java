package com.hello.hru.calendar.model.service;

import java.util.ArrayList; 

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.calendar.exception.InsertExceptionforCalendar;
import com.hello.hru.calendar.exception.SelectExceptionforCalendar;
import com.hello.hru.calendar.model.dao.CalendarDao;
import com.hello.hru.calendar.model.vo.Mschedule;

@Service
public class CalendarServiceImpl implements CalendarService{

	@Autowired
	private CalendarDao cd;

	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	public ArrayList<Mschedule> selectEmpCalendarMain(Mschedule ms) throws SelectExceptionforCalendar{
		return (ArrayList) cd.selectEmpCalendarMain(sqlSession, ms);
	}

	@Override
	public int insertSchedule(Mschedule ms) throws InsertExceptionforCalendar {
		return cd.insertSchedule(sqlSession, ms);
	}
	
	
}
