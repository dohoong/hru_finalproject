package com.hello.hru.calendar.model.dao;

import java.util.ArrayList;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.calendar.exception.InsertExceptionforCalendar;
import com.hello.hru.calendar.exception.SelectExceptionforCalendar;
import com.hello.hru.calendar.model.vo.Mschedule;
 
@Repository
public class CalendarDaoImpl implements CalendarDao{

	@Override
	public ArrayList<Mschedule> selectEmpCalendarMain(SqlSessionTemplate sqlSession, Mschedule ms) throws SelectExceptionforCalendar{
		
		return (ArrayList) sqlSession.selectList("Calendar.selectEmpCalendarMain", ms);
	}

	@Override
	public int insertSchedule(SqlSessionTemplate sqlSession, Mschedule ms) throws InsertExceptionforCalendar {
		
		return sqlSession.insert("Calendar.insertSchedule", ms);
	}

}
