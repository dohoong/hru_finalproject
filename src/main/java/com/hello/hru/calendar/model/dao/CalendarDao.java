package com.hello.hru.calendar.model.dao;

import java.util.ArrayList; 

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.calendar.exception.InsertExceptionforCalendar;
import com.hello.hru.calendar.exception.SelectExceptionforCalendar;
import com.hello.hru.calendar.model.vo.Mschedule;

public interface CalendarDao {

	ArrayList<Mschedule> selectEmpCalendarMain(SqlSessionTemplate sqlSession, Mschedule ms) throws SelectExceptionforCalendar;

	int insertSchedule(SqlSessionTemplate sqlSession, Mschedule ms) throws InsertExceptionforCalendar;

}
