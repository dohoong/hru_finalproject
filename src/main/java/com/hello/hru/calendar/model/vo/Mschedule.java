package com.hello.hru.calendar.model.vo;
 
import java.sql.Date;

public class Mschedule implements java.io.Serializable {

	private int mscheduleid;
	private int mid;
	private String title;
	private String mname;
	private Date starttime;
	private Date endtime;
	private String content;
	private String removeyn;

	public Mschedule() {
	}

	public Mschedule(int mscheduleid, int mid, String title, String mname, Date starttime, Date endtime, String content,
			String removeyn) {
		super();
		this.mscheduleid = mscheduleid;
		this.mid = mid;
		this.title = title;
		this.mname = mname;
		this.starttime = starttime;
		this.endtime = endtime;
		this.content = content;
		this.removeyn = removeyn;
	}

	public int getMscheduleid() {
		return mscheduleid;
	}

	public void setMscheduleid(int mscheduleid) {
		this.mscheduleid = mscheduleid;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemoveyn() {
		return removeyn;
	}

	public void setRemoveyn(String removeyn) {
		this.removeyn = removeyn;
	}

	@Override
	public String toString() {
		return "Mschedule [mscheduleid=" + mscheduleid + ", mid=" + mid + ", title=" + title + ", mname=" + mname
				+ ", starttime=" + starttime + ", endtime=" + endtime + ", content=" + content + ", removeyn="
				+ removeyn + "]";
	}


}
