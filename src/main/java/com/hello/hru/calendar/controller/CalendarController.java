package com.hello.hru.calendar.controller;

import java.sql.Date;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.calendar.exception.InsertExceptionforCalendar;
import com.hello.hru.calendar.exception.SelectExceptionforCalendar;
import com.hello.hru.calendar.model.service.CalendarService;
import com.hello.hru.calendar.model.vo.Mschedule;
import com.hello.hru.member.model.vo.Member;

@Controller
public class CalendarController {

	@Autowired
	private CalendarService cs;

	@RequestMapping("selectEmpCalendarMain.cal")
	public String selectEmpCalendarMain(HttpServletRequest request,Model model,Mschedule ms) {

		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		ms.setMid(mid);

		try {
			ArrayList<Mschedule> list = cs.selectEmpCalendarMain(ms);

			model.addAttribute("list", list);

			return "member/employeeCalendar/empCalendarMain";		
		} catch (SelectExceptionforCalendar e) {
			model.addAttribute("msg", "조회 실패!!");

			return "common/errorPage";

		}				
	}

	@RequestMapping("insertSchedule.cal")
	public String insertSchedule(HttpServletRequest request, Model model, String title, String content, Date starttime, Date endtime) {

		Mschedule ms = new Mschedule();
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		ms.setMid(mid);		
		ms.setTitle(title);
		ms.setContent(content);
		ms.setStarttime(starttime);
		ms.setEndtime(endtime);	

		try {
			cs.insertSchedule(ms);
			 
			model.addAttribute("ms", ms);
			
			return "redirect: selectEmpCalendarMain.cal";
		} catch (InsertExceptionforCalendar e) {
			
			model.addAttribute("msg", "조회 실패!!");

			return "common/errorPage";

		}
	}

}
