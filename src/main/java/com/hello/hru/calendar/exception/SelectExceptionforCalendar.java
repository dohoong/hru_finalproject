package com.hello.hru.calendar.exception;

public class SelectExceptionforCalendar extends Exception{
	public SelectExceptionforCalendar(String msg) {
		super(msg);
	}
}
