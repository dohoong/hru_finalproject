package com.hello.hru.system.model.vo;

public class Team {
	private int teamCode;
	private int groupCode;
	private String teamName;
	private String groupName;
	private String status;
	
	public Team() {}

	public Team(int teamCode, int groupCode, String teamName, String groupName, String status) {
		super();
		this.teamCode = teamCode;
		this.groupCode = groupCode;
		this.teamName = teamName;
		this.groupName = groupName;
		this.status = status;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	public int getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(int groupCode) {
		this.groupCode = groupCode;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Team [teamCode=" + teamCode + ", groupCode=" + groupCode + ", teamName=" + teamName + ", groupName="
				+ groupName + ", status=" + status + "]";
	}
	
}
