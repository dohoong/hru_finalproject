package com.hello.hru.system.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

public interface StatisticalDao {

	HashMap<String, Object> selectMemberCounts(SqlSessionTemplate sqlSession);


	ArrayList<String> getPpNameList(SqlSessionTemplate sqlSession);


	Object selectppNameCounts(SqlSessionTemplate sqlSession, String ppName);


	Object selectMECount(SqlSessionTemplate sqlSession, int month);


	Object selectMOCount(SqlSessionTemplate sqlSession, int month);


	Object selectYear(SqlSessionTemplate sqlSession, int year);

}
