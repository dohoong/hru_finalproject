package com.hello.hru.system.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.system.model.dao.SystemDao;
import com.hello.hru.system.model.vo.BusinessGroup;
import com.hello.hru.system.model.vo.EnterCategory;
import com.hello.hru.system.model.vo.IP;
import com.hello.hru.system.model.vo.Job;
import com.hello.hru.system.model.vo.Position;
import com.hello.hru.system.model.vo.Preposition;
import com.hello.hru.system.model.vo.Team;

@Service
public class SystemServiceImpl implements SystemService{
	@Autowired
	private SystemDao sd;
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	//회사정보 select
	@Override
	public HashMap<String, Object> selectBusinessGroup() {
		
		ArrayList<BusinessGroup> balist = sd.selectBusinessGroup(sqlSession);

		
		HashMap<String, Object> bglistMap = new HashMap<String, Object>();
		bglistMap.put("balist", balist);
		
		return bglistMap;
	}

	//회사정보 update
	@Override
	public int updateBusinessGroup(BusinessGroup bg) {
		
		return sd.updateBusinessGroup(sqlSession,bg);
	}
	
	
	//조직관리  select 
	@Override
	public HashMap<String, Object> selectOrganization() {
		
		ArrayList<Team> otlist = sd.selectTeam(sqlSession);
		ArrayList<Job> ojlist = sd.selectJob(sqlSession);
		ArrayList<EnterCategory> oelist = sd.selectEnterCategory(sqlSession);
		
		HashMap<String, Object> solistMap = new HashMap<String, Object>();
		solistMap.put("otlist", otlist);
		solistMap.put("ojlist", ojlist);
		solistMap.put("oelist", oelist);
		
		
		return solistMap;
	}
	//조직관리 직종 insert
	@Override
	public int insertJob(Job job) {
		return sd.insertJob(sqlSession,job);
	}
	
	//조직관리 직종update
	@Override
	public int updateJob(Job job) {
		return sd.updateJob(sqlSession,job);
	}
	
	//조직관리 입사구분 insert
	@Override
	public int insertEnterCategory(EnterCategory enter) {

		return sd.insertEnterCategory(sqlSession,enter);
	}

	
	
	//조직관리 입사구분 update
	@Override
	public int updateEnterCategory(EnterCategory enter) {
		
		return sd.updateEnterCategory(sqlSession,enter);
	}
	//조직관리 입사구분 delete
	@Override
	public int deleteEnterCategory(EnterCategory enter) {
		
		return sd.deleteEnterCategory(sqlSession,enter);
	}

	
	
	
	//직급관리 select 
	@Override
	public HashMap<String, Object> selectAuthorityAdmin() {
		
		ArrayList<Preposition> applist = sd.selectPreposition(sqlSession);
		ArrayList<Position> aplist = sd.selectPosition(sqlSession);
		
		HashMap<String, Object> salistMap = new HashMap<String, Object>();
		
		salistMap.put("applist",applist);
		salistMap.put("aplist",aplist);
		
		return salistMap;
	}
	
	//직급관리 insert
	@Override
	public int insertPreposition(Preposition pp) {		
		return sd.insertPreposition(sqlSession,pp);
	}
	
	//직급관리 delete
	@Override
	public int deletePreposition(Preposition pp) {
		
		return sd.deletePreposition(sqlSession,pp);
	}
	
	//직급관리 update
	@Override
	public int updatePreposition(Preposition pp) {
		
		return sd.updatePreposition(sqlSession,pp);
	}
	
	//호봉관리 update
	@Override
	public int updatePosition(Position p) {
		
		return sd.updatePosition(sqlSession,p);
	}
	
	//호봉관리 insert
	@Override
	public int insertPosition(Position p) {
		
		return sd.insertPosition(sqlSession,p);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchLeaderO(int category, String text) {
		
		ArrayList<HashMap<String, Object>> lslist = null;
		
		if(category == 1) {
			lslist = sd.searchTeamCode(sqlSession,text);
			
			
		}else if(category == 2){
			lslist = sd.searchTeamName(sqlSession,text);
			
		}else {
			lslist = sd.searchLeaderName(sqlSession,text);
		}
		
		
		return lslist;
	}
	//ip주소 insert 
	@Override
	public int insertIP(IP ip) {
		return sd.insertIP(ip, sqlSession);
	}

	//ip접속관리 뷰 - ipList불러오기
	@Override
	public ArrayList<IP> selectIPList() {
		return sd.selectIPList(sqlSession);
	}
	//ip주소 delete
	@Override
	public int deleteIP(int ipCode) {
		return sd.deleteIP(sqlSession, ipCode);
	}
	
	//ip주소 update
	@Override
	public int updateIP(IP ip) {
		return sd.updateIP(sqlSession, ip);
	}

	//ip주소 사용여부
	@Override
	public int ipUSE(String ipUse) {
		return sd.ipUSE(sqlSession, ipUse);
	}

	//ip대역폭 사용유무 
	@Override
	public int ipUseCheck() {
		String useYN = sd.ipUseCheck(sqlSession);
		int result = 0;
		if(useYN.equals("Y")) {
			result = 1;
		}else {
			result = 2;
		}
		return result;
	}

	@Override
	public int compayStay(BusinessGroup bg) {
		
		return sd.companyStay(sqlSession,bg);
	}

	@Override
	public int insertTeam(Team team) {
		
		return sd.insertTeam(sqlSession,team);
	}

	@Override
	public int updateTeam(Team team) {
		
		return sd.updateTeam(sqlSession,team);
	}

	@Override
	public int deleteJob(Job job) {

		return sd.deleteJob(sqlSession,job);
	}
	



}
