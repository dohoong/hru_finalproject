package com.hello.hru.system.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;

import com.hello.hru.system.model.vo.BusinessGroup;
import com.hello.hru.system.model.vo.EnterCategory;
import com.hello.hru.system.model.vo.IP;
import com.hello.hru.system.model.vo.Job;
import com.hello.hru.system.model.vo.Position;
import com.hello.hru.system.model.vo.Preposition;
import com.hello.hru.system.model.vo.Team;

public interface SystemDao {
	
	//회사정보select
	ArrayList<BusinessGroup> selectBusinessGroup(SqlSessionTemplate sqlSession);
	
	//회사정보update
	int updateBusinessGroup(SqlSessionTemplate sqlSession, BusinessGroup bg);
	
	//조직관리 부서관리 select
	ArrayList<Team> selectTeam(SqlSessionTemplate sqlSession);
	
	//조직관리 직종 insert
	int insertJob(SqlSessionTemplate sqlSession, Job job);
	
	//조직관리 직종관리 select
	ArrayList<Job> selectJob(SqlSessionTemplate sqlSession);
	
	//조직관리 입사관리 select
	ArrayList<EnterCategory> selectEnterCategory(SqlSessionTemplate sqlSession);
	
	//조직관리 직종 update
	int updateJob(SqlSessionTemplate sqlSession, Job job);
	
	//조직관리 입사구분 insert
	int insertEnterCategory(SqlSessionTemplate sqlSession, EnterCategory enter);
	
	//조직관리 입사구분 update
	int updateEnterCategory(SqlSessionTemplate sqlSession, EnterCategory enter);
	
	//조직관리 입사구분 delete
	int deleteEnterCategory(SqlSessionTemplate sqlSession, EnterCategory enter);
	
	//직급관리 직위 select
	ArrayList<Preposition> selectPreposition(SqlSessionTemplate sqlSession);
	
	//호봉관리 select
	ArrayList<Position> selectPosition(SqlSessionTemplate sqlSession);
	
	//직급관리 insert
	int insertPreposition(SqlSessionTemplate sqlSession, Preposition pp);
	
	//직급관리 delete
	int deletePreposition(SqlSessionTemplate sqlSession, Preposition pp);
	
	//직급관리 update
	int updatePreposition(SqlSessionTemplate sqlSession, Preposition pp);
	
	//호봉관리 update
	int updatePosition(SqlSessionTemplate sqlSession, Position p);
	
	//호봉관리 insert
	int insertPosition(SqlSessionTemplate sqlSession, Position p);
	
	//ip주소 insert 
	int insertIP(IP ip, SqlSessionTemplate sqlSession);

	////ip접속관리 뷰 - ipList불러오기
	ArrayList<IP> selectIPList(SqlSessionTemplate sqlSession);

	//ip주소 delete
	int deleteIP(SqlSessionTemplate sqlSession, int ipCode);

	//ip주소 update
	int updateIP(SqlSessionTemplate sqlSession, IP ip);

	//ip주소 사용여부
	int ipUSE(SqlSessionTemplate sqlSession, String ipUse);

	//ip대역폭 사용유무
	String ipUseCheck(SqlSessionTemplate sqlSession);
	
	//부서장 search
	ArrayList<HashMap<String, Object>> searchTeamName(SqlSessionTemplate sqlSession, String text);

	ArrayList<HashMap<String, Object>> searchLeaderName(SqlSessionTemplate sqlSession, String text);

	ArrayList<HashMap<String, Object>> searchTeamCode(SqlSessionTemplate sqlSession, String text);

	int companyStay(SqlSessionTemplate sqlSession, BusinessGroup bg);

	int insertTeam(SqlSessionTemplate sqlSession, Team team);

	int updateTeam(SqlSessionTemplate sqlSession, Team team);

	int deleteJob(SqlSessionTemplate sqlSession, Job job);




	

	
	
	

	
}
