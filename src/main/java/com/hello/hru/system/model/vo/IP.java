package com.hello.hru.system.model.vo;

public class IP {
	private int ipCode;
	private String startBand;
	private String endBand;
	private String memo;
	private String removeYN;
	
	public IP() {}

	public IP(int ipCode, String startBand, String endBand, String memo, String removeYN) {
		super();
		this.ipCode = ipCode;
		this.startBand = startBand;
		this.endBand = endBand;
		this.memo = memo;
		this.removeYN = removeYN;
	}

	public int getIpCode() {
		return ipCode;
	}

	public void setIpCode(int ipCode) {
		this.ipCode = ipCode;
	}

	public String getStartBand() {
		return startBand;
	}

	public void setStartBand(String startBand) {
		this.startBand = startBand;
	}

	public String getEndBand() {
		return endBand;
	}

	public void setEndBand(String endBand) {
		this.endBand = endBand;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getRemoveYN() {
		return removeYN;
	}

	public void setRemoveYN(String removeYN) {
		this.removeYN = removeYN;
	}

	@Override
	public String toString() {
		return "IP [ipCode=" + ipCode + ", startBand=" + startBand + ", endBand=" + endBand + ", memo=" + memo
				+ ", removeYN=" + removeYN + "]";
	}
	
	
}
