package com.hello.hru.system.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.system.model.dao.StatisticalDao;

@Service
public class StatisticalServiceImpl implements StatisticalService {
	@Autowired
	private StatisticalDao std;
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	
	@Override
	public HashMap<String, Object> memberCounts() {
		return std.selectMemberCounts(sqlSession);	
	}


	@Override
	public ArrayList<HashMap<String, Object>> ppCounts() {
		ArrayList<HashMap<String, Object>> returnList = new ArrayList<>();
		
		ArrayList<String> ref = std.getPpNameList(sqlSession);
		
		int index = 0;
		for(String ppName : ref) {
			HashMap<String, Object> map = new HashMap<>();
			map.put("ppName", ppName);
			map.put("ppCount", std.selectppNameCounts(sqlSession, ppName));
			index++;
			returnList.add(map);
		}
		return returnList;
	}


	@Override
	public HashMap<String, Object> MemberEnter() {
		HashMap<String, Object> map = new HashMap<>();
		
		for(int i=1; i <= 12; i++) {
			map.put("" + i, std.selectMECount(sqlSession, i));
		}
		
		return map;
	}


	@Override
	public HashMap<String, Object> MemberOut() {
		HashMap<String, Object> map = new HashMap<>();
		
		for(int i=1; i <= 12; i++) {
			map.put("" + i, std.selectMOCount(sqlSession, i));
		}
		
		return map;
	}


	@Override
	public HashMap<String, Object> memberEnterYear() {
		HashMap<String, Object> map = new HashMap<>();
		
		for(int i=2017; i <= 2020; i++) {
			map.put("" + i, std.selectYear(sqlSession, i));
		}
		
		return map;
	}

}
