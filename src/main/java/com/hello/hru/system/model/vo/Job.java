package com.hello.hru.system.model.vo;

public class Job {
	private int jobCode;
	private String jobName;
	private String status;
	
	public Job() {}

	public Job(int jobCode, String jobName, String status) {
		super();
		this.jobCode = jobCode;
		this.jobName = jobName;
		this.status = status;
	}

	public int getJobCode() {
		return jobCode;
	}

	public void setJobCode(int jobCode) {
		this.jobCode = jobCode;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Job [jobCode=" + jobCode + ", jobName=" + jobName + ", status=" + status + "]";
	}
	
	

}
