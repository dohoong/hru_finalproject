package com.hello.hru.system.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.hello.hru.system.model.vo.BusinessGroup;
import com.hello.hru.system.model.vo.EnterCategory;
import com.hello.hru.system.model.vo.IP;
import com.hello.hru.system.model.vo.Job;
import com.hello.hru.system.model.vo.Position;
import com.hello.hru.system.model.vo.Preposition;
import com.hello.hru.system.model.vo.Team;

public interface SystemService {
	
	//회사정보select
	HashMap<String, Object> selectBusinessGroup();
	//회사정보update
	int updateBusinessGroup(BusinessGroup bg);
	
	//조직관리 select
	HashMap<String, Object> selectOrganization();
	
	//조직관리 직종 insert
	int insertJob(Job job);
	
	//조직관리 직종 update 
	int updateJob(Job job);
	
	//조직관리 입사구분 insert
	int insertEnterCategory(EnterCategory enter);
	
	//조직관리 입사구분 delete
	int deleteEnterCategory(EnterCategory enter);
	
	//조직관리 입사구분 update
	int updateEnterCategory(EnterCategory enter);
	
	//직급/호봉관리 select
	HashMap<String, Object> selectAuthorityAdmin();
	
	//직급 insert
	int insertPreposition(Preposition pp);
	
	//직급 delete
	int deletePreposition(Preposition pp);
	
	//직급 update 
	int updatePreposition(Preposition pp);
	
	//호봉 update 
	int updatePosition(Position p);
	
	//호봉 insert
	int insertPosition(Position p);
	
	//부서장 검섹
	ArrayList<HashMap<String, Object>> searchLeaderO(int category, String text);
	
	//사용여부 업데이트 
	int compayStay(BusinessGroup bg);
	
	//팀 insert
	int insertTeam(Team team);
	

	

	


	
	//ip주소 insert 
	int insertIP(IP ip);
	
	//ip접속관리 뷰 - ipList불러오기
	ArrayList<IP> selectIPList();
	
	//ip주소 delete
	int deleteIP(int ipCode);
	
	//ip주소 update
	int updateIP(IP ip);
	
	//ip주소 사용여부
	int ipUSE(String ipUse);
	
	//ip대역폭 사용유무 체크
	int ipUseCheck();
	
	int updateTeam(Team team);
	
	int deleteJob(Job job);
	
	
	
	
	
	
	
	


	







	
}
