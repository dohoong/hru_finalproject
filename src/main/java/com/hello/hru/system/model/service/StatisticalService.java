package com.hello.hru.system.model.service;

import java.util.ArrayList;
import java.util.HashMap;

public interface StatisticalService {

	HashMap<String, Object> memberCounts();

	ArrayList<HashMap<String, Object>> ppCounts();

	HashMap<String, Object> MemberEnter();

	HashMap<String, Object> MemberOut();

	HashMap<String, Object> memberEnterYear();

}
