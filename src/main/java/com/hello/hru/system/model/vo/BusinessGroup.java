package com.hello.hru.system.model.vo;

import java.sql.Date;

import org.mybatis.spring.SqlSessionTemplate;

public class BusinessGroup {
	private int groupCode;
	private String groupName;
	private String businessNumber;
	private String corporation_YN;
	private String phone;
	private String corporation_Num;
	private String faxNumber;
	private Date birthDate;
	private String bussinessType;
	private String zipCode;
	private String address;
	private String ceo;
	
	public BusinessGroup() {}

	public BusinessGroup(int groupCode, String groupName, String businessNumber, String corporation_YN, String phone,
			String corporation_Num, String faxNumber, Date birthDate, String bussinessType, String zipCode,
			String address, String ceo) {
		super();
		this.groupCode = groupCode;
		this.groupName = groupName;
		this.businessNumber = businessNumber;
		this.corporation_YN = corporation_YN;
		this.phone = phone;
		this.corporation_Num = corporation_Num;
		this.faxNumber = faxNumber;
		this.birthDate = birthDate;
		this.bussinessType = bussinessType;
		this.zipCode = zipCode;
		this.address = address;
		this.ceo = ceo;
	}

	public int getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(int groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getBusinessNumber() {
		return businessNumber;
	}

	public void setBusinessNumber(String businessNumber) {
		this.businessNumber = businessNumber;
	}

	public String getCorporation_YN() {
		return corporation_YN;
	}

	public void setCorporation_YN(String corporation_YN) {
		this.corporation_YN = corporation_YN;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCorporation_Num() {
		return corporation_Num;
	}

	public void setCorporation_Num(String corporation_Num) {
		this.corporation_Num = corporation_Num;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBussinessType() {
		return bussinessType;
	}

	public void setBussinessType(String bussinessType) {
		this.bussinessType = bussinessType;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCeo() {
		return ceo;
	}

	public void setCeo(String ceo) {
		this.ceo = ceo;
	}

	@Override
	public String toString() {
		return "BusinessGroup [groupCode=" + groupCode + ", groupName=" + groupName + ", businessNumber="
				+ businessNumber + ", corporation_YN=" + corporation_YN + ", phone=" + phone + ", corporation_Num="
				+ corporation_Num + ", faxNumber=" + faxNumber + ", birthDate=" + birthDate + ", bussinessType="
				+ bussinessType + ", zipCode=" + zipCode + ", address=" + address + ", ceo=" + ceo + "]";
	}

	
	
}
