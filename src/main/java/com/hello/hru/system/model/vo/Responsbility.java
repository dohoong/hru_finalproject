package com.hello.hru.system.model.vo;

public class Responsbility {
	private int responsCode;
	private String responsName;
	
	public Responsbility() {}

	public Responsbility(int responsCode, String responsName) {
		super();
		this.responsCode = responsCode;
		this.responsName = responsName;
	}

	public int getResponsCode() {
		return responsCode;
	}

	public void setResponsCode(int responsCode) {
		this.responsCode = responsCode;
	}

	public String getResponsName() {
		return responsName;
	}

	public void setResponsName(String responsName) {
		this.responsName = responsName;
	}

	@Override
	public String toString() {
		return "Responsbility [responsCode=" + responsCode + ", responsName=" + responsName + "]";
	}
	
	

}
