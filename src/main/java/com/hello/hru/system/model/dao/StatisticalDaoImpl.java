package com.hello.hru.system.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StatisticalDaoImpl implements StatisticalDao {

	@Override
	public HashMap<String, Object> selectMemberCounts(SqlSessionTemplate sqlSession) {
		HashMap<String, Object> map = new HashMap<>();
		
		map.put("allCount", sqlSession.selectOne("System.countAllMember"));
		map.put("joinCount", sqlSession.selectOne("System.countJoinMember"));
		map.put("leaveCount", sqlSession.selectOne("System.countLeaveMember"));
		
		return map;
	}

	@Override
	public ArrayList<String> getPpNameList(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("System.getPPName");
	}

	@Override
	public Object selectppNameCounts(SqlSessionTemplate sqlSession, String ppName) {
		return sqlSession.selectOne("System.countPPName", ppName);
	}

	@Override
	public Object selectMECount(SqlSessionTemplate sqlSession, int month) {
		return sqlSession.selectOne("System.countEMonth", month);
	}

	@Override
	public Object selectMOCount(SqlSessionTemplate sqlSession, int month) {
		return sqlSession.selectOne("System.countOMonth", month);
	}

	@Override
	public Object selectYear(SqlSessionTemplate sqlSession, int year) {
		return sqlSession.selectOne("System.countYear", year);
	}


}
