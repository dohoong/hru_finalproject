package com.hello.hru.system.model.vo;

public class MemberHrinfo {
	private int mid;
	private int teamCode;
	private int jobCode;
	private int enterCode;
	private int positionCode;
	private int responsibilityCode;
	
	public MemberHrinfo() {}

	public MemberHrinfo(int mid, int teamCode, int jobCode, int enterCode, int positionCode, int responsibilityCode) {
		super();
		this.mid = mid;
		this.teamCode = teamCode;
		this.jobCode = jobCode;
		this.enterCode = enterCode;
		this.positionCode = positionCode;
		this.responsibilityCode = responsibilityCode;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	public int getJobCode() {
		return jobCode;
	}

	public void setJobCode(int jobCode) {
		this.jobCode = jobCode;
	}

	public int getEnterCode() {
		return enterCode;
	}

	public void setEnterCode(int enterCode) {
		this.enterCode = enterCode;
	}

	public int getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(int positionCode) {
		this.positionCode = positionCode;
	}

	public int getResponsibilityCode() {
		return responsibilityCode;
	}

	public void setResponsibilityCode(int responsibilityCode) {
		this.responsibilityCode = responsibilityCode;
	}

	@Override
	public String toString() {
		return "MemberHrinfo [mid=" + mid + ", teamCode=" + teamCode + ", jobCode=" + jobCode + ", enterCode="
				+ enterCode + ", positionCode=" + positionCode + ", responsibilityCode=" + responsibilityCode + "]";
	}

	
	
	
}
