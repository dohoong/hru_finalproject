package com.hello.hru.system.model.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hello.hru.system.model.vo.BusinessGroup;
import com.hello.hru.system.model.vo.EnterCategory;
import com.hello.hru.system.model.vo.IP;
import com.hello.hru.system.model.vo.Job;
import com.hello.hru.system.model.vo.Position;
import com.hello.hru.system.model.vo.Preposition;
import com.hello.hru.system.model.vo.Team;

@Repository
public class SystemDaoImpl implements SystemDao{
	
	//회사정보select
	@Override
	public ArrayList<BusinessGroup> selectBusinessGroup(SqlSessionTemplate sqlSession) {
		ArrayList<BusinessGroup> bglist = (ArrayList)sqlSession.selectList("System.selectBusinessGroup");
		
		return bglist;
	}

	//회사정보update
	@Override
	public int updateBusinessGroup(SqlSessionTemplate sqlSession, BusinessGroup bg) {
		return sqlSession.update("System.updateBusinessGroup",bg);
	}
	
	//조직관리 부서관리select
	@Override
	public ArrayList<Team> selectTeam(SqlSessionTemplate sqlSession) {
		ArrayList<Team> otlist = (ArrayList)sqlSession.selectList("System.selectTeam");
		return otlist;
	}
	
	//조직관리 직종관리select
	@Override
	public ArrayList<Job> selectJob(SqlSessionTemplate sqlSession) {
		ArrayList<Job> ojlist = (ArrayList)sqlSession.selectList("System.selectJob");
		return ojlist;
	}
	
	//조직관리 입사구분관리select
	@Override
	public ArrayList<EnterCategory> selectEnterCategory(SqlSessionTemplate sqlSession) {
		ArrayList<EnterCategory> oelist = (ArrayList)sqlSession.selectList("System.selectEnterCategory");
		return oelist;
	}
	
	//조직관리 직종 insert
	@Override
	public int insertJob(SqlSessionTemplate sqlSession, Job job) {
		return sqlSession.insert("System.insertJob",job);
	}

	
	//조직관리 직종 update 
	@Override
	public int updateJob(SqlSessionTemplate sqlSession, Job job) {
		return sqlSession.update("System.updateJob",job);
	}
	//조직관리 입사구분 insert
	@Override
	public int insertEnterCategory(SqlSessionTemplate sqlSession, EnterCategory enter) {
		return sqlSession.insert("System.insertEnterCategory",enter);
	}
	
	//조직관리 입사구분 update
	@Override
	public int updateEnterCategory(SqlSessionTemplate sqlSession, EnterCategory enter) {
		return sqlSession.update("System.updateEnterCategory",enter);
	}
	//조직관리 입사구분 delete
	@Override
	public int deleteEnterCategory(SqlSessionTemplate sqlSession, EnterCategory enter) {

		return sqlSession.delete("System.deleteEnterCategory",enter);
	}
	
	//직급관리 직위 select
	@Override
	public ArrayList<Preposition> selectPreposition(SqlSessionTemplate sqlSession) {
		ArrayList<Preposition> applist = (ArrayList)sqlSession.selectList("System.selectPreposition");
		return applist;
	}
	
	//직급관리 호봉 select
	@Override
	public ArrayList<Position> selectPosition(SqlSessionTemplate sqlSession) {
		ArrayList<Position> aplist = (ArrayList)sqlSession.selectList("System.selectPosition");
		return aplist;
	}
	
	//직급관리 insert
	@Override
	public int insertPreposition(SqlSessionTemplate sqlSession, Preposition pp) {
		return sqlSession.insert("System.insertPreposition",pp);
	}
	
	//직급관리 delete
	@Override
	public int deletePreposition(SqlSessionTemplate sqlSession, Preposition pp) {
		
		return sqlSession.delete("System.deletePreposition",pp);
	}

	@Override
	public int updatePreposition(SqlSessionTemplate sqlSession, Preposition pp) {
		
		return sqlSession.update("System.updatePreposition",pp);
	}

	@Override
	public int updatePosition(SqlSessionTemplate sqlSession, Position p) {
		return sqlSession.update("System.updatePosition",p);
	}
	//호봉관리 insert
	@Override
	public int insertPosition(SqlSessionTemplate sqlSession, Position p) {
		
		return sqlSession.insert("System.insertPosition",p);
	}

	//ip주소 insert 
	@Override
	public int insertIP(IP ip, SqlSessionTemplate sqlSession) {
		return sqlSession.insert("System.insertIP", ip);
	}

	//ip접속관리 뷰 - ipList불러오기
	@Override
	public ArrayList<IP> selectIPList(SqlSessionTemplate sqlSession) {
		return (ArrayList) sqlSession.selectList("System.selectIPList");
	}

	//ip주소 delete
	@Override
	public int deleteIP(SqlSessionTemplate sqlSession, int ipCode) {
		return sqlSession.delete("System.deleteIP", ipCode);
	}

	//ip주소 update
	@Override
	public int updateIP(SqlSessionTemplate sqlSession, IP ip) {
		return sqlSession.update("System.updateIP", ip);
	}

	@Override
	public int ipUSE(SqlSessionTemplate sqlSession, String ipUse) {
		return sqlSession.update("System.updateIpUse", ipUse);
	}

	//ip대역폭 사용유무
	@Override
	public String ipUseCheck(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("System.ipUseCheck");
	}


	
	


	@Override
	public ArrayList<HashMap<String, Object>> searchTeamName(SqlSessionTemplate sqlSession, String text) {
		return (ArrayList)sqlSession.selectList("System.searchTeamName",text);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchLeaderName(SqlSessionTemplate sqlSession, String text) {
		return (ArrayList)sqlSession.selectList("System.searchLeaderName",text);
	}

	@Override
	public ArrayList<HashMap<String, Object>> searchTeamCode(SqlSessionTemplate sqlSession, String text) {
		
		return (ArrayList)sqlSession.selectList("System.searchTeamCode",text);
	}

	@Override
	public int companyStay(SqlSessionTemplate sqlSession, BusinessGroup bg) {
		return sqlSession.update("System.compayStay",bg);
	}

	@Override
	public int insertTeam(SqlSessionTemplate sqlSession, Team team) {
		
		return sqlSession.insert("System.insertTeam",team);
	}

	@Override
	public int updateTeam(SqlSessionTemplate sqlSession, Team team) {
		return sqlSession.update("System.updateTeam",team);
	}

	@Override
	public int deleteJob(SqlSessionTemplate sqlSession, Job job) {

		return sqlSession.delete("System.deleteJob",job);
	}






	
	







	

}
