package com.hello.hru.system.model.vo;

public class Position {
	private int positionCode;
	private int ppCode;
	private String positionName;
	private int positionLevel;
	
	public Position() {}

	public Position(int positionCode, int ppCode, String positionName, int positionLevel) {
		super();
		this.positionCode = positionCode;
		this.ppCode = ppCode;
		this.positionName = positionName;
		this.positionLevel = positionLevel;
	}

	public int getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(int positionCode) {
		this.positionCode = positionCode;
	}

	public int getPpCode() {
		return ppCode;
	}

	public void setPpCode(int ppCode) {
		this.ppCode = ppCode;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public int getPositionLevel() {
		return positionLevel;
	}

	public void setPositionLevel(int positionLevel) {
		this.positionLevel = positionLevel;
	}

	@Override
	public String toString() {
		return "Position [positionCode=" + positionCode + ", ppCode=" + ppCode + ", positionName=" + positionName
				+ ", positionLevel=" + positionLevel + "]";
	}
	
	

}
