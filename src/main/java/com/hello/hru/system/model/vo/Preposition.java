package com.hello.hru.system.model.vo;

public class Preposition {
	private int ppCode;
	private String ppName;
	private int maxSalary;
	private int minSalary;
	private String ppContent;
	
	public Preposition() {}

	

	public Preposition(int ppCode, String ppName, int maxSalary, int minSalary, String ppContent) {
		super();
		this.ppCode = ppCode;
		this.ppName = ppName;
		this.maxSalary = maxSalary;
		this.minSalary = minSalary;
		this.ppContent = ppContent;
	}



	public int getPpCode() {
		return ppCode;
	}



	public void setPpCode(int ppCode) {
		this.ppCode = ppCode;
	}



	public String getPpName() {
		return ppName;
	}



	public void setPpName(String ppName) {
		this.ppName = ppName;
	}



	public int getMaxSalary() {
		return maxSalary;
	}



	public void setMaxSalary(int maxSalary) {
		this.maxSalary = maxSalary;
	}



	public int getMinSalary() {
		return minSalary;
	}



	public void setMinSalary(int minSalary) {
		this.minSalary = minSalary;
	}



	public String getPpContent() {
		return ppContent;
	}



	public void setPpContent(String ppContent) {
		this.ppContent = ppContent;
	}



	@Override
	public String toString() {
		return "Preposition [ppCode=" + ppCode + ", ppName=" + ppName + ", maxSalary=" + maxSalary + ", minSalary="
				+ minSalary + ", ppContent=" + ppContent + "]";
	}


	
	
	

}
