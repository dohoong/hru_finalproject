package com.hello.hru.system.model.vo;

public class EnterCategory {
	private int enterCode;
	private String enterName;
	private String status;
	
	public EnterCategory() {}

	public EnterCategory(int enterCode, String enterName, String status) {
		super();
		this.enterCode = enterCode;
		this.enterName = enterName;
		this.status = status;
	}

	public int getEnterCode() {
		return enterCode;
	}

	public void setEnterCode(int enterCode) {
		this.enterCode = enterCode;
	}

	public String getEnterName() {
		return enterName;
	}

	public void setEnterName(String enterName) {
		this.enterName = enterName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "EnterCategory [enterCode=" + enterCode + ", enterName=" + enterName + ", status=" + status + "]";
	}

	
	
}
