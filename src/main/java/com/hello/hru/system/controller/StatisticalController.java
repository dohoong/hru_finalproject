package com.hello.hru.system.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.system.model.service.StatisticalService;

@Controller
public class StatisticalController {
	
	@Autowired
	private StatisticalService sts;
	@Autowired
	private AuthorityService as;
	
	@RequestMapping("showStatisticalData.st")
	public String statisticalData(HttpServletRequest request, Model m) {
		int mid = 0;
		if (request.getSession().getAttribute("loginUser") != null) {
			mid = ((Member) request.getSession().getAttribute("loginUser")).getMid();
		}

		String authority = as.checkAuthority(mid, 1);

		if (authority.equals("Y")) {
			HashMap<String, Object> memberCount = sts.memberCounts();
			HashMap<String, Object> memberEnter = sts.MemberEnter();
			HashMap<String, Object> memberOut = sts.MemberOut();
			ArrayList<HashMap<String, Object>> ppCount = sts.ppCounts();
			HashMap<String, Object> enterYear = sts.memberEnterYear();
			
			m.addAttribute("enterYear", enterYear);
			m.addAttribute("enter", memberEnter);
			m.addAttribute("out", memberOut);
			m.addAttribute("countPP", ppCount);
			m.addAttribute("countMember", memberCount);
			return "system/statisticalData";
		} else {
			return "common/errorPage";
		}
		

	}
}
