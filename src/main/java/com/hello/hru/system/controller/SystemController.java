package com.hello.hru.system.controller;



import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.attendance.model.vo.VacationBasic;
import com.hello.hru.member.model.service.AuthorityService;
import com.hello.hru.member.model.service.MemberService;
import com.hello.hru.member.model.vo.HrInfo;
import com.hello.hru.member.model.vo.Member;
import com.hello.hru.system.model.service.SystemService;
import com.hello.hru.system.model.vo.BusinessGroup;
import com.hello.hru.system.model.vo.EnterCategory;
import com.hello.hru.system.model.vo.IP;
import com.hello.hru.system.model.vo.Job;
import com.hello.hru.system.model.vo.Position;
import com.hello.hru.system.model.vo.Preposition;
import com.hello.hru.system.model.vo.Team;

@Controller
public class SystemController {
	
	@Autowired
	private SystemService ss;
	
	
	//회사정보 select
	@RequestMapping("showCompanyInfo.st")
	public String selectBusinessGroup(Model model) {
		
		
		
		//회사정보select
		HashMap<String, Object> bgselectMap = ss.selectBusinessGroup();
		
		//System.out.println("controller :"  + bgselectMap);
		
		//회사정보select
		model.addAttribute("bgselectMap" , bgselectMap);
		
		return "system/companyInfo";
	}
	
	//회사 정보 수정
	@RequestMapping("updateCompanyInfo.st")
	public String updateBusinessGroup(BusinessGroup bg) {
		
		System.out.println("bg:" + bg);
		
		int result = ss.updateBusinessGroup(bg);
		
		if(result > 0) {
			return "redirect:/showCompanyInfo.st";
		}else {
			return "common/errorPage";
		}
				
		
	}
	
	//조직관리 select
	@RequestMapping("showOrganizationManageMent.st")
	public String selectOrganization(Model model) {
		
		HashMap<String, Object> oglistMap = ss.selectOrganization();
		
		model.addAttribute("oglistMap",oglistMap);
				
		return "system/organizationAdministration";
		
	}
	//부서관리 insert
	@RequestMapping("insertTeam.st")
	public String insertTeam(Model model, Team team) {
		
		int result = ss.insertTeam(team);
		
		
		return"";
	}
	//부서관리 update
	@RequestMapping("updateTeam.st")
	public String updateTeam(Team team) {
		
		System.out.println(team);
		int result = ss.updateTeam(team);
		
		if(result>0) {
			return "redirect:/showOrganizationManageMent.st";
		}else {
			return "common/errorPage";
		}
	}
	//직종 insert
	@RequestMapping("insertJob.st")
	public String insertJob(Job job) {
		
		int result = ss.insertJob(job);
		
		if(result > 0) {
			return "redirect:/showOrganizationManageMent.st";
		}else {
			return "common/errorPage";
		}
		
	}
	
	//조직관리 직종 update
	@RequestMapping("updateJob.st")
	public String updateJob(Job job) {
		
		int result = ss.updateJob(job);
		
		if(result > 0) {
			return"redirect:/showOrganizationManageMent.st";
		}else {
			return "common/errorPage";
		}
		
		
	}
	@RequestMapping("deleteJob.st")
	public String deleteJob(Job job) {
		
		int result = ss.deleteJob(job);
		
		if(result >0) {
			return "redirect:/showOrganizationManageMent.st";
			
		}else {
			return "common/errorPage";
		}
		
	}
	//조직관리 입사구분 insert
	@RequestMapping("insertEnterCategory.st")
	public String insertEnterCategory(EnterCategory enter) {
		
		
		int result = ss.insertEnterCategory(enter);
		System.out.println(enter);
		if(result > 0) {
			return "redirect:/showOrganizationManageMent.st";
		}else {
			return "common/errorPage";
		}
		
	}
	//조직관리 입사구분 update
	@RequestMapping("updateEnterCategory")
	public String updateEnterCategory(EnterCategory enter) {
		
		int result = ss.updateEnterCategory(enter);
		
		if(result > 0) {
			return "redirect:/showOrganizationManageMent.st";
		}else {
			return "common/errorPage";
		}
	}
	
	//조직관리 입사구분 delete
	@RequestMapping("deleteEnterCategoy.st")
	public String deleteEnterCategory(EnterCategory enter) {
		
		int result = ss.deleteEnterCategory(enter);
		
		if(result > 0) {
			return "redirect:/showOrganizationManageMent.st";
		}else {
			return "common/errorPage";
		}
		
		
	}
	
	
	//직급관리/호봉관리 select
	@RequestMapping("showPositionAdministration.st")
	public String selectAuthorityAdmin(Model model) {
		
		HashMap<String, Object> salistMap = ss.selectAuthorityAdmin();
		
		model.addAttribute("salistMap",salistMap);
		
		//System.out.println(salistMap);
		
		return"system/positionAdministration";
	}
	
	
	//직급관리 insert
	@RequestMapping("insertPreposition.st")
	public String insertPreposition(Preposition pp) {
		
		int result = ss.insertPreposition(pp);
		
		System.out.println(pp);
		
		if(result > 0) {
			return"redirect:/showPositionAdministration.st";
		}else {
			return"common/errorPage";
		}
		

	}
	
	
	//직급관리 DELETE
	@RequestMapping("deletePreposition.st")
	public String deletePreposition(Preposition pp) {
		
		int result = ss.deletePreposition(pp);
		
		System.out.println(pp);
		
		if(result > 0) {
			return"redirect:/showPositionAdministration.st";
		}else {
			return "common/errorPage";
		}
	}
	
	//직급관리 update
	@RequestMapping("updatePreposition.st")
	public String updatePreposition(Preposition pp) {
		
		int result = ss.updatePreposition(pp);
		
		System.out.println(pp);
		if(result > 0) {
			return"redirect:/showPositionAdministration.st";
		}else {
			return "common/errorPage";
		}
		
	}
	
	//호봉관리 update
	@RequestMapping("updatePosition.st")
	public String updatePosition(Position p) {
		int result = ss.updatePosition(p);
		
		if(result > 0) {
			return "redirect:/showPositionAdministration.st";
		}else {
			return "common/errorPage";
		}
	}
	
	//호봉관리 insert
	@RequestMapping("insertPosition.st")
	public String insertPosition(Position p) {
		
		int result = ss.insertPosition(p);
		if(result > 0) {
			return "redirect:/showPositionAdministration.st";
		}else {
			return"common/errorPage";
		}
		
		
	}
	 
	
	
	//ip접속관리 뷰 - ipList불러오기
	@RequestMapping("showConnectionManagement.st")
	public String showConnectionManagement(Model model) {
		//ip대역폭 사용유무 
		int useYN = ss.ipUseCheck();	//사용 1, 미사용 2
		//ip 리스트
		ArrayList<IP> ipList = ss.selectIPList();
		HashMap<String, Object> hmap = new HashMap<>();
		hmap.put("ipList", ipList);
		hmap.put("useYN", useYN);
		model.addAttribute("hmap", hmap);
		System.out.println("controller ipList : " + ipList);
		return "system/connectionManagement";
	}
	
	//ip주소 insert 
	@RequestMapping("ipInsert.st")
	public ModelAndView ipInsert(ModelAndView mv, IP ip) {
		System.out.println("아이피주소 : " + ip);
		int result = ss.insertIP(ip);
		mv.addObject("result", result); 
		mv.setViewName("jsonView"); 
		return mv;
	}

	//ip주소 delete
	@RequestMapping("deleteIP.st")
	public ModelAndView deleteIP(HttpServletRequest request, ModelAndView mv) {
		int ipCode = Integer.parseInt(request.getParameter("ipCode"));
		int result = ss.deleteIP(ipCode);
		mv.addObject("result", result); 
		mv.setViewName("jsonView"); 
		return mv;
	}
	
	//ip주소 update
	@RequestMapping("updateIP.st")
	public ModelAndView updateIP(HttpServletRequest request, ModelAndView mv, IP ip) {
		System.out.println("변경할 ip : " + ip);
		int result = ss.updateIP(ip);
		mv.addObject("result", result); 
		mv.setViewName("jsonView"); 
		return mv;
	}
	
	//ip주소 사용여부
	@RequestMapping("ipUSE.st")
	public ModelAndView ipUSE(ModelAndView mv, HttpServletRequest request) {
		String ipUse = request.getParameter("ipUSE");
		System.out.println("ipUse : " + ipUse);
		int result = ss.ipUSE(ipUse);
		System.out.println("result : " + result);
		mv.addObject("result", result); 
		mv.setViewName("jsonView"); 
		return mv;
	}
	
}
