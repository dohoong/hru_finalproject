package com.hello.hru.common.model.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.hru.approval.model.dao.ApprovalDao;
import com.hello.hru.approval.model.vo.Alarm;
import com.hello.hru.common.ApprovalLine;
import com.hello.hru.humanResource.model.dao.MovementDao;
import com.hello.hru.humanResource.model.vo.Movement;
import com.hello.hru.member.model.dao.AuthorityDao;
import com.hello.hru.member.model.vo.AuthorityMember;

@Service
public class CheckServiceImpl implements CheckService{
	@Autowired
	private MovementDao md;
	@Autowired
	private AuthorityDao ad;
	@Autowired
	private ApprovalDao apd;
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	
	@Override
	public void callBack() {
		//매일 아침 11시에 실행되는 메소드입니다.
		
		
		//해당 날짜에 인사발령 내역이 있다면, 적용해주는 메소드!
		Date day = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String daystr = format.format(day);
		//String daystr = "2020-02-20";
		
		ArrayList<Movement> mlist = md.selectListOneDay(sqlSession, daystr);
		//System.out.println("movementList : " + mlist);
		
		if(mlist.size()>0) { //내역이 하나라도 있다면,
			for(int i=0; i<mlist.size(); i++) {
				if(mlist.get(i).getApprovalYn().equals("Y") && mlist.get(i).getCategory()!=1) {
					
					int result = md.updateHRInfo(sqlSession, mlist.get(i));
					if(result>0) { //업데이트에 성공했을 경우
						md.updateMovementState(sqlSession, mlist.get(i));
						Alarm alarm = null;
						String text = "";
						switch(mlist.get(i).getCategory()) {
							case 1 : text = "입사를 축하합니다!"; break;
							case 2 : text = "인사발령(직무이동)이 완료되었습니다."; break;
							case 3 : text = "인사발령(부서이동)이 완료되었습니다."; break;
							case 4 : text = "인사발령(조직책임자 임명)이 완료되었습니다."; break;
							case 5 : text = "진급이 완료되었습니다. 축하합니다!"; break;
							default : text = "인사발령이 완료되었습니다.";
						}
						alarm = new ApprovalLine().makingAlarm(text, 1, mlist.get(i).getMid());
						apd.insertAlarm(sqlSession, alarm);
					}
				}
				
				if(mlist.get(i).getCategory()!=5) { //진급일 때는 권한에 대한 변화 없음
					//권한 부여
					//1. 원래 있던 권한을 모두 삭제시키고
					int result1 = ad.updateAuthMemberDelete(sqlSession, mlist.get(i).getMid());
					
					//2. 새로운 권한을 부여한다.
					//if - 인사팀장으로 임명되었을 경우,
					int result2 = 0;
					if((mlist.get(i).getTeamCode()==2||mlist.get(i).getTeamName().contains("인사"))&&mlist.get(i).getrCode()==2) {
						ArrayList<Integer> aulist = ad.selectListAuthority2(sqlSession, 1);
						for(int j=0; j<aulist.size();j++) { //인사팀장에 대한 권한임!
							AuthorityMember authm = new AuthorityMember();
							authm.setAuthorityId(aulist.get(j));
							authm.setMid(mlist.get(i).getMid());
							result2 += ad.insertAuthorityMember2(sqlSession,authm);
						}
						
						
					} else if(mlist.get(i).getTeamCode()==2||mlist.get(i).getTeamName().contains("인사")) {
						//else if - 인사부서로 옮겼을 경우,
						ArrayList<Integer> aulist = ad.selectListAuthority2(sqlSession, 2);
						for(int j=0; j<aulist.size(); j++) { //인사부서 혹은 조직책임자에 대한 권한임!
							if(aulist.get(j)<25) {
								AuthorityMember authm = new AuthorityMember();
								authm.setAuthorityId(aulist.get(j));
								authm.setMid(mlist.get(i).getMid());
								result2 += ad.insertAuthorityMember2(sqlSession,authm);
							}
						}
						
					} else if(mlist.get(i).getrCode()==2) {
						//else if - 팀장으로 임명되었을 경우,
						ArrayList<Integer> aulist = ad.selectListAuthority2(sqlSession, 4);
						for(int j=0; j<aulist.size(); j++) { //인사부서 혹은 조직책임자에 대한 권한임!
							if(aulist.get(j)<25) {
								AuthorityMember authm = new AuthorityMember();
								authm.setAuthorityId(aulist.get(j));
								authm.setMid(mlist.get(i).getMid());
								result2 += ad.insertAuthorityMember2(sqlSession,authm);
							}
						}
						
						
					}
					
				}
			}
		}
		//인사발령 완료!
		
		
		
		
		
		
	}

}
