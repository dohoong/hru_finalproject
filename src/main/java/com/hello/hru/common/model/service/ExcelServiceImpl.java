package com.hello.hru.common.model.service;

import java.util.ArrayList;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hello.hru.approval.model.dao.ApprovalDao;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalHistory;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.model.vo.ExcelMember;
import com.hello.hru.member.model.dao.MemberDao;

@Service
public class ExcelServiceImpl implements ExcelService{
	@Autowired
	private ApprovalDao ad;
	@Autowired
	private MemberDao md;
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	//생성한 엑셀 워크북을 컨트롤러에서 받게해줄 매소드
	@Override
	public SXSSFWorkbook excelFileDownloadProcess(ArrayList<ExcelMember> mlist) {
		return this.makeSimpleFruitExcelWorkbook(mlist);
	}
	
	
	//과일 리스트를 간단한 엑셀 워크북 객체로 생성
	@Override
	public SXSSFWorkbook makeSimpleFruitExcelWorkbook(ArrayList<ExcelMember> mlist) {
		SXSSFWorkbook workbook = new SXSSFWorkbook();
		
		//시트생성
		SXSSFSheet sheet = workbook.createSheet("사원명단");
		
		//시트 열 너비 설정
		/*sheet.setColumnWidth(0, 1500); //번호
		sheet.setColumnWidth(0, 3000); //이름
		sheet.setColumnWidth(0, 5000); //
		sheet.setColumnWidth(0, 1500); */
		
		//헤더 행 생성
		Row headerRow = sheet.createRow(0);
		//해당 행의 첫번째 열 셀 생성
		Cell headerCell = headerRow.createCell(0);
		headerCell.setCellValue("번호");

		headerCell = headerRow.createCell(1);
		headerCell.setCellValue("이름");
		headerCell = headerRow.createCell(2);
		headerCell.setCellValue("이메일");
		headerCell = headerRow.createCell(3);
		headerCell.setCellValue("입사날짜");

		headerCell = headerRow.createCell(4);
		headerCell.setCellValue("내/외국인");
		headerCell = headerRow.createCell(5);
		headerCell.setCellValue("국적");
		headerCell = headerRow.createCell(6);
		headerCell.setCellValue("주민등록번호");
		headerCell = headerRow.createCell(7);
		headerCell.setCellValue("전화번호");
		headerCell = headerRow.createCell(8);
		headerCell.setCellValue("주소");

		headerCell = headerRow.createCell(9);
		headerCell.setCellValue("부서코드");
		headerCell = headerRow.createCell(10);
		headerCell.setCellValue("직종코드");
		headerCell = headerRow.createCell(11);
		headerCell.setCellValue("계약형태");
		headerCell = headerRow.createCell(12);
		headerCell.setCellValue("직급코드");
		headerCell = headerRow.createCell(13);
		headerCell.setCellValue("직책코드");
		headerCell = headerRow.createCell(14);
		headerCell.setCellValue("휴가수");

		
		headerCell = headerRow.createCell(15);
		headerCell.setCellValue("은행명");
		headerCell = headerRow.createCell(16);
		headerCell.setCellValue("계좌번호");
		headerCell = headerRow.createCell(17);
		headerCell.setCellValue("급여형태");
		headerCell = headerRow.createCell(18);
		headerCell.setCellValue("4대보험 취득신고여부");
		headerCell = headerRow.createCell(19);
		headerCell.setCellValue("급여액");
		
		//과일표 내용 행 및 셀 생성
		Row bodyRow = null;
		Cell bodyCell = null;
		for(int i=0; i<mlist.size(); i++) {
			ExcelMember m = mlist.get(i);
			int j = 0;
			//행 생성
			bodyRow = sheet.createRow(i+1);
			//데이터 번호 표시
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(i+1);
			//데이터1 : 이름
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getmName());
			//데이터2 : 이메일
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getEmail());
			//데이터3 : 입사날짜
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getEnterDate());
			
			//데이터4 : 내/외국인
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getKorean());
			//데이터5 : 국적
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getCountry());
			//데이터6 : 주민등록번호
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getSsn());
			//데이터7 : 전화번호
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getPhone());
			//데이터8 : 주소
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getAddress());
			
			//데이터9 :부서코드
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getTeamCode());
			//데이터10 : 직종코드
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getJobCode());
			//데이터11 : 계약형태
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getEnterCode());
			//데이터12 : 직급코드
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getPositionCode());
			//데이터13 : 직책코드
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getResponsibilityCode());
			//데이터14 : 휴가수
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getRemainVacation());
			
			//데이터15 : 은행명
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getBankName());
			//데이터16 : 계좌번호
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getAccountNumber());
			//데이터17 : 급여형태
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getSalaryType());
			//데이터18 : 4대보험 취득신고 여부
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getInsuranceYn());
			//데이터19 : 급여액
			bodyCell = bodyRow.createCell(j++);
			bodyCell.setCellValue(m.getSalaryAmount());
		}
		
		
		return workbook;
	}

	@Override
	public ArrayList<ExcelMember> uploadExcelFile(MultipartFile file) {
		ArrayList<ExcelMember> mlist = new ArrayList<ExcelMember>();
		try {
			OPCPackage opcPackage = OPCPackage.open(file.getInputStream());
			XSSFWorkbook workbook = new XSSFWorkbook(opcPackage);
			
			//첫번째 시트 불러오기
			XSSFSheet sheet = workbook.getSheetAt(0);
			
			for(int i=1; i<sheet.getLastRowNum()+1; i++) {
				//Fruit fruit = new Fruit();
				ExcelMember m = new ExcelMember();
				XSSFRow row = sheet.getRow(i);
				
				//행이 존재하지 않으면 패스
				if(row == null) {
					continue;
				}
				int j = 1;
				//행의 두번째 열(이름부터 받아오기)
				XSSFCell cell = row.getCell(j++);
				if(cell != null) m.setmName(cell.getStringCellValue());
				//2 : 이메일
				cell = row.getCell(j++);
				if(cell != null) m.setEmail(cell.getStringCellValue());
				//3 : 입사일
				cell = row.getCell(j++);
				if(cell != null) m.setEnterDate(cell.getStringCellValue());
				
				//4 : 내/외국인
				cell = row.getCell(j++);
				if(cell != null) m.setKorean(cell.getStringCellValue());
				//5 : 국적
				cell = row.getCell(j++);
				if(cell != null) m.setCountry(cell.getStringCellValue());
				//6 : 주민등록번호
				cell = row.getCell(j++);
				if(cell != null) m.setSsn(cell.getStringCellValue());
				//7 : 전화번호
				cell = row.getCell(j++);
				if(cell != null) m.setPhone(cell.getStringCellValue());
				//8 : 주소
				cell = row.getCell(j++);
				if(cell != null) m.setAddress(cell.getStringCellValue());
				
				//9. 부서코드
				cell = row.getCell(j++);
				if(cell != null) m.setTeamCode((int) cell.getNumericCellValue());
				//10. 직종코드
				cell = row.getCell(j++);
				if(cell != null) m.setJobCode((int) cell.getNumericCellValue());
				//11. 계약코드
				cell = row.getCell(j++);
				if(cell != null) m.setEnterCode((int) cell.getNumericCellValue());
				//12. 직급코드
				cell = row.getCell(j++);
				if(cell != null) m.setPositionCode((int) cell.getNumericCellValue());
				//13. 직책코드
				cell = row.getCell(j++);
				if(cell != null) m.setResponsibilityCode((int) cell.getNumericCellValue());
				//14. 남은 휴가
				cell = row.getCell(j++);
				if(cell != null) m.setRemainVacation((int) cell.getNumericCellValue());
				
				//15. 은행명
				cell = row.getCell(j++);
				if(cell != null) m.setBankName(cell.getStringCellValue());
				//16. 계좌번호
				cell = row.getCell(j++);
				if(cell != null) m.setAccountNumber(cell.getStringCellValue());
				//17. 급여형태
				cell = row.getCell(j++);
				if(cell != null) m.setSalaryType((int) cell.getNumericCellValue());
				//18. 4대보험 취득신고 여부
				cell = row.getCell(j++);
				if(cell != null) m.setInsuranceYn(cell.getStringCellValue());
				//19. 급여액
				cell = row.getCell(j++);
				if(cell != null) m.setSalaryAmount((int) cell.getNumericCellValue());
				
				mlist.add(m);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return mlist;
	}



	@Override
	public int insertMembers(ArrayList<ExcelMember> list, Approval app, Attachment att) {
		int result = 0;
		
		int approvalId = ad.insertApproval(sqlSession, app);
		int result12 = 0;
		String approvalYn = "N";
		if(approvalId>0) {
			if(app.getAhlist() != null) {
				for(int i=0; i<app.getAhlist().size(); i++) {
					ApprovalHistory apphis = app.getAhlist().get(i);
					apphis.setApprovalId(approvalId);
					System.out.println("Service Apphis : " +apphis);
					result12 = ad.insertApprovalHistory(sqlSession, apphis);
				}
				approvalYn = "N";
			} else {
				result12 = 1;
				approvalYn = "Y"; //결재가 필요없을 경우라면, 카테고리를 3:승인으로 바꿈!
			}
		}
		
		int result2 = 0;
		for(int i=0; i<list.size(); i++) {
			if(list.get(i).getmName()!=null) {
				list.get(i).setApprovalId(approvalId);
				list.get(i).setApprovalYn(approvalYn);
				//System.out.println(list.get(i));
				result2 += ad.insertMembers(sqlSession, list.get(i));	
			}
		}
		
		int result3 = 0;
		if(att != null) {
			att.setApprovalId(approvalId);
			result3 = md.insertAttachment(sqlSession, att);
		}
		
		if(result12>0 && result2>0 && result3>0) {
			result = 1;
		}
		
		
		return result;
	}
	
	
}













