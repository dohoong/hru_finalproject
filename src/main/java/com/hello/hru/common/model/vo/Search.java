package com.hello.hru.common.model.vo;

import java.sql.Date;

/**
 * @author Lee
 *
 */
public class Search {
	private String searchDate;		//검색하는 날짜
	private int searchTeamCode;		//검색하는 팀 코드
	private String searchTeamName;	//검색하는 팀 이름
	private String searchContent;	//검색하는 내용
	private int mid;				//검색하는 멤버 id
	private int searchDay;			//검색하는 날짜 int형
	private int searchAccountCode;
	private Date searchMonthDate;
	public Search(String searchDate, int searchTeamCode, String searchTeamName, String searchContent, int mid,
			int searchDay, int searchAccountCode, Date searchMonthDate) {
		super();
		this.searchDate = searchDate;
		this.searchTeamCode = searchTeamCode;
		this.searchTeamName = searchTeamName;
		this.searchContent = searchContent;
		this.mid = mid;
		this.searchDay = searchDay;
		this.searchAccountCode = searchAccountCode;
		this.searchMonthDate = searchMonthDate;
	}
	public Search() {
		super();
	}
	public String getSearchDate() {
		return searchDate;
	}
	public void setSearchDate(String searchDate) {
		this.searchDate = searchDate;
	}
	public int getSearchTeamCode() {
		return searchTeamCode;
	}
	public void setSearchTeamCode(int searchTeamCode) {
		this.searchTeamCode = searchTeamCode;
	}
	public String getSearchTeamName() {
		return searchTeamName;
	}
	public void setSearchTeamName(String searchTeamName) {
		this.searchTeamName = searchTeamName;
	}
	public String getSearchContent() {
		return searchContent;
	}
	public void setSearchContent(String searchContent) {
		this.searchContent = searchContent;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getSearchDay() {
		return searchDay;
	}
	public void setSearchDay(int searchDay) {
		this.searchDay = searchDay;
	}
	public int getSearchAccountCode() {
		return searchAccountCode;
	}
	public void setSearchAccountCode(int searchAccountCode) {
		this.searchAccountCode = searchAccountCode;
	}
	public Date getSearchMonthDate() {
		return searchMonthDate;
	}
	public void setSearchMonthDate(Date searchMonthDate) {
		this.searchMonthDate = searchMonthDate;
	}
	@Override
	public String toString() {
		return "Search [searchDate=" + searchDate + ", searchTeamCode=" + searchTeamCode + ", searchTeamName="
				+ searchTeamName + ", searchContent=" + searchContent + ", mid=" + mid + ", searchDay=" + searchDay
				+ ", searchAccountCode=" + searchAccountCode + ", searchMonthDate=" + searchMonthDate + "]";
	}
	
	
	
}
