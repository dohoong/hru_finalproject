package com.hello.hru.common.model.service;

import java.util.ArrayList;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.common.Attachment;
import com.hello.hru.common.model.vo.ExcelMember;

public interface ExcelService {

	SXSSFWorkbook excelFileDownloadProcess(ArrayList<ExcelMember> mlist);
	
	SXSSFWorkbook makeSimpleFruitExcelWorkbook(ArrayList<ExcelMember> mlist);

	ArrayList<ExcelMember> uploadExcelFile(MultipartFile file);

	int insertMembers(ArrayList<ExcelMember> list, Approval app, Attachment att);
}
