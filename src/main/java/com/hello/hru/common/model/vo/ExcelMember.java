package com.hello.hru.common.model.vo;

public class ExcelMember {
	private int mid;
	private String mName;
	private String email;
	private String enterDate;
	
	private String korean;
	private String country;
	private String ssn;
	private String phone;
	private String address;
	
	private int teamCode;
	private int jobCode;
	private int enterCode;
	private int positionCode;
	private int responsibilityCode;
	private int remainVacation;
	
	private String bankName;
	private String accountNumber;
	private int salaryType;
	private String insuranceYn;
	private int salaryAmount;
	
	private int approvalId;
	private String approvalYn;
	
	public ExcelMember() {}
	
	

	public ExcelMember(int mid, String mName, String email, String enterDate, String korean, String country, String ssn,
			String phone, String address, int teamCode, int jobCode, int enterCode, int positionCode,
			int responsibilityCode, int remainVacation, String bankName, String accountNumber, int salaryType,
			String insuranceYn, int salaryAmount, int approvalId, String approvalYn) {
		super();
		this.mid = mid;
		this.mName = mName;
		this.email = email;
		this.enterDate = enterDate;
		this.korean = korean;
		this.country = country;
		this.ssn = ssn;
		this.phone = phone;
		this.address = address;
		this.teamCode = teamCode;
		this.jobCode = jobCode;
		this.enterCode = enterCode;
		this.positionCode = positionCode;
		this.responsibilityCode = responsibilityCode;
		this.remainVacation = remainVacation;
		this.bankName = bankName;
		this.accountNumber = accountNumber;
		this.salaryType = salaryType;
		this.insuranceYn = insuranceYn;
		this.salaryAmount = salaryAmount;
		this.approvalId = approvalId;
		this.approvalYn = approvalYn;
	}



	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEnterDate() {
		return enterDate;
	}

	public void setEnterDate(String enterDate) {
		this.enterDate = enterDate;
	}

	public String getKorean() {
		return korean;
	}

	public void setKorean(String korean) {
		this.korean = korean;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	public int getJobCode() {
		return jobCode;
	}

	public void setJobCode(int jobCode) {
		this.jobCode = jobCode;
	}

	public int getEnterCode() {
		return enterCode;
	}

	public void setEnterCode(int enterCode) {
		this.enterCode = enterCode;
	}

	public int getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(int positionCode) {
		this.positionCode = positionCode;
	}

	public int getResponsibilityCode() {
		return responsibilityCode;
	}

	public void setResponsibilityCode(int responsibilityCode) {
		this.responsibilityCode = responsibilityCode;
	}

	public int getRemainVacation() {
		return remainVacation;
	}

	public void setRemainVacation(int remainVacation) {
		this.remainVacation = remainVacation;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(int salaryType) {
		this.salaryType = salaryType;
	}

	public String getInsuranceYn() {
		return insuranceYn;
	}

	public void setInsuranceYn(String insuranceYn) {
		this.insuranceYn = insuranceYn;
	}

	public int getSalaryAmount() {
		return salaryAmount;
	}

	public void setSalaryAmount(int salaryAmount) {
		this.salaryAmount = salaryAmount;
	}
	

	public int getApprovalId() {
		return approvalId;
	}



	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}



	public String getApprovalYn() {
		return approvalYn;
	}



	public void setApprovalYn(String approvalYn) {
		this.approvalYn = approvalYn;
	}



	@Override
	public String toString() {
		return "ExcelMember [mid=" + mid + ", mName=" + mName + ", email=" + email + ", enterDate=" + enterDate
				+ ", korean=" + korean + ", country=" + country + ", ssn=" + ssn + ", phone=" + phone + ", address="
				+ address + ", teamCode=" + teamCode + ", jobCode=" + jobCode + ", enterCode=" + enterCode
				+ ", positionCode=" + positionCode + ", responsibilityCode=" + responsibilityCode + ", remainVacation="
				+ remainVacation + ", bankName=" + bankName + ", accountNumber=" + accountNumber + ", salaryType="
				+ salaryType + ", insuranceYn=" + insuranceYn + ", salaryAmount=" + salaryAmount + ", approvalId="
				+ approvalId + ", approvalYn=" + approvalYn + "]";
	}
	
}
