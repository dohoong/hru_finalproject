package com.hello.hru.common;

public class Attachment {
	private int attachmentId;
	private String originName;
	private String changeName;
	private String path;
	private int category;
	private String removeYn;
	
	private int fileLevel;
	private int yearendId;
	private int noticeId;
	private int mid;
	private int tempMid;
	private int approvalId;
	
	public Attachment() {}
	
	

	public Attachment(int attachmentId, String originName, String changeName, String path, int category,
			String removeYn, int fileLevel, int yearendId, int noticeId, int mid, int tempMid, int approvalId) {
		super();
		this.attachmentId = attachmentId;
		this.originName = originName;
		this.changeName = changeName;
		this.path = path;
		this.category = category;
		this.removeYn = removeYn;
		this.fileLevel = fileLevel;
		this.yearendId = yearendId;
		this.noticeId = noticeId;
		this.mid = mid;
		this.tempMid = tempMid;
		this.approvalId = approvalId;
	}



	public int getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getRemoveYn() {
		return removeYn;
	}

	public void setRemoveYn(String removeYn) {
		this.removeYn = removeYn;
	}

	public int getFileLevel() {
		return fileLevel;
	}

	public void setFileLevel(int fileLevel) {
		this.fileLevel = fileLevel;
	}

	public int getYearendId() {
		return yearendId;
	}

	public void setYearendId(int yearendId) {
		this.yearendId = yearendId;
	}

	public int getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getTempMid() {
		return tempMid;
	}

	public void setTempMid(int tempMid) {
		this.tempMid = tempMid;
	}
	

	public int getApprovalId() {
		return approvalId;
	}



	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}



	@Override
	public String toString() {
		return "Attachment [attachmentId=" + attachmentId + ", originName=" + originName + ", changeName=" + changeName
				+ ", path=" + path + ", category=" + category + ", removeYn=" + removeYn + ", fileLevel=" + fileLevel
				+ ", yearendId=" + yearendId + ", noticeId=" + noticeId + ", mid=" + mid + ", tempMid=" + tempMid
				+ ", approvalId=" + approvalId + "]";
	}
	
	
}
