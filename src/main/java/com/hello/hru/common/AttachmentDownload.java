package com.hello.hru.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

public class AttachmentDownload extends AbstractView{

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		Attachment attachment = (Attachment) model.get("attachment");
		
		String ext = attachment.getOriginName().substring(attachment.getOriginName().lastIndexOf("."));
		int position = attachment.getChangeName().lastIndexOf(".");
		//System.out.println("position : " + position);
		File downFile = null;
		String fileName = null;
		

		String root = request.getSession().getServletContext().getRealPath("resources");
		String os = System.getProperty("os.name").toLowerCase();
		//System.out.println(os);
		String sl = "";
		if(os.equals("mac os x")) {
			sl = "/";
		} else {
			sl = "\\";
		}
		String filePath = root + sl + "uploadFiles";
		attachment.setPath(filePath);
		
		if(position == -1) {
			downFile = new File(attachment.getPath() + "/" + attachment.getChangeName() + ext);
			fileName = attachment.getChangeName() + ext;
		} else {
			downFile = new File(attachment.getPath() + "/" + attachment.getChangeName());
			fileName = attachment.getChangeName();
		}
		
		System.out.println(downFile);
		//response.setContentType(getContentType());
		response.setContentType("application/download;charset=utf-8");
		response.setContentLength((int) downFile.length());
		
		//각 브라우저에 따른 파일이름 인코딩 작업
		String browser = request.getHeader("User-Agent");
		if(browser.indexOf("MSIE")>-1) {
			fileName = URLEncoder.encode(fileName, "UTF-8").replace("\\", "%20");
		} else if(browser.indexOf("Trident")>-1) {
			fileName = URLEncoder.encode(fileName, "UTF-8").replace("\\", "%20");
		} else if(browser.indexOf("Firefox")>-1) {
			fileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
		} else if(browser.indexOf("Opera")>-1) {
			fileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
		} else if(browser.indexOf("Chrome")>-1) {
			StringBuffer sb = new StringBuffer();
			for(int i=0; i<fileName.length(); i++) {
				char c = fileName.charAt(i);
				if(c>'~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			fileName = sb.toString();
		} else if(browser.indexOf("Safari")>-1) {
			fileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
		} else {
			fileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
		}
		
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		OutputStream out = response.getOutputStream();
		FileInputStream fis = null;
		
		try {
			fis = new FileInputStream(downFile);
		
			FileCopyUtils.copy(fis, out);
		} catch(Exception e) {
			if(fis != null) {
				try {
					fis.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
			
			out.flush();
		}
		
	}

}
