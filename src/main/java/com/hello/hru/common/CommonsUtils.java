package com.hello.hru.common;

import java.util.UUID;

public class CommonsUtils {
	public static String getRandomString() {
		return UUID.randomUUID().toString().replace("/","").replace("\\","").replace(".","").replace("-", "");
	}
}
