package com.hello.hru.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TemporaryController {

	@GetMapping("showExample.temp")
	public String showExample() {
		return "common/example";
	}

	//메인메뉴
	@GetMapping("showSystemView.temp")
	public String showSystemMain() {
		return "main/systemMenu";
	}
	@GetMapping("showHRView.temp")
	public String showHRMain() {
		return "main/hrMenu";
	}
	@GetMapping("showAccountView.temp")
	public String showAccountMain() {
		return "main/accountMenu";
	}
	@GetMapping("showLeaderView.temp")
	public String showLeaderMain() {
		return "main/leaderMenu";
	}


	//시스템 서브메뉴
//	 @GetMapping("showCompanyInfo.temp") 
//	 public String showCompanyInfo() { 
//		 return"system/companyInfo"; 
//	}

	@GetMapping("showOrganizationAdministration.temp")
	public String showOrganAdmin() {
		return "system/organizationAdministration";
	}
	@GetMapping("showAuthorityAdministration.temp")
	public String showAuthorityAdmin() {
		return "system/authorityAdministration";
	}
	@GetMapping("showStatisticalData.temp")
	public String showStatics() {
		return "system/statisticalData";
	}

	//시스템 섭써브메뉴(상단메뉴)
	@GetMapping("showConnectionManagement.temp")
	public String showConnection() {
		return "system/connectionManagement";
	}
	@GetMapping("showPositionAdministration.temp")
	public String showPosition() {
		return "system/positionAdministration";
	}
	@GetMapping("showVacationAdministration.temp")
	public String showVacation() {
		return "system/vacationAdministration";
	}
	@GetMapping("showCalendarAdministration.temp")
	public String showCalendarAdmin() {
		return "system/calendarAdministration";
	}
	@GetMapping("showApprovalLine.temp")
	public String showApprovalLine() {
		return "system/approvalLine";
	} 
	/*
	 * @GetMapping("showSalaryAttitudeAdministration.temp") public String
	 * showSalaryAdmin() { return "system/salaryAttitudeAdministration"; }
	 */

	//인사 서브메뉴
	@GetMapping("showMemberInsert.temp")
	public String showMemberInsert() {
		return "humanResource/memberInsert";
	}
	@GetMapping("showMemberList.temp")
	public String showMemberList() {
		return "humanResource/memberList";
	}
	@GetMapping("showTeamList.temp")
	public String showTeamList() {
		return "humanResource/teamList";
	}


	//인사 썹써브메뉴(상단메뉴)


	@GetMapping("showWageBasicInformation.temp")
	public String showWageBasicInformation() {
		return "salary/wagebasicinformation";
	}
	@GetMapping("showinsertWage.temp")
	public String showinsertWage() {
		return "salary/wageinsert";
	}

	@GetMapping("showEvaluateMember.temp")
	public String showEvaluateMember() {
		return "humanResource/evaluateMember";
	}



	//근태/급여 서브메뉴
	@GetMapping("showOneDayAttend.temp")
	public String showOneDay() {
		return "account/oneDayAttend";
	}
	@GetMapping("showWageBasicImformation.temp")
	public String showWageBasicImformation() {
		return "salary/wagebasicinformation";
	}


	@GetMapping("showMonthlyWageStatus.temp")
	public String showMonthlyWageStatus() {
		return "salary/monthlyWageStatus";
	}
	@GetMapping("showWageStatusAndEmail.temp")
	public String showWageStatusAndEmail() {
		return "salary/wageStatusAndEmail";
	}
	@GetMapping("showQuitMoneyResult.temp")
	public String showQuitMoneyResult() {
		return "salary/quitMoneyResult";
	}


	//근태/급여 썹써브메뉴(상단메뉴)
	@GetMapping("showAttendanceManage.temp")
	public String showAttendanceMgt() {
		return "account/attendanceManage";
	}
	@GetMapping("showAttendSearch.temp")
	public String showAttendSearch() {
		return "account/attendSearch";
	}
	@GetMapping("showMonthAttend.temp")
	public String showMonthAttend() {
		return "account/monthAttend";
	}
	@GetMapping("showRetiringMember.temp")
	public String showRetiringMember() {
		return "salary/retiringMember";
	}

	//조직책임자 서브메뉴
	@GetMapping("showOrganizationManagement.temp")
	public String showOrganMgt() {
		return "leader/organizationManagement";
	}
//	@GetMapping("showAuthorizationWaiting.temp")
//	public String showAuthoWait() {
//		return "leader/authorizationWaiting";
//	}

	//조직책임자 썹써브(상단메뉴)
	@GetMapping("showAuthorizationList.temp")
	public String showAuthoList() {
		return "leader/authorizationList";
	}

	//사원 서브메뉴
	@GetMapping("showInfoAttendance.temp")
	public String showInfoAtt() {
		return "member/infoAttendance";
	}
	@GetMapping("showInfoMember.temp")
	public String showInfoMember() {
		return "member/infoMember";
	}
	@GetMapping("showInfoSalary.temp")
	public String showInfoSalary() {
		return "member/infoSalary";
	}
	//복리 후생 메인으로
	@GetMapping("showSocializingMain.temp")
	public String showSocializingMain() {
		return "member/socializingBoard/socializingMain";
	}
	//사원 캘린더 메인으로
	@GetMapping("showEmpCalendarMain.temp")
	public String showEmpCalendarMain() {
		return "member/employeeCalendar/empCalendarMain";
	}
	//건의 사항 게시판 메인으로
	/*
	 * @GetMapping("showProposalBoardMain.temp") public String
	 * showProposalBoardMain() { return "member/proposalBoard/pbList"; }
	 */

	//사원 썹써브(상단메뉴)
	@GetMapping("showInfoCareer.temp")
	public String showInfoCareer() {
		return "member/infoCareer";
	}
	@GetMapping("showCertificateManagement.temp")
	public String showCertificateManagement() {
		return "member/certificateManagement";
	}
	@GetMapping("showInfoVacation.temp")
	public String showInfoVacation() {
		return "member/infoVacation";
	}
	@GetMapping("showInfoPersonalHR.temp")
	public String showInfoPersonalHR() {
		return "member/infoPersonalHR";
	}
	@GetMapping("showInfoQuitMoney.temp")
	public String showInfoQuitMoney() {
		return "member/infoQuitMoney";
	}

	//복리후생 상조금으로
	@GetMapping("showHighMoneyBoardMain.temp")
	public String showHighMoneyBoard() {
		return "member/highMoneyBoard/highMoneyBoardMain";
	}
	//친목 도모 게시판
	/*
	 * @GetMapping("showSocializingList.temp") public String showSocializingList() {
	 * return "member/socializingBoard/socializingList"; }
	 */
	/*
	 * @GetMapping("showSocializingListInsert.temp") public String
	 * showSocializingListInsert() { return
	 * "member/socializingBoard/socializingListInsert"; }
	 */
	/*
	 * @GetMapping("showSocializingListforWriterVolunteer.temp") public String
	 * showSocializingListforWriterVolunteer() { return
	 * "member/socializingBoard/socializingListforWriterVolunteer"; }
	 */

	/*
	 * @GetMapping("showSocializingListApplyVolunteer.temp") public String
	 * showSocializingListApplyVolunteer() { return
	 * "member/socializingBoard/socializingListApplyVolunteer"; }
	 */
	/*
	 * @GetMapping("showSocializingListforWriterVolunteer.temp") public String
	 * showSocializingListforWriterVolunteer() { return
	 * "member/socializingBoar/socializingListforWriterVolunteer"; }
	 */
	//건의 사항 게시판
//	@GetMapping("showInsertpbList.temp")
//	public String showInsertpbList() {
//		return "member/proposalBoard/insertpbList";
//	}
	@GetMapping("showDetailpbListwithComment.temp")
	public String showDetailpbListwithComment() {
		return "member/proposalBoard/detailpbListwithComment";
	}
	@GetMapping("showDetailpbList.temp")
	public String showDetailpbList() {
		return "member/proposalBoard/detailpbList";
	}
	//사원 캘린더
	@GetMapping("showEmpCalendarDetail.temp")
	public String showEmpCalendarDetail() {
		return "member/employeeCalendar/empCalendarDetail";
	}
	
	@GetMapping("showYearEndCalculator.temp")
	public String showYearEndCalculator(){
		return "member/yearEndCalculator";
	}
	
	@GetMapping("showTestCalendar.temp")
	public String showCalendarTest() {
		return "test/calenddarTest";
	}
	@GetMapping("showOrganizationChart.temp")
	public String showOrganizationChart() {
		return "leader/organizationChart";
	}
	@GetMapping("showCountUpTest.temp")
	public String showCountUpTest() {
		return "test/countUpTest";
	}
}





