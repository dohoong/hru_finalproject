package com.hello.hru.common;

import java.util.ArrayList;
import java.util.Date;

import com.hello.hru.approval.model.vo.Alarm;
import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalHistory;

public class ApprovalLine {
	public ApprovalLine() {};
	
	public Approval makingApprovalLine(int code, int mid, String title, int firstApproval, int secondApproval, int thirdApproval) {
		
		
		Approval app = new Approval();
		app.setMid(mid); //기안자 사번
		app.setApprovalTitle(title); //제목
		app.setApprovalCode(code); //결재코드(사원등록)
		
		if(firstApproval != 0) { //첫번째 결재자가 있을 때,
			app.setAhlist(new ArrayList<ApprovalHistory>());
			
			ApprovalHistory apphis1 = new ApprovalHistory();
			apphis1.setResult(1);
			apphis1.setMid(firstApproval);

			if(secondApproval == 0) { //2번째 결재자가 없을 때,
				apphis1.setFinalYn("Y");
				app.getAhlist().add(apphis1);
			} else {				  //2번째 결재자가 있을 때,
				apphis1.setFinalYn("N");
				app.getAhlist().add(apphis1);
				
				ApprovalHistory apphis2 = new ApprovalHistory();
				apphis2.setResult(2);
				apphis2.setMid(secondApproval);
				
				if(thirdApproval == 0) { //3번째 결재가가 없을 때,
					apphis2.setFinalYn("Y");
					app.getAhlist().add(apphis2);

				} else { 				 //3번째 결재자가 있을 때,
					apphis2.setFinalYn("N");
					app.getAhlist().add(apphis2);
					
					ApprovalHistory apphis3 = new ApprovalHistory();
					apphis3.setResult(2);
					apphis3.setMid(thirdApproval);
					apphis3.setFinalYn("Y");
					app.getAhlist().add(apphis3);
				}
			}
			
			app.setStatus(1);
			
		} else {
			app.setStatus(3);
		}
		
		
		
		return app;
	}
	
	public Alarm makingAlarm(String content, int sendMid, int takeMid) {
		Alarm alarm = new Alarm();
		alarm.setContent(content);
		alarm.setSendMid(sendMid);
		alarm.setTakeMid(takeMid);
		alarm.setReadYn("N");
		alarm.setAlarmDate(new Date().toString());
		
		return alarm;
	}
	
}
