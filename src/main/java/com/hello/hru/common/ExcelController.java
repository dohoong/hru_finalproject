package com.hello.hru.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.hello.hru.approval.model.vo.Approval;
import com.hello.hru.approval.model.vo.ApprovalBasic;
import com.hello.hru.common.model.service.ExcelService;
import com.hello.hru.common.model.vo.ExcelMember;
import com.hello.hru.member.model.vo.Member;

@Controller
public class ExcelController {
	@Autowired
	private ExcelService es;
	
	@RequestMapping("showExcelTest.app")
	public String showExcelTest() {
		return "test/excelTest";
	}
	
	
	
	@RequestMapping(value="/downloadExcelFile.app", method=RequestMethod.POST)
	public String downloadExcelFile(Model model) {
		//샘플 데이터
		ExcelMember m = new ExcelMember(0, "홍길동", "honghong@hru.com", "2019-07-22", "Y", "대한민국", "123456-1234567", "010-1234-5678", "서울시 강남구",
										2, 1, 1, 1, 1, 15, "신한은행", "110-123-123456", 1, "Y", 30000000, 0, null);
		ArrayList<ExcelMember> mlist = new ArrayList<>();
		mlist.add(m);
		SXSSFWorkbook workbook = es.excelFileDownloadProcess(mlist);
		
		
		
		model.addAttribute("locale", Locale.KOREA);
		model.addAttribute("workbook", workbook);
		model.addAttribute("workbookName", "사원명단");
		return "excelDownloadView";
	}
	
	@RequestMapping(value="/uploadExcelFile.app", method=RequestMethod.POST)
	public ModelAndView uploadExcelFile(MultipartHttpServletRequest request, ModelAndView mv, ApprovalBasic ab) {
		MultipartFile file = null;
		Iterator<String> iterator = request.getFileNames();
		
		if(iterator.hasNext()) {
			file = request.getFile(iterator.next());
			//System.out.println("file : " + file);
		}
		
		//엑셀데이터를 멤버리스트로 바꿔주는 메소드
		ArrayList<ExcelMember> list = es.uploadExcelFile(file);
		
		
		//전자결재를 위한 정보
		int mid = ((Member) (request.getSession().getAttribute("loginUser"))).getMid();
		String title = request.getParameter("title");
		
		//결재라인 만들어주는 메소드
		Approval app = new ApprovalLine().makingApprovalLine(13, mid, title, ab.getFirstApproval(), ab.getSecondApproval(), ab.getThirdApproval());
		//System.out.println("app : " + app);
		//System.out.println("list" + list);
		
		//파일에 대한 처리 (결재)
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String os = System.getProperty("os.name").toLowerCase();
		System.out.println(os);
		String sl = "";
		if(os.equals("mac os x")) {
			sl = "/";
		} else {
			sl = "\\";
		}
		
		Attachment att = null;
		String filePath = root + sl + "uploadFiles";
		try {
			if(!file.getOriginalFilename().equals("")) { //전자결재 첨부파일!
				String originFileName = file.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();
				
				att = new Attachment();
				att.setOriginName(originFileName);
				att.setChangeName(changeName);
				att.setPath(filePath);
				att.setCategory(6);
				att.setFileLevel(0);
				
				file.transferTo(new File(filePath + sl + changeName + ext));
			}

			int result = es.insertMembers(list, app, att);
			
			
			if(result>0) {
				mv.addObject("msg", 1);
			} else {
				mv.addObject("msg", 2);				
			}
		
		} catch(IllegalStateException | IOException e) {
			//new File(filePath + sl + changeName + ext).delete();
			
			mv.addObject("msg", 3);
			
		}

		//mv.addObject("list", list);
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	
}
