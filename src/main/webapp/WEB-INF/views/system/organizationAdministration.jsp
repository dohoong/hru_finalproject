<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hru</title>
<style>
	#addO {
		padding-bottom: 8px;
	
	}
	
</style>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->

</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<div>
		<form>
			<table>
				<tr>
					<td colspan="1"><h1 style="color:black">조직관리</h1></td>	
				</tr>
			</table>
			</form>
		</div>
		<hr>
		<div class="hru segment">
				<h3><i class="check square icon"></i>부서관리</h3>
				<div class="scrollArea" style="width:50%;">
					<table class="hru scroll table" >
						<thead>
							<tr>
								<th>사업장</th>
								<th>부서코드</th>
								<th>부서명</th>
								<th>부서장</th>
								<th>사용여부</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="c" items="${ oglistMap.otlist }">
								<tr class="scroll-trOne">
									<td>${c.groupName }</td>
									<td>${c.teamCode}</td>
									<td>${c.teamName}</td>
									<td>${c.mName}</td>
									<td>${c.status}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

				<div class="editArea" style="width:40%;">
					<form  method="post" id="OTeamForm">
					<table class="hru table" border="1">
						<thead>
							<tr class="table-title">
								<th colspan="3" class="table-title">수 정</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>사업장</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="Ogroup"  name="groupName"></div></td>
							</tr>
							<tr>
								<th>부서코드</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="Ocode"   name="teamCode"></div></td>
							</tr>
							<tr>
								<th>부서명</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="Oname" name="teamName"></div>
									<input type="hidden" name="teamCode" id="teamCode">
									</td>
				
							</tr>
							<tr>
								<th>부서장</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="Oleader" name="mName" ></div>
									</td>
							</tr>
							<tr>
								<th>사용여부</th>
								<td><div class="ui radio checkbox"><input type="radio" id="Ouse" name="yn"><label for="use"> 사용</label></div></td>
								<td><div class="ui radio checkbox"><input type="radio" id="Onouse" name="yn"><label for="nouse"> 비사용</label></div></td>
							</tr>

						</tbody>
					</table>
					</form>
				<div class="buttonArea">
						<button class="ui blue button mini" id="add1" type="button">추가</button>
						<button class="ui blue button mini" id="complete1" style="display:none;" type="submit" >저장</button>
						<button class="ui blue button mini" id="save1" type="button" onclick="updateTeam()">저장</button>
					</div>
					
					</div>
					</div>
					
					<br><br>
			<div class="hru segment">
				<h3><i class="check square icon"></i>직종관리</h3>
				<div class="scrollArea" style="width:50%;">
					<table class="hru scroll table">
						<thead>
							<tr>
								<th>직종코드</th>
								<th colspan="3">직종명</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="c" items="${oglistMap.ojlist}">
								<tr class="scroll-trTwo">
									<td>${c.jobCode}</td>
									<td colspan="3">${c.jobName}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			
				<div class="editArea" style="width:40%;">
				<form method="post" id="OJobForm">
					<table class="hru table">
						<thead>
							<tr  class="table-title">
								<th colspan="3" class="table-title">수 정</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>직종코드</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="code2" name="jobCode" readonly></div></td>
							</tr>
							<tr>
								<th>직종명</th>
								<td colspan="4"><div class="ui mini input">
								<input type="text" id="Oname2" name="jobName">
								<input type="hidden" id="enter" name="jobCode" readonly></div>
								</td>
							</tr>
						</tbody>
					</table>
				<div class="buttonArea">
						<button class="ui blue button mini" id="add2" type="button">추가</button>
						<button class="ui blue button mini" id="complete2" style="display:none;" type="submit" onclick="insertJob()">저장</button>
						<button class="ui blue button mini" id="save2" type="button" onclick="updateJob()">저장</button>
						<button class="ui gray button mini" id="delete2" type="button" onclick="deleteJob()">삭제</button>
					</div>
					</form>
					</div>
					</div>
				<br><br>
			<div class="hru segment">
				<h3><i class="check square icon"></i>입사구분</h3>
				<div class="scrollArea" style="width:50%;">
					<table class="hru scroll table">
						<thead>
							<tr>
								<th>입사구분코드</th>
								<th colspan="4">입사구분코드명</th>
							</tr>
						</thead>
						<tbody>	
						<c:forEach var="c" items="${ oglistMap.oelist }">
								<tr class="scroll-trThree">
									<td>${ c.enterCode }</td>
									<td colspan="4">${ c.enterName }</td>
								</tr>
						</c:forEach>	
						</tbody>
					</table>
				</div>

				<div class="editArea" style="width:40%;">
					<form method="post" id="enterCategoryForm">
					<table class="hru table">
						<thead>
							<tr class="table-title">
								<th colspan="3" class="table-title">수 정</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>입사구분코드</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text"  id="code3" name="enterCode"readonly>
									</div>
									</td>
							</tr>
							<tr>
								<th>입사구분명</th>
								<td colspan="4"><div class="ui mini input">
								<input type="text" id="name3" name="enterName">
								</div>
								</td>
								
							</tr>
						</tbody>
					</table>
					</form>
					<div class="buttonArea">
						<button class="ui blue button mini" id="add3" type="button">추가</button>
						<button class="ui blue button mini" id="complete3" style="display:none;" type="submit" onclick="insertEnter()">저장</button>
						<button class="ui blue button mini" id="save3" type="button" onclick="updateEnter()">저장</button>
						<button class="ui gray button mini" id="delete3"type="button" onclick="deleteOP()">삭제</button>
					</div>	
				
			</div>
		</div>

		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	
	
	<script>
	$(function(){
		$("#system2").show(); 
		$(".ui.selection.dropdown").dropdown();
	});
	//행클릭
	$(".scroll-trOne").click(function(){
		$("#Ogroup").val($(this).children().eq(0).text()).attr("readonly",true);
		$("#Ocode").val($(this).children().eq(1).text()).attr("readonly",true);
		$("#Oname").val($(this).children().eq(2).text());
		$("#Oleader").val($(this).children().eq(3).text());
		$("#teamCode").val($(this).find("input[type='hidden']").val());
		if($(this).children().eq(4).text()== "Y"){
			$("#Ouse").prop("checked",true);
			
		}else{
			$("#Onouse").prop("checked",true);
		}
		$("#save1").show(); 
		$("#complete1").hide(); 
		
	});
	

	
	//행클릭
	$(".scroll-trTwo").click(function(){
		$("#code2").val($(this).children().eq(0).text()).attr("readonly",true);
		$("#Oname2").val($(this).children().eq(1).text()).attr("readonly",false);
		$("#add").show(); 
		$("#delete2").show();
		$("#save2").show(); 
		$("#complete2").hide(); 
		$("#code2").attr("disabled", false);
	});
	
	//행클릭
	$(".scroll-trThree").click(function(){
			$("#code3").val($(this).children().eq(0).text()).attr("readonly",true);
			$("#name3").val($(this).children().eq(1).text()).attr("readonly",false);
			$("#add3").show();  
			$("#delete3").show(); 
			$("#save3").show(); 
			$("#complete3").hide();
			$("#code3").attr("disabled", false);
		});
	
	//추가버튼 클릭시
	$("#add1").click(function(){
		$("#Ocode").val("");
		$("#Oname").val("");
		$("#Oleader").val("");
		$("#add1").hide();
		$("#search").hide();
		$("#save1").show();
		$("#delete1").hide();
		$("#Ouse").show();
		$("#Onuse").show();
	});
	
	
	
	
	//추가버튼 클릭시2
	$("#add2").click(function(){
		$("#code2").val("").attr("readonly", false);
		$("#name2").val("").attr("readonly",false);
		$("#add2").hide();
		$("#complete2").show();
		$("#save2").hide();
		$("#delete2").hide();
	});
	
	//추가 버튼 클릭시3
	$("#add3").click(function(){
		$("#code3").val("").attr("readonly", false);
		$("#name3").val("").attr("readonly",false);
		$("#add3").hide();
		$("#complete3").show();
		$("#save3").hide();
		$("#delete3").hide();
	});
	
	function updateTeam(){
		var action = document.forms.OTeamForm;
		$("#OTeamForm").attr("action","updateTeam.st");
		$("#OTeamForm").submit();
	}
	
	
	//조직 추가
	function insertJob(){
		var action = document.forms.OJobForm;
		$("#OJobForm").attr("action","insertJob.st");
		$("#OJobForm").submit();
	};
	
	
	//조직 수정 
	function updateJob(){
		var action = document.forms.OJobForm;
		$("#OJobForm").attr("action","updateJob.st");
		$("#OJobForm").submit();
	};
	//조직 삭제
	function deleteJob(){
		var action = document.forms.OJobForm;
		$("#OJobForm").attr("action","deleteJob.st");
		$("#OJobForm").submit();
		
	};
	//입사구분 추가
	function insertEnter(){
		var action = document.forms.enterCategoryForm;
		$("#enterCategoryForm").attr("action","insertEnterCategory.st");
		$("#enterCategoryForm").submit();
		
	};
	
	//입사구분 수정
	function updateEnter(){
		var action = document.forms.enterCategoryForm;
		$("#enterCategoryForm").attr("action","updateEnterCategory.st");
		$("#enterCategoryForm").submit();
	};
	
		
	function deleteOP(){
		var action = document.forms.enterCategoryForm;
		$("#enterCategoryForm").attr("action","deleteEnterCategoy.st");
		$("#enterCategoryForm").submit();
		
	}
	
	
	
	
	

	
	</script>
	

</body>
</html>