<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hru</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	 #categoryStyle{
		text-align:left;
		padding-left:25px;
		background:lightgray;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<h1>권한관리</h1>
		<div class="ui divider"></div>
		<div class="hru segment">
			<form action="update.au" method="post">
				<table class="hru board2" border="1" id="authTable">
					<thead>
						<tr>
							<th width="100px;">권한코드</th>
							<th>메뉴명</th>
							<th width="150px">인사부서장</th>
							<th width="150px">인사부서</th>
							<th width="150px">급여관리</th>
							<th width="150px">조직책임자</th>
							<th width="150px">전체</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="i" begin="0" end="${alist.size()/5-1}">
						<c:if test="${alist[i*5].menuCode==1}"><tr><th colspan="7" id="categoryStyle">메인메뉴</th></tr></c:if>
						<tr>
							<td><c:out value="${alist[i*5].authorityId}"/></td>
							<td><c:out value="${alist[i*5].menuName}"/></td>
							<c:forEach var="j" begin="${i*5}" end="${i*5+4}">
								<td><input type="hidden" value="${alist[j].authorityId}" name="list[${j}].authorityId">
									<input type="hidden" value="${alist[j].menuName}" name="list[${j}].menuName">
									<input type="hidden" value="${alist[j].menuCode}" name="list[${j}].menuCode">
									<input type="text" value="${alist[j].category}" name="list[${j}].category" style="display:none;">
								<div class="ui fitted toggle checkbox">
									<c:if test="${alist[j].useYn.equals('Y')}">
										<input class="authcheck" type="checkbox" name="list[${j}].useYn" checked><label></label>
									</c:if>
									<c:if test="${alist[j].useYn.equals('N')}">
										<input class="authcheck" type="checkbox" name="list[${j}].useYn"><label></label>
									</c:if>
								</div>
								</td>
							</c:forEach>
						</tr>
						<c:if test="${alist[i*5].menuCode==5}"><tr><th colspan="7" id="categoryStyle">서브메뉴</th></tr></c:if>
						<c:if test="${alist[i*5].menuCode==13}"><tr><th colspan="7" id="categoryStyle">상단메뉴 (3차메뉴)</th></tr></c:if>
						</c:forEach>
					</tbody>
				</table>
				<div class="buttonArea">
						<button class="ui blue button mini" id="store">저 장</button>
						<button class="ui gray button mini" id="cancel" type="reset">기본값</button>
				</div>
			</form>
		</div><!-- 테이블영역 끝! -->
		<div class="hru segment">
			<div class="ui message">
					<ul class="list">
						<li>CEO는 모든 메뉴에 접근할 수 있는 권한을 가지고, 부여할 수 있습니다.</li>
						<li>인사팀장에 임명된 사람이 인사담당자/회계담당자를 지정할 수 있으며, 각 담당자는 담당하는 부서에 대한 권한만 부여받습니다.</li>
						<li>조직책임자 또한 책임지고 있는 부서에 대한 권한만 부여받게됩니다. (인사팀장의 경우, 시스템관리를 제외한 모든 권한을 기본값으로 부여받습니다.)</li>
						<li>전체 사원이 이용할 수 있는 '사원 메뉴'에 대한 권한은 변경할 수 없습니다.</li>
					</ul>
				</div>
		</div>
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function(){
			$("#authTable").find("tr").eq(6).find("input[type=checkbox]").prop("disabled", true);
			for(var i=8;i<=15;i++){
				$("#authTable").find("tr").eq(i).find("td:eq(4)").find("input[type=checkbox]").prop("checked",false).prop("disabled", true);
				$("#authTable").find("tr").eq(i).find("td:eq(5)").find("input[type=checkbox]").prop("checked",false).prop("disabled", true);						
				$("#authTable").find("tr").eq(i).find("td:eq(6)").find("input[type=checkbox]").prop("checked",false).prop("disabled", true);						
			}
			
			$(".authcheck").change(function(){
				var authCate = $(this).parent().parent().find("input[type=text]").val();
				//console.log(authCate);
				
				if(authCate == 5){ //사원(전체)를 눌렀을 때,
					if($(this).is(":checked")){
						$(this).parent().parent().parent().find("input[type=checkbox]").prop("checked",true);
					} 
					
				} else if(authCate == 4){//조직책임자를 눌렀을 때,
					if($(this).is(":checked")){
						$(this).parent().parent().parent().children().eq(2).find("input[type=checkbox]").prop("checked",true);
					} else{
						$(this).parent().parent().parent().children().eq(6).find("input[type=checkbox]").prop("checked",false);
					}
				} else if(authCate == 2){ //인사팀 눌렀을 때,
					if($(this).is(":checked")){
						$(this).parent().parent().parent().children().eq(2).find("input[type=checkbox]").prop("checked",true);
					} else{
						$(this).parent().parent().parent().children().eq(6).find("input[type=checkbox]").prop("checked",false);
					}
				} else if(authCate == 1){ //인사팀장 눌렀을 때,
					if(!$(this).is(":checked")){
						$(this).parent().parent().parent().children().eq(3).find("input[type=checkbox]").prop("checked",false);
						$(this).parent().parent().parent().children().eq(5).find("input[type=checkbox]").prop("checked",false);
						$(this).parent().parent().parent().children().eq(6).find("input[type=checkbox]").prop("checked",false);
					}
				}
				var authorityId = $(this).parent().parent().find("input[type=hidden]").eq(0).val();
				//console.log(authorityId);
				
				if(authorityId == 6){
					console.log("인사관리가 체크됨!");
					if(!$(this).is(":checked")){
						for(var i=8;i<=15;i++){
							$("#authTable").find("tr").eq(i).find("td:eq(2)").find("input[type=checkbox]").prop("checked",false).prop("disabled", true);
							$("#authTable").find("tr").eq(i).find("td:eq(3)").find("input[type=checkbox]").prop("checked",false).prop("disabled", true);						
						}
					} else{
						for(var i=8;i<=15;i++){
							$("#authTable").find("tr").eq(i).find("td:eq(2)").find("input[type=checkbox]").prop("disabled", false);				
						}
					}
				} else if(authorityId == 7){
					if(!$(this).is(":checked")){
						for(var i=8;i<=15;i++){
							$("#authTable").find("tr").eq(i).find("td:eq(3)").find("input[type=checkbox]").prop("checked",false).prop("disabled", true);							
						}
					} else{
						for(var i=8;i<=15;i++){
							$("#authTable").find("tr").eq(i).find("td:eq(2)").find("input[type=checkbox]").prop("disabled", false);				
							$("#authTable").find("tr").eq(i).find("td:eq(3)").find("input[type=checkbox]").prop("disabled", false);				
						}
						
					}
				}
				
			});
		});
		
	</script>
</body>
</html>