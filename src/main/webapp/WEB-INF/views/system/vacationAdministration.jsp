<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		
		<!-- 휴가항목 설정 -->
		<div class="hru segment">
		<h1 style="color:black">휴가관리</h1>
		<hr style="color:lightgray;"/>
			<h3><i class="check square icon"></i>휴가항목 설정</h3>
			<label><i class="exclamation circle icon" id="exclamation"></i>근무상 주말 이외에 휴일로 분류되는 각종 휴일을 관리합니다.(신정, 구정, 추석, 선거일, 창립기념일...)</label>
			<br><br>
			
			<div class="scrollArea" style="width:50%;">
					<table class="hru scroll table" id="vTable">
						<thead>
							<tr>
								<th>구분</th>
								<th>코드</th>
								<th>명칭</th>
								<th>급여지급</th>
								<th>사용여부</th>
							</tr>
						</thead>
						<tbody>
						<%-- <c:set var="1" value="연차사용/유급"/>
						<c:set var="2" value="연차미사용/무급"/>
						<c:set var="3" value="연차미사용/유급"/> --%>
							<c:forEach var="i" items="${vMap.vList}">
								<tr class="scrollVacation-tr">
										<c:if test="${i.category == 1}"><td>연차사용/유급</td></c:if>
										<c:if test="${i.category == 2}"><td>연차미사용/무급</td></c:if>
										<c:if test="${i.category == 3}"><td>연차미사용/유급</td></c:if>
									<td>${i.vacationCode}</td>
									<td>${i.vacationName}</td>
									<td>${i.salaryYN}</td>
									<td>${i.useYN}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="editArea" style="width:45%;">
					<form action="vacationBasicInsert.att" method="post" id="vacationBasicForm">
					<table class="hru table">
						<thead>
							<tr  class="table-title">
								<th colspan="3" class="table-title"><label id="vbTitle">휴 가 항 목</label></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>구분</th>
								<td colspan="2"><div class="ui mini input" id="annualCinput">
											<input type="text" id="seperation" readonly></div>
										<div id="annualCategory" style="display:none;">
											<select class="ui dropdown" name="category" id="vCategory">
											  <option value="">휴가구분 선택</option>
											  <option value="3">연차미사용/유급</option>
											  <option value="2">연차미사용/무급</option>
											  <option value="1">연차사용/유급</option>
											</select></div>
								</td>
							</tr>
							<tr id="codeTr">
								<th>코드</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" name="vacationCode" id="code" readonly></div></td>
							</tr>
							<tr>
								<th>명칭</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" name="vacationName" id="name" readonly></div></td>
							</tr>
							<tr id="yearTr">
								<th>급여지급</th>
								<td><div class="ui radio checkbox"><input type="radio" id="yearUse" name="yyn"><label for="yearUse"> 지급</label></div></td>
								<td><div class="ui radio checkbox"><input type="radio" id="yearNouse" name="yyn"><label for="yearNouse"> 비지급</label></div></td>
							</tr>
							<tr>
								<th>사용여부</th>
								<td><div class="ui radio checkbox"><input type="radio" id="use" name="useYN" value="Y"><label for="use"> 사용</label></div></td>
								<td><div class="ui radio checkbox"><input type="radio" id="nouse" name="useYN" value="N"><label for="nouse"> 비사용</label></div></td>
							</tr>
						</tbody>
					</table>
					<div class="buttonArea">
						<button class="ui blue button mini" type="button" id="vbAdd">추가</button>
						<button class="ui blue button mini" type="submit" style="display:none;" id="complete">저장</button>
						<button class="ui blue button mini" type="button" id="modify">수정</button>
						<button class="ui blue button mini" type="submit" style="display:none;" id="modiComplete" onclick="vbUpdate();">완료</button>
						<button class="ui gray button mini" type="reset" id="vbReset">취소</button>
						<button class="ui gray button mini" id="vbDelete" onclick="vbdelete();">삭제</button>
					</div>
					</form>
				</div>
		</div><!-- 휴가항목 설정 끝-->
		
		<!-- 연차 부여일수 설정 -->
		<div class="hru segment">
			<h3><i class="check square icon"></i>연차 부여일수</h3>
			<div class="scrollArea" style="width:50%;">
					<table class="hru scroll table">
						<thead>
							<tr>
								<th>연차기준년수</th>
								<th>휴가일 수</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="a" items="${vMap.aList}">
								<tr class="scrollYear-tr">
									<td>${a.annualStd}</td>
									<td>${a.vacationNum}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="editArea" style="width:45%;">
					<form action="annualVacationUpdate.att" method="post" id="annualVacationForm">
					<table class="hru table">
						<thead>
							<tr  class="table-title">
								<th colspan="3" class="table-title">추 가 / 수 정</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>연차 기준년수</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="yearCtn" name="annualStd"></div> </td>
							</tr>
							<tr>
								<th>휴가일 수</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="offCtn" name="vacationNum"></div>
									<input type="hidden" value="1" name="vacationCode"><input type="hidden" value="0" name="annualCode"></td>
							</tr>
						</tbody>
					</table>
					<div class="buttonArea">
						<button class="ui blue button mini" type="submit" id="avAdd">추가/수정</button>
						<button class="ui gray button mini" type="button" onclick="avDelet();" id="avDelete">삭제</button>
					</div>
					</form>
					<label><i class="exclamation circle icon" id="exclamation"></i>근로기준법 이하로는 설정이 불가능합니다.</label><br>
				<label><i class="exclamation circle icon" id="exclamation"></i>입사 1년 미만 근로자와 1년 동안 80% 미만 출근한 근로자에게는 </label>	<br>
				<label style="padding-left:20px;">1월 개근 시 1일의 연차휴가를 부여하면 됩니다.</label>
					</div>
		</div><!-- 연차 부여일수 설정 끝-->
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		
		$(function(){
			/* 상단메뉴 */
			$("#system2").show();
			/* 드롭다운 */
			$(".ui.dropdown").dropdown();
			//$("#annualCategory").hide();
			//$("#annualCinput").hide();
			$("#vbReset").hide();
			
			/* 휴가항목 설정 */
		    $(".scrollVacation-tr").click(function(){
				$("#seperation").val($(this).find("td").eq(0).text()).attr("readonly",true); 
				$("#code").val($(this).find("td").eq(1).text()).attr("readonly",true);
				$("#name").val($(this).find("td").eq(2).text()).attr("readonly",true);
				if($(this).find("td").eq(3).text() == "Y"){
				    $("#yearUse").prop("checked", true);
				} else{
					$("#yearNouse").prop("checked", true);
				}
			   if($(this).find("td").eq(4).text() == "Y"){
				    $("#use").prop("checked", true);
				} else{
					$("#nouse").prop("checked", true);
				}
				$("#add").show();
				$("#complete").hide();
				
				/* console.log($(this).find("td").eq(0).text()); */
				if($(this).find("td").eq(0).text()=="연차사용/유급"){
					$("#vCategory").val("1").prop("selected", true);
				}else if($(this).find("td").eq(0).text()=="연차미사용/무급"){
					$("#vCategory").val("2").prop("selected", true);
				}
				else if($(this).find("td").eq(0).text()=="연차미사용/유급"){
					$("#vCategory").val("3").prop("selected", true);
				}
				$(".ui.dropdown").dropdown();
		    });
		});
		    
		    /* 연차 부여일수 */
		    $(".scrollYear-tr").click(function(){
				$("#yearCtn").val($(this).find("td").eq(0).text()).attr("readonly",false);
				$("#offCtn").val($(this).find("td").eq(1).text()).attr("readonly",false);
		    });
		    
		    /* 휴가 항목설정 추가버튼(value 초기화) */
			$("#vbAdd").click(function(){
				$("#vbTitle").text("추 가");
				$("#seperation").val("").attr("readonly",false);
				$("#code").val("0").attr("readonly",true);
				$("#name").val("").attr("readonly",false);
				$("#vbAdd").hide();
				$("#modify").hide();
				$("#complete").show();
				$("#vbDelete").hide();
				$("#vbReset").show();
				$("#yearUse").prop("checked", true);
				$("#yearNouse").prop("checked", false);
				$("#use").prop("checked", true);
				$("#nouse").prop("checked", false);
				$("#annualCinput").hide();
				$("#annualCategory").show();
				$("#codeTr").hide();
				$("#yearTr").hide();
			});
		    
		    /* 휴가항목설정 수정버튼 */
			$("#modify").click(function(){
				$("#vbTitle").text("수 정");
				$("#annualCinput").hide();
				$("#annualCategory").show();
				$("#codeTr").hide();
				$("#yearTr").hide();
				$("#vbReset").show();
				$("#name").attr("readonly",false);
				$("#leader").attr("readonly",false);
				$("#modify").hide();
				$("#vbAdd").hide();
				$("#complete").hide();
				$("#vbDelete").hide();
				$("#modiComplete").show();							
			});
		    
		    /* 휴가항목설정 취소버튼 */
		    $("#vbReset").click(function(){
		    		$("#vbTitle").text("휴 가 항 목");
		    		$("#codeTr").show();
					$("#yearTr").show();
					$("#vbReset").hide();
					$("#vbDelete").show();
					$("#modify").show();
					$("#complete").hide();
					$("#modiComplete").hide();
					$("#vbAdd").show();
					$(".ui.dropdown").dropdown();
		    })
		/* 휴가항목 수정 */    
		function vbUpdate(){
		    var action = document.forms.vacationBasicForm;
		    $("#vacationBasicForm").attr("action", "vacationBasicUpdate.att");
			$("#vacationBasicForm").submit();
		};
		
		/* 휴가항목 삭제 */
		function vbdelete(){
			var action = document.forms.vacationBasicForm;
			$("#vacationBasicForm").attr("action", "vacationBasicDelete.att");
			$("#vacationBasicForm").submit();
		}
		
		/* 연차부여일수 삭제 */
		function avDelet(){
			var action = document.forms.annualVacationForm;
			$("#annualVacationForm").attr("action", "annualVacationDelete.att");
			$("#annualVacationForm").submit();
		};
		
		


		
	</script>
</body>
</html>