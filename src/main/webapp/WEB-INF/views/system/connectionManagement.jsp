<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html" />
<style>
	#ip{
		width:80px;
	}
	.ipInput{
		width:70px;
		text-align:center;
	}
	
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp" />
	<div class="outer"><!--outer Area -->
		<div class="contentArea"><!-- 내용부분! -->
		
		
		
		<div class="hru segment"><!-- ip주소 기록 -->
		<h1 style="color:black">접속관리</h1>
		<hr style="color:lightgray;"/>
         <h3><i class="check square icon"></i>IP주소 접근 설정</h3>
         <br>
         <div class="ui form">
           <div class="inline fields" align="center">
             <div class="field">
               <div class="ui radio checkbox">
                 <input type="radio" name="frequency" value="Y" <c:if test="${hmap.useYN == 1}">checked</c:if>>
                 <label>IP주소로 출/퇴근 기록 허용</label>
               </div>
             </div>
             <div class="field">
               <div class="ui radio checkbox">
                 <input type="radio" name="frequency" value="N" <c:if test="${hmap.useYN == 2}">checked</c:if>>
                 <label>IP주소로 출/퇴근 기록 허용하지 않음</label>
               </div>
             </div>
           </div>
         </div>
      
		<h3><i class="check square icon"></i>IP주소</h3>
		<table class="hru board" align="center" id="ipTable">
						<thead>
							<tr>
								<th style="text-align:center;">IP 대역폭</th>
								<th style="text-align:center;">메모</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="i" items="${hmap.ipList}">
							<tr>
								<td style="text-align:center;">${i.startBand} ~ ${i.endBand}</td>
								<td style="text-align:center;">${i.memo}
										<input type="hidden" id="ipCode" value="${i.ipCode}">
										<input type="hidden" id="ipMemo" value="${i.memo}">
										<input type="hidden" id="ipstartBand" value="${i.startBand}">
										<input type="hidden" id="ipendBand" value="${i.endBand}">
								</td>
								
							</tr>
							</c:forEach>
						</tbody>
					</table>
			<div class="buttonArea">
						<button class="ui blue button mini" id="ipAdd" onclick="addIp();">IP주소 등록</button>
		</div><!-- ip주소 기록 끝-->
		</div>
	</div>
	
	<!-- IP주소 등록 모달창 -->
	<div class="ui modal" id="ipModal" style="width:800px;">
	  <div class="header" align="center">IP 대역폭 추가</div>
	  <div class="content" align="center">
	  	<form action="" method="post" id="ipForm">
	    	<table>
	    		<tr>
	    			<td>접속가능 IP</td>
	    			<td>
	    				<div class="ui mini icon input" style="display: block; margin-botton:10px;">
  							<input class="ipInput" id="ip1" type="text">.&nbsp;&nbsp;&nbsp;<input class="ipInput" id="ip2" type="text">.&nbsp;&nbsp;&nbsp;<input class="ipInput" id="ip3" type="text">.&nbsp;&nbsp;&nbsp;<input class="ipInput" id="ip4" type="text">
  							&nbsp;&nbsp;&nbsp;~&nbsp;&nbsp;&nbsp;<input class="ipInput" id="ip5" type="text">.&nbsp;&nbsp;&nbsp;<input class="ipInput" id="ip6" type="text">.&nbsp;&nbsp;&nbsp;<input class="ipInput" id="ip7" type="text">.&nbsp;&nbsp;&nbsp;<input class="ipInput" id="ip8" type="text">
  							<input type="hidden" id="startIp" name="startBand">
  							<input type="hidden" id="endIp" name="endBand">
  							<input type="hidden" id="ipcode" name="ipCode" value="0">
						</div>
					</td>
				</tr>
	    		<tr><td style="text-align: center;">메모</td><td><div class="ui mini icon input"><input type="text" style="width:410px;" id="mMomo" name="memo"></div></td></tr>
	    	</table>
	    </form>
	  </div>
	  <div class="actions">
	    <div class="buttonArea modal">
			<button class="ui blue button mini" id="add">추가</button>
		</div>
	  </div>
	</div>
</div>	
	<script>
		/* ip대역폭 추가 모달 */
		function addIp(){
			$('.ui.modal').modal('show');
		}
		
		$(function(){
			$("#system2").show();
		});
		
		//IP대역폭 사용 여부
		$(document).on('change', ':input:radio[name=frequency]', function(){
			var ipUSE = $(":input:radio[name=frequency]:checked").val();
			
			$.ajax({
				url:"ipUSE.st",
				type:"post",
				data:{"ipUSE":ipUSE},
				success:function(data){
					console.log(data);
					if(data.result != 0){
						Swal.fire(
								  'IP주소 접근설정',
								  'IP주소 접근설정이 완료되었습니다.',
								  'success'
						)
					}else{
						Swal.fire(
								  'IP주소 접근설정 실패',
								  'IP주소 접근설정이 실패되었습니다.',
								  'warning'
						)
					}
				},
				error:function(status){
					console.log("fail!");
				}
			});
		});
		
		$(document).on('click', "#modify", function(){
			var ipCode = $("#ipcode").val();
			var startIp = $("#ip1").val()+"."+$("#ip2").val()+"."+$("#ip3").val()+"."+$("#ip4").val();
			$("#startIp").val(startIp);
			var endIp = $("#ip5").val()+"."+$("#ip6").val()+"."+$("#ip7").val()+"."+$("#ip8").val();
			$("#endIp").val(endIp);
			console.log($("#endIp").val());
			var params = $("#ipForm").serialize();
			$.ajax({
				url:"updateIP.st",
				type:"post",
				data:params,
				success:function(data){
					console.log(data);
					if(data.result==1){
						Swal.fire(
								  'IP 수정완료',
								  'IP대역폭 수정이 완료되었습니다.',
								  'success'
						).then((result)=>{
							$('.ui.modal').modal('hide');
						}).then((result)=>{
							window.location.replace('showConnectionManagement.st');
						});
					}else{
						Swal.fire(
								  'IP 수정실패',
								  'IP대역폭 수정이 실패되었습니다.',
								  'warning'
						).then((result)=>{
							$('.ui.modal').modal('hide');
						});
					}
				},
				error:function(status){
					console.log("fail!");
				}
			});
		})
		
		/* ip대역폭 추가 */
		$("#add").click(function(){
			var startIp = $("#ip1").val()+"."+$("#ip2").val()+"."+$("#ip3").val()+"."+$("#ip4").val();
			$("#startIp").val(startIp);
			var endIp = $("#ip5").val()+"."+$("#ip6").val()+"."+$("#ip7").val()+"."+$("#ip8").val();
			$("#endIp").val(endIp);
			console.log($("#endIp").val());
			var params = $("#ipForm").serialize();
			$.ajax({
				url:"ipInsert.st",
				type:"post",
				data:params,
				success:function(data){
					console.log(data);
					if(data.result==1){
						Swal.fire(
								  'IP 입력완료',
								  'IP대역폭이 입력되었습니다.',
								  'success'
						).then((result)=>{
							$('.ui.modal').modal('hide');
						}).then((result)=>{
							window.location.replace('showConnectionManagement.st');
						});
					}else{
						Swal.fire(
								  'IP 입력실패',
								  'IP대역폭이 입력이 실패되었습니다.',
								  'warning'
						).then((result)=>{
							$('.ui.modal').modal('hide');
						});
					}
				},
				error:function(status){
					console.log("fail!");
				}
			
			});
		});
		
		$("#ipTable tr").click(function(){
			$(".buttonArea.modal").empty();
			var ipCode = $(this).find("#ipCode").val();
			console.log("ipCode : " + ipCode);
			var ips = $(this).find("#ipstartBand").val();
			var ipsArray = ips.split(".");
			var ipe = $(this).find("#ipendBand").val();
			var ipeArray = ipe.split(".");
			var memo = $(this).find("#ipMemo").val();
			//console.log(memo);
			$("#ip1").val(ipsArray[0]);
			$("#ip2").val(ipsArray[1]);
			$("#ip3").val(ipsArray[2]);
			$("#ip4").val(ipsArray[3]);
			$("#ip5").val(ipeArray[0]);
			$("#ip6").val(ipeArray[1]);
			$("#ip7").val(ipeArray[2]);
			$("#ip8").val(ipeArray[3]);
			$("#mMomo").val(memo);
			$("#ipcode").val(ipCode);
			//console.log(ipsArray);
			console.log($("#ipcode").val());
			$(".buttonArea.modal").append("<button class='ui blue button mini' id='modify'>수정</button>")
			$(".buttonArea.modal").append("<button class='ui blue button mini' id='delete'>삭제</button>")
			$("#add").hide();
			$('.ui.modal').modal('show');
		});
		
		

		//ip대역폭 수정
		/* $(document.body).delegate('#modity', 'click', function(){
			console.log("delete클릭");
			console.log($("#ipcode").val());
		
			
		}) */
		//ip대역폭 삭제
		$(document.body).delegate('#delete', 'click', function(){
			console.log("delete클릭");
			console.log($("#ipcode").val());
			var ipCode = $("#ipcode").val();
			$.ajax({
				url:"deleteIP.st",
				type:"post",
				data:{"ipCode":ipCode},
				success:function(data){
					console.log(data);
					if(data.result == 1){
						Swal.fire(
								  '삭제 완료',
								  'IP대역폭 삭제가 완료되었습니다.',
								  'success'
						).then((result)=>{
							$('.ui.modal').modal('hide');
						}).then((result)=>{
							window.location.replace('showConnectionManagement.st');
						});
					}else{
						Swal.fire(
								  '삭제 실패',
								  'IP대역폭 삭제가 실패되었습니다.',
								  'warning'
						).then((result)=>{
							$('.ui.modal').modal('hide');
						});
					}
					
				},
				error:function(status){
					console.log("fail!");
				}
			});
		})
	</script>
</body>
</html>