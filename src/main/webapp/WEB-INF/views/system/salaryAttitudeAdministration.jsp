<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html" />
<!-- 상대경로 잘 지정해야함! -->
<style>
	.leftMenu{
		height:2200px;
	}
</style>
</head>
<body> 
	<jsp:include page="../common/menubar.jsp" />
	<!-- 상대경로 잘 지정해야함! -->
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<!-- 여기에 내용부분 작성! -->
		<form action="standardInfo.salary" method="post"><!-- 급여기준 , 근태기준 폼 시작 -->
			<div class="hru segment"><!-- 급여기준 시작 -->
				<h3><i class="check square icon"></i>급여기준</h3>
				<hr><br>
				<table class="hru table" >
					<tr>
						<th>급여계약기준</th>
						<td>
							<select class="ui dropdown" id="SalaryStandard" name ="salaryStandard">
								<option value="" selected disabled hidden>카테고리</option>
								<option value="1" <c:if test="${sMap.slistOne[0].content eq '연봉제' }">selected="selected"</c:if>>연봉제</option>
								<option value="2" <c:if test="${sMap.slistOne[0].content eq '월급제' }">selected="selected"</c:if>>월급제</option>
                                <option value="3" <c:if test="${sMap.slistOne[0].content eq '시급제' }">selected="selected"</c:if>>시급제</option>
                            </select>
						</td>
						<th>급여지급일</th>
						<td>
							<div class="ui mini input">
								<c:if test="${sMap.slistOne[1].content eq null}">
									<input type="text" id="paymentdate" name="giveMoneyDate">
								</c:if>
								<c:if test="${sMap.slistOne[1].content ne null}">
									<input type="text" id="paymentdate" name="giveMoneyDate" value="${sMap.slistOne[1].content }">
								</c:if>
							</div>
						</td>
						<th>근태 시작일</th>
						<td>
							<select class="ui dropdown" id="AccountStandard" name="accountStandard">
								<option value="1" <c:if test="${sMap.slistOne[2].content eq '월말'}">selected="selected"</c:if>>월말</option>
								<option value="2" <c:if test="${sMap.slistOne[2].content eq '월초'}">selected="selected"</c:if>>월초</option>
							</select>
						</td>
					</tr>
				</table>
			</div><!-- 급여기준 end -->
			<div class="hru segment"><!-- 근태기준 start -->
				<h3><i class="check square icon"></i>근태기준</h3>
				<hr><br>
				<table class="hru table" >
					<tr>
						<th style="width:5%" colspan="1">토요일근무산정방법</th>
						<td colspan="3">
							<select class="ui dropdown" id="SaturdaySelect" name="saturdaySelect">
								<option value="" selected disabled hidden>카테고리</option>
								<option value="1" <c:if test="${sMap.slistTwo[0].content eq '무급휴무일'}">selected="selected"</c:if>>무급휴무일</option>
								<option value="2" <c:if test="${sMap.slistTwo[0].content eq '정상근무'}">selected="selected"</c:if>>정상근무</option>
							</select>
						</td>
					</tr>
					<tr>
						<th style="width:5%">근무시각</th>
							<td>
								<c:if test="${sMap.slistTwo[1].content eq null }">
									<div class="ui mini input"><input type="text" id="workingstart" name="workingstart"></div>
									 ~  																	
									<div class="ui mini input"><input type="text" id="workingend" name="workingend"></div> 
								</c:if>
								<c:if test="${sMap.slistTwo[1].content ne null }">
									<div class="ui mini input"><input type="text" id="workingstart" name="workingstart" value="${fn:substring((sMap.slistTwo[1].content),0,5)}"></div>
									 ~  																	
									<div class="ui mini input"><input type="text" id="workingend" name="workingend" value="${fn:substring((sMap.slistTwo[1].content),6,12) }"></div>  
								</c:if>
							</td>
						<th>연장근로시각</th>
							<td>
							<c:if test="${sMap.slistTwo[2].content eq null}">
								<div class="ui mini input"><input type="text" id="extensionstart" name="extensionstart"></div> 
								~ 
								<div class="ui mini input"><input type="text" id="extensionend" name="extensionend" ></div> 
							</c:if>
							<c:if test="${sMap.slistTwo[2].content ne null }">
								<div class="ui mini input"><input type="text" id="extensionstart" name="extensionstart" value="${fn:substring((sMap.slistTwo[3].content),0,5)}" ></div> 
								~ 
								<div class="ui mini input"><input type="text" id="extensionend" name="extensionend" value="${fn:substring((sMap.slistTwo[3].content),6,12) }"></div> 
							</c:if>
							</td>
					</tr>
					<tr>
						<th style="width:20%">휴계시각</th>
							<td>
							<c:if test="${sMap.slistTwo[3].content eq null }">
								<div class="ui mini input"><input type="text" id="breakstart" name="breakstart"></div> 
								~ 
								<div class="ui mini input"><input type="text" id="breakend" name="breakend"></div> 
							</c:if>
							<c:if test="${sMap.slistTwo[3].content ne null }">
								<div class="ui mini input"><input type="text" id="breakstart" name="breakstart" value="${fn:substring((sMap.slistTwo[2].content),0,5) }"></div> 
								~ 
								<div class="ui mini input"><input type="text" id="breakend" name="breakend" value="${fn:substring((sMap.slistTwo[2].content),6,12) }"></div> 
							</c:if>
							</td>
						<th style="width:20%">야간근로시각</th>
							<td>
							<c:if test="${sMap.slistTwo[4].content eq null }">
								<div class="ui mini input"><input type="text" id="nightstart" name="nightstart"></div> 
								~ 
								<div class="ui mini input"><input type="text" id="nightend" name="nightend"></div> 
							</c:if>
							<c:if test="${sMap.slistTwo[4].content ne null }">
								<div class="ui mini input"><input type="text" id="nightstart" name="nightstart" value="${fn:substring((sMap.slistTwo[4].content),0,5) }" ></div> 
								~ 
								<div class="ui mini input"><input type="text" id="nightend" name="nightend" value="${fn:substring((sMap.slistTwo[4].content),6,12) }"></div> 
							</c:if>
							</td>
					</tr>
					<tr>
						<th style="width:20%">토요근무시각</th>
							<td>
								<div class="ui mini input"><input type="text" id="saturdayworkstart" name="saturdayworkstart" disabled="disabled"></div>
								 ~ 
								<div class="ui mini input"><input type="text" id="saturdayworkend" name="saturdayworkend" disabled="disabled"></div> 
							</td>
						<th style="width:20%">토요연장근로시각</th>
							<td>
								<div class="ui mini input"><input type="text" id="saturdayextensionstart" name="saturdayextensionstart" disabled="disabled" ></div>
								 ~ 
								<div class="ui mini input"><input type="text" id="saturdayextensionend" name="saturdayextensionend" disabled="disabled"></div> 
							</td>
					</tr>
					<tr>
						<th style="width:20%">토요휴계시각</th>
							<td>
								<div class="ui mini input"><input type="text" id="saturdaybreakstart" name="saturdaybreakstart" disabled="disabled" ></div>
								 ~ 
								<div class="ui mini input"><input type="text" id="saturdaybreakend" name="saturdaybreakend" disabled="disabled"></div> 
							</td>
						<th style="width:20%">토요야간근로시각</th>
							<td>
								<div class="ui mini input"><input type="text" id="saturdaynightstart" name="saturdaynightstart" disabled="disabled" ></div>
								 ~ 
								<div class="ui mini input"><input type="text" id="saturdaynightend" name="saturdaynightend" disabled="disabled"></div> 
							</td>
					</tr>
				</table>
				<br>
				<div class="ui right pointing inverted yellow label" style="color:white;">TIP</div>
				<span>토요일이 정상근무인 경우에는 '토요근무산정방법'을 정상근무로 선택한 후 토요근무시간을 입력해야 합니다.</span>
				<br>
				<button class="ui blue button mini" id="addStandard" name="addStandard" style="float: right">저장</button>
			</div><!-- 근태기준 종료 -->
		</form><!-- 급여기준 , 근태기준 폼 종료 -->
		<form method="post" action="SalaryInfo.salary"><!-- 기본급/상여금 폼 시작 -->
			<div class="hru segment"><!-- 기본급/상여금 시작 -->
				<h3><i class="check square icon"></i>기본급/상여금</h3>
				<hr><br>
				<table class="hru table">
					<tr>
						<th style="width: 20%">기본급</th>
						<td>
							<div class="ui mini input"><input type="text" id="defaultPayment" name="nameASList[0].stdpercent"  value="${sMap.aslistOne[0].stdpercent}"></div>%	 
						</td>
						<th>정기상여</th>
						<td><div class="ui mini input"><input type="text" id="plusPayment" name="nameASList[1].stdpercent" value="${sMap.aslistOne[1].stdpercent }"></div> 
							% &nbsp; &nbsp;
							<select class="ui dropdown" id="MonthlyplusSalary" name="nameASList[4].accountstd">
								<option value="" selected disabled hidden>카테고리</option>
								<option value="1" <c:if test="${sMap.aslistOne[4].accountstd eq '매달지급'}">selected="selected"</c:if>>매달지급</option>
								<option value="2" <c:if test="${sMap.aslistOne[4].accountstd eq '짝수달지급'}">selected="selected"</c:if>>짝수달 지급</option>
								<option value="3" <c:if test="${sMap.aslistOne[4].accountstd eq '홀수달지급'}">selected="selected"</c:if>>홀수달 지급</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>시점상여(구정)</th>
						<td><div class="ui mini input"><input type="text" id="plusPaymentOne" name="nameASList[3].stdpercent" value="${sMap.aslistOne[3].stdpercent}"></div>%</td>
						<th>시점상여(추석)</th>
						<td><div class="ui mini input"><input type="text" id="plusPaymentTwo" name="nameASList[2].stdpercent" value="${sMap.aslistOne[2].stdpercent}"></div>%</td>							
					</tr>
				</table>
				<br>
				<div class="ui right pointing inverted yellow label" style="color:white;">TIP</div>
				<span>기본급은 100%를 매월 지급되는 금액의 12개월 분입니다.</span>
			</div><!-- 기본급/상여금  종료-->
		<button class="ui blue button mini" id="addSalayStandard" style="float: right">저장</button>
		<br>
		</form><!-- 기본급/상여금 폼 종료 -->
		<form method="post" action="SalaryNoneTaxAdd.salary" id="SalaryNoneTax">
			<div><!-- 비과세 항목 시작 -->
				<h3><i class="check square icon"></i>비과세 항목</h3>
				<hr><br>
				<div class="scrollArea" style="width:50%;" >
					<table class="hru scroll table noneSalary">
						<thead>
							<tr>
								<th>급여코드</th>
								<th>항목명</th>
								<th>금액(원)</th>
								<th>적용범위</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="i" items="${sMap.aslistTwo}">
								<tr class="scroll-tr">
									<td>${i.accountCode}</td>
									<td>${i.accountName}</td>
									<td>${i.accountMoney}</td>
									<td>${i.accountstd }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="editArea" style="width:40%;">
					<table class="hru table">
						<thead>
							<tr  class="table-title">
								<th colspan="3" class="table-title">수 정</th>
							</tr>
						</thead>
						<tbody>
							<tr id="SalaryCodeTr">
								<th>급여코드</th>
								<td colspan="2"><div class="ui mini input"><input type="number" id="Salarycode" name="accountCode"  readonly></div></td>
							</tr>
							<tr>
								<th>항목명</th>
								<td colspan="2"><div class="ui mini input"><input type="text" id="Salaryname" name="accountName" readonly></div></td>
							</tr>
							<tr>
								<th>금액</th>
								<td colspan="2"><div class="ui mini input"><input type="text" id="Salarymoney" name="accountMoney" readonly></div></td>
							</tr>
							<tr>
								<th>젹용범위</th>
								<td><div class="ui radio checkbox"><input type="radio" id="Salaryuse" name="countNumber" value="1"><label for="Salaryuse"> 전체사원</label></div></td>
								<td><div class="ui radio checkbox"><input type="radio" id="Salarynouse" name="countNumber" value="2"><label for="Salarynouse"> 일부사원</label></div></td>
							</tr>
						</tbody>
					</table>
					<div class="buttonArea">
						<button class="ui blue button mini" id="addnoneSalary" type="button">추가</button><!-- 비과세항목 view 이동 -->
						<button class="ui blue button mini" style="display:none;" type="submit" id="nondSalarycomplete">저장</button><!-- 비과세항목 추가 저장 버튼 -->
						<button class="ui blue button mini" id="savenoneSalary" type="button" onclick="return saveOneSalary();" >저장</button><!-- 비과세항목 업데이트 버튼 -->
						<button class="ui gray button mini" id="deletenoneSalary"type="button" onclick="return deleteNoneSalary();">삭제</button><!-- 비과세항목 삭제 버튼 -->
					</div>
				</div>
			</div><!-- 비과세 항목 end -->
		</form>
			<div style="height:100px;"></div>
			<div><!-- 연장 근무수당 설정 start -->
			<h3><i class="check square icon"></i>연장근무수당 설정</h3>
				<hr><br>
				<div class="scrollArea" style="width:50%;">
						<table class="hru scroll table">
							<thead>
								<tr>
									<th>급여코드</th>
									<th>항목명</th>
									<th>가산 % </th>
								</tr>
							</thead>
							<tbody>
							<c:forEach  items="${sMap.aslistThree }" var="i">
								<tr class="scrollTwo-tr">
									<td>${ i.accountCode}</td>
									<td>${ i.accountName}</td>
									<td>${ i.stdpercent}</td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="editArea" style="width:40%;">
						<form action="ExtensionAdd.salary" method="post" id="ExtensionAdd">
						<table class="hru table">
							<thead>
								<tr class="table-title">
									<th colspan="3" class="table-title">수 정</th>
								</tr>
							</thead>
							<tbody>
								<tr id="plusWorkTr">
									<th>급여코드</th>
									<td colspan="2"><div class="ui mini input"><input type="number" id="code" name="accountCode"  readonly="readonly"></div></td>
								</tr>
								<tr>
									<th>항목명</th>
									<td colspan="2"><div class="ui mini input"><input type="text" id="name" name="accountName"></div></td>
								</tr>
								<tr>
									<th>가산</th>
									<td colspan="2">( &nbsp;
										<div class="ui mini input">
											<input type="text" id="plusmoney" readonly value="100" style="text-align: center;">
										</div>&nbsp;+ 
										<div class="ui mini input">
											<input type="text" id="pluspercent" name="stdpercent">
										</div>
										 &nbsp;
										)&nbsp; %</td>
								</tr>
							</tbody>
						</table>
						<div class="buttonArea">
							<button class="ui blue button mini" id="add" type="button">추가</button><!-- 연장근무수당 추가view이동 -->
							<button class="ui blue button mini" id="complete" style="display:none;" type="submit" >저장</button><!-- 연장근무수당 추가 insert   -->
							<button class="ui blue button mini" id="save" type="button" onclick="return savePlus();">저장</button><!-- 연장근무수당 update -->
							<button class="ui gray button mini" id="delete" type="button" onclick="return deletePlus();">삭제</button><!-- 연장근무수당 delete -->
						</div>
						</form>
					</div>
			</div><!-- 연장근무수당 설정 end  -->
			<div style="height:100px;"></div>
			<div><!-- 4대보험 요율 설정  start-->
				<h3><i class="check square icon"></i>4대보험 요율 설정</h3>
					<hr><br>
					<form method="post" action="fourInsuranceUpdate.salary">
					<div style="display: inline-block; width: 45%">
						<table class="hru table" >
							<thead>
								<tr class="table-title">
									<th colspan="4" class="table-title">회사</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>건강보험</th>
									<td><div class="ui mini input"><input type="text" id="firstCompany" name="nameASList[2].stdpercent" value="${sMap.aslistFour[2].stdpercent}"></div> &nbsp; %</td>
									<th>장기요양보험</th>
									<td><div class="ui mini input"><input type="text" id="secondCompany" name="nameASList[4].stdpercent" value="${sMap.aslistFour[4].stdpercent}"></div>&nbsp; %</td>
								</tr>
								<tr>
									<th>국민연금</th>
									<td><div class="ui mini input"><input type="text" id="thirdCompany" name="nameASList[0].stdpercent" value="${sMap.aslistFour[0].stdpercent }"></div>&nbsp; %</td>
									<th>고용보험</th>
									<td><div class="ui mini input"><input type="text" id="fourCompany" name="nameASList[5].stdpercent" value="${sMap.aslistFour[5].stdpercent }"></div>&nbsp; %</td>
								</tr>
								<tr>
									<th>산재보험</th>
									<td colspan="3"><div class="ui mini input"><input type="text" id="fiveCompany" name="nameASList[7].stdpercent" value="${sMap.aslistFour[7].stdpercent }"></div>&nbsp; %</td>
								</tr>
							</tbody>
						</table>
						<br>
					</div>
					<div style="display: inline-block; width: 45%; margin-left: 100px; vertical-align: top"  >
						<table class="hru table" >
							<thead>
								<tr class="table-title">
									<th colspan="4" class="table-title">근로자</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>건강보험</th>
									<td><div class="ui mini input"><input type="text" id="firstPerson" name="nameASList[3].stdpercent" value="${sMap.aslistFour[3].stdpercent}"></div> &nbsp; %</td>
									<th>장기요양보험</th>
									<td><div class="ui mini input"><input type="text" id="secondPerson" name="nameASList[4].stdpercent" value="${sMap.aslistFour[4].stdpercent}"></div>&nbsp; %</td>
								</tr>
								<tr>
									<th>국민연금</th>
									<td><div class="ui mini input"><input type="text" id="thirdPerson"  name="nameASList[1].stdpercent" value="${sMap.aslistFour[1].stdpercent }"></div>&nbsp; %</td>
									<th>고용보험</th>
									<td><div class="ui mini input"><input type="text" id="fourPerson" name="nameASList[6].stdpercent"value="${sMap.aslistFour[6].stdpercent }"></div>&nbsp; %</td>
								</tr>
							</tbody>
						</table>
						<br>
					</div>
					<button class="ui blue button mini" id="addTax" style="float: right">저장</button>
					</form>
			</div><!-- 4대보험 요율 설정 end -->
			<div style="height: 100px;"></div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function() {
			$(".ui.dropdown").dropdown();
			$("#system1").show();
			if($("#SaturdaySelect option:selected").val() == 2){
				/* 토요 근무 시각 */
				var saturdayworkstart = "${(sMap.slistTwo[5].content)}".substring(0,5);
				var saturdayworkend = "${(sMap.slistTwo[5].content)}".substring(6,12);
				/* 토요 연장근로 시각 */
				var saturdayextensionstart = "${(sMap.slistTwo[7].content)}".substring(0,5);
				var saturdayextensionend = "${(sMap.slistTwo[7].content)}".substring(6,12);
				/* 토요휴계시각 */
				var saturdaybreakstart = "${(sMap.slistTwo[6].content)}".substring(0,5);
				var saturdaybreakend = "${(sMap.slistTwo[6].content)}".substring(6,12);
				/* 토요야간근로시각 */
				var saturdaynightstart = "${(sMap.slistTwo[8].content)}".substring(0,5);
				var saturdaynightend = "${(sMap.slistTwo[8].content)}".substring(6,12);
				
				$("#saturdayworkstart").attr("disabled" , false).val(saturdayworkstart)
				$("#saturdayworkend").attr("disabled" , false).val(saturdayworkend)
				$("#saturdayextensionstart").attr("disabled" , false).val(saturdayextensionstart)
				$("#saturdayextensionend").attr("disabled" , false).val(saturdayextensionend)
				$("#saturdaybreakstart").attr("disabled" , false).val(saturdaybreakstart)
				$("#saturdaybreakend").attr("disabled" , false).val(saturdaybreakend)
				$("#saturdaynightstart").attr("disabled" , false).val(saturdaynightstart)
				$("#saturdaynightend").attr("disabled" , false).val(saturdaynightend)
			}
		});
		/* 데이트피커 */
		$('#date_calendar').calendar({
		    type: 'date',
		    formatter: {
		    	date: function (date, settings) {
		    	      if (!date) return '';
		    	      var day = date.getDate();
		    	      var month = date.getMonth() + 1;
		    	      var year = date.getFullYear();
		    	      return year + '-' + month + '-' + day;
		    	}
		    }
		});
		
		/* 토요근무산정방법 변경 script */
		$("#SaturdaySelect").change(function(){
			if($("#SaturdaySelect option:selected").val() == 2){
				/* 토요 근무 시각 */
				var saturdayworkstart = "${(sMap.slistTwo[5].content)}".substring(0,5);
				var saturdayworkend = "${(sMap.slistTwo[5].content)}".substring(6,12);
				/* 토요 연장근로 시각 */
				var saturdayextensionstart = "${(sMap.slistTwo[7].content)}".substring(0,5);
				var saturdayextensionend = "${(sMap.slistTwo[7].content)}".substring(6,12);
				/* 토요휴계시각 */
				var saturdaybreakstart = "${(sMap.slistTwo[6].content)}".substring(0,5);
				var saturdaybreakend = "${(sMap.slistTwo[6].content)}".substring(6,12);
				/* 토요야간근로시각 */
				var saturdaynightstart = "${(sMap.slistTwo[8].content)}".substring(0,5);
				var saturdaynightend = "${(sMap.slistTwo[8].content)}".substring(6,12);
				
				$("#saturdayworkstart").attr("disabled" , false).val(saturdayworkstart)
				$("#saturdayworkend").attr("disabled" , false).val(saturdayworkend)
				$("#saturdayextensionstart").attr("disabled" , false).val(saturdayextensionstart)
				$("#saturdayextensionend").attr("disabled" , false).val(saturdayextensionend)
				$("#saturdaybreakstart").attr("disabled" , false).val(saturdaybreakstart)
				$("#saturdaybreakend").attr("disabled" , false).val(saturdaybreakend)
				$("#saturdaynightstart").attr("disabled" , false).val(saturdaynightstart)
				$("#saturdaynightend").attr("disabled" , false).val(saturdaynightend)
			}else{
				$("#saturdayworkstart").attr("disabled" , true).val("")
				$("#saturdayworkend").attr("disabled" , true).val("")
				$("#saturdayextensionstart").attr("disabled" , true).val("")
				$("#saturdayextensionend").attr("disabled" , true).val("")
				$("#saturdaybreakstart").attr("disabled" , true).val("")
				$("#saturdaybreakend").attr("disabled" , true).val("")
				$("#saturdaynightstart").attr("disabled" , true).val("")
				$("#saturdaynightend").attr("disabled" , true).val("")
			}
		})
		$(".scroll-tr").click(function() {
			//console.log($(this).find("td").eq(0).text());
			$("#Salarycode").val($(this).find("td").eq(0).text()).attr("readonly", true);
			$("#Salaryname").val($(this).find("td").eq(1).text()).attr("readonly", false);
			$("#Salarymoney").val($(this).find("td").eq(2).text()).attr("readonly", false);
			$("#SalaryCodeTr").show();  
			$("#savenoneSalary").show();  /* 저장버튼 표시 */
			$("#deletenoneSalary").show(); /* 삭제버튼 표시 */
			$("#Salarycode").attr("disabled", false);
			if ($(this).find("td").eq(3).text() == "전체") {
				$("#Salaryuse").prop("checked", true);
				//console.log("Y");
				} else {
				$("#Salarynouse").prop("checked", true);
				//console.log("N");
				}
				$("#addnoneSalary").show(); /* 추가버튼 표시  */
				$("#nondSalarycomplete").hide(); /* 저장버튼 숨김 */
		});
		/* 추가버튼 클릭시 */
		$("#addnoneSalary").click(function() {
			$("#SalaryCodeTr").hide();
			$("#Salarycode").attr("disabled", true);
			$("#Salarycode").val("").attr("readonly", false);
			$("#Salaryname").val("").attr("readonly", false);
			$("#Salarymoney").val("").attr("readonly", false);
			$("#addnoneSalary").hide(); /* 추가버튼 숨김 */
			$("#nondSalarycomplete").show();  /* 저장 버튼 표시 */
			$("#savenoneSalary").hide();  /* 저장버튼 숨김 */
			$("#deletenoneSalary").hide();  /* 삭제버튼 숨김 */
			$("#Salaryuse").prop("checked", false);
			$("#Salarynouse").prop("checked", false);
		});
		/* 비과세 항목 업데이트 버튼 */
		function saveOneSalary(){
			var action = document.forms.SalaryNoneTax;
			$("#SalaryNoneTax").attr("action", "SalaryNoneTaxUpdate.salary");
			$("#SalaryNoneTax").submit();
		}
		/* 비과세항목 삭제버튼 */
		function deleteNoneSalary(){
			var action = document.forms.SalaryNoneTax;
			$("#SalaryNoneTax").attr("action", "SalaryNoneTaxDelete.salary");
			$("#SalaryNoneTax").submit();
		}
		
		/* 행 클릭시 script */
		$(".scrollTwo-tr").click(function(){
			$("#code").val($(this).find("td").eq(0).text()).attr("readonly",true);
			$("#name").val($(this).find("td").eq(1).text()).attr("readonly",false);
			$("#pluspercent").val($(this).find("td").eq(2).text()).attr("readonly" , false);
			$("#plusWorkTr").show();
			$("#add").show();  /* 추가버튼 표시 */
			$("#delete").show(); /* 삭제버튼 표시 */
			$("#save").show(); /* 저장버튼 표시  */
			$("#complete").hide(); /* 추가저장버튼 숨김 */
			$("#code").attr("disabled", false);
		});
		/* 추가버튼 클릭시  */
		$("#add").click(function(){
			$("#code").attr("disabled", true);
			$("#code").val("").attr("readonly", false);
			$("#name").val("").attr("readonly",false);
			$("#pluspercent").val("").attr("readonly",false);
			$("#plusWorkTr").hide();
			$("#add").hide();
			$("#complete").show();
			$("#save").hide();
			$("#delete").hide();
		})
		/* 연장근무수당 UPDATE */
		function savePlus(){
			var action = document.forms.ExtensionAdd;
			$("#ExtensionAdd").attr("action", "SalaryExtensionUpdate.salary");
			$("#ExtensionAdd").submit();
		}
		/* 연장근수당 DELETE */
		function deletePlus(){
			var action = document.forms.ExtensionAdd;
			$("#ExtensionAdd").attr("action", "SalaryExtensionDelete.salary");
			$("#ExtensionAdd").submit();
		}
	</script>
</body>
</html>