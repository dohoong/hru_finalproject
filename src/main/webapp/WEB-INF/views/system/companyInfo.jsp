<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회사 정보</title>
<jsp:include page="../common/importlib.html" />
</head>
<body>

	<jsp:include page="../common/menubar.jsp" />
	<div class="outer"><!--outer Area -->
		<div class="contentArea"><!-- 내용부분! -->
		<div class="hru segment">
		
				<h3><i class="check square icon"></i>회사 정보</h3>
				<form id="companyForm" method="post">
				<table class="hru table">
				
					<tr>
						<th>회사명</th>
						<td id="Gname"><c:out value="${ bgselectMap.balist[groupCode ==1].groupName }" /></td>
						<th>법인구분</th>  
						<td><div class="ui compact selection dropdown">
									<input type="hidden" name="category"><i class="dropdown icon"></i>
										<div class="default text">법인구분</div>
									<div class="menu">
									<div class="item">
									<c:choose>
												
												<c:when test="${bgselectMap.balist[groupCode==1].corporation_YN eq 'N' }"><c:out value="개인"/></c:when>
												<c:when test="${bgselectMap.balist[groupCode==1].corporation_YN eq 'Y' }"><c:out value="법인"/></c:when>
									</c:choose>
									</div>	
								</div>
							</div>	
						</td>
							
										
					</tr>
					
					<tr>
						<th>사업자 등록번호</th>
						<td id="Gnumber"><c:out value="${ bgselectMap.balist[groupCode ==1].businessNumber }"/></td>
						<th>전화 번호</th>
						<td><div class="ui input" id="GNumber">
  							<input type="text" placeholder="사업자 등록번호" value="${ bgselectMap.balist[groupCode ==1].phone }"></div>
  						</td>
  						<td><input type="hidden" id="groupCode" name="groupCode" value="1"readonly>
						<input type="hidden" id="groupCode" name="groupCode" value="0"readonly></td>
					</tr>
					<tr>
						<th>법인 등록번호</th>
						<td id="Ginumber"><c:out value="${ bgselectMap.balist[groupCode ==1].corporation_Num }"/></td>
						<th>Fax 번호</th>
						<td><div class="ui input" id="GFnumber">
  							<input type="text" value="${ bgselectMap.balist[groupCode ==1].faxNumber }"></div>
						</td>
						<td><input type="hidden" id="groupCode" name="groupCode" value="1"readonly>
						<input type="hidden" id="groupCode" name="groupCode" value="0"readonly></td>
					</tr>
					<tr>
						<th>대표자 이름</th>
						<td id="CEO"><c:out value="${ bgselectMap.balist[groupCode ==1].ceo }"/></td>
						<th>설립일자</th>
						<td><div class="ui calendar" id="standard_calendar">
							  <div class="ui input left icon" id="GBday">
							    <c:out value="${ bgselectMap.balist[groupCode ==1].birthDate }"/>
							  	<input type="hidden" id="groupCode" name="groupCode" value="1"readonly>
								<input type="hidden" id="groupCode" name="groupCode" value="0"readonly>
							  </div>
							</div>
						</td>
					</tr>
					<tr>
						<th>대표자 주민등록번호</th>
						<td colspan="4"><c:out value="950101-1******"/></td>
					</tr>
					<tr>
						<th>주소(우편번호)</th>
						<td><div class="ui input">
  							<input type="text" value="${ bgselectMap.balist[groupCode ==1].zipCode }">&nbsp;&nbsp;
  							</div>
  						</td>
  						<td><input type="hidden" id="groupCode" name="groupCode" value="1"readonly>
						<input type="hidden" id="groupCode" name="groupCode" value="0"readonly></td>
					</tr>
					<tr>
						<th>상세주소</th>
						<td colspan="4"><div class="ui input" style="width: 35%">
  							<input type="text" value="${ bgselectMap.balist[groupCode ==1].address }"></div>
						</td>
						<td><input type="hidden" id="groupCode" name="groupCode" value="1"readonly>
						<input type="hidden" id="groupCode" name="groupCode" value="0"readonly></td>
					</tr>
					
				</table>
				</form>
				<div class="buttonArea">
						<button class="ui blue button mini" id="bUpdate" type="button" onclick="ciUpdate()">저장</button>
					</div>
					
		
		</div>
		
				
		<div class="hru segment">
		<h3><i class="check square icon"></i>사업장 정보</h3>
				<table class="hru scroll table">
					<thead>
						<tr>
							<th>사업장코드</th>
							<th>사업장명</th>
							<th>사업장등록증상 법인명(상호)</th>
							<th>사업장 등록번호</th>
							<th>CEO</th>
						</tr>
					</thead>
					<tbody align="center">
						<c:forEach var="pay" items="${ bgselectMap.balist }">
						<tr class="trtrG">
							<td><c:out value="${ pay.groupCode }"/></td>
							<td><c:out value="${ pay.groupName }"/></td>
							<td><c:out value="${ pay.groupName }"/></td>
							<td><c:out value="${ pay.businessNumber }"/></td>
							<td><c:out value="${ pay.ceo }"/></td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
		</div>
		</div>
	</div>
	
	<script>
		/* 드롭다운 */
		$(function(){
			$("#system1").show(); 
			$(".ui.selection.dropdown").dropdown();
		});
		
		$(".trtrG").click(function(){
			$("#CCode").val($(this).children().eq(0).text());
			$("#CGroup").val($(this).children().eq(1).text());
			$("#CName").val($(this).children().eq(2).text());
			$("#CNumber").val($(this).children().eq(3).text());
		});

		//회사정보 수정
 		function ciUpdate(){
			var action = document.forms.companyForm;
			$("#companyForm").attr("action","updateCompanyInfo.st");
			$("#companyForm").submit();
		}  
	</script>
</body>
</html>