<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hru</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script src="resources/js/countUp.min.js"></script>
<style>
	.hru.segment{
		text-align:center;
	}
	.ui.big.statistic{
		width:30%;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
			<div class="hru segment" style="margin-bottom:0px">
				<div class="ui segment">
				<h2 style="text-align:left;"><i class="chart line icon"></i> 2020년 사원 현황</h2>
					<div class="ui big statistic">
						<div class="label" style="color:gray;">사원수</div>
						<div class="value" id="allMember"></div>
					</div>
					<div class="ui big statistic">
						<div class="label" style="color:gray;">입사자수</div>
						<div class="value" id="joinMember"></div>
					</div>
					<div class="ui big statistic">
						<div class="label" style="color:gray;">퇴사자수</div>
						<div class="value" id="leaveMember"></div>
					</div>
				
				<div class="hru segment chartjs-render-monitor" style="text-align: center;">
					<div class="chart-container" style="position: relative; height:400px; width:90%; margin:20px; display: inline-block;">
					  <canvas id="countM"></canvas>
					</div>
				</div>	
			</div>
		</div>
		<div class="hru segment">
				<div class="ui segment" style="width:49%; float:left; margin-top:0px;">
				<h2 style="text-align:left;"><i class="chart pie icon"></i> 직급통계</h2>
					<div class="chart-container" style="position: relative; height:400px; width:90%; margin:20px; display: inline-block;">
						 <canvas id="countpp"></canvas>
					</div>
				</div>
				<div class="ui segment" style="width:49%; float:right; margin-top:0px;">
				<h2 style="text-align:left;"><i class="signal icon"></i> 재직기간</h2>
					<div class="chart-container" style="position: relative; height:400px; width:90%; margin:20px; display: inline-block;">
						 <canvas id="workYear"></canvas>
					</div>
				</div>
		</div>
			
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		var options = {
			useEasing : true, 
			useGrouping : true, 
			separator : ',', 
			decimal : '.', 
			prefix : '', 
			suffix : '' 
		};
		var all = new CountUp("allMember", 0, ${countMember.get('allCount')}, 0, 2.5, options);
		var join = new CountUp("joinMember", 0, ${countMember.get('joinCount')}, 0, 2.5, options);
		var leave = new CountUp("leaveMember", 0, ${countMember.get('leaveCount')}, 0, 2.5, options);
		all.start();
		join.start();
		leave.start();
		
	
		//사원수 차트
		
		var ctx = document.getElementById("countM"); //캔버스 id값 가져오기
		var myChart = new Chart(ctx, {
		    type: 'line', //그래프 형태 지정하기
		    data: {
		        labels: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"], //X축 제목
		        datasets: [{
		            label: "입사자",
					fill: false,
		            data: [${enter.get('1')},${enter.get('2')},${enter.get('3')},${enter.get('4')},${enter.get('5')},${enter.get('6')},${enter.get('7')},${enter.get('8')},${enter.get('9')},${enter.get('10')},${enter.get('11')},${enter.get('12')}],
		            borderColor: [
		            	'rgba(0, 255, 255, 0.8)'
		            ],
		            backgroundColor: [
		                'rgba(0, 255, 255, 0.8)'
		            ],
		            borderWidth: 2
		        }, {
		        	label: "퇴사자",
		        	fill: false,
		            data: [${out.get('1')},${out.get('2')},${out.get('3')},${out.get('4')},${out.get('5')},${out.get('6')},${out.get('7')},${out.get('8')},${out.get('9')},${out.get('10')},${out.get('11')},${out.get('12')},],
		            borderColor: [
		            	'rgba(255, 55, 0, 0.8)'
		            ],
		            backgroundColor: [
		                'rgba(255, 55, 0, 0.8)'
		            ],
		            borderWidth: 2 //선굵기
		        }]
		    },
		    options: {
		    	maintainAspectRatio: false,
		        scales: { //X,Y축 옵션
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true  //Y축의 값이 0부터 시작
		                }
		            }]
		        }
		    }
		});

	
		//직위별
		var ctx = document.getElementById("countpp");
		var myChart = new Chart(ctx, {
		    type: 'doughnut',
		    data: {
		        labels: ["${countPP[0].get('ppName')}", "${countPP[1].get('ppName')}", "${countPP[2].get('ppName')}", "${countPP[3].get('ppName')}", "${countPP[4].get('ppName')}"], //X축 제목
		        datasets: [{
		            label: false,
		            data: ["${countPP[0].get('ppCount')}", "${countPP[1].get('ppCount')}", "${countPP[2].get('ppCount')}", "${countPP[3].get('ppCount')}", "${countPP[4].get('ppCount')}"],
		            backgroundColor: [
		                'rgba(255, 55, 0, 0.7)',
		                'rgba(151, 255, 44, 0.7)',
		                'rgba(0, 255, 255, 0.7)',
		                'rgba(255, 70, 140, 0.7)',
		                'rgba(255, 236, 0, 0.7)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		    	maintainAspectRatio: false,
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		})
	
		var ctx = document.getElementById("workYear");
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["2017", "2018", "2019", "2020"],
		        datasets: [{
		            label: "입사년도",
		            data: ["${enterYear.get('2017')}", "${enterYear.get('2018')}", "${enterYear.get('2019')}", "${enterYear.get('2020')}"],
		            backgroundColor: [
		                'rgba(34, 48, 116, 0.7)',
		                'rgba(34, 72, 141, 0.7)',
		                'rgba(34, 54, 208, 0.7)',
		                'rgba(0, 76, 255, 0.7)'
		            ],
		            borderColor: [
		            	'rgba(34, 48, 116, 1)',
		                'rgba(34, 72, 141, 1)',
		                'rgba(34, 54, 208, 1)',
		                'rgba(0, 76, 255, 1)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		    	maintainAspectRatio: false,
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		})
	</script>
</body>
</html>