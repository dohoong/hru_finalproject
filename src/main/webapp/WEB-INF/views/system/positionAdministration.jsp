<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hru</title>

<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div>
			<table>
				<tr>
					<td><h1 style="color:black">직급관리</h1></td>	
				</tr>
			</table>
		</div>
		<hr>
		<div class="hru segment">
				<h3><i class="check square icon"></i>구분</h3>
				<div class="scrollArea" style="width:40%;">
					<table class="hru scroll table">
						<thead>
							<tr>
								<th>직위</th>
								<th>최저 연봉(만원)</th>
								<th>최고 연봉(만원)</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="c" items="${ salistMap.applist }">
								<tr class="scroll-trOne">
									<td>${c.ppName}<input type="hidden" value="${c.ppCode}"></td>
									<td>${c.minSalary}</td>
									<td> ${c.maxSalary}</td>
									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

				<div class="editArea" style="width:40%;">
					<form method="post" id="PositionForm">
					<table class="hru table">
						
						<thead>
							<tr class="table-title">
								<th colspan="3" class="table-title">수 정</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>직위</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="wlrrmq" name="ppName"readonly></div></td>
							</tr>
							<tr>
								<th>최저 연봉</th>
								<td colspan="4"><div class="ui mini input">
								<input type="text" id="minSalary" name="minSalary">만원</div>
								</td>
							</tr>
							<tr>
								<th>최고 연봉</th>
								<td colspan="4"><div class="ui mini input">
								<input type="text" id="maxSalary" name="maxSalary">만원</div>
								<input type="hidden" name="ppCode" id="ppCode">
								</td>
								
							</tr>
						</tbody>
						
					</table>
					</form>
				<div class="buttonArea">
						<button class="ui blue button mini" id="add2" type="button">추가</button>
						<button class="ui blue button mini" id="complete2" style="display:none;" type="button" onclick="insert()">저장</button>
						<button class="ui blue button mini" id="save2"  type="button" onclick="updatePP()">저장</button>
						<button class="ui gray button mini" id="delete2" type="button" onclick="deletePP()">삭제</button>
					</div>
					
					</div>
					</div>
				<br><br>
			
			<table>
				<tr>
					<td colspan="1"><h1 style="color:black">호봉관리</h1></td>
				</tr>
			</table>
			<hr>
				<div class="hru segment" >
				<h3><i class="check square icon"></i>구분</h3>
				<div class="scrollArea" style="width:40%;">
					<table class="hru scroll table">
						<thead>
							<tr>
								<th>직급</th>
								<th>호봉</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="c" items="${salistMap.aplist }">
								<tr class="scroll-trTwo">
									<td>${c.ppName}</td>
									<td>${c.positionLevel}</td>

								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				

				
				<div class="editArea" style="width:40%;">
					<form method="post" id="positionForm2">
					<table class="hru table">
						<thead>
							<tr class="table-title">
								<th colspan="3" class="table-title">수 정</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>직급</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="wlrrmq2" name="positionName"></div></td>
							</tr>
							<tr>
								<th>호봉</th>
								<td colspan="4"><div class="ui mini input">
								<input type="text" id="Holevel"name="positionLevel"></div>
								<input type="hidden" name="positonCode"></td>
								
							</tr>
						</tbody>
					</table>
					</form>
					<br>
					
				<div class="buttonArea">
						<button class="ui blue button mini" id="add3" type="button">추가</button>
						<button class="ui blue button mini" id="complete3" style="display:none;" type="submit" onclick="insertPositon()">저장</button>
						<button class="ui blue button mini" id="save3" type="button" onclick="updateP()">저장</button>
						<button class="ui gray button mini" id="delete3" type="submit" onclick="deleteP()">삭제</button>
					</div>

					</div>
					</div>
				<br><br>
		
		
		<script>
			$(function(){
				$("#system2").show();
				$(".ui.dropdown").dropdown();
			});
			
			//행클릭1
			$(".scroll-trOne").click(function(){
				$("#wlrrmq").val($(this).find("td").eq(0).text()).attr("readonly",true);
				$("#minSalary").val($(this).find("td").eq(1).text()).attr("readonly",false);
				$("#maxSalary").val($(this).find("td").eq(2).text()).attr("readonly",false);
				$("#ppCode").val($(this).find("input[type='hidden']").val());
			})
			
			//행클릭2
			$(".scroll-trTwo").click(function(){
				$("#wlrrmq2").val($(this).find("td").eq(0).text()).attr("readonly",true);
				$("#Holevel").val($(this).find("td").eq(1).text()).attr("readonly",false);
			
			});
			
			
			//추가 버튼
			$("#add2").click(function(){
				$("#wlrrmq").val("").attr("readonly",false);
				$("#minSalary").val("").attr("readonly",false);
				$("#maxSalary").val("").attr("readonly",false);
				$("#add2").hide();
				$("#complete2").show();
				$("#save2").hide();
				$("#delete2").hide();
				
			});
			
			//추가버튼2
			$("#add3").click(function(){
				$("#wlrrmq2").val("").attr("readonly",false);
				$("#Holevel").val("").attr("readonly",false);
				$("#add3").hide();
				$("#complete3").show();
				$("#save3").hide();
				$("#delete3").hide();
				
			})
			

			
			//직위 추가
			function insert(){
				var action = document.forms.PositionForm;
				$("#PositionForm").attr("action","insertPreposition.st");
				$("#PositionForm").submit();
			}
			
			//직위 수정
			function updatePP(){
				var action = document.forms.PositionForm;
				$("#PositionForm").attr("action","updatePreposition.st");
				$("#PositionForm").submit();
			}
			
			//직위 삭제
			function deletePP(){
				var action = document.forms.PositionForm;
				$("#PositionForm").attr("action","deletePreposition.st");
				$("#PositionForm").submit();
			}
			
			//호봉 추가
			function insertPosition(){
				var action3 = document.forms.positionForm2;
				$("#positionForm2").attr("action3","insertPosition.st");
				$("#positionForm2").submit();
				
			}
			
			//호봉 수정
			function updatePosition(){
				var action7 = document.form.positionForm2;
				$("#positionForm2").attr("action7","updatePosition.st");
				$("#positionForm2").submit();
			}
			
		</script>



		
		
		
		
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->

</body>
</html>