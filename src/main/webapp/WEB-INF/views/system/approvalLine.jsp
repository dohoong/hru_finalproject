<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	#lTitle{
		width:200px;
		padding-bottom: 250px;
		text-align: center;
		font-weight:bold;
	}
	.hru.segment.approvalLine{
		margin-bottom:0.5%;
	}
	.inline{
		display:inline-block;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea" style="width:99%;"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
			<form action="updateApprovalLine.app" method="post">
			<h1>결재라인</h1>
			<div class="buttonArea">
			<input type="reset" class="ui button" value="다시하기" onclick="location.href='showApprovalLine.app'">
			<input type="submit" class="ui blue button" value="제출하기"></div>
			<div class="ui divider"></div>
			<c:forEach var="i" begin="0" end="${ablist.size()-1}">
				<c:if test="${i==0}"><h3 style="padding-left:20px;"><i class="check square icon"></i>인사관리</h3></c:if>
				<c:if test="${i==4}"><h3 style="padding-left:20px;"><i class="check square icon"></i>급여/근태관리</h3></c:if>
				<c:if test="${i==6}"><h3 style="padding-left:20px;"><i class="check square icon"></i>조직책임자/사원</h3></c:if>
				<div class="hru segment inline"  style="width:48%;">
					<input type="hidden" value="${ablist.get(i).approvalCode}" class="codeClass" name="ablist[${i}].approvalCode">
					<table class="hru table">
						<tr>
							<th rowspan="6" style="width:200px;"><c:out value="${ablist.get(i).approvalName}" /></th>
							<td colspan="2"><div class="ui checkbox">
								<c:if test="${ablist.get(i).status=='Y'}"><input type="checkbox" name="ablist[${i}].status" checked></c:if>
								<c:if test="${ablist.get(i).status=='N'}"><input type="checkbox" name="ablist[${i}].status"></c:if>
									<label>사용</label></div></td>
						</tr>
						<tr>
							<td>작성자 본인</td>
							<td><select class="ui dropdown" disabled>
							<c:if test="${i<4}"><option value="2">인사담당자</option></c:if>
							<c:if test="${i<6 && i>=4}"><option value="1">급여/근태담당자</option></c:if>
							<c:if test="${i>=6}"><option value="0">본인</option></c:if>
							</select></td>
						</tr>
						<tr>
							<td>1차 결재자</td>
							<td><input type="hidden" value="${ablist.get(i).firstApproval}" name="ablist[${i}].firstApproval">
							<select class="ui dropdown">
								<option value="">결재자 선택</option>
								<c:forEach var="j" begin="1" end="5">
									<c:if test="${ablist.get(i).firstApproval==j}">
										<option value="${j}" selected>
											<c:choose>
												<c:when test="${j==1}">급여담당자</c:when>
												<c:when test="${j==2}">인사담당자</c:when>
												<c:when test="${j==3}">조직책임자</c:when>
												<c:when test="${j==4}">인사팀장</c:when>
												<c:when test="${j==5}">회사대표</c:when>
											</c:choose>
										</option>
									</c:if>
									<c:if test="${ablist.get(i).firstApproval!=j}">
										<option value="${j}">
											<c:choose>
												<c:when test="${j==1}">급여담당자</c:when>
												<c:when test="${j==2}">인사담당자</c:when>
												<c:when test="${j==3}">조직책임자</c:when>
												<c:when test="${j==4}">인사팀장</c:when>
												<c:when test="${j==5}">회사대표</c:when>
											</c:choose>
										</option>
									</c:if>
								</c:forEach>
							</select>
							</td>
						</tr>
						<tr>
							<td>2차 결재자</td>
							<td><input type="hidden" value="${ablist.get(i).secondApproval}" name="ablist[${i}].secondApproval">
							<select class="ui dropdown">
								<option value="">결재자 선택</option>
								<c:forEach var="j" begin="3" end="5">
									<c:if test="${ablist.get(i).secondApproval==j}">
										<option value="${j}" selected>
											<c:choose>
												<c:when test="${j==3}">조직책임자</c:when>
												<c:when test="${j==4}">인사팀장</c:when>
												<c:when test="${j==5}">회사대표</c:when>
											</c:choose>
										</option>
									</c:if>
									<c:if test="${ablist.get(i).secondApproval!=j}">
										<option value="${j}">
											<c:choose>
												<c:when test="${j==3}">조직책임자</c:when>
												<c:when test="${j==4}">인사팀장</c:when>
												<c:when test="${j==5}">회사대표</c:when>
											</c:choose>
										</option>
									</c:if>
								</c:forEach>
							</select>
						</tr>
						<tr>
							<td>3차 결재자</td>
							<td><input type="hidden" value="${ablist.get(i).thirdApproval}" name="ablist[${i}].thirdApproval">
							<select class="ui dropdown">
								<option value="">결재자 선택</option>
								<c:forEach var="j" begin="4" end="5">
									<c:if test="${ablist.get(i).thirdApproval==j}">
										<option value="${j}" selected>
											<c:choose>
												<c:when test="${j==4}">인사팀장</c:when>
												<c:when test="${j==5}">회사대표</c:when>
											</c:choose>
										</option>
									</c:if>
									<c:if test="${ablist.get(i).thirdApproval!=j}">
										<option value="${j}">
											<c:choose>
												<c:when test="${j==4}">인사팀장</c:when>
												<c:when test="${j==5}">회사대표</c:when>
											</c:choose>
										</option>
									</c:if>
								</c:forEach>
							</select>
						</tr>
					</table>
				</div>
			</c:forEach>
			</form>
			
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function(){
			$("#system1").show();
			
			$('.ui.dropdown').dropdown();
			
			$("input[type=checkbox]").click(function(){
				if($(this).is(":checked")){
					$(this).parent().parent().parent().parent().find(".dropdown").not(":eq(0)").removeClass("disabled");
				} else{
					$(this).parent().parent().parent().parent().find(".dropdown").addClass("disabled");	
					$(this).parent().parent().parent().parent().find("input[type=hidden]").val(0);	
					$(this).parent().parent().parent().parent().find("div[class='text']").not(":eq(0)").text("결재자 선택").css("color", "lightgray");
					$(this).parent().parent().parent().parent().find("option").prop("selected", false);
				}
				
				
			});
			
			
			$("select").change(function(){
				//console.log("누른거 : " + $(this).val());
				//console.log($(this).parent().find("div[class='text']"));
				$(this).parent().find("div[class='text']").css("color", "black");
				
				
				var pressed = $(this).val();
				var previous = $(this).parent().parent().parent().prev().find("select").val();
				//console.log($(this).parent().find("div[class='text']"));
				//console.log("이전결재 : " + previous);
				$(this).parent().parent().parent().find("input[type=hidden]").val(pressed);

				if(previous==0 && $(this).parent().parent().parent().find("td:eq(0)").text()!="1차 결재자"){
					//alert("이전 결재자부터 선택해주세요!");
					Swal.fire({
						  icon: 'error',
						  title: '잘못된 접근',
						  text: '이전 결재자부터 선택해주세요!',
					});
					
					$(this).parent().find("div[class='text']").text("결재자 선택").css("color", "lightgray");
					$(this).parent().find("option").prop("selected", false);
					$(this).parent().parent().parent().find("input[type=hidden]").val("0");
				}
				
				if(pressed<=previous){
					//alert("이전 결재자보다 낮은 결재자를 선택할 수 없습니다!");
					Swal.fire({
						  icon: 'error',
						  title: '잘못된 접근',
						  text: '해당 결재자는 이전 결재자보다 높아야 합니다!',
					});
					//console.log($(this).parent().parent());
					$(this).parent().parent().parent().find("option").prop("selected", false);
					$(this).parent().find("div[class='text']").text("결재자 선택").css("color", "lightgray");
					$(this).parent().parent().parent().find("input[type=hidden]").val("0");

				}
				$(this).parent().parent().parent().next().find("div[class='text']").text("결재자 선택").css("color", "lightgray");
				$(this).parent().parent().parent().next().find("option").prop("selected", false);
				$(this).parent().parent().parent().next().next().find("div[class='text']").text("결재자 선택").css("color", "lightgray");
				$(this).parent().parent().parent().next().next().find("option").prop("selected", false);
				
			});
			
		});
		 

	</script>
</body>
</html>