<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>건의 사항 게시판</title>
<jsp:include page="../../common/importlib.html" />
<!-- 상대경로 잘 지정해야함! -->
<style>
.btnAdd {
	float: right;
}

.board2 tbody tr:hover {
	background: #eee;
	cursor: pointer;
}

.btnInsert {
	float: right;
}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<!-- 상대경로 잘 지정해야함! -->
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<!-- 여기에 내용부분 작성! -->
			<h2>건의 사항 게시판</h2>
			<div class="hru segment">
				<div class="searchArea">
					<table align="center" cellspacing="10px">
						<tr>
							<td><h3>검색어</h3></td>
							<td><select class="ui dropdown divCategory"
								id="searchCondition" name="searchCondition">
									<option value="" selected disabled hidden>카테고리</option>
									<option value="title">제목</option>
									<option value="writer">작성자</option>
							</select></td>
							<td>
								<div class="ui icon input">
									<input type="search" size="30" id="searchValue"
										name="searchValue" placeholder="Search..."><i
										class="search icon"></i>
								</div>
							</td>
							<td>
								<button onclick="searchPbBoard();" class="ui blue button">검색하기</button>
							</td>
						</tr>
					</table>
				</div>
				<!-- searchArea end -->
				<script>
					function searchPbBoard() {
						var searchCondition = $("#searchCondition").val();
						var searchValue = $("#searchValue").val();

						location.href = "searchPbList.bo?searchCondition="
								+ searchCondition + "&searchValue="
								+ searchValue;
					}

					$(function() {
						$(".ui.dropdown").dropdown();
					});
				</script>

				<div class="hru segment" style="width: 100%;">
					<table id="tblPb" class="hru board2" border="1">
						<thead>
							<tr>
								<th>글 번호</th>
								<th>제목</th>
								<th>작성자</th>
								<th>조회수</th>
								<th>날짜</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="pb" items="${ list }">
								<tr>
									<input type="hidden" value="${mid}">
									<td><c:out value="${pb.proposalid }" /></td>
									<td><c:out value="${pb.title}" /></td>
									<td><c:out value="${pb.mname}" /></td>
									<td><c:out value="${pb.count}" /></td>
									<td><c:out value="${pb.createdate}" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<!-- 테이블영역 끝! -->
				</div>
			</div>
			<div class="pagingArea">
				<!-- <div class="paging" style="margin-right:15px;"><i class="angle double left icon"></i></div> -->
				<c:if test="${ pi.currentPage <= 1 }">
					<div class="paging-disabled" style="margin-right: 15px;">
						<i class="angle double left icon" style="color: white;"></i>
					</div>
				</c:if>
				<c:if test="${ pi.currentPage > 1 }">
					<c:url var="pageCheck0" value="selectPbList.bo">
						<c:param name="currentPage" value="${pi.currentPage -1}" />
					</c:url>
					<div class="paging" onclick="location.href='${pageCheck0}'"
						style="margin-right: 15px;">
						<i class="angle double left icon"></i>
					</div>
				</c:if>

				<c:forEach var="p" begin="${pi.startPage}" end="${pi.endPage}">
					<c:if test="${p eq pi.currentPage}">
						<div class="paging-disabled">${p}</div>
					</c:if>
					<c:if test="${p ne pi.currentPage}">
						<c:url var="pageCheck" value="selectPbList.bo">
							<c:param name="currentPage" value="${ p }" />
						</c:url>
						<div class="paging" onclick="location.href='${pageCheck}'">${p}</div>
					</c:if>
				</c:forEach>

				<!-- <div class="paging" style="margin-left:15px;"><i class="angle double right icon"></i></div> -->
				<c:if test="${ pi.currentPage >= pi.maxPage }">
					<div class="paging-disabled" style="margin-left: 15px;">
						<i class="angle double right icon" style="color: white;"></i>
					</div>
				</c:if>
				<c:if test="${pi.currentPage < pi.maxPage}">
					<c:url var="pageCheck2" value="selectPbList.bo">
						<c:param name="currentPage" value="${pi.currentPage+1}" />
					</c:url>
					<div class="paging" onclick="location.href='${pageCheck2}'"
						style="margin-left: 15px;">
						<i class="angle double right icon"></i>
					</div>
				</c:if>
			</div>
		</div>

	</div>

	<div class="btnArea">
		<button class="ui blue button btnInsert"
			onclick="location.href='selectInsertPbList.bo'">글 작성</button>
	</div>
	<!-- 내용 끝 -->

	<!-- outer end -->
	<script>
		$(function() {
			$("#member4").show();
		});

		$(function() {
			$("#tblPb")
					.find("td")
					.click(
							function() {
								var proposalid = $(this).parent()
										.children("td").eq(0).text();
								var mid = $(this).parent().children("input")
										.eq(0).val();

								location.href = "selectOnePbListwithComment.bo?proposalid="
										+ proposalid + "&mid=" + mid;

							});
		})
	</script>
</body>
</html>