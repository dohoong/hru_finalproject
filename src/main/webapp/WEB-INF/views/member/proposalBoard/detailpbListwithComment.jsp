<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>건의 사항 상세보기(댓글)</title>
<style>
.insertArea {
	width: 100%;
	height: 120px;
	margin-top: -40px;
	margin-left: 10px;
	float: left !important;
	resize: none;
	border: 1px solid black;
}

.btnArea {
	margin-top: 20px;
	float: right;
}

#addReply {
	float: right;
}

.comments {
	padding-top: 150px;
}

.btnArea2 {
	margin: auto;
	width: 10%;
	margin-left: 750px;
}

.btnArea2 button {
	font-size: 7px !important;
}

.tblApply {
	float: left;
	width: 100%;
	height: 50%;
}

#replyContent {
	width: 100%;
	margin-top: 10px;
	margin-left: 10px;
	resize: none;
	border: 1px solid black;
}

.titleRep {
	padding-top: 130px;
}
</style>
<jsp:include page="../../common/importlib.html" />
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="outer">
		<!--outer Area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<div class="hru segment">
				<h1>
					<i class="check square icon"></i>건의 사항 댓글 달기
				</h1>
				<table class="hru table">
					<tr>
						<th>작성자</th>
						<td><c:out value="${p.mname}" /></td>
						<input type="hidden" id="memberid" value="${mid}">
						<input type="hidden" id="proposalid" value="${p.proposalid }">
						<th>등록일</th>
						<td><c:out value="${p.createdate }" /></td>
					</tr>
					<tr>
						<th>제목</th>
						<td><c:out value="${p.title }" /></td>
						<td colspan="3"></td>
					</tr>
				</table>
			</div>
			<div>
				<div>
					<div class="insertArea">${p.content }</div>
				</div>
				<c:if test="${p.mid eq mid}">
					<div class="btnArea">
						<button id="btnUpdate" class="ui blue button"
							onclick="hrefUpdate();">수정</button>
						<button type="reset" class="ui blue button"
							onclick="location.href='selectPbList.bo'">취소</button>
					</div>
				</c:if>
			</div>

			<!--댓글 영역 시작-->
			<form action="insertReply.bo" method="post">
				<div class="titleRep">
					<h1>
						<i class="check square icon"></i>댓글 달기
					</h1>
				</div>
				<table align="center" class="tblApply">
					<tr>
						<input type="hidden" name="proposalid" value="${p.proposalid }">
						<td><textarea id="replyContent" class="content2"
								name="content2"></textarea>
							<button class="ui blue button mini content2" type="submit"
								id="addReply">등록하기</button></td>
					</tr>
					<tr>
						<td colspan="2"><b>댓글 </b></td>
					</tr>
				</table>
			</form>

			<table align="center">
				<c:forEach var="pc" items="${ list2 }">
					<tr>
						<input type="hidden" class="procommentid" name="procommentid"
							value="${pc.procommentid}">
						<input type="hidden" class="proposalid2" name="proposalid"
							value="${p.proposalid}">
						<%-- <c:choose>
							<c:when test="${pc.mid eq mid}">
								<td width="100px"><c:out value="${mname}" /></td>
							</c:when>
							<c:otherwise> --%>
								<td width="100px"><c:out value="${pc.mname2}" /></td>
						<%-- 	</c:otherwise>
						</c:choose> --%>
						<c:choose>
							<c:when test="${p.mid eq mid}">
								<td width="300px"><input type="text" class="content2"
									name="content2" value="${pc.content2 }"></td>
							</c:when>
							<c:otherwise>
								<td width="300px"><c:out value="${pc.content2}" /></td>
							</c:otherwise>
						</c:choose>
						<td width="100px"><c:out value="${pc.createdate2}" /></td>
						<td><div class="btnArea2">
								<button class="ui blue button mini replyUpdate">수정</button>
								<button class="ui blue button mini replyDelete">삭제</button>
							</div></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<script>
		$("#rangestart").calendar({
			type : 'date',
			endCalendar : $('#rangeend'),
			formatter : {
				date : function(date, settings) {
					if (!date)
						return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					return year + '-' + month + '-' + day;
				}
			}
		});
		$("#rangeend").calendar({
			type : 'date',
			startCalendar : $('#rangestart'),
			formatter : {
				date : function(date, settings) {
					if (!date)
						return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					return year + '-' + month + '-' + day;
				}
			}
		});

		$(function() {
			$("#member4").show();
		});

		function hrefUpdate() {

			var mid = $("#memberid").val();
			var proposalid = $("#proposalid").val();

			location.href = "selectOnePbList.bo?mid=" + mid + "&proposalid="
					+ proposalid;
		}

		$(function() {
			$(".replyUpdate").click(
					function() {
						var procommentid = $(this).parent().parent().parent()
								.children("input").eq(0).val();
						var proposalid = $(this).parent().parent().parent()
								.children("input").eq(1).val();
						var mid = $
						{
							mid
						}
						;
						var content2 = $(this).parent().parent().parent()
								.children("td").children("input").eq(0).val();

						location.href = "replyUpdate.bo?procommentid="
								+ procommentid + "&proposalid=" + proposalid
								+ "&mid=${mid}" + "&content2=" + content2;
					});

		})

		$(function() {
			$(".replyDelete").click(
					function() {
						var procommentid = $(this).parent().parent().parent()
								.children("input").eq(0).val();
						var proposalid = $(this).parent().parent().parent()
								.children("input").eq(1).val();

						location.href = "replyDelete.bo?procommentid="
								+ procommentid + "&proposalid=" + proposalid
								+ "&mid=${mid}";
					});
		})
	</script>
</body>
</html>