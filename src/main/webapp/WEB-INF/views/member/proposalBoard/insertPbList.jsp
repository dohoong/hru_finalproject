<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>건의 사항 작성</title>
<style>
.insertArea {
	width: 100%;
	height: 300px;
	resize: none;
}

.btnArea {
	float: right;
}

.title {
	width: 350px;
}
</style>
<jsp:include page="../../common/importlib.html" />
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="outer">
		<!--outer Area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<div class="hru segment">
				<h1>
					<i class="check square icon"></i>건의 사항 작성
				</h1>
				<form action="insertPbList.bo" method="post">
					<table class="hru table">
						<tr>
							<th>작성자</th>
							<input type="hidden" name="mid" value="${mid }">
							<td><c:out value="${ list[0].mname}" /></td>
							<th>등록일</th>
							<td><c:set var="now" value="<%=new java.util.Date()%>" /> <c:set
									var="sysYear">
									<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" />
								</c:set>
								<c:out value="${sysYear}" /></td>
						</tr>
						<tr>
							<th>제목</th>
							<td colspan="3"><div class="ui mini input">
									<input type="text" id="leader" class="title" name="title"
										placeholder="제목을 입력하시오.">
								</div></td>
						</tr>
					</table>
					<textarea class="insertArea" name="content"
						placeholder="내용을 입력하시오."></textarea>
					<div class="btnArea">
						<button type="submit" class="ui blue button">등록</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- 내정보 끝 -->
	<script>
		$(function() {
			$("#member4").show();
		});
	</script>
</body>
</html>