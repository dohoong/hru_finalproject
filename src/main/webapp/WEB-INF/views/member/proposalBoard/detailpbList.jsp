<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>건의 사항 상세보기</title>
<style>
.insertArea {
	width: 1100px;
	height: 300px;
	display: block !important;
	resize: none;
}

.divInsert {
	margin-top: 30px;
}

.btnArea {
	margin-top: 20px;
	float: right;
}

.title {
	width: 350px;
}
</style>
<jsp:include page="../../common/importlib.html" />
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="outer">
		<!--outer Area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<div class="hru segment">
				<h1>
					<i class="check square icon"></i>건의 사항 수정하기
				</h1>
				<form id="updateForm" method="post">
					<table class="hru table2">
						<tr>
							<th>작성자</th>
							<td><c:out value="${list[0].mname }"/></td>
							<input type="hidden" name="proposalid" value="${list[0].proposalid }">
							<th>등록일</th>
							<td><c:set var="now" value="<%=new java.util.Date()%>" /> <c:set
									var="sysYear">
									<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" />
								</c:set> <c:out value="${sysYear}" /></td>
						</tr>
						<tr>
							<th>제목</th>
							<td colspan="3"><div class="ui mini input">
									<input type="text" id="leader" class="title" name="title"
										value="${list[0].title }">
								</div></td>
						</tr>
					</table>
					<div class="divInsert">
						<textarea class="insertArea" name="content"><c:out
								value="${list[0].content }" /></textarea>
					</div>
					<div class="btnArea">
						<button type="submit" class="ui blue button"
							onclick="updateInfoforPb();">수정</button>
						<button type="submit" class="ui blue button"
							onclick="deleteInfoforPb();">삭제</button>
					</div>
			</form>
			</div>
		</div>
	</div>
	<!-- 내정보 끝 -->
	<script>
		$("#rangestart").calendar({
			type : 'date',
			endCalendar : $('#rangeend'),
			formatter : {
				date : function(date, settings) {
					if (!date)
						return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					return year + '-' + month + '-' + day;
				}
			}
		});

		$(function() {
			$("#member4").show();
		});

		function updateInfoforPb() {
			$("#updateForm").attr("action", "infoUpdateforPb.bo");
		}

		function deleteInfoforPb() {
			$("#updateForm").attr("action", "infoDeleteforPb.bo");
		}
	</script>
</body>
</html>