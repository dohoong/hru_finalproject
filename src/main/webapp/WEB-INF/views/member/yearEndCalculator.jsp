<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<style>
	/* .hru.segment td input{
		text-align: center;
	} */
	.hru.table td{
		height: 50px;
	}
	.hru.table.second{
		text-align: center;
	}
	.hru.table.second th{
		text-align: center;
	}
	.yefile:hover{
		text-decoration:underline;
		cursor:pointer;
	}
</style>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment" style='width:99%'>
			<h1 style="margin-bottom:50px;"><i class='check square icon'></i>연말정산</h1>
			<!-- <div class="ui divider"></div> -->
			<div class="ui top attached tabular menu">
				<a class="active item" data-tab="first">연말정산 기간</a>
				<a class="item" data-tab="third">연말정산 자료 제출</a>
				<a class="disabled item" data-tab="second">연말정산 간이 계산</a> 
			</div>	
			
		<!-- 연말정산 view start -->
		<div class="ui bottom active attached tab segment" data-tab="first">
			<div class="ui message">
				<ul class="list">
					<li>연말정산에만 이용할 수 있는 메뉴입니다. 일정을 확인하셔서 이용해주시기 바랍니다.</li>
					<li>연말정산 간소화 서비스 사이트 : <a href="https://www.hometax.go.kr/websquare/websquare.wq?w2xPath=/ui/pp/index_pp.xml">http://www.hometax.go.kr</a></li>
				</ul>
			</div>
			<c:if test="${!empty yeMap}">
				<table class="hru table">
					<tr>
						<th style="width:350px;">연말정산 실시</th>
						<td style="width:200px;"><div class="ui disabled calendar" id="start1"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="startDate" value="${yeMap.get('startDate')}" readonly></div></div></td>
						<td style="width:100px; text-align:center;"><label class="hru label">~</label></td>
						<td style="width:200px;"><div class="ui disabled calendar" id="end1"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="endDate" value="${yeMap.get('endDate')}" readonly></div></div></td>
						<td></td>
					</tr>
					<tr>
						<th>연말정산 간소화 서비스 자료 확인 및 제출 기한</th>
						<td style="width:200px;"><div class="ui disabled calendar" id="start2"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="documentStart" value="${yeMap.get('documentStart')}" readonly></div></div></td>
						<td style="width:100px; text-align:center;"><label class="hru label">~</label></td>
						<td style="width:200px;"><div class="ui disabled calendar" id="end2"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="documentEnd" value="${yeMap.get('documentEnd')}" readonly></div></div></td>
					</tr>
					<tr>
						<th>추가자료 제출 기한</th>
						<td style="width:200px;"><div class="ui disabled calendar" id="start3"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" value="${yeMap.get('documentStart')}" readonly></div></div></td>
						<td style="width:100px; text-align:center;"><label class="hru label">~</label></td>
						<td style="width:200px;"><div class="ui disabled calendar" id="end3"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" value="${yeMap.get('documentEnd')}" readonly></div></div></td>
						<td></td>
					</tr>
				</table>
			</c:if>
			<c:if test="${empty yeMap}">
				<h2 style="color:lightgray; padding:50px; text-align:center;"><label><i class="exclamation circle icon"></i><c:out value="연말정산 기간이 아닙니다."/></label></h2>
			</c:if>
		</div>
		<div class="ui bottom attached tab segment" data-tab="second">
			<div class="hru segment">
			<h3><i class="check square icon"></i>급여 및 예상 세액</h3>
			<div style="color: lightgray; float: right">(단위 : 원)</div>
			 <table class="hru table"><!-- 급여 및 예상 세액 -->
			 	<tr>
			 		<th><c:out value="① 총급여"></c:out></th>
			 		<td><div class="ui input"><input type="text" value="40,000,000" readonly></div></td>
			 		<th><c:out value="② 근로소득금액"></c:out></th>
			 		<td><div class="ui input"><input type="text"></div></td>
			 	</tr>
			 	<tr>
			 		<th><c:out value="③ 과세표준"></c:out></th>
			 		<td><div class="ui input"><input type="text"></div></td>
			 		<th><c:out value="④ 산출세액"></c:out></th>
			 		<td><div class="ui input"><input type="text"></div></td>
			 	</tr>
			 	<tr>
			 		<th><c:out value="⑤ 결정세액"></c:out></th>
			 		<td><div class="ui input"><input type="text"></div></td>
			 		<th><c:out value="⑥ 기납부 소득세액(먼저 낸 세금)"></c:out></th>
			 		<td><div class="ui input"><input type="text" value="3,120,000" readonly></div></td>
			 	</tr>
			 	<tr>
			 		<th><c:out value="⑦ 차감징수납부(환급)예상세액(⑤-⑥)"></c:out></th>
			 		<td><div class="ui input"><input type="text"></div></td>
			 		<th><c:out value="⑧ 농어촌특별세"></c:out></th>
			 		<td><div class="ui input"><input type="text"></div></td>
			 	</tr>
			 </table><!-- 급여 및 예상 세액 -->
			 <br>
			 <button class="ui blue button" style="float: right">계산하기</button>
			 <br>
			</div>
			<h3><i class="check square icon"></i>소득공제</h3>
			<div style="color: lightgray; float: right;">(단위 : 원)</div>
			<table class="hru table"><!-- 소득공제 테이블 시작 -->
				<thead>
					<tr>
						<th colspan="2" style="width: 100px;"><c:out value="공제항목명"></c:out></th>
						<th><c:out value="사용(납입)금액"></c:out></th>
						<th><c:out value="소득공제액(①)"></c:out></th>
						<th><c:out value="공제한도(②)"></c:out></th>
						<th><c:out value="한도미달액(②-①)"></c:out></th>
					</tr>
				</thead>
				<tbody>
					<tr> 
						<td style="font-weight: bold;"><c:out value="인적공제"></c:out></td>
						<td><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr>
						<td colspan="2"><c:out value=" - 기본공제"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr>
						<td colspan="2"><c:out value=" - 추가공제"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr>
						<td style="font-weight: bold;"><c:out value="연금보험료 공제"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr>
						<td style="font-weight: bold;" colspan="2"><c:out value="특별소득공제"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr>
						<td><c:out value=" - 건강보험료 등"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>			
					</tr>
					<tr>
						<td><c:out value=" - 주택자금"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>			
					</tr>
					<tr>
						<td colspan="2"><c:out value=" - 주택임차 차입금 원리금 상환액 대출기관"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>			
					</tr>
					<tr>
						<td colspan="2"><c:out value=" - 주택임차  차입금 원리금 상환액 거주자"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>			
					</tr>
					<tr>
						<td colspan="2"><c:out value=" - 장기주택 저당차입금 이자상환액"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>			
					</tr>
					<tr>
						<td><c:out value=" - 기부금(이월분)"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>			
					</tr>
					<tr>
						<td style="font-weight: bold;" colspan="2"><c:out value="그 밖의 소득공제"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr>
						<td ><c:out value=" - 개인연금저축"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="720,000"></c:out></td>
						<td><c:out value="720,000"></c:out></td>
					</tr>
					<tr>
						<td ><c:out value=" - 소기업 ,소상공인 공제부금"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="5,000,000"></c:out></td>
						<td><c:out value="5,000,000"></c:out></td>
					</tr>
					<tr>
						<td ><c:out value=" - 주택마련저축"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr>
						<td ><c:out value=" - 투자조합출자 등"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="14,375,000"></c:out></td>
						<td><c:out value="14,375,000"></c:out></td>
					</tr>
					<tr>
						<td ><c:out value=" - 신용카드 등"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="3,000,000"></c:out></td>
						<td><c:out value="3,000,000"></c:out></td>
					</tr>
					<tr>
						<td ><c:out value=" - 우리사주조합 출연금"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="15,000,000"></c:out></td>
						<td><c:out value="15,000,000"></c:out></td>
					</tr>
					<tr>
						<td><c:out value=" - 고용유지 중소기업 근로자"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="10,000,000"></c:out></td>
						<td><c:out value="10,000,000"></c:out></td>
					</tr>
					<tr>
						<td><c:out value=" - 장기집합투자증권저축"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="2,400,000"></c:out></td>
						<td><c:out value="2,400,000"></c:out></td>
					</tr>
					<tr style="background: #e0e0e0">
						<td colspan="2" style="text-align: center;"><c:out value="소계"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value=""></c:out></td>
						<td><c:out value=""></c:out></td>
					</tr>
				</tbody>
			</table><!-- 소득공제 테이블 종료 -->
			<BR><bR><BR><BR>
			<h3><i class="check square icon"></i>세액감면 ・ 세액공제</h3>
			<table class="hru table"><!-- 세액감면-세액공제 테이블 시작 -->
				<thead>
					<tr>
						<th colspan="2" style="width: 100px;"><c:out value="공제항목명"></c:out></th>
						<th><c:out value="사용(납입)금액"></c:out></th>
						<th><c:out value="소득공제액(①)"></c:out></th>
						<th><c:out value="공제한도(②)"></c:out></th>
						<th><c:out value="한도미달액(②-①)"></c:out></th>
					</tr>
				</thead>
				<tbody>
					<tr> 
						<td style="font-weight: bold;"><c:out value="세액감면"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td style="font-weight: bold;"colspan="2"><c:out value="근로소득"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="684,000"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td style="font-weight: bold;"><c:out value="자녀"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td colspan="2"><c:out value=" - 자녀"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td colspan="2"><c:out value=" - 출생입양"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td style="font-weight: bold;"><c:out value="연금계좌"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td colspan="2"><c:out value=" - 퇴직연금계좌"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td colspan="2"><c:out value=" - 연금저축계좌"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td style="font-weight: bold;" colspan="2"><c:out value="특별세액공제"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td ><c:out value=" - 보장성보험료"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td ><c:out value=" - 의료비"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td ><c:out value=" - 교육비"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td ><c:out value=" - 기부금"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td colspan="2"><c:out value=" - 표준세액공제"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td style="font-weight: bold;"><c:out value="납세조합공제"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td style="font-weight: bold;"><c:out value="주택차입금"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td style="font-weight: bold;"><c:out value="외국납부세액"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr> 
						<td style="font-weight: bold;"><c:out value="월세액"></c:out></td>
						<td ><button class="ui icon button" ><i class="plus icon"></i></button></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="0"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="-"></c:out></td>
					</tr>
					<tr style="background: #e0e0e0">
						<td colspan="2" style="text-align: center;"><c:out value="소계"></c:out></td>
						<td><c:out value="-"></c:out></td>
						<td><c:out value="684,000"></c:out></td>
						<td><c:out value=""></c:out></td>
						<td><c:out value=""></c:out></td>
					</tr>
				</tbody>	
			</table><!-- 세액감면-세액공제 테이블 종료 -->
			<br>
			<button class="ui blue button" style="float: right">계산하기</button>
			<br><br>
		</div>
		<div class="ui bottom attached tab segment" data-tab="third">
		<c:if test="${!empty yeMap}">
		<br><h2 style="margin-left: 30px;"><i class="check square icon"></i>연말정산 자료제출</h2>
			<div style="width: 95%; padding-left:80px; margin-bottom:50px;">
				<form method="post" action="insertDocument.me" enctype="multipart/form-data">
				<c:if test="${!empty yearEnd}"><input type="hidden" value="${yearEnd.get('yearendId')}" name="yearendId"></c:if>
				<div style="display:inline-block; width:90%;">
					<h3>연말정산 공제 증명서류 제출</h3>
					<div class="ui icon input" id="FileDiv">
						<c:set var="cnt" value="0"/>
						<c:if test="${!empty yearEnd.get('attlist') }">
						<c:forEach var="i" begin="0" end="${yearEnd.get('attlist').size()-1}">
							<c:if test="${yearEnd.get('attlist').get(i).getFileLevel()==1}"><c:set var="cnt" value="1"/></c:if>
						</c:forEach>
						</c:if>
						<c:if test="${cnt==1}"><input type="text" id="fileBtn" placeholder="파일명" style="width:400px" disabled></c:if>
						<c:if test="${cnt==0}">
						<input type="text" id="fileBtn" placeholder="파일명" style="width:400px" readonly></c:if>
						<i class="upload icon"></i>
						<input type="file" id="inputFile" name="inputFile">
					</div><!-- 첨부파일 끝! -->
					<h3>기타서류 증빙 제출</h3>
					<div class="ui icon input" id="FileDiv">
						<input type="text" id="fileBtn2" placeholder="파일명" style="width:400px" readonly>
						<i class="upload icon"></i>
						<input type="file" id="inputFile2" name="inputFile2">
					</div><!-- 첨부파일 끝! -->
					<br><br>
					<div>
						<!-- <i class="exclamation circle icon" id="exclamation"></i> --><c:out value="- 연말정산 관련 서류 제출 페이지입니다."></c:out><br>
						<c:out value="- 부적절한 파일을 제출시 불이익을 받을 수 있습니다."></c:out><br>
						<c:out value="- 제출하기 버튼을 클릭시 첨부한 파일이 회계팀에게 일괄 전송됩니다."></c:out><br>
						<c:out value="- 공지사항에 공지된 기간이 지나면 연말정산 카테고리는 비활성화 되고 그 이외의 사항은 회계부서에 문의 바랍니다."></c:out><br>
						<c:out value="- 자료제출 목록에서 파일을 삭제할 수 있으며  , 공지사항에 공지된 기간 안에서는 제출 파일을 삭제 , 추가를 할 수 있습니다."></c:out><br>
					</div>
				</div>
				<div style="display:inline-block;"><button class="ui blue button" style="float: right">제출하기</button></div>
				</form>
			</div>
			<div class="ui divider"></div>
			<h2 style="margin-left: 30px;"><i class="check square icon"></i>연말정산 자료제출 목록</h2>
			<br>
			<table class="hru table second">
				<thead>
					<tr>
						<th><c:out value="File No"></c:out></th>
						<th><c:out value="성명"></c:out></th>
						<th><c:out value="제출서류"></c:out></th>
						<th><c:out value="구분"></c:out></th>
						<th><c:out value="삭제"></c:out></th>
					</tr>
					<c:if test="${!empty yearEnd.get('attlist')}">
					<c:forEach begin="0" end="${yearEnd.get('attlist').size()-1}" var="i">
						<tr>
							<td><c:out value="${yearEnd.get('attlist').get(i).getAttachmentId()}"></c:out></td>
							<td><c:out value="${sessionScope.loginUser.getmName()}"></c:out></td>
							<td><div class="yefile"><c:out value="${yearEnd.get('attlist').get(i).getOriginName()}"></c:out></div></td>
							<td><c:if test="${yearEnd.get('attlist').get(i).getFileLevel() == 1}"><c:out value="증명서류"/></c:if>
								<c:if test="${yearEnd.get('attlist').get(i).getFileLevel() == 0}"><c:out value="기타서류"/></c:if></td>
							<td><input type="hidden" value="${yearEnd.get('attlist').get(i).getAttachmentId()}">
							<button class="ui blue button mini removeAtt">삭제하기</button></td>
						</tr>
					</c:forEach>
					</c:if>
				</thead>
			</table>
			</c:if>
		</div>	
		<br>
		<Br>
		
		
		</div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function(){
			/* 상단메뉴 */
			$("#member3").show();
			/* 탭메뉴 */
			
			$(".menu .item").click(function(){
				var start = new Date("${yeMap.get('startDate')}");
				var end = new Date("${yeMap.get('endDate')}");
				var now = new Date();
				if(now > start && now < end){
					$(this).tab();
				} else{
					Swal.fire({
						  title: '잘못된 접근',
						  text: "지금은 연말정산 기간이 아닙니다.",
						  icon: 'error',
						  confirmButtonText: '확인'
					});
				}
			});
			
			$(".removeAtt").click(function(){
				var attid = $(this).parent().find("input[type=hidden]").val();
				console.log("attid : " +attid);
				location.href="deleteAttforYE.me?attachmentId=" + attid;
			});
			
			$("#start1").calendar({
				type:'date'
			});
			$("#end1").calendar({
				type:'date'
			});

			$("#start2").calendar({
				type:'date'
			});
			$("#end2").calendar({
				type:'date'
			});

			$("#start3").calendar({
				type:'date'
			});
			$("#end3").calendar({
				type:'date'
			});
		});
		
		$("#fileBtn").click(function() {
			$(this).parent().find("input:file").click();
		});
		$("#fileBtn2").click(function() {
			$(this).parent().find("input:file").click();
		});

		$("input:file[name='inputFile']").on('change', function(e) {
			var name = null;
			if(e.target.files[0] != null) {
				console.log(e.target.files[0]);
				name = e.target.files[0].name;
			}
			$("input:text[id='fileBtn']").val(name);
		});
		$("input:file[name='inputFile2']").on('change', function(e) {
			var name = null;
			if(e.target.files[0] != null) {
				console.log(e.target.files[0]);
				name = e.target.files[0].name;
			}
			$("input:text[id='fileBtn2']").val(name);
		});
	</script>
</body>
</html>