<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	.hru.segment.quitMoney{
		margin-bottom:10px;
	}
</style>
</head>
<body>
<jsp:include page="../common/menubar.jsp"/>
<div class="outer"><!--outer Area -->
<div class="contentArea"> <!-- 내용부분! -->
		<div class="ui top attached tabular menu">
				<a class="active item" data-tab="first">퇴직금 조회</a> 
				<a class="item" data-tab="second" onclick="quitApply()">퇴직 신청</a>
		</div>
		
		<!-- 퇴직금 조회 -->
		<div class="ui bottom attached active tab segment" data-tab="first">
			<!-- 상단 정보 테이블 -->
			<div class="hru segment quitMoney">
			
				<table class="hru table" id="QuitBasic">
					<tr>
						<th>성명</th>
						<td colspan="3"><c:out value="${qList.mName }"/></td>
						<th>사원번호</th>
						<td colspan="3"><c:out value="${qList.mid }"/></td>
					</tr>
					<tr>
						<th>입사일</th>
						<td><div class="ui small icon input">
							  <input type="text" value="${qList.enterDate }" readonly>
							</div></td>
						<th>퇴사일</th>
						<td><div class="ui small icon input">
							  <input type="text" value="${qList.quitDate }" readonly>
							</div></td>
						<th>퇴직금 지급일</th>
						<td><div class="ui small icon input">
							  <input type="text" value="${qList.plusMonth }" readonly>
							</div></td>
						<th>근속기간</th>
						<td><div class="ui small icon input">
							  <input id="employeeDate" type="text" value="${qList.employeeDate }" readonly>
							</div></td>
					</tr>
				</table>
				<br>
				<button class="ui mini blue button" style="float: right;" onclick="quitCalculation()">계산하기</button>
				
			</div><!-- 상단 정보 테이블 끝-->
			
			<!-- 퇴직전 3개월 임금총액 계산내역 -->
			<div class="hru segment quitMoney">
				<h3><i class="check square icon"></i>퇴직전 3개월 임금총액 계산내역</h3>
				<table class="hru board2" border="1">
					<thead>
						<tr>
							<th>퇴직금 근속기간</th>
							<th>일수</th>
							<th>기본급</th>
							<th>기타수당</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><div class="ui small icon input">
							 	 <input id="startOne" type="text" value="" readonly>
							 	 ~ 
							 	 <input id="endOne" type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
							 	 <input id="periodOne" type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
								  <input id="basicSalaryOne" type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
								  <input id="plusSalaryOne" type="text" value="" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<td><div class="ui small icon input">
							 	 <input id="startTwo" type="text" value="" readonly>
							 	 ~ 
							 	 <input id="endTwo"type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
							 	 <input id="periodTwo" type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
								  <input id="basicSalaryTwo" type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
								  <input id="plusSalaryTwo" type="text" value="" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<td><div class="ui small icon input">
							 	 <input id="startThree"type="text" value="" readonly>
							 	 ~ 
							 	 <input id="endThree" type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
							 	 <input  id="periodThree" type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
								  <input id="basicSalaryThree" type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
								  <input id="plusSalaryThree" type="text" value="" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<td>계</td>
							<td><div class="ui small icon input">
							 	 <input  id="dataTotal" type="text" value="" readonly>
								</div>
							</td>
							<td><div class="ui small icon input">
								  <input id="basicTotal" type="text" value="" readonly>
								</div></td>
							<td><div class="ui small icon input">
								  <input id="plusTotal" type="text" value="" readonly>
								</div></td>
						</tr>
					</tbody>
				</table>
			</div><!-- 퇴직전 3개월 임금총액 계산내역 끝-->
			
			<!-- 퇴직전 1년간 상여내역 -->
			<div class="hru segment quitMoney">
				<h3><i class="check square icon"></i>퇴직전 1년간 상여내역</h3>
				<table class="hru table">
					<tr>
						<th>연차수당</th>
						<td><div class="ui small icon input">
								  <input id="vacationSalary"type="text" value="" readonly>
							</div></td>
						<th>상여금</th>
						<td><div class="ui small icon input">
								  <input id="plusSalary"type="text" value="" readonly>
							</div></td>
					</tr>
				</table>
			</div><!-- 퇴직전 1년간 상여내역 끝-->
			
			<div class="hru segment quitMoney">
				<h3><i class="check square icon"></i>퇴직금 계산</h3>
				<table class="hru table">
					<tr>
						<th>일평균 임금</th>
						<td><div class="ui small icon input">
								  <input id="dateAvgSalary"type="text" value="" readonly>
							</div></td>
						<th>일통상 임금</th>
						<td><div class="ui small icon input">
								  <input id="ordinarySalary"type="text" value="" readonly>
							</div></td>
						<th>퇴직금</th>
						<td><div class="ui small icon input">
								  <input id="quitSalary" type="text" value="" readonly>
							</div></td>
					</tr>
					<tr>
						<th>퇴직소득세</th>
						<td><div class="ui small icon input">
								  <input id="quitTax"type="text" value="" readonly>
							</div></td>
						<th>퇴직지방소득세</th>
						<td><div class="ui small icon input">
								  <input id="quitTax2"type="text" value="" readonly>
							</div></td>
						<th>차인지급액</th>
						<td><div class="ui small icon input">
								  <input id="quitResult" type="text" value="" readonly>
							</div></td>
					</tr>
				</table>
			</div>
			
		</div><!-- 퇴직금 조회 끝-->
		
		<!-- 퇴직 신청 -->
		<div class="ui bottom attached tab segment" data-tab="second">
			<div class="hru segment" style="height: 400px">
				<h3><i class="check square icon"></i>퇴직신청</h3>
				<!-- 기본정보 -->
				<form action="memberQuitApproval.salary" method="post" id="quitApproval" enctype="multipart/form-data">
					<table class="hru table" id="quitInfoTable">
					<tbody>
						<tr>
							<th>사원번호</th>
							<td><c:out value=""/></td>
							<th>이름</th>
							<td><c:out value=""/></td>
						</tr>
						<tr>
							<th>사업장</th>
							<td colspan="3"><c:out value=""/></td>
						</tr>
						<tr>
							<th>부서</th>
							<td><c:out value=""/></td>
							<th>직급</th>
							<td><c:out value=""/></td>
						</tr>
					</tbody>
					</table>
					<BR>
					<BR>
						<div class="buttonArea">
						<input type="hidden" name="quitDate" id="quitDate">
						<input type="hidden" name="title" value="제목" id="title2">
						<input type="hidden" name="firstApproval" id="firstApp2">
						<input type="hidden" name="secondApproval" id="secondApp2">
						<input type="hidden" name="thirdApproval" id="thirdApp2">
						<button class="ui blue button mini" id="add" onclick="return approvalFunction();">전자결재</button>
						</div>
					</form>
				</div><!-- 기본정보 끝-->
		</div><!-- 퇴직 신청 끝-->
</div><!-- 내용부분! 끝-->
</div><!--outer Area 끝 -->

<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
	<script>
		var memberId =  ${qList.mid };
		
		
		/* 퇴직신청 Tap 동작 ajax */
		function quitApply(){
			var mid = memberId;
			
			$.ajax({
				url:"selectMemberQuitInfo.me",
				type : "post",
				data : {mid : mid},
				success : function(data){
					console.log(data);
					$tableBody = $("#quitInfoTable tbody")
					$tableBody.html('');
					
					var $tr = $("<tr>");
					var $midTh = $("<th>").text("사원번호");
					var $mid = $("<td>").text(data.mList.mid);
					var $nameTh = $("<th>").text("이름");
					var $name = $("<td>").text(data.mList.mName);
					
					$tr.append($midTh);
					$tr.append($mid);
					$tr.append($nameTh);
					$tr.append($name);
					
					
					var $tr2 = $("<tr>");
					var $groupTh = $("<th>").text("사업장");
					var $group = $("<td>").text(data.mList.groupName);
					var $quitTh = $("<th>").text("퇴직 날짜");
					var $quit =$("<td>").append($("<div class='ui calendar' id='date_calendar'>").append($("<div class='ui input left icon'>").append("<i class='calendar icon'>").append("<input type='text' id='quitDay' readonly name='quitDate' placeholder='퇴직일자 선택'>")));
					
					
					var $tr3 = $("<tr>");
					var $teamTh = $("<th>").text("부서");
					var $team = $("<td>").text(data.mList.teamName);
					var $ppNameTh = $("<th>").text("직급");
					var $ppName = $("<td>").text(data.mList.ppName);
					
					
					$tr2.append($groupTh);
					$tr2.append($group);
					$tr2.append($quitTh);
					$tr2.append($quit);
					
					$tr3.append($teamTh);
					$tr3.append($team);
					$tr3.append($ppNameTh);
					$tr3.append($ppName);
					
					$tableBody.append($tr);
					$tableBody.append($tr2);
					$tableBody.append($tr3);
					
					result();
				},
				error : function(status){
					console.log(status);
				}
			})
		}
		function result(){
			$('#date_calendar').calendar({
			    type: 'date',
			    formatter: {
			    	date: function (date, settings) {
			    	      if (!date) return '';
			    	      var day = date.getDate();
			    	      var day2;
			    	      var month = date.getMonth() + 1;
			    	      var month2;
			    	      if(month < 10){
			    	    	  month2 = '0' + month;
			    	      }else {
			    	    	  month2 = month;
			    	      }
			    	      if(day < 10){
			    	    	  day2 = '0' + day;
			    	      }else {
			    	    	  day2 = day;
			    	      }
			    	      var year = date.getFullYear();
			    	      return year + '-' + month2 + '-' + day2;
			    	}
			    }
			});
		}
		
		/* 전자결제 ajax */	
		function approvalFunction(){
			var approvalCode = 9; //퇴직신청에대한 결제코드
			//console.log("전자결재");
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					console.log(data);
					$("#appType").val("퇴직신청");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thirdName = data.appBasic.mlist[2].mName;
									var thirdMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
				},
				error : function(status){
					console.log(status);
				}
			});
			return false;
		}
		
		/* 결제 요청 버튼 눌렀을 때!! */
		/* 선택한 날짜의 기준에 맞춰서 퇴직신청을 할 수 있어야한다. */
		/* 신청하기만 가능하다. 나머지값들은 정산할 때 계산한다. */
		$('#request').click(function(){
			var date = $("#quitDay").val();
			
			$("#quitDate").val(date);
			$("#title2").val($("#title").val());
			$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
			$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
			$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
			
			$('#quitApproval').submit();
			
			var content = "결재요청 : " + $("#title").val();
			var tmid = $("#firstApp").find("input[type=hidden]").val();
			if(tmid!="" && tmid!=null && tmid!=0){
				send(tmid, content);
			}
		});
		
		
		function quitCalculation(){
			var employee = $("#employeeDate").val();
			/* 근속기간이 1년이 넘음 -> 퇴직금 계산 가능 */
			if(employee > 365 ){
				var mid = memberId;
				$.ajax({
					url : "selectMemberQuitMoney.me",
					type : "post",
					success : function(data){
						//console.log(data);	
						var totalDate = 0;
						var salaryMoney = 0;
						var plusMoney = 0;
							
						for(var i=0; i< data.tMoneyList.length; i++){
							 totalDate += data.tMoneyList[i].minus;
							 salaryMoney += data.tMoneyList[i].money;
							 plusMoney +=data.tMoneyList[i].plus;
						}
						
						$("#startOne").val(data.tMoneyList[0].firstDate);
						$("#endOne").val(data.tMoneyList[0].lastDate);
						$("#periodOne").val(data.tMoneyList[0].minus);
						$("#basicSalaryOne").val(numberFormat(data.tMoneyList[0].money));
						$("#plusSalaryOne").val(numberFormat(data.tMoneyList[0].plus));
						
						$("#startTwo").val(data.tMoneyList[1].firstDate);
						$("#endTwo").val(data.tMoneyList[1].lastDate);
						$("#periodTwo").val(data.tMoneyList[1].minus);
						$("#basicSalaryTwo").val(numberFormat(data.tMoneyList[1].money));
						$("#plusSalaryTwo").val(numberFormat(data.tMoneyList[1].plus));
						
						$("#startThree").val(data.tMoneyList[2].firstDate);
						$("#endThree").val(data.tMoneyList[2].lastDate);
						$("#periodThree").val(data.tMoneyList[2].minus);
						$("#basicSalaryThree").val(numberFormat(data.tMoneyList[2].money));
						$("#plusSalaryThree").val(numberFormat(data.tMoneyList[2].plus));
						
						$("#dataTotal").val(numberFormat(totalDate));
						$("#basicTotal").val(numberFormat(salaryMoney));
						$("#plusTotal").val(numberFormat(plusMoney));
						
						
						$("#vacationSalary").val(numberFormat(data.plusSalary.vacation));
						$("#plusSalary").val(numberFormat(data.plusSalary.plus));
						
						
						$("#dateAvgSalary").val(numberFormat(data.qSalary.avgSalary));
						$("#ordinarySalary").val(numberFormat(data.qSalary.ordinarySalary));
						$("#quitSalary").val(numberFormat(data.qSalary.quitSalary));
						
						
						//퇴직급여
						var quitSalary = data.qSalary.quitSalary;
						//근속년수
						var employeeDate = parseInt(${qList.employeeDate } / 365) + 1;
						//근속연수공제
						var employeeDiscount = 0;
						//근속연수 공제 계산식
						if(employeeDate < 5){
							employeeDiscount = 300000 * employeeDate;
						}else if(employeeDate >5 && employeeDate <= 10){
							employeeDiscount = 1500000 + (500000 * (employeeDate-5));
						}else if(employeeDate >10 && employeeDate <= 20){
							employeeDiscount = 4000000 + (800000 * (employeeDate-10));
						}else{
							employeeDiscount = 12000000 + (1200000 * (employeeDate-20));
						}
						//근속 1년당 산출세액
						var employeeDiscountResult = quitSalary - employeeDiscount;
						//console.log("근속1년당 산출세액 : " + employeeDiscountResult);
						//환산급여 (산출세약 * 12 / 정상근속연수)
						var ChangeSalary = (employeeDiscountResult * 12) / employeeDate;
						//환산급여 공제 요율 
						var ChangeDiscount = 0;
						//console.log("환산급여 :" + ChangeSalary);
						//환산급여 공제 (35% ~ 100%  계산)
						if(ChangeSalary <= 8000000){
							ChangeDiscount = ChangeSalary;
						}else if(ChangeSalary > 8000000 && ChangeSalary <= 70000000){
							ChangeDiscount = 8000000 + ((ChangeSalary - 8000000) * 0.6);
						}else if(ChangeSalary > 70000000 && ChangeSalary <= 100000000){
							ChangeDiscount = 45200000 + ((ChangeSalary - 70000000) * 0.55);
						}else if(ChangeSalary > 100000000 && ChangeSalary <= 300000000){
							ChangeDiscount = 61700000 + ((ChangeSalary - 100000000) * 0.45);
						}else{
							ChangeDiscount = 151700000 + ((ChangeSalary - 300000000) * 0.35);
						}
						//퇴직소득 과세표준
						var QuitSalaryTax = ChangeSalary - ChangeDiscount;
						//console.log("퇴직소득 과세표준  : " +  QuitSalaryTax);
						//소득세율 계산
						var getTax = 0;
						if(QuitSalaryTax <= 12000000){
							getTax = QuitSalaryTax * 0.06;
						}else if(QuitSalaryTax <= 46000000){
							getTax = (QuitSalaryTax * 0.15) - 1080000;
						}else if(QuitSalaryTax <= 88000000){
							getTax = (QuitSalaryTax * 0.24) - 52200000;
						}else if(QuitSalaryTax <= 150000000){
							getTax = (QuitSalaryTax * 0.35) - 14900000;
						}else if(QuitSalaryTax <= 300000000){
							getTax = (QuitSalaryTax * 0.38) - 19400000;
						}else if(QuitSalaryTax <= 500000000){
							getTax = (QuitSalaryTax * 0.40) - 25400000;
						}else if(QuitSalaryTax >= 500000000){
							getTax = (QuitSalaryTax * 0.42) - 35400000;
						}
						
						//console.log("환산 산출세액 : " + getTax);
						//퇴직 소득 산출 세액
						var resultTax = 0;
						//지방소득세 
						var resultTax2 = 0;
						resultTax = Math.floor((getTax * employeeDate)/12/10) * 10 ;
						resultTax2 = Math.floor(resultTax * 0.1/10)*10 ;
						
						//console.log("소득세 : " + resultTax);
						//console.log("지방소득세 : " + resultTax2);	
						
						$("#quitTax").val(numberFormat(resultTax));
						$("#quitTax2").val(numberFormat(resultTax2));
						$("#quitResult").val(numberFormat(quitSalary- (resultTax + resultTax2)))
						
					},
					error: function(status){
						console.log(status);
					}
				})
			}else{
				alert("1년 미만의 근로자는 퇴직금이 지급되지 않습니다.");
			}
		}
		
		function numberFormat(inputNumber) {
			   return inputNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
		
		
	
		$(function(){
			/* 상단메뉴 */
			$("#member3").show();
			/* 탭메뉴 */
			$(".menu .item").tab();
			$('#date_calendar').calendar({
			    type: 'date',
			    formatter: {
			    	date: function (date, settings) {
			    	      if (!date) return '';
			    	      var day = date.getDate();
			    	      var day2;
			    	      var month = date.getMonth() + 1;
			    	      var month2;
			    	      if(month < 10){
			    	    	  month2 = '0' + month;
			    	      }else {
			    	    	  month2 = month;
			    	      }
			    	      if(day < 10){
			    	    	  day2 = '0' + day;
			    	      }else {
			    	    	  day2 = day;
			    	      }
			    	      var year = date.getFullYear();
			    	      return year + '-' + month2 + '-' + day2;
			    	}
			    }
			});
		});
		
	</script>
</body>
</html>