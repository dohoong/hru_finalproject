<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>   
<html>
<head>
<meta charset="UTF-8">
<title>경조비 관리</title>
<jsp:include page="../../common/importlib.html" />
<!-- 상대경로 잘 지정해야함! -->
<style>
.input {
	width: 100px;  
}

.inpRemark {
	width: 100%;
}

.btnPay {
	float: left !important;
	width: 80px;
	height: 30px;
}
</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<!-- 상대경로 잘 지정해야함! -->
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<!-- 여기에 내용부분 작성! -->
			<div class="hru segment">
				<div class="searchArea">
					<form action="" method="get">
						<table align="center" cellspacing="10px">
							<tr>
								<td><h3>신청일자&nbsp;&nbsp;</h3></td>
								<td>
									<div class="ui calendar" id="rangestart">
										<div class="ui input left icon">
											<i class="calendar icon"></i> <input type="text"
												placeholder="Start">
										</div>
									</div>
								</td>
								<td><div class="ui calendar" id="rangeend">
										<div class="ui input left icon">
											<i class="calendar icon"></i> <input type="text"
												placeholder="End">
										</div>
									</div></td>
								<td>
									<h3>사원 번호&nbsp;&nbsp;</h3>
								</td>
								<td><div class="ui icon input">
										<input type="text" size="30" placeholder=""><i
											class="search icon"></i>
									</div></td>
								<td width="100px"></td>
								<td>
									<button class="ui blue button">검색하기</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<!-- searchArea end -->

			</div>
			<!-- 검색영역 끝 -->
			<div class="ui horizontal segments">
				<div class="ui segment">
					<h1>
						<i class="check square icon"></i>경조비 신청 내역
					</h1>
					<table class="hru board">
						<thead>
							<tr>
								<th>No.</th>
								<th>신청자</th>
								<th>경조 구분</th>
								<th>대상자</th>
								<th>경조 일자</th>
								<th>경조 금액</th>
								<th>전자 결재 상태</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>유영일</td>
								<td>부모님 회갑</td>
								<td>유영일</td>
								<td>2020.20.20</td>
								<td>20만원</td>
								<td>N</td>
							</tr>
						</tbody>
					</table>
					<div class="pagingArea">
						<div class="paging" style="margin-right: 15px;">
							<i class="angle double left icon"></i>
						</div>
						<c:forEach var="p" begin="1" end="10">
							<div class="paging">${p}</div>
						</c:forEach>
						<div class="paging" style="margin-left: 15px;">
							<i class="angle double right icon"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="hru segment">
				<h1>
					<i class="check square icon"></i>내 정보
				</h1>
				<table class="hru table">
					<tr>
						<th>신청번호</th>
						<td><c:out value="1" /></td>
						<th>* 신청자 사번</th>
						<td><label class="easterEgg"
							style="animation: twink 2s infinite;"><div class="ui mini input">
								<input type="text" id="leader">
							</div></label><i
							class="search icon"></i></td>
					</tr>
					<tr>
						<th>신청 일자</th>
						<td><div class="ui calendar" id="rangestart2">
								<div class="ui input left icon">
									<i class="calendar icon"></i> <input type="text"
										placeholder="신청">
								</div>
							</div></td>
						<th>* 신청자 명</th>
						<td><div class="ui mini input">
								<input type="text" id="leader">
							</div></td>
					</tr>
					<tr>
						<th>* 대상자 성명</th>
						<td><div class="ui mini input">
								<input type="text" id="leader">
							</div></td>
						<th>* 경조 구분</th>
						<td><div class="ui mini input">
								<input type="text" id="leader">
							</div></td>
					</tr>
					<tr>
						<th>* 신청 금액</th>
						<td><div class="ui mini input">
								<input type="text" id="leader">
							</div></td>
						<th>* 가족 관계</th>
						<td><div class="ui mini input">
								<input type="text" id="leader">
							</div></td>
					</tr>
					<tr>
						<th>* 은행</th>
						<td><div class="ui mini input">
								<input type="text" id="leader">
							</div></td>
						<th>* 경조 일자</th>
						<td><div class="ui calendar" id="rangestart3">
								<div class="ui input left icon">
									<i class="calendar icon"></i> <input type="text"
										placeholder="일자">
								</div>
							</div></td>
					</tr>
					<tr>
						<th>* 계좌번호</th>
						<td><div class="ui mini input">
								<input type="text" id="leader">
							</div></td>
						<th>전자 결재 상태</th>
						<td><div class="ui mini input">
								<input type="text" id="leader">
							</div></td>
					</tr>
					<tr>
						<th>비고</th>
						<td colspan="3"><div class="ui mini input">
								<input type="text" id="leader">
							</div></td>
					</tr>
				</table>
				<script>
					$("#rangestart2").calendar({
						type : 'date',
						formatter : {
							date : function(date, settings) {
								if (!date)
									return '';
								var day = date.getDate();
								var month = date.getMonth() + 1;
								var year = date.getFullYear();
								return year + '-' + month + '-' + day;
							}
						}
					});
					$("#rangestart").calendar({
						type : 'date',
						endCalendar : $('#rangeend'),
						formatter : {
							date : function(date, settings) {
								if (!date)
									return '';
								var day = date.getDate();
								var month = date.getMonth() + 1;
								var year = date.getFullYear();
								return year + '-' + month + '-' + day;
							}
						}
					});
					$("#rangestart3").calendar({
						type : 'date',
						formatter : {
							date : function(date, settings) {
								if (!date)
									return '';
								var day = date.getDate();
								var month = date.getMonth() + 1;
								var year = date.getFullYear();
								return year + '-' + month + '-' + day;
							}
						}
					});
					$("#rangeend").calendar({
						type : 'date',
						startCalendar : $('#rangestart'),
						formatter : {
							date : function(date, settings) {
								if (!date)
									return '';
								var day = date.getDate();
								var month = date.getMonth() + 1;
								var year = date.getFullYear();
								return year + '-' + month + '-' + day;
							}
						}
					});
				</script>
				<div class="buttonArea">
					<button class="paging btnPay" id="add">전자결재</button>
					<button class="ui blue button mini" id="add">추가</button>
					<button class="ui blue button mini" style="display: none;"
						id="complete">완료</button>
					<button class="ui blue button mini" id="modify">수정</button>
					<button class="ui blue button mini" style="display: none;"
						id="modiComplete">완료</button>
					<button class="ui gray button mini" id="delete">삭제</button>
				</div>
			</div>
		</div>

		<!-- 내용 끝 -->
	</div>
	<!-- outer end -->
<script>
	$(function(){		
		$("#member4").show();
	});
	</script>

</body>
</html>