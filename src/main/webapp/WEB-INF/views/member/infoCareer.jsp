<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
</head>
<body>
<jsp:include page="../common/menubar.jsp"/>
<div class="outer"><!--outer Area -->
	<div class="contentArea"> <!-- 내용부분! -->
		<div class="ui top attached tabular menu">
				<a class="active item" data-tab="first">학력정보</a> 
				<a class="item" data-tab="second">경력정보</a>
				<a class="item" data-tab="third">자격/어학정보</a>
		</div>
		
		<!-- 학력정보 -->
		<div class="ui bottom attached active tab segment" data-tab="first">
			<div class="hru segment">
				<h3><i class="check square icon"></i>학력정보</h3>
				<form id="eduFormC">
				<table class="hru board2">
					<thead>
						<tr>
							<th>NO</th>
							<th>학교명</th>
							<th>상태</th>
							<th>입학날짜</th>
							<th>졸업날짜</th>
						</tr>
					</thead>
					<tbody id="eduTbodyOrigin">
						<c:if test="${eduList.size() > 0}">
							<c:forEach var="el" items="${eduList}" varStatus="status">
								<tr>
									<td><c:out value="${status.count}" /></td>
									<td><c:out value="${el.maddTitle}"/></td>
									<td><c:out value="${el.maddContent}"/></td>
									<td><c:out value="${fn:split(el.startDate,'-')[0]}"/>년 <c:out value="${fn:split(el.startDate,'-')[1]}"/>월</td>
									<td><c:out value="${fn:split(el.endDate,'-')[0]}"/>년 <c:out value="${fn:split(el.endDate,'-')[1]}"/>월</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${eduList.size() eq 0}">
							<tr>
								<td style="padding: 50px" colspan="5">
									<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 학력정보가 없습니다.</label></h2>
								</td>
							</tr>
						</c:if>
					</tbody>
					<tbody id="eduTbodyChange" style="display:none">
						<c:if test="${eduList.size() > 0}">
							<c:forEach var="el" items="${eduList}" varStatus="status">
								<tr>
									<td><button class="ui icon red button mini" type="button" onclick="deleteAdd('${el.maddId}');"><i class="minus icon"></i></button></td>
									<td><input type="hidden" name="maddId" value="${el.maddId}"><div class="ui small input"><input type="text" name="title" size="40" value="${el.maddTitle}"></div></td>
									<td><div class="ui small input"><input type="text" name="content" size="15" value="${el.maddContent}"></div></td>
									<td>
										<div class="ui small input"><input type="text" size="4" name="startYear" value="${fn:split(el.startDate,'-')[0]}"></div><label>년 </label>
										<div class="ui small input"><input type="text" size="2" name="startMonth" value="${fn:split(el.startDate,'-')[1]}"></div><label>월</label>
									</td>
									<td>
										<div class="ui small input"><input type="text" size="4" name="endYear" value="${fn:split(el.endDate,'-')[0]}"></div><label>년 </label>
										<div class="ui small input"><input type="text" size="2" name="endMonth" value="${fn:split(el.endDate,'-')[1]}"></div><label>월</label>
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
				</form>
				<div style="margin-top:20px; float:right;">
					<button class="ui blue button" name="addBtn" value="1">추가</button>
					<button class="ui blue button <c:if test='${eduList.size() eq 0}'>disabled</c:if>" id="btn1" name="changeBtn" value="1">수정</button>
					<button class="ui blue button" style="display:none" name="saveBtn" value="1">저장</button>
					<button class="ui button" style="display:none" name="cancelBtn" value="1">취소</button>
				</div>
			</div>	
		</div>
	
		<!-- 경력정보 -->
		<div class="ui bottom attached tab segment" data-tab="second">
			<div class="hru segment">
				<h3><i class="check square icon"></i>경력정보</h3>
				<form id="careerFormC">
				<table class="hru board2">
					<thead>
						<tr>
							<th>NO</th>
							<th>회사명</th>
							<th>입사일자</th>
							<th>퇴사일자</th>
							<th>최종직위</th>
						</tr>
					</thead>
					<tbody id="careerTbodyOrigin">
						<c:if test="${careerList.size() > 0}">
							<c:forEach var="cl" items="${careerList}" varStatus="status">
								<tr>
									<td><c:out value="${status.count}" /></td>
									<td><c:out value="${cl.maddTitle}"/></td>
									<td><c:out value="${fn:split(cl.startDate,'-')[0]}"/>년 <c:out value="${fn:split(cl.startDate,'-')[1]}"/>월</td>
									<td><c:out value="${fn:split(cl.endDate,'-')[0]}"/>년 <c:out value="${fn:split(cl.endDate,'-')[1]}"/>월</td>
									<td><c:out value="${cl.maddContent}"/></td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${careerList.size() eq 0}">
							<tr>
								<td style="padding: 50px" colspan="5">
									<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 경력정보가 없습니다.</label></h2>
								</td>
							</tr>
						</c:if>
					</tbody>
					<tbody id="careerTbodyChange" style="display:none">
						<c:if test="${careerList.size() > 0}">
							<c:forEach var="cl" items="${careerList}" varStatus="status">
								<tr>
									<td><button class="ui icon red button mini" type="button" onclick="deleteAdd('${cl.maddId}');"><i class="minus icon"></i></button></td>
									<td><input type="hidden" name="maddId" value="${cl.maddId}"><div class="ui small input"><input type="text" name="title" size="40" value="${cl.maddTitle}"></div></td>
									<td>
										<div class="ui small input"><input type="text" size="4" name="startYear" value="${fn:split(cl.startDate,'-')[0]}"></div><label>년 </label>
										<div class="ui small input"><input type="text" size="2" name="startMonth" value="${fn:split(cl.startDate,'-')[1]}"></div><label>월</label>
									</td>
									<td>
										<div class="ui small input"><input type="text" size="4" name="endYear" value="${fn:split(cl.endDate,'-')[0]}"></div><label>년 </label>
										<div class="ui small input"><input type="text" size="2" name="endMonth" value="${fn:split(cl.endDate,'-')[1]}"></div><label>월</label>
									</td>
									<td><div class="ui small input"><input type="text" size="20" name="content" value="${cl.maddContent}"></div></td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
				</form>
				<div style="margin-top:20px; float:right;">
					<button class="ui blue button" name="addBtn" value="2">추가</button>
					<button class="ui blue button <c:if test='${careerList.size() eq 0}'>disabled</c:if>" id="btn2" name="changeBtn" value="2">수정</button>
					<button class="ui blue button" style="display:none" name="saveBtn" value="2">저장</button>
					<button class="ui button" style="display:none" name="cancelBtn" value="2">취소</button>
				</div>
			</div>
		</div><!-- 경력정보 끝-->
	
		<!-- 자격/어학 정보 -->
		<div class="ui bottom attached tab segment" data-tab="third">
			<div class="hru segment">
				<h3><i class="check square icon"></i>자격정보</h3>
				<form id="cerfiFormC">
				<table class="hru board2">
					<thead>
						<tr>
							<th>NO</th>
							<th>자격명</th>
							<th>발행처 / 발행기관</th>
							<th>자격 취득일자</th>
						</tr>
					</thead>
					<tbody id="cerfiTbodyOrigin">
						<c:if test="${cerfiList.size() > 0}">
							<c:forEach var="cf" items="${cerfiList}" varStatus="status">
								<tr>
									<td><c:out value="${status.count}" /></td>
									<td><c:out value="${cf.maddTitle}"/></td>
									<td><c:out value="${cf.maddContent}"/></td>
									<td><c:out value="${fn:split(cf.startDate,'-')[0]}"/>년 <c:out value="${fn:split(cf.startDate,'-')[1]}"/>월</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${cerfiList.size() eq 0}">
							<tr>
								<td style="padding: 50px" colspan="4">
									<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 자격정보가 없습니다.</label></h2>
								</td>
							</tr>
						</c:if>
					</tbody>
					<tbody id="cerfiTbodyChange" style="display:none">
						<c:if test="${cerfiList.size() > 0}">
							<c:forEach var="cf" items="${cerfiList}" varStatus="status">
								<tr>
									<td><button class="ui icon red button mini" type="button" onclick="deleteAdd('${cf.maddId}');"><i class="minus icon"></i></button></td>
									<td><div class="ui small input"><input type="text" size="40" name="title" value="${cf.maddTitle}"></div><input type="hidden" name="maddId" value="${cf.maddId}"></td>
									<td><div class="ui small input"><input type="text" size="30" name="content" value="${cf.maddContent}"></div></td>
									<td>
										<div class="ui small input"><input type="text" size="4" name="startYear" value="${fn:split(cf.startDate,'-')[0]}"></div><label>년 </label>
										<div class="ui small input"><input type="text" size="2" name="startMonth" value="${fn:split(cf.startDate,'-')[1]}"></div><label>월</label>
										<input type="hidden" name="endYear" value="${fn:split(cf.endDate,'-')[0]}"><input type="hidden" name="endMonth" value="${fn:split(cf.endDate,'-')[1]}">
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
				</form>
				<div style="margin-top:20px; float:right;">
					<button class="ui blue button" name="addBtn" value="3">추가</button>
					<button class="ui blue button <c:if test='${cerfiList.size() eq 0}'>disabled</c:if>" id="btn3" name="changeBtn" value="3">수정</button>
					<button class="ui blue button" style="display:none" name="saveBtn" value="3">저장</button>
					<button class="ui button" style="display:none" name="cancelBtn" value="3">취소</button>
				</div>
			</div>

			<div class="hru segment">	
				<h3><i class="check square icon"></i>어학정보</h3>
				<form id="languageFormC">
				<table class="hru board2">
					<thead>
						<tr>
							<th>NO</th>
							<th>시험명</th>
							<th>등급(점수)</th>
							<th>점수 취득일자</th>
						</tr>
					</thead>
					<tbody id="languageTbodyOrigin">
						<c:if test="${languageList.size() > 0}">
							<c:forEach var="ll" items="${languageList}" varStatus="status">
								<tr>
									<td><c:out value="${status.count}" /></td>
									<td><c:out value="${ll.maddTitle}"/></td>
									<td><c:out value="${ll.maddContent}"/></td>
									<td><c:out value="${fn:split(ll.startDate,'-')[0]}"/>년 <c:out value="${fn:split(ll.startDate,'-')[1]}"/>월</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${languageList.size() eq 0}">
							<tr>
								<td style="padding: 50px" colspan="4">
									<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 어학정보가 없습니다.</label></h2>
								</td>
							</tr>
						</c:if>
					</tbody>
					<tbody id="languageTbodyChange" style="display:none">
						<c:if test="${languageList.size() > 0}">
							<c:forEach var="ll" items="${languageList}" varStatus="status">
								<tr>
									<td><button class="ui icon red button mini" type="button" onclick="deleteAdd('${ll.maddId}');"><i class="minus icon"></i></button></td>
									<td><div class="ui small input"><input type="text" size="40" name="title" value="${ll.maddTitle}"><input type="hidden" name="maddId" value="${ll.maddId}"></div></td>
									<td><div class="ui small input"><input type="text" size="10" name="content" value="${ll.maddContent}"></div></td>
									<td>
										<div class="ui small input"><input type="text" size="4" name="startYear" value="${fn:split(ll.startDate,'-')[0]}"></div><label>년 </label>
										<div class="ui small input"><input type="text" size="2" name="startMonth" value="${fn:split(ll.startDate,'-')[1]}"></div><label>월</label>
										<input type="hidden" name="endYear" value="${fn:split(ll.endDate,'-')[0]}"><input type="hidden" name="endMonth" value="${fn:split(ll.endDate,'-')[1]}">
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
				</form>
				<div style="margin-top:20px; float:right;">
					<button class="ui blue button" name="addBtn" value="4">추가</button>
					<button class="ui blue button <c:if test='${cerfiList.size() eq 0}'>disabled</c:if>" id="btn4" name="changeBtn" value="4">수정</button>
					<button class="ui blue button" style="display:none" name="saveBtn" value="4">저장</button>
					<button class="ui button" style="display:none" name="cancelBtn" value="4">취소</button>
				</div>
			</div>
		</div><!-- 자격/어학 정보 끝 -->
	</div>
</div>

	<!-- 학력 추가 모달 -->
	<div class="large ui modal" style="width:1200px" id="eduAddModal">
		<div class="header" align="center">학력정보 추가</div>
		<div class="content" align="center">
			<form id="addEduForm">
			<input type="hidden" name="category" value="1">
				<table class="hru board2">
					<tr>
						<th>학교명</th>
						<th>상태</th>
						<th>입학일</th>
						<th>졸업일/졸업예정일</th>
					</tr>
					<tr>
						<td><div class="ui small input"><input type="text" size="40" name="maddTitle"></div></td>
						<td><div class="ui small input"><input type="text" size="15" name="maddContent"></div></td>
						<td>
							<div class="ui small input"><input type="text" size="4" name="startYear"></div><label> 년</label>&nbsp;
							<div class="ui small input"><input type="text" size="2" name="startMonth"></div><label> 월</label>
						</td>
						<td>
							<div class="ui small input"><input type="text" size="4" name="endYear"></div><label> 년</label>&nbsp;
							<div class="ui small input"><input type="text" size="2" name="endMonth"></div><label> 월</label>
						</td>
					</tr>
				</table>
			</form>
			<div class="ui two column very relaxed grid">
				<div class="column">
					<div style="margin-top:20px; margin-bottom:50px; float:left">
						<i class="exclamation circle icon"></i>퇴학의 경우 퇴학일자를, 휴학 및 재학중인 경우 졸업예정일을 입력해주세요.
					</div>
				</div>
				<div class="column">
					<div style="margin-top:10px; margin-bottom:50px; float: right;">
						<button class="ui blue button" name="addSave" value="1">추가</button>
						<button class="ui button" name="addCancel" value="1">취소</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 학력 추가 모달 끝 -->

	<!-- 경력 추가 모달 -->
	<div class="large ui modal" style="width:1200px" id="careerAddModal">
		<div class="header" align="center">경력정보 추가</div>
		<div class="content" align="center">
			<form id="addCareerForm">
			<input type="hidden" name="category" value="2">
				<table class="hru board2">
					<tr>
						<th>회사명</th>
						<th>입사일자</th>
						<th>퇴사일자</th>
						<th>최종직위</th>
					</tr>
					<tr>
						<td><div class="ui small input"><input type="text" size="40" name="maddTitle"></div></td>
						<td>
							<div class="ui small input"><input type="text" size="4" name="startYear"></div><label> 년</label>&nbsp;
							<div class="ui small input"><input type="text" size="2" name="startMonth"></div><label> 월</label>
						</td>
						<td>
							<div class="ui small input"><input type="text" size="4" name="endYear"></div><label> 년</label>&nbsp;
							<div class="ui small input"><input type="text" size="2" name="endMonth"></div><label> 월</label>
						</td>
						<td><div class="ui small input"><input type="text" size="20" name="maddContent"></div></td>
					</tr>
				</table>
			</form>
			<div style="margin-top:20px; margin-bottom:50px; float: right;">
				<button class="ui blue button" name="addSave" value="2">추가</button>
				<button class="ui button" name="addCancel" value="2">취소</button>
			</div>
		</div>
	</div>
	<!-- 경력 추가 모달 끝 -->
	
	<!-- 자격 추가 모달 -->
	<div class="large ui modal" style="width:1200px" id="cerfiAddModal">
		<div class="header" align="center">자격정보 추가</div>
		<div class="content" align="center">
			<form id="addCerfiForm">
			<input type="hidden" name="category" value="3">
				<table class="hru board2">
					<tr>
						<th>자격명</th>
						<th>발행처/발행기관</th>
						<th>자격 취득일</th>
					</tr>
					<tr>
						<td><div class="ui small input"><input type="text" size="40" name="maddTitle"></div></td>
						<td><div class="ui small input"><input type="text" size="30" name="maddContent"></div></td>
						<td>
							<div class="ui small input"><input type="text" size="4" name="startYear"></div><label> 년</label>&nbsp;
							<div class="ui small input"><input type="text" size="2" name="startMonth"></div><label> 월</label>
							<input type="hidden" name="endYear" value="2000"><input type="hidden" name="endMonth" value="01">
						</td>
					</tr>
				</table>
			</form>
			<div style="margin-top:20px; margin-bottom:50px; float: right;">
				<button class="ui blue button" name="addSave" value="3">추가</button>
				<button class="ui button" name="addCancel" value="3">취소</button>
			</div>
		</div>
	</div>
	<!-- 자격 추가 모달 끝 -->
	
	<!-- 어학 추가 모달 -->
	<div class="large ui modal" style="width:1200px" id="languageAddModal">
		<div class="header" align="center">어학정보 추가</div>
		<div class="content" align="center">
			<form id="addLanguageForm">
				<input type="hidden" name="category" value="4">
				<table class="hru board2">
					<tr>
						<th>시험명</th>
						<th>등급(점수)</th>
						<th>점수 취득일자</th>
					</tr>
					<tr>
						<td><div class="ui small input"><input type="text" size="40" name="maddTitle"></div></td>
						<td><div class="ui small input"><input type="text" size="10" name="maddContent"></div></td>
						<td>
							<div class="ui small input"><input type="text" size="4" name="startYear"></div><label> 년</label>&nbsp;
							<div class="ui small input"><input type="text" size="2" name="startMonth"></div><label> 월</label>
							<input type="hidden" name="endYear" value="2000"><input type="hidden" name="endMonth" value="01">
						</td>
					</tr>
				</table>
			</form>
			<div style="margin-top:20px; margin-bottom:50px; float: right;">
				<button class="ui blue button" name="addSave" value="4">추가</button>
				<button class="ui button" name="addCancel" value="4">취소</button>
			</div>
		</div>
	</div>
	<!-- 어학 추가 모달 끝 -->
	
	
	<script>
		$(function(){
			$(".menu .item").tab();
			$("#member1").show();
			$(".ui.selection.dropdown").dropdown();
			
			$("button[name='addBtn']").click(function(){
				switch($(this).val()){
					case "1": $("#eduAddModal").modal('show'); break;
					case "2": $("#careerAddModal").modal('show'); break;
					case "3": $("#cerfiAddModal").modal('show'); break;
					case "4": $("#languageAddModal").modal('show'); break;
				}
			});
			
			$("button[name='addCancel']").click(function(){
				switch($(this).val()){
					case "1": $("#eduAddModal").modal('hide'); break;
					case "2": $("#careerAddModal").modal('hide'); break;
					case "3": $("#cerfiAddModal").modal('hide'); break;
					case "4": $("#languageAddModal").modal('hide'); break;
				}
			});
			
			$("button[name='addSave']").click(function(){
				switch($(this).val()){
					case "1":
						var queryString = $("#addEduForm").serializeArray();
						$("#eduAddModal").modal('hide');
						$("#btn1").removeClass("disabled");
						break;
					case "2":
						var queryString = $("#addCareerForm").serializeArray();
						$("#careerAddModal").modal('hide');
						$("#btn2").removeClass("disabled");
						break;
					case "3":
						var queryString = $("#addCerfiForm").serializeArray();
						$("#cerfiAddModal").modal('hide');
						$("#btn3").removeClass("disabled");
						break;
					case "4":
						var queryString = $("#addLanguageForm").serializeArray();
						$("#languageAddModal").modal('hide');
						$("#btn4").removeClass("disabled");
						break;
				}
				$(this).parent().parent().find("input:text").val("");
				
				$.ajax({
					url:"insertMemberAdd.me",
					type:"POST",
					data:queryString,
					dataType : "json",
					success:function(data){
						var edu = data.eduList;
						var cer = data.careerList;
						var crf = data.cerfiList;
						var lge = data.languageList;
						
						//학력 부분
						$('#eduTbodyOrigin').empty();
						$('#eduTbodyChange').empty();
						
						if(edu.length > 0) {
							$.each(edu, function(index, e){
								var $tr = $('<tr>');
								var $trC = $('<tr>');
								var sDate = e.startDate.split('-');
								var eDate = e.endDate.split('-');
								
								$tr.append($('<td>').text(index + 1));
								$tr.append($('<td>').text(e.maddTitle));
								$tr.append($('<td>').text(e.maddContent));
								$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
								$tr.append($('<td>').text(eDate[0] + '년 ' + eDate[1] + '월'));
								
								$('#eduTbodyOrigin').append($tr);
								
								$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
								$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="20" value="' + e.maddContent + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="endYear" value="' + eDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="endMonth" value="' + eDate[1] + '"></div><label>월</label>')));
								
								$('#eduTbodyChange').append($trC);
							});
						} else {
							$('#eduTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 학력정보가 없습니다.</label>'))));
							$('#eduTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 학력정보가 없습니다.</label>'))));
							$("#btn1").addClass("disabled");
						}
						
						//경력부분
						$('#careerTbodyOrigin').empty();
						$('#careerTbodyChange').empty();

						if(cer.length > 0) {
							$.each(cer, function(index, e){
								var $tr = $('<tr>');
								var $trC = $('<tr>');
								var sDate = e.startDate.split('-');
								var eDate = e.endDate.split('-');
								
								$tr.append($('<td>').text(index + 1));
								$tr.append($('<td>').text(e.maddTitle));
								$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
								$tr.append($('<td>').text(eDate[0] + '년 ' + eDate[1] + '월'));
								$tr.append($('<td>').text(e.maddContent));
								
								$('#careerTbodyOrigin').append($tr);
								
								$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
								$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="endYear" value="' + eDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="endMonth" value="' + eDate[1] + '"></div><label>월</label>')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="15" value="' + e.maddContent + '">')));
								
								$('#careerTbodyChange').append($trC);
							});
						} else {
							$('#careerTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 경력정보가 없습니다.</label>'))));
							$('#careerTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 경력정보가 없습니다.</label>'))));
							$("#btn2").addClass("disabled");
						}
						
						//자격부분
						$('#cerfiTbodyOrigin').empty();
						$('#cerfiTbodyChange').empty();

						if(crf.length > 0) {
							$.each(crf, function(index, e){
								var $tr = $('<tr>');
								var $trC = $('<tr>');
								var sDate = e.startDate.split('-');
								var eDate = e.endDate.split('-');
								
								$tr.append($('<td>').text(index + 1));
								$tr.append($('<td>').text(e.maddTitle));
								$tr.append($('<td>').text(e.maddContent));
								$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
								
								$('#cerfiTbodyOrigin').append($tr);
								
								$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
								$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="30" value="' + e.maddContent + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')).append($('<input type="hidden" name="endYear" value="' + eDate[0] + '"><input type="hidden" name="endMonth" value="' + eDate[1] + '"')));
								
								$('#cerfiTbodyChange').append($trC);
							});
						} else {
							$('#cerfiTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 자격정보가 없습니다.</label>'))));
							$('#cerfiTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 자격정보가 없습니다.</label>'))));
							$("#btn3").addClass("disabled");
						}
						
						//어학부분
						$('#languageTbodyOrigin').empty();
						$('#languageTbodyChange').empty();

						if(lge.length > 0) {
							$.each(lge, function(index, e){
								var $tr = $('<tr>');
								var $trC = $('<tr>');
								var sDate = e.startDate.split('-');
								var eDate = e.endDate.split('-');
								
								$tr.append($('<td>').text(index + 1));
								$tr.append($('<td>').text(e.maddTitle));
								$tr.append($('<td>').text(e.maddContent));
								$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
								
								$('#languageTbodyOrigin').append($tr);
								
								$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
								$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="10" value="' + e.maddContent + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')).append($('<input type="hidden" name="endYear" value="' + eDate[0] + '"><input type="hidden" name="endMonth" value="' + eDate[1] + '"')));
								
								$('#languageTbodyChange').append($trC);
							});
						} else {
							$('#languageTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 어학정보가 없습니다.</label>'))));
							$('#languageTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 어학정보가 없습니다.</label>'))));
							$("#btn4").addClass("disabled");
						}
						
					},
					error:function(error){
						alert("실패");
					}
				});
			});
			
			$("button[name='changeBtn']").click(function(){
				switch($(this).val()){
					case "1": $("#eduTbodyOrigin").css("display", "none"); $("#eduTbodyChange").css("display", ""); break;
					case "2": $("#careerTbodyOrigin").css("display", "none"); $("#careerTbodyChange").css("display", ""); break;
					case "3": $("#cerfiTbodyOrigin").css("display", "none"); $("#cerfiTbodyChange").css("display", ""); break;
					case "4": $("#languageTbodyOrigin").css("display", "none"); $("#languageTbodyChange").css("display", ""); break;
				}
				$(this).css("display", "none");
				$(this).parent().find("button[name='saveBtn']").css("display", "");
				$(this).parent().find("button[name='cancelBtn']").css("display", "");
			});
			
			$("button[name='saveBtn']").click(function(){
				switch($(this).val()){
					case "1": 
						$("#eduTbodyOrigin").css("display", "");
						$("#eduTbodyChange").css("display", "none");
						var queryString = $("#eduFormC").serializeArray();
						break;
					case "2": 
						$("#careerTbodyOrigin").css("display", "");
						$("#careerTbodyChange").css("display", "none");
						var queryString = $("#careerFormC").serializeArray();
						break;
					case "3": 
						$("#cerfiTbodyOrigin").css("display", ""); 
						$("#cerfiTbodyChange").css("display", "none"); 
						var queryString = $("#cerfiFormC").serializeArray();
						break;
					case "4": 
						$("#languageTbodyOrigin").css("display", ""); 
						$("#languageTbodyChange").css("display", "none"); 
						var queryString = $("#languageFormC").serializeArray();
						break;
				}
				$(this).css("display", "none");
				$(this).parent().find("button[name='saveBtn']").css("display", "none");
				$(this).parent().find("button[name='changeBtn']").css("display", "");
				
				$.ajax({
					url:"updateMemberAdd.me",
					type:"POST",
					data:queryString,
					dataType : "json",
					success:function(data){
						var edu = data.eduList;
						var cer = data.careerList;
						var crf = data.cerfiList;
						var lge = data.languageList;
						
						//학력 부분
						$('#eduTbodyOrigin').empty();
						$('#eduTbodyChange').empty();
						
						if(edu.length > 0) {
							$.each(edu, function(index, e){
								var $tr = $('<tr>');
								var $trC = $('<tr>');
								var sDate = e.startDate.split('-');
								var eDate = e.endDate.split('-');
								
								$tr.append($('<td>').text(index + 1));
								$tr.append($('<td>').text(e.maddTitle));
								$tr.append($('<td>').text(e.maddContent));
								$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
								$tr.append($('<td>').text(eDate[0] + '년 ' + eDate[1] + '월'));
								
								$('#eduTbodyOrigin').append($tr);
								
								$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
								$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="20" value="' + e.maddContent + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="endYear" value="' + eDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="endMonth" value="' + eDate[1] + '"></div><label>월</label>')));
								
								$('#eduTbodyChange').append($trC);
							});
						} else {
							$('#eduTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 학력정보가 없습니다.</label>'))));
							$('#eduTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 학력정보가 없습니다.</label>'))));
							$("#btn1").addClass("disabled");
						}
						
						//경력부분
						$('#careerTbodyOrigin').empty();
						$('#careerTbodyChange').empty();

						if(cer.length > 0) {
							$.each(cer, function(index, e){
								var $tr = $('<tr>');
								var $trC = $('<tr>');
								var sDate = e.startDate.split('-');
								var eDate = e.endDate.split('-');
								
								$tr.append($('<td>').text(index + 1));
								$tr.append($('<td>').text(e.maddTitle));
								$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
								$tr.append($('<td>').text(eDate[0] + '년 ' + eDate[1] + '월'));
								$tr.append($('<td>').text(e.maddContent));
								
								$('#careerTbodyOrigin').append($tr);
								
								$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
								$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="endYear" value="' + eDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="endMonth" value="' + eDate[1] + '"></div><label>월</label>')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="15" value="' + e.maddContent + '">')));
								
								$('#careerTbodyChange').append($trC);
							});
						} else {
							$('#careerTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 경력정보가 없습니다.</label>'))));
							$('#careerTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 경력정보가 없습니다.</label>'))));
							$("#btn2").addClass("disabled");
						}
						
						//자격부분
						$('#cerfiTbodyOrigin').empty();
						$('#cerfiTbodyChange').empty();

						if(crf.length > 0) {
							$.each(crf, function(index, e){
								var $tr = $('<tr>');
								var $trC = $('<tr>');
								var sDate = e.startDate.split('-');
								var eDate = e.endDate.split('-');
								
								$tr.append($('<td>').text(index + 1));
								$tr.append($('<td>').text(e.maddTitle));
								$tr.append($('<td>').text(e.maddContent));
								$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
								
								$('#cerfiTbodyOrigin').append($tr);
								
								$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
								$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="30" value="' + e.maddContent + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')).append($('<input type="hidden" name="endYear" value="' + eDate[0] + '"><input type="hidden" name="endMonth" value="' + eDate[1] + '"')));
								
								$('#cerfiTbodyChange').append($trC);
							});
						} else {
							$('#cerfiTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 자격정보가 없습니다.</label>'))));
							$('#cerfiTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 자격정보가 없습니다.</label>'))));
							$("#btn3").addClass("disabled");
						}
						
						//어학부분
						$('#languageTbodyOrigin').empty();
						$('#languageTbodyChange').empty();

						if(lge.length > 0) {
							$.each(lge, function(index, e){
								var $tr = $('<tr>');
								var $trC = $('<tr>');
								var sDate = e.startDate.split('-');
								var eDate = e.endDate.split('-');
								
								$tr.append($('<td>').text(index + 1));
								$tr.append($('<td>').text(e.maddTitle));
								$tr.append($('<td>').text(e.maddContent));
								$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
								
								$('#languageTbodyOrigin').append($tr);
								
								$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
								$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="10" value="' + e.maddContent + '">')));
								$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')).append($('<input type="hidden" name="endYear" value="' + eDate[0] + '"><input type="hidden" name="endMonth" value="' + eDate[1] + '"')));
								
								$('#languageTbodyChange').append($trC);
							});
						} else {
							$('#languageTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 어학정보가 없습니다.</label>'))));
							$('#languageTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 어학정보가 없습니다.</label>'))));
							$("#btn4").addClass("disabled");
						}
					},
					error:function(error){
						alert("실패");
					}
				});
				
			});
			
			$("button[name='cancelBtn']").click(function(){
				switch($(this).val()){
					case "1": $("#eduTbodyOrigin").css("display", ""); $("#eduTbodyChange").css("display", "none"); break;
					case "2": $("#careerTbodyOrigin").css("display", ""); $("#careerTbodyChange").css("display", "none"); break;
					case "3": $("#cerfiTbodyOrigin").css("display", ""); $("#cerfiTbodyChange").css("display", "none"); break;
					case "4": $("#languageTbodyOrigin").css("display", ""); $("#languageTbodyChange").css("display", "none"); break;
				}
				$(this).css("display", "none");
				$(this).parent().find("button[name='saveBtn']").css("display", "none");
				$(this).parent().find("button[name='changeBtn']").css("display", "");
			});
		});
		
		function deleteAdd(val) {
			Swal.fire({
				title: "삭제하시겠습니까?",
				icon: "warning",
				showCancelButton: true,
				confirmButtonColor: "#0F4C81",
				confirmButtonText: "예, 삭제합니다.",
				cancelButtonText: "취소"
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url:"deleteMemberAdd.me",
							type:"POST",
							data:{"maddId":val},
							dataType : "json",
							success:function(data){
								var edu = data.eduList;
								var cer = data.careerList;
								var crf = data.cerfiList;
								var lge = data.languageList;
								
								//학력 부분
								$('#eduTbodyOrigin').empty();
								$('#eduTbodyChange').empty();
								
								if(edu.length > 0) {
									$.each(edu, function(index, e){
										var $tr = $('<tr>');
										var $trC = $('<tr>');
										var sDate = e.startDate.split('-');
										var eDate = e.endDate.split('-');
										
										$tr.append($('<td>').text(index + 1));
										$tr.append($('<td>').text(e.maddTitle));
										$tr.append($('<td>').text(e.maddContent));
										$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
										$tr.append($('<td>').text(eDate[0] + '년 ' + eDate[1] + '월'));
										
										$('#eduTbodyOrigin').append($tr);
										
										$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
										$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="20" value="' + e.maddContent + '">')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="endYear" value="' + eDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="endMonth" value="' + eDate[1] + '"></div><label>월</label>')));
										
										$('#eduTbodyChange').append($trC);
									});
								} else {
									$('#eduTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 학력정보가 없습니다.</label>'))));
									$('#eduTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 학력정보가 없습니다.</label>'))));
									$("#btn1").addClass("disabled");
									$('#eduTbodyOrigin').css("display", "");
									$('#eduTbodyChange').css("display", "none");
									$("button[name='changeBtn']:button[value='1']").css("display", "");
									$("button[name='saveBtn']:button[value='1']").css("display", "none");
									$("button[name='cancelBtn']:button[value='1']").css("display", "none");
								}
								
								//경력부분
								$('#careerTbodyOrigin').empty();
								$('#careerTbodyChange').empty();

								if(cer.length > 0) {
									$.each(cer, function(index, e){
										var $tr = $('<tr>');
										var $trC = $('<tr>');
										var sDate = e.startDate.split('-');
										var eDate = e.endDate.split('-');
										
										$tr.append($('<td>').text(index + 1));
										$tr.append($('<td>').text(e.maddTitle));
										$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
										$tr.append($('<td>').text(eDate[0] + '년 ' + eDate[1] + '월'));
										$tr.append($('<td>').text(e.maddContent));
										
										$('#careerTbodyOrigin').append($tr);
										
										$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
										$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="endYear" value="' + eDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="endMonth" value="' + eDate[1] + '"></div><label>월</label>')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="15" value="' + e.maddContent + '">')));
										
										$('#careerTbodyChange').append($trC);
									});
								} else {
									$('#careerTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 경력정보가 없습니다.</label>'))));
									$('#careerTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="5">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 경력정보가 없습니다.</label>'))));
									$("#btn2").addClass("disabled");
									$('#careerTbodyOrigin').css("display", "");
									$('#careerTbodyChange').css("display", "none");
									$("button[name='changeBtn']:button[value='2']").css("display", "");
									$("button[name='saveBtn']:button[value='2']").css("display", "none");
									$("button[name='cancelBtn']:button[value='2']").css("display", "none");
								}
								
								//자격부분
								$('#cerfiTbodyOrigin').empty();
								$('#cerfiTbodyChange').empty();

								if(crf.length > 0) {
									$.each(crf, function(index, e){
										var $tr = $('<tr>');
										var $trC = $('<tr>');
										var sDate = e.startDate.split('-');
										var eDate = e.endDate.split('-');
										
										$tr.append($('<td>').text(index + 1));
										$tr.append($('<td>').text(e.maddTitle));
										$tr.append($('<td>').text(e.maddContent));
										$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
										
										$('#cerfiTbodyOrigin').append($tr);
										
										$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
										$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="30" value="' + e.maddContent + '">')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')).append($('<input type="hidden" name="endYear" value="' + eDate[0] + '"><input type="hidden" name="endMonth" value="' + eDate[1] + '"')));
										
										$('#cerfiTbodyChange').append($trC);
									});
								} else {
									$('#cerfiTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 자격정보가 없습니다.</label>'))));
									$('#cerfiTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 자격정보가 없습니다.</label>'))));
									$("#btn3").addClass("disabled");
									$('#cerfiTbodyOrigin').css("display", "");
									$('#cerfiTbodyChange').css("display", "none");
									$("button[name='changeBtn']:button[value='3']").css("display", "");
									$("button[name='saveBtn']:button[value='3']").css("display", "none");
									$("button[name='cancelBtn']:button[value='3']").css("display", "none");
								}
								
								//어학부분
								$('#languageTbodyOrigin').empty();
								$('#languageTbodyChange').empty();

								if(lge.length > 0) {
									$.each(lge, function(index, e){
										var $tr = $('<tr>');
										var $trC = $('<tr>');
										var sDate = e.startDate.split('-');
										var eDate = e.endDate.split('-');
										
										$tr.append($('<td>').text(index + 1));
										$tr.append($('<td>').text(e.maddTitle));
										$tr.append($('<td>').text(e.maddContent));
										$tr.append($('<td>').text(sDate[0] + '년 ' + sDate[1] + '월'));
										
										$('#languageTbodyOrigin').append($tr);
										
										$trC.append($('<td>').append($('<button class="ui icon red button mini" type="button" onclick="deleteAdd(' + e.maddId + ');"><i class="minus icon"></i>')));
										$trC.append($('<td>').append($('<input type="hidden" name="maddId" value="' +e.maddId + '">')).append($('<div class="ui small input"><input type="text" name="title" size="40" value="'+ e.maddTitle + '">')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" name="content" size="10" value="' + e.maddContent + '">')));
										$trC.append($('<td>').append($('<div class="ui small input"><input type="text" size="4" name="startYear" value="' + sDate[0] + '"></div><label>년 </label><div class="ui small input"><input type="text" size="2" name="startMonth" value="' + sDate[1] + '"></div><label>월</label>')).append($('<input type="hidden" name="endYear" value="' + eDate[0] + '"><input type="hidden" name="endMonth" value="' + eDate[1] + '"')));
										
										$('#languageTbodyChange').append($trC);
									});
								} else {
									$('#languageTbodyOrigin').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 어학정보가 없습니다.</label>'))));
									$('#languageTbodyChange').append($('<tr>').append($('<td style="padding: 50px" colspan="4">').append($('<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 어학정보가 없습니다.</label>'))));
									$("#btn4").addClass("disabled");
									$('#languageTbodyOrigin').css("display", "");
									$('#languageTbodyChange').css("display", "none");
									$("button[name='changeBtn']:button[value='4']").css("display", "");
									$("button[name='saveBtn']:button[value='4']").css("display", "none");
									$("button[name='cancelBtn']:button[value='4']").css("display", "none");
								}
							},
							error:function(error){
								alert("실패");
							}
						});
					}
				});
		}
	</script>
</body>
</html>