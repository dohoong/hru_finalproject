<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html" />
<!-- 상대경로 잘 지정해야함! -->
<style>
.searchArea {
	padding-top: 7px;
}
#startDate, #endDate {
	width: 200px;
}

emberInfo {
	margin-bottom: 0px;;
}

#memberTable>tbody>td {
	width: 200px;
}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp" />
	<!-- 상대경로 잘 지정해야함! -->
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<!-- 여기에 내용부분 작성! -->
			<div class="ui top attached tabular menu" onclick="accountNumber()">
				<a class="active item" data-tab="first">급여조회</a><a class="item"data-tab="second">계좌관리</a>
			</div>
			<!-- 급여조회 -->
			<div class="ui bottom attached active tab segment" data-tab="first">
				<div class="hru segment" id="dateSearch">
					<div class="searchArea" style="height: 70px;">
							<table align="center" cellspacing="10px">
								<tr>
									<td><h3 id="title1">급여지급 일자</h3></td>
									<td style="width: 50px"></td>
									<td>
										<div class="ui calendar" id="month_year_calendar">
											<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" readonly="readonly" placeholder="근무년월 선택" id="searchDate">
											</div>
										</div>
									</td>
									<td> ~ </td>
									<td>
										<div class="ui calendar" id="month_year_calendar2">
											<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" readonly="readonly" placeholder="근무년월 선택" id="searchDate2">
											</div>
										</div>
									</td>
									<td style="width: 50px"></td>
									<td>
										<button class="ui blue button" id="periodButton" onclick="search();">검색하기</button>
									</td>
								</tr>
							</table>
					</div>
					<!-- searchArea end -->
				</div>

				<div class="hru segment">
					<table class="hru board2" border="1" id="salaryList">
						<thead>
							<tr>
								<th>No</th>
								<th>지급기준일</th>
								<th>실지급금액</th>
								<th>급여명세서</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
					<%-- <div class="pagingArea">
						<div class="paging" style="margin-right: 15px;">
							<i class="angle double left icon"></i>
						</div>
						<c:forEach var="p" begin="1" end="10">
							<div class="paging">${p}</div>
						</c:forEach>
						<div class="paging" style="margin-left: 15px;">
							<i class="angle double right icon"></i>
						</div>
					</div> --%>
				</div>
				<!-- 테이블영역 끝! -->
			</div>
			<!-- 급여조회 끝 -->


			<!-- 계좌관리 -->
			<div class="ui bottom attached tab segment" data-tab="second">
				<div class="hru segment">
					<h3><i class="check square icon"></i>계좌 정보</h3>
					<table class="hru table" id="SalaryAccount">
						<tr>
							<th>은행</th>
							<td><c:out value="신한" /></td>
							<th>예금주</th>
							<td><label class="easterEgg"
								style="animation: twink 2s infinite;"><c:out value="이도훈" /></label></td>
						</tr>
						<tr>
							<th>계좌번호</th>
							<td colspan="3"><c:out value="110412019211" /></td>
						</tr>
					</table>
				</div>
			</div>
			<!-- 계좌관리끝  -->

		</div>
		<!-- 내용 끝 -->
	</div>
	<!-- outer end -->

	<!-- 급여명세서 모달 -->
	<div class="ui modal" id="ModalSalary">
		<i class="close icon"></i>
		<div class="header" id="header">
		
		</div>
		<div class="image content" style="display: inline-block;">
			<div class="hru segment" id="memberInfo"
				style="padding-top: 0px; padding-bottom: 0px;">
				<table id="memberTable">
					<tbody>
						
					</tbody>
				</table>
			</div>
			<div class="hru segment">
				<table class="hru board2" border="1" id="MemberDetailSalary">
					<thead>
						<tr>
							<th style="width: 200px;">지급항목</th>
							<th>지급액</th>
							<th style="width: 200px;">공제항목</th>
							<th>공제액</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
				<BR><BR><BR>
				<label class="bottomText" style="float: left">귀하의 노고에
					감사드립니다.</label><label class="bottomText" style="float: right"><c:out
						value="오렌지레드 컴퍼니" /></label>
			</div>
		</div>
		<div class="actions">
			<div class="ui black deny button" style="background: #8f8f8f;">
				취소</div>
			<div onclick="return printScript();"class="ui positive right labeled icon button"style="background: #1f6650;">인쇄하기 <i class="print icon"></i></div>
		</div>
	</div>

	<!-- 스크립트 시작 -->
	<script>
		/* 본인 계좌 조회 */
		function accountNumber(){
			
			$.ajax({
				url:"selectMemberSalary.me",
				type:"post",
				success:function(data){
					$tableBody=$("#SalaryAccount");
					$tableBody.html('');
					
					var $tr = $("<tr>");
					var $bank = $("<th>").text("은행");
					var $bankName = $("<td>").text(data.bList.bank);
					var $name = $("<th>").text("예금주");
					var $mName = $("<td>").text(data.bList.mName);
					
					$tr.append($bank);
					$tr.append($bankName);
					$tr.append($name);
					$tr.append($mName);
					
					var $tr2 = $("<tr>");
					var $account = $("<th>").text("계좌번호");
					var $accountNumber= $("<td>").attr('colspan',3).text(data.bList.accountNumber);
					
					$tr2.append($account);
					$tr2.append($accountNumber);
					
					$tableBody.append($tr);
					$tableBody.append($tr2);
				},
				error:function(status){
					console.log(status);
				}
			})
		}
	
		/* 인쇄자바스크립트 */
		function printScript(){
			window.print();
		}
		/* 급여지급 내용 검색 */
		function search(){
			MemberId = 0;
			//검색 일자는 급여를 지급한 날짜를 의미한다. 
			//(ex : 201912 ~ 201912를 검색하면 201911월에 근무를해서 20191210일에 받은 급여내용이 검색된다.)
			//검색한 날짜 데이터
			var Date1 = $("#searchDate").val();
			var Date2 = $("#searchDate2").val();
			//날짜데이터 문자열 자르기
			var searchDateSplit1 = Date1.split('-');
			var searchDateSplit2 = Date2.split('-');
			//date타입으로 변환
			var transDate1 = new Date(searchDateSplit1[0] , searchDateSplit1[1]-1 , 01);
			var transDate2 = new Date(searchDateSplit2[0] , searchDateSplit2[1]-1 , 30);
			//YYYY-MM-DD 형식으로 변환 (타입 : STRING) -> 검색 조건
			var searchDate1 = dateToYYYYMMDD(transDate1);
			var searchDate2 = dateToYYYYMMDD(transDate2);
			var form = {
					searchDate1:searchDate1,
					searchDate2:searchDate2
			}
			$.ajax({
				url:"selectSalaryList.me",
				type:"post",
				data: form,
				success:function(data){
					
				var length =  data.sList.length;
				
				$tableBody=$("#salaryList tbody");
				$tableBody.html('');
				for(var i = 0; i<length; i++){
					var $tr = $("<tr>");
					var $no = $("<td>").text(i+1);
					var $date = $("<td>").text(data.sList[i].salaryDate);
					var $money = $("<td>").text(numberFormat(data.sList[i].salaryMoney));
					var $icon = $("<td>").append($("<i class='ui icon print' onclick='showModal(this);'>"));
					$tr.append($no);
					$tr.append($date);
					$tr.append($money);
					$tr.append($icon);
					$tableBody.append($tr);
				}
				
				},
				error:function(status){
					console.log(status);
				}
				
			})
			
		}
		/* 선택한 급여 목록의 상세정보인 모달창을 띄운다. */
		function showModal(select){
			var date = $(select).parent().parent().find("td:eq(1)").text();
			
			form = {
					date:date
			}
			$.ajax({
				url:"selectDateSalary.me",
				type:"post",
				data: form,
				success:function(data){
					var year = data.sdList[0].salaryDate.substr(0,4);
					var month = data.sdList[0].salaryDate.substr(5,2);
					$header=$("#header");
					$header.empty();
					$header.append($("<div style='display: inline-block; width:48%; vertical-align:top;'>").append("<h2 style='color:black;'>"+year+"년"+month+"월 급여명세서</h2>"))
					$header.append($("<div style='display: inline-block; width:48%; float:right;'><div>").append("<label style='font-size:medium; float:right; padding-top:7px;'>지급일 : "+data.sdList[0].salaryDate +"</lable>"))
					/* 사원 성명 , 부서*/
					$tableBodyOne=$("#memberTable");
					$tableBodyOne.html('');
					var $memberTr = $("<tr>");
					var $name = $("<td>").text("성명 : "+ data.sdList[0].mName);
					var $tdOne = $("<td style='width:50px'>");
					var $team = $("<td>").text("부서 : "+ data.sdList[0].teamName);
					var $tdTwo = $("<td style='width:50px'>");
					$memberTr.append($name);
					$memberTr.append($tdOne);
					$memberTr.append($team);
					$memberTr.append($tdTwo);
					
					
					$tableBodyTwo=$("#MemberDetailSalary");
					$tableBodyTwo.html('');
					/* 기본급 , 국민연금 */
					var $salaryTrOne = $("<tr>");
					var $basic = $("<td>").text("기본급")
					var $basicSalary = $("<td>").text(numberFormat(data.sdList[0].sdMoney));
					var $national = $("<td>").text("국민연금")
					var $nationalTax = $("<td>").text(numberFormat(data.sdList[8].sdMoney));
					
					$salaryTrOne.append($basic);
					$salaryTrOne.append($basicSalary);
					$salaryTrOne.append($national);
					$salaryTrOne.append($nationalTax);
					/* 상여금 , 건강보험 */
					var $salaryTrTwo = $("<tr>");					
					var $plus = $("<td>").text("상여금")
					var $plusSalary = $("<td>").text(numberFormat(data.sdList[1].sdMoney));
					var $health = $("<td>").text("건강보험")
					var $healthTax = $("<td>").text(numberFormat(data.sdList[9].sdMoney));
					$salaryTrTwo.append($plus);
					$salaryTrTwo.append($plusSalary);
					$salaryTrTwo.append($health);
					$salaryTrTwo.append($healthTax);
					/* 식대 , 장기요양보험 */
					var $salaryTrThree = $("<tr>");					
					var $meal = $("<td>").text("식대")
					var $mealSalary = $("<td>").text(numberFormat(data.sdList[3].sdMoney));
					var $long = $("<td>").text("장기요양보험")
					var $longTax = $("<td>").text(numberFormat(data.sdList[10].sdMoney));
					$salaryTrThree.append($meal);
					$salaryTrThree.append($mealSalary);
					$salaryTrThree.append($long);
					$salaryTrThree.append($longTax);
					
					/* 차량보조 , 고용보험*/
					var $salaryTrFour = $("<tr>");					
					var $car = $("<td>").text("차량보조")
					var $carSalary = $("<td>").text(numberFormat(data.sdList[4].sdMoney));
					var $hire = $("<td>").text("고용보험")
					var $hireTax = $("<td>").text(numberFormat(data.sdList[11].sdMoney));
					$salaryTrFour.append($car);
					$salaryTrFour.append($carSalary);
					$salaryTrFour.append($hire);
					$salaryTrFour.append($hireTax);
					
					/* 연장근무수당 , 소득세*/
					var $salaryTrFive = $("<tr>");					
					var $extension = $("<td>").text("연장근무수당")
					var $extensionSalary = $("<td>").text(numberFormat(data.sdList[5].sdMoney));
					var $money = $("<td>").text("소득세")
					var $moneyTax = $("<td>").text(numberFormat(data.sdList[12].sdMoney));
					$salaryTrFive.append($extension);
					$salaryTrFive.append($extensionSalary);
					$salaryTrFive.append($money);
					$salaryTrFive.append($moneyTax);
					
					/* 야근수당 , 지방소득세*/
					var $salaryTrsix = $("<tr>");					
					var $night = $("<td>").text("야근수당")
					var $nightSalary = $("<td>").text(numberFormat(data.sdList[6].sdMoney));
					var $money2 = $("<td>").text("지방소득세")
					var $moneyTax2= $("<td>").text(numberFormat(data.sdList[13].sdMoney));
					$salaryTrsix.append($night);
					$salaryTrsix.append($nightSalary);
					$salaryTrsix.append($money2);
					$salaryTrsix.append($moneyTax2);
					
					/* 휴일수당 */
					var $salaryTrSeven = $("<tr>");					
					var $holiy = $("<td>").text("휴일수당")
					var $holiySalary = $("<td>").text(numberFormat(data.sdList[7].sdMoney));
					var $none = $("<td>")
					var $none2= $("<td>")
					$salaryTrSeven.append($holiy);
					$salaryTrSeven.append($holiySalary);
					$salaryTrSeven.append($none);
					$salaryTrSeven.append($none2);
					
					
					var giveTotal = 0;
					var taxTotal = 0;
					var resultSalary = 0;
					
					for(var j=0; j<13;j++){
						if(j<8){
							giveTotal += data.sdList[j].sdMoney;
						}else{
							taxTotal += data.sdList[j].sdMoney;
						}
					}
					
					resultSalary = giveTotal - taxTotal;
					
					/* 지급항목계  , 공제항목계 */
					var $salaryTrEight = $("<tr >");					
					var $give = $("<td style='font-weight: bold;'>").text("지금항목계")
					var $giveSalary = $("<td>").text(numberFormat(giveTotal));
					var $tax = $("<td style='font-weight: bold;'>").text("공제항목계")
					var $taxSalary= $("<td>").text(numberFormat(taxTotal));
					$salaryTrEight.append($give);
					$salaryTrEight.append($giveSalary);
					$salaryTrEight.append($tax);
					$salaryTrEight.append($taxSalary);
					/* 실지급 금액계 */
					var $salaryTrNine = $("<tr >");					
					var $result = $("<td style='font-weight: bold; background:#e0e0e0;'>").attr('colspan',3).text("실지급금액계");
					var $resultSalary = $("<td>").text(numberFormat(resultSalary));
					$salaryTrNine.append($result);
					$salaryTrNine.append($resultSalary);
					
					
					$tableBodyOne.append($memberTr);
					$tableBodyTwo.append($salaryTrOne)
					$tableBodyTwo.append($salaryTrTwo)
					$tableBodyTwo.append($salaryTrThree)
					$tableBodyTwo.append($salaryTrFour)
					$tableBodyTwo.append($salaryTrFive)
					$tableBodyTwo.append($salaryTrsix)
					$tableBodyTwo.append($salaryTrSeven)
					$tableBodyTwo.append($salaryTrEight)
					$tableBodyTwo.append($salaryTrNine)
					
				},
				error:function(status){
					console.log(status);
				}
			})
			
			
			$('.ui.modal').modal('show');
		}
		
		
		
		function numberFormat(inputNumber) {
			   return inputNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
	
	
		function dateToYYYYMMDD(date){
		    function pad(num) {
		        num = num + '';
		        return num.length < 2 ? '0' + num : num;
		    }
		    return date.getFullYear() + '-' + pad(date.getMonth()+1) + '-' + pad(date.getDate());
		}
	
	
		$(function() {
			/* 상단메뉴 */
			$("#member3").show();
			/* 탭메뉴 */
			$(".menu .item").tab();
		});

		/* 서치영역 데이트피커 */
		$("#month_year_calendar").calendar(
				{
					type : 'month',
					endCalendar : $('#month_year_calendar2'),
					 formatter: {
					    	date: function (date, settings) {
					    	      if (!date) return '';
					    	      var month = date.getMonth() + 1;
				                   var month2;
				                   if(month < 10){
				                      month2 = '0' + month;
				                   }else {
				                      month2 = month;
				                   }
					    	      var year = date.getFullYear();
					    	      return year + '-' + month2;
					    	}
					    },
					text : {
						monthsShort : [ '1월', '2월', '3월', '4월', '5월', '6월',
								'7월', '8월', '9월', '10월', '11월', '12월' ]
					}
				});
		$("#month_year_calendar2").calendar(
				{
					type : 'month',
					startCalendar : $('#month_year_calendar'),
					 formatter: {
					    	date: function (date, settings) {
					    	      if (!date) return '';
					    	      var month = date.getMonth() + 1;
				                   var month2;
				                   if(month < 10){
				                      month2 = '0' + month;
				                   }else {
				                      month2 = month;
				                   }
					    	      var year = date.getFullYear();
					    	      return year + '-' + month2;
					    	}
					},
					text : {
						monthsShort : [ '1월', '2월', '3월', '4월', '5월', '6월',
								'7월', '8월', '9월', '10월', '11월', '12월' ]
					}
				});
		
	
		function dateToYYYYMMDD(date){
		    function pad(num) {
		        num = num + '';
		        return num.length < 2 ? '0' + num : num;
		    }
		    return date.getFullYear() + '-' + pad(date.getMonth()+1) + '-' + pad(date.getDate());
		}
	</script>
</body>
</html>