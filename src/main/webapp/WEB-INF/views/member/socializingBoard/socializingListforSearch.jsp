<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>친목 도모 게시판</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<jsp:include page="../../common/importlib.html" />
<style>
.divCategory {
	float: left;
	margin-left: 10px;
}

.tblArea {
	text-align: center;
	width: 90%;
}

.btn {
	float: right;
}

.btnInsert {
	float: right;
}

.board2 tbody tr:hover {
	background: #eee;
	cursor: pointer;
}

.marginDiv {
	height: 1000px;
}
</style>
<!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<!-- 여기에 내용부분 작성! -->
			<h1>
				<i class="check square icon"></i>사원 친목 도모 게시판
			</h1>
			<div class="hru segment">
				<div class="searchArea" style="width: 100%;">
					<table align="center" cellspacing="10px">
						<tr>
							<td><h3>검색어</h3></td>
							<td><select class="ui dropdown divCategory"
								id="searchCondition" name="searchCondition">
									<option value="" selected disabled hidden>카테고리</option>
									<option value="title">제목</option>
									<option value="writer">작성자</option>
							</select></td>
							<td>
								<div class="ui icon input">
									<input type="search" size="30" id="searchValue"
										name="searchValue" placeholder="Search..."><i
										class="search icon"></i>
								</div>
							</td>
							<td>
								<button onclick="searchBoard();" class="ui blue button">검색하기</button>
							</td>
						</tr>
					</table>
				</div>
				<!-- searchArea end -->
				<script>
					function searchBoard() {
						var searchCondition = $("#searchCondition").val();
						var searchValue = $("#searchValue").val();
						var category = $("#category").val();
						
				
						location.href = "searchSocialList.bo?searchCondition="
								+ searchCondition + "&searchValue="
								+ searchValue+"&category="+category;
					}
					$(function() {
						$(".ui.dropdown").dropdown();
					});
				</script>
			</div>
			<!-- 검색영역 끝 -->
			<div>
				<button class="ui blue button btn"
					onclick="location.href='selectSocialListforWriterMain.bo'">내가
					쓴글 보기</button>
			</div>
			<select class="ui dropdown divCategory" id="filterCondition" name="category" onchange="filterBoard();">
				<option value="" hidden selected disabled>카테고리</option>
				<option value="1">요리</option>
				<option value="2">운동</option>
				<option value="3">토익</option>
				<option value="4">봉사</option>
				<option value="5">문화</option>
				<option value="6">소통</option>
			</select>
			<script>
				function filterBoard() {
					var filterCondition = $("#filterCondition").val();

					location.href = "filterSocialList.bo?filterCondition="
							+ filterCondition;
				}
			</script>
			<div class="hru segment" style="width: 100%;">
				<table id="tblFri" class="hru board2" border="1">
					<thead>
						<tr>
							<th>No.</th>
							<th>제목</th>
							<th>작성자</th>
							<th>모집 인원</th>
							<th>마감 일자</th>
							<th>조회수</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="sb" items="${ list }">
							<tr>
								<input type="hidden" id="category" value="${category}">
								<input type="hidden" name="mid" value="${sb.mid}">
								<td><c:out value="${ sb.friendid }" /></td>
								<td><c:out value="${ sb.title }" /></td>
								<td><c:out value="${ sb.mname }" /></td>
								<td><c:out value="${ sb.meetperson }" /></td>
								<td><c:out value="${ sb.meetenddate }" /></td>
								<td><c:out value="${ sb.count }" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
						<div class="pagingArea">
							<!-- <div class="paging" style="margin-right:15px;"><i class="angle double left icon"></i></div> -->
							<c:if test="${ pi.currentPage <= 1 }">
								<div class="paging-disabled" style="margin-right: 15px;">
									<i class="angle double left icon" style="color: white;"></i>
								</div>
							</c:if>
							<c:if test="${ pi.currentPage > 1 }">
								<c:url var="pageCheck0" value="searchSocialList.bo">
									<c:param name="currentPage" value="${pi.currentPage -1}" />
								</c:url>
								<div class="paging" onclick="location.href='${pageCheck0}'"
									style="margin-right: 15px;">
									<i class="angle double left icon"></i>
								</div>
							</c:if>

							<c:forEach var="p" begin="${pi.startPage}" end="${pi.endPage}">
								<c:if test="${p eq pi.currentPage}">
									<div class="paging-disabled">${p}</div>
								</c:if>
								<c:if test="${p ne pi.currentPage}">
									<c:url var="pageCheck" value="searchSocialList.bo">
										<c:param name="currentPage" value="${ p }" />
									</c:url>
									<div class="paging" onclick="location.href='${pageCheck}'">${p}</div>
								</c:if>
							</c:forEach>

							<!-- <div class="paging" style="margin-left:15px;"><i class="angle double right icon"></i></div> -->
							<c:if test="${ pi.currentPage >= pi.maxPage }">
								<div class="paging-disabled" style="margin-left: 15px;">
									<i class="angle double right icon" style="color: white;"></i>
								</div>
							</c:if>
							<c:if test="${pi.currentPage < pi.maxPage}">
								<c:url var="pageCheck2" value="searchSocialList.bo">
									<c:param name="currentPage" value="${pi.currentPage+1}" />
								</c:url>
								<div class="paging" onclick="location.href='${pageCheck2}'"
									style="margin-left: 15px;">
									<i class="angle double right icon"></i>
								</div>
							</c:if>
						</div>
					
				<div class="btnArea">
					<button class="ui blue button btnInsert"
						onclick="location.href='selectSocialListInsert.bo'">글 작성</button>
				</div>
			</div>
			<!-- 테이블영역 끝! -->
		</div>
	</div>

	<!-- 내용 끝 -->
	<!-- outer end -->
	<script>
		$(function() {
			$("#member4").show();
		});

		$(function() {
			$("#tblFri").find("td").click(
					function() {
						var friendid = $(this).parent().children("td").eq(0)
								.text();

						console.log(friendid);

						location.href = "selectOneSocialListApply.bo?friendid="
								+ friendid;
					});

		})
	</script>

</body>
</html>