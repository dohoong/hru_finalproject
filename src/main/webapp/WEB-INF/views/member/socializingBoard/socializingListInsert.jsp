<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>친목 도모 게시판 작성</title>
<style>
.insertArea {
	width: 1200px;
	height: 300px;
}

.btnArea {
	float: right;
}

.divCategory {
	display: inline-block;
}

#areaIntro {
	width: 70%;
	height: 10%;
	resize: none;
}

.marginDiv{
	height : 1000px;
}

</style>
<jsp:include page="../../common/importlib.html" />
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<!-- 상대경로 잘 지정해야함! -->
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<!-- 여기에 내용부분 작성! -->
			<script>
				$(function() {
					$(".ui.dropdown").dropdown();
				});
			</script>
			<div class="hru segment marginDiv">
				<h1>
					<i class="check square icon"></i>글 작성
				</h1>
				<form action="insertSocialList.bo" method="post">
				<table class="hru table tbl">
						<tr>
							<th>이름</th>
							<input type="hidden" name="mid" value="${list[0].mid }">
							<td><c:out value="${ list[0].mname }" /></td>
							<th>카테고리</th>
							<td><select class="ui dropdown" id="category"
								onchange="sel();" name="category">
									<option value="" selected disabled hidden>카테고리</option>
									<option value="1">요리</option>
									<option value="2">운동</option>
									<option value="3">토익</option>
									<option value="4">봉사</option>
									<option value="5">문화</option>
									<option value="6">소통</option>
							</select></td>
						</tr>
						<tr>
							<th>이메일</th>
							<td><c:out value="${list[0].email }" /></td>
							<th>전화번호</th>
							<td><c:out value="${list[0].phone }" /></td>
						</tr>
						<tr>
							<th>모임 이름</th>
							<td><div class="ui mini input">
									<input type="text" id="leader" name="title">
								</div></td>
							<th>부서</th>
							<td><c:out value="${list[0].teamname }" /></td>
						</tr>
						<tr>
							<th>모집 인원</th>
							<td><div class="ui mini input">
									<input type="text" id="leader" name="meetperson">
								</div></td>
							<th>나이</th>
							<td><c:out value="${list[0].ssn }" /></td>
						</tr>
						<tr>
							<th>모임 일자</th>
							<td><div class="ui calendar" id="rangestart">
										<div class="ui input left icon">
											<i class="calendar icon"></i> <input type="text"
												name="meetingdate" placeholder="meet" readonly>
										</div>
									</div>
								</td>
							<th>마감일자</th>
							<td><div class="ui calendar" id="rangeend">
									<div class="ui input left icon">
										<i class="calendar icon"></i> <input type="text"
											name="meetenddate" placeholder="End" readonly>
									</div>
								</div></td>
						</tr>
						<tr>
							<th>한줄 소개</th>
							<td colspan="3"><textarea id="areaIntro" name="content"></textarea></td>
						</tr>
					</table>										
					<div class="btnArea">
						<button type="reset" class="ui blue button"
							onclick="location.href='selectSocializingList.bo'">취소</button>
						<button type="submit" class="ui blue button">작성</button>
					</div>
				</form>
			</div>
			</div>
		</div>
		
	<!-- 내용 끝 -->
	<!-- outer end -->
	<script>
		$("#rangestart").calendar({
			type : 'date',
			endCalendar : $('#rangeend'),
			formatter : {
				date : function(date, settings) {
					if (!date)
						return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					return year + '-' + month + '-' + day;
				}
			}
		});
		$("#rangeend").calendar({
			type : 'date',
			startCalendar : $('#rangestart'),
			formatter : {
				date : function(date, settings) {
					if (!date)
						return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					return year + '-' + month + '-' + day;
				}
			}
		});
	</script>
	<script>
		$(function() {
			$("#member4").show();
		});

		function sel() {
			var sel = document.getElementById("category").value;
			$.ajax({
				url : "selInsertSocial.bo",
				type : "post",
				data : {
					sel : sel
				},
				success : function(data) {
					console.log(data.sel);
				},
				error : function(data) {
					console.log("실패");
				}
			});
		}
	</script>
</body>
</html>