<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>친목 도모 게시판 글쓴이용</title>
<style>
.areaApply {
	width: 800px;
	height: 110px;
}

.btnArea {
	float: right;
}

.input {
	width: 100px;
}
</style>
<jsp:include page="../../common/importlib.html" />
<!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<!-- 상대경로 잘 지정해야함! -->
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<form id="updateForm" method="post">
				<div>
					<div>
						<div class="ui horizontal segments">
							<div class="ui segment">
								<h1>
									<i class="check square icon"></i>사원 친목 도모 게시판
								</h1>

								<table class="hru table">
									<tr>
										<th>카테고리</th>
										<td><c:choose>
												<c:when test="${list1[0].category == 1}">
													<c:out value="요리" />
												</c:when>
												<c:when test="${list1[0].category == 2}">
													<c:out value="운동" />
												</c:when>
												<c:when test="${list1[0].category == 3}">
													<c:out value="토익" />
												</c:when>
												<c:when test="${list1[0].category == 4}">
													<c:out value="봉사" />
												</c:when>
												<c:when test="${list1[0].category == 5}">
													<c:out value="문화" />
												</c:when>
												<c:otherwise>
													<c:out value="소통" />
												</c:otherwise>
											</c:choose></td>
										<td colspan="2"><input type="hidden" name="friendid"
											value="${list1[0].friendid}"></td>
										<th>모집 인원</th>
										<td><div class="ui mini input mini">
												<input type="text" id="leader" name="meetperson"
													value="${list1[0].meetperson}">
											</div></td>
									</tr>
									<tr>
										<th>제목</th>
										<td><div class="ui mini input mini">
												<input type="text" id="leader" name="title"
													value="${list1[0].title}">
											</div></td>
										<th>모임 일자</th>
										<td>
											<div class="ui calendar" id="rangestart">
												<div class="ui input left icon mini">
													<i class="calendar icon"></i> <input name="meetingdate"
														type="text" value="${list1[0].meetingdate }"
														readonly>
												</div>
											</div>
										</td>
										<th>마감 일자</th>
										<td><div class="ui calendar" id="rangeend">
												<div class="ui input left icon mini">
													<i class="calendar icon"></i> <input name="meetenddate"
														type="text"  value="${list1[0].meetenddate }"
														readonly>
												</div>
											</div></td>
									</tr>
								</table>

								<div>
									<h3>소개</h3>
									<textarea class="areaApply" name="content"
										style="resize: none;"><c:out
											value="${list1[0].content}" /></textarea>
								</div>
							</div>

							<div class="ui segment" style="width: 400px;">
								<h1>
									<i class="check square icon"></i>모임장 정보
								</h1>
								<table class="hru table">
									<tr>
										<th>모임장 이름</th>
										<td><c:out value="${list1[0].mname}" /></td>
									</tr>
									<tr>
										<th>전화번호</th>
										<td colspan="3"><c:out value="${list1[0].phone}" /></td>
									</tr>
									<tr>
										<th>나이</th>
										<td><c:out value="${list1[0].ssn}" /></td>
									</tr>
									<tr>
										<th>이메일</th>
										<td><c:out value="${list1[0].email}" /></td>
									</tr>
									<tr>
										<th>부서</th>
										<td><c:out value="${list1[0].teamname}" /></td>
									</tr>
								</table>
							</div>
						</div>
					</div>

					<br> <br>
					<div>
						<div class="ui horizontal segments divide">
							<div class="ui segment">
								<h1>
									<i class="check square icon"></i>신청자 정보
								</h1>
								<table class="hru table">
									<thead>
										<tr>
											<th>참가자 이름</th>
											<th>전화번호</th>
											<th>이메일</th>
											<th>나이</th>
											<th><c:choose>
													<c:when test="${list1[0].category == 1}">
														<c:out value="요리실력" />
													</c:when>
													<c:when test="${list1[0].category == 2}">
														<c:out value="운동실력" />
													</c:when>
													<c:when test="${list1[0].category == 3}">
														<c:out value="토익점수" />
													</c:when>
													<c:when test="${list1[0].category == 4}">
														<c:out value="봉사경험" />
													</c:when>
													<c:when test="${list1[0].category == 5}">
													</c:when>
													<c:otherwise>
													</c:otherwise>
												</c:choose></th>
											<th>참가 여부</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="ssb2" items="${ list2 }">
											<tr>
												<td><c:out value="${ssb2.mname}" /></td>
												<td><c:out value="${ssb2.phone}" /></td>
												<td><c:out value="${ssb2.email}" /></td>
												<td><c:out value="${ssb2.ssn}" /></td>
												<td><c:choose>
														<c:when test="${list1[0].category == 1}">
															<c:out value="${ssb2.plusinfo }" />
														</c:when>
														<c:when test="${list1[0].category == 2}">
															<c:out value="${ssb2.plusinfo }" />
														</c:when>
														<c:when test="${list1[0].category == 3}">
															<c:out value="${ssb2.plusinfo }" />
														</c:when>
														<c:when test="${list1[0].category == 4}">
															<c:out value="${ssb2.plusinfo }" />
														</c:when>
														<c:when test="${list1[0].category == 5}">
														</c:when>
														<c:otherwise>
														</c:otherwise>
													</c:choose></td>
												<td><select class="ui dropdown divCategory"
													name="applyyn">
														<option value="" hidden selected disabled>${ssb2.applyyn}</option>
														<option value="Y">Y</option>
														<option value="N">N</option>
												</select></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="btnArea">
						<button type="submit" class="ui blue button"
							onclick="updateInfo();">수정</button>
						<button type="submit" class="ui blue button"
							onclick="deleteInfo();">삭제</button>
					</div>
				</div>
			</form>
		</div>
		<!-- outer end -->
	</div>
	<script>
		$(function() {
			$(".ui.dropdown").dropdown();
		});

		$("#rangestart").calendar({
			type : 'date',
			endCalendar : $('#rangeend'),
			formatter : {
				date : function(date, settings) {
					if (!date)
						return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					return year + '-' + month + '-' + day;
				}
			}
		});
		$("#rangeend").calendar({
			type : 'date',
			startCalendar : $('#rangestart'),
			formatter : {
				date : function(date, settings) {
					if (!date)
						return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					return year + '-' + month + '-' + day;
				}
			}
		});

		$(function() {
			$("#member4").show();
		});

		function updateInfo() {
			$("#updateForm").attr("action", "infoUpdate.bo");
		}

		function deleteInfo() {
			$("#updateForm").attr("action", "infoDelete.bo");
		}
	</script>
</body>
</html>