<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>친목 도모 게시판 신청</title>
<style>
.areaApply {
	width: 800px;
	height: 110px;
}

.btnArea {
	float: right;
}

.updateInfoArea {
	text-align: center;
}

.content {
	text-align: center;
}
</style>
<jsp:include page="../../common/importlib.html" />
<!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<!-- 상대경로 잘 지정해야함! -->
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<!-- 여기에 내용부분 작성! -->
			<div>
				<div>
					<div class="ui horizontal segments">
						<div class="ui segment">
							<h1>
								<i class="check square icon"></i>사원 친목 도모 게시판
							</h1>
							<table class="hru table">
								<tr>
									<th>카테고리</th>
									<td><c:choose>
											<c:when test="${list1[0].category == 1}">
												<c:out value="요리" />
											</c:when>
											<c:when test="${list1[0].category == 2}">
												<c:out value="운동" />
											</c:when>
											<c:when test="${list1[0].category == 3}">
												<c:out value="토익" />
											</c:when>
											<c:when test="${list1[0].category == 4}">
												<c:out value="봉사" />
											</c:when>
											<c:when test="${list1[0].category == 5}">
												<c:out value="문화" />
											</c:when>
											<c:otherwise>
												<c:out value="소통" />
											</c:otherwise>
										</c:choose></td>
									<td colspan="2"></td>
									<th>모집 인원</th>
									<td><c:out value="${list1[0].meetperson}" /></td>
								</tr>
								<tr>
									<th>제목</th>
									<td><c:out value="${list1[0].title}" /></td>
									<th>모임 일자</th>
									<td><c:out value="${list1[0].meetingdate }" /></td>

									<th>마감 일자</th>
									<td><c:out value="${list1[0].meetenddate }" /></td>
								</tr>
							</table>
							<div>
								<h3>소개</h3>
								<div
									style="width: 700px; height: 100px; border: 1px solid black;">
									<c:out value="${list1[0].content}" />
								</div>
							</div>
						</div>

						<div class="ui segment" style="width: 400px;">
							<h1>
								<i class="check square icon"></i>모임장 정보
							</h1>
							<table class="hru table" id="tblWriter">
								<tr>
									<th>모임장 이름</th>
									<td><c:out value="${list1[0].mname }" /></td>
								</tr>
								<tr>
									<th>전화번호</th>
									<td colspan="3"><c:out value="${list1[0].phone }" /></td>
								</tr>
								<tr>
									<th>나이</th>
									<td><c:out value="${list1[0].ssn }" /></td>
								</tr>
								<tr>
									<th>이메일</th>
									<td><c:out value="${list1[0].email }" /></td>
								</tr>
								<tr>
									<th>부서</th>
									<td><c:out value="${list1[0].teamname}" /></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<br> <br>
				<div>
					<div class="ui horizontal segments divide">
						<div class="ui segment">
							<h1>
								<i class="check square icon"></i>신청자 정보
							</h1>
							<table class="hru board" id="tblApply">
								<thead>
									<tr>
										<th>참가자 이름</th>
										<th>나이</th>
										<th><c:choose>
												<c:when test="${list1[0].category == 1}">
													<c:out value="요리 실력" />
												</c:when>
												<c:when test="${list1[0].category == 2}">
													<c:out value="운동 실력" />
												</c:when>
												<c:when test="${list1[0].category == 3}">
													<c:out value="토익 점수" />
												</c:when>
												<c:when test="${list1[0].category == 4}">
													<c:out value="봉사 경험" />
												</c:when>
												<c:when test="${list1[0].category == 5}">
													<c:out value="" />
												</c:when>
												<c:otherwise>
													<c:out value="" />
												</c:otherwise>
											</c:choose></th>
										<th>참가 여부</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="ssb2" items="${ list2 }">
										<tr>
											<input type="hidden" name="fmemberid"
												value="${ ssb2.fmemberid }">
											<td><c:out value="${ssb2.mname }" /></td>
											<td><c:out value="${ssb2.ssn}" /></td>
											<td><c:choose>
													<c:when test="${ssb2.plusinfo == null}">
														<c:out value="" />
													</c:when>
													<c:otherwise>
														<c:out value="${ssb2.plusinfo}" />
													</c:otherwise>
												</c:choose></td>
											<td><c:out value="${ssb2.applyyn }" /></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<script>
							$(function() {
								$(".ui.dropdown").dropdown();
							});
						</script>
						<div class="ui segment" style="width: 100px;">
							<h1>
								<i class="check square icon"></i>나의 정보
							</h1>
							<form action="" method="post" id="applyForm">
								<table class="hru table" id="tblMy">
									<tr>
										<th>이름</th>
										<input type="hidden" name="friendid" value="${ friendid }">
										<input type="hidden" name="mid" value="${ mid }">
										<td><c:out value="${list3[0].mname}" /></td>
									</tr>
									<tr>
										<th>전화번호</th>
										<td><c:out value="${list3[0].phone}" /></td>
									</tr>
									<tr>
										<th>나이</th>
										<td><c:out value="${list3[0].ssn}" /></td>
									</tr>
									<tr>
										<th><c:choose>
												<c:when test="${list1[0].category == 1}">
													<c:out value="요리 실력" />
												</c:when>
												<c:when test="${list1[0].category == 2}">
													<c:out value="운동 실력" />
												</c:when>
												<c:when test="${list1[0].category == 3}">
													<c:out value="토익 점수" />
												</c:when>
												<c:when test="${list1[0].category == 4}">
													<c:out value="봉사 경험" />
												</c:when>
												<c:when test="${list1[0].category == 5}">
													<c:out value="" />
												</c:when>
												<c:otherwise>
													<c:out value="" />
												</c:otherwise>
											</c:choose></th>
										<td><c:choose>
												<c:when test="${list1[0].category == 1}">
													<select class="ui dropdown divCategory" id="plusinfo"
														name="plusinfo">
														<option value="" selected disabled hidden>추가 기입
															사항</option>
														<option value="상">상</option>
														<option value="중">중</option>
														<option value="하">하</option>
													</select>
												</c:when>
												<c:when test="${list1[0].category == 2}">
													<select class="ui dropdown divCategory" id="plusinfo"
														name="plusinfo">
														<option value="" selected disabled hidden>추가 기입
															사항</option>
														<option value="상">상</option>
														<option value="중">중</option>
														<option value="하">하</option>
													</select>
												</c:when>
												<c:when test="${list1[0].category == 3}">
													<div class="ui mini input">
														<input type="text" id="leader" name="plusinfo"
															placeholder="토익 점수">
													</div>
												</c:when>
												<c:when test="${list1[0].category == 4}">
													<select class="ui dropdown divCategory" id="plusinfo"
														name="plusinfo">
														<option value="" selected disabled hidden>추가 기입
															사항</option>
														<option value="Y">Y</option>
														<option value="N">N</option>
													</select>
												</c:when>
												<c:when test="${list1[0].category == 5}">
													<input type="hidden" name="plusinfo">
												</c:when>
												<c:otherwise>
													<input type="hidden" name="plusinfo">
												</c:otherwise>
											</c:choose></td>
									</tr>
									<tr>
										<th>부서</th>
										<td><c:out value="${list3[0].teamname}" /></td>
									</tr>

								</table>
								<div class="btnArea">
									<button type="reset" class="ui blue button"
										onclick="location.href='selectSocializingList.bo'">뒤로가기</button>
									<button type="button" class="ui blue button"
										onclick="return reAlert();">신청</button>
								</div>
							</form>

							<%-- <div class="ui tiny test modal">
								<div class="header">추가 기입 사항 수정 혹은 신청취소</div>
								<div class="content">
									<p>
										<c:choose>
											<c:when test="${list1[0].category == 1}">
												<c:out value="요리 실력 수정" />
											</c:when>
											<c:when test="${list1[0].category == 2}">
												<c:out value="운동 실력 수정" />
											</c:when>
											<c:when test="${list1[0].category == 3}">
												<c:out value="토익 점수 수정" />
											</c:when>
											<c:when test="${list1[0].category == 4}">
												<c:out value="봉사 경험 수정 " />
											</c:when>
											<c:when test="${list1[0].category == 5}">
												<c:out value="수정할 사항이 없습니다." />
											</c:when>
											<c:otherwise>
												<c:out value="수정할 사항이 없습니다." />
											</c:otherwise>
										</c:choose>
									</p>
								</div>
								<div class="updateInfoArea">
									<c:choose>
										<c:when test="${list1[0].category == 1}">
											<select class="ui dropdown divCategory updatePlusInfo"
												name="updatePlusInfo">
												<option value="" selected disabled hidden>추가 기입 사항</option>
												<option value="상">상</option>
												<option value="중">중</option>
												<option value="하">하</option>
											</select>
										</c:when>
										<c:when test="${list1[0].category == 2}">
											<select class="ui dropdown divCategory updatePlusInfo"
												name="updatePlusInfo">
												<option value="" selected disabled hidden>추가 기입 사항</option>
												<option value="상">상</option>
												<option value="중">중</option>
												<option value="하">하</option>
											</select>
										</c:when>
										<c:when test="${list1[0].category == 3}">
											<div class="ui mini input">
												<input name="updatePlusInfo" class="updatePlusInfo"
													placeholder="토익 점수">
											</div>
										</c:when>
										<c:when test="${list1[0].category == 4}">
											<select class="ui dropdown divCategory updatePlusInfo"
												name="updatePlusInfo">
												<option value="" selected disabled hidden>추가 기입 사항</option>
												<option value="Y">Y</option>
												<option value="N">N</option>
											</select>
										</c:when>
										<c:when test="${list1[0].category == 5}">
											<input type="hidden" name="updatePlusInfo"
												class="updatePlusInfo">
										</c:when>
										<c:otherwise>
											<input type="hidden" name="updatePlusInfo"
												class="updatePlusInfo">
										</c:otherwise>
									</c:choose>
								</div>
								<div class="actions">
									<button class="ui red button"
										onclick="cancelNo();">뒤로가기</button>
									<button class="ui blue button"
										onclick="cancelApply();">신청 취소</button>
									<button type="submit" class="ui blue button"
										onclick="updateInfo();">수정</button>
								</div>
							</div>
						</div> --%>

							<div class="ui mini test modal">
								<div class="header">신청 취소 하시겠습니까?</div>
								<div class="actions">
									<button class="ui red button" onclick="cancelNo();">뒤로가기</button>
									<button type="submit" class="ui blue button"
										onclick="cancelApply();">Yes</button>
								</div>
							</div>
						</div>
						<!-- outer end -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		var fmemberid = 0;

		$("#tblApply tr").click(function(){
					var tr = $(this);
					var td = tr.children().eq(1);
					var name2 = td.eq(0).text();
					var name3 = $("#tblMy tr td").eq(0).text();
					fmemberid = $(this).find("td").parent().children("input").eq(0).val();
					
					//console.log(fmemberid);

					if (name2 == name3) {
						$('.mini.modal').modal('show');
					} else {
						Swal.fire({
							icon : 'error',
							title : 'Oops...',
							text : '자기 것만 선택할 수 있습니다!',
							footer : ''
						})
					}

				})

		$(function() {
			$("#member4").show();

		});

		function reAlert() {
			var name1 = $("#tblWriter tr td").eq(0).text();
			var name3 = $("#tblMy tr td").eq(0).text();

			var count = 0;

			<c:forEach var="ssb2" items="${ list2 }">
			if ((name1 == name3) || ("${ssb2.mname}" == name3)) {
				count++;
			}
			</c:forEach>

			if ((count == 0) && (name1 != name3)) {
				var action = document.forms.applyForm;
				$("#applyForm").attr("action", "applySocialList.bo");
				$("#applyForm").submit();

			} else if (name1 == name3) {
				Swal.fire({
					icon : 'error',
					title : 'Oops...',
					text : '모임장은 신청 불가 합니다!',
					footer : ''
				});
			} else {
				Swal.fire({
					icon : 'error',
					title : 'Oops...',
					text : '중복 신청 불가합니다!!',
					footer : ''
				})
			}

		};
		function cancelNo() {
			$('.mini.modal').modal('hide');

		}

		/* function cancelModal() {
			$('.tiny.modal').modal('hide');
		}
		 */
		function updateInfo() {
			var friendid = $("#tblMy").find("td").parent().children("input")
					.eq(0).val();
			var fmemberid = $("#tblApply").find("td").parent()
					.children("input").eq(0).val();
			var updatePlusInfo = $(".updatePlusInfo").val();

			location.href = "updatePlusInfo.bo?fmemberid=" + fmemberid
					+ "&updatePlusInfo=" + updatePlusInfo + "&friendid="
					+ friendid;
		}

		function cancelApply() {
		
			var friendid = $("#tblMy").find("td").parent().children("input")
					.eq(0).val();

			console.log(fmemberid);
//			console.log(friendid);

			location.href = "cancelApply.bo?fmemberid=" + fmemberid	+ "&friendid=" + friendid;

		}
	</script>
</body>
</html>