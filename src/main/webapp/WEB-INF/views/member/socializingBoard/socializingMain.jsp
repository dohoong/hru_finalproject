<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>사원 친목 도모 게시판 메인</title>
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<jsp:include page="../../common/importlib.html"/>
<style>
	body{
		background:#ededed;
	}
	#headMenu{
		font-family: 'Noto Sans KR', sans-serif;
		color:#0F4C81;
		font-weight:bold;
		text-align:left;
		margin-left:15%;
		margin-top:3%;
	}
	#menuName{
		color:#0F4C81;
		font-family: 'Noto Sans KR', sans-serif;
		font-weight:bold;
		font-size:15px;
	}
	#outer{
		margin-left:auto;
		margin-right:auto;
		margin-top:10%;
	}
	.image{
		magin:auto auto;
	}
	.ui.segment{
		width:150px;
		height:150px;
		margin-left:auto;
		margin-right:auto;
	}
	.ui.segment:hover{
		cursor:pointer;
	}
	.menuIcon{
		height:70px;
	}
	.tblArea td{
		width:190px;
	}
	.tblArea tr{
		height:210px;
	}
	#inner{
		margin-top:3%;
	}
	.tblArea{
		margin-top:3%;
	}

</style>
</head>
<body>
	<jsp:include page="../../common/menubar.jsp"/>
	<div class="outer">
		<div class="contentArea" align="center">
			<h2 id="headMenu">
				<i class="users icon"></i>사원 친목 도모 게시판
			</h2>
			<table class="tblArea">
				<tr>
					<td>
						<div class="ui segment" align="center">
							<div id="inner"
								onclick="location.href='selectSocializingList.bo'">
								<img class="menuIcon" name="filterCondition"
									src="resources/images/socializingImages/cook.png">
								<h3 id="menuName" class="menuName">요리</h3>
							</div>
						</div>
					</td>
					<td>
						<div class="ui segment" align="center">
							<div id="inner"
								onclick="location.href='selectSocializingList2.bo'">
								<img class="menuIcon" name="filterCondition"
									src="resources/images/socializingImages/sports.png">
								<h3 id="menuName" class="menuName">운동</h3>
							</div>
						</div>
					</td>
					<td>
						<div class="ui segment" align="center">
							<div id="inner"
								onclick="location.href='selectSocializingList3.bo'">
								<img class="menuIcon"
									src="resources/images/socializingImages/toeic.png">
								<h3 id="menuName" class="menuName">토익</h3>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="ui segment" align="center">
							<div id="inner"
								onclick="location.href='selectSocializingList4.bo'">
								<img class="menuIcon"
									src="resources/images/socializingImages/volunteer.png">
								<h3 id="menuName" class="menuName">봉사</h3>
							</div>
						</div>
					</td>
					<td>
						<div class="ui segment" align="center">
							<div id="inner"
								onclick="location.href='selectSocializingList5.bo'">
								<img class="menuIcon"
									src="resources/images/socializingImages/culture.png">
								<h3 id="menuName" class="menuName">문화</h3>
							</div>
						</div>
					</td>
					<td>
						<div class="ui segment" align="center">
							<div id="inner"
								onclick="location.href='selectSocializingList6.bo'">
								<img class="menuIcon"
									src="resources/images/socializingImages/talk.png">

								<h3 id="menuName" class="menuName">소통</h3>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<script>
		$(function() {
			$("#member4").show();
		});
	</script>
</body>
</html>