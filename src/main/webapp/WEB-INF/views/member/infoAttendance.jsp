<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	#ipInfo{
		margin:0 auto;
	}
	.searchArea{
		padding-top:7px;
	}
	#monthAt{
		background:#F5F5F5;
	}
	.atInput{
		width:60px;
	}
	#attend{
		color:#387bb5;
	}
	#late{
		color:#c9485b;
	}
	#special{
		color:#fddb3a;	
	}
	#vacation{
		color:#91bd3a;
	}
	#goWorkModal, #leaveModal{
		width:500px;
		heigth:300px;
	}
	.i.black.deny.button{
		color:blue;
	}
	.ui.modal>.image.content{
		display:block;
		text-align:center;
		padding-top: 80px;
	}
	.ui.modal>.actions{
		background:white;
		border-top:none;
		text-align:center;
		padding-bottom: 60px;
	}
	.ui.modal .actions>.button{
		background:#387bb5;
	}
	.hru.segment.infoAttendance{
		margin-bottom:2%;
	}
	.ui.input>.atInput{
		text-align:center;
		font-weight:bold;
	}
</style>
</head>
<body>
<jsp:include page="../common/menubar.jsp"/>
<div class="outer"><!--outer Area -->
<div class="contentArea"> <!-- 내용부분! -->
		<div class="ui top attached tabular menu">
				<a class="active item" data-tab="first">일일근태등록</a> 
				<a class="item" data-tab="second">월 근태현황</a>
				<a class="item" data-tab="third">근태 신청</a>
		</div>
		
		<!-- 일일근태등록 -->
		<div class="ui bottom attached active tab segment" data-tab="first">
			<div class="hru segment infoAttendance">
				<div class="searchArea" style="height:70px;">
					<form action="" method="get">
					<table align="center" cellspacing="10px">
						<tr>
							<td><h3>근무 일자 </h3></td>
							<td>
								<div class="ui left icon input">
								<c:if test="${aMap.da.dattDate != null}">
								  <input type="date" id="today" value="${aMap.da.dattDate}" readonly>
								</c:if>
								<c:if test="${aMap.da.dattDate == null}">
									 <input type="date" id="today" value="" readonly>
									 <script>
									 var dt = new Date();
	
										var recentYear = dt.getFullYear();
										var recentMonth = dt.getMonth() + 1;
										var recentDay = dt.getDate();
	
										if(recentMonth < 10) recentMonth = "0" + recentMonth;
										if(recentDay < 10) recentDay = "0" + recentDay;
										$("#today").val(recentYear + "-" + recentMonth + "-" + recentDay);
									 </script>
								 	
								</c:if>
								  <i class="calendar check icon"></i>
								</div>
							</td>
						</tr>
					</table>
					</form>
				</div><!-- searchArea end -->
			</div>
			
			<!-- 접속 아이피 / 출퇴근 버튼  -->
			<div class="hru segment" id="ipInfo">
				<div class="buttonArea">
					
					<c:if test="${aMap.useYN == 1 }">	<!-- ip대역폭 사용 -->
					<label id="ipLabel" style="float:left"><i class="plug icon"></i>&nbsp;</label>
					<c:if test="${aMap.ipUse > 0}">
					<label style="float:left; font-weight:bold" id="memberIp"><c:out value="출퇴근 허가 IP"/></label>
					</c:if>
					<c:if test="${aMap.ipUse == 0}">
					<label style="float:left; font-weight:bold;color:red;" id="memberIp"><c:out value="출퇴근 허가되지 않은 IP"/></label>
					</c:if>
					<c:if test="${aMap.ipUse > 0}">
					<button class="ui blue button mini" id="go">출근</button>
					</c:if>
					<c:if test="${aMap.ipUse == 0}">
					<button class="ui blue button mini" id="go" onclick="forbiddenIP();">출근</button>
					</c:if>
					<c:if test="${aMap.ipUse > 0}">
					<button class="ui blue button mini" id="leave">퇴근</button>
					</c:if>
					<c:if test="${aMap.ipUse == 0}">
					<button class="ui blue button mini" id="leave" onclick="forbiddenIP();">퇴근</button>
					</c:if>
					</c:if>
					<c:if test="${aMap.useYN == 2 }"><!-- ip대역폭 미사용 -->
					<button class="ui blue button mini" id="go">출근</button>
					<button class="ui blue button mini" id="leave">퇴근</button>
					</c:if>
				</div>
			</div><!-- 접속 아이피 / 출퇴근 버튼  끝 -->
			
			<!-- 근태 테이블 -->
			<div class="hru segment">	
				<table class="hru board2" border="1" id="dayAttendanceTable">
						<thead>
							<tr>
								<th>사원번호</th>
								<th>성명</th>
								<th>출근시간</th>
								<th>퇴근시간</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${aMap.m.mid}</td>
								<td>${aMap.m.mName}</td>
								<td id="dayStart">${aMap.da.startDate}</td>
								<td id="dayEnd">${aMap.da.endDate}</td>
							</tr>
						</tbody>
				</table>
			</div>
		</div><!-- 일일근태등록 끝-->
		
		<!-- 월 근태현황 -->
		<div class="ui bottom attached tab segment" data-tab="second">
			<!-- 근태년월 선택란 -->
			<div class="hru segment infoAttendance">
				<div class="searchArea" style="height:70px;">
					<table align="center" cellspacing="10px">
						<tr>
							<td><h3>근무 년월 </h3></td>
							<td>
								<div class="ui calendar" id="month_year_calendar">
									<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" id="maDate" name="workMonth" placeholder="근무년월 선택" readonly>
									</div>
								</div>
							</td>
							<td>
								<button class="ui blue button" id="maBtn">검색하기</button>
							</td>
						</tr>
					</table>
				</div><!-- searchArea end -->
			</div>
			
			<!-- 월 근태 일수 -->
			<div class="hru segment infoAttendance">
			<h3><i class="check square icon"></i>월 근태 일수</h3>
			<label><i class="exclamation circle icon" id="exclamation"></i>월 근태 일수는 월근태마감이 이루워진 달만 계산됩니다.</label>
				<div class="searchArea" style="height:70px;" id="monthAt">
					<table align="center" cellspacing="10px">
						<tr>
							<td><h3>총 근무일수 </h3></td>
							<td><div class="ui input">
  								<input class="atInput" id="totalDay" type="text" readonly></div>
							</td>
							<td><h3>평일 근무일수 </h3></td>
							<td><div class="ui input">
  								<input class="atInput" id="weekendDay" type="text" readonly></div>
							</td>
							<td><h3>휴일 근무일수 </h3></td>
							<td><div class="ui input">
  								<input class="atInput" id="holiDay" type="text" readonly></div>
							</td>
							<td><h3>휴가 사용일수 </h3></td>
							<td><div class="ui input">
  								<input class="atInput" id="vacationDay" type="text" readonly></div>
							</td>
						</tr>
					</table>
				</div><!-- searchArea end -->
				
			</div><!-- 월 근태 일수 끝 -->
			
			<!-- 월 근태 테이블 -->
			<div class="hru segment infoAttendance">
				<h3><i class="check square icon"></i>월근태 현황</h3>
				<div style="float:right">
					<label>출근<i class="ui circle grey icon" id="attend"></i></label>
					<label>지각<i class="ui circle grey icon" id="late"></i></label>
					<label>야특근<i class="ui circle grey icon" id="special"></i></label>
					<label>휴가<i class="ui circle grey icon" id="vacation"></i></label>
				</div>
				
				<table class="hru board2" border="1">
					<thead>
						<tr>
							<th rowspan="2">사원번호</th>
							<th rowspan="2">성명</th>
							<th rowspan="2">부서</th>
							<th rowspan="2">직급</th>
							<c:forEach var="day" begin="1" end="16" varStatus="status">
								<th class="day1Th" id="day1Th${status.count}"></th>
							</c:forEach>
							<th rowspan="2">비고</th>
						</tr>
						<tr>
							<c:forEach var="day" begin="17" end="32"  varStatus="status">
								<c:if test="${day<=32}">
									<th class="day2Th" id="day2Th${status.count}"></th>
								</c:if>
							</c:forEach>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="2"><c:out value="${aMap.mhi.mid}"/></td>
							<td rowspan="2"><c:out value="${aMap.m.mName}"/></td>
							<td rowspan="2"><c:out value="${aMap.mhi.tname}"/></td>
							<td rowspan="2"><c:out value="${aMap.mhi.ppname}"/></td>
							<c:forEach var="day" begin="1" end="16">
								<td class="day1Td"><!-- <i class="ui circle grey icon" id="vacation"></i> --></td>
							</c:forEach>
							<td rowspan="2"></td>
						</tr>
						<tr>
							<c:forEach var="day" begin="17" end="32">
								<c:if test="${day<=31}">
									<td class="day2Td"><i class="ui circle grey icon" style="color:white;" id="attend"></i></td>
								</c:if>
								<c:if test="${day>31}">
									<td class="day1Td"><i class="ui circle grey icon" style="color:white;" id="attend"></i></td>
								</c:if>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div><!-- <!-- 월 근태 테이블 끝-->
		</div><!-- 월 근태현황 끝-->
		
		<!-- 근태신청  -->
		<div class="ui bottom attached tab segment" data-tab="third">
			<div class="hru segment" style="margin-bottom:150px;">
				<h3><i class="check square icon"></i>근태신청</h3>
				<form action="insertSpecialAttendance.att" id="specialAttendanceForm" enctype="multipart/form-data">
			<table class="hru table">
					<tr>
						<th>*기안일자</th>
						<td>
							<div class="ui calendar" id="standard_calendar">
							  <div class="ui input left icon">
							    <i class="calendar icon"></i>
							    <input type="text"  id="requestDate">
							  </div>
							</div>
						</td>
						<th>*근태구분</th>
						<td>
							<select class="ui dropdown" name="category" id="saCategory">
								<option value="">근태구분 선택</option>
								<option value="2">휴일근로</option>
								<option value="1">연장/야간근로</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>*야특근 일자</th>
						<td>
							<div class="ui calendar" id="date_calendar">
							  <div class="ui input left icon">
							    <i class="calendar icon"></i>
							    <input type="text" name="sattDate" placeholder="Date" readonly>
							  </div>
							</div>
							<div class="ui calendar" id="startTime">
							  <div class="ui input left icon">
							    <i class="time icon"></i>
							    <input type="text" name="startTime" placeholder="시작시간" readonly>
							  </div>
							</div>
							<div class="ui calendar" id="date_calendar2">
							  <div class="ui input left icon">
							    <i class="calendar icon"></i>
							    <input type="text" name="endDate" placeholder="Date" readonly>
							  </div>
							</div>
							<div class="ui calendar" id="endTime">
							  <div class="ui input left icon">
							    <i class="time icon"></i>
							    <input type="text" name="endTime" placeholder="종료시간" readonly>
							  </div>
							</div>
						</td>
						<th>전자결재상태</th>
						<td>
							<div class="ui input">
							  <input type="text" value="요청" readonly>
							</div>
						</td>
					</tr>
		</table>
				<div class="buttonArea">
						<input type="hidden" name="title" value="제목" id="title2">
						<input type="hidden" name="firstApproval" id="firstApp2">
						<input type="hidden" name="secondApproval" id="secondApp2">
						<input type="hidden" name="thirdApproval" id="thirdApp2">
						<button class="ui blue button mini" id="add" onclick="return approvalFunction();">전자결재</button>
				</div>
		</form>
			</div>
		</div><!-- 근태신청  끝 -->
		
</div><!-- 내용부분! 끝-->
</div><!--outer Area 끝 -->


	<!-- 출근 모달 -->
	<div class="ui modal" id="goWorkModal">
	  <i class="close icon"></i>
	  <div class="image content" align="center">
	  <h3 style="color:black"><i class="exclamation circle icon" id="exclamation"></i>출근시간 입력완료</h3>
	  </div>
	  <div class="actions">
	    <div class="ui black deny button" onclick="location.href='dayStart.att'">
	      	확인
	    </div>
	  </div>
	</div><!-- 출근 모달 끝 -->
	
	<!-- 퇴근 모달 -->
	<div class="ui modal" id="leaveModal">
	  <i class="close icon"></i>
	  <div class="image content" align="center">
	  <h3 style="color:black"><i class="exclamation circle icon" id="exclamation"></i>퇴근시간 입력완료</h3>
	  </div>
	  <div class="actions">
	    <div class="ui black deny button" onclick="location.href='dayEnd.att'">
	      	확인
	    </div>
	  </div>
	</div><!-- 퇴근 모달 끝 -->
	
	<!-- 근태신청 모달 -->
	<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
<script>
		$(function(){
			/* 상단메뉴 */
			$("#member2").show();
			/* 탭메뉴 */
			$(".menu .item").tab();
			/* 드롭다운 */
			$(".ui.dropdown").dropdown();
			/* 출퇴근 버튼 활성화&비활성화 */
			if($("#dayStart").text()== ""){
				/* 퇴근 비활성화 */
				$("#leave").hide();
			}else if($("#dayEnd").text() ==""){
				$("#go").hide();
			}else if($("#dayStart").text() != ""){
				$("#go").hide();
			}
			/* 전자결재 기안일자 */
			var date1 = getTodayType();
			$("#requestDate").val(date1);
			
			function getTodayType(){
				var date = new Date();
				return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
			}
			
			
			/* 야특근 시작 일자 데이트피커 */
			$('#date_calendar')
				  .calendar({
				    type: 'date',
				    formatter: {
				        date: function (date, settings) {
				        	 if (!date) return '';
				    	      var day = date.getDate();
				    	      var day2;
				    	      var month = date.getMonth() + 1;
				    	      var month2;
				    	      if(month < 10){
				    	    	  month2 = '0' + month;
				    	      }else {
				    	    	  month2 = month;
				    	      }
				    	      if(day < 10){
				    	    	  day2 = '0' + day;
				    	      }else {
				    	    	  day2 = day;
				    	      }
				    	      var year = date.getFullYear();
				    	      return year + '-' + month2 + '-' + day2;
				        }
				      }
			});
			
			/* 야특근 종료 일자 데이트피커 */
			$('#date_calendar2')
				  .calendar({
				    type: 'date',
				    formatter: {
				        date: function (date, settings) {
				        	 if (!date) return '';
				    	      var day = date.getDate();
				    	      var day2;
				    	      var month = date.getMonth() + 1;
				    	      var month2;
				    	      if(month < 10){
				    	    	  month2 = '0' + month;
				    	      }else {
				    	    	  month2 = month;
				    	      }
				    	      if(day < 10){
				    	    	  day2 = '0' + day;
				    	      }else {
				    	    	  day2 = day;
				    	      }
				    	      var year = date.getFullYear();
				    	      return year + '-' + month2 + '-' + day2;
				        }
				      }
			});
			
			
			
			$('#startTime')
			  .calendar({
			    type: 'time',
			    ampm: false,
			    disableMinute: true
			  });
			$('#endTime')
			  .calendar({
			    type: 'time',
			    ampm: false,
			    disableMinute: true
			  });
			
			var dt = new Date();

			var recentYear = dt.getFullYear();
			var recentMonth = dt.getMonth() + 1;
			var recentDay = dt.getDate();

			if(recentMonth < 10) recentMonth = "0" + recentMonth;
			if(recentDay < 10) recentDay = "0" + recentDay;

			return recentYear + "-" + recentMonth + "-" + recentDay;
			$("#today").val(recentYear + "-" + recentMonth + "-" + recentDay);
			
		});
		
		/* 출근 모달 */
		/* function goWork(){
			$('#goWorkModal').modal('show');
		} */
		
		/* 퇴근 모달 */
		function leaveWork(){
			$('#leaveModal').modal('show');
		}
		//출근 버튼 ajax
		$("#go").click(function(){
			$.ajax({
				url:"dayStart.att",
				type:"post",
				success:function(data){
					console.log(data);
					if(data.result == 1){
						Swal.fire(
								  '출근 완료',
								  '출근 시간이 입력되었습니다.',
								  'success'
						).then((result)=>{
							var url = "showInfoAttendance.att";
							$(location).attr('href', url);
						});
					}
				},
				error:function(status){
					console.log("fail!");
				}
			});
		});
		//퇴근 버튼 ajax
		$("#leave").click(function(){
			$.ajax({
				url:"dayEnd.att",
				type:"post",
				success:function(data){
					console.log(data);
					if(data.result == 1){
						Swal.fire(
								  '퇴근 완료',
								  '퇴근 시간이 입력되었습니다.',
								  'success'
						).then((result)=>{
							var url = "showInfoAttendance.att";
							$(location).attr('href', url);
						});
					}
				},
				error:function(status){
					console.log("fail!");
				}
			});
		});
		
		/*월선택 데이트피커 */
		$('#month_year_calendar').calendar({
				    type: 'month',
				    formatter: {
				    	date: function (date, settings) {
				    	      if (!date) return '';
				    	      var month = date.getMonth() + 1;
				    	      var month2;
				    	      var year = date.getFullYear();
				    	      if(month < 10){
				    	    	  month2 = '0' + month;
				    	      }else {
				    	    	  month2 = month;
				    	      }
				    	      return year + '-' + month2;
				    	}
				    },
				    text: {
				        monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
				    }
		});
		
		/* 근태신청 전자결재 */
		function request(){
			$("#approvalModal").modal('show');
		}
		
		
		/* 전자결재 */
		function approvalFunction(){
			var approvalCode = 7; //야특근 신청에 대한 결재코드
			//console.log("전자결재");
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					console.log(data);
					$("#appType").val("야특근신청");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thirdName = data.appBasic.mlist[2].mName;
									var thirdMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
				},
				error : function(status){
					console.log(status);
				}
			});
			
			
			return false;
		}
			
			
			$(function(){
				$('#request').click(function(){
					console.log("눌렸음!");
					
					$("#title2").val($("#title").val());
					$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
					$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
					$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
					
					/* $('#specialAttendanceForm').submit(); */
					
					var params = $("#specialAttendanceForm").serialize();
					$.ajax({
						url:"insertSpecialAttendance.att",
						type:'post',
						data:params,
						success:function(data){
							console.log(data);
							if(data.result == 1){
								Swal.fire(
										  '중복신청',
										  '신청하신 날짜가 중복됩니다.',
										  'warning'
								).then((result)=>{
									$("#approvalModal").modal('hide');
								});
							}else if(data.result == 2){
								Swal.fire(
										  '주 야특근 시간 초과',
										  '주 52시간 근무 초과입니다.',
										  'warning'
								).then((result)=>{
									$("#approvalModal").modal('hide');
								});
							}else if(data.result == 3){
								Swal.fire(
										  '근태 신청 완료',
										  '야특근 신청이 완료되었습니다.',
										  'success'
								).then((result)=>{
									$("#approvalModal").modal('hide');
								});
							}
							
							var content = "결재요청 : " + $("#title").val();
							var tmid = $("#firstApp").find("input[type=hidden]").val();
							if(tmid!="" && tmid!=null && tmid!=0){
								send(tmid, content);
							}
						},
						error:function(status){
							console.log("실패");
						}
					});
				});
			});
			
			/* 월 근태현황 검색 */
			$("#maBtn").click(function(){
				if($("#maDate").val() == ""){
					Swal.fire(
							  '날짜 미선택',
							  '검색할 날짜를 선택해주세요.',
							  'warning'
					)
				}else {
					var maDate = $("#maDate").val();
					$(".day1Th").empty();
					$(".day2Th").empty();
					$(".day1Td").empty();
					$(".day2Td").empty();
					$(".day1Td").css("background", "white");
					$(".day2Td").css("background", "white");
					$.ajax({
						url:"selectMemberMAstatus.att",
						type:"post",
						data:{'maDate':maDate},
						success:function(data){
							console.log(data);
							$("#totalDay").val("");
							$("#weekendDay").val("");
							$("#holiDay").val("");
							$("#vacationDay").val("");
							$("#totalDay").val();
							var ma = data.ma;
							if(ma != null){
								$("#weekendDay").val(ma.weekdayNum);
								$("#holiDay").val(ma.holidayNum);
								$("#vacationDay").val(ma.vacationNum);
								$("#totalDay").val(Number(ma.weekdayNum)+Number(ma.holidayNum));
								console.log("ma is not null");
							}
							var msList = data.msList;
							if(msList.length > 0){
								var n = 0;
								$.each(msList, function(index, i){
									$attendC = $("<i class='ui circle grey icon' id='attend'></i>");
									$lateC = $("<i class='ui circle grey icon' id='late'></i>");
									$specialC = $("<i class='ui circle grey icon' id='special'></i>");
									$vacationC = $("<i class='ui circle grey icon' id='vacation'></i>");
									if(i.day <= 16){
										$(".day1Th").eq(n).text(i.day);
										if(i.status == 1){
											$(".day1Td").eq(n).append($attendC);
										}else if(i.status == 2){
											$(".day1Td").eq(n).append($lateC);
										}else if(i.status == 3){
											$(".day1Td").eq(n).append($specialC);
										}else if(i.status == 4){
											$(".day1Td").eq(n).append($vacationC);
										}
										if(i.weekend == 1){
											$(".day1Td").eq(n).css("background", "#ffedef");
										}
										
									}else if(i.day > 16){
										$(".day2Th").eq(n-16).text(i.day);
										if(i.status == 1){
											$(".day2Td").eq(n-16).append($attendC);
										}else if(i.status == 2){
											$(".day2Td").eq(n-16).append($lateC);
										}else if(i.status == 3){
											$(".day2Td").eq(n-16).append($specialC);
										}else if(i.status == 4){
											$(".day2Td").eq(n-16).append($vacationC);
										}
										if(i.weekend == 1){
											$(".day2Td").eq(n-16).css("background", "#ffedef");
										}
										
									}
									n++;
								});
								
							}
						},
						error:function(status){
							console.log("fail!");
						}
						
					})
				};
				
				
			});
			
			function forbiddenIP(){
				Swal.fire(
					'출퇴근 입력 불가',
					'출퇴근 등록하실 수 없는 IP입니다.',
					'warning'
				)
			}
			
			</script>


</body>
</html>