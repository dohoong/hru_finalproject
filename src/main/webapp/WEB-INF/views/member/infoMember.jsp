<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>인적사항</title>
<style>
#imgArea {
	width:202px;
	height:205px;
	margin-bottom:8px;
	overflow:hidden;
	object-fit:cover;
}

.img {
	width:202px;
	height:205px;
	padding-top:2px;
	overflow:hidden;
	object-fit:cover;
}

#imgArea:hover {
	cursor:pointer;
}
</style>
<jsp:include page="../common/importlib.html"/>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	<div class="outer">
		<!--outer Area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<div class="ui top attached tabular menu">
				<a class="active item" data-tab="first">인적사항</a>
				<a class="item" data-tab="second">가족사항</a>
			</div>
			<!-- 인적사항 -->
			<div class="ui bottom attached active tab segment" data-tab="first">
				<div class="hru segment">
					<table style="width: 100%; margin-bottom: 20px;" id="showTable">
						<tr>
							<td width="202px">
								<c:if test="${mi.changeName != null}">
									<img class="img" src="resources/uploadFiles/${mi.changeName}">
								</c:if>
								<c:if test="${mi.changeName == null}">
									<img class="img" src="resources/images/defaultProfile.png" class="img">
								</c:if>
							</td>
							<td>
								<table class="hru table2" style="margin-left: 10px">
									<tr>
										<th>사원번호</th>
										<td><c:out value="${mi.mid}" /></td>
										<th>재직상태</th>
										<td><c:choose>
												<c:when test="${mi.workStatus eq 1}"><c:out value="재직중"/></c:when>
												<c:when test="${mi.workStatus eq 2}"><c:out value="퇴직 (${mi.outDate})"/></c:when>
												<c:when test="${mi.workStatus eq 3}"><c:out value="휴직"/></c:when>
											</c:choose>
											<c:out value="/ ${mi.enterName}"/>
										</td>
									</tr>
									<tr>
										<th>성명</th>
										<td><c:out value="${mi.mName}"/></td>
										<th>주민등록번호</th>
										<td><c:out value="${fn:substring(mi.ssn,0,fn:length(mi.ssn)-6)}******"/></td>
									</tr>
									<tr>
										<th>전화번호</th>
										<td><c:out value="${mi.phone}"/></td>
										<th>이메일</th>
										<td><c:out value="${mi.email}"/></td>
									</tr>
									<tr>
										<th>내외국인</th>
										<td>
											<c:if test="${mi.korean eq 'Y'}">
												<c:out value="내국인"/>
											</c:if>
											<c:if test="${mi.korean eq 'N'}">
												<c:out value="외국인"/>
											</c:if>
										</td>
										<th>국적</th>
										<td><c:out value="${mi.country}"/></td>
									</tr>
									<tr>
										<th>주소</th>
										<td colspan="3"><c:out value="${mi.address}"/></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<form action="updateMemberInfo.me" method="post" id="updateForm" enctype="multipart/form-data">
						<input type="hidden" name="attId" value="${mi.attachmentId}">
						<table style="width: 100%; margin-bottom: 20px; display: none;" id="updateTable">
							<tr>
								<td width="202px" align="center">
									<div id="imgArea">
										<c:if test="${mi.changeName != null}">
											<img class="img" src="resources/uploadFiles/${mi.changeName}" id="profileImg">
										</c:if>
										<c:if test="${mi.changeName == null}">
											<img class="img" src="resources/images/defaultProfile.png" id="profileImg">
										</c:if>
									</div>
									<input type="file" id="imgFile" name="imgFile" onchange="loadImg(this)" accept="image/*">
									<button class="mini ui button" id="imgBtn" type="button"><i class="file image outline icon"></i>사진첨부</button>
								</td>
								<td>
									<table class="hru table4" style="margin-left: 10px">
										<tr>
											<th>사원번호</th>
											<td>
												<div class="ui small input disabled">
													<input type="text" name="mid" value="${mi.mid}">
												</div>
											</td>
											<th>재직상태</th>
											<td>
												<c:choose>
													<c:when test="${mi.workStatus eq 1}"><c:out value="재직중"/></c:when>
													<c:when test="${mi.workStatus eq 2}"><c:out value="퇴직 (${mi.outDate})"/></c:when>
													<c:when test="${mi.workStatus eq 3}"><c:out value="휴직"/></c:when>
												</c:choose>
												<c:out value="/ ${mi.enterName}"/>
											</td>
										</tr>
										<tr>
											<th>성명</th>
											<td><div class="ui small input"><input type="text" name="name" value="${mi.mName}"></div></td>
											<th>주민등록번호</th>
											<td>
												<div class="ui small input"><input type="text" maxlength="6" size="6" name="ssn1" value="${fn:split(mi.ssn,'-')[0]}"></div>&nbsp;-&nbsp;
												<div class="ui small input"><input type="text" maxlength="7" size="7" name="ssn2" value="${fn:split(mi.ssn,'-')[1]}"></div>
											</td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td>
												<div class="ui small input"><input type="text" maxlength="3" size="3" name="phone1" value="${fn:split(mi.phone,'-')[0]}"></div>&nbsp;-&nbsp;
												<div class="ui small input"><input type="text" maxlength="4" size="4" name="phone2" value="${fn:split(mi.phone,'-')[1]}"></div>&nbsp;-&nbsp;
												<div class="ui small input"><input type="text" maxlength="4" size="4" name="phone3" value="${fn:split(mi.phone,'-')[2]}"></div>
											</td>
											<th>이메일</th>
											<td><div class="ui small input"><input type="text" name="email" value="${mi.email}"></div></td>
										</tr>
										<tr>
											<th>내외국인</th>
											<td>
												<select class="ui compact dropdown" name="korean">
													<option value="Y" <c:if test="${mi.korean eq 'Y'}"> selected</c:if>>내국인</option>
													<option value="N" <c:if test="${mi.korean eq 'N'}"> selected</c:if>>외국인</option>
												</select>
											</td>
											<th>국적</th>
											<td><div class="ui small input"><input type="text" name="country" value="${mi.country}"></div></td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3"><div class="ui small input"><input type="text" size="80" name="address" value="${mi.address}"></div></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</form>

					<table class="hru table">
						<tr>
							<th>사업장</th>
							<td><c:out value="${mi.groupName}"/></td>
							<th>부서</th>
							<td><c:out value="${mi.teamName}"/></td>
							<th>직종</th>
							<td><c:out value="${mi.jobName}"/></td>
						</tr>
						<tr>
							<th>직위</th>
							<td><c:out value="${mi.ppName}"/></td>
							<th>직급</th>
							<td><c:out value="${mi.positionName}"/></td>
							<th>직책</th>
							<td><c:out value="${mi.responsibilityName}"/></td>
						</tr>
						<tr>
							<th>입사일자</th>
							<td><c:out value="${mi.enterDate}"/></td>
							<th>급여형태</th>
							<td>
								<c:choose>
									<c:when test="${mi.salaryType eq 1}"><c:out value="연봉"/></c:when>
									<c:when test="${mi.salaryType eq 2}"><c:out value="월급"/></c:when>
									<c:when test="${mi.salaryType eq 3}"><c:out value="시급"/></c:when>
								</c:choose>
							</td>
							<th>급여액</th>
							<td><fmt:formatNumber value="${mi.salaryAmount}" pattern="#,###" /> 원</td>
						</tr>
					</table>
					<div style="margin-top: 20px; float: right;">
						<button class="ui blue button" id="changePass">비밀번호 변경</button>
						<button class="ui blue button" id="showUpdateBtn">정보수정</button>
						<button class="ui blue button" style="display: none" id="saveUpdateBtn">저장</button>
						<button class="ui button" style="display: none" id="cancelUpdateBtn">취소</button>
					</div>
				</div>
			</div>
			<!-- 인적사항 끝-->

			<!-- 가족사항 -->
			<div class="ui bottom attached tab segment" data-tab="second">
				<div class="hru segment">
					<h3><i class="check square icon"></i>가족사항</h3>
					<table class="hru board2" id="showFamily">
						<thead>
							<tr>
								<th>NO</th>
								<th>이름</th>
								<th>관계</th>
								<th>주민등록번호</th>
								<th>동거여부</th>
								<th>소득세공제 부양여부</th>
								<th>건강보험 피부양자</th>
							</tr>
						</thead>
						<tbody id="showTB">
							<c:if test="${mfList.size() != 0}">
								<c:forEach var="mf" items="${mfList}" varStatus="status">
									<tr>
										<td><c:out value="${status.count}" /></td>
										<td><c:out value="${mf.fName}" /></td>
										<td>
											<c:choose>
												<c:when test="${mf.fCategory eq 1}"><c:out value="배우자"/></c:when>
												<c:when test="${mf.fCategory eq 2}"><c:out value="자녀"/></c:when>
												<c:when test="${mf.fCategory eq 3}"><c:out value="형제자매"/></c:when>
												<c:when test="${mf.fCategory eq 4}"><c:out value="직계존속"/></c:when>
												<c:when test="${mf.fCategory eq 5}"><c:out value="직계비속"/></c:when>
											</c:choose>
										</td>
										<td><c:out value="${fn:substring(mf.fSsn,0,fn:length(mf.fSsn)-6)}******"/></td>
										<td><div class="ui checkbox disabled"><input type="checkbox" <c:if test="${mf.togetherYN eq 'Y'}">checked="checked"</c:if> disabled><label></label></div></td>
										<td><div class="ui checkbox disabled"><input type="checkbox" <c:if test="${mf.deductionYN eq 'Y'}">checked="checked"</c:if> disabled><label></label></div></td>
										<td><div class="ui checkbox disabled"><input type="checkbox" <c:if test="${mf.insuranceYN eq 'Y'}">checked="checked"</c:if> disabled><label></label></div></td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${mfList.size() == 0}">
								<tr>
									<td style="padding: 100px" colspan="7">
										<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 가족이 없습니다.</label></h2>
									</td>
								</tr>
							</c:if>
						</tbody>
					</table>

					<form method="post" id="familyForm">
						<table class="hru board2" id="updateFamily" style="display: none">
							<thead>
								<tr>
									<th>NO</th>
									<th>이름</th>
									<th>관계</th>
									<th>주민등록번호</th>
									<th>동거여부</th>
									<th>소득세공제 부양여부</th>
									<th>건강보험 피부양자</th>
								</tr>
							</thead>
							<tbody id="changeTB">
								<c:if test="${mfList.size() != 0}">
									<c:forEach var="mf" items="${mfList}" varStatus="status">
										<tr>
											<td><button class="ui icon red button mini" type="button" onclick="deleteFamily('${mf.fid}', '${mf.fName}');"><i class="minus icon"></i></button></td>
											<td><input type="hidden" name="fid" value="${mf.fid}"><div class="ui small input"><input type="text" size="6" name="fname" value="${mf.fName}"></div></td>
											<td>
												<select class="ui compact dropdown" name="fCategory">
													<option value="">가족관계</option>
													<option value="1" <c:if test='${mf.fCategory eq 1}'> selected</c:if>>배우자</option>
													<option value="2" <c:if test='${mf.fCategory eq 2}'> selected</c:if>>자녀</option>
													<option value="3" <c:if test='${mf.fCategory eq 3}'> selected</c:if>>형제자매</option>
													<option value="4" <c:if test='${mf.fCategory eq 4}'> selected</c:if>>직계존속</option>
													<option value="5" <c:if test='${mf.fCategory eq 5}'> selected</c:if>>직계비속</option>
												</select>
											</td>
											<td>
												<div class="ui small input"><input type="text" maxlength="6" size="6" name="fssn1" value="${fn:split(mf.fSsn,'-')[0]}"></div>&nbsp;-&nbsp;
												<div class="ui small input"><input type="text" maxlength="7" size="7" name="fssn2" value="${fn:split(mf.fSsn,'-')[1]}"></div>
											</td>
											<td>
												<div class="ui checkbox">
												<input type="checkbox" name="togetherC" <c:if test="${mf.togetherYN eq 'Y'}">checked="checked"</c:if>><label></label>
												<input type="hidden" name="together" <c:if test="${mf.togetherYN eq 'Y'}">value="Y"</c:if><c:if test="${mf.togetherYN eq 'N'}">value="N"</c:if>>
												</div>
											</td>
											<td>
												<div class="ui checkbox">
												<input type="checkbox" name="deductionC" <c:if test="${mf.deductionYN eq 'Y'}">checked="checked"</c:if>><label></label>
												<input type="hidden" name="deduction" <c:if test="${mf.deductionYN eq 'Y'}">value="Y"</c:if><c:if test="${mf.deductionYN eq 'N'}">value="N"</c:if>>
												</div>
											</td>
											<td>
												<div class="ui checkbox">
												<input type="checkbox" name="insuranceC" <c:if test="${mf.insuranceYN eq 'Y'}">checked="checked"</c:if>><label></label>
												<input type="hidden" name="insurance" <c:if test="${mf.insuranceYN eq 'Y'}">value="Y"</c:if><c:if test="${mf.insuranceYN eq 'N'}">value="N"</c:if>>
												</div>
											</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${mfList.size() == 0}">
									<tr>
										<td style="padding: 100px" colspan="7">
											<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 가족이 없습니다.</label></h2>
										</td>
									</tr>
								</c:if>
							</tbody>
						</table>
					</form>
				</div>
				<div class="hru segment">
					<div style="margin-top: 20px; margin-bottom: 50px; float: right;">
							<button class="ui blue button" id="addFamily">추가</button>
							<button class="ui blue button <c:if test='${mfList.size() eq 0}'>disabled</c:if>" id="showFUpdateBtn">수정</button>
							<button class="ui blue button" style="display: none" id="saveFUpdateBtn">저장</button>
							<button class="ui button" style="display: none" id="cancelFUpdateBtn">취소</button>
					</div>
				</div>
				<div class="hru segment"></div>
			</div>
		</div>
	</div>
	<!-- 가족사항 끝-->

	<!-- 비밀번호 변경 모달 -->
	<div class="ui modal" style="width: 700px;" id="passwordModal">
		<div class="header" align="center">비밀번호 변경</div>
		<div class="content" align="center">
			<form action="updatePassword.me" method="post" id="passForm">
				<table>
					<tr>
						<td>비밀번호 변경</td>
						<td><div class="ui input" id="passDiv1"><input type="password" name="pass1" id="pass1"></div></td>
					</tr>
					<tr>
						<td>비밀번호 확인</td>
						<td><div class="ui input" id="passDiv2"><input type="password" name="pass2" id="pass2"></div></td>
					</tr>
					<tr>
						<td colspan="2"><i class="exclamation circle icon"></i>비밀번호는 영어 대·소문자/숫자/특수문자 포함 8자리 이상으로 변경해주세요</td>
					</tr>
				</table>
			</form>
		</div>
		<div class="actions">
			<div class="ui button" id="chagePassSave">변경</div>
			<div class="ui button" id="chagePassCancel">취소</div>
		</div>
	</div>

	<!-- 가족 추가 모달 -->
	<div class="large ui modal" style="width:1200px" id="familyAddModal">
		<div class="header" align="center">가족사항 추가</div>
		<div class="content" align="center">
			<form id="addFamilyForm" method="post">
				<table class="hru board2">
					<tr>
						<th>이름</th>
						<th>관계</th>
						<th>주민등록번호</th>
						<th>동거여부</th>
						<th>소득세공제 부양여부</th>
						<th>건강보험 피부양자</th>
					</tr>
					<tr>
						<td><div class="ui small input"><input type="text" size="6" name="name"></div></td>
						<td>
							<select class="ui compact dropdown" name="category" id="categoey">
								<option value="">가족관계</option>
								<option value="1">배우자</option>
								<option value="2">자녀</option>
								<option value="3">형제자매</option>
								<option value="4">직계존속</option>
								<option value="5">직계비속</option>
							</select>
						</td>
						<td>
							<div class="ui small input"><input type="text" maxlength="6" size="6" name="ssn1"></div>&nbsp;-&nbsp;
							<div class="ui small input"><input type="text" maxlength="7" size="7" name="ssn2"></div>
						</td>
						<td><div class="ui checkbox"><input type="checkbox" name="together"><label></label></div></td>
						<td><div class="ui checkbox"><input type="checkbox" name="deduction"><label></label></div></td>
						<td><div class="ui checkbox"><input type="checkbox" name="insurance"><label></label></div></td>
					</tr>
				</table>
			</form>
			<div style="margin-top:20px; margin-bottom:50px; float: right;">
				<button class="ui blue button" id="addSave">추가</button>
				<button class="ui button" id="addCancel">취소</button>
			</div>
		</div>
	</div>


	<script>
		$(function() {
			$(".menu .item").tab();
			$("#member1").show();
			$(".ui.dropdown").dropdown();
			var initYn = "${loginUser.getMhi().isInitYn()}";
			//console.log(initYn);
			if(initYn=='true'){
				Swal.fire({
					  title: '비밀번호 변경',
					  text: "비밀번호를 변경해주세요.",
					  icon: 'warning'
				}).then((result) => {
					if (result.value) {
						$('#passwordModal').modal('show');
					}
				});
			}
			
			$("#showUpdateBtn").click(function(){
				$("#showTable").css("display", "none");
				$("#showUpdateBtn").css("display", "none");
				$("#updateTable").css("display", "");
				$("#saveUpdateBtn").css("display", "");
				$("#cancelUpdateBtn").css("display", "");
			});
			
			$("#cancelUpdateBtn").click(function(){
				$("#showTable").css("display", "");
				$("#showUpdateBtn").css("display", "");
				$("#updateTable").css("display", "none");
				$("#saveUpdateBtn").css("display", "none");
				$("#cancelUpdateBtn").css("display", "none");
			});
			
			$("#saveUpdateBtn").click(function(){
				$('#updateForm').submit();
			});
			
			$("#showFUpdateBtn").click(function(){
				$("#showFamily").css("display", "none");
				$("#showFUpdateBtn").css("display", "none");
				$("#updateFamily").css("display", "");
				$("#saveFUpdateBtn").css("display", "");
				$("#cancelFUpdateBtn").css("display", "");
			});
			
			$("input:checkbox").change(function(){
				if(this.checked) {
					$(this).parent().find("input:hidden").val("Y");
				}else{
					$(this).parent().find("input:hidden").val("N");
				}
				
			})
			
			$("#saveFUpdateBtn").click(function(){
				var familyForm = $("#familyForm").serializeArray() ;
				
				$.ajax({
					url:"updateFamily.me",
					type:"POST",
					data:familyForm,
					dataType : "json",
					success:function(data){
						var ml = data.mfList;
						
						$("#showTB").empty();
						$("#changeTB").empty();
						
						if(ml.length > 0){
							$.each(ml, function(index, mf){
								var $tr = $("<tr>");
								var $tr2 = $("<tr>");
								var ssn = mf.fSsn.split('-');
								
								$tr.append($("<td>").text(index + 1));
								$tr.append($("<td>").text(mf.fName));
								switch(mf.fCategory){
									case 1 : $tr.append($("<td>").text("배우자")); break;
									case 2 : $tr.append($("<td>").text("자녀")); break;
									case 3 : $tr.append($("<td>").text("형제자매")); break;
									case 4 : $tr.append($("<td>").text("직계존속")); break;
									case 5 : $tr.append($("<td>").text("직계비속")); break;
								}
								$tr.append($("<td>").text(ssn[0] + ssn[1].substring(0,0) + " - ******"));
								if(mf.togetherYN != 'Y') {
									$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' disabled><label></label>")));
								}else{
									$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' checked='checked' disabled><label></label>")));
								}
								
								if(mf.deductionYN != 'Y') {
									$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' disabled><label></label>")));
								}else{
									$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' checked='checked' disabled><label></label>")));
								}
								
								if(mf.insuranceYN != 'Y') {
									$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' disabled><label></label>")));
						 		}else{
									$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' checked='checked' disabled><label></label>")));
								}
								
								$("#showTB").append($tr);
								
								$tr2.append($("<td>").append($("<button class='ui icon red button mini' type='button' onclick='deleteFamily(" + mf.fid + ", \"" + mf.fName + "\");'><i class='minus icon'></i>")));
								$tr2.append($("<td>").append($("<div class='ui small input'>").append($("<input type='text' size='6' name='fname'>").val(mf.fName))).append($("<input type='hidden' name='fid'>").val(mf.fid)));
								switch(mf.fCategory){
									case 1 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1' selected>배우자</option><option value='2'>자녀</option><option value='3'>형제자매</option><option value='4'>직계존속</option><option value='5'>직계비속</option>"))); break;
									case 2 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2' selected>자녀</option><option value='3'>형제자매</option><option value='4'>직계존속</option><option value='5'>직계비속</option>"))); break;
									case 3 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2'>자녀</option><option value='3' selected>형제자매</option><option value='4'>직계존속</option><option value='5'>직계비속</option>"))); break;
									case 4 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2'>자녀</option><option value='3'>형제자매</option><option value='4' selected>직계존속</option><option value='5'>직계비속</option>"))); break;
									case 5 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2'>자녀</option><option value='3'>형제자매</option><option value='4'>직계존속</option><option value='5' selected>직계비속</option>"))); break;
								}
								$tr2.append($("<td>").append($("<div class='ui small input'>").append($("<input type='text' size='6' name='fssn1'>").val(ssn[0]))).append($("<label>").text(" - ")).append($("<div class='ui small input'>").append($("<input type='text' size='7' name='fssn2'>").val(ssn[1]))));
							    if(mf.togetherYN != 'Y') {
									$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' name='togetherC'><label></label><input type='hidden' name='together' value='N'>")));
								}else{
									$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' checked='checked' name='togetherC'><label></label><input type='hidden' name='together' value='Y'>")));
								}
								
								if(mf.deductionYN != 'Y') {
									$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' name='deductionC'><label></label><input type='hidden' name='deduction' value='N'>")));
								}else{
									$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' checked='checked' name='deductionC'><label></label><input type='hidden' name='deduction' value='N'>")));
								}
								
								if(mf.insuranceYN != 'Y') {
									$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' name='insuranceC'><label></label><input type='hidden' name='insurance' value='N'>")));
								}else{
									$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' checked='checked' name='insuranceC'><label></label><input type='hidden' name='insurance' value='N'>")));
								}
								
								$("#changeTB").append($tr2);
			  				});
						}else{
							$("#showTB").append($('<tr><td style="padding:100px" colspan="7"><h2 style="color:lightgray"><label><i class="exclamation circle icon"></i>등록된 가족이 없습니다.</label></h2></td>'));
							$("#changeTB").append($('<tr><td style="padding:100px" colspan="7"><h2 style="color:lightgray"><label><i class="exclamation circle icon"></i>등록된 가족이 없습니다.</label></h2></td>'));
							$("#showFUpdateBtn").addClass("disabled");
						}
						$("#showFamily").css("display", "");
						$("#showFUpdateBtn").css("display", "");
						$("#updateFamily").css("display", "none");
						$("#saveFUpdateBtn").css("display", "none");
						$("#cancelFUpdateBtn").css("display", "none");
						$(".ui.dropdown").dropdown();
					},
					error:function(status){
						alert("실패");
						console.log(status);
					}
			   });		
			});
			
			$("#cancelFUpdateBtn").click(function(){
				$("#showFamily").css("display", "");
				$("#showFUpdateBtn").css("display", "");
				$("#updateFamily").css("display", "none");
				$("#saveFUpdateBtn").css("display", "none");
				$("#cancelFUpdateBtn").css("display", "none");
			});
			
			$("#addFamily").click(function(){
				$("#familyAddModal").modal('show');
			});
			
			$("#addSave").click(function(){
				if($("#categoey").val() != "") {
					var queryString = $("#addFamilyForm").serializeArray() ;
					
					$(this).parent().parent().find("input:text").val("");
					$.ajax({
						url:"addFamily.me",
						type:"POST",
						data:queryString,
						dataType : "json",
						success:function(data){
							var ml = data.mfList;
							
							$("#showTB").empty();
							$("#changeTB").empty();
							
							if(ml.length > 0){
								$.each(ml, function(index, mf){
									var $tr = $("<tr>");
									var $tr2 = $("<tr>");
									var ssn = mf.fSsn.split('-');
									
									$tr.append($("<td>").text(index + 1));
									$tr.append($("<td>").text(mf.fName));
									switch(mf.fCategory){
										case 1 : $tr.append($("<td>").text("배우자")); break;
										case 2 : $tr.append($("<td>").text("자녀")); break;
										case 3 : $tr.append($("<td>").text("형제자매")); break;
										case 4 : $tr.append($("<td>").text("직계존속")); break;
										case 5 : $tr.append($("<td>").text("직계비속")); break;
									}
									$tr.append($("<td>").text(ssn[0] + ssn[1].substring(0,0) + " - ******"));
									if(mf.togetherYN != 'Y') {
										$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' disabled><label></label>")));
									}else{
										$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' checked='checked' disabled><label></label>")));
									}
									
									if(mf.deductionYN != 'Y') {
										$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' disabled><label></label>")));
									}else{
										$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' checked='checked' disabled><label></label>")));
									}
									
									if(mf.insuranceYN != 'Y') {
										$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' disabled><label></label>")));
							 		}else{
										$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' checked='checked' disabled><label></label>")));
									}
									
									$("#showTB").append($tr);
									
									$tr2.append($("<td>").append($("<button class='ui icon red button mini' type='button' onclick='deleteFamily(" + mf.fid + ", \"" + mf.fName + "\");'><i class='minus icon'></i>")));
									$tr2.append($("<td>").append($("<div class='ui small input'>").append($("<input type='text' size='6' name='fname'>").val(mf.fName))).append($("<input type='hidden' name='fid'>").val(mf.fid)));
									switch(mf.fCategory){
										case 1 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1' selected>배우자</option><option value='2'>자녀</option><option value='3'>형제자매</option><option value='4'>직계존속</option><option value='5'>직계비속</option>"))); break;
										case 2 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2' selected>자녀</option><option value='3'>형제자매</option><option value='4'>직계존속</option><option value='5'>직계비속</option>"))); break;
										case 3 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2'>자녀</option><option value='3' selected>형제자매</option><option value='4'>직계존속</option><option value='5'>직계비속</option>"))); break;
										case 4 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2'>자녀</option><option value='3'>형제자매</option><option value='4' selected>직계존속</option><option value='5'>직계비속</option>"))); break;
										case 5 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2'>자녀</option><option value='3'>형제자매</option><option value='4'>직계존속</option><option value='5' selected>직계비속</option>"))); break;
									}
									$tr2.append($("<td>").append($("<div class='ui small input'>").append($("<input type='text' size='6' name='fssn1'>").val(ssn[0]))).append($("<label>").text(" - ")).append($("<div class='ui small input'>").append($("<input type='text' size='7' name='fssn2'>").val(ssn[1]))));
									if(mf.togetherYN != 'Y') {
										$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' name='togetherC'><label></label><input type='hidden' name='together' value='N'>")));
									}else{
										$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' checked='checked' name='togetherC'><label></label><input type='hidden' name='together' value='Y'>")));
									}
									
									if(mf.deductionYN != 'Y') {
										$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' name='deductionC'><label></label><input type='hidden' name='deduction' value='N'>")));
									}else{
										$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' checked='checked' name='deductionC'><label></label><input type='hidden' name='deduction' value='N'>")));
									}
									
									if(mf.insuranceYN != 'Y') {
										$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' name='insuranceC'><label></label><input type='hidden' name='insurance' value='N'>")));
									}else{
										$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' checked='checked' name='insuranceC'><label></label><input type='hidden' name='insurance' value='N'>")));
									}
									$("#showFUpdateBtn").removeClass("disabled");
									$("#changeTB").append($tr2);
				  				});
							}else{
								$("#showTB").append($('<tr><td style="padding:100px" colspan="7"><h2 style="color:lightgray"><label><i class="exclamation circle icon"></i>등록된 가족이 없습니다.</label></h2></td>'));
								$("#changeTB").append($('<tr><td style="padding:100px" colspan="7"><h2 style="color:lightgray"><label><i class="exclamation circle icon"></i>등록된 가족이 없습니다.</label></h2></td>'));
								$("#showFUpdateBtn").addClass("disabled");
							}
							$("#showFamily").css("display", "");
							$("#showFUpdateBtn").css("display", "");
							$("#updateFamily").css("display", "none");
							$("#saveFUpdateBtn").css("display", "none");
							$("#cancelFUpdateBtn").css("display", "none");
							$(".ui.dropdown").dropdown();
						},
						error:function(error){
							alert("실패");
						}
				   });
					$("#familyAddModal").modal('hide');
				}else {
					Swal.fire({
						text: '가족관계를 선택해주세요!',
						icon: 'error'
					})
				}
			});
			
			$("#addCancel").click(function(){
				$(this).parent().parent().find("input:text").val("");
				$("#familyAddModal").modal('hide');
			});
			
			$("#imgArea").click(function(){
				$("#imgFile").click();	
			});
			
			$("#imgBtn").click(function(){
				$("#imgFile").click();	
			});
			
			$("#changePass").click(function(){		
				$('#passwordModal').modal('show');
			});
			
			$("#chagePassCancel").click(function(){
				$("#passwordModal").modal('hide');
				$("#passDiv1").removeClass("error");
				$("#passDiv2").removeClass("error");
				$("#pass1").val("");
				$("#pass2").val("");
			});
			
			$("#chagePassSave").click(function(){
				 var check = /^[a-zA-Z0-9]{8,16}$/;
				
				if($("#pass1").val() == "") {
					$("#passDiv1").addClass("error");
					$("#pass1").focus();
				}else{
					if(!check.test($("#pass1").val())){
						if($("#pass2").val() == "") {
							$("#passDiv2").addClass("error");
							$("#pass2").focus();
						}else{
							if($("#pass1").val() == $("#pass2").val()) {
								Swal.fire({
									text: '비밀번호가 변경되었습니다!',
									icon: 'success' 
								}).then((result) => {
									$("#passForm").submit();
									
								});
							} else {
								Swal.fire({
									text: '비밀번호가 일치하지 않습니다!',
									icon: 'error'
								})
							}
						}	
					} else {
						Swal.fire({
							text: '유효하지 않은 비밀번호입니다!',
							icon: 'error'
						})
					}
					
				}
				
			});
			
			$("#pass1").change(function() {
			    $("#passDiv1").removeClass("error");
			});
			
			$("#pass2").change(function() {
			    $("#passDiv2").removeClass("error");
			});
		});
		
		function loadImg(value) {
			
			if(value.files && value.files[0]) {
				
				var reader = new FileReader();
				
				reader.onload = function(e) {
					
					$("#profileImg").attr("src",e.target.result);
						
				}
				reader.readAsDataURL(value.files[0]);
				
			}
			
		}
		
		function deleteFamily(fid, fName){
			
			Swal.fire({
				title: "삭제하시겠습니까?",
				text: fName + " 구성원을 삭제하시겠습니까?",
				icon: "warning",
				showCancelButton: true,
				confirmButtonColor: "#0F4C81",
				confirmButtonText: "예, 삭제합니다.",
				cancelButtonText: "취소"
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url:"removeFamily.me",
							type:"POST",
							data:{'fid':fid},
							dataType : "json",
							success:function(data){
								var ml = data.mfList;
								
								$("#showTB").empty();
								$("#changeTB").empty();
								
								if(ml.length > 0){
									$.each(ml, function(index, mf){
										var $tr = $("<tr>");
										var $tr2 = $("<tr>");
										var ssn = mf.fSsn.split('-');
										
										$tr.append($("<td>").text(index + 1));
										$tr.append($("<td>").text(mf.fName));
										switch(mf.fCategory){
											case 1 : $tr.append($("<td>").text("배우자")); break;
											case 2 : $tr.append($("<td>").text("자녀")); break;
											case 3 : $tr.append($("<td>").text("형제자매")); break;
											case 4 : $tr.append($("<td>").text("직계존속")); break;
											case 5 : $tr.append($("<td>").text("직계비속")); break;
										}
										$tr.append($("<td>").text(ssn[0] + ssn[1].substring(0,0) + " - ******"));
										if(mf.togetherYN != 'Y') {
											$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' disabled><label></label>")));
										}else{
											$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' checked='checked' disabled><label></label>")));
										}
										
										if(mf.deductionYN != 'Y') {
											$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' disabled><label></label>")));
										}else{
											$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' checked='checked' disabled><label></label>")));
										}
										
										if(mf.insuranceYN != 'Y') {
											$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' disabled><label></label>")));
								 		}else{
											$tr.append($("<td>").append($("<div class='ui checkbox disabled'><input type='checkbox' checked='checked' disabled><label></label>")));
										}
										
										$("#showTB").append($tr);
										
										$tr2.append($("<td>").append($("<button class='ui icon red button mini' type='button' onclick='deleteFamily(" + mf.fid + ", \"" + mf.fName + "\");'><i class='minus icon'></i>")));
										$tr2.append($("<td>").append($("<div class='ui small input'>").append($("<input type='text' size='6' name='fname'>").val(mf.fName))).append($("<input type='hidden' name='fid'>").val(mf.fid)));
										switch(mf.fCategory){
											case 1 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1' selected>배우자</option><option value='2'>자녀</option><option value='3'>형제자매</option><option value='4'>직계존속</option><option value='5'>직계비속</option>"))); break;
											case 2 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2' selected>자녀</option><option value='3'>형제자매</option><option value='4'>직계존속</option><option value='5'>직계비속</option>"))); break;
											case 3 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2'>자녀</option><option value='3' selected>형제자매</option><option value='4'>직계존속</option><option value='5'>직계비속</option>"))); break;
											case 4 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2'>자녀</option><option value='3'>형제자매</option><option value='4' selected>직계존속</option><option value='5'>직계비속</option>"))); break;
											case 5 : $tr2.append($("<td>").append($("<select class='ui compact dropdown' name='fCategory'><option value=''>가족관계</option><option value='1'>배우자</option><option value='2'>자녀</option><option value='3'>형제자매</option><option value='4'>직계존속</option><option value='5' selected>직계비속</option>"))); break;
										}
										$tr2.append($("<td>").append($("<div class='ui small input'>").append($("<input type='text' size='6' name='fssn1'>").val(ssn[0]))).append($("<label>").text(" - ")).append($("<div class='ui small input'>").append($("<input type='text' size='7' name='fssn2'>").val(ssn[1]))));
										if(mf.togetherYN != 'Y') {
											$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' name='togetherC'><label></label><input type='hidden' name='together' value='N'>")));
										}else{
											$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' checked='checked' name='togetherC'><label></label><input type='hidden' name='together' value='Y'>")));
										}
										
										if(mf.deductionYN != 'Y') {
											$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' name='deductionC'><label></label><input type='hidden' name='deduction' value='N'>")));
										}else{
											$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' checked='checked' name='deductionC'><label></label><input type='hidden' name='deduction' value='N'>")));
										}
										
										if(mf.insuranceYN != 'Y') {
											$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' name='insuranceC'><label></label><input type='hidden' name='insurance' value='N'>")));
										}else{
											$tr2.append($("<td>").append($("<div class='ui checkbox'><input type='checkbox' checked='checked' name='insuranceC'><label></label><input type='hidden' name='insurance' value='N'>")));
										}
										
										$("#changeTB").append($tr2);
					  				});
								}else{
									$("#showTB").append($('<tr><td style="padding:100px" colspan="7"><h2 style="color:lightgray"><label><i class="exclamation circle icon"></i>등록된 가족이 없습니다.</label></h2></td>'));
									$("#changeTB").append($('<tr><td style="padding:100px" colspan="7"><h2 style="color:lightgray"><label><i class="exclamation circle icon"></i>등록된 가족이 없습니다.</label></h2></td>'));
									$("#showFUpdateBtn").addClass("disabled");
								}
								$("#showFamily").css("display", "");
								$("#showFUpdateBtn").css("display", "");
								$("#updateFamily").css("display", "none");
								$("#saveFUpdateBtn").css("display", "none");
								$("#cancelFUpdateBtn").css("display", "none");
								$(".ui.dropdown").dropdown();
							},
							error:function(status){
								alert("실패");
								console.log(status);
							}
					   });
						
					}
				});
			}		
	</script>
</body>
</html>