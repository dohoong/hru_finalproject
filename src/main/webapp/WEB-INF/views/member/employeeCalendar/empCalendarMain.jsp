<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link href='https://unpkg.com/@fullcalendar/core@4.3.1/main.min.css'
	rel='stylesheet' />
<link href='https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.css'
	rel='stylesheet' />
<link href='https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.css'
	rel='stylesheet' />

<link href='https://unpkg.com/@fullcalendar/list@4.3.0/main.min.css'
	rel='stylesheet' />
<script src='https://unpkg.com/@fullcalendar/core@4.3.1/main.min.js'></script>

<script src='https://unpkg.com/@fullcalendar/core@4.3.1/locales-all.js'></script>

<script
	src='https://unpkg.com/@fullcalendar/interaction@4.3.0/main.min.js'></script>

<script src='https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.js'></script>

<script src='https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.js'></script>

<script src='https://unpkg.com/@fullcalendar/list@4.3.0/main.min.js'></script>

<!-- 캘린더 초기 설정 -->
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var Calendar = FullCalendar.Calendar;
		var Draggable = FullCalendarInteraction.Draggable;

		var calendarEl = document.getElementById('calendar');

		var calendar = new Calendar(calendarEl, {
			plugins : [ 'interaction', 'dayGrid', 'timeGrid' ],
			header : {
				left : 'prev,next today',
				center : 'title',
				right : 'dayGridMonth,timeGridWeek,timeGridDay'
			},
			editable : true,
			droppable : true, 
			drop : function(info) {
				if (checkbox.checked) {
					info.draggedEl.parentNode.removeChild(info.draggedEl);
				}
			},
			locale : 'ko',
			events : [ {
				title : 'evt1',
				start : '2019-09-03'
			}, {
				title : 'evt2',
				start : '2019-09-10',
				end : '2019-09-20'
			}, {
				title : 'evt3',
				start : '2019-09-25T12:30:00',
				allDay : false
			} ]
		});

		calendar.render();

		var arrTest = getCalendarDataInDB();
		$.each(arrTest, function(index, item) {
			console.log('outer loop_in_cal' + index + ' : ' + item);
			$.each(item, function(iii, ttt) {
				console.log('inner loop_in_cal => ' + iii + ' : ' + ttt);
			});
		});

		$("#btnAddTest").click(
				function() {
					//var arr = getCalendarEvent();
					var arr = getCalendarDataInDB();
					$.each(arr, function(index, item) {
						calendar.addEvent(item);
						console.log('click evt loop_in_cal' + index + ' : '
						 		+ item);
						$.each(item, function(iii, ttt) {
							console.log('click evt inner loop_in_cal => ' + iii
									+ ' : ' + ttt);
						});
					});

					//calendar.addEvent( {'title':'evt4', 'start':'2019-09-04', 'end':'2019-09-06'});
					calendar.render();
				});
	});

	function getCalendarEvent() {
		//var arr = [ {'title':'evt4', 'start':'2019-09-04', 'end':'2019-09-06'} ];
		var arr = {
			'title' : 'evt4',
			'start' : '2019-09-04',
			'end' : '2019-09-06'
		};
		return arr;
	}

	function getCalendarDataInDB() {
		var arr = [ {
			title : 'evt1',
			start : 'ssssss'
		}, {
			title : 'evt2',
			start : '123123123'
		} ];

		//배열 초기화
		var viewData = {};
		//data[키] = 밸류
		viewData["id"] = $("#currId").text().trim();
		viewData["title"] = $("#title").val();
		viewData["content"] = $("#content").val();

		/* $.ajax({
			contentType : 'application/json',
			dataType : 'json',
			
			type : 'post',
			async : false,
			data : JSON.stringify(viewData),
			success : function(resp) {
				//alert(resp.f.id + ' ggg');     
				$.each(resp, function(index, item) {
					console.log(index + ' : ' + item);
					$.each(item, function(iii, ttt) {
						console.log('inner loop => ' + iii + ' : ' + ttt);
					});
				});
				arr = resp;
			},
			error : function() {
				alert('저장 중 에러가 발생했습니다. 다시 시도해 주세요.');
			}
		}); */

		return arr;

	}

	/* $.ajax({
		type : "POST",
		url : "insertSchedule.cal",
		data : {
			calendar_title : calendar_title,
			calendar_start_date : calendar_start_date,
			calendar_end_date : calendar_end_date,
			calendar_content : calendar_content
		},
		success : function(data) {
			console.log(data.ms.title);
			console.log(data.ms.content);
			console.log(data.ms.starttime);
			console.log(data.ms.endtime);
		},
		error : function(data) {
			console.log("실패..");
		}
	}); 
	
	return false; */

	function register() {
		var calendar_title = $(".calendar_title").val();
		var calendar_start_date = $(".calendar_start_date").val();
		var calendar_end_date = $(".calendar_end_date").val();
		var calendar_content = $("#calendar_content").val();

		if (!calendar_title) {
			alert("일정명을 입력해주세요.");
			return false;
		}
		if (!calendar_start_date) {
			alert("시작 날짜를 입력해주세요.");
			return false;
		}
		if (!calendar_end_date) {
			alert("마침 날짜를 입력 해주세요.");
			return false;
		}
		if (!calendar_content) {
			alert("내용을 입력해 주세요.");
			return false;
		}
		var action = document.forms.registerForm;
		$("#registerForm").attr("action", "insertSchedule.cal");
		$("#registerForm").submit();

	}

	function register() {
		var calendar_title = $(".calendar_title").val();
		var calendar_start_date = $(".calendar_start_date").val();
		var calendar_end_date = $(".calendar_end_date").val();
		var calendar_content = $("#calendar_content").val();

		if (!calendar_title) {
			alert("일정명을 입력해주세요.");
			return false;
		}
		if (!calendar_start_date) {
			alert("시작 날짜를 입력해주세요.");
			return false;
		}
		if (!calendar_end_date) {
			alert("마침 날짜를 입력 해주세요.");
			return false;
		}
		if (!calendar_content) {
			alert("내용을 입력해 주세요.");
			return false;
		}
	

		/* $.ajax({
			type : "POST",
			url : "insertSchedule.cal",
			data : {
				calendar_title : calendar_title,
				calendar_start_date : calendar_start_date,
				calendar_end_date : calendar_end_date,
				calendar_content : calendar_content
			},
			success : function(data) {
				console.log(data.ms.title);
				console.log(data.ms.content);
				console.log(data.ms.starttime);
				console.log(data.ms.endtime);
			},
			error : function(data) {
				console.log("실패..");
			}
		}); 
		
		return false; */
	}
</script>

<script>
	function addSchedule() {

		$('.tiny.modal').modal('show');

		$("#rangestart").calendar({
			type : 'date',
			endCalendar : $('#rangeend'),
			formatter : {
				date : function(date, settings) {
					if (!date)
						return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					return year + '-' + month + '-' + day;
				}
			}
		});
		$("#rangeend").calendar({
			type : 'date',
			startCalendar : $('#rangestart'),
			formatter : {
				date : function(date, settings) {
					if (!date)
						return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
					return year + '-' + month + '-' + day;
				}
			}
		});

	}
</script>
<style>
html, body {
	margin: 0;
	padding: 0;
	font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
	font-size: 14px;
}

#external-events {
	position: fixed;
	z-index: 2;
	top: 300px;
	left: 140px;
	width: 150px;
	padding: 0 10px;
	border: 1px solid #ccc;
	background: #eee;
}

.demo-topbar+#external-events { /* will get stripped out */
	top: 60px;
}

#external-events .fc-event {
	margin: 1em 0;
	cursor: move;
}

#calendar-container {
	position: relative;
	z-index: 1;
	margin-left: 200px;
}

#calendar {
	max-width: 900px;
	margin: 20px auto;
}
</style>
<jsp:include page="../../common/importlib.html" />

</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<!-- 상대경로 잘 지정해야함! -->
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<!-- 여기에 내용부분 작성! -->
			<div style="max-width: 900px; margin: 0 auto; height: 30px">
				<div style="float: right;">
					<button class="ui blue button" onclick="addSchedule();">일정등록</button>
				</div>
			</div>
			<div>
				<h1 style="margin-left: 100px;">
					<c:out value="${ list[0].mname }" />
					's 캘린더
				</h1>
				<div id='calendar' style="max-width: 900px; margin: 0 auto;"></div>
			</div>
		</div>

		<div class="ui tiny test modal">
			<div class="header">일정 등록</div>
			<div class="content">
				<div class="editArea tblEmpCal">
					<form action="insertSchedule.cal" method="post">
						<table class="hru table" style="width: 482px; height: 80px;">
							<thead>
								<tr class="table-title">
									<th colspan="3" class="table_title"><div
											class="ui mini input">
											<input type="text" id="leader" class="calendar_title"
												name="title" placeholder="Enter Title" style="width: 300px;">
										</div></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>시작 일자</th>
									<td><div class="ui calendar" id="rangestart">
											<div class="ui input left icon mini">
												<i class="calendar icon"></i> <input type="text"
													class="calendar_start_date" name="starttime"
													placeholder="Start" readonly>
											</div>
										</div></td>
								</tr>
								<tr>
									<th>종료 일자</th>
									<td><div class="ui calendar" id="rangeend">
											<div class="ui input left icon mini">
												<i class="calendar icon"></i> <input type="text"
													class="calendar_end_date" name="endtime" placeholder="End"
													readonly>
											</div>
										</div></td>
								</tr>
								<tr>
									<th>내용</th>
									<td colspan="2"><div>
											<textarea style="resize: none;" class="areaContent"
												id="calendar_content" name="content" placeholder="설명추가"></textarea>
										</div>
								</tr>
							</tbody>
						</table>
						<div class="buttonArea">
							<button class="ui blue button mini" type="submit" id="insert"
								>등록</button>
							<button class="ui gray button mini" type="reset">취소</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
