<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
     
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>사원 캘린더 상세보기</title>
<style>
.tblEmpCal {
	position: absolute;
	top: 30%;
	left: 30%;
}  

.titInput {
	width: 100%;
}

.areaApply {
	width: 500px;
	height: 120px;
}
</style>
<jsp:include page="../../common/importlib.html" />
<!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../../common/menubar.jsp" />
	<!-- 상대경로 잘 지정해야함! -->
	<div class="outer">
		<!-- outer area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<!-- 여기에 내용부분 작성! -->

			<div class="editArea tblEmpCal" style="width: 50%; height: 75%;">
				<form action="" method="post">
					<table class="hru table">
						<thead>
							<tr class="table-title">
								<th colspan="3" class="table-title"><div class="ui mini input">
								<input type="text" id="leader" placeholder="Enter Title">
							</div></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>시작 일자</th>
								<td colspan="2"><div class="ui calendar" id="rangestart">
										<div class="ui input left icon">
											<i class="calendar icon"></i> <input type="text"
												placeholder="meet">
										</div>
									</div></td>
							</tr>
							<tr>
								<th>종료 일자</th>
								<td colspan="2"><div class="ui calendar" id="rangeend">
										<div class="ui input left icon">
											<i class="calendar icon"></i> <input type="text"
												placeholder="End">
										</div>
									</div></td>
							</tr>
							<tr>
								<th>내용</th>
								<td colspan="2"><div>
										<textarea class="areaApply" placeholder="설명추가"></textarea>
									</div>
							</tr>
						</tbody>
					</table>
				</form>
				<div class="buttonArea">
					<button class="ui blue button mini" id="add">추가</button>
					<button class="ui blue button mini" style="display: none;"
						id="complete">완료</button>
					<button class="ui blue button mini" id="modify">수정</button>
					<button class="ui blue button mini" style="display: none;"
						id="modiComplete">완료</button>
					<button class="ui gray button mini" id="delete">삭제</button>
				</div>
			</div>
			<script>
				$("#rangestart").calendar({
					type : 'date',
					endCalendar : $('#rangeend'),
					formatter : {
						date : function(date, settings) {
							if (!date)
								return '';
							var day = date.getDate();
							var month = date.getMonth() + 1;
							var year = date.getFullYear();
							return year + '-' + month + '-' + day;
						}
					}
				});
				$("#rangeend").calendar({
					type : 'date',
					startCalendar : $('#rangestart'),
					formatter : {
						date : function(date, settings) {
							if (!date)
								return '';
							var day = date.getDate();
							var month = date.getMonth() + 1;
							var year = date.getFullYear();
							return year + '-' + month + '-' + day;
						}
					}
				});
			</script>
			<script>
				$(function() {
					$(".scroll-tr").click(
							function() {
								//console.log($(this).find("td").eq(0).text());
								$("#code").val($(this).find("td").eq(0).text())
										.attr("readonly", true);
								$("#name").val($(this).find("td").eq(1).text())
										.attr("readonly", true);
								$("#leader").val(
										$(this).find("td").eq(2).text()).attr(
										"readonly", true);
								if ($(this).find("td").eq(3).text() == "Y") {
									$("#use").prop("checked", true);
									//console.log("Y");
								} else {
									$("#nouse").prop("checked", true);
									//console.log("N");
								}
								$("#add").show();
								$("#complete").hide();

							});
					$("#add").click(function() {
						$("#code").val("").attr("readonly", false);
						$("#name").val("").attr("readonly", false);
						$("#leader").val("").attr("readonly", false);
						$("#add").hide();
						$("#complete").show();
						$("#use").prop("checked", false);
						$("#nouse").prop("checked", false);
					});
					$("#modify").click(function() {
						$("#code").attr("readonly", false);
						$("#name").attr("readonly", false);
						$("#leader").attr("readonly", false);
						$("#modify").hide();
						$("#modiComplete").show();
					});
				});
			</script>
		</div>
		<!-- 부서관리 끝 -->






	</div>
	<!-- 내용 끝 -->
	</div>
	<!-- outer end -->
	<script>
		$(function() {
			$("#member5").show();
		});
	</script>
</body>
</html>