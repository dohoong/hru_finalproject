<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="ui top attached tabular menu">
				<a class="active item" data-tab="first">제증명발급 신청이력</a> 
				<a class="item" data-tab="second">증명서 신청</a>
			</div>
			
			<!-- 제증명발급 신청이력 -->
			<div class="ui bottom attached active tab segment" data-tab="first">
				<div class="hru segment">
				<h3><i class="check square icon"></i>증명서 신청내역</h3>
				<table class="hru board2" border="1">
						<thead>
							<tr>
								<th>NO</th>
								<th>증명서 구분</th>
								<th>신청일자</th>
								<th>전자결재상태</th>
								<th>증명서 용도</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="c" items="${cfMap.cList}" varStatus="status">
							<tr>
								<td>${status.count}</td>
								<c:if test="${c.category == 1}">
								<td><c:out value="재직증명서"/></td></c:if>
								<c:if test="${c.category == 2}">
								<td><c:out value="직무확인서"/></td></c:if>
								<c:if test="${c.category == 3}">
								<td><c:out value="퇴직증명서"/></td></c:if>
								<c:if test="${c.category == 4}">
								<td><c:out value="원천징수영수증"/></td></c:if>
								<td><c:out value="${c.requestDate}"/></td>
								<c:if test="${c.approvalYN =='Y'}">
								<td><c:out value="승인"/></td>
								</c:if>
								<c:if test="${c.approvalYN =='N'}">
								<td><c:out value="미승인"/></td>
								</c:if>
								<td><c:out value="${c.usage}"/></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div><!-- 제증명발급 신청이력 끝-->
		
			<!-- 증명서 신청 -->
			<div class="ui bottom attached tab segment" data-tab="second">
				<!-- 기본정보 -->
				<div class="hru segment">
				<h3><i class="check square icon"></i>기본정보</h3>
					<table class="hru table">
						<tr>
							<th>사원번호</th>
							<td><c:out value="${cfMap.mId}"/></td>
							<th>이름</th>
							<td><c:out value="${cfMap.mName}"/></td>
						</tr>
						<tr>
							<th>사업장</th>
							<td colspan="3"><c:out value="${cfMap.gName}"/></td>
						</tr>
						<tr>
							<th>부서</th>
							<td><c:out value="${cfMap.tName}"/></td>
							<th>직급</th>
							<td><c:out value="${cfMap.ppName}"/></td>
						</tr>
					</table>
				</div><!-- 기본정보 끝-->
				<!-- 증명서 신청란 -->
				<div class="hru segment">
				<h3><i class="check square icon"></i>증명서 신청</h3>
				<form action="insertCertification.att" id="certificationForm">
					<table class="hru table">
						<tr>
							<th>증명서 구분</th>
							<td>
								<select class="ui dropdown" name="category" id="cfCategory">
									<option value="">증명서 선택</option>
									<option value="4">원천징수영수증</option>
									<option value="3">퇴직증명서</option>
									<option value="2">직무확인서</option>
									<option value="1">재직증명서</option>
								</select>
							</td>
							<th>신청일자</th>
							<td>
								<div class="ui input">
								  <input type="date" id="today" name="requestDate" value="" readonly>
								</div>
							</td>
						</tr>
						<tr>
							<th>용도</th>
							<td colspan="3">
								<div class="ui fluid icon input">
								  <input type="text" name="usage" placeholder="용도 입력">
								</div>
							</td>
						</tr>
					</table>
					<div class="buttonArea">
						<input type="hidden" name="title" value="제목" id="title2">
						<input type="hidden" name="firstApproval" id="firstApp2">
						<input type="hidden" name="secondApproval" id="secondApp2">
						<input type="hidden" name="thirdApproval" id="thirdApp2">
						<button class="ui blue button mini" id="add" onclick="return approvalFunction();">전자결재</button>
				</div>
				</form>
				</div><!-- 증명서 신청란 끝-->
			</div><!-- 증명서 신청 끝-->
		
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	
	<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
	<script>
		$(function() {
			/* 상단메뉴 */
			$("#member3").show();
			/* 탭메뉴 */
			$(".menu .item").tab();
			/* 드롭다운 */
			$(".ui.dropdown").dropdown();
		});
		
		/* 증명서 신청일자  */
		document.getElementById("today").valueAsDate = new Date();
		
		function request(){
			$("#approvalModal").modal('show');
		}
		
		/* 전자결재 */
		function approvalFunction(){
			var approvalCode = 12; //제증명 신청에 대한 결재코드
			//console.log("전자결재");
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					console.log(data);
					$("#appType").val("제증명 신청");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thirdName = data.appBasic.mlist[2].mName;
									var thirdMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
				},
				error : function(status){
					console.log(status);
				}
			});
			
			
			return false;
		}
			
			
			$(function(){
				$('#request').click(function(){
					console.log("눌렸음!");
					
					$("#title2").val($("#title").val());
					$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
					$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
					$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
					
					$('#certificationForm').submit();
					
				});
			})
		
	</script>
</body>
</html>