<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	.hru.board2 tbody>tr:not(.noeval):hover{
		cursor:pointer;
		background:#eee;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
			<div class="hru segment">
				<h3><i class="check square icon"></i>인사발령</h3>
				<table class="hru board2" style="width:99%">
					<thead>
						<tr>
							<th style="width:150px;">발령구분</th>
							<th style="width:200px;">발령일자</th>
							<th style="width:250px;">사업장</th>
							<th style="width:250px;">발령부서</th>
							<th>사유</th>
						</tr>
					</thead>
					<c:if test="${hrList.size() > 0}">
					<tbody>
						<c:forEach var="hr" items="${hrList}" varStatus="status">
							<tr class="noeval">
								<td>
									<c:choose>
										<c:when test="${hr.category eq 1}"><c:out value="입사"/></c:when>
										<c:when test="${hr.category eq 2}"><c:out value="직무이동"/></c:when>
										<c:when test="${hr.category eq 3}"><c:out value="부서이동"/></c:when>
										<c:when test="${hr.category eq 4}"><c:out value="조직책임자 임명"/></c:when>
									</c:choose>
								</td>
								<td><c:out value="${hr.moveDate}"/></td>
								<td><c:out value="${hr.groupName}"/></td>
								<td><c:out value="${hr.teamName}"/></td>
								<td><c:out value="${hr.reason}"/></td>
							</tr>
						</c:forEach>
					</tbody>
					</c:if>
					<c:if test="${hrList.size() eq 0}">
					<tbody>
						<tr class="noeval">
							<td style="padding: 50px" colspan="5">
								<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 인사발령 내역이 없습니다.</label></h2>
							</td>
						</tr>
					</tbody>
					</c:if>
				</table>
			</div>
			<div class="hru segment">
				<h3><i class="check square icon"></i>진급내역</h3>
				<table class="hru board2" style="width:99%">
					<thead>
						<tr class="noeval">
							<th style="width:200px;">진급일자</th>
							<th style="width:300px;">직급</th>
							<th style="width:300px;">사업장</th>
							<th style="width:300px;">부서</th>
							<th>사유</th>
						</tr>
					</thead>
					<c:if test="${prList.size() > 0}">
					<tbody>
						<c:forEach var="pr" items="${prList}" varStatus="status">
							<tr class="noeval">
								<td><c:out value="${pr.moveDate}"/></td>
								<td><c:out value="${fn:substring(pr.positionName, 0, 2)}"/></td>
								<td><c:out value="${pr.groupName}"/></td>
								<td><c:out value="${pr.teamName}"/></td>
								<td><c:out value="${pr.reason}"/></td>
							</tr>
						</c:forEach>
					</tbody>
					</c:if>
					<c:if test="${prList.size() eq 0}">
					<tbody>
						<tr class="noeval">
							<td style="padding: 50px" colspan="5">
								<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 진급 내역이 없습니다.</label></h2>
							</td>
						</tr>
					</tbody>
					</c:if>
					</tbody>
				</table>
			</div>
			<div class="hru segment">
				<h3><i class="check square icon"></i>인사평가 내역</h3>
				<table class="hru board2 eval" style="width:99%">
					<thead>
						<tr>
							<th>해당연도</th>
							<th>평가결과</th>
							<th>진행상황</th>
							<th>사업장</th>
							<th>부서</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${evalList.size() > 0 }">
						<c:forEach var="e" items="${evalList}" varStatus="status">
							<tr onclick="showInput('${e.evaluationId}')">
								<td><c:out value="${e.evaluationYear}"/></td>
								<td><c:out value="${e.totalScore}"/></td>
								<td>
									<c:if test="${e.approvalId == 0}">
										<c:if test="${e.approvalYn == 'P'}">입력기간</c:if>
										<c:if test="${e.approvalYn == 'N'}">준비기간</c:if>
									</c:if>
									<c:if test="${e.approvalId != 0}">
										<c:if test="${e.approvalYn == 'P'}">평가진행중</c:if>
										<c:if test="${e.approvalYn == 'Y'}">평가완료</c:if>
									</c:if>
								</td>
								<td><c:out value="${e.groupName}"/></td>
								<td><c:out value="${e.teamName}"/></td>
							</tr>
						</c:forEach>
						</c:if>
						<c:if test="${evalList.size() eq 0}">
							<tr class="noeval">
								<td style="padding: 50px" colspan="5">
									<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 인사평가 내역이 없습니다.</label></h2>
								</td>
							</tr>
						</c:if>
					</tbody>
				</table>
				<jsp:include page="../common/evaluation.jsp"/>
			</div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function(){
			$('#member1').show();
			
			$('#complete').click(function completeEval(){ //평가완료일때 --> 결재로 넘어가야 함
				console.log($("#complete").val());
				var type = $("#complete").val();
				var queryString = $("#evaluationForm").serializeArray();

				Swal.fire({
					  title: '저장하시겠습니까?',
					  text: "목표수립은 한번 저장하면, 변경할 수 없습니다.",
					  icon: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: '저장하기',
					  cancelButtonText : '취소하기',
					}).then((result) => {
					  if (result.value) {
					  	$.ajax({
							url:"updateEvalPersonal.mov",
							type:"POST",
							data:queryString,
							dataType : "json",
							success:function(data){
								$('#evaluationModal').modal('hide');
							},
							error:function(data){
								$('#evaluationModal').modal('hide');
							}
						});
					  }
					});
			});
		});
		
		function showInput(evalId){
			$.ajax({
				url : "selectEvaluationDetail.eval",
				type : "post",
				data : {
					evaluationId : evalId
				},
				success : function(data){
					console.log(data);
					var evaluation = data.eval;
					var edlist = data.edlist;
					var $twoWay = $("#twoWay");
					$twoWay.html("");
					var $oneWayBody = $("#oneWay tbody");
					$oneWayBody.html("");
					var $idInput = $("<input type='hidden'>").attr("name","evaluationId").val(evaluation.evaluationId);
					
					
					$("*[data-tab=twoWay]").addClass("active");
					$("*[data-tab=oneWay]").removeClass("active");
					$("#onewayScore").val("").append($idInput);
					$('#evaluationModal').find(".dropdown").addClass("disabled");
					$('#evaluationModal').find(".dropdown").find(".text").addClass("default").text("진급대상자 여부");
					$("#complete").prop("disabled", true);
					$("#evaluationMenu").find("a").addClass("disabled");
					
					for(var i=0; i<edlist.length; i++){
						if(edlist[i].category==1 && evaluation.approvalYn=='P' && evaluation.approvalId == 0 && edlist[i].ropinion == ""){ //양방향 평가일 경우 & 입력해야될때
							var $table = $("<table class='hru evaluation'>");
							var $tBody = $("<tbody>");
							
							var $th4 = $("<th colspan='4'>").html("* "+ edlist[i].evalName + " <span style='color:gray;'> (" + edlist[i].evalContent + ") </span>");
							var $tr0 = $("<tr>").append($th4);
							$tBody.append($tr0);
							
							var edId = "edlist[" + i + "].edId";
							var $input11 = $("<input type='hidden'>").val(edlist[i].edId).attr("name",edId);
							
							var $th1 = $("<th colspan='2'>").text("목표수립").append($input11);
							var $th2 = $("<th>").text("피평가자 의견");
							var $th3 = $("<th>").text("평가자 의견");
							var $tr1 = $("<tr>").append($th1).append($th2).append($th3);
							$tBody.append($tr1);
							
							var evalContent = "edlist[" + i + "].evalContent";
							var evalRopinion = "edlist[" + i + "].ropinion";
							var $td1 = $("<td>").append($("<textarea rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].edContent)); //EVAL_CONTENT
							var $td2 = $("<td>").text(">>");
							var $td3 = $("<td>").append($("<textarea rows='5' cols='30' style='resize:none;' name='ropinion'>")).append($("<input type='hidden' name='evalId'>").val(edlist[i].edId));
							var $td4 = $("<td>").append($("<textarea rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].gopinion));
							var $tr2 = $("<tr>").append($td1).append($td2).append($td3).append($td4)
							$tBody.append($tr2);
							
							var $td5 = $("<td colspan='5' style='text-align:right'>");
							var $label1 = $("<label class='hru label'>").text("가중치(%) : "); //EVAL_WEIGHT
							$td5.append($label1).append($("<div class='ui disabled input small' style='margin-right:20px;'>").append($("<input type='text' size='5' readonly>").val(edlist[i].evalWeight)));
							var $label2 = $("<label class='hru label'>").text("평가자점수 : ");
							$td5.append($label2).append($("<div class='ui disabled input small' style='margin-right:20px;'>").append("<input type='text' size='5' readonly>"));
							var $label3 = $("<label class='hru label'>").text("점수 : ");
							$td5.append($label3).append($("<div class='ui disabled input small' style='margin-right:20px;'>").append("<input type='text' size='5' readonly>"));
							var $tr3 = $("<tr>").append($td5);
							$tBody.append($tr3);
							
							$table.append($tBody);
							$twoWay.append($table).append($("<div class='ui divider'>"));
							$("#complete").text("제출").val(1);
							$("#complete").prop("disabled", false);

						} else if(edlist[i].category==1){
							var $table = $("<table class='hru evaluation'>");
							var $tBody = $("<tbody>");
							
							var $th4 = $("<th colspan='4'>").html("* "+ edlist[i].evalName + " <span style='color:gray;'> (" + edlist[i].evalContent + ") </span>");
							var $tr0 = $("<tr>").append($th4);
							$tBody.append($tr0);
							
							var edId = "edlist[" + i + "].edId";
							var $input11 = $("<input type='hidden'>").val(edlist[i].edId).attr("name",edId);
							
							var $th1 = $("<th colspan='2'>").text("목표수립").append($input11);
							var $th2 = $("<th>").text("피평가자 의견");
							var $th3 = $("<th>").text("평가자 의견");
							var $tr1 = $("<tr>").append($th1).append($th2).append($th3);
							$tBody.append($tr1);
							
							
							var $td1 = $("<td>").append($("<textarea rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].edContent)); //EVAL_CONTENT
							var $td2 = $("<td>").text(">>");
							var $td3 = $("<td>").append($("<textarea rows='5' class='ropinion' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].ropinion));
							var $td4 = $("<td>").append($("<textarea class='gopinion' rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].gopinion));
							var $tr2 = $("<tr>").append($td1).append($td2).append($td3).append($td4);
							$tBody.append($tr2);
							
							var $td5 = $("<td colspan='5' style='text-align:right'>");
							var $label3 = $("<label class='hru label'>").text("점수 : ");
							$td5.append($label3).append($("<div class='ui disabled input small disabled' style='margin-right:20px;'>").append($("<input class='each' type='text' size='5' value='0' readonly>").val(edlist[i].evalScore)));
							var $tr3 = $("<tr>").append($td5);
							$tBody.append($tr3);
							
							$table.append($tBody);
							$twoWay.append($table).append($("<div class='ui divider'>"));
							$("#bottomContent").css("display", "none");
						} 
					}
					
					$('#evaluationModal').modal('show');
			        
				},
				error : function(status){
					console.log(status);
				}
			});
			
			
		}
	</script>
</body>
</html>