<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	.ui.form .fields{
		margin-bottom: 0px;
	}
</style>
</head>
<body>
<jsp:include page="../common/menubar.jsp"/>
<div class="outer"><!--outer Area -->
<div class="contentArea"> <!-- 내용부분! -->
	<!-- 휴가 정보란 -->
	<div class="hru segment" style="margin-bottom:10px;">
		<label><i class="exclamation circle icon" id="exclamation"></i>휴가신청 후 (1)실제사용여부에 체크 (2)전자결재상태가 '완료(승인)'or'자동승인'이 된 경우에만 휴가 사용일수에 '+'가 되고 잔여일수에서'-'가 됩니다.</label>
		<table class="hru table">
					<tr>
						<th>사원번호</th>
						<td><c:out value="${vMap.mav.mId}"/></td>
						<th>성명</th>
						<td><c:out value="${vMap.mav.mName}"/></td>
						<th>연차기간</th>
						<td colspan="3"><c:out value="${vMap.mav.yearStart}"/> ~ <c:out value="${vMap.mav.yearEnd}"/></td>
					</tr>
					<tr>
						<th>발생일수</th>
						<td><c:out value="${vMap.mav.avNum}"/></td>
						<th>조정연차일수</th>
						<td><c:out value="${vMap.mav.addNum}"/></td>
						<th>사용일수</th>
						<td><c:out value="${vMap.mav.useNum}"/></td>
						<th>잔여일수</th>
						<td><c:out value="${vMap.mav.restNum}"/></td>
					</tr>
		</table>
	</div><!-- 휴가 정보란 끝-->
	
	<!-- 휴가신청란 -->
	<div class="hru segment" style="margin-bottom:10px;">
			<h3><i class="check square icon"></i>휴가신청</h3>
			<form action="insertVacationRequest.att" id="vacationRequestForm" enctype="multipart/form-data">
			<table class="hru table">
					<tr>
						<th>*기안일자</th>
						<td>
							<div class="ui calendar" id="standard_calendar">
							  <div class="ui input left icon">
							    <i class="calendar icon"></i>
							    <input type="text" id="requestDate">
							  </div>
							</div>
						</td>
						<th>*휴가구분</th>
						<td>
							<select class="ui dropdown" name="vCode" id="category">
								<option value="">휴가구분 선택</option>
								<option value="3">연차미사용/유급</option>
								<option value="2">연차미사용/무급</option>
								<option value="1">연차사용/유급</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>*휴가기간</th>
						<td><div class="ui form">
								<div class="two fields">
									<div class="field">
										<div class="ui calendar" id="rangestart"><div class="ui input left icon">
												<i class="calendar icon"></i> <input type="text" id="vaStart" name="vStart" placeholder="시작일" style="width:100px;" readonly>
											</div>
										</div>
									</div>
									~
									<div class="field">
										<div class="ui calendar" id="rangeend"><div class="ui input left icon">
												<i class="calendar icon"></i> <input type="text" id="vaEnd" placeholder="종료일" style="width:100px;" readonly>
											</div>
										</div>
									</div>
									
								<div class="ui input">
								  (일수: <input style="width:50px;" id="vaNum" name="vNum" type="text" value="" readonly>)
								</div>
								<label> </label>
								</div>
								
							</div>
						</td>
						<th>전자결재상태</th>
						<td>
							<div class="ui input">
							  <input type="text" value="요청" readonly>
							</div>
						</td>
					</tr>
					<tr>
						<th>*휴가사유</th>
						<td colspan="3">
							<div class="ui fluid icon input">
							  <input type="text" name="reason" placeholder="휴가사유 입력">
							</div>
						</td>
					</tr>
		</table>
				<div class="buttonArea">
						<input type="hidden" name="title" value="제목" id="title2">
						<input type="hidden" name="firstApproval" id="firstApp2">
						<input type="hidden" name="secondApproval" id="secondApp2">
						<input type="hidden" name="thirdApproval" id="thirdApp2">
						<button class="ui blue button mini" id="add" onclick="return approvalFunction();">전자결재</button>
				</div>
		</form>
	</div>
	<div class="hru segment">
			<h3><i class="check square icon"></i>휴가신청 이력</h3>
			<table class="hru board2" border="1">
						<thead>
							<tr>
								<th>NO</th>
								<th>구분</th>
								<th>휴가구분</th>
								<th>신청일</th>
								<th>시작일</th>
								<th>종료일</th>
								<th>일수</th>
								<th>전자결재 상태</th>
								<th>사유</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="i" items="${vMap.vrList}" varStatus="status">
							<tr class="vacationTable-tr">
								<td>${status.count} <input type="hidden" value="${i.vacationId}"></td>
								<c:if test="${i.approvalCode == 8}">
								<td>휴가신청</td>
								</c:if>
								<c:if test="${i.approvalCode == 11}">
								<td>휴가취소</td>
								</c:if>
								<c:if test="${i.vacationCode == 1}">
								<td><c:out value="연차사용/유급"/></td></c:if>
								<c:if test="${i.vacationCode == 2}">
								<td><c:out value="연차미사용/무급"/></td></c:if>
								<c:if test="${i.vacationCode == 3}">
								<td><c:out value="연차미사용/유급"/></td></c:if>
								<td>${i.approvalDate }</td>
								<td>${i.vacationStart}</td>
								<td>${i.vacationEnd}</td>
								<td>${i.vacationNum }</td>
								<c:if test="${i.approvalYN =='Y' }">
								<td><c:out value="승인"/></td></c:if>
								<c:if test="${i.approvalYN =='N' }">
								<td><c:out value="미승인"/></td></c:if>
								<c:if test="${i.approvalYN =='C' }">
								<td><c:out value="반려"/></td></c:if>
								<td>${i.reason}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
	</div>
	<form action="vacationCancel.att" id="vacationCancelForm" method="post">
		<input type="hidden" name="vacationId" id="cancleVId" value="">
		<input type="hidden" name="title" value="제목" id="title3">
		<input type="hidden" name="firstApproval" id="firstApp3">
		<input type="hidden" name="secondApproval" id="secondApp3">
		<input type="hidden" name="thirdApproval" id="thirdApp3">
	</form>
</div>
</div>
<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
<script>
function approvalFunction(){
	var approvalCode = 8; //신규사원등록에 대한 결재코드
	//console.log("전자결재");
	$.ajax({
		url : "selectOneBasic.app",
		type : "post",
		data : {approvalCode : approvalCode},
		success : function(data){
			console.log(data);
			$("#appType").val("휴가신청");
			var usermid = "${loginUser.mid}"; //기안자사번
			var yesApp = 0;
			
			for(var i=0; i<data.appBasic.mlist.length; i++){
				if(data.appBasic.mlist[i].mid == usermid){
					yesApp = 1;
				}
			}
			
			if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
				
				if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
					var firstName = data.appBasic.mlist[0].mName;
					var firstMid = data.appBasic.mlist[0].mid;
					var $input = $("<input type='hidden' name='first'>").val(firstMid);
					$("#firstApp").text(firstName).append($input);

					if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
						var secondName = data.appBasic.mlist[1].mName;
						var secondMid = data.appBasic.mlist[1].mid;
						var $input = $("<input type='hidden' name='second'>").val(secondMid);
						$("#secondApp").text(secondName).append($input);
						
						if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
							var thirdName = data.appBasic.mlist[2].mName;
							var thirdMid = data.appBasic.mlist[2].mid;
							var $input = $("<input type='hidden' name='third'>").val(thirdMid);
							$("#thirdApp").text(thirdName).append($input);
						}
					}
				}
			}
			$("#approvalModal").modal('show');
		},
		error : function(status){
			console.log(status);
		}
	});
	
	
	return false;
}
	
	
	$(function(){
		$('#request').click(function(){
			console.log("눌렸음!");
			
			
			if($("#appType").val() == "휴가신청 취소"){
				$("#title3").val($("#title").val());
				$("#firstApp3").val($("#firstApp").find("input[type=hidden]").val());
				$("#secondApp3").val($("#secondApp").find("input[type=hidden]").val());
				$("#thirdApp3").val($("#thirdApp").find("input[type=hidden]").val());
				$("#vacationCancelForm").submit();
			}else if($("#appType").val() == "휴가신청"){
				$("#title2").val($("#title").val());
				$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
				$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
				$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
				/* $('#vacationRequestForm').submit(); */
				
				/* 휴가신청 AJAX */
				var params = $("#vacationRequestForm").serialize();
				$.ajax({
					url:"insertVacationRequest.att",
					type:'post',
					data:params,
					success:function(data){
						console.log(data);
						if(data.result == 1){
							Swal.fire(
									  '연차 초과',
									  '연차 사용을 초과하셨습니다.',
									  'warning'
							).then((result)=>{
								$("#approvalModal").modal('hide');
							});
						}else if(data.result == 2){
							Swal.fire(
									  '날짜 중복',
									  '해당 일자에 이미 휴가를 신청하셨습니다.',
									  'warning'
							).then((result)=>{
								$("#approvalModal").modal('hide');
							});
						}else if(data.result == 3){
							Swal.fire(
									  '휴가 신청 완료',
									  '휴가 신청을 완료되었습니다.',
									  'success'
							).then((result)=>{
								$("#approvalModal").modal('hide');
								var url = "showInfoVacation.att";
								$(location).attr('href',url);
							});
						}
						
						var content = "결재요청 : " + $("#title").val();
						var tmid = $("#firstApp").find("input[type=hidden]").val();
						if(tmid!="" && tmid!=null && tmid!=0){
							send(tmid, content);
						}
					},
					error:function(status){
						console.log("실패!");
					}
				})
			}
			
			
		});
	})

	$(function() {
		/* 상단메뉴 */
		$("#member2").show();
		/* 드롭다운 */
		$(".ui.dropdown").dropdown();
		
		var date = getTodayType();
		$("#requestDate").val(date);
		
		function getTodayType(){
			var date = new Date();
			return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
		}
		
		/* 후가 시작 input */
		var $sInput = $("#vaStart");
		$("#vaStart").on('input', function(){
			console.log("startInput text chagne!" + $(this).val());
		})
		
		/* 휴가 끝 input */
		var $input = $("#vaEnd");
		$("#vaEnd").on('input', function(){
			console.log("input text change!" + $(this).val());
			var startdate =$("#vaStart").val();
			var enddate = $(this).val();
			sDate = new Date(startdate);
			eDate = new Date(enddate);
			
			diffDay = (eDate.getTime() - sDate.getTime()) / (1000*60*60*24) + 1;
			console.log("sDate : " + sDate);
			
			console.log("sDate.getTime() : " + sDate.getTime());
			console.log("eDate : " + eDate);
			console.log("eDate.getTime() : " +(eDate.getTime()/(1000*60*60*24)));
			console.log("일 차이 :" + diffDay);
			sNum = ((sDate.getTime()+ (1000*60*60*24)) - sDate.getTime()) / (1000*60*60*24);
			eNum = ((eDate.getTime()+ (1000*60*60*24)) - sDate.getTime()) / (1000*60*60*24);
			console.log("sNum : " + sNum);
			console.log("eNum : " + eNum);
			
			week = new Array("","월", "화", "수", "목", "금","토","일");
			theDayOfWeek = sDate.getDay();
			theDayOfLast = eDate.getDay();
			console.log("theDayOfWeek : " + theDayOfWeek);
			console.log("theDayOfLast : " + theDayOfLast);
			ctn = 0;
			legnth = theDayOfWeek + diffDay;
			console.log("시작요일 : " + sDate.getDay());
			console.log("하루 더하기 : " + sDate.setDate(sDate.getDate() + 1));
			console.log("하루 + 1 : " + sDate.toLocaleString());
			console.log("sNum: " + sNum);
			for(i = sNum; i <= eNum; i ++){
				var fday = sDate.setDate(sDate.getDate() + i );
				console.log("date: " + new Date(fday));
				console.log("fday : "  + fday);
				var yearMonthDate = new Date(fday);
				console.log("yearMothDate: "  + yearMonthDate);
				console.log(typeof(yearMonthDate));
				
				var weeks = yearMonthDate.getDay();
				console.log("week : " + weeks);
				if(weeks == 6 || weeks == 7){
					ctn ++;
				}
			} 
			console.log("ctn : " + ctn);
			
			$("#vaNum").val(diffDay);
			console.log(sDate);
			console.log($("#vaNum").val());
			
		});
		
		var originVal = $.fn.val;
		$.fn.val = function(value){
			var res = originVal.apply(this, arguments);
			
			if(this.is('input:text') && arguments.length>=1){
				this.trigger("input");
			}
			
			return res;
		}
		
		/* 휴가신청 이력 테이블 */
		$(".vacationTable-tr").click(function(){
			/* vacationId */
			var vacationId = $(this).find("input").eq(0).val();
			console.log("input value : " + vacationId);
			$("#cancleVId").val(vacationId);
			console.log($("#cancleVId").val());
			var vacationDate = $(this).find("td").eq(4).text();
			var vacationDateArr = vacationDate.split("-");
			var today = new Date();
			var year = today.getFullYear();
			var month = today.getMonth()+1; 
			var date = today.getDate();
			var todayCompare = new Date(year,month, date);
			var vacationDate = new Date(vacationDateArr[0], vacationDateArr[1], vacationDateArr[2])
			
			
			var type = $(this).find("td").eq(1).text();
			console.log(type);
			
			if(vacationDate.getTime() > todayCompare.getTime() && type == "휴가신청"){
					var approvalCode = 11; //휴가신청 취소에 대한 결재코드
					//console.log("전자결재");
					$.ajax({
						url : "selectOneBasic.app",
						type : "post",
						data : {approvalCode : approvalCode},
						success : function(data){
							console.log(data);
							$("#appType").val("휴가신청 취소");
							var usermid = "${loginUser.mid}"; //기안자사번
							var yesApp = 0;
							
							for(var i=0; i<data.appBasic.mlist.length; i++){
								if(data.appBasic.mlist[i].mid == usermid){
									yesApp = 1;
								}
							}
							
							if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
								
								if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
									var firstName = data.appBasic.mlist[0].mName;
									var firstMid = data.appBasic.mlist[0].mid;
									var $input = $("<input type='hidden' name='first'>").val(firstMid);
									$("#firstApp").text(firstName).append($input);

									if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
										var secondName = data.appBasic.mlist[1].mName;
										var secondMid = data.appBasic.mlist[1].mid;
										var $input = $("<input type='hidden' name='second'>").val(secondMid);
										$("#secondApp").text(secondName).append($input);
										
										if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
											var thidName = data.appBasic.mlist[2].mName;
											var thidMid = data.appBasic.mlist[2].mid;
											var $input = $("<input type='hidden' name='third'>").val(thirdMid);
											$("#thirdApp").text(thirdName).append($input);
										}
									}
								}
							}
							$("#approvalModal").modal('show');
						},
						error : function(status){
							console.log(status);
						}
					});
					
					return false;
				
			}else{
				if(type =="휴가취소"){
					Swal.fire(
						'휴가취소 불가',
						'이미 휴가취소된 결재입니다.',
						'error'
					)
				}else {
					/* 사용한 휴가 */
					Swal.fire(
						  '휴가 취소 불가',
						  '이미 사용하신 휴가입니다.',
						  'warning'
					)
				}
				
				
				/* console.log("휴가 신청 불가");
				alert("휴기 신청 취소가 불가능합니다."); */
			} 
			
		});
	});	
	
	/* 휴가기간 선택 데이터피커  */
	$("#rangestart").calendar({
						type:'date',
						endCalendar : $('#rangeend'),
						formatter: {
						      date: function (date, settings) {
						        if (!date) return '';
						        var day = date.getDate();
						        var day2;
						        var month = date.getMonth() + 1;
						        var month2;
					    	    var year = date.getFullYear();
					    	      if(month < 10){
					    	    	  month2 = '0' + month;
					    	      }else {
					    	    	  month2 = month;
					    	      }
					    	      if(day < 10){
					    	    	  day2 = '0' + day;
					    	      }else {
					    	    	  day2 = day;
					    	      }
						        var year = date.getFullYear();
						        return year + '-' + month2 + '-' + day2;
						      }
						},
						disabledDaysOfWeek: [0,6]
					});
					$("#rangeend").calendar({
						type:'date',
						startCalendar : $('#rangestart'),
						formatter: {
						      date: function (date, settings) {
						        if (!date) return '';
						        var day = date.getDate();
						        var day2;
						        var month = date.getMonth() + 1;
						        var month2;
					    	      var year = date.getFullYear();
					    	      if(month < 10){
					    	    	  month2 = '0' + month;
					    	      }else {
					    	    	  month2 = month;
					    	      }
					    	      if(day < 10){
					    	    	  day2 = '0' + day;
					    	      }else {
					    	    	  day2 = day;
					    	      }
						        var year = date.getFullYear();
						        return year + '-' + month2 + '-' + day2;
						      }
						
						},
						
						disabledDaysOfWeek: [0,6]
	}); 
					
	$("#vaEnd").on("propertychange change keyup paste input", function(){
		console.log("마지막날 변경");
	});
					
	</script>
</body>
</html>