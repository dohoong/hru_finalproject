<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hru</title>
<jsp:include page="../common/importlib.html"/>
<link href='resources/js/calendar/core-main.min.css' rel='stylesheet' />
<link href='resources/js/calendar/daygrid-main.min.css' rel='stylesheet' />
<link href='resources/js/calendar/timegrid-main.min.css' rel='stylesheet' />
<script src='resources/js/calendar/core-main.min.js'></script>
<script src='resources/js/calendar/daygrid-main.min.js'></script>
<script src='resources/js/calendar/interaction-main.min.js'></script>
<script src='resources/js/calendar/timegrid-main.min.js'></script>
<style>
	.leftMenu{
		height:1020px;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	<div class="outer"><!--outer Area -->
		<div class="contentArea"> <!-- 내용부분! -->
			<div class="hru segment">
				<h1><i class="check square icon"></i>내 정보</h1>
				<table class="hru table">
					<tr>
						<th style="width:350px;">사원번호</th>
						<td><c:out value="${loginUser.mid}"/></td>
						<th style="width:350px;">이름</th>
						<td><c:out value="${loginUser.mName}"/></td>
					</tr>
					<tr>
						<th>사업장</th>
						<td colspan="3"><c:out value="${loginUser.mhi.gname}"/></td>
					</tr>
					<tr>
						<th>부서</th>
						<td><c:out value="${loginUser.mhi.tname}"/></td>
						<th>직급</th>
						<td><c:out value="${loginUser.mhi.ppname}"/></td>
					</tr>
				</table>
			</div> <!-- 내정보 끝 -->
			<div class="hru segment">
				<h1><i class="check square icon"></i>캘린더</h1>
				<!-- <div style="height:400px; border:1px solid lightgray; border-radius:10px;"></div> -->
				<div  id="calendar2" style=""></div>
			</div><!-- 캘린더 끝 -->
			<div class="ui horizontal segments">
				<div class="ui segment"><h3><i class="check square icon"></i>공지사항</h3>
					<table class="hru board">
						<thead>
							<tr>
								<th style="width:150px;">작성일자</th>
								<th style="width:350px;">제목</th>
								<th style="width:120px;">작성자</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>2020-01-02</td>
								<td>새해가 되었습니다. 올해도 열심히 일합시다.</td>
								<td>대표이사</td>
							</tr>
							<tr>
								<td>2019-12-01</td>
								<td>[인사팀] 인사평가 기간입니다.</td>
								<td>인사팀장</td>
							</tr>
							<tr>
								<td>2019-05-20</td>
								<td>[인사팀] 회사 전체 야유회 일정 공지</td>
								<td>인사팀장</td>
							</tr>
							<tr>
								<td>2019-02-15</td>
								<td>[회계팀] 연말정산 서비스를 마감합니다.</td>
								<td>업무지원팀</td>
							</tr>
							<tr>
								<td>2019-02-02</td>
								<td>[회계팀] 연말정산 기간내 서류 제출해주세요.</td>
								<td>업무지원팀</td>
							</tr>
							<tr>
								<td>2019-01-01</td>
								<td>공지사항 게시판이 열렸습니다.</td>
								<td>대표이사</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="ui segment" style="width:800px;"><h3><i class="check square icon"></i>전자결재</h3>
					<table class="hru board">
						<thead>
							<tr>
								<th>기안일자</th>
								<th>제목</th>
								<th>종류</th>
								<th>상태</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${!empty applist && applist.size()<5}">
							<c:forEach var="i" begin="0" end="${applist.size()-1}">
								<tr onclick="goApprovalList();">
									<td><fmt:formatDate value="${applist.get(i).get('approvalDate')}" pattern="yyyy-MM-dd" /></td>
									<td><c:out value="${applist.get(i).get('approvalTitle')}"/></td>
									<td><c:out value="${applist.get(i).get('approvalName')}"/></td>
									<td><c:if test="${applist.get(i).get('status')==2 || applist.get(i).get('status')==1}">진행중</c:if>
										<c:if test="${applist.get(i).get('status')==3}">완료</c:if>
										<c:if test="${applist.get(i).get('status')==4}">반려</c:if></td>
								</tr>
							</c:forEach>
							</c:if>
							<c:if test="${applist.size()>5}">
							<c:forEach var="i" begin="0" end="5">
								<tr onclick="goApprovalList();">
									<td><fmt:formatDate value="${applist.get(i).get('approvalDate')}" pattern="yyyy-MM-dd" /></td>
									<td><c:out value="${applist.get(i).get('approvalTitle')}"/></td>
									<td><c:out value="${applist.get(i).get('approvalName')}"/></td>
									<td><c:if test="${applist.get(i).get('status')==2 || applist.get(i).get('status')==1}">진행중</c:if>
										<c:if test="${applist.get(i).get('status')==3}">완료</c:if>
										<c:if test="${applist.get(i).get('status')==4}">반려</c:if></td>
								</tr>
							</c:forEach></c:if>
						</tbody>
					</table>
				
				
				
				</div>
			</div><!-- 공지사항/전자결재 끝 -->
			
		</div>
		
	</div>
	<!-- <button class="ui blue button" id="test">토스트테스트!</button> -->
	<script>
		<c:if test="${!empty alist}">
		<c:forEach var="i" begin="0" end="${alist.size()-1}">
			var alarmId = "${alist.get(i).alarmId}";
			var content = "${alist.get(i).content}";
			var alarmDate = "${alist.get(i).alarmDate}";
			//var delta = 1000*"${i}";
			//console.log(delta);
			openToast(alarmId, alarmDate, content);
			$("#alarm").css("color", "red").transition('tada').transition('tada');
		</c:forEach>
		</c:if>
		
		
		function goApprovalList(){
			location.href="showApprovalRequestList.app";
		}
		
		document.addEventListener('DOMContentLoaded', function() {
			var Calendar = FullCalendar.Calendar;
			var calendarEl2 = document.getElementById('calendar2');
			
			var calendar2 = new FullCalendar.Calendar(calendarEl2, {
				plugins: [ 'dayGrid', 'timeGrid' ],
				defaultView: 'dayGridMonth',
				fixedWeekCount : false,
				defaultDate: new Date(),
				height : 500,
				header: {
					right: 'prev,next today',
					left: 'title',
					center : ''
				},
				eventSources : [{
					events : function(info, successCallback, failureCallback){
						$.ajax({
							url : "selectCalendarAll.app",
							type : "post",
							dataType : 'json',
							data : {
								start : moment(info.startStr).format('YYYY-MM-DD'),
								end : moment(info.endStr).format('YYYY-MM-DD')
							},
							success : function(data){
								//console.log(data.jarr);
								successCallback(data.jarr);
							},
							error : function(status){
								failureCallback(status);
							}
						});
					}
				}],
				eventRender: function(info) {
					var category = "";
					switch(info.event.extendedProps.category){
					case 1 : category="비공식일정"; break;
					case 2 : category="공식일정"; break;
					case 3 : category="법정/약정휴일"; break;
					}
					$(info.el).popup({
						title : category,
						content : info.event.extendedProps.content
					});
					
				 }
			});
			
			calendar2.render();
			
		});
	</script>
</body>
</html>









