<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<jsp:include page="../common/importlib.html"/>
<style>
	body{
		background:#ededed;
	}
	#headMenu{
		font-family: 'Noto Sans KR', sans-serif;
		color:#0F4C81;
		font-weight:bold;
		text-align:left;
		margin-left:15%;
		margin-top:3%;
	}
	#menuName{
		color:#0F4C81;
		font-family: 'Noto Sans KR', sans-serif;
		font-weight:bold;
		font-size:15px;
	}
	#outer{
		margin-left:auto;
		margin-right:auto;
		margin-top:10%;
	}
	.image{
		magin:auto auto;
	}
	.ui.segment{
		width:150px;
		height:150px;
		margin-left:auto;
		margin-right:auto;
	}
	.ui.segment:hover{
		cursor:pointer;
	}
	.menuIcon{
		height:70px;
	}
	.contentArea td{
		width:170px;
	}
	.contentArea tr{
		height:170px;
	}
	#inner{
		margin-top:3%;
	}
	/* table{
		margin-top:3%;
	} */
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	
	<div class="outer">
	<div class="contentArea" align="center"> 
		<h2 id="headMenu"><i class="won sign icon"></i>급여관리</h2>
		<table>
			<tr>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showOneDayAttend.att'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/일근태관리.png">
							<h3 id="menuName">일 근태관리</h3>
						</div>
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showAttendanceManage.att'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/월근태관리.png">
							<h3 id="menuName">월 근태관리</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showMonthAttend.att'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/월근태현황.png">
							<h3 id="menuName">월 근태현황</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showAttendSearch.att'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/기타근무조회.png">
							<h3 id="menuName">기타근무조회</h3>
						</div> 
					</div> 
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showQuitMoneyResult.temp'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/퇴직금산정.png">
							<h3 id="menuName">퇴직금 산정/조회</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showYearEndAccount.me'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/연말정산.png">
							<h3 id="menuName">연말정산 기초</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showYearEndDocument.me'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/증명서류.png">
							<h3 id="menuName">증명 서류</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showYearEndEtcDocument.me'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/기타서류.png">
							<h3 id="menuName">기타 서류</h3>
						</div> 
					</div> 
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showWageBasicInformation.salary'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/급여기본정보.png">
							<h3 id="menuName">급여 기본정보</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showinsertWage.salary'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/급여입력.png">
							<h3 id="menuName">급여 입력</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showMonthlyWageStatus.salary'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/급여지급현황.png">
							<h3 id="menuName">급여 지급 현황</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showWageStatusAndEmail.salary'">
							<img class="menuIcon" src="resources/images/salaryManagementIcon/이메일 전송.png">
							<h3 id="menuName">이메일 발송</h3>
						</div> 
					</div> 
				</td>
			</tr>
		</table>
	</div>
	</div>
	
</body>
</html>