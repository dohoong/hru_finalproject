<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hru</title>
<jsp:include page="../common/importlib.html"/>
<style>
	body{
		background:#ededed;
	}
	#headMenu{
		font-family: 'Noto Sans KR', sans-serif;
		color:#0F4C81;
		font-weight:bold;
		text-align:left;
		margin-left:15%;
		margin-top:3%;
	}
	#menuName{
		color:#0F4C81;
		font-family: 'Noto Sans KR', sans-serif;
		font-weight:bold;
		font-size:15px;
	}
	#outer{
		margin-left:auto;
		margin-right:auto;
		margin-top:10%;
	}
	.image{
		magin:auto auto;
	}
	.ui.segment{
		width:150px;
		height:150px;
		margin-left:auto;
		margin-right:auto;
	}
	.ui.segment:hover{
		cursor:pointer;
	}
	.menuIcon{
		height:70px;
	}
	td{
		width:190px;
	}
	tr{
		height:210px;
	}
	#inner{
		margin-top:3%;
	}
	table{
		margin-top:3%;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	
	<div class="outer">
	<div class="contentArea" align="center"> 
		<h2 id="headMenu"><i class="users icon"></i>인사관리</h2>
		<table>
			<tr>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showMemberInsert.me'">
							<img class="menuIcon" src="resources/images/humanManagementIcon/신입사원등록.png">
							<h3 id="menuName">신규사원 등록</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showMemberList.hr'">
							<img class="menuIcon" src="resources/images/humanManagementIcon/사원관리.png">
							<h3 id="menuName">사원관리</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showOrganizationChart.app'">
							<img class="menuIcon" src="resources/images/humanManagementIcon/조직도.png">
							<h3 id="menuName">조직도</h3>
						</div> 
					</div> 
				</td>
			</tr>
			
			<tr>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showTeamList.au'">
							<img class="menuIcon" src="resources/images/humanManagementIcon/조직조회.png">
							<h3 id="menuName">조직조회</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showEnterpriseMemberMain.hr'">
							<img class="menuIcon" src="resources/images/humanManagementIcon/인사계획.png">
							<h3 id="menuName">인사계획</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showEvaluateMember.hr'">
							<img class="menuIcon" src="resources/images/humanManagementIcon/인사평가.png">
							<h3 id="menuName">인사평가</h3>
						</div> 
					</div> 
				</td>
			</tr>
		</table>
	</div>
	</div>
	
	
	<!-- <button class="ui blue button" id="test">토스트테스트!</button> -->
	<script>
	</script>
</body>
</html>