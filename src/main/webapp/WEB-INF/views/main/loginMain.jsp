<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	.bgDiv{
		width: 100%;
		height: 100%;
		background-image: url('./resources/images/mainPolygon.png');
	}
	.rightDiv{
		width:700px;
		height:100%;
		float:right;
		background:#E9F4FF;
	}
	.outer2{
		width:500px;
		margin-right:auto;
		margin-left:auto;
	}
	.upperArea{
		height:400px;
		text-align:center;
		vertical-align:bottom;
		padding-top:130px;
	}
	.loginArea{
		padding:20px;
		text-align:center;
		margin-right:auto;
		margin-left:auto;
		background:white;
		border-radius:5px;
		width:400px;
		
		}
	.loginBtn{
		height:80px;
		width:80px;
		border-radius:5px;
	}
	.loginBtn:hover{
		cursor:pointer;
	}
</style>
</head>
<body>
	<c:set var="contextPath" value="${ pageContext.servletContext.contextPath }" />
	<div class="bgDiv">
		<div class="rightDiv">
			<div class="outer2">
				<div class="upperArea">
					<h1 style="color:#0F4C81">Human Resource for U</h1>
					<img src="${contextPath}/resources/images/HRU.png" width="200px;">
				</div>
				<div class="lowerArea">
				<form action="login.me" method="post">
					<table class="loginArea" align="center" cellspacing="10px">
						<tr>
							<!-- <td>아이디</td> -->
							<td><div class="ui left icon fluid input"><i class="user icon"></i>
								<input type="text" name="mid" placeholder="사원번호를 입력하세요"></div></td>
							<!-- <td rowspan="2"><button class="loginBtn">로그인</button></td> -->
						</tr>
						<tr>
							<!-- <td>비밀번호 </td> -->
							<td><div class="ui left icon fluid input"><i class="lock icon"></i>
								<input type="password" name="mPwd" placeholder="비밀번호를 입력하세요"></div></td>
						</tr>
						<tr>
							<td colspan="2"><button class="ui blue fluid button" id="loginBtn">로 그 인</button></td>
						</tr>
						<tr>
							<td colspan="2" align="right">
								<a href="#"> 아이디/비밀번호 찾기</a>
							</td>
						</tr>
					</table>
				</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>