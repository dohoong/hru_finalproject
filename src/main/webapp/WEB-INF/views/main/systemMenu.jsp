<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<jsp:include page="../common/importlib.html"/>
<style>
	body{
		background:#ededed;
	}
	#headMenu{
		font-family: 'Noto Sans KR', sans-serif;
		color:#0F4C81;
		font-weight:bold;
		text-align:left;
		margin-left:15%;
		margin-top:3%;
	}
	#menuName{
		color:#0F4C81;
		font-family: 'Noto Sans KR', sans-serif;
		font-weight:bold;
		font-size:15px;
	}
	#outer{
		margin-left:auto;
		margin-right:auto;
		margin-top:10%;
	}
	.image{
		magin:auto auto;
	}
	.ui.segment{
		width:150px;
		height:150px;
		margin-left:auto;
		margin-right:auto;
	}
	.ui.segment:hover{
		cursor:pointer;
	}
	.menuIcon{
		height:70px;
	}
	td{
		width:190px;
	}
	tr{
		height:210px;
	}
	#inner{
		margin-top:3%;
	}
	table{
		margin-top:3%;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	
	<div class="outer">
	<div class="contentArea" align="center"> 
		<h2 id="headMenu"><i class="server icon"></i>시스템관리</h2>
		<table>
			<tr>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showOrganizationManageMent.st'">
							<img class="menuIcon" src="resources/images/systemManagementIcon/조직관리.png">
							<h3 id="menuName">조직관리</h3>
						</div>
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showPositionAdministration.st'">
							<img class="menuIcon" src="resources/images/systemManagementIcon/직급관리.png">
							<h3 id="menuName">직급관리</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showCalendarAdministration.temp'">
							<img class="menuIcon" src="resources/images/systemManagementIcon/달력관리.png">
							<h3 id="menuName">달력관리</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showVacationAdministration.att'">
							<img class="menuIcon" src="resources/images/systemManagementIcon/휴가관리.png">
							<h3 id="menuName">휴가관리</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showConnectionManagement.st'">
							<img class="menuIcon" src="resources/images/systemManagementIcon/접속관리.png">
							<h3 id="menuName">접속관리</h3>
						</div> 
					</div> 
				</td>
			</tr>
					<tr>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='selectList.au'">
							<img class="menuIcon" src="resources/images/systemManagementIcon/권한관리.png">
							<h3 id="menuName">권한관리</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showCompanyInfo.st'">
							<img class="menuIcon" src="resources/images/systemManagementIcon/회사정보.png">
							<h3 id="menuName">회사정보</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showSalaryAttitudeAdministration.salary'">
							<img class="menuIcon" src="resources/images/systemManagementIcon/급여근태기준.png">
							<h3 id="menuName">급여/근태 기준</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner" onclick="location.href='showApprovalLine.app'">
							<img class="menuIcon" src="resources/images/systemManagementIcon/결재라인.png">
							<h3 id="menuName">결재라인</h3>
						</div> 
					</div> 
				</td>
				<td>
					<div class="ui segment" align="center">
						<div id="inner">
							<img class="menuIcon" src="resources/images/systemManagementIcon/통계자료.png">
							<h3 id="menuName">통계자료</h3>
						</div> 
					</div> 
				</td>
			</tr>
		</table>
	</div>
	</div>
	
	
	<!-- <button class="ui blue button" id="test">토스트테스트!</button> -->

</body>
</html>