<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<link rel="stylesheet" type="text/css" href="resources/js/orgchart/jquery.orgchart.css">
<script type="text/javascript" src="resources/js/orgchart/jquery.orgchart.js"></script>
<style>
	#chart-container{
		width:100%;
		/* border:1px solid lightgray; */
		border-radius:10px;
		text-align:center;
	}
    .orgchart { 
    	background: #fff; 
    	margin-right:auto;
    	margin-left:auto;
    }
    .orgchart .content{
    	font-size:1.1em !important;
    	height: 100% !important;
    }
    
    .orgchart .title{
    	font-size:1.2em !important;
    }
    .orgchart .topLine, .orgchart .rightLine, .orgchart .leftLine{
    	border-color:lightgray !important;
    }
    .orgchart .lines .downLine {
    	background-color:lightgray !important;
    }
    
    .orgchart td.left, .orgchart td.right, .orgchart td.top { border-color: #aaa; }
    .orgchart td>.down { background-color: #aaa; }
    .orgchart .middle-level .title { background-color: #3886ad; }
    .orgchart .middle-level .content { border-color: #3886ad; }
    .orgchart .third-level .title { background-color: #459ead; }
    .orgchart .third-level .content { border-color: #459ead; }
    .orgchart .third-level2 .title { background-color: purple; }
    .orgchart .third-level2 .content { border-color: purple; }
 
    .orgchart .second-menu-icon {
      transition: opacity .5s;
      opacity: 0;
      right: -5px;
      top: -5px;
      z-index: 2;
      position: absolute;
    }
    .orgchart .second-menu-icon::before { background-color: rgba(68, 157, 68, 0.5); }
    .orgchart .second-menu-icon:hover::before { background-color: #449d44; }
    .orgchart .node:hover .second-menu-icon { opacity: 1; }
    .orgchart .node .second-menu {
      display: none;
      position: absolute;
      top: 0;
      right: -70px;
      border-radius: 35px;
      box-shadow: 0 0 10px 1px #999;
      background-color: #fff;
      z-index: 1;
    }
    .orgchart .node .second-menu .avatar {
      width: 60px;
      height: 60px;
      border-radius: 30px;
      float: left;
      margin: 5px;
    }
    .hru.segment .ui.card{
    	width:23% !important;
    }
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea" style="padding-top:30px;"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
			<div class="hru segment" style="width:99%; margin-bottom:0;">
				<h3><i class="check square icon"></i>조직도</h3>
				<div id="chart-container" style="overflow:hidden;"></div>
				<div class="ui divider"></div>
			</div>
			<div class="hru segment" style="width:99%;">
				<h3><i class="check square icon"></i>협력업체</h3>
				<div class="ui horizontal cards" style="margin-left:30px;">
					<a class="ui card" href="#">
						<div class="content">
							<div class="header">HOTELS COMPILE</div>
							<div class="meta"><span class="category">부띠끄호텔 관리 서비스</span></div>
							<div class="description">
								<p><img src="resources/images/hotelsCompoileLogo.png" width="100%">
								</p></div>
						</div></a>
					<a class="ui card" href="#">
						<div class="content">
							<div class="header">SELLMORE</div>
							<div class="meta"><span class="category">영업관리 프로그램</span></div>
							<div class="description">
								<p><img src="resources/images/sellmoreLogo.png" width="100%">
								</p></div>
						</div></a>
					<a class="ui card" href="#">
						<div class="content">
							<div class="header">CORE WORKS</div>
							<div class="meta"><span class="category">그룹웨어 프로그램</span></div>
							<div class="description">
								<p><img src="resources/images/coreworksLogo.png" width="100%">
								</p></div>
						</div></a>
					<a class="ui card" href="#">
						<div class="content">
							<div class="header">SURVWAY</div>
							<div class="meta"><span class="category">리서치 서비스 제공</span></div>
							<div class="description">
								<p><img src="resources/images/survwayLogo.png" width="100%">
								</p></div>
						</div></a>
				</div>
				</div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>

    $(function() {
    	$("#hr2").show();
    	let datasource = ${job};
    	//console.log(datasource);

    	
        $('#chart-container').orgchart({
            'data' : datasource,
            'visibleLevel': 3,
            'nodeContent': 'title',
			'draggable': true,
			'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
			if($draggedNode.find('.content').text().indexOf('manager') > -1 && $dropZone.find('.content').text().indexOf('engineer') > -1) {
				return false;
			}
				return true;
			}
		});
        
        $(".node").click(function(){
        	console.log($(this).attr("id"));
        });
        

		/* oc.$chart.on('nodedrop.orgchart', function(event, extraParams) {
			console.log('draggedNode:' + extraParams.draggedNode.children('.title').text()
						+ ', dragZone:' + extraParams.dragZone.children('.title').text()
						+ ', dropZone:' + extraParams.dropZone.children('.title').text());
		}); */


  });
  
	</script>
</body>
</html>