<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	.searchArea{
		height:80px;
		padding-top:10px;
	}
	.hru.board2 tbody>tr:not(.noeval):hover{
		cursor:pointer;
		background:#eee;
	}
	.year-info{
		float:right;
	}
</style>
</head>
<body>
<jsp:include page="../common/menubar.jsp"/>

<div class="outer"><!--outer Area -->
	<div class="contentArea"> <!-- 내용부분! -->
		<div class="hru segment">
		<h1 style="color:black">사원 관리</h1>
			<div class="searchArea">
				<form action="showSearchList.hr" method="POST">
				<table align="center" cellspacing="10px">
					<tr>
						<td><h3>검색어</h3></td>
						<td>
							<select class="ui dropdown" name="category">
								<option value="0">사원명</option>
								<option value="1">부서명</option>
								<option value="2">직급명</option>
							</select>
						</td>
						<td>
							<div class="ui input" style="width:400px"><input type="text" name="searchVal"></div>
						</td>
						<td>
							<button class="ui blue button" type="submit">검색하기</button>
						</td>
					</tr>
				</table>
				</form>
			</div><!-- searchArea end -->
		</div><!-- 검색영역 끝 -->
			
		<!-- 사원 테이블 -->
		<div class="hru segment">
			<table class="hru board2">
						<thead>
							<tr>
								<th>사원번호</th>
								<th>사원명</th>
								<th>직급</th>
								<th>사업장</th>
								<th>부서</th>
								<th>입사일자</th>
								<th>회사이메일</th>
							</tr>
						</thead>
						<tbody>
						<c:if test="${allist != null}">
							<c:forEach var="hm" items="${allist}">
								<tr onclick="selectMember(${hm.get('mid')})">
									<td><c:out value="${hm.get('mid')}"/></td>
									<td><c:out value="${hm.get('mName')}"/></td>
									<td><c:out value="${hm.get('ppName')}"/></td>
									<td><c:out value="${hm.get('groupName')}"/></td>
									<td><c:out value="${hm.get('teamName')}"/></td>
									<td><c:out value="${hm.get('enterDate')}"/></td>
									<td><c:out value="${hm.get('email')}"/></td>
								</tr>
							</c:forEach>
						</c:if>
						<c:if test="${allist == null}">
							<tr class="noeval">
								<td style="padding:100px" colspan="7">
									<h1 style="color:lightgray"><label><i class="exclamation circle icon"></i>검색 결과가 없습니다.</label></h1>
								</td>
							</tr>
						</c:if>
						</tbody>
					</table>
				<div class="pagingArea" id="gudok">
					<!-- <div class="paging" style="margin-right:15px;"><i class="angle double left icon"></i></div> -->
					<c:if test="${pi.currentPage <= 1}"><div class="paging-disabled" style="margin-right:15px;"><i class="angle double left icon" style="color:white;"></i></div></c:if>
					<c:if test="${pi.currentPage > 1}">
						<c:url var="pageCheck0" value="showMemberList.hr">
							<c:param name="currentPage" value="${pi.currentPage -1}"/>
						</c:url>
						<div class="paging" onclick="location.href='${pageCheck0}'" style="margin-right:15px;"><i class="angle double left icon"></i></div>
					</c:if>
					
					<c:forEach var="p" begin="${pi.startPage}" end="${pi.endPage}">
						<c:if test="${p eq pi.currentPage}"><div class="paging-disabled">${p}</div></c:if>
						<c:if test="${p ne pi.currentPage}">
							<c:url var="pageCheck" value="showMemberList.hr">
								<c:param name="currentPage" value="${ p }"/>
							</c:url>
							<div class="paging" onclick="location.href='${pageCheck}'">${p}</div></c:if>
					</c:forEach>
					
					<!-- <div class="paging" style="margin-left:15px;"><i class="angle double right icon"></i></div> -->
					<c:if test="${pi.currentPage >= pi.maxPage}"><div class="paging-disabled" style="margin-left:15px;"><i class="angle double right icon" style="color:white;"></i></div></c:if>
					<c:if test="${pi.currentPage < pi.maxPage}">
						<c:url var="pageCheck2" value="showMemberList.hr">
							<c:param name="currentPage" value="${pi.currentPage+1}"/>
						</c:url>
						<div class="paging" onclick="location.href='${pageCheck2}'" style="margin-left:15px;"><i class="angle double right icon"></i></div>
					</c:if>
				</div>
			</div><!-- 사원 테이블 끝 -->
		</div>
	</div>

	<script>
	$(function() {
		$('.ui.dropdown').dropdown();
	});
	
	function selectMember(fmid){
		$.ajax({
			url:"showMemberDetail.hr",
			type:"POST",
			data:{'fmid':fmid},
			success:function(data){
				var access = data.access;
				if(access == "Y"){
					location.href="memberInfo.hr?mid=" + fmid;
				}else{
					Swal.fire(
						'접근불가',
						'접근 권한이 없는 사원입니다.',
						'error'
					)
				}
			}
		});
	};
	</script>
</body>
</html>