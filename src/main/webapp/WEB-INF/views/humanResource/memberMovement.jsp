<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<table style="width:100%">
			<tr>
				<td>
					<h3><i class="check square icon"></i>발령작업 내용</h3>
				</td>
				<td style="text-align:right">
					<c:set var="now" value="<%=new java.util.Date()%>" />
					<c:set var="sysYear"><fmt:formatDate value="${now}" pattern="yyyy" /></c:set> 
					<select class="ui compact dropdown" onchange="selectYear();" id="yearSelect">
						<c:if test="${!empty yearlist}">
							<c:forEach var="y" begin="0" end="${yearlist.size()-1}">
								<c:if test="${sysYear eq yearlist.get(y)}">
									<option value="${yearlist.get(y)}" selected><c:out value="${yearlist.get(y)}년"/></option>								
								</c:if>
								<c:if test="${sysYear ne yearlist.get(y)}">
									<option value="${yearlist.get(y)}"><c:out value="${yearlist.get(y)}년"/></option>								
								</c:if>
							</c:forEach>
						</c:if>
						<c:if test="${empty yearlist}">
							<option value="${sysYear}" selected><c:out value="${sysYear}년"/></option>								
						</c:if>
					</select>
				</td>
			</tr>
		</table>
			<div class="hru segment">
				<div class="scrollArea" style="width: 100%; height:240px;">
					<table class="hru scroll table" id="movelistTable">
						<thead>
							<tr>
								<th>발령상태</th>
								<th>발령구분</th>
								<th>사번</th>
								<th>이름</th>
								<th>이동예정인 조직</th>
								<th>생성/수정 일자</th>
								<th>발령예정일자</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${!empty movlist}">
								<c:forEach var="i" begin="0" end="${movlist.size()-1}">
									<tr class="scroll-tr" onclick="showMemberTr(this, 1)">
										<td><input type="hidden" value="${movlist.get(i).movementId}">
										<c:if test="${movlist.get(i).approvalId==0}">입력중</c:if>
										<c:if test="${movlist.get(i).approvalId!=0}">
											<c:if test="${movlist.get(i).approvalYn eq 'N'}">결재 진행중</c:if>
											<c:if test="${movlist.get(i).approvalYn eq 'Y'}">결재 완료</c:if>
											<c:if test="${movlist.get(i).approvalYn eq 'C'}">결재 반려</c:if>
											<c:if test="${movlist.get(i).approvalYn eq 'O'}">발령 완료</c:if>
										</c:if>
										</td>
										<td><c:if test="${movlist.get(i).category==1}">입사</c:if>
											<c:if test="${movlist.get(i).category==2}">직무이동</c:if>
											<c:if test="${movlist.get(i).category==3}">부서이동</c:if>
											<c:if test="${movlist.get(i).category==4}">조직책임자 임명</c:if>
											<c:if test="${movlist.get(i).category==5}">진급</c:if>
										</td>
										<td><c:out value="${movlist.get(i).mid}"/></td>
										<td><c:out value="${movlist.get(i).mName}"/></td>
										<td><c:out value="${movlist.get(i).teamName}"/></td>
										<td><c:out value="${movlist.get(i).requestDate}"/></td>
										<td><c:out value="${movlist.get(i).moveDate}"/></td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
				<h3><i class="check square icon"></i>인사발령 이력</h3>
				<form action="insert.mov" method="post" id="insertMovementForm" enctype="multipart/form-data">
				<input type="hidden" value="0" name="movementId" id="movementId">
				<table class="hru table">
					<thead>
						<tr class="table-title">
							<th colspan="4" class="table-title">발령정보</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>*발령구분</th>
							<td>
								<select class="ui dropdown" name="category" id="category">
									<option value="">선택</option>
									<option value="1">입사</option>
									<option value="2">직무이동</option>
									<option value="3">부서이동</option>
									<option value="4">조직책임자 임명</option>
									<option value="5">진급</option>
								</select>
							</td>
							<th>*사번/이름</th>
							<td>
								<div class="ui icon input">
									<input type="text" id="search" name="mName"><input type="hidden" name="mid" id="mid">
									<i class="search icon"></i>
								</div>
							</td>
						</tr>
						<tr>
							<th>*발령일자</th>
							<td>
								<div class="ui calendar" id="start">
									<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="text" placeholder="시작일" readonly name="moveDate" id="moveDate">
									</div>
								</div>
							</td>
							<th>발령종료일자</th>
							<td>
								<div class="ui calendar" id="end">
									<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="text" placeholder="종료일" readonly>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				
				<div style="width:100%; height:327px; margin-top:50px">
					<div style="width:49%; margin-right:1%; float:left">
					<table class="hru table" id="beforeTable">
						<thead>
							<tr class="table-title">
								<th colspan="2" class="table-title">발령전</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>사업장/부서</th>
								<td>
									<div class="ui input disabled">
										<input type="text" placeholder="사업장" id="bgName">
									</div>
									<div class="ui input disabled">
										<input type="text" placeholder="부서" id="btName">
									</div>
								</td>
							</tr>
							<tr>
								<th>직군</th>
								<td>
									<div class="ui input disabled">
										<input type="text" placeholder="직군" id="bjName">
									</div>
								</td>
							</tr>
							<tr>
								<th>직급/직책</th>
								<td>
									<div class="ui input disabled">
										<input type="text" placeholder="직급" id="bppName">
									</div>
									<div class="ui input disabled">
										<input type="text" placeholder="직책" id="brName">
									</div>
								</td>
							</tr><tr>
								<th>시원유형</th>
								<td>
									<div class="ui input disabled">
										<input type="text" placeholder="정규직" id="beName">
									</div>
								</td>
							</tr>
							<tr>
								<th>개인정보활용동의</th>
								<td>
									<div class="ui input disabled">
										<input type="text" placeholder="Y" value="Y">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
					<div style="width:49%; margin-left:1%; float:right">
					<table class="hru table" id="afterTable">
						<thead>
							<tr class="table-title">
								<th colspan="2" class="table-title">발령후</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>사업장/부서</th>
								<td>
									<select class="ui dropdown" name="groupCode" id="groupSelect">
										 <option value="">사업장</option>
										  <c:forEach var="i" begin="0" end="${hi.getGlist().size()-1}">
										  	<option value="${hi.getGlist().get(i).get('groupCode')}"><c:out value="${hi.getGlist().get(i).get('groupName')}"/></option>
										  </c:forEach>
									</select>
									<select class="ui dropdown" name="teamCode" id="teamSelect">
										 <option value="">부서</option>
									</select>
								</td>
							</tr>
							<tr>
								<th>직군</th>
								<td>
									<select class="ui dropdown" name="jobCode" id="jobSelect">
										 <option value="">직군</option>
										  <c:forEach var="i" begin="0" end="${hi.getJlist().size()-1}">
										  	<option value="${hi.getJlist().get(i).get('jobCode')}"><c:out value="${hi.getJlist().get(i).get('jobName')}"/></option>
										  </c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th>직급/직책</th>
								<td>
									<select class="ui compact dropdown" id="ppSelect">
									 <option value="">직급</option>
									  <c:forEach var="i" begin="0" end="${hi.getPplist().size()-1}">
									  	<option value="${hi.getPplist().get(i).get('ppCode')}"><c:out value="${hi.getPplist().get(i).get('ppName')}"/></option>
									  </c:forEach>
									</select>
									<select class="ui dropdown" name="positionCode" id="positionSelect">
										<option value="">호봉</option>
									</select><label> / </label>
									<select class="ui compact dropdown" name="rCode" id="rSelect">
										 <option value="">직책</option>
										  <c:forEach var="i" begin="0" end="${hi.getRlist().size()-1}">
										  	<option value="${hi.getRlist().get(i).get('rCode')}"><c:out value="${hi.getRlist().get(i).get('rName')}"/></option>
										  </c:forEach>
									</select>
								</td>
							</tr><tr>
								<th>시원유형</th>
								<td>
									<select class="ui dropdown" name="enterCode" id="enterSelect">
									  <option value="">계약정보</option>
									  <c:forEach var="i" begin="0" end="${hi.getElist().size()-1}">
									  	<option value="${hi.getElist().get(i).get('enterCode')}"><c:out value="${hi.getElist().get(i).get('enterName')}"/></option>
									  </c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th>개인정보활용동의</th>
								<td>
									<select class="ui dropdown" id="lastSelect">
										<option value="">선택</option>
										<option value="0">Y</option>
										<option value="1">N</option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
				<table class="hru table" style="margin-top:50px">
						<thead>
							<tr class="table-title">
								<th colspan="2" class="table-title">발령사유</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="ui input" style="width:100%">
								    	<textarea style="width:100%; height:80px; resize:none;" name="reason" id="reason"></textarea>
								    </div>
								</td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" name="title" id="title2">
					<input type="hidden" name="firstApproval" id="firstApp2">
					<input type="hidden" name="secondApproval" id="secondApp2">
					<input type="hidden" name="thirdApproval" id="thirdApp2">
					<input type="file"id="attach" name="appFile" onchange="textchange();">
				</form>
				<div class="buttonArea">
					<button class="ui basic button" id="new">새로입력</button>
					<button class="ui gray button" id="store">저장</button>
					<button class="ui blue button" onclick="approvalFunction();" id="goApproval">전자결재</button>
				</div>
			</div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
		
	<div class="ui modal" id="searchMember">
		<i class="close icon"></i>
		<div class="header" align="center">
			<h2>사원 검색</h2>
		</div>
		<div class="searchArea" style="margin:20px; width:95%; height:100px; padding:20px;">
				<table align="center" cellspacing="10px">
					<tr>
						<td><h3>검색어</h3></td>
						<td><select class="ui dropdown" id="scategory">
							<option value="1">사원명</option>
							<option value="2">부서명</option>
						</select></td>
						<td>
							<div class="ui icon input">
								<input type="text" size="40" placeholder="Search..." id="stext"><i class="search icon"></i>
							</div>
						</td>
						<td>
							<button class="ui blue button" id="goSearch">검색하기</button>
						</td>
					</tr>
				</table>
		</div><!-- searchArea end -->
		<div class="hru segment" style="text-align:center;">
		<div class="scrollArea searchResult" style="width:98%; height:250px;">
			<table class="hru scroll table" id="resultTable">
				<thead>
					<tr>
						<th width="100px;">사번</th>
						<th>사원이름</th>
						<th>부서</th>
						<th>직급</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		</div>
	</div>
	<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
	
	<script>

		$(function(){
			$("#hr3").show();
			$(".ui.dropdown").dropdown();
			
			$('#start').calendar({
				type: 'date',
				formatter: {
				      date: function (date, settings) {
				        if (!date) return '';
				        var day = date.getDate();
				        var month = date.getMonth() + 1;
				        var year = date.getFullYear();
				        if(date.getMonth()>=10){
				        	if(date.getDate()>=10){
						        return year + '-' + month + '-' + day;
				        	} else{
						        return year + '-' + month + '-0' + day;
				        	}
				        } else{
				        	if(date.getDate()>=10){
					        	return year + '-0' + month + '-' + day;
				        	} else{
					        	return year + '-0' + month + '-0' + day;
				        	}
				        }
				      }
				}
			});
			$('#end').calendar({
				type: 'date',
				formatter: {
				      date: function (date, settings) {
				        if (!date) return '';
				        var day = date.getDate();
				        var month = date.getMonth() + 1;
				        var year = date.getFullYear();
				        return year + '-' + month + '-' + day;
				      }
				}
			});
			
			$("#new").click(function(){
				location.href="showMemberMovement.mov";
			});
			
			$("#store").click(function(){
				//빈칸검사!
				if($("select[name=teamCode]").val() == ""){
					Swal.fire({
						  icon: 'error',
						  title: '빈칸불가',
						  text: '필수사항을 다 기입해주세요.'
					});
					$("select[name=teamCode]").parent().addClass("error");
					return false;
				}
				if($("select[name=jobCode]").val() == ""){
					Swal.fire({
						  icon: 'error',
						  title: '빈칸불가',
						  text: '필수사항을 다 기입해주세요.'
					});
					$("select[name=jobCode]").parent().addClass("error");
					return false;
				}
				if($("select[name=positionCode]").val() == ""){
					Swal.fire({
						  icon: 'error',
						  title: '빈칸불가',
						  text: '필수사항을 다 기입해주세요.'
					});
					$("select[name=positionCode]").parent().addClass("error");
					return false;
				}
				if($("select[name=rCode]").val() == ""){
					Swal.fire({
						  icon: 'error',
						  title: '빈칸불가',
						  text: '필수사항을 다 기입해주세요.'
					});
					$("select[name=responsibilityCode]").parent().addClass("error");
					return false;
				}
				if($("select[name=enterCode]").val() == ""){
					Swal.fire({
						  icon: 'error',
						  title: '빈칸불가',
						  text: '필수사항을 다 기입해주세요.'
					});
					$("select[name=enterCode]").parent().addClass("error");
					return false;
				}
				
				$("#insertMovementForm").submit();
			});
			
			$("#groupSelect").change(function(){
				//console.log($(this).val());
				$("#teamSelect").html("<option value=''>부서</option>");
				$("#teamSelect").parent().find(".text").text("부서");
				
				
				<c:forEach var="i" begin="0" end="${hi.getTlist().size()-1}">
					
					var teamCode = "${hi.getTlist().get(i).get('teamCode')}";
					var teamName = "${hi.getTlist().get(i).get('teamName')}";
					var groupCode = "${hi.getTlist().get(i).get('groupCode')}";
					
					if(groupCode == $(this).val()){
						var $option = $("<option>").text(teamName).val(teamCode);
						$("#teamSelect").append($option);
					}
				</c:forEach>
				
			});
			
			$("#ppSelect").change(function(){
				//console.log($(this).val());
				$("#positionSelect").html("<option value=''>호봉</option>");
				$("#positionSelect").parent().find(".text").text("호봉");
				
				
				<c:forEach var="i" begin="0" end="${hi.getPlist().size()-1}">
					var positionCode = "${hi.getPlist().get(i).get('positionCode')}";
					var positionName = "${hi.getPlist().get(i).get('positionName')}";
					var prePositionCode = "${hi.getPlist().get(i).get('prePositionCode')}";
					
					if(prePositionCode == $(this).val()){
						var $option = $("<option>").text(positionName).val(positionCode);
						$("#positionSelect").append($option);
					}
				</c:forEach>
				
			});
			
			
			$("#search").click(function(){
				$("#searchMember").modal('show');
			});
			
			
			$("#goSearch").click(function(){
				var category = $("#scategory").val();
				var text = $("#stext").val();
				
				//console.log("category : " +category);
				//console.log("text : " +text);
				
				$.ajax({
					url: "search.me",
					type: "GET",
					data: {
						category:category,
						text:text
					},
					success:function(data){
						//console.log(data);
						
						$tbody = $("#resultTable tbody");
						$tbody.html('');
						
						for(var i=0; i<data.mlist.length; i++){

							var $td1 = $("<td>").text(data.mlist[i].mid);
							var $td2 = $("<td>").text(data.mlist[i].mName);
							var $td3 = $("<td>").text(data.mlist[i].tName);
							var $td4 = $("<td>").text(data.mlist[i].pName);
							
							$tbody.append($("<tr class='mclick'>").append($td1).append($td2).append($td3).append($td4));
							
							
							$(".mclick").click(function(){
								var mid = $(this).children().eq(0).text();
								var mName = $(this).children().eq(1).text();
								//console.log(mid + ", " + mName);
								
								$("#search").val(mName);
								$("#mid").val(mid);
								$("#searchMember").modal('hide');
								showMemberInfo(mid);
							});
						}
						
						
					},
					error:function(status){
						console.log(status);
					}
				});
			});
			
			
			
			$('#request').click(function(){
				//console.log("눌렸음!");
				
				if($("#title").val()==""){
					Swal.fire({
						  icon: 'error',
						  title: '빈칸불가',
						  text: '제목을 입력해주세요.'
					});
					$("#title").addClass("error");
					return false;
				}
				
				$("#title2").val($("#title").val());
				$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
				$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
				$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
				
				
				$('#insertMovementForm').submit();
				var content = "결재요청 : " + $("#title").val();
				var tmid = $("#firstApp").find("input[type=hidden]").val();
				if(tmid!="" && tmid!=null && tmid!=0){
					send(tmid, content);
				}
				
			});
		});
		
		//사원정보(현재)를 가져오는 함수
		function showMemberInfo(num){
			//console.log(num);
			
			$.ajax({
				url : "showMemberInfo.mov",
				type : "POST",
				data : {
					mid : num
				},
				success : function(data){
					//console.log(data);
					
					$("#bgName").val(data.mhi.gname);
					$("#btName").val(data.mhi.tname);
					$("#bjName").val(data.mhi.jname);
					$("#bppName").val(data.mhi.ppname);
					$("#brName").val(data.mhi.rname);
					$("#beName").val(data.mhi.ename);
					
				},
				error : function(status){
					console.log(status);
				}
			});
		}
		
		//전자결재 라인 보기를 위한 함수
		function approvalFunction(){
			var approvalCode = 3; //인사발령에 대한 결재코드
			//console.log("전자결재");
			
			//빈칸검사!
			if($("select[name=teamCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=teamCode]").parent().addClass("error");
				return false;
			}
			if($("select[name=jobCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=jobCode]").parent().addClass("error");
				return false;
			}
			if($("select[name=positionCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=positionCode]").parent().addClass("error");
				return false;
			}
			if($("select[name=rCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=responsibilityCode]").parent().addClass("error");
				return false;
			}
			if($("select[name=enterCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=enterCode]").parent().addClass("error");
				return false;
			}
			
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					
					$("#appType").val("인사발령");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thidName = data.appBasic.mlist[2].mName;
									var thidMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
				},
				error : function(status){
					console.log(status);
				}
			});
			
			
			return false;
		}
		
		function selectYear(){
			var year = $("#yearSelect").val();
			console.log(year);
			
			$.ajax({
				url : "searchYear.mov",
				type : "GET",
				data : {
					year : year
				},
				success : function(data){
					console.log(data);
					
					var $tbody = $("#movelistTable tbody");
					$tbody.html("");
					//console.log($tbody);
					
					var movlist = data.movlist;
					for(var i=0; i<movlist.length; i++){
						var $input = $("<input type=hidden>").val(movlist[i].movementId);
						
						var $td1 = "";
						switch(movlist[i].approvalYn){
							case "N" : $td1 = $("<td>").text("결재 진행중").append($input); break;
							case "Y" : $td1 = $("<td>").text("결재 완료").append($input); break;
							case "O" : $td1 = $("<td>").text("발령 완료").append($input); break;
							case "C" : $td1 = $("<td>").text("결재 반려").append($input); break;
						}
						if(movlist[i].approvalId==0){
							$td1.text("입력중").append($input);
						}
						var $td2 = "";
						switch(movlist[i].category){
							case 1 : $td2 = $("<td>").text("입사"); break;
							case 2 : $td2 = $("<td>").text("직무이동"); break;
							case 3 : $td2 = $("<td>").text("부서이동"); break;
							case 4 : $td2 = $("<td>").text("조직책임자 임명"); break;
							case 5 : $td2 = $("<td>").text("진급"); break;
						}
						var $td3 = $("<td>").text(movlist[i].mid);
						var $td4 = $("<td>").text(movlist[i].mName);
						var $td5 = $("<td>").text(movlist[i].teamName);
						var $td6 = $("<td>").text(movlist[i].requestDate);
						var $td7 = $("<td>").text(movlist[i].moveDate);
						
						$tbody.append($("<tr onclick='showMemberTr(this, 2)'>").append($td1).append($td2).append($td3).append($td4).append($td5).append($td6).append($td7));
					}
					
					
					
				},
				error : function(status){
					console.log(status);
				}
			});
		}
		
		function showMemberTr(num, category){
			if(category==1){
				var mid = num.childNodes[5].innerText;
			} else{
				var mid = num.childNodes[2].innerText;
			}
			
			var firstChild = num.firstChild;
			if(firstChild.nodeType==3){
				var firstChild2 = firstChild.nextSibling.firstChild;
				if(firstChild2.nodeType==3){
					var movementId = firstChild2.nextSibling.value;
				} else{
					var movementId = firstChild2.value;
				}
			} else{
				var firstChild2 = firstChild.firstChild;
				if(firstChild2.nodeType==3){
					var movementId = firstChild2.nextSibling.value;
				} else{
					var movementId = firstChild2.value;
				}
			}
			
			console.log("mid : " + mid + "movementId : " +movementId);

			$.ajax({
				url : "selectMemMovInfo.mov",
				type : "post",
				data : {
					mid : mid,
					movementId : movementId
				},
				success : function(data){
					console.log(data);
					
					var mhi = data.mMap.mhi;
					var mov = data.mMap.mov;
					
					$("#movementId").val(mov.movementId);
					$("#category").val(mov.category);
					$("#category").parent().dropdown();
					$("#mid").val(mov.mid)
					$("#search").val(mov.mName);
					$("#moveDate").val(mov.moveDate);
					
					if(mov.approvalId==0){
						$("#bgName").val(mhi.gname);
						$("#btName").val(mhi.tname);
						$("#bjName").val(mhi.jname);
						$("#bppName").val(mhi.ppname);
						$("#brName").val(mhi.rname);
						$("#beName").val(mhi.ename);
						
						//사업장 세팅
						$("#groupSelect").val(mov.groupCode);
						$("#groupSelect").parent().dropdown();
						//부서 세팅
						$("#teamSelect").html("<option value=''>부서</option>");
						$("#teamSelect").parent().find(".text").text("부서");
						
						
						<c:forEach var="i" begin="0" end="${hi.getTlist().size()-1}">
							
							var teamCode = "${hi.getTlist().get(i).get('teamCode')}";
							var teamName = "${hi.getTlist().get(i).get('teamName')}";
							var groupCode = "${hi.getTlist().get(i).get('groupCode')}";
							
							if(groupCode == mov.groupCode){
								var $option = $("<option>").text(teamName).val(teamCode);
								$("#teamSelect").append($option);
							}
						</c:forEach>
						$("#teamSelect").val(mov.teamCode);
						
						//직군
						$("#jobSelect").val(mov.jobCode);
						$("#jobSelect").parent().dropdown();
						
						//직위 세팅
						$("#ppSelect").val(mov.ppCode);
						$("#ppSelect").parent().dropdown();
						//직급 세팅
						$("#positionSelect").html("<option value=''>호봉</option>");
						$("#positionSelect").parent().find(".text").text("호봉");
						
						
						<c:forEach var="i" begin="0" end="${hi.getPlist().size()-1}">
							var positionCode = "${hi.getPlist().get(i).get('positionCode')}";
							var positionName = "${hi.getPlist().get(i).get('positionName')}";
							var prePositionCode = "${hi.getPlist().get(i).get('prePositionCode')}";
							
							if(prePositionCode == mov.ppCode){
								var $option = $("<option>").text(positionName).val(positionCode);
								$("#positionSelect").append($option);
							}
						</c:forEach>
						$("#positionSelect").val(mov.positionCode);
						
						
						//직책
						$("#rSelect").val(mov.rCode);
						$("#rSelect").parent().dropdown();
						
						//계약형태	
						$("#enterSelect").val(mov.enterCode);
						$("#enterSelect").parent().dropdown();
						
						$("#lastSelect").val(0);
						$("#lastSelect").parent().dropdown();
						
						$("#afterTable").css("visibility", "visible");
						$("#beforeTable").find("th[class=table-title]").text("발령전");
						
						$("#store").prop("disabled", false);
						$("#goApproval").prop("disabled", false);
						
					} else{ //전자결재가 이미 들어간 건에 대해서... (혹은 발령완료)
						$("#bgName").val(mhi.gname);
						$("#btName").val(mhi.tname);
						$("#bjName").val(mhi.jname);
						$("#bppName").val(mhi.ppname);
						$("#brName").val(mhi.rname);
						$("#beName").val(mhi.ename);
						
						$("#groupSelect").addClass("noselection");
						$("#groupSelect").parent().find("div[class=text]").addClass("default").text("사업장");
						
						
						$("#teamSelect").addClass("noselection");
						$("#teamSelect").parent().find("div[class=text]").addClass("default").text("부서");
						
						$("#jobSelect").addClass("noselection");
						$("#jobSelect").parent().find("div[class=text]").addClass("default").text("직군");

						$("#ppSelect").addClass("noselection");
						$("#ppSelect").parent().find("div[class=text]").addClass("default").text("호봉");
						$("#positionSelect").addClass("noselection");
						$("#positionSelect").parent().find("div[class=text]").addClass("default").text("호봉");

						$("#rSelect").addClass("noselection");
						$("#rSelect").parent().find("div[class=text]").addClass("default").text("직책");

						$("#enterSelect").addClass("noselection");
						$("#enterSelect").parent().find("div[class=text]").addClass("default").text("계약정보");

						$("#lastSelect").addClass("noselection");
						$("#lastSelect").parent().find("div[class=text]").addClass("default").text("선택");
						
						$("#afterTable").css("visibility", "hidden");
						console.log($("#afterTable").find("th[class=table-title]").text());
						$("#beforeTable").find("th[class=table-title]").text("발령내용");
						
						$("#store").prop("disabled", true);
						$("#goApproval").prop("disabled", true);
					}
					
					
					$("#reason").text(mov.reason);
				},
				error : function(status){
					console.log(status);
				}
			});
			
		}

	/* 	$(".memberTr").click(function(){
			var mid = $(this).children().eq(2).text();
			var movementId = $(this).children().find("input[type=hidden]").val();
			//console.log(mid + ", movementId : " + movementId);
			
			
			
		}); */
			
	</script>
</body>
</html>