<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
 	.title{
		 width:100%;
	}
/* 	.hru.board2 tbody>tr:hover{
		cursor:pointer;
		background:#eee;
	}
	.hru.board tbody>tr>th{
		text-align:center;
	} */
	.btnDiv{
		 width:12%;
		 height:489px;
	 	 float:left;
	 	 padding-top:160px;
	}
	.btnDiv>tr,.btnDiv>td{
		padding:10px;
	}
	.chartBtn{
		width:80px;	
	}
	.hori-segment{
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<table class="title">
			<tr>
				<td width="150px">
					<h1 style="">인사계획</h1>	
					<c:set var="now" value="<%=new java.util.Date()%>"/>
					<c:set var="sysYear"><fmt:formatDate value="${now}" pattern="yyyy"/></c:set>
				</td>
				<td>
					<!-- <select class="ui compact dropdown">
						<option value="0">2020년</option>
						<option value="1">2019년</option>
						<option value="2">2018년</option>
					</select> -->
				</td>
				<td style="text-align:right">
					<form action="approvalHRPlan.hr" method="post" id="approvalHRPlanForm" enctype="multipart/form-data">
						<c:if test="${empty hrplan.get('approvalId') || hrplan.get('approvalYn') == 'C'}"><button class="ui blue button" onclick="return approvalFunction();">전자결재</button></c:if>
						<c:if test="${!empty hrplan.get('approvalId') && hrplan.get('approvalYn') != 'C'}"><button class="ui blue button" disabled>전자결재</button></c:if>
						<input type="hidden" name="hrplanId" value="${hrplan.get('hrplanId')}">
						<input type="hidden" name="title" value="제목" id="title2">
						<input type="hidden" name="firstApproval" id="firstApp2">
						<input type="hidden" name="secondApproval" id="secondApp2">
						<input type="hidden" name="thirdApproval" id="thirdApp2">
						<input type="file"id="attach" name="appFile" onchange="textchange();">
					</form>
				</td>
			</tr>
		</table>
		<div class="ui divider"></div>
		<div class="hru segment">
			<div class="ui message">
				<ul class="list">
					<li>매년 내년에 대한 인사 계획을 세우실 수 있습니다.</li>
					<li>실제 적용되는 팀의 정보만 불러오므로, 신설 팀이 있을경우 시스템설정에서 팀을 추가하셔야 반영 가능합니다.</li>
					<li>실제 기획 정보가 시스템에 반영되는 것은 아니며, 인사 계획 및 의사결정을 위한 메뉴입니다.(과거 이력은 전자결재 내역에서 보실 수 있습니다.)</li>
				</ul>
			</div>
		</div>
		<div class="hru segment">
			<div class="hori-segment" style="width:32%">
				<table class="hru table">
					<thead>
						<tr class="table-title">
							<th colspan="3" class="table-title">조직현황</th>
						</tr>
						<tr>
							<th style="text-align:center; width:180px;">사업장/부서</th>
							<th style="text-align:center;">조직책임자</th>
							<th style="text-align:center;">인원수</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${!empty tlist}">
						<c:forEach var="i" begin="0" end="${tlist.size()-1}">
							<tr style="text-align:center">
								<td><c:out value="${tlist.get(i).get('gName')}"/>/<c:out value="${tlist.get(i).get('teamName')}"/></td>
								<td><c:out value="${tlist.get(i).get('lname')}"/></td>
								<td><c:out value="${tlist.get(i).get('count')}"/></td>
							</tr>
						</c:forEach>
						</c:if>
					</tbody>
				</table>
			</div>
			<div class="hori-segment" style="width:5%; padding-top:200px; text-align:center;">
				<i class="big angle double right icon"></i>
			</div>
			<div class="hori-segment" style="width:62%">
				<table class="hru table">
						<thead>
							<tr class="table-title">
								<th colspan="6" class="table-title">조직개편 계획</th>
							</tr>
							<tr>
								<th style="text-align:center; width:180px;">사업장/부서</th>
								<th style="text-align:center;">조직책임자</th>
								<th style="text-align:center;">인원수</th>
								<th style="text-align:center;">충원예정</th>
								<th style="text-align:center;">방출예정</th>
								<th style="text-align:center;">비고</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${!empty tplist}">
							<c:forEach var="j" begin="0" end="${tplist.size()-1}">
							<tr style="text-align:center">
								<td><c:out value="${tplist.get(j).gName}"/>/<c:out value="${tplist.get(j).teamName}"/></td>
								<td><c:out value="${tplist.get(j).lname}"/></td>
								<td><c:out value="${tplist.get(j).peopleNum}"/></td>
								<td><c:out value="${tplist.get(j).addNum}"/></td>
								<td><c:out value="${tplist.get(j).outNum}"/></td>
								<td><c:out value="${tplist.get(j).detail}"/></td>
							</tr>
							</c:forEach>
							</c:if>
						</tbody>
				</table>
			</div>
		</div>
		<div class="ui divider"></div>
		<div class="hru segment">
			<div class="hori-segment" style="width:44%;">
				<h3><i class="check square icon"></i><c:out value="${sysYear}"/>년 조직도</h3>
				<div class="scrollArea" style="width: 100%; height:300px;">
				<table class="hru scroll table">
					<thead>
						<tr>
							<th>사업장</th>
							<th>부서</th>
							<th>조직책임자</th>
							<th>팀원수</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${!empty tlist}">
						<c:forEach var="i" begin="0" end="${tlist.size()-1}">
						<tr class="beforeTr" style="text-align:center" onclick="clickTr(this);">
							<td><input type="hidden" value="${tlist.get(i).get('teamCode')}"><c:out value="${tlist.get(i).get('gName')}"/></td>
							<td><c:out value="${tlist.get(i).get('teamName')}"/></td>
							<td><input type="hidden" value="${tlist.get(i).get('lmid')}"><c:out value="${tlist.get(i).get('lname')}"/></td>
							<td><c:out value="${tlist.get(i).get('count')}"/></td>
						</tr>
						</c:forEach>
						</c:if>
					</tbody>
				</table>
				</div>
			</div>
			<div class="hori-segment" style="width:10%; text-align:center; padding-top:120px;">
				<c:if test="${empty hrplan.get('approvalId') || hrplan.get('approvalYn') == 'C'}">
				<table style="width: 100%; text-align: center">
					<tr>
						<td><button class="chartBtn ui button mini" onclick="addAll();">전체 추가</button></td>
					</tr>
					<tr>
						<td><button class="chartBtn ui button mini" onclick="minusAll();">전체 삭제</button></td>
					</tr>
					<tr>
						<td><button class="chartBtn ui button mini" onclick="addOne();">>></button></td>
					</tr>
					<tr>
						<td><button class="chartBtn ui button mini" onclick="minusOne()"><<</button></td>
					</tr>
				</table>
				</c:if>
			</div>
			<div class="hori-segment" style="width:44%;">
				<h3><i class="check square icon"></i><c:out value="${sysYear+1}"/>년 조직도</h3>
				<div class="scrollArea" style="width: 100%; height:300px;">
				<table class="hru scroll table">
					<thead>
						<tr>
							<th style="width:120px;">사업장</th>
							<th style="width:180px;">부서</th>
							<th>조직책임자</th>
							<th>팀원수</th>
						</tr>
					</thead>
					<tbody id="afterTbody">
						<c:if test="${!empty tplist}">
						<c:forEach var="j" begin="0" end="${tplist.size()-1}">
						<tr class="afterTr" style="text-align:center" onclick="clickTr2(this)">
							<td><input class="tcode" type="hidden" value="${tplist.get(j).teamCode}"><c:out value="${tplist.get(j).gName}"/></td>
							<td><input class="pdetail" type="hidden" value="${tplist.get(j).pdetailId}"><c:out value="${tplist.get(j).teamName}"/></td>
							<td><input class="mid" type="hidden" value="${tplist.get(j).mid}"><c:out value="${tplist.get(j).lname}"/></td>
							<td><c:out value="${tplist.get(j).peopleNum}"/></td>
						</tr>
						</c:forEach>
						</c:if>
					</tbody>
				</table>
				</div>
			</div>
		</div>
		<div class="hru segment" style="padding-right:20px;">
			<div class="buttonArea" style="margin-bottom:10px;">
				<c:if test="${empty hrplan.get('approvalId') || hrplan.get('approvalYn') == 'C'}">
					<button class="ui blue button mini" id="modify">저장하기</button>
					<button class="ui gray button mini" id="delete">삭제하기</button>
				</c:if>
			</div>
				<form action="updateHRPlan.hr" method="post" id="updateHRPlanForm">
				<input type="hidden" name="hrplanId" value="${hrplan.get('hrplanId')}">
				<table class="hru table" style="width:50%; float:right; margin-bottom:80px;">
					<thead>
						<tr  class="table-title">
							<th colspan="3" class="table-title">수 정</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>사업장/부서</th>
							<td>
								<div class="ui input"><input type="text" name="gName" id="gName" readonly></div>
								<input type="hidden" name="pdetailId" id="pdetailId">
							</td>
							<td>
								<div class="ui input"><input type="text" name="teamName" id="teamName" readonly></div>
								<input type="hidden" name="teamCode" id="teamCode">
							</td>
						</tr>
						<tr>
							<th>조직책임자</th>
							<td colspan="2">
								<div class="ui input" style="margin-right:20px;"><input type="text" name="lname" id="lname"></div>
								<input type="hidden" name="mid" id="mid">
								<input type="button" class="ui blue button mini" value="사원검색" id="search">
							</td>
						</tr>
						<tr>
							<th>팀원수</th>
							<td>
								(기존)&nbsp;<div class="ui input" style="width:70px"><input type="number" name="currentNum" id="currentNum" readonly></div>&nbsp;명
							</td>
							<td>
								(변경)&nbsp;<div class="ui input" style="width:70px"><input type="number" name="peopleNum" id="peopleNum" readonly></div>&nbsp;명
							</td>
						</tr>
						<tr>
							<th>변경인원</th>
							<td>
								(충원)&nbsp;<div class="ui input" style="width:70px"><input type="number" name="addNum" id="addNum" min="0"></div>&nbsp;명
							</td>
							<td>
								(방출)&nbsp;<div class="ui input" style="width:70px"><input type="number" name="outNum" id="outNum" min="0"></div>&nbsp;명
							</td>
						</tr>
						<tr>
							<th>비고</th>
							<td colspan="2"><div class="ui input"><input type="text" size="70" id="detail" name="detail"></div></td>
						</tr>
					</tbody>
				</table>
				</form>
		</div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<div class="ui modal" id="searchMember">
		<i class="close icon"></i>
		<div class="header" align="center">
			<h2>사원 검색</h2>
		</div>
		<div class="searchArea" style="margin:20px; width:95%; height:100px; padding:20px;">
				<table align="center" cellspacing="10px">
					<tr>
						<td><h3>검색어</h3></td>
						<td><select class="ui dropdown" id="scategory">
							<option value="1">사원명</option>
							<option value="2">부서명</option>
						</select></td>
						<td>
							<div class="ui icon input">
								<input type="text" size="40" placeholder="Search..." id="stext"><i class="search icon"></i>
							</div>
						</td>
						<td>
							<button class="ui blue button" id="goSearch">검색하기</button>
						</td>
					</tr>
				</table>
		</div><!-- searchArea end -->
		<div class="hru segment" style="text-align:center;">
		<div class="scrollArea searchResult" style="width:98%; height:250px;">
			<table class="hru scroll table" id="resultTable">
				<thead>
					<tr>
						<th width="100px;">사번</th>
						<th>사원이름</th>
						<th>부서</th>
						<th>직급</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		</div>
	</div>
	<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
	<script>
		var selectedTeam = new Array(6);
		var selectedTeamId;
		$(function(){
			
			$("#hr3").show();
			$(".ui.dropdown").dropdown();
			
			$("#modify").click(function(){
				$("#updateHRPlanForm").submit();
			});
			$("#delete").click(function(){
				var num = $("#pdetailId").val();
				if(num>0){
					location.href="deleteHRPlan.hr?num=" + num;										
				} else{
					alert("저장되지 않은 정보입니다.");
				}
			});
			
			$("#addNum").on("propertychange change keyup paste input", function(){
	        	var add = Number($("#addNum").val());
	        	var out = Number($("#outNum").val());
	        	var curr = Number($("#currentNum").val());
	        	$("#peopleNum").val(curr+add-out);
	        });
			$("#outNum").on("propertychange change keyup paste input", function(){
	        	var add = Number($("#addNum").val());
	        	var out = Number($("#outNum").val());
	        	var curr = Number($("#currentNum").val());
	        	$("#peopleNum").val(curr+add-out);
	        });
			
			$("#search").click(function(){
				$("#searchMember").modal('show');
			});
			
			$("#goSearch").click(function(){
				var category = $("#scategory").val();
				var text = $("#stext").val();
				
				//console.log("category : " +category);
				//console.log("text : " +text);
				
				$.ajax({
					url: "search.me",
					type: "GET",
					data: {
						category:category,
						text:text
					},
					success:function(data){
						console.log(data);
						
						$tbody = $("#resultTable tbody");
						$tbody.html('');
						
						for(var i=0; i<data.mlist.length; i++){

							var $td1 = $("<td>").text(data.mlist[i].mid);
							var $td2 = $("<td>").text(data.mlist[i].mName);
							var $td3 = $("<td>").text(data.mlist[i].tName);
							var $td4 = $("<td>").text(data.mlist[i].pName);
							
							$tbody.append($("<tr class='mclick'>").append($td1).append($td2).append($td3).append($td4));
							
							
							$(".mclick").click(function(){
								var mid = $(this).children().eq(0).text();
								var mName = $(this).children().eq(1).text();
								//console.log(mid + ", " + mName);
								
								$("#lname").val(mName);
								$("#mid").val(mid);
								$("#searchMember").modal('hide');
							});
						}
						
						
					},
					error:function(status){
						console.log(status);
					}
				});
			});
			

			$('#request').click(function(){
				$("#approvalModal").modal('hide');

				//console.log("눌렸음!");
				$("#title2").val($("#title").val());
				$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
				$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
				$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
					
				$('#approvalHRPlanForm').submit();
				var content = "결재요청 : " + $("#title").val();
				var tmid = $("#firstApp").find("input[type=hidden]").val();
				if(tmid!="" && tmid!=null && tmid!=0){
					send(tmid, content);
				}
			});
		});
		
		function clickTr(num){
			//console.log(num.parentNode.childNodes);
			$(".beforeTr").css("background", "");
			num.style.background='#eee';
			selectedTeam[0] = num.childNodes[1].firstChild.value; //팀코드
			selectedTeam[1] = num.childNodes[1].innerText; //사업장명
			selectedTeam[2] = num.childNodes[3].innerText; //팀명
			selectedTeam[3] = num.childNodes[5].innerText; //조직책임자
			selectedTeam[4] = num.childNodes[7].innerText; //인원수
			selectedTeam[5] = num.childNodes[5].firstChild.value; //조직책임자 MID
			console.log(selectedTeam);
		}
		
		function clickTr2(num){
			$(".afterTr").css("background", "");
			num.style.background='#eee';
			//console.log(num.childNodes[1]);
			//console.log(num.childNodes[3]);
			if(num.childNodes[0].nodeType==3){
				selectedTeamId = num.childNodes[0].nextSibling.firstChild.value;
			} else{
				selectedTeamId = num.childNodes[0].firstChild.value;
			}
			
			var selectedId = 0;
			if(num.childNodes[3].firstChild.nodeType==3){ //추가된거ㅠ
				selectedId = num.childNodes[1].firstChild.value;
			} else{ //원래 있던거
				selectedId = num.childNodes[3].firstChild.value;
			}
			console.log(selectedId);
			if(selectedId!=0){
				$.ajax({
					url : "selectDetailPlan.hr",
					type : "POST",
					data : {
						pdetailId : selectedId
					}, 
					success : function(data){
						console.log(data.tpd);
						var team = data.tpd;
						
						
						$("#gName").val(team.gName);
						$("#teamName").val(team.teamName);
						$("#teamCode").val(team.teamCode);
						$("#lname").val(team.lname);
						$("#mid").val(team.mid);
						$("#currentNum").val(team.currentNum);
						$("#addNum").val(team.addNum);
						$("#outNum").val(team.outNum);
						var peopleNum = team.currentNum + team.addNum - team.outNum;
						$("#peopleNum").val(peopleNum);
						$("#pdetailId").val(team.pdetailId);
						$("#detail").val(team.detail);
						
					},
					error : function(status){
						console.log(status);
					}
				});
			} else{
				//console.log(num.childNodes[2]);
				$("#pdetailId").val(0);
				$("#gName").val(num.childNodes[0].innerText);
				$("#teamName").val(num.childNodes[1].innerText);
				$("#teamCode").val(num.childNodes[0].firstChild.value);
				$("#lname").val(num.childNodes[2].innerText);
				$("#mid").val(num.childNodes[2].firstChild.value);
				$("#currentNum").val(num.childNodes[3].innerText);
				$("#addNum").val(0);
				$("#outNum").val(0);
				$("#peopleNum").val(num.childNodes[3].innerText);
				$("#detail").val("");

			}
			
		}
		
		function addOne(){
			var count = 0;
			var inputs = $(".afterTr").find(".tcode");
			//console.log(inputs[0]);
			for(var i=0; i<inputs.length; i++){
				if(inputs[i].value==selectedTeam[0]){
					//alert("있다!");
					break;
				} else if(selectedTeam[0]==null){
					//alert("빈칸!");
					break;
				} else{
					count++;
				}
			}
			
			if(count==inputs.length){
				$.ajax({
					url : "insertOneTeam.hr",
					type : "POST",
					data : {
						teamCode : selectedTeam[0],
						lmid : selectedTeam[5],
						peopleNum : selectedTeam[4]
					},
					success : function(data){
						//console.log(data);
						
						$afterTbody = $("#afterTbody");
						$input1 = $("<input type='hidden' class='tcode'>").val(selectedTeam[0]);
						//console.log($input1);
						$td1 = $("<td>").append($input1).append(selectedTeam[1]);
						$input2 = $("<input type='hidden' class='pdetail'>").val(data.pdetailId);
						$td2 = $("<td>").append($input2).append(selectedTeam[2]);
						$input3 = $("<input type='hidden' class='mid'>").val(selectedTeam[5]);
						$td3 = $("<td>").append($input3).append(selectedTeam[3]);
						$td4 = $("<td>").text(selectedTeam[4]);
						
						
						$afterTbody.append($("<tr class='afterTr' onclick='clickTr2(this)'>").append($td1).append($td2).append($td3).append($td4));
						
					},
					error : function(status){
						console.log(status);
					}
				});
				
			}
		}
		
		function minusOne(){
			//console.log(selectedTeamId);
			var inputs = $(".afterTr").find(".tcode");
			for(var i=0; i<inputs.length; i++){
				if(inputs[i].value==selectedTeamId){
					inputs[i].parentNode.parentNode.remove();
					$.ajax({
						url : "deleteOneTeam.hr",
						type : "POST",
						data : {
							teamCode : selectedTeamId
						},
						success : function(data){
							//console.log(data);

							$("#gName").val("");
							$("#teamName").val("");
							$("#teamCode").val("");
							$("#lname").val("");
							$("#mid").val("");
							$("#currentNum").val("");
						},
						error : function(status){
							console.log(status);
						}
					});
				}
			}
		}
		
		function addAll(){
			$teamInfo = $(".beforeTr");
			var selectArr = new Array();
			for(var i=0; i<$teamInfo.length; i++){
				var $someTeam = $teamInfo.eq(i);
				//console.log($someTeam.html());
				selectedTeam[0] = $someTeam.find("input[type=hidden]").val(); //팀코드
				selectedTeam[1] = $someTeam.children().eq(0).text(); //사업장명
				selectedTeam[2] = $someTeam.children().eq(1).text(); //팀명
				selectedTeam[3] = $someTeam.children().eq(2).text(); //조직책임자
				selectedTeam[4] = $someTeam.children().eq(3).text(); //인원수
				selectedTeam[5] = $someTeam.children().eq(2).find("input[type=hidden]").val(); //조직책임자 MID
				//addOne();
				var count = 0;
				var inputs = $(".afterTr").find(".tcode");
				
				for(var j=0; j<inputs.length; j++){
					if(inputs[j].value==selectedTeam[0]){ //이미있음
						break;
					} else if(selectedTeam[0]==null){ //빈칸임
						break;
					} else{
						count++;
					}
				}
				//console.log(selectedTeam);
				if(count==inputs.length){
					//console.log("선택된 팀");
					//console.log(selectedTeam);					
					selectArr.push(selectedTeam);
					selectedTeam = new Array(6);
				}
			}
			//console.log(JSON.stringify(selectArr));
			
			
			$.ajax({
				url : "insertAllTeamPlan.hr",
				type : "POST",
				data : {
					jsonData : JSON.stringify(selectArr)
				},
				success : function(data){
					//console.log(data);
					
					$afterTbody = $("#afterTbody");
					
					var pdlist = data.idlist;
					var j = 0;
					for(var i=0;i<selectArr.length; i++){
						var $input1 = $("<input type='hidden' class='tcode'>").val(selectArr[i][0]);
						//console.log($input1);
						var $td1 = $("<td>").append($input1).append(selectArr[i][1]);
						var $input2 = $("<input type='hidden' class='pdetail'>").val(pdlist[j++]);
						var $td2 = $("<td>").append($input2).append(selectArr[i][2]);
						var $input3 = $("<input type='hidden' class='mid'>").val(selectArr[i][5]);
						var $td3 = $("<td>").append($input3).append(selectArr[i][3]);
						var $td4 = $("<td>").text(selectArr[i][4]);
						
						
						$afterTbody.append($("<tr class='afterTr' onclick='clickTr2(this)'>").append($td1).append($td2).append($td3).append($td4));
					}
					
				},
				error : function(status){
					console.log(status);
				}
			});
		}
		
		function minusAll(){
			$teamInfo = $(".afterTr");
			var idArr = new Array();
			for(var i=0; i<$teamInfo.length; i++){
				var $someTeam = $teamInfo.eq(i);
				//console.log($someTeam.html());
				selectedTeamId = $someTeam.find("input[type=hidden]").val(); //팀코드
				//minusOne();
				idArr.push(selectedTeamId);
				selectedTeamId = "";
			}
			
			$.ajax({
				url : "deleteAllTeamPlan.hr",
				type : "POST",
				data : {
					jsonData : JSON.stringify(idArr)
				},
				success:function(data){
					console.log(data);
					
					var inputs = $(".afterTr").find(".tcode");
					for(var j=0; j<idArr.length; j++){
						for(var i=0; i<inputs.length; i++){
							if(inputs[i].value==idArr[j]){
								inputs[i].parentNode.parentNode.remove();
							}
						}
					}
					
					
				},
				error:function(status){
					console.log(status);
				}
			});
		}

		function approvalFunction(){
			var approvalCode = 2; //신규사원등록에 대한 결재코드
			//console.log("전자결재");
			
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					
					$("#appType").val("인사기획");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thirdName = data.appBasic.mlist[2].mName;
									var thirdMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
				},
				error : function(status){
					console.log(status);
				}
			});
			
			
			return false;
		}
		
	</script>
</body>
</html>









