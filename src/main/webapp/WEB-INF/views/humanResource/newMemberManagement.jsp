<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	.searchArea{
		height:80px;
		padding-top:10px;
	}
	.year-info{
		float:right;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	<div class="outer"><!--outer Area -->
		<div class="contentArea"> <!-- 내용부분! -->
			<h1>신규사원 관리</h1>
			
			<!-- 추가 설명부분 -->
			<div class="hru segment oneDay">
				<div class="ui message">
					<ul class="list">
						<li>신규 사원 등록시 4대보험 취득신고를 14일 이내, 급여통장 등록은 해당 월의 말일까지 등록이 완료되어야 합니다. </li>
						<li>4대보험 취득신고를 기한내에 못할경우 과태료가 부가됩니다.</li>
						<li>급여통장 미등록시 해당사원의 급여가 미지급됩니다.</li>
					</ul>
				</div>
			</div><!-- 추가 설명부분 끝! -->
				
			<div class="hru segment">
					<div class="hru segment">
					<table class="hru board2" border="1">
						<thead>
							<tr>
								<th style="width:80px;">사번</th>
								<th>사원명</th>
								<th style="width:150px;">직급/직책</th>
								<th style="width:200px;">입사일자</th>
								<th colspan="2" style="width:180px;" >4대보험 취득신고 (마감기한)</th>
								<th colspan="2" style="width:180px;" >급여통장 등록 (마감기한)</th>
								<th>비고</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${!empty mlist}">
								<c:forEach var="m" begin="0" end="${mlist.size()-1}">
									<tr>
										<td><c:out value="${mlist.get(m).mid}"/></td>
										<td><c:out value="${mlist.get(m).mName}"/></td>
										<td><c:out value="${mlist.get(m).positionName}"/><c:if test="${mlist.get(m).rCode eq 2}"> (팀장)</c:if></td>
										<td><c:out value="${mlist.get(m).enterDate}"/></td>
										<c:if test="${mlist.get(m).insuranceYn eq 'N' }">
										<td><div class='ui checkbox'><input type="checkbox" class='insuranceYn' name="insuranceYn"><label></label></div></td>
										<td style="width:150px;"><c:out value="(${mlist.get(m).insDate})"/></td></c:if>
										<c:if test="${mlist.get(m).insuranceYn ne 'N' }"><td><div class='ui checked checkbox'><input class='insuranceYn' type="checkbox" name="insuranceYn" checked=""><label></label></div><td></td></c:if>
										<c:if test="${empty mlist.get(m).accountNumber}">
											<td><button class="ui blue button mini bank">등록하기</button></td>
											<td style="width:150px;"><c:out value="(${mlist.get(m).bankDate})"/></td></c:if>
											<c:if test="${!empty mlist.get(m).accountNumber}">
											<td><button class="ui button mini" disabled>등록완료</button></td><td></td>
											</c:if>
										<td>
										<c:if test="${mlist.get(m).insuranceYn eq 'Y' && !empty mlist.get(m).accountNumber}">
											<c:if test="${mlist.get(m).tCode eq 2}"><button class="ui button mini" id="authHR">권한(인사팀)</button></c:if>
											<c:if test="${mlist.get(m).rCode eq 2}"><button class="ui button mini" id="authLeader">권한(조직책임자)</button></c:if>
											<c:if test="${mlist.get(m).rCode eq 2 && mlist.get(m).tCode eq 2}"><button class="ui button mini" id="authHRL">권한(인사팀장)</button></c:if>									
										</c:if></td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
			</div> <!-- 리스트 끝 -->
		</div>
	</div>
	<div class="ui modal" id="bankModal">
		<i class="close icon"></i>
		<div class="header" align="center">
			<h3>급여통장 등록</h3>
		</div>
		<div class="hru segment" style="margin-bottom:5px;">
			<form action="updateBankInfo.me" method="POST">
			<table align="center">
				<tr>
					<td><label class="hru label">은행명</label></td>
					<td><select class="ui dropdown" name="bankNum">
						<option value="">은행선택</option>
						<option value="0">신한은행</option>
						<option value="1">우리은행</option>
						<option value="2">국민은행</option>
						<option value="3">하나은행</option>
						<option value="4">카카오뱅크</option></select></td>
					<td><label class="hru label">통장번호</label></td>
					<td><div class="ui input"><input type="text" size="40" name="accountNumber"></div></td>
				</tr>
			</table>
			<input type="hidden" id="modalMid" name="mid">
			<div class="buttonArea"><button class="ui blue button mini" id="enroll">등록</button>
					<button type="button" class="ui button mini" id="cancel">취소</button></div>
			</form>
		</div>
	</div>

	<script>
		$(function() {
			$("#hr1").show();
			$(".ui.dropdown").dropdown();
			
			$(".insurance").click(function(){
				var mid = $(this).parent().parent().children().eq(0).text();
				//console.log("취득 신고하기! mid : " +mid);
				
				location.href="updateInsurance.me?mid=" + mid;
			});
			$(".insuranceYn").click(function(){
				var mid = $(this).parent().parent().parent().children().eq(0).text();
				var insYn = $(this).is(":checked");
				console.log("취득 신고하기 " + insYn);
				location.href="updateInsurance.me?mid=" + mid + "&insYn=" + insYn;
			});
			
			$(".bank").click(function(){
				var mid = $(this).parent().parent().children().eq(0).text();
				//console.log("은행 등록하기! mid : " +mid);
				$("#modalMid").val(mid);
				
				$("#bankModal").modal('show');
			});
			
			$("#cancel").click(function(){
				$("#bankModal").modal('hide');
			});
			
			$("#authHR").click(function(){
				var mid = $(this).parent().parent().children().eq(0).text();
				
				location.href="insertAuthorityMember.au?type=2&mid=" + mid;
			});

			$("#authLeader").click(function(){
				var mid = $(this).parent().parent().children().eq(0).text();
				
				location.href="insertAuthorityMember.au?type=4&mid=" + mid;
			});

			$("#authHRL").click(function(){
				var mid = $(this).parent().parent().children().eq(0).text();
				
				location.href="insertAuthorityMember.au?type=1&mid=" + mid;
			});
		});
		
		
	</script>
</body>
</html>