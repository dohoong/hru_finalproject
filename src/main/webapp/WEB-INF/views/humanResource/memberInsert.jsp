<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	.calendar tr{
		height:30px;
	}
	.step .title:hover{
		cursor:pointer;
		/* color:orangered; */
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	<div class="outer"><!--outer Area -->
		<div class="contentArea"> <!-- 내용부분! -->
		<form action="insert.me" method="post" id="memberInsertForm" enctype="multipart/form-data">
		<h1>신규사원 등록</h1>
		<div class="ui divider"></div>
		<div class="hru segment">
				<h3><i class="check square icon"></i>기본정보</h3>
				<table class="hru table3">
					<tr>
						<!-- <th>*사원번호</th>
						<td><div class="ui input" style="width:200px"><input type="text" maxlength="10" readonly></div></td> -->
						<th>*성명</th>
						<td colspan="3"><div class="ui input" style="width:150px"><input type="text" maxlength="6" name="tempName"></div></td>
					</tr>
					<tr>
						<th>*주민등록번호</th>
						<td><div class="ui input" style="width:90px"><input type="text" maxlength="6" name="tempSsn"><div class="bar">-</div><input type="text" maxlength="7" name="tempSsn2"></div></td>
						<th>*회사이메일</th>
						<td><div class="ui input" style="width:250px"><input type="email" name="tempEmail"></div></td>
					</tr>
					<tr>
						<th>*내외국인</th>
						<td>
							<div class="ui radio checkbox">
								<input type="radio" name="tempKorean" id="local" checked="checked" value="Y">
								<label for="local">내국인</label>
							</div>
							<div class="ui radio checkbox" style="margin-left:20px">
								<input type="radio" name="tempKorean" id="foreigner" value="N">
								<label for="foreigner">외국인</label>
							</div>
						</td>
						<th>*국적</th>
						<td><div class="ui input">
							  <input type="text" name="tempCountry">
							</div></td>
					</tr>
					<tr>
						<th>*입사일</th>
						<td>
						<div class="ui calendar" id="refoundDay">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="입사일" readonly name="startDate">
							</div>
						</div>
						</td>
						<th> 퇴사일</th>
						<td>
						<div class="ui calendar disabled" id="endDate">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="퇴사일" name="">
							</div>
						</div>
						</td>
					</tr>
					<!-- <tr>
						<th>*수습여부</th>
						<td colspan="3"><div class="ui checkbox">
						  <input type="checkbox" name="example">
						  <label>수습</label>
						</div></td>
					</tr> -->
				</table>
				
				<h3><i class="check square icon"></i>인사정보</h3>
				<table class="hru table3">
					<tr>
						<th>*계약정보</th>
						<td>
								<select class="ui dropdown" name="enterCode">
								  <option value="">계약정보</option>
								  <c:forEach var="i" begin="0" end="${hi.getElist().size()-1}">
								  	<option value="${hi.getElist().get(i).get('enterCode')}"><c:out value="${hi.getElist().get(i).get('enterName')}"/></option>
								  </c:forEach>
								</select>
						</td>
						<th>*사업장 / 부서</th>
						<td>
							<select class="ui dropdown" name="" id="groupSelect">
								 <option value="">사업장</option>
								  <c:forEach var="i" begin="0" end="${hi.getGlist().size()-1}">
								  	<option value="${hi.getGlist().get(i).get('groupCode')}"><c:out value="${hi.getGlist().get(i).get('groupName')}"/></option>
								  </c:forEach>
							</select>
							<select class="ui dropdown" name="teamCode" id="teamSelect">
								 <option value="">부서</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>*입사 직급</th>
						<td>
							<select class="ui dropdown" id="ppSelect">
								 <option value="">입사직급</option>
								  <c:forEach var="i" begin="0" end="${hi.getPplist().size()-1}">
								  	<option value="${hi.getPplist().get(i).get('ppCode')}"><c:out value="${hi.getPplist().get(i).get('ppName')}"/></option>
								  </c:forEach>
							</select>
							<select class="ui dropdown" name="positionCode" id="positionSelect">
								<option value="">호봉</option>
							</select>
						</td>
						<th>*입사 직종</th>
						<td>
							<select class="ui dropdown" name="jobCode">
								 <option value="">입사직종</option>
								  <c:forEach var="i" begin="0" end="${hi.getJlist().size()-1}">
								  	<option value="${hi.getJlist().get(i).get('jobCode')}"><c:out value="${hi.getJlist().get(i).get('jobName')}"/></option>
								  </c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>직책</th>
						<td>
							<select class="ui dropdown" name="responsibilityCode">
								 <!-- <option value="">직책</option> -->
								  <c:forEach var="i" begin="0" end="${hi.getRlist().size()-1}">
								  	<option value="${hi.getRlist().get(i).get('rCode')}"><c:out value="${hi.getRlist().get(i).get('rName')}"/></option>
								  </c:forEach>
							</select>
						</td>
						<th>*급여형태</th>
						<td>
							<select class="ui dropdown" name="salaryType">
								 <option value="">급여형태</option>
								 <option value="1">연봉</option>
								 <option value="2">월급</option>
								 <option value="3">시급</option>
							</select>
							<div class="ui left icon input"><i class="won sign icon"></i><input type="text" name="salaryAmount"></div>
						</td>
					</tr>
					<tr>
						<th>급여지급 계좌</th>
						<td colspan="3">
							<select class="ui dropdown">
								 <option value="">은행선택</option>
								 <option value="0">신한은행</option>
								 <option value="1">우리은행</option>
								 <option value="2">국민은행</option>
								 <option value="3">하나은행</option>
								 <option value="4">카카오뱅크</option>
							</select> &nbsp;
							<div class="ui input" style="width:400px">
								<input type="text">
							</div>
						</td>
					</tr>
				</table>
				
				<h3><i class="check square icon"></i>계약서 업데이트</h3>
				<table class="hru table3">
					<tr>
						<th>*근로계약서</th>
						<td>
							<div class="ui icon input" id="contractFileDiv">
								<input type="text" id="contractFileBtn" placeholder="파일명" style="width:400px" readonly>
								<i class="upload icon"></i>
								<input type="file" id="contractFile" name="contractFile">
							</div>
						</td>
						<th>이력서</th>
						<td>
							<div class="ui icon input" id="resumeFileDiv">
								<input type="text" id="resumeFileBtn" placeholder="파일명" style="width:400px" readonly>
								<i class="upload icon"></i>
								<input type="file" id="resumeFile" name="resumeFile">
							</div>
						</td>
					</tr>
					<tr>
						<th>주민등록등본</th>
						<td colspan="3">
							<div class="ui icon input" id="attestedFileDiv">
								<input type="text" id="attestedFileBtn" placeholder="파일명" style="width:400px" readonly>
								<i class="upload icon"></i>
								<input type="file" id="attestedFile" name="attestedFile">
							</div>
						</td>
					</tr>
				</table>
				<div style="margin-top:20px; margin-bottom:50px; float:right;">
					<input type="hidden" name="title" value="제목" id="title2">
					<input type="hidden" name="firstApproval" id="firstApp2">
					<input type="hidden" name="secondApproval" id="secondApp2">
					<input type="hidden" name="thirdApproval" id="thirdApp2">
					<input type="file"id="attach" name="appFile" onchange="textchange();">
					<button class="ui button" onclick="return enrollAll();">일괄등록</button>
					<button class="ui blue button" onclick="return approvalFunction();">전자결재</button>
				</div>
			</div> <!-- 내정보 끝 -->
		</form>	
		</div>
	</div>
	<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
	<div class="ui modal" id="enrollAllModal" style="padding:10px; width:1000px;">
		<i class="close icon"></i>
		<div class="header" align="center">
			<h2>일괄등록</h2>
		</div>
		<div class="hru segment" style="margin-bottom:10px; margin-right:0;">
			<form id="form1" name="form1" method="post" enctype="multipart/form-data">
				<input type="file" id="excelFile" name="fileInput" style="" onchange="uploadOK();">
				<div class="ui three steps">
					<div class="active step">
						<i class="file excel icon"></i>
						<div class="content">
							<div class="title" onclick="doExcelDownladProcess();">Excel Download</div>
							<div class="description">형식에 맞는 엑셀을 다운로드하세요.</div>
						</div>
					</div>
					<div class="step">
						<i class="upload icon"></i>
						<div class="content">
							<div class="title" onclick="fileUpload();">Excel Upload</div>
							<div class="description">형식에 맞춰 작성한 후 업로드하세요.
								<div id="fileName1" style="display:none;">파일명: </div>
							</div>
						</div>
					</div>
					<div class="step">
						<i class="file icon"></i>
						<div class="content">
							<div class="title" onclick="doExcelUploadProcess();">Enroll & Approval</div>
							<div class="description">등록과 전자결재를 하세요.</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="title" value="제목" id="title3">
				<input type="hidden" name="firstApproval" id="firstApp3">
				<input type="hidden" name="secondApproval" id="secondApp3">
				<input type="hidden" name="thirdApproval" id="thirdApp3">
			</form>
			<div class="ui message">
				<ul class="list">
					<li>엑셀파일을 다운받아 형식에 맞게 작성해주세요. 형식에 맞지않으면, 일괄등록이 되지 않습니다.</li>
					<li>근로계약서 등은 일괄적으로 등록이 불가하오니, 등록이 완료된 후에 사원별로 등록바랍니다.</li>
					<li>업로드한 파일은 결재내역에서 동일하게 첨부됩니다. 첨부파일을 참고해주세요.</li>
				</ul>
			</div>
		</div>
		
	</div>
	<script>
		var excelStatus=0;
		
		function enrollAll(){
			$("#enroll").prop("disabled", true);
			$("#enrollAllModal").modal('show');
			
			return false;	
		}
		
		function doExcelUploadProcess(){
			if(excelStatus==2){
				$.ajax({
					url : "selectOneBasic.app",
					type : "post",
					data : {approvalCode : 13},
					success : function(data){
						
						$("#appType").val("신규사원 일괄등록");
						var usermid = "${loginUser.mid}"; //기안자사번
						var yesApp = 0;
						
						for(var i=0; i<data.appBasic.mlist.length; i++){
							if(data.appBasic.mlist[i].mid == usermid){
								yesApp = 1;
							}
						}
						
						if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
							
							if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
								var firstName = data.appBasic.mlist[0].mName;
								var firstMid = data.appBasic.mlist[0].mid;
								var $input = $("<input type='hidden' name='first'>").val(firstMid);
								$("#firstApp").text(firstName).append($input);

								if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
									var secondName = data.appBasic.mlist[1].mName;
									var secondMid = data.appBasic.mlist[1].mid;
									var $input = $("<input type='hidden' name='second'>").val(secondMid);
									$("#secondApp").text(secondName).append($input);
									
									if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
										var thirdName = data.appBasic.mlist[2].mName;
										var thirdMid = data.appBasic.mlist[2].mid;
										var $input = $("<input type='hidden' name='third'>").val(thirdMid);
										$("#thirdApp").text(thirdName).append($input);
									}
								}
							}
						}
						$("#attachArea").prop("disabled", true);
						$("#approvalModal").modal('show');
					},
					error : function(status){
						console.log(status);
					}
				});
			} else{
				alert("단계에 맞춰 진행해주세요.");
			}
			
			
		}
		
		function doExcelDownladProcess(){
			var f = document.form1;
			f.action = "downloadExcelFile.app";
			f.submit();
			
			excelStatus = 1;
			$(".step").eq(0).removeClass("active").addClass("completed");
			$(".step").eq(1).addClass("active");
		}
		
		function fileUpload(){
			if(excelStatus==1){
				$("#excelFile").click();
				$("#fileName1").css("display","block");
			} else{
				alert("단계에 맞춰 진행해주세요.");
			}
		}
		function uploadOK(){
			var fileName = $("#excelFile").val().substring(12, 30) + "...";
			if(fileName != ""){
				$("#fileName1").text($("#fileName1").text() + fileName);
				excelStatus = 2;
				$(".step").eq(1).removeClass("active").addClass("completed");
				$(".step").eq(2).addClass("active");
			}
		}
	
		function approvalFunction(){
			var approvalCode = 1; //신규사원등록에 대한 결재코드
			//console.log("전자결재");
			
			//빈칸검사!
			if($("input[name=tempName]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("input[name=tempName]").parent().addClass("error");
				return false;
			}
			if($("input[name=tempSsn]").val() == "" || $("input[name=tempSsn2]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("input[name=tempSsn]").parent().addClass("error");
				$("input[name=tempSsn]").parent().addClass("error");
				return false;
			}
			if($("input[name=tempEmail]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("input[name=tempEmail]").parent().addClass("error");
				return false;
			}
			if($("input[name=startDate]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("input[name=startDate]").parent().addClass("error");
				return false;
			}
			if($("select[name=enterCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=enterCode]").parent().addClass("error");
				return false;
			}
			if($("select[name=teamCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=teamCode]").parent().addClass("error");
				return false;
			}
			if($("select[name=positionCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=positionCode]").parent().addClass("error");
				return false;
			}
			if($("select[name=jobCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=jobCode]").parent().addClass("error");
				return false;
			}
			if($("select[name=responsibilityCode]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=responsibilityCode]").parent().addClass("error");
				return false;
			}
			if($("select[name=salaryType]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("select[name=salaryType]").parent().addClass("error");
				return false;
			}
			if($("input[name=salaryAmount]").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '필수사항을 다 기입해주세요.'
				});
				$("input[name=salaryAmount]").parent().addClass("error");
				return false;
			}
			if($("#contractFileBtn").val() == ""){
				Swal.fire({
					  icon: 'error',
					  title: '빈칸불가',
					  text: '근로계약서는 반드시 첨부해야합니다.'
				});
				$("#contractFileBtn").parent().addClass("error");
				return false;
			}
			
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					
					$("#appType").val("신규사원등록");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thidName = data.appBasic.mlist[2].mName;
									var thidMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
				},
				error : function(status){
					console.log(status);
				}
			});
			
			
			return false;
		}
		
		
		
		$(function() {
			$("#hr1").show();
			$('#refoundDay').calendar({ 
				type: 'date',
				formatter: {
				      date: function (date, settings) {
				        if (!date) return '';
				        var day = date.getDate();
				        if(day < 10){day = "0" + day;}
				        var month = date.getMonth() + 1;
				        if(month < 10){ month = "0" + month;}
				        var year = date.getFullYear();
				        return year + '-' + month + '-' + day;
				      }
				}
			});

			$('.ui.dropdown').dropdown();
			
			
			$('#request').click(function(){
				$("#approvalModal").modal('hide');

				//console.log("눌렸음!");
				if($("#appType").val()=="신규사원등록"){
					$("#title2").val($("#title").val());
					$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
					$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
					$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
					
					$('#memberInsertForm').submit();
					var content = "결재요청 : " + $("#title").val();
					var tmid = $("#firstApp").find("input[type=hidden]").val();
					if(tmid!="" && tmid!=null && tmid!=0){
						send(tmid, content);
					}
				} else{
					//alert("일괄등록!");
					$("#title3").val($("#title").val());
					$("#firstApp3").val($("#firstApp").find("input[type=hidden]").val());
					$("#secondApp3").val($("#secondApp").find("input[type=hidden]").val());
					$("#thirdApp3").val($("#thirdApp").find("input[type=hidden]").val());

					var f = new FormData(document.getElementById('form1'));

					$.ajax({
						url : "uploadExcelFile.app",
						data : f,
						processData : false,
						contentType : false,
						type : "post",
						success : function(data){
							var msg = data.msg;
							var content = "결재요청 : " + $("#title").val();
							var tmid = $("#firstApp").find("input[type=hidden]").val();
							if(msg==1){
								if(tmid!="" && tmid!=null && tmid!=0){send(tmid, content);} //아직 테스트 못했음!
								Swal.fire({
									  icon: 'success',
									  title: "일괄등록에 성공했습니다!",
									  text: '등록여부는 결재함을 확인해주세요.',
								});
							} else{
								Swal.fire({
									  icon: 'error',
									  title: "일괄등록에 실패했습니다!",
									  text: '엑셀에 형식이 알맞은지 확인해주세요.',
								});
							}
							
						},
						error : function(status){
							console.log(status);

							Swal.fire({
								  icon: 'error',
								  title: "일괄등록에 실패했습니다!",
								  text: '엑셀에 형식이 알맞은지 확인해주세요.',
							});
						}
					});
				}
				
				
			});
			
			$("#contractFileBtn").click(function() {
				$(this).parent().find("input:file").click();
			});

			$("input:file[name='contractFile']").on('change', function(e) {
				var name = null;
				if(e.target.files[0] != null) {
					console.log(e.target.files[0]);
					name = e.target.files[0].name;
				}
				$("input:text[id='contractFileBtn']").val(name);
			});
			
			$("#resumeFileBtn").click(function() {
				$(this).parent().find("input:file").click();
			});
			
			$("input:file[name='resumeFile']").on('change', function(e) {
				var name = null;
				if(e.target.files[0] != null) {
					console.log(e.target.files[0]);
					name = e.target.files[0].name;
				}
				$("input:text[id='resumeFileBtn']").val(name);
			});
			
			$("#attestedFileBtn").click(function() {
				$(this).parent().find("input:file").click();
			});
			
			$("input:file[name='attestedFile']").on('change', function(e) {
				var name = null;
				if(e.target.files[0] != null) {
					console.log(e.target.files[0]);
					name = e.target.files[0].name;
				}
				$("input:text[id='attestedFileBtn']").val(name);
			});
			
			

			$("#groupSelect").change(function(){
				//console.log($(this).val());
				$("#teamSelect").html("<option value=''>부서</option>");
				$("#teamSelect").parent().find(".text").text("부서");
				
				console.log("test");
				<c:forEach var="i" begin="0" end="${hi.getTlist().size()-1}">
					
					var teamCode = "${hi.getTlist().get(i).get('teamCode')}";
					var teamName = "${hi.getTlist().get(i).get('teamName')}";
					var groupCode = "${hi.getTlist().get(i).get('groupCode')}";
					
					if(groupCode == $(this).val()){
						var $option = $("<option>").text(teamName).val(teamCode);
						$("#teamSelect").append($option);
					}
				</c:forEach>
				
			});
			
			$("#ppSelect").change(function(){
				console.log($(this).val());
				$("#positionSelect").html("<option value=''>호봉</option>");
				$("#positionSelect").parent().find(".text").text("호봉");
				
				
				<c:forEach var="i" begin="0" end="${hi.getPlist().size()-1}">
					var positionCode = "${hi.getPlist().get(i).get('positionCode')}";
					var positionName = "${hi.getPlist().get(i).get('positionName')}";
					var prePositionCode = "${hi.getPlist().get(i).get('prePositionCode')}";
					
					if(prePositionCode == $(this).val()){
						var $option = $("<option>").text(positionName).val(positionCode);
						$("#positionSelect").append($option);
					}
				</c:forEach>
				
			});
			
		});
		
		
	</script>
</body>
</html>












