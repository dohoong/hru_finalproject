<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<style>
.img {
	width:202px;
	height:205px;
	padding-top:2px;
	overflow:hidden;
	object-fit:cover;
}
.hoverTr:hover {
	cursor:pointer;
	background:#eee;
}
.download:hover{
	cursor:pointer;
	text-decoration:underline;
}
</style>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<h1 style="color:black">사원 상세조회</h1>
			<!-- 메뉴선택 바 -->
			<div class="ui top attached tabular menu" style="margin-top:50px">
					<a class="active item" data-tab="first">기본정보</a> 
					<a class="item" data-tab="second">경력/학력</a>
					<a class="item" data-tab="third">인사정보</a>
			</div><!-- 메뉴선택 바 끝-->
			
			<!-- 기본정보 -->
			<div class="ui bottom attached active tab segment" data-tab="first">
				<div class="hru segment">
					<table style="width:100%; margin-bottom:50px;">
						<tr>
							<td width="202px">
								<c:if test="${mi.changeName != null}">
									<img class="img" src="resources/uploadFiles/${mi.changeName}">
								</c:if>
								<c:if test="${mi.changeName == null}">
									<img class="img" src="resources/images/defaultProfile.png" class="img">
								</c:if>
							</td>
							<td>
								<table class="hru table2" style="margin-left: 10px">
									<tr>
										<th>사원번호</th>
										<td><c:out value="${mi.mid}" /></td>
										<th>재직상태</th>
										<td><c:choose>
												<c:when test="${mi.workStatus eq 1}"><c:out value="재직중"/></c:when>
												<c:when test="${mi.workStatus eq 2}"><c:out value="퇴직 (${mi.outDate})"/></c:when>
												<c:when test="${mi.workStatus eq 3}"><c:out value="휴직"/></c:when>
											</c:choose>
											<c:out value="/ ${mi.enterName}"/>
										</td>
									</tr>
									<tr>
										<th>성명</th>
										<td><c:out value="${mi.mName}"/></td>
										<th>주민등록번호</th>
										<td><c:out value="${fn:substring(mi.ssn,0,fn:length(mi.ssn)-6)}******"/></td>
									</tr>
									<tr>
										<th>전화번호</th>
										<td><c:out value="${mi.phone}"/></td>
										<th>이메일</th>
										<td><c:out value="${mi.email}"/></td>
									</tr>
									<tr>
										<th>내외국인</th>
										<td>
											<c:if test="${mi.korean eq 'Y'}">
												<c:out value="내국인"/>
											</c:if>
											<c:if test="${mi.korean eq 'N'}">
												<c:out value="외국인"/>
											</c:if>
										</td>
										<th>국적</th>
										<td><c:out value="${mi.country}"/></td>
									</tr>
									<tr>
										<th>주소</th>
										<td colspan="3"><c:out value="${mi.address}"/></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<table class="hru table2" style="margin-bottom:50px">
						<tr>
							<th>사업장</th>
							<td><c:out value="${mi.groupName}"/></td>
							<th>부서</th>
							<td><c:out value="${mi.teamName}"/></td>
							<th>직종</th>
							<td><c:out value="${mi.jobName}"/></td>
						</tr>
						<tr>
							<th>직위</th>
							<td><c:out value="${mi.ppName}"/></td>
							<th>직급</th>
							<td><c:out value="${mi.positionName}"/></td>
							<th>직책</th>
							<td><c:out value="${mi.responsibilityName}"/></td>
						</tr>
						<tr>
							<th>입사일자</th>
							<td><c:out value="${mi.enterDate}"/></td>
							<th>급여형태</th>
							<td>
								<c:choose>
									<c:when test="${mi.salaryType eq 1}"><c:out value="연봉"/></c:when>
									<c:when test="${mi.salaryType eq 2}"><c:out value="월급"/></c:when>
									<c:when test="${mi.salaryType eq 3}"><c:out value="시급"/></c:when>
								</c:choose>
							</td>
							<th>급여액</th>
							<td><fmt:formatNumber value="${mi.salaryAmount}" pattern="#,###" /> 원</td>
						</tr>
					</table>
					
					<table class="hru table2">	
						<tr>
							<th>근로계약서</th>
							<td><label class='download' onclick='downloadAtt()'>근로계약서_첨부.png</label></td>
							<th>이력서</th>
							<td><label class='download' onclick='downloadAtt()'>이력서_첨부.png</label></td>
						</tr>
					</table>
				</div>
				<div class="hru segment">
					<h3><i class="check square icon"></i>가족정보</h3>
					<table class="hru board2">
						<thead>
							<tr>
								<th>이름</th>
								<th>관계</th>
								<th>주민등록번호</th>
								<th width="200px">동거여부</th>
								<th width="200px">소득세공제 부양여부</th>
								<th width="200px">건강보험 피부양자</th>
							</tr>
						</thead>
						<tbody id="showTB">
							<c:if test="${mfList.size() != 0}">
								<c:forEach var="mf" items="${mfList}" varStatus="status">
									<tr>
										<td><c:out value="${mf.fName}" /></td>
										<td>
											<c:choose>
												<c:when test="${mf.fCategory eq 1}"><c:out value="배우자"/></c:when>
												<c:when test="${mf.fCategory eq 2}"><c:out value="자녀"/></c:when>
												<c:when test="${mf.fCategory eq 3}"><c:out value="형제자매"/></c:when>
												<c:when test="${mf.fCategory eq 4}"><c:out value="직계존속"/></c:when>
												<c:when test="${mf.fCategory eq 5}"><c:out value="직계비속"/></c:when>
											</c:choose>
										</td>
										<td><c:out value="${fn:substring(mf.fSsn,0,fn:length(mf.fSsn)-6)}******"/></td>
										<td><c:if test="${mf.togetherYN eq 'Y'}"><i class="check big green icon"></i></c:if></td>
										<td><c:if test="${mf.deductionYN eq 'Y'}"><i class="check big green icon"></i></c:if></td>
										<td><c:if test="${mf.insuranceYN eq 'Y'}"><i class="check big green icon"></i></c:if></td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${mfList.size() == 0}">
								<tr>
									<td style="padding: 20px" colspan="7">
										<h3 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 가족이 없습니다.</label></h3>
									</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
			</div><!-- 기본정보 끝 -->
			
			<!-- 이력정보 -->
			<div class="ui bottom attached tab segment" data-tab="second">
				<div class="hru segment">
					<h3><i class="check square icon"></i>학력정보</h3>
					<table class="hru board2">
						<thead>
							<tr>
								<th>NO</th>
								<th>학교명</th>
								<th>상태</th>
								<th>입학날짜</th>
								<th>졸업날짜</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${eduList.size() > 0}">
								<c:forEach var="el" items="${eduList}" varStatus="status">
									<tr>
										<td><c:out value="${status.count}" /></td>
										<td><c:out value="${el.maddTitle}"/></td>
										<td><c:out value="${el.maddContent}"/></td>
										<td><c:out value="${fn:split(el.startDate,'-')[0]}"/>년 <c:out value="${fn:split(el.startDate,'-')[1]}"/>월</td>
										<td><c:out value="${fn:split(el.endDate,'-')[0]}"/>년 <c:out value="${fn:split(el.endDate,'-')[1]}"/>월</td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${eduList.size() eq 0}">
								<tr>
									<td style="padding: 20px" colspan="5">
										<h3 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 학력정보가 없습니다.</label></h3>
									</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
				<div class="hru segment">
					<h3><i class="check square icon"></i>경력정보</h3>
					<table class="hru board2">
						<thead>
							<tr>
								<th>NO</th>
								<th>회사명</th>
								<th>입사일자</th>
								<th>퇴사일자</th>
								<th>최종직위</th>
							</tr>
						</thead>
						<tbody id="careerTbodyOrigin">
							<c:if test="${careerList.size() > 0}">
								<c:forEach var="cl" items="${careerList}" varStatus="status">
									<tr>
										<td><c:out value="${status.count}" /></td>
										<td><c:out value="${cl.maddTitle}"/></td>
										<td><c:out value="${fn:split(cl.startDate,'-')[0]}"/>년 <c:out value="${fn:split(cl.startDate,'-')[1]}"/>월</td>
										<td><c:out value="${fn:split(cl.endDate,'-')[0]}"/>년 <c:out value="${fn:split(cl.endDate,'-')[1]}"/>월</td>
										<td><c:out value="${cl.maddContent}"/></td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${careerList.size() eq 0}">
								<tr>
									<td style="padding: 20px" colspan="5">
										<h3 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 경력정보가 없습니다.</label></h3>
									</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
				<div class="hru segment">
					<h3><i class="check square icon"></i>자격정보</h3>
					<table class="hru board2">
						<thead>
							<tr>
								<th>NO</th>
								<th>자격명</th>
								<th>발행처 / 발행기관</th>
								<th>자격 취득일자</th>
							</tr>
						</thead>
						<tbody id="cerfiTbodyOrigin">
							<c:if test="${cerfiList.size() > 0}">
								<c:forEach var="cf" items="${cerfiList}" varStatus="status">
									<tr>
										<td><c:out value="${status.count}" /></td>
										<td><c:out value="${cf.maddTitle}"/></td>
										<td><c:out value="${cf.maddContent}"/></td>
										<td><c:out value="${fn:split(cf.startDate,'-')[0]}"/>년 <c:out value="${fn:split(cf.startDate,'-')[1]}"/>월</td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${cerfiList.size() eq 0}">
								<tr>
									<td style="padding: 20px" colspan="4">
										<h2 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 자격정보가 없습니다.</label></h2>
									</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
				<div class="hru segment">	
					<h3><i class="check square icon"></i>어학정보</h3>
					<table class="hru board2">
						<thead>
							<tr>
								<th>NO</th>
								<th>시험명</th>
								<th>등급(점수)</th>
								<th>점수 취득일자</th>
							</tr>
						</thead>
						<tbody id="languageTbodyOrigin">
							<c:if test="${languageList.size() > 0}">
								<c:forEach var="ll" items="${languageList}" varStatus="status">
									<tr>
										<td><c:out value="${status.count}" /></td>
										<td><c:out value="${ll.maddTitle}"/></td>
										<td><c:out value="${ll.maddContent}"/></td>
										<td><c:out value="${fn:split(ll.startDate,'-')[0]}"/>년 <c:out value="${fn:split(ll.startDate,'-')[1]}"/>월</td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${languageList.size() eq 0}">
								<tr>
									<td style="padding: 20px" colspan="4">
										<h3 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 어학정보가 없습니다.</label></h3>
									</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
			</div><!-- 이력정보 끝-->
			
			<!-- 인사정보 -->
			<div class="ui bottom attached tab segment" data-tab="third">
				<div class="hru segment">
					<h3><i class="check square icon"></i>인사발령</h3>
					<table class="hru board2">
						<thead>
							<tr>
								<th style="width:150px;">발령구분</th>
								<th style="width:200px;">발령일자</th>
								<th style="width:250px;">사업장</th>
								<th style="width:250px;">발령부서</th>
								<th>사유</th>
							</tr>
						</thead>
						<c:if test="${hrList.size() > 0}">
						<tbody>
							<c:forEach var="hr" items="${hrList}" varStatus="status">
								<tr>
									<td>
										<c:choose>
											<c:when test="${hr.category eq 1}"><c:out value="입사"/></c:when>
											<c:when test="${hr.category eq 2}"><c:out value="직무이동"/></c:when>
											<c:when test="${hr.category eq 3}"><c:out value="부서이동"/></c:when>
											<c:when test="${hr.category eq 4}"><c:out value="조직책임자 임명"/></c:when>
										</c:choose>
									</td>
									<td><c:out value="${hr.moveDate}"/></td>
									<td><c:out value="${hr.groupName}"/></td>
									<td><c:out value="${hr.teamName}"/></td>
									<td><c:out value="${hr.reason}"/></td>
								</tr>
							</c:forEach>
						</tbody>
						</c:if>
						<c:if test="${hrList.size() eq 0}">
						<tbody>
							<tr>
								<td style="padding: 20px" colspan="5">
									<h3 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 인사발령 내역이 없습니다.</label></h3>
								</td>
							</tr>
						</tbody>
						</c:if>
					</table>
				</div>
				<div class="hru segment">
					<h3><i class="check square icon"></i>진급내역</h3>
					<table class="hru board2" style="width:99%">
						<thead>
							<tr class="noeval">
								<th style="width:200px;">진급일자</th>
								<th style="width:300px;">직급</th>
								<th style="width:300px;">사업장</th>
								<th style="width:300px;">부서</th>
								<th>조직책임자</th>
							</tr>
						</thead>
						<c:if test="${prList.size() > 0}">
						<tbody>
							<c:forEach var="pr" items="${prList}" varStatus="status">
								<tr>
									<td><c:out value="${pr.moveDate}"/></td>
									<td><c:out value="${fn:substring(pr.positionName, 0, 2)}"/></td>
									<td><c:out value="${pr.groupName}"/></td>
									<td><c:out value="${pr.teamName}"/></td>
									<td><c:out value="${pr.reason}"/></td>
								</tr>
							</c:forEach>
						</tbody>
						</c:if>
						<c:if test="${prList.size() eq 0}">
						<tbody>
							<tr class="noeval">
								<td style="padding: 20px" colspan="5">
									<h3 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 진급 내역이 없습니다.</label></h3>
								</td>
							</tr>
						</tbody>
						</c:if>
					</table>
				</div>
				<div class="hru segment">
					<h3><i class="check square icon"></i>인사평가 내역</h3>
					<table class="hru board2 eval" style="width:99%">
						<thead>
							<tr>
								<th>해당연도</th>
								<th>평가결과</th>
								<th>진행상황</th>
								<th>사업장</th>
								<th>부서</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${evalList.size() > 0 }">
							<c:forEach var="e" items="${evalList}" varStatus="status">
								<tr class="hoverTr" onclick="showInput('${e.evaluationId}')">
									<td><c:out value="${e.evaluationYear}"/></td>
									<td><c:out value="${e.totalScore}"/></td>
									<td>
										<c:if test="${e.approvalId == 0}">
											<c:if test="${e.approvalYn == 'P'}">입력기간</c:if>
											<c:if test="${e.approvalYn == 'N'}">준비기간</c:if>
										</c:if>
										<c:if test="${e.approvalId != 0}">
											<c:if test="${e.approvalYn == 'P'}">평가진행중</c:if>
											<c:if test="${e.approvalYn == 'Y'}">평가완료</c:if>
										</c:if>
									</td>
									<td><c:out value="${e.groupName}"/></td>
									<td><c:out value="${e.teamName}"/></td>
								</tr>
							</c:forEach>
							</c:if>
							<c:if test="${evalList.size() eq 0}">
								<tr class="noeval">
									<td style="padding: 20px" colspan="5">
										<h3 style="color: lightgray"><label><i class="exclamation circle icon"></i>등록된 인사평가 내역이 없습니다.</label></h3>
									</td>
								</tr>
							</c:if>
						</tbody>
					</table>
					<jsp:include page="../common/evaluation.jsp"/>
				</div>
			</div><!-- 인사정보 끝-->
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	
	<script>
	$(function(){
		$(".menu .item").tab();
	});
	
	$(function() {
		$(".ui.horizontal.card").hide();

		$(".alarm.large.icon").mouseenter(function() {
			$(this).transition('tada');
		});
		$("#showInfo").hover(function(){
			$(this).css("cursor", "pointer");
		}).click(function(){
			$(".ui.horizontal.card").show();
			$(".ui.horizontal.card").mouseleave(function(){
				$(this).hide();
			});
		});
	});
	
	function showInput(evalId){
		$.ajax({
			url : "selectEvaluationDetail.eval",
			type : "post",
			data : {
				evaluationId : evalId
			},
			success : function(data){
				console.log(data);
				var evaluation = data.eval;
				var edlist = data.edlist;
				var $twoWay = $("#twoWay");
				$twoWay.html("");
				var $oneWayBody = $("#oneWay tbody");
				$oneWayBody.html("");
				var $idInput = $("<input type='hidden'>").attr("name","evaluationId").val(evaluation.evaluationId);
				
				
				$("*[data-tab=twoWay]").addClass("active");
				$("*[data-tab=oneWay]").removeClass("active");
				$("#onewayScore").val("").append($idInput);
				$('#evaluationModal').find(".dropdown").addClass("disabled");
				$('#evaluationModal').find(".dropdown").find(".text").addClass("default").text("진급대상자 여부");
				$("#complete").prop("disabled", true);
				
				for(var i=0; i<edlist.length; i++){
					if(edlist[i].category==1){
						var $table = $("<table class='hru evaluation'>");
						var $tBody = $("<tbody>");
						
						var $th4 = $("<th colspan='4'>").html("* "+ edlist[i].evalName + " <span style='color:gray;'> (" + edlist[i].evalContent + ") </span>");
						var $tr0 = $("<tr>").append($th4);
						$tBody.append($tr0);
						
						var edId = "edlist[" + i + "].edId";
						var $input11 = $("<input type='hidden'>").val(edlist[i].edId).attr("name",edId);
						
						var $th1 = $("<th colspan='2'>").text("목표수립").append($input11);
						var $th2 = $("<th>").text("피평가자 의견");
						var $th3 = $("<th>").text("평가자 의견");
						var $tr1 = $("<tr>").append($th1).append($th2).append($th3);
						$tBody.append($tr1);
						
						
						var $td1 = $("<td>").append($("<textarea rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].edContent)); //EVAL_CONTENT
						var $td2 = $("<td>").text(">>");
						var $td3 = $("<td>").append($("<textarea rows='5' class='ropinion' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].ropinion));
						var $td4 = $("<td>").append($("<textarea class='gopinion' rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].gopinion));
						var $tr2 = $("<tr>").append($td1).append($td2).append($td3).append($td4);
						$tBody.append($tr2);
						
						var $td5 = $("<td colspan='5' style='text-align:right'>");
						var $label3 = $("<label class='hru label'>").text("점수 : ");
						$td5.append($label3).append($("<div class='ui disabled input small disabled' style='margin-right:20px;'>").append($("<input class='each' type='text' size='5' value='0' readonly>").val(edlist[i].evalScore)));
						var $tr3 = $("<tr>").append($td5);
						$tBody.append($tr3);
						
						$table.append($tBody);
						$twoWay.append($table).append($("<div class='ui divider'>"));
						$("#bottomContent").css("display", "none");
					}  else if(edlist[i].category==2){
						switch(edlist[i].type){
						case 1 : var $td1 = $("<td>").text("업적"); break;
						case 2 : var $td1 = $("<td>").text("능력"); break;
						case 3 : var $td1 = $("<td>").text("태도"); break;
						}
						var edId = "edlist[" + i + "].edId";
						var $input11 = $("<input type='hidden'>").val(edlist[i].edId).attr("name",edId);
						$td1.append($input11);
						
						var etext = "edlist[" + i + "].evalScore";
						var radio = "radio" + i;
						var $td2 = $("<td class='etitle' style='cursor:pointer;' onclick='showEvalContent(this);'>").text(edlist[i].evalName).append($("<input type='hidden'>").val(edlist[i].evalContent));
						var $td3 = $("<td>").append($("<div class='ui input disabled'>").append($("<input type='radio' value='100'>").attr('name', radio)));
						var $td4 = $("<td>").append($("<div class='ui input disabled'>").append($("<input type='radio' value='80'>").attr('name', radio)));
						var $td5 = $("<td>").append($("<div class='ui input disabled'>").append($("<input type='radio' value='60'>").attr('name', radio)));
						var $td6 = $("<td>").append($("<div class='ui input disabled'>").append($("<input type='radio' value='40'>").attr('name', radio)));
						var $td7 = $("<td>").append($("<div class='ui input disabled'>").append($("<input type='radio' value='20'>").attr('name', radio)));
						var $td8 = $("<td>").append($("<div class='ui input small disabled'>").append($("<input type='text' size='5' readonly>").val(edlist[i].evalWeight)));
						var $td9 = $("<td>").append($("<div class='ui input small disabled'>").append($("<input type='text' size='5' class='each' value='0' readonly>").attr("name", etext).val(edlist[i].evalScore)));
						var $tr1 = $("<tr class='evall'>").append($td1).append($td2).append($td3).append($td4).append($td5).append($td6).append($td7).append($td8).append($td9);
						$oneWayBody.append($tr1);
					}
				}
				
				$('#evaluationModal').modal('show');
		        
			},
			error : function(status){
				console.log(status);
			}
		});
	}
	
	function downloadAtt(id){
		Swal.fire(
				  '다운로드 실패!',
				  '서버가 혼잡해 다운로드 할 수 없습니다.',
				  'error'
				)
	}
	</script>
</body>
</html>