<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	.addTable{
		background:#eee;
		margin-top:20px;
		padding:10px 20px 10px 20px;
		border-radius:5px;
		color:#0F4C81;
	}
	.plus{
		display:inline-block;
		margin-left:10px;
	}
	.hru.table td{
		border:0;
	}
	.hru.table th{
		border:0;
	}
	.hru.table{
		border-bottom:1px solid #ccc;
	}
	.hru.table tbody{
		border-bottom:1px solid #ccc;
	}
	.searchArea h3{
		margin-right:20px;
		margin-left:50px;
	}
	.hide{
		display:none;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<form id="evalForm">
		<div class="hru segment">
			<h1>인사평가</h1>
			<div class="ui divider"></div>
			<div class="searchArea">
				<table align="center" cellspacing="10px">
					<tr>
						<td><h3>사업장/부서 </h3></td>
						<td>
							<select class="ui dropdown" id="groupSelect">
								<option value="">사업장</option>
									<c:forEach var="i" begin="0" end="${glist.size() - 1}">
										<option value="${glist[i].groupCode}"><c:out value="${glist[i].groupName}"/></option>
									</c:forEach>
							</select>
							<select class="ui dropdown" name="teamCode" id="teamSelect">
								<option value="">부서</option>
							</select>
						</td>
						<th><h3>평가년도</h3></th>
						<td width="200px">
							<div class="ui calendar" id="yearCalendar">
								<div class="ui input left icon"><i class="calendar icon"></i>
									<input type="text" placeholder="해당연도" size="10" id="evalYear" name="evalYear"></div>
							</div>
						<td>
							<button class="ui blue button" id="serchEval" type="button">검색하기</button>
						</td>
					</tr>
				</table>
			</div><!-- searchArea end -->
			<div class="ui message">
					<ul class="list">
						<li>인사담당자는 조직책임자와 협의 하에 평가항목 및 기준을 설정할 수 있습니다.</li>
						<li>양방향평가 : 평가자와 피평가자가 함께 평가내역을 설정합니다. 평가기간에 피평가자와 평가자는 모두 의견을 제시하고 피평가자는 본인의 성과를 어필할 수 있습니다. </li>
						<li>단방향평가 : 평가항목이 결정되면, 평가자의 평가만 이루어집니다. 해당평가는 피평가자가 볼 수 없습니다.</li>
					</ul>
			</div>
		</div>
		<div id="codeArea"></div>
		<div class="hru segment">
			<table class="hru table" id="evalTable">
			</table>
			<div class="addTable hide" id="addArea">
				<i class="ui plus icon"></i>
				<div class="ui selection dropdown" id="evalCategory">
					<input type="hidden"><i class="dropdown icon"></i>
					<div class="default text">카테고리</div>
					<div class="menu">
						<div class="item" data-value="1">양방향평가</div>
						<div class="item" data-value="2">단방향평가</div></div>
				</div>
				<div class="plus">
					<button class="ui icon blue button mini" id="add" type="button"><i class="plus icon"></i></button>
					<button class="ui icon red button mini" id="remove" type="button"><i class="minus icon"></i></button>
				</div>	
			</div>
			<div class="buttonArea hide" style="margin-top:20px;" id="btnArea">
				<button class="ui basic blue button" id="store" type="button">임시저장</button>
				<button class="ui blue button" id="approv" onclick="return approvalFunction();">전자결재</button>
			</div>
		</div>
		<input type="hidden" name="title" value="제목" id="title2">
		<input type="hidden" name="firstApproval" id="firstApp2">
		<input type="hidden" name="secondApproval" id="secondApp2">
		<input type="hidden" name="thirdApproval" id="thirdApp2">
		</form>
		<div class="hru segment hide">
			<table class="hru table" id="prepareTable1">
				<tbody>
					<tr>
						<th rowspan="2" style="width:150px;">양방향평가</th>
						<td><label class="hru label">*카테고리 : </label></td>
						<td>
							<select class="ui compact dropdown" name="typeB">
								<option value="1">업적</option>
								<option value="2">능력</option>
								<option value="3">태도</option>
							</select>
						</td>
						<td><label class="hru label">* 항목명 : </label></td>
						<td><div class="ui input small"><input type="text" size="50" name="nameB"></div></td>
						<td><label class="hru label">* 가중치 : </label></td>
						<td><div class="ui input small"><input type="number" size="10" min="0" max="100" name="weightB"></div> %</td>
					</tr>
					<tr>
						<td><label class="hru label">* 설명 : </label></td>
						<td colspan="6">
							<div class="ui input small"><input type="text" size="120" name="contentB"></div>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="hru table" id="prepareTable2">
				<tbody>
					<tr>
						<th rowspan="6" style="width:150px;">단방향평가</th>
						<td><label class="hru label">*카테고리 : </label></td>
						<td>
							<select class="ui compact dropdown" name="typeO">
								<option value="1">업적</option>
								<option value="2">능력</option>
								<option value="3">태도</option>
							</select>
						</td>
						<td><label class="hru label">* 항목명 : </label></td>
						<td><div class="ui input small"><input type="text" size="50" name="nameO"></div></td>
						<td><label class="hru label">* 가중치 : </label></td>
						<td><div class="ui input small"><input type="number" size="10" min="0" max="100" name="weightO"></div> %</td>
					</tr>
					<tr>
						<td><label class="hru label">* 평가지표</label></td>
						<td colspan="6"><label class="hru label">A:</label><div class="ui input small"><input type="text" size="100" name="contentOA"></div>
							<label class="hru label">(80% ~ 100%)</label>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="6"><label class="hru label">B:</label><div class="ui input small"><input type="text" size="100" name="contentOB"></div>
							<label class="hru label">(60% ~ 80%)</label>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="6"><label class="hru label">C:</label><div class="ui input small"><input type="text" size="100" name="contentOC"></div>
							<label class="hru label">(40% ~ 60%)</label>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="6"><label class="hru label">D:</label><div class="ui input small"><input type="text" size="100" name="contentOD"></div>
							<label class="hru label">(20% ~ 40%)</label>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="6"><label class="hru label">E:</label><div class="ui input small"><input type="text" size="100" name="contentOE"></div>
							<label class="hru label">(0% ~ 20%)</label>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div><!-- 내용 끝 -->
		<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
		</div><!-- outer end -->
	<script>
		$(function(){
			$("#hr3").show();
			$(".ui.dropdown").dropdown();
			
			$("#yearCalendar").calendar({
				type : 'year',
				initialDate : new Date()
			});
			
			$("#groupSelect").change(function(){
				$("#teamSelect").html("<option value=''>부서</option>");
				$("#teamSelect").parent().find(".text").text("부서");
				
				<c:forEach var="i" begin="0" end="${tlist.size()-1}">
					
					var teamCode = "${tlist[i].teamCode}";
					var teamName = "${tlist[i].teamName}";
					var groupCode = "${tlist[i].groupCode}";
					
					if(groupCode == $(this).val()){
						var $option = $("<option value='teamCode'>").text(teamName).val(teamCode);
						$("#teamSelect").append($option);
					}
				</c:forEach>
				
			});
			
			$("#serchEval").click(function(){
				if($("#teamSelect").val() == "") {
					Swal.fire(
							  '사업장과 부서를 선택해주세요.',
							  '',
							  'error'
							)
				} else {
					var teamCode = $("#teamSelect").val();
					var year = $("#evalYear").val()
					$("#add").removeClass("disabled");
					$("#remove").removeClass("disabled");
					$("#evalCategory").removeClass("disabled");
					$("#store").removeClass("disabled");
					$("#approv").removeClass("disabled");
					
					
					$("#evalTable").empty();
					$("#codeArea").empty();

					$.ajax({
						url:"showEvaluateList.hr",
						type:"POST",
						data:{'teamCode' : teamCode, 'year' : year},
						success:function(data){
							var evalList = data.evalList;
							var count = 0;
							
							if(evalList.length > 0){
								$.each(evalList, function(index, e){
									var $tr1 = $("<tr>");
									var $tr2 = $("<tr>");
									var $tr3 = $("<tr>");
									var $tr4 = $("<tr>");
									var $tr5 = $("<tr>");
									var $tr6 = $("<tr>");
									var $tbody = $("<tbody>");
									var $nameB = $('<input type="text" size="50" name="nameB">').val(e.evalName);
									var $nameO = $('<input type="text" size="50" name="nameO">').val(e.evalName);
									var $weightB = $('<input type="number" size="10" min="0" max="100" name="weightB">').val(e.evalWeight);
									var $weightO = $('<input type="number" size="10" min="0" max="100" name="weightO">').val(e.evalWeight);
									var $contentB = $('<input type="text" size="120" name="contentB">').val(e.evalContent);
									
									
									if(year != new Date().getFullYear()){
										if(e.category == 1) {
											$tbody.append($tr1.append($('<th rowspan="2" style="width:150px;">').text("양방향평가")));
											$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 카테고리 : "))));
											switch(e.type){
												case 1 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled" name="typeB"><option value="1" selected>업적</option><option value="2">능력</option><option value="3">태도</option></select>'))); break;
												case 2 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled" name="typeB"><option value="1">업적</option><option value="2" selected>능력</option><option value="3">태도</option></select>'))); break;
												case 3 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled" name="typeB"><option value="1">업적</option><option value="2">능력</option><option value="3" selected>태도</option></select>'))); break;
											}
											$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 항목명 : "))));
											$tbody.append($tr1.append($('<td>').append($('<div class="ui input small disabled">').append($nameB))));
											$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 가중치 : "))));
											$tbody.append($tr1.append($('<td>').append($('<div class="ui input small disabled">').append($weightB))));
											$tbody.append($tr2.append($('<td>').append($('<label class="hru label">').text("* 설명 : "))));
											$tbody.append($tr2.append($('<td colspan="6">').append($('<div class="ui input small disabled">').append($contentB))));
											
											$("#evalTable").append($tbody);
											$(".ui.dropdown").dropdown();
										} else {
											var content = (e.evalContent).split("#");
											$tbody.append($tr1.append($('<th rowspan="6" style="width:150px;">').text("단방향평가")));
											$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 카테고리 : "))));
											switch(e.type){
												case 1 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled" name="typeO"><option value="1" selected>업적</option><option value="2">능력</option><option value="3">태도</option></select>'))); break;
												case 2 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled" name="typeO"><option value="1">업적</option><option value="2" selected>능력</option><option value="3">태도</option></select>'))); break;
												case 3 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled" name="typeO"><option value="1">업적</option><option value="2">능력</option><option value="3" selected>태도</option></select>'))); break;
											}
											$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 항목명 : "))));
											$tbody.append($tr1.append($('<td>').append($('<div class="ui input small disabled">').append($nameO))));
											$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 가중치 : "))));
											$tbody.append($tr1.append($('<td>').append($('<div class="ui input small disabled">').append($weightO))));
											$tbody.append($tr2.append($('<td>').append($('<label class="hru label">').text("* 평가지표 : "))));
											$tbody.append($tr2.append($('<td colspan="6"><label class="hru label">A:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100" name="contentOA">').val(content[0]))).append($('<label class="hru label">').text("(80% ~ 100%)"))));
											$tbody.append($tr3.append($('<td>')));
											$tbody.append($tr3.append($('<td colspan="6"><label class="hru label">B:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100" name="contentOB">').val(content[1]))).append($('<label class="hru label">').text("(60% ~ 80%)"))));
											$tbody.append($tr4.append($('<td>')));
											$tbody.append($tr4.append($('<td colspan="6"><label class="hru label">C:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100" name="contentOC">').val(content[2]))).append($('<label class="hru label">').text("(40% ~ 60%)"))));
											$tbody.append($tr5.append($('<td>')));
											$tbody.append($tr5.append($('<td colspan="6"><label class="hru label">D:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100" name="contentOD">').val(content[3]))).append($('<label class="hru label">').text("(20% ~ 40%)"))));
											$tbody.append($tr6.append($('<td>')));
											$tbody.append($tr6.append($('<td colspan="6"><label class="hru label">E:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100" name="contentOE">').val(content[4]))).append($('<label class="hru label">').text("(0% ~ 20%)"))));
											
											$("#evalTable").append($tbody);
											$(".ui.dropdown").dropdown();
										}
									}else{
										if(e.approvalId == 0){
											$("#codeArea").append($('<input type="hidden" name="evalCode">').val(e.evalCode));
										}
										
										if(e.category == 1){
											if(e.approvalId != 0){
												$("#add").addClass("disabled");
												$("#remove").addClass("disabled");
												$("#evalCategory").addClass("disabled");
												$("#store").addClass("disabled");
												$("#approv").addClass("disabled");
												$tbody.append($tr1.append($('<th rowspan="2" style="width:150px;">').text("양방향평가")));
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 카테고리 : "))));
												switch(e.type){
													case 1 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled"><option value="1" selected>업적</option><option value="2">능력</option><option value="3">태도</option></select>'))); break;
													case 2 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled"><option value="1">업적</option><option value="2" selected>능력</option><option value="3">태도</option></select>'))); break;
													case 3 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled"><option value="1">업적</option><option value="2">능력</option><option value="3" selected>태도</option></select>'))); break;
												}
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 항목명 : "))));
												$tbody.append($tr1.append($('<td>').append($('<div class="ui input small disabled">').append($('<input type="text" size="50">').val(e.evalName)))));
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 가중치 : "))));
												$tbody.append($tr1.append($('<td>').append($('<div class="ui input small disabled">').append($('<input type="number" size="10" min="0" max="100">').val(e.evalWeight)))));
												$tbody.append($tr2.append($('<td>').append($('<label class="hru label">').text("* 설명 : "))));
												$tbody.append($tr2.append($('<td colspan="6">').append($('<div class="ui input small disabled">').append($('<input type="text" size="120">').val(e.evalContent)))));
												
												$("#evalTable").append($tbody);
												$(".ui.dropdown").dropdown();
											} else {
												$tbody.append($tr1.append($('<th rowspan="2" style="width:150px;">').text("양방향평가")));
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 카테고리 : "))));
												switch(e.type){
													case 1 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown" name="typeB"><option value="1" selected>업적</option><option value="2">능력</option><option value="3">태도</option></select>'))); break;
													case 2 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown" name="typeB"><option value="1">업적</option><option value="2" selected>능력</option><option value="3">태도</option></select>'))); break;
													case 3 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown" name="typeB"><option value="1">업적</option><option value="2">능력</option><option value="3" selected>태도</option></select>'))); break;
												}
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 항목명 : "))));
												$tbody.append($tr1.append($('<td>').append($('<div class="ui input small">').append($nameB))));
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 가중치 : "))));
												$tbody.append($tr1.append($('<td>').append($('<div class="ui input small">').append($weightB))));
												$tbody.append($tr2.append($('<td>').append($('<label class="hru label">').text("* 설명 : "))));
												$tbody.append($tr2.append($('<td colspan="6">').append($('<div class="ui input small">').append($contentB))));
												
												$("#evalTable").append($tbody);
												$(".ui.dropdown").dropdown();
											}
										}else{
											var content = (e.evalContent).split("#");
											if(e.approvalId != 0){
												$("#add").addClass("disabled");
												$("#remove").addClass("disabled");
												$("#evalCategory").addClass("disabled");
												$("#store").addClass("disabled");
												$("#approv").addClass("disabled");
												$tbody.append($tr1.append($('<th rowspan="6" style="width:150px;">').text("단방향평가")));
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 카테고리 : "))));
												switch(e.type){
													case 1 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled"><option value="1" selected>업적</option><option value="2">능력</option><option value="3">태도</option></select>'))); break;
													case 2 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled"><option value="1">업적</option><option value="2" selected>능력</option><option value="3">태도</option></select>'))); break;
													case 3 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown disabled"><option value="1">업적</option><option value="2">능력</option><option value="3" selected>태도</option></select>'))); break;
												}
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 항목명 : "))));
												$tbody.append($tr1.append($('<td>').append($('<div class="ui input small disabled">').append($('<input type="text" size="50">').val(e.evalName)))));
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 가중치 : "))));
												$tbody.append($tr1.append($('<td>').append($('<div class="ui input small disabled">').append($('<input type="number" size="10" min="0" max="100">').val(e.evalWeight)))));
												$tbody.append($tr2.append($('<td>').append($('<label class="hru label">').text("* 평가지표 : "))));
												$tbody.append($tr2.append($('<td colspan="6"><label class="hru label">A:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100">').val(content[0]))).append($('<label class="hru label">').text("(80% ~ 100%)"))));
												$tbody.append($tr3.append($('<td>')));
												$tbody.append($tr3.append($('<td colspan="6"><label class="hru label">B:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100">').val(content[1]))).append($('<label class="hru label">').text("(60% ~ 80%)"))));
												$tbody.append($tr4.append($('<td>')));
												$tbody.append($tr4.append($('<td colspan="6"><label class="hru label">C:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100">').val(content[2]))).append($('<label class="hru label">').text("(40% ~ 60%)"))));
												$tbody.append($tr5.append($('<td>')));
												$tbody.append($tr5.append($('<td colspan="6"><label class="hru label">D:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100">').val(content[3]))).append($('<label class="hru label">').text("(20% ~ 40%)"))));
												$tbody.append($tr6.append($('<td>')));
												$tbody.append($tr6.append($('<td colspan="6"><label class="hru label">E:</label>').append($('<div class="ui input small disabled">').append($('<input type="text" size="100">').val(content[4]))).append($('<label class="hru label">').text("(0% ~ 20%)"))));
												
												$("#evalTable").append($tbody);
												$(".ui.dropdown").dropdown();
											} else {
												$tbody.append($tr1.append($('<th rowspan="6" style="width:150px;">').text("단방향평가")));
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 카테고리 : "))));
												switch(e.type){
													case 1 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown" name="typeO"><option value="1" selected>업적</option><option value="2">능력</option><option value="3">태도</option></select>'))); break;
													case 2 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown" name="typeO"><option value="1">업적</option><option value="2" selected>능력</option><option value="3">태도</option></select>'))); break;
													case 3 : $tbody.append($tr1.append($('<td>').append('<select class="ui compact dropdown" name="typeO"><option value="1">업적</option><option value="2">능력</option><option value="3" selected>태도</option></select>'))); break;
												}
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 항목명 : "))));
												$tbody.append($tr1.append($('<td>').append($('<div class="ui input small">').append($nameO))));
												$tbody.append($tr1.append($('<td>').append($('<label class="hru label">').text("* 가중치 : "))));
												$tbody.append($tr1.append($('<td>').append($('<div class="ui input small">').append($weightO))));
												$tbody.append($tr2.append($('<td>').append($('<label class="hru label">').text("* 평가지표 : "))));
												$tbody.append($tr2.append($('<td colspan="6"><label class="hru label">A:</label>').append($('<div class="ui input small">').append($('<input type="text" size="100" name="contentOA">').val(content[0]))).append($('<label class="hru label">').text("(80% ~ 100%)"))));
												$tbody.append($tr3.append($('<td>')));
												$tbody.append($tr3.append($('<td colspan="6"><label class="hru label">B:</label>').append($('<div class="ui input small">').append($('<input type="text" size="100" name="contentOB">').val(content[1]))).append($('<label class="hru label">').text("(60% ~ 80%)"))));
												$tbody.append($tr4.append($('<td>')));
												$tbody.append($tr4.append($('<td colspan="6"><label class="hru label">C:</label>').append($('<div class="ui input small">').append($('<input type="text" size="100" name="contentOC">').val(content[2]))).append($('<label class="hru label">').text("(40% ~ 60%)"))));
												$tbody.append($tr5.append($('<td>')));
												$tbody.append($tr5.append($('<td colspan="6"><label class="hru label">D:</label>').append($('<div class="ui input small">').append($('<input type="text" size="100" name="contentOD">').val(content[3]))).append($('<label class="hru label">').text("(20% ~ 40%)"))));
												$tbody.append($tr6.append($('<td>')));
												$tbody.append($tr6.append($('<td colspan="6"><label class="hru label">E:</label>').append($('<div class="ui input small">').append($('<input type="text" size="100" name="contentOE">').val(content[4]))).append($('<label class="hru label">').text("(0% ~ 20%)"))));
												
												$("#evalTable").append($tbody);
												$(".ui.dropdown").dropdown();
											}
										}
									}
									
								});
							}else{
								$("#evalTable").append($('<tbody style="text-align:center" id="noItem"><tr><td style="padding:150px"><h1 style="color:lightgray"><label><i class="exclamation circle icon"/>검색결과가 없습니다.</label></h1></td></tr>'));
							}						
						},
						error:function(status){
							console.log("실패!");
						}
					});
					if(year != new Date().getFullYear()){
						$("#addArea").addClass("hide");
						$("#btnArea").addClass("hide");
					}else{
						$("#addArea").removeClass("hide");
						$("#btnArea").removeClass("hide");	
					}

				}
				
			});
			
			$("#add").click(function(){
				var num = $('#evalCategory').find("input[type=hidden]").val();
				$('#noItem').remove();
				if(num == 1){
					$('#evalTable').append($('#prepareTable1').html());
				} else if(num ==2){
					$('#evalTable').append($('#prepareTable2').html());
				} else {
					alert("카테고리를 선택해주세요!");
				}
				$(".ui.dropdown").dropdown();
				
			});
			
			$("#store").click(function(){
				var queryString = $("#evalForm").serializeArray() ;
				
				$.ajax({
					url:"insertEvaluateList.hr",
					type:"POST",
					data:queryString,
					dataType : 'json',
					success:function(data){
						Swal.fire({
							  icon: 'success',
							  title: data.text
						})
						
						$("#codeArea").empty();
						var evalList = data.evalList;
						if(evalList.length > 0){
							$.each(evalList, function(index, e){
								$("#codeArea").append($('<input type="hidden" name="evalCode">').val(e.evalCode));
							});
						}
					},
					error:function(status){
						console.log("실패");
					}
				});
			});

			$('#request').click(function(){
				$("#title2").val($("#title").val());
				$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
				$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
				$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
				var queryString = $("#evalForm").serializeArray() ;
				
				$.ajax({
					url:"insertEvalApproval.hr",
					type:"POST",
					data:queryString,
					dataType : 'json',
					success:function(data){
						Swal.fire({
							  icon: data.icon,
							  title: data.text
						}).then((result) => {
							var content = "결재요청 : " + $("#title").val();
							var tmid = $("#firstApp").find("input[type=hidden]").val();
							if(tmid!="" && tmid!=null && tmid!=0){
								send(tmid, content);
							}
							$("#approvalModal").modal('hide');
							$("#add").addClass("disabled");
							$("#remove").addClass("disabled");
							$("#evalCategory").addClass("disabled");
							$("#store").addClass("disabled");
							$("#approv").addClass("disabled");
						}) 
					},
					error:function(status){
						console.log("실패");
					}
				});
			});
			
			
			$("#remove").click(function(){
				$('#evalTable').find("tbody").filter(":last").remove();
			});
		});
		
		function approvalFunction(){
			var approvalCode = 4;
			$("#attachDiv").addClass("disabled");
			
			
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					
					$("#appType").val("평가기준등록");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thirdName = data.appBasic.mlist[2].mName;
									var thirdMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
				},
				error : function(status){
					console.log(status);
				}
			});
			
			
			return false;
		}
	</script>
</body>
</html>