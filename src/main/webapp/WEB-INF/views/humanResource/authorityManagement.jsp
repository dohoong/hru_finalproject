<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<h1>담당자설정</h1>
		<div class="ui divider"></div>
			<div class="hru segment" style="height:740px;">
				<div class="scrollArea" style="width: 100%; height:300px;">
					<table class="hru scroll table">
						<thead>
							<tr>
								<th>사업장</th>
								<th>부서코드</th>
								<th>부서</th>
								<th>조직책임자</th>
								<th>인사담당자</th>
								<th>급여담당자</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${ !empty aulist }">
							<c:forEach var="a" begin="1" end="${aulist.size()-1}">
								<tr class="scroll-tr">
									<td><c:out value="${aulist.get(a).get('groupName')}"/></td>
									<td><c:out value="${aulist.get(a).get('teamCode')}"/></td>
									<td><c:out value="${aulist.get(a).get('teamName')}"/></td>
									<td><c:if test="${!empty aulist.get(a).get('lmName')}"><c:out value="${aulist.get(a).get('lmName')}"/></c:if>
									<input type="hidden" value="${aulist.get(a).get('lmid')}">
									</td>
									<td><c:if test="${!empty aulist.get(a).get('hrmName')}"><c:out value="${aulist.get(a).get('hrmName')}"/></c:if>
									<input type="hidden" value="${aulist.get(a).get('hrmid')}">
									</td>
									<td><c:if test="${!empty aulist.get(a).get('acmName')}"><c:out value="${aulist.get(a).get('acmName')}"/>
									<input type="hidden" value="${aulist.get(a).get('acmid')}">
									</c:if></td>
								</tr>
							</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
				<br>
				<br>
				<div class="editArea" style="width:40%; padding-right:0; margin-bottom:50px;">
					<form action="insertMember.au" method="post">
					<table class="hru table">
						<thead>
							<tr  class="table-title">
								<th colspan="3" class="table-title">수 정</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>사업장</th>
								<td colspan="2"><div class="ui disabled input">
									<input type="text" id="groupName" size="23" readonly></div></td>
							</tr>
							<tr>
								<th>부서명</th>
								<td><div class="ui disabled input">
									<input type="text" id="teamName" size="23" readonly></div>
									<input type="hidden" id="teamCode" name="teamCode"></td>
								<td><label class="hru label">부서장 :</label>
									<div class="ui disabled input">
									<input type="text" id="lmName" size="10" readonly></div>
								</td>
							</tr>
							<tr>
								<th>인사담당자</th>
								<td colspan="2">
									<select class="ui dropdown" id="hrmName" name="hrmid">
										<option value="">인사담당자</option>
										<c:forEach var="h" begin="0" end="${mlist.size()-1}">
											<option value="${mlist.get(h).get('mid')}"><c:out value="${mlist.get(h).get('mName')}"/></option>
										</c:forEach>
									</select></td>
							</tr>
							<tr>
								<th>급여담당자</th>
								<td colspan="2"><div class="ui input"><input type="hidden" id="acmid" name="acmid">
									<input type="text" id="acmName" size="23" readonly>
									<input type="button" class="ui blue button small" id="search" value="사원검색" style="margin-left:20px;"></div></td>
							</tr>
						</tbody>
					</table>
					<div class="buttonArea">
						<button type="submit" class="ui blue button mini" id="modify">수정</button>
						<button type="reset" class="ui gray button mini" id="cancel">취소</button>
					</div>
					</form>
				</div>
			</div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<div class="ui modal" id="searchMember">
		<i class="close icon"></i>
		<div class="header" align="center">
			<h2>사원 검색</h2>
		</div>
		<div class="searchArea" style="margin:20px; width:95%; height:100px; padding:20px;">
				<table align="center" cellspacing="10px">
					<tr>
						<td><h3>검색어</h3></td>
						<td><select class="ui dropdown" id="scategory">
							<option value="1">사원명</option>
							<option value="2">부서명</option>
						</select></td>
						<td>
							<div class="ui icon input">
								<input type="text" size="40" placeholder="Search..." id="stext"><i class="search icon"></i>
							</div>
						</td>
						<td>
							<button class="ui blue button" id="goSearch">검색하기</button>
						</td>
					</tr>
				</table>
		</div><!-- searchArea end -->
		<div class="hru segment" style="text-align:center;">
		<div class="scrollArea searchResult" style="width:98%; height:250px;">
			<table class="hru scroll table" id="resultTable">
				<thead>
					<tr>
						<th width="100px;">사번</th>
						<th>사원이름</th>
						<th>부서</th>
						<th>직급</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		</div>
	</div>
	<script>
		$(function(){
			$("#hr2").show();
			$(".ui.dropdown").dropdown();
			
			$(".scroll-tr").click(function(){
				$("#groupName").val($(this).children().eq(0).text());
				$("#teamName").val($(this).children().eq(2).text());
				$("#teamCode").val($(this).children().eq(1).text());
				$("#lmName").val($(this).children().eq(3).text());
				$("#hrmName").val($(this).children().eq(4).find("input[type=hidden]").val());
				$("#acmName").val($(this).children().eq(5).text());
				$("#acmid").val($(this).children().eq(5).find("input[type=hidden]").val());
				$(".ui.dropdown").dropdown();

			});
			
			$("#search").click(function(){
				$("#searchMember").modal('show');
			});
			
			
			$("#goSearch").click(function(){
				var category = $("#scategory").val();
				var text = $("#stext").val();
				
				//console.log("category : " +category);
				//console.log("text : " +text);
				
				$.ajax({
					url: "search.me",
					type: "GET",
					data: {
						category:category,
						text:text
					},
					success:function(data){
						console.log(data);
						
						$tbody = $("#resultTable tbody");
						$tbody.html('');
						
						for(var i=0; i<data.mlist.length; i++){

							var $td1 = $("<td>").text(data.mlist[i].mid);
							var $td2 = $("<td>").text(data.mlist[i].mName);
							var $td3 = $("<td>").text(data.mlist[i].tName);
							var $td4 = $("<td>").text(data.mlist[i].pName);
							
							$tbody.append($("<tr class='mclick'>").append($td1).append($td2).append($td3).append($td4));
							
							
							$(".mclick").click(function(){
								var mid = $(this).children().eq(0).text();
								var mName = $(this).children().eq(1).text();
								//console.log(mid + ", " + mName);
								
								$("#acmName").val(mName);
								$("#acmid").val(mid);
								$("#searchMember").modal('hide');
							});
						}
						
						
					},
					error:function(status){
						console.log(status);
					}
				});
			});
			
		});
	</script>
</body>
</html>