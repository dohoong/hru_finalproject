<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	.searchArea{
		height:80px;
		padding-top:10px;
	}
	.hru.board2 tbody>tr:hover{
		cursor:pointer;
		background:#eee;
	}
	.year-info{
		float:right;
	}
</style>
</head>
<body>
<jsp:include page="../common/menubar.jsp"/>

<div class="outer"><!--outer Area -->
<div class="contentArea"> <!-- 내용부분! -->
	<div class="hru segment">
		<h1>조직 관리</h1>
		
	</div><!-- 검색영역 끝 -->
			
			<div class="hru segment">
				<table class="hru board2" border="1">
					<thead>
						<tr>
							<th style="width:200px;">부서코드</th>
							<th style="width:200px;">사업장</th>
							<th style="width:300px;">부서명</th>
							<th style="width:200px;">부서장</th>
							<th style="width:200px;">인사담당자</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${!empty tllist}"/>
						<c:forEach var="c" begin="0" end="${ tllist.size() -1}">
							<tr id="hrTeam" onclick="location.href='showOrganizationManagement.me?teamCode=${ tllist.get(c).get('teamCode') }'" >
								<td><c:out value="${ tllist.get(c).get('teamCode') }"/></td>
								<td><c:out value="${ tllist.get(c).get('groupName') }"/></td>
								<td><c:out value="${ tllist.get(c).get('teamName') }"/></td>
								<td><c:if test="${!empty tllist.get(c).get('lmName')}"><c:out value="${tllist.get(c).get('lmName')}"/></c:if>
									<input type="hidden" value="${tllist.get(c).get('lmid')}"></td>
								<td><c:if test="${!empty tllist.get(c).get('hrmName')}"><c:out value="${tllist.get(c).get('hrmName')}"/></c:if>
									<input type="hidden" value="${tllist.get(c).get('hrmid')}"></td>
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
</div>
</div>

	<script>
		$(function() {
			$("#hr2").show();
			$(".ui.compact.dropdown").dropdown();
			});

/* 	 	function infoTeam(tcode){
			$.ajax({
				url:"showTeamList.au",
				type:"POST",
				data:{'tcode':tcode},
				success:function(data){
					console.log(data);
					var access = data.access;
					if(access == "Y"){
						location.href ="showOrganizationManagement.me?teamCode=" + tcode;
					}else{
						Swal.fire(
							'접근불가'
						)
						
					}
				}
			
			})
			
		}  */
		
		
		
	</script>
</body>
</html>