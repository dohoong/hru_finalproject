<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<%-- <jsp:include page="../common/menubar.jsp"/> --%> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea" style="height:500px;"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<button class="ui button" id="confirm">확인하기!</button>
		<select class="ui dropdown" id="testSelect">
			<option value="">선택해주세요</option>
			<option value="1">1번 선택지</option>
			<option value="2">2번 선택지</option>
			<option value="3">3번 선택지</option>
		</select>
		
		
		
		
		
		
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function(){
			$('.ui.dropdown').dropdown();
			
			$("#confirm").click(function(){
				console.log("들어옴!");
				$("#testSelect option:eq(1)").prop("selected", true);
				$('.ui.dropdown').dropdown();
			});
		});
	</script>
</body>
</html>