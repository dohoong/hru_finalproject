<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<!-- <script src="resources/js/demo.js" type="module"></script> -->
<script src="resources/js/countUp.min.js"></script>
<style>
	.hru.segment{
		text-align:center;
	}
	.ui.huge.statistic{
		width:400px;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/><!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment">
			<!-- <!-- <div class="ui statistics"> -->
				<div class="ui huge statistic">
					<div class="label" style="color:gray;">사원수</div>
					<div class="value" id="myTargetElement"></div>
				</div>
				<div class="ui huge statistic">
					<div class="label">입사자수</div>
					<div class="value" style="color:blue;" id="myTargetElement2"></div>
				</div>
				<div class="ui huge statistic">
					<div class="label">퇴사자수</div>
					<div class="value" style="color:red;" id="myTargetElement3"></div>
				</div>
			</div>
	

<script>
	var options = {
		useEasing : true, 
		useGrouping : true, 
		separator : ',', 
		decimal : '.', 
		prefix : '', 
		suffix : '' 
	};
	var demo1 = new CountUp("myTargetElement", 0, 1354462, 0, 2.5, options);
	var demo2 = new CountUp("myTargetElement2", 0, 130, 0, 2.5, options);
	var demo3 = new CountUp("myTargetElement3", 0, 46, 0, 2.5, options);
	demo1.start();
	demo2.start();
	demo3.start();
</script>



		</div>
		
		
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
</body>
</html>