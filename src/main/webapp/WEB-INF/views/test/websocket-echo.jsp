<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.4.0/sockjs.min.js"></script>
<style>
	.socketArea{
		border:1px solid gray;
		border-radius:5px;
		margin-top:50px;
		width:500px;
		height:300px;
	}
	.alermArea{
		display:inline-block;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<%-- <label class="hru label">내 정보 : </label><div class="ui input"><input type="text" readonly id="userName" value="${sessionScope.loginUser.mName}"></div> --%>
		<label class="hru label">쪽지보낼 사람 : </label><div class="ui input"><input type="text" id="search"></div>
		<input type="hidden" id="tmid">
		<br><br>
		<div class="ui input"><input type="text" id="messageinput" size="50"></div>
		<!-- <input type="button" class="ui button" id="sendBtn" value="전송"/> -->
        
        <!-- <button class="ui button" onclick="openSocket();">Open</button> -->
        <button class="ui button" onclick="sendText()">Send</button>
        <!-- <button class="ui button" onclick="closeSocket();">close</button>
		<div class="alermArea"><i class="alarm large icon"></i></div> -->
		
		<div id="message" class="socketArea"></div>
		
		
		
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	
	<div class="ui modal" id="searchMember">
		<i class="close icon"></i>
		<div class="header" align="center">
			<h2>사원 검색</h2>
		</div>
		<div class="searchArea" style="margin:20px; width:95%; height:100px; padding:20px;">
				<table align="center" cellspacing="10px">
					<tr>
						<td><h3>검색어</h3></td>
						<td><select class="ui dropdown" id="scategory">
							<option value="1">사원명</option>
							<option value="2">부서명</option>
						</select></td>
						<td>
							<div class="ui icon input">
								<input type="text" size="40" placeholder="Search..." id="stext"><i class="search icon"></i>
							</div>
						</td>
						<td>
							<button class="ui blue button" id="goSearch">검색하기</button>
						</td>
					</tr>
				</table>
		</div><!-- searchArea end -->
		<div class="hru segment" style="text-align:center;">
		<div class="scrollArea searchResult" style="width:98%; height:250px;">
			<table class="hru scroll table" id="resultTable">
				<thead>
					<tr>
						<th width="100px;">사번</th>
						<th>사원이름</th>
						<th>부서</th>
						<th>직급</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		</div>
	</div>
	<script>
		$(function(){
			$(".ui.dropdown").dropdown();

			$("#sendMessage").click(function(){
				send(2, $("#messageinput").val());
				$("#messageinput").val("");
			});
			

			$("#search").click(function(){
				$("#searchMember").modal('show');
			});
			
			
			$("#goSearch").click(function(){
				var category = $("#scategory").val();
				var text = $("#stext").val();
				
				//console.log("category : " +category);
				//console.log("text : " +text);
				
				$.ajax({
					url: "search.me",
					type: "GET",
					data: {
						category:category,
						text:text
					},
					success:function(data){
						//console.log(data);
						
						$tbody = $("#resultTable tbody");
						$tbody.html('');
						
						for(var i=0; i<data.mlist.length; i++){

							var $td1 = $("<td>").text(data.mlist[i].mid);
							var $td2 = $("<td>").text(data.mlist[i].mName);
							var $td3 = $("<td>").text(data.mlist[i].tName);
							var $td4 = $("<td>").text(data.mlist[i].pName);
							
							$tbody.append($("<tr class='mclick'>").append($td1).append($td2).append($td3).append($td4));
							
							
							$(".mclick").click(function(){
								var mid = $(this).children().eq(0).text();
								var mName = $(this).children().eq(1).text();
								//console.log(mid + ", " + mName);
								
								$("#search").val(mName);
								$("#tmid").val(mid);
								$("#searchMember").modal('hide');
							});
						}
						
						
					},
					error:function(status){
						console.log(status);
					}
				});
			});
		});
		
		function sendText(){
			var tmid = $("#tmid").val();
			var content = "쪽지 : " + $("#messageinput").val() + " / ${sessionScope.loginUser.mName}"; 
			$("#messageinput").val("");
			send(tmid, content);
		}
	</script>
	
	 <%-- <script>
        var ws;
        var messages = document.getElementById("message");
        var cnt = 1;
        var wsAddress = "ws://";
        var mid = "${loginUser.mid}";
       	$(function(){
       		var link = document.location.href.split('/');
       		wsAddress += link[2] + "/hru/echo.so";
       		console.log('${contextPath}');
       	});
        
        function openSocket2(){
            if(ws!==undefined && ws.readyState!==WebSocket.CLOSED)
            {
                writeResponse("이미 연결중입니다.");
                return;
            } 
            
            //웹소켓 객체 만드는 코드
            ws = new WebSocket(wsAddress);
            
            ws.onopen=function(event){
                if(event.data===undefined) return;
                writeResponse(event.data);
            };
            ws.onmessage=function(event){
                writeResponse(event.data);
            };
            ws.onclose=function(event){
                writeResponse("Connection closed");
            }
        }
        function send2(){
            var inputText = document.getElementById("messageinput");
            var name = $("#userName").val();
            var sendMessage = {
            	    type: "message",
            	    text: inputText.value,
            	    smid: mid,
            	    name: name,
            	    date: Date.now()
            	  };
			ws.send(JSON.stringify(sendMessage));
            inputText.value="";
        }
        function closeSocket2(){
            ws.close();
        }
        function writeResponse2(text){
        	var takeMessage = JSON.parse(text);
        	var time = new Date(takeMessage.date);
			var tformat = moment(time).format('YYYY-MM-DD HH:mm');
			
        	var tmid = takeMessage.tmid;
        	//var tname = takeMessage.name;
        	var tmessage = takeMessage.text + "(" + tformat + "): ";
        	
        	if(mid==tmid && text != "Connection closed"){ //받는사람이 나일때
	        	$("#alarm").css("color", "red").transition('tada').transition('tada').transition('tada').transition('tada');
	        	$("#circle").css("visibility", "visible").text(cnt++);
	        	$('body')
				  .toast({
				    displayTime: 3000,
				    title: '메시지가 도착했습니다.',
				    message: tmessage
				  });
        	}
        	
            message.innerHTML+="<br/>"+ tmessage;
        }
        function readAlarm2(){
        	$("#alarm").css("color", "gray").transition('stop');
        	$("#circle").css("visibility", "hidden").text("");
        	cnt = 1;
        }
        
    </script> --%>
</body>
</html>








