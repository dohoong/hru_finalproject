<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<h1>HELLO EXCEL WORLD!</h1>
		<form id="form1" name="form1" method="post" enctype="multipart/form-data">
			<input type="file" id="fileInput" name="fileInput" style="display:inline-block"><br>
			<button class="ui blue button" type="button" onclick="doExcelUploadProcess();">엑셀업로드 작업</button>
			<button class="ui button" type="button" onclick="doExcelDownladProcess();">엑셀다운로드 작업</button>
		</form>
		<div id="result">
		
		</div>
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script type="text/javascript">
		function doExcelUploadProcess(){
			var f = new FormData(document.getElementById('form1'));
			
			$.ajax({
				url : "uploadExcelFile.app",
				data : f,
				processData : false,
				contentType : false,
				type : "post",
				success : function(data){
					console.log(data);
					//$("#result").text("성공!");
					
				},
				error : function(status){
					console.log(status);
				}
			});
		}
		
		function doExcelDownladProcess(){
			var f = document.form1;
			f.action = "downloadExcelFile.app";
			f.submit();
		}
	</script>
</body>
</html>