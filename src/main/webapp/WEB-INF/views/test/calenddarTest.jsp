<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<link href='resources/js/calendar/core-main.min.css' rel='stylesheet' />
<link href='resources/js/calendar/daygrid-main.min.css' rel='stylesheet' />
<link href='resources/js/calendar/timegrid-main.min.css' rel='stylesheet' />
<script src='resources/js/calendar/core-main.min.js'></script>
<script src='resources/js/calendar/daygrid-main.min.js'></script>
<script src='resources/js/calendar/interaction-main.min.js'></script>
<script src='resources/js/calendar/timegrid-main.min.js'></script>
<style>
	#calendar{
		max-width:600px;
		display:inline-block;
		overflow:hidden;
	}
	.detail{
		display:inline-block;
		border:1px solid lightgray;
		padding:20px;
		margin:20px;
		margin-bottom:0;
		width:54%;
		overflow:hidden;
		height:444px;
	}
	.hru.segment{
		vertical-align:top;
		text-align:center;
	}
	.hru.table{
		text-align:left;
	}
	.hru.table th{
		width:120px;
	}
	.fc-event-container{
		height:60px;
	}
	.fc-event-container *{
		height:80%;
	}
	.fc-content{
		padding:5px;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<h1><i class="check square icon"></i>회사 일정</h1>
		<div class="ui divider"></div>
		<div class="hru segment">
			<div class="ui message">
				<ul class="list">
					<li>회사일정(법정휴일, 약정휴일)을 설정하실 수 있습니다.</li>
					<li>해당 일정이 사원 근태에 반영됩니다.</li>
				</ul>
			</div>
		</div>
		<div class="hru segment" style="display:none;">
			<div  id="calendar"> </div>
			<div class="detail">
				<form action="addAndChangeSch.app" method="post" id="detailScheduleForm">
				<input type="hidden" name="scheduleCode" id="code" value="0">
				<table class="hru table">
					<tr>
						<th>*일정제목</th>
						<td colspan="3"><div class="ui input"><input type="text" size="70" name="scheduleTitle" id="title"></div></td>
					</tr>
					<tr>
						<th>*시작일</th>
						<td>
							<div class="ui calendar" id="startDay">
							<div class="ui input left icon"><i class="calendar icon"></i>
								<input type="text" placeholder="시작일" readonly name="startDate" id="start">
							</div>
						</div>
						</td>
						<th>종료일</th>
						<td>
							<div class="ui calendar" id="endDay">
							<div class="ui input left icon"><i class="calendar icon"></i>
								<input type="text" placeholder="종료일" readonly name="endDate" id="end">
							</div>
						</div>
						</td>
					</tr>
					<tr>
						<th>*일정종류</th>
						<td colspan="3">
							<select class="ui dropdown" name="category" id="category">
								<option value="1">회사일정(파랑)</option>
								<option value="2">약정휴일(초록)</option>
								<option value="3">법정휴일(빨강)</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>일정 상세</th>
						<td colspan="3">
							<div class="ui input" style="width:100%;">
								<textarea style="width:100%; height:150px; resize:none;" name="scheduleContent" id="content"></textarea>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="border-bottom:white; text-align:right;">
							<button type="button" class="ui blue button" id="scheduleAdd">일정추가</button>
							<button type="button" class="ui button" id="scheduleChange">일정변경</button>
							<button type="button" class="ui basic button" id="scheduleDelete">일정삭제</button>
						</td>
					</tr>
				</table>
				</form>
			</div><!-- 상세일정 끝 -->
		</div><!-- segment끝 -->
		<div class="hru segment">
			<div  id="calendar2"></div>
		</div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function(){
			$(".ui.dropdown").dropdown();
			
			$('#startDay').calendar({ 
				type: 'date',
				formatter: {
				      date: function (date, settings) {
				        if (!date) return '';
				        var day = date.getDate();
				        if(day<10){day = "0" + day;}
				        var month = date.getMonth() + 1;
				        if(month<10){month = "0" + month;}
				        var year = date.getFullYear();
				        return year + '-' + month + '-' + day;
				      }
				},
				endCalendar : $('#endDay')
			});
			$('#endDay').calendar({ 
				type: 'date',
				formatter: {
				      date: function (date, settings) {
				        if (!date) return '';
				        var day = date.getDate();
				        if(day<10){day = "0" + day;}
				        var month = date.getMonth() + 1;
				        if(month<10){month = "0" + month;}
				        var year = date.getFullYear();
				        return year + '-' + month + '-' + day;
				      }
				},
				startCalendar : $('#startDay')
			});
			
			
			$("#scheduleAdd").click(function(){
				if($("#title").val()=="" || $("#start").val() == ""){
					Swal.fire({
						  icon: 'error',
						  title: 'Oops!',
						  text: '필수 기입사항을 채워주세요.'
					});	
				} else {
					$("#code").val(0);
					$("#detailScheduleForm").submit();					
				}
			});
			$("#scheduleChange").click(function(){
				if($("#code").val()=="" || $("#code").val()==0){
					Swal.fire({
						  icon: 'error',
						  title: 'Oops!',
						  text: '변경할 일정이 없습니다.'
					});
				} else if($("#title").val()=="" || $("#start").val() == ""){
					Swal.fire({
						  icon: 'error',
						  title: 'Oops!',
						  text: '필수 기입사항을 채워주세요.'
					});
					
				} else {
					$("#detailScheduleForm").submit();					
				}
			});
			$("#scheduleDelete").click(function(){
				if($("#code").val()=="" || $("#code").val()==0){
					Swal.fire({
						  icon: 'error',
						  title: 'Oops!',
						  text: '삭제할 일정이 없습니다.'
					});
				} else{
					var num = $("#code").val();
					location.href="deleteSchedule.app?scheduleCode=" + num;
				}
			});
			
		});
		document.addEventListener('DOMContentLoaded', function() {
			//var calendarEl = document.getElementById('calendar');
			var Calendar = FullCalendar.Calendar;
			var Draggable = FullCalendarInteraction.Draggable;
			 
			var calendarEl = document.getElementById('calendar');
			var calendarEl2 = document.getElementById('calendar2');

			var checkbox = document.getElementById('drop-remove');
			
			var calendar = new FullCalendar.Calendar(calendarEl, {
				plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
				defaultView: 'dayGridMonth',
				defaultDate: new Date(),
				header: {
					right: 'prev,next today',
					left: 'title',
					center : ''
				},
				editable : true,
				droppable : true,
				drop : function(info){
					console.log(info);
					if(checkbox.checked){
						info.draggedEl.parentNode.removeChild(info.graggedEl);
					}
				},
				contentHeight: 'auto',
				eventSources : [{
					events : function(info, successCallback, failureCallback){
						$.ajax({
							url : "selectCalendarAll.app",
							type : "post",
							dataType : 'json',
							data : {
								start : moment(info.startStr).format('YYYY-MM-DD'),
								end : moment(info.endStr).format('YYYY-MM-DD')
							},
							success : function(data){
								//console.log(data.jarr);
								successCallback(data.jarr);
							},
							error : function(status){
								failureCallback(status);
							}
						});
					}
				}],
				dateClick: function(info) {
				    //alert('Date: ' + info.dateStr);
					$("#start").val(info.dateStr);
					$("#title").val("");
					$("#end").val("");
					$("#code").val("");
					$("#content").text("");
					$("#category").val(1);
					$(".ui.dropdown").dropdown();
				},
				eventClick : function(info){
					//console.log(info.event.extendedProps);
					$("#title").val(info.event.title);
					$("#start").val(moment(info.event.start).format('YYYY-MM-DD'));
					//console.log(info.event.end - info.event.start);
					if(info.event.end - info.event.start>86400000){
						$("#end").val(moment(info.event.end).subtract(1, 'days').format('YYYY-MM-DD'));	
					} else{
						$("#end").val("");
					}
					$("#code").val(info.event.extendedProps.code);
					$("#content").text(info.event.extendedProps.content);
					$("#category").val(info.event.extendedProps.category);
					$(".ui.dropdown").dropdown();

				},
				eventDrop: function(info) { //드래그 앤 드롭하면 실행되는 함수
				    //alert(info.event.title + " was dropped on " + info.event.start.toISOString());
				    if (!confirm("일정을 변경하시겠습니까?")) {
				      info.revert();
				    } else{
				    	//console.log(info.event.start);
				    	//console.log(info.event.extendedProps.code);
				    	
				    	$.ajax({
				    		url:"updateSchedule.app",
				    		type:"POST",
				    		data:{
				    			scheduleCode : info.event.extendedProps.code,
				    			startDate : moment(info.event.start).format('YYYY-MM-DD'),
				    			endDate : moment(info.event.end).format('YYYY-MM-DD')
				    		},
				    		success : function(data){
								$("#start").val("");
								$("#title").val("");
								$("#end").val("");
								$("#code").val("");
								$("#content").text("");
								$("#category").val(1);
								$(".ui.dropdown").dropdown();
				    		},
				    		error : function(status){
				    			
				    		}
				    	});
				    }
				},
				eventResize: function(info) { //일정 크기를 변경하면 실행되는 함수
				   //alert(info.event.title + " end is now " + info.event.end.toISOString());

				    if (!confirm("일정을 변경하시겠습니까?")) {
				      info.revert();
				    } else{
				    	//console.log(info.event.start);
				    	//console.log(info.event.end);
				    	//console.log(info.event.extendedProps.code);
				    	
				    	$.ajax({
				    		url:"updateSchedule.app",
				    		type:"POST",
				    		data:{
				    			scheduleCode : info.event.extendedProps.code,
				    			startDate : moment(info.event.start).format('YYYY-MM-DD'),
				    			endDate : moment(info.event.end).format('YYYY-MM-DD')
				    		},
				    		success : function(data){
								$("#start").val("");
								$("#title").val("");
								$("#end").val("");
								$("#code").val("");
								$("#content").text("");
								$("#category").val(1);
								$(".ui.dropdown").dropdown();
				    		},
				    		error : function(status){
				    			
				    		}
				    	});
				    	
				    }
				  }
				
			});
			
			var calendar2 = new FullCalendar.Calendar(calendarEl2, {
				plugins: [ 'dayGrid', 'timeGrid' ],
				defaultView: 'dayGridWeek',
				defaultDate: new Date(),
				height : 300,
				header: {
					right: 'prev,next today',
					left: 'title',
					center : ''
				},
				eventSources : [{
					events : function(info, successCallback, failureCallback){
						$.ajax({
							url : "selectCalendarAll.app",
							type : "post",
							dataType : 'json',
							data : {
								start : moment(info.startStr).format('YYYY-MM-DD'),
								end : moment(info.endStr).format('YYYY-MM-DD')
							},
							success : function(data){
								//console.log(data.jarr);
								successCallback(data.jarr);
							},
							error : function(status){
								failureCallback(status);
							}
						});
					}
				}]
			});
			
			calendar2.render();
			calendar.render();
		});

	</script>
</body>
</html>