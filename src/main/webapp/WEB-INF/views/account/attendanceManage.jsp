<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	.searchArea{
		padding-top:7px;
	}
	#monthAt{
		background:#F5F5F5;
	}
	.atInput{
		width:100px;
		text-align:right;
	}
	#searchBox{
		margin-bottom: 10px;
	}
	.ui.input>.atInput{
		text-align:center;
		font-weight:bold;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
<div class="outer"><!--outer Area -->
<div class="contentArea"> <!-- 내용부분! -->

	<div class="hru segment" id="searchBox">
		<h1 style="color:black">월 근태생성 / 마감</h1>
				<div class="searchArea" style="height:70px;">
					<table align="center" cellspacing="10px">
						<tr>
							<td>
								<h3>근무일자 </h3>
							</td>
							<td>
								<div class="ui calendar" id="month_year_calendar">
									<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" name="workMonth" id="monthSelect" placeholder="근무년월 선택" readonly>
									</div>
								</div>
							<td>
							<td><h3>사업장 </h3></td>
							<td>
								<select class="ui dropdown group" id="groupSelect">
								<option value="">사업장</option>
								 <option value="">사업장</option>
									<c:forEach var="i" begin="0" end="${glist.size() - 1}">
										<option value="${glist[i].groupCode}"><c:out value="${glist[i].groupName}"/></option>
									</c:forEach>
								</select>
								<select class="ui dropdown team" name="teamCode" id="teamSelect">
								 <option value="">부서</option>
								</select>					
							</td>
							<td>
								<button class="ui blue button" id="searchButton">검색하기</button>
							</td>
						</tr>
					</table>
					
		</div><!-- searchArea end -->
		<div class="ui message">
					<ul class="list">
						<li>사업장과 부서, 근무 년월을 선택시, 해당 소속된 사원리스트가 표시됩니다.</li>
						<li>간단한 사원정보가 표시되고 사원을 눌렀을 때 그 사원의 월근태가 아래 목록에 표시됩니다.</li>
						<li>월 근태마감은 기준일로부터 5일동안 가능합니다.</li>
					</ul>
			</div>
			<input class="atInput" id="mid" type="hidden" readonly>
			<input class="atInput" id="mattDate" type="hidden" readonly>
			<input type="hidden" name="title" id="title2">
			<input type="hidden" name="firstApproval" id="firstApp2">
			<input type="hidden" name="secondApproval" id="secondApp2">
			<input type="hidden" name="thirdApproval" id="thirdApp2">
	</div>
		<!-- 월 근태 사원정보 -->
		<div class="hru segment 1">
		</div><!-- 테이블영역 끝! -->
	
		<!-- 월 근태 일수 -->
		<div class="hru segment 2">
		</div><!-- 월 근태 일수 끝 -->
		
		<!-- 월 근태 시간 -->
		<div class="hru segment 3">
		</div><!-- 월 근태 시간 끝 -->
	
</div>
</div>
<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
	<script>
		$(function() {
			$("#pay1").show();
			/* 드롭다운 */
			$(".ui.dropdown").dropdown();
			
			/* search Area 셀렉트박스 */
			$("#groupSelect").change(function(){
				$("#teamSelect").html("<option value=''>부서</option>");
				$("#teamSelect").parent().find(".text").text("부서");
				
				<c:forEach var="i" begin="0" end="${tlist.size()-1}">
				
				var teamCode = "${tlist[i].teamCode}";
				var teamName = "${tlist[i].teamName}";
				var groupCode = "${tlist[i].groupCode}";
				
				if(groupCode == $(this).val()){
					var $option = $("<option value='teamCode'>").text(teamName).val(teamCode);
					$("#teamSelect").append($option);
				}
			</c:forEach>
				$(".ui.dropdown team").dropdown();
			});
		});	

		/* 데이트피커 */
		$('#month_year_calendar').calendar({
		    type: 'month',
		    formatter: {
		    	date: function (date, settings) {
		    	      if (!date) return '';
		    	      var month = date.getMonth() + 1;
		    	      var month2;
		    	      var year = date.getFullYear();
		    	      if(month < 10){
		    	    	  month2 = '0' + month;
		    	      }else {
		    	    	  month2 = month;
		    	      }
		    	      return year + '-' + month2;
		    	}
		    },
		    text: {
		        monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
		    }
		  });
		
		/* 월 근태시간 수정버튼(readonly 비활성화) */
		function timeModify(){
			$("input[name='weekOver']").attr("readonly", false);
			$("input[name='weekNight']").attr("readonly", false);
			$("input[name='offWork']").attr("readonly", false);
			$("input[name='offExtended']").attr("readonly", false);
			$("input[name='offNight']").attr("readonly", false);
			$("input[name='late']").attr("readonly", false);
			$("input[name='early']").attr("readonly", false);
			$("input[name='total']").attr("readonly", false);
		};
		
		/* 검색 ajax */
		$("#searchButton").click(function(){
			$(".hru.segment.1").empty();
			if($("#teamSelect").val() == ""){
				Swal.fire(
						  '검색 불가',
						  '해당 조건으로 검색할 수 없습니다.',
						  'warning'
				)
			} else {
				var teamCode = $("#teamSelect").val();
				var groupSelect = $("#groupSelect").val();
				var monthSelect = $("#monthSelect").val();
				$(".hru.segment.2").empty();
				$(".hru.segment.3").empty();
				$.ajax({
					url:"selectGroupMember.att",
					type:"post",
					data:{'teamCode' : teamCode, 'groupSelect' : groupSelect,'monthSelect' : monthSelect},
					success:function(data){
						console.log(data);
						if(data.result == 1){
							Swal.fire(
									  '월 마감 이전',
									  '해당 월 근태 테이블이 형성되지 않았습니다.',
									  'warning'
							)
						}else if(data.result == 2){
							Swal.fire(
									  '데이터 미존재',
									  '해당 월 근태 정보가 존재하지 않습니다.',
									  'warning'
							)
						}
						
						var smList = data.smList;
						if(smList.length > 0){
							
							var $tbody = $("<tbody>");
							var $thead = $("<thead><tr><th>NO</th><th>사원번호</th><th>성명</th><th>부서</th><th>직급</th></tr></thead>");
							var $title = $("<h3><i class='check square icon'></i>월 근태 사원정보<button style='float:right; width:150px; height:30px; padding-bottom:22px;' class='ui linkedin button' id='monthAtBtn'><i class='share icon'></i>월 근태마감</button></h3>");
							var $table = $("<table class='hru board2' id='memberTable' border='1'>");
							
							$table.append($thead);
							$.each(smList, function(index, i){
								var $tr1 = $("<tr class='memberTr'>");
								$tbody.append($tr1.append($('<td>').append($('<label>').text(index + 1))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.mid))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.mName))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.teamName))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.positionName))));
								
								$table.append($tbody);
							});
							$(".hru.segment.1").append($title);
							$(".hru.segment.1").append($table);
							
							/* 월 근태마감 1~5일  */
							date = new Date();
							year = date.getFullYear();
							month = date.getMonth() + 1;
							day = date.getDate();
							day = 3;
							
							if((month+"").length <= 2){
								month = "0" + month;
							}
							var getToday = year + "-" + month + "-" + day;
							
							if((day >= 1) && (day <= 5)){
								var monthSelect = $("#monthSelect").val();
								var searchDate = new Date(monthSelect.substring(0,4), monthSelect.substring(5,7)-1, 01);
								var years = year - searchDate.getFullYear();
								var months = month - searchDate.getMonth();
								var days = day - searchDate.getDate();
								var diffrenceMonth = years*12 + months + (days >= 0 ? 0 : -1);
								if(diffrenceMonth == 2){
									$("#monthAtBtn").show();
								}else {
									$("#monthAtBtn").hide();
								}
								
							}else{
								$("#monthAtBtn").hide();
							}
						}
					},
					error:function(status){
						console.log("실패!");
					}
				});
			}
		});
		
		
		$(function(){
			$(document.body).delegate('.memberTr', 'mouseover', function() {
				var name = $(this).find("td").eq(2).text();
			});
			
			
			
			$(document.body).delegate('.memberTr', 'click', function() {
				$(this).css("background","#4988bf");
				var month = $("#monthSelect").val();
				var mid = $(this).find("td").eq(1).text();
				$("#mid").val(mid);
				$("#mattDate").val(month);
				
				/* tr 색상 */
				$it = $(this);
				$it.css({
					'background':'#ededed',
					'font-weight': 'bold'
				}),
				$(".memberTr").not($it).css({
					'background':'white',
					'font-weight': 'normal'
				});
				$(".hru.segment.2").empty();
				$(".hru.segment.3").empty();
				
				$.ajax({
					url:"selectMemberMA.att",
					type:"post",
					data:{'month':month, 'mid':mid},
					success:function(data){
						console.log(data);
						var ma = data.ma;
						
						if(ma != null){
							var $title = $("<h3 id='monthAttTitle'><i class='check square icon'></i>월 근태 일수</h3>");
							var $div = $("<div class='searchArea' style='height:70px;' id='monthAt'>");
							var $table = $("<table align='center' id='monthAttTable' cellspacing='10px'>");
							var $tr1 = $("<tr>");
							
							var $totalDay = $('<input class="atInput" id="totalDay" type="text" readonly>').val(ma.weekdayNum + ma.holidayNum);
							$weekdayNum = $('<input class="atInput" id="weekdayNum" type="text" readonly>').val(ma.weekdayNum);
							$holidayNum = $('<input class="atInput" id="holidayNum" type="text" readonly>').val(ma.holidayNum);
							$vacationNum = $('<input class="atInput" id="vacationNum" type="text" readonly>').val(ma.vacationNum);
							
							$table.append($tr1.append($('<td>').append($('<h3 style="color:#e32c10">').text("총 근무일수"))));
							$table.append($tr1.append($('<td>').append($('<div class="ui input">').append($totalDay))));
							$table.append($tr1.append($('<td>').append($('<h3>').text("평일 근무일수"))));
							$table.append($tr1.append($('<td>').append($('<div class="ui input">').append($weekdayNum))));
							$table.append($tr1.append($('<td>').append($('<h3>').text("휴일 근무일수"))));
							$table.append($tr1.append($('<td>').append($('<div class="ui input">').append($holidayNum))));
							$table.append($tr1.append($('<td>').append($('<h3>').text("휴가 사용일수"))));
							$table.append($tr1.append($('<td>').append($('<div class="ui input">').append($vacationNum))));
							
							
							$(".hru.segment.2").append($title);
							$(".hru.segment.2").append($div);
							$(".hru.segment.2").append("<div class='ui message'><li>총 근무일수는 평일 근무일수와 휴일 근무일수를 합산한 일수입니다.</li><li>휴가 사용일수는 연차 사용에 반영되는 일수입니다.</li></ul></div>");
							$($div).append($table);
							
							/* 야특근 근태 시간 */
							var $workMore = $('<input class="atInput" id="workMore" type="text" readonly>').val(ma.workMore);
							var $workNight = $('<input class="atInput" id="workNight" type="text" readonly>').val(ma.workNight);
							var $workHolidayOne = $('<input class="atInput" id="workHolidayOne" type="text" readonly>').val(ma.workHolidayOne);
							var $workHolidayTwo = $('<input class="atInput" id="workHolidayTwo" type="text" readonly>').val(ma.workHolidayTwo);
							var $totalTime = $('<input class="atInput" id="totalTime" type="text" readonly>').val(ma.workMore + ma.workNight + ma.workHolidayOne + ma.workHolidayTwo);
							
							var $table2 = $("<table align='center' id='monthAttTable' cellspacing='10px'>");
							var $stitle = $("<h3><i class='check square icon'></i>월 야특근 근태 시간</h3>");
							var $div2 = $("<div class='searchArea'  id='monthAt' style='height:70px;'>");
							
							var $tr2 = $("<tr>");
							
							$table2.append($tr2.append($('<td>').append($('<h3 style="color:#e32c10">').text("야특근시간 총합"))));
							$table2.append($tr2.append($('<td>').append($('<div class="ui input">').append($totalTime))));
							$table2.append($tr2.append($('<td>').append($('<h3>').text("평일 연장"))));
							$table2.append($tr2.append($('<td>').append($('<div class="ui input">').append($workMore))));
							$table2.append($tr2.append($('<td>').append($('<h3>').text("평일 야간"))));
							$table2.append($tr2.append($('<td>').append($('<div class="ui input">').append($workNight))));
							$table2.append($tr2.append($('<td>').append($('<h3>').text("휴일 연장"))));
							$table2.append($tr2.append($('<td>').append($('<div class="ui input">').append($workHolidayOne))));
							$table2.append($tr2.append($('<td>').append($('<h3>').text("휴일 야간"))));
							$table2.append($tr2.append($('<td>').append($('<div class="ui input">').append($workHolidayTwo))));
							
							$div2.append($table2);
							
							
							$(".hru.segment.3").append($stitle);
							$(".hru.segment.3").append($div2);
							$($div2).append("<div class='buttonArea'><button class='ui blue button mini' id='modify' type='button'>수정</button><button class='ui gray button mini' id='save' type='submit' style='display:none'>저장</button></div>");
							
						}
						
						
					},
					error:function(status){
						console.log("fail!");
					}
					
				});
			});
			
			/* 수정버튼 클릭 */
			$(document.body).delegate('#modify', 'click', function(){
				var mid = $("#mid").val()
				var mattDateString = $("#monthSelect").val();
				var mArray = mattDateString.split("-");
				var montt = mArray[0] + mArray[1];
				var mon = Number(montt);
				$.ajax({
					url:"maApprovalCheck.att",
					type:"post",
					data:{"mid":mid, "mon":mon},
					success:function(data){
						console.log(data);
						if(data.result == 1){
							Swal.fire(
								'전자결재 마감',
								'결재 마감된 근태정보입니다.',
								'info'
							)
						}else {
							$("#save").show();
							$("#weekdayNum").removeAttr("readonly"); 
							$("#weekdayNum").css("border-color","#4988bf");
							$("#holidayNum").removeAttr("readonly"); 
							$("#holidayNum").css("border-color","#4988bf");
							$("#workMore").removeAttr("readonly"); 
							$("#workMore").css("border-color","#4988bf");
							$("#workNight").removeAttr("readonly"); 
							$("#workNight").css("border-color","#4988bf");
							$("#workHolidayOne").removeAttr("readonly"); 
							$("#workHolidayOne").css("border-color","#4988bf");
							$("#workHolidayTwo").removeAttr("readonly"); 
							$("#workHolidayTwo").css("border-color","#4988bf");
							
							$("#vacationNum").css("border-color","#e3897d");
							$("#totalDay").css("border-color","#e3897d");
							$("#totalTime").css("border-color","#e3897d");
						}
					},
					error:function(status){
						console.log("fail");
					}
					
				});
				
			});
			/* 수정 완료 버튼 클릭 */
			$(document.body).delegate('#save', 'click', function(){
				var $form = $('<form></form>');
				$form.attr('action', "modifyMonthAttendnace.att");
				$form.attr('method', 'post');
				$form.attr('id', 'atModifyForm');
				$form.appendTo('body');
				
				var weekdayNum = $('<input class="atInput" id="weekdayNum2" name="weekdayNum" type="hidden" readonly>');
				var holidayNum = $('<input class="atInput" id="holidayNum2" name="holidayNum" type="hidden" readonly>');
				var workMore = $('<input class="atInput" id="workMore2" name="workMore" type="hidden" readonly>');
				var workNight = $('<input class="atInput" id="workNight2" name="workNight" type="hidden" readonly>');
				var workHolidayOne = $('<input class="atInput" id="workHolidayOne2" name="workHolidayOne" type="hidden" readonly>');
				var workHolidayTwo = $('<input class="atInput" id="workHolidayTwo2" name="workHolidayTwo" type="hidden" readonly>');
				var mid = $('<input class="atInput" id="mid2" name="mid" type="hidden" readonly>');
				var monttDate = $('<input class="atInput" id="monttDate2" name="mattDate" type="hidden" readonly>');
				
				var mattDateString = $("#monthSelect").val();
				var mArray = mattDateString.split("-");
				var montt = mArray[0] + mArray[1];
				var mon = Number(montt);
				
				$form.append(weekdayNum);
				$form.append(holidayNum);
				$form.append(workMore);
				$form.append(workNight);
				$form.append(workHolidayOne);
				$form.append(workHolidayTwo);
				$form.append(mid);
				$form.append(monttDate);
				
				$("#mid2").val(Number($("#mid").val()));
				$("#monttDate2").val(mon);
				$("#weekdayNum2").val(Number($("#weekdayNum").val()));
				$("#holidayNum2").val(Number($("#holidayNum").val()));
				$("#workMore2").val(Number($("#workMore").val()));
				$("#workNight2").val(Number($("#workNight").val()));
				$("#workHolidayOne2").val(Number($("#workHolidayOne").val()));
				$("#workHolidayTwo2").val(Number($("#workHolidayTwo").val()));
				//$form.submit();
				
				
				/* ajax */
				var monthAttendString = $("#atModifyForm").serialize() ;
				$.ajax({
					type:'post',
					url:"modifyMonthAttendnace.att",
					data:monthAttendString,
					success:function(data){
						console.log(data);
						if(data.result == 1){
							Swal.fire(
									  '수정 완료',
									  '월 근태 수정을 완료했습니다.',
									  'success'
							)
						}
					},
					error:function(status){
						console.log("fail!");
					}
					
				});
			});
		});
		
		
		/* 월 근태 마감 */
		$(document).on('click', '#monthAtBtn', function(){
		
			var approvalCode = 5; //월근태마감에 대한 결재코드
			//console.log("전자결재");
			
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					
					$("#appType").val("월 근태마감");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thirdName = data.appBasic.mlist[2].mName;
									var thirdMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
				},
				error : function(status){
					console.log(status);
				}
			});
			
			
			return false;
		});
		
		$(function(){
			
			$('#request').click(function(){
				
				var title = $("#title").val();
				var firstApp = $("#firstApp").find("input[type=hidden]").val();
				var secondApp = $("#secondApp").find("input[type=hidden]").val();
				var thirdApp = $("#thirdApp").find("input[type=hidden]").val();
				var month = $("#monthSelect").val();
				var teamCode = $("#teamSelect").val();
					/* $('#vacationRequestForm').submit(); */
					
					/* 월근태마감 AJAX */
					
					$.ajax({
 						url:"insertMonthAttendanceRequest.att",
						type:'post',
						data:{"title":title, "firstApp":firstApp, "secondApp":secondApp, "thirdApp":thirdApp, "month":month, "teamCode":teamCode},
						success:function(data){
							console.log(data);
							if(data.result >= 1){
								Swal.fire(
										  '전자결재 완료',
										  '월근태 전자결재 처리를 완료했습니다.',
										  'success'
								).then((result)=>{
									$("#approvalModal").modal('hide');
								});
								var content = "결재요청 : " + $("#title").val();
								var tmid = $("#firstApp").find("input[type=hidden]").val();
								if(tmid!="" && tmid!=null && tmid!=0){
									send(tmid, content);
								}
							}else{
								Swal.fire(
										  '전자결재 실패',
										  '월근태 전자결재 처리가 취소되었습니다.',
										  'warning'
								).then((result)=>{
									$("#approvalModal").modal('hide');
								});
							}
						},
						error:function(status){
							console.log("실패!");
						}
					})
				
				
				
			});
		})
		
		
		
	</script>
	
	
</body>
</html>