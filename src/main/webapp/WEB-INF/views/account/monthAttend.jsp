<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	#searchBox{
		padding-top:7px;
	}
	#attend{
		color:#387bb5;
	}
	#late{
		color:#c9485b;
	}
	#special{
		color:#fddb3a;	
	}
	#vacation{
		color:#91bd3a;
	}
</style>
</head>
<body>
<jsp:include page="../common/menubar.jsp"/>
<div class="outer"><!--outer Area -->
<div class="contentArea" > <!-- 내용부분! -->
	<div class="hru segment">
		<h1 style="color:black">월 근태현황</h1>
				<div class="searchArea" id="searchBox" style="height:70px; margin-bottom:50px;">
					<table align="center" cellspacing="10px">
						<tr>
							<td>
								<h3>근무일자 </h3>
							</td>
							<td>
								<div class="ui calendar" id="month_year_calendar">
									<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" name="workMonth" id="monthSelect" placeholder="근무년월 선택" readonly>
									</div>
								</div>
							<td>
							<td><h3>사업장 </h3></td>
							<td>
								<select class="ui dropdown group" id="groupSelect">
								<option value="">사업장</option>
								<c:forEach var="i" begin="0" end="${glist.size() - 1}">
										<option value="${glist[i].groupCode}"><c:out value="${glist[i].groupName}"/></option>
									</c:forEach>
								</select>
								<select class="ui dropdown team" name="teamCode" id="teamSelect">
								 <option value="">부서</option>
								</select>					
							</td>
							<td>
								<button class="ui blue button" id="searchButton">검색하기</button>
							</td>
						</tr>
					</table>
			</div>
	</div>
	
	<div class="hru segment 2">
	</div><!-- <!-- 월 근태 테이블 끝-->
</div>
</div>
	<script>
		$(function(){
			$("#pay1").show();
			$(".ui.dropdown").dropdown();
			
			/* search Area 셀렉트박스 */
			$("#groupSelect").change(function(){
				$("#teamSelect").html("<option value=''>부서</option>");
				$("#teamSelect").parent().find(".text").text("부서");
				
				<c:forEach var="i" begin="0" end="${tlist.size()-1}">
				
				var teamCode = "${tlist[i].teamCode}";
				var teamName = "${tlist[i].teamName}";
				var groupCode = "${tlist[i].groupCode}";
				
				if(groupCode == $(this).val()){
					var $option = $("<option value='teamCode'>").text(teamName).val(teamCode);
					$("#teamSelect").append($option);
				}
			</c:forEach>
				$(".ui.dropdown team").dropdown();
			});
		});
		
		/* 데이트피커 */
		$('#month_year_calendar').calendar({
		    type: 'month',
		    formatter: {
		    	date: function (date, settings) {
		    	      if (!date) return '';
		    	      var month = date.getMonth() + 1;
		    	      var month2;
		    	      var year = date.getFullYear();
		    	      if(month < 10){
		    	    	  month2 = '0' + month;
		    	      }else {
		    	    	  month2 = month;
		    	      }
		    	      return year + '-' + month2;
		    	}
		    },
		    text: {
		        monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
		    }
		});
		
		/* 검색 AJAX */
		$("#searchButton").click(function(){
			$(".hru.segment.1").empty();
			$(".hru.segment").css("margin-bottom","0");
			$(".hru.segment").css("padding-bottom","0");
			if($("#teamSelect").val() == ""){
				Swal.fire(
						  '검색 불가',
						  '해당 조건으로 검색할 수 없습니다.',
						  'warning'
				)
			} else {
				var teamCode = $("#teamSelect").val();
				var groupSelect = $("#groupSelect").val();
				var monthSelect = $("#monthSelect").val();
				$(".hru.segment.2").empty();
				$.ajax({
					url:"selectTeamMonthAttendance.att",
					type:"post",
					data:{"teamCode":teamCode, "monthSelect":monthSelect},
					success:function(data){
						console.log(data);
						console.log(data.hmap.mhList);
						  var $tbody = $("<tbody>");
		                  var $thead = $("<thead><tr><th rowspan='2'>사원번호</th><th rowspan='2'>성명</th><th rowspan='2'>부서</th><th rowspan='2'>직급</th><c:forEach var='day' begin='1' end='16' varStatus='status'><th class='day1Th' id='day1Th${status.count}'><laebl style='opacity:0'>00</label></th></c:forEach></tr><tr><c:forEach var='day' begin='17' end='32'  varStatus='status'><c:if test='${day<=32}'><th class='day2Th' id='day2Th${status.count}'><i class='ui circle grey icon' style='opacity:0'></i></th></c:if></c:forEach></tr></thead>");
		                  var $title = $("<h3><i class='check square icon'></i>월근태 현황</h3>");
		                  var $table = $("<table class='hru board2' id='MATable' border='1'>");
		                  var $status = $("<div style='float:right'><label>출근<i class='ui circle grey icon' id='attend'></i></label><label>지각<i class='ui circle grey icon' id='late'></i></label><label>야특근<i class='ui circle grey icon' id='special'></i></label><label>휴가<i class='ui circle grey icon' id='vacation'></i></label></div>");
		                  var $td1 = $("<td rowspan='2'>");
		                  
		                  $table.append($thead);
		                  $table.append($tbody);
		                  $(".hru.segment.2").append($title);
		                  $(".hru.segment.2").append($status);
		                  $(".hru.segment.2").append($table);
		                  $(".hru.segment.2").css("margin-bottom", "40px");
		                  $("#searchBox").css("margin-bottom", "20px");
		                  //날짜 넣어주기
		                  var dayList = data.hmap.tList[0];
		                  var d = 0;
		                  $.each(dayList, function(index, i){
		                     if(i.day <= 16){
		                        $(".day1Th").eq(d).text(i.day);
		                     }else if(i.day > 16){
		                        $(".day2Th").eq(d-16).text(i.day);
		                     }
		                     d++;
		                  });
						
		                  var mhList = data.hmap.mhList;
		                  var tList = data.hmap.tList;
		                  console.log(tList);
		                  
		                  var str = "<tr>";
		                  for(var i = 0; i <mhList.length; i++){
		             		str += '<td rowspan="2">' + mhList[i].mid +'</td>'+ '<td rowspan="2">' + mhList[i].mName +'</td>' + '<td rowspan="2">' + mhList[i].teamName +'</td>' +
		             		'<td rowspan="2">'+ mhList[i].positionName + '</td>';
		           			for(var j = 0; j <= 15; j++){
		           				str += "<td class='dayTd'></td>"
		           			}
		           			str += "</tr>";
		           			for(var f = 16; f <= 31; f++){
		           				str += "<td class='dayTd'></td>"
		                     }
			                 str += "</tr>";
			                
		                  }
		                  $tbody.append(str);
		                  
		                  n = 0;
		                  console.log("here");
		                  for(var t = 0; t < tList.length; t++){
		                	for(var z = 0; z < tList[t].length; z++){
		                		  if(tList[t][z].status == 1){
		                			  $(".dayTd").eq(n).append("<i class='ui circle grey icon' id='attend'></i>");
		                		  }else if(tList[t][z].status == 2){
		                			  $(".dayTd").eq(n).append("<i class='ui circle grey icon' id='late'></i>");
		                		  }else if(tList[t][z].status == 3){
		                			  $(".dayTd").eq(n).append("<i class='ui circle grey icon' id='special'></i>");
		                		  }else if(tList[t][z].status == 4){
		                			  $(".dayTd").eq(n).append("<i class='ui circle grey icon' id='vacation'></i>");
		                		  }
		                		  
		                		  if(tList[t][z].weekend == 1){
		                              $(".dayTd").eq(n).css("background", "#ffedef");
		                           } 

		                		  n++;
		                	}
		                	n = 32*(t+1);
		                	console.log("n : " + n);
		                  }
		                  
					},
					error:function(status){
						console.log("fail!");
					}
				});
			}
		});
	</script>
</body>
</html>