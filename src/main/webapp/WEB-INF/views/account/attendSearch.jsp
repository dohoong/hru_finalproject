<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	.searchArea{
		padding-top:7px;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment">
		<h1 style="color:black">휴일·연장·야간  근무조회</h1>
				<div class="searchArea" style="height:70px; margin-bottom:70px;">
					<table align="center" cellspacing="10px">
						<tr>
							<td>
								<h3>근무일자 </h3>
							</td>
							<td>
								<div class="ui calendar" id="month_year_calendar">
									<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" name="workMonth" id="monthSelect" placeholder="근무년월 선택" readonly>
									</div>
								</div>
							<td>
							<td><h3>사업장 </h3></td>
							<td>
								<select class="ui dropdown group" id="groupSelect">
								<option value="">사업장</option>
								<option value="">사업장</option>
									<c:forEach var="i" begin="0" end="${glist.size() - 1}">
										<option value="${glist[i].groupCode}"><c:out value="${glist[i].groupName}"/></option>
									</c:forEach>
								</select>
								<select class="ui dropdown team" name="teamCode" id="teamSelect">
								 <option value="">부서</option>
								</select>					
							</td>
							<td>
								<button class="ui blue button" id="searchButton">검색하기</button>
							</td>
						</tr>
					</table>
				</div><!-- searchArea end -->
			</div><!-- 검색영역 끝 -->
			<div class="hru segment">
				<h3 id="saTitle"></h3>
			<!-- <h3><i class="check square icon"></i>휴일·연장·야간  근무이력</h3> -->
				<table class="hru board2" id="specialAttendanceTable" border="1"></table>
			</div>
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	
	<script>
		$(function(){
			$("#pay1").show();
			$(".ui.dropdown").dropdown();
			
			/* search Area 셀렉트박스 */
			$("#groupSelect").change(function(){
				$("#teamSelect").html("<option value=''>부서</option>");
				$("#teamSelect").parent().find(".text").text("부서");
				
				<c:forEach var="i" begin="0" end="${tlist.size()-1}">
				
				var teamCode = "${tlist[i].teamCode}";
				var teamName = "${tlist[i].teamName}";
				var groupCode = "${tlist[i].groupCode}";
				
				if(groupCode == $(this).val()){
					var $option = $("<option value='teamCode'>").text(teamName).val(teamCode);
					$("#teamSelect").append($option);
				}
			</c:forEach>
				$(".ui.dropdown team").dropdown();
			});
		});
		
		/* 데이트피커 */
		var today = new Date();
		$('#month_year_calendar').calendar({
				    type: 'month',
				    formatter: {
				    	date: function (date, settings) {
				    	      if (!date) return '';
				    	      var month = date.getMonth() + 1;
				    	      var month2;
				    	      var year = date.getFullYear();
				    	      if(month < 10){
				    	    	  month2 = '0' + month;
				    	      }else {
				    	    	  month2 = month;
				    	      }
				    	      return year + '-' + month2;
				    	}
				    },
				    text: {
				        monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
				    },
				    maxDate : new Date(today.getFullYear(), today.getMonth())
		});
		
		$("#searchButton").click(function(){
			console.log("click");
		})
		
			
		
		/* Ajax */
		$("#searchButton").click(function(){
			if($("#teamSelect").val() == ""){
				Swal.fire(
						  '검색 불가',
						  '해당 조건으로 검색할 수 없습니다.',
						  'warning'
				)
			} else{
				var teamCode = $("#teamSelect").val();
				var groupSelect = $("#groupSelect").val();
				var monthSelect = $("#monthSelect").val();
				
				$("#specialAttendanceTable").empty();
				$("#saTitle").empty();
				$.ajax({
					url:"selectSpecialAttendance.att",
					type:"post",
					data:{'teamCode' : teamCode, 'groupSelect' : groupSelect, 'monthSelect' : monthSelect},
					success:function(data){
						console.log(data);
						var saList = data.saList;
						if(saList.length > 0){
							var $tbody = $("<tbody>");
							var $thead = $("<thead><tr><th>NO</th><th>사원번호</th><th>성명</th><th>부서</th><th>직급</th><th>근태종류</th><th>근태일자</th><th>출근시간</th><th>퇴근시간</th></tr></thead>");
							$("#saTitle").append("<i class='check square icon'></i>휴일·연장·야간  근무이력");
							$("#specialAttendanceTable").append($thead);
							$.each(saList, function(index, i){
								var $tr1 = $("<tr>");
								var category;
								if(i.category == 1){
									category ="연장/야간근로";
								}else{
									category = "휴일근로";
								}
								$tbody.append($tr1.append($('<td>').append($('<label>').text(index + 1))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.mId))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.mName))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.team))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.position))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(category))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.dattDate))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.startTime))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.endTime))));
								
								$("#specialAttendanceTable").append($tbody);
							});
						}
					},error:function(status){
						console.log("실패!");
					}
				})
			}
		});
		
	</script>
</body>
</html>