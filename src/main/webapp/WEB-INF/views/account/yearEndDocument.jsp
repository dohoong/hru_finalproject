<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	.searchArea{
		height:80px;
		padding-top:10px;
	}
	.yefile:hover{
		text-decoration:underline;
		cursor:pointer;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment" style="width:99%">
			<h1>소득, 세액공제 증명 서류 제출 여부</h1>
			<div class="ui divider"></div>
			
			<h3><i class="check square icon"></i>연말정산 공제 증명서류 제출 여부</h3>
			<div class="searchArea">
			<!-- <form action="" method="get" id="searchForm"> -->
				<table align="center" cellspacing="10px">
					<tr>
						<td><h3>* 연도 </h3></td>
						<td><div class="ui calendar" style="margin-right:30px;">
							<div class="ui left icon input"><i class="calendar icon"></i><input type="text" size="10" name="year"></div></div></td>
						<td><div style="width: 50px;"></div></td>
						<td><h3>* 부서 </h3></td>
						<td><select class="ui dropdown" name="teamCode">
								<c:if test="${!empty tlist}">
									<c:forEach var="i" begin="1" end="${tlist.size()-1}">
										<option value="${tlist.get(i).get('teamCode')}"><c:out value="${tlist.get(i).get('teamName')}"/></option>
									</c:forEach>
									<option value="${tlist.get(0).get('teamCode')}"><c:out value="${tlist.get(0).get('teamName')}"/></option>
								</c:if>
							</select>
						</td>
						<td><div style="width: 50px;"></div></td>
						<td><h3>성명 </h3></td>
						<td>
							<div class="ui right icon input" style="margin-right:30px;"><input type="text" placeholder="사원명" name="mName">
								<i class="search icon"></i>
							</div>
						</td>								
						<td><input type="hidden" value="1" name="type">
							<button type="button" class="ui blue button" id="searchYE">검색하기</button>
						</td>
					</tr>
				</table>
			<!-- </form> -->
		</div><!-- searchArea end -->
			
		</div>
		<div class="hru segment" style="width:99%">
				<table class="hru board2" border="1" id="docTable">
					<thead>
						<tr>
							<th style="width:180px;">사원번호</th>
							<th style="width:200px;">성명</th>
							<th style="width:300px;">직급</th>
							<th style="width:200px;">부서</th>
							<th style="">제출 서류</th>
						</tr>
					</thead>
					<tbody> 
						<c:if test="${!empty yeDoculist}">
						<c:forEach var="i" begin="0" end="${yeDoculist.size()-1}">
							<tr>
								<td><c:out value="${yeDoculist.get(i).get('mid')}"/></td>
								<td><c:out value="${yeDoculist.get(i).get('mName')}"/></td>
								<td><c:out value="${yeDoculist.get(i).get('positionName')}"/></td>
								<td><c:out value="${yeDoculist.get(i).get('teamName')}"/></td>
								<td><div class="yefile"><input type="hidden" value="${yeDoculist.get(i).get('attachmentId')}"><c:out value="${yeDoculist.get(i).get('originName')}"/></div></td>
							</tr>
						</c:forEach>
						</c:if>
					</tbody>
				</table>
			</div>
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function(){
			$('#pay3').show();
			$(".ui.dropdown").dropdown();
			
			$('.ui.calendar').calendar({
				type : 'year',
				initialDate : new Date()
			});
			
			$(".yefile").click(function(){
				var num = $(this).find("input[type=hidden]").val();
				//console.log(num);
				location.href="fileDownload.me?attid=" +  num;
			});
			
			$("#searchYE").click(function(){
				var year = $("input[name=year]").val();
				var mName = $("input[name=mName]").val();
				var teamCode = $("select[name=teamCode]").val();
				var type = $("input[name=type]").val();
				//console.log(mName);
				
				$.ajax({
					url : "searchYE.me",
					type : "POST",
					data : {
						year : year,
						mName : mName,
						teamCode : teamCode,
						type : type
					},
					success : function(data){
						//console.log(data);
						var yelist = data.yelist;
						var $tbody = $("#docTable tbody");
						$tbody.html("");
						for(var i=0; i<yelist.length; i++){
							var $td1 = $("<td>").text(yelist[i].mid);
							var $td2 = $("<td>").text(yelist[i].mName);
							var $td3 = $("<td>").text(yelist[i].positionName);
							var $td4 = $("<td>").text(yelist[i].teamName);
							var $td5 = $("<td>").append($("<div class='yefile' onclick='clickTr(this)'>").append($("<input type='hidden'>").val(yelist[i].attachmentId)).append(yelist[i].originName));
							
							$tbody.append($("<tr>").append($td1).append($td2).append($td3).append($td4).append($td5));
						}
						
						
					},
					error : function(status){
						
					}
					
				});
			});
		});
		
		function clickTr(num){
			var selected = num.firstChild;
			if(selected.nodeType==3){
				selected = selected.siblingNode.value;
			} else{
				selected = selected.value;
			}
			location.href="fileDownload.me?attid=" +  selected;
		}
	</script>
</body>
</html>