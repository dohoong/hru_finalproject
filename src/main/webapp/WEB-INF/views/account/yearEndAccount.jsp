<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment" style="width:99%">
			<h1>연말정산</h1>
			<div class="ui divider"></div>
			<div class="ui message">
					<ul class="list">
						<li>1~3월까지 해당 서비스를 이용하실 수 있으며, 당해년도 연말정산 기간을 설정하여 저장버튼을 눌러주세요.</li>
						<li>설정하신 기간에 사원들이 연말정산 메뉴를 이용할 수 있습니다.</li>
						<li>제출자료는 공제증명서류 및 기타서류 메뉴에서 확인 가능합니다.</li>
					</ul>
			</div>
			<h3><i class="check square icon"></i>연말정산 일정 설정</h3>
			<c:if test="${empty yeMap}">
				<form action="storeYearEnd.me" method="post" id="storeYEForm">
				<table class="hru table">
					<tr>
						<th style="width:350px;">연말정산 실시</th>
						<td style="width:200px;"><div class="ui calendar" id="start1"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="startDate" readonly></div></div></td>
						<td style="width:100px; text-align:center;"><label class="hru label">~</label></td>
						<td style="width:200px;"><div class="ui calendar" id="end1"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="endDate" readonly></div></div></td>
						<td></td>
					</tr>
					<tr>
						<th>연말정산 간소화 서비스 자료 확인 및 제출 기한</th>
						<td style="width:200px;"><div class="ui calendar" id="start2"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="documentStart" readonly></div></div></td>
						<td style="width:100px; text-align:center;"><label class="hru label">~</label></td>
						<td style="width:200px;"><div class="ui calendar" id="end2"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="documentEnd" readonly></div></div></td>
					</tr>
					<tr>
						<th>추가자료 제출 기한</th>
						<td style="width:200px;"><div class="ui calendar" id="start3"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" readonly></div></div></td>
						<td style="width:100px; text-align:center;"><label class="hru label">~</label></td>
						<td style="width:200px;"><div class="ui calendar" id="end3"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" readonly></div></div></td>
						<td></td>
					</tr>
				</table>
				</form>
			<div class="buttonArea">
				<button class="ui blue button" id="store1">저장하기</button>
			</div>
			</c:if>
			<c:if test="${!empty yeMap}">
				<table class="hru table">
					<tr>
						<th style="width:350px;">연말정산 실시</th>
						<td style="width:200px;"><div class="ui disabled calendar" id="start1"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="startDate" value="${yeMap.get('startDate')}" readonly></div></div></td>
						<td style="width:100px; text-align:center;"><label class="hru label">~</label></td>
						<td style="width:200px;"><div class="ui disabled calendar" id="end1"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="endDate" value="${yeMap.get('endDate')}" readonly></div></div></td>
						<td></td>
					</tr>
					<tr>
						<th>연말정산 간소화 서비스 자료 확인 및 제출 기한</th>
						<td style="width:200px;"><div class="ui disabled calendar" id="start2"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="documentStart" value="${yeMap.get('documentStart')}" readonly></div></div></td>
						<td style="width:100px; text-align:center;"><label class="hru label">~</label></td>
						<td style="width:200px;"><div class="ui disabled calendar" id="end2"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" name="documentEnd" value="${yeMap.get('documentEnd')}" readonly></div></div></td>
					</tr>
					<tr>
						<th>추가자료 제출 기한</th>
						<td style="width:200px;"><div class="ui disabled calendar" id="start3"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" value="${yeMap.get('documentStart')}" readonly></div></div></td>
						<td style="width:100px; text-align:center;"><label class="hru label">~</label></td>
						<td style="width:200px;"><div class="ui disabled calendar" id="end3"><div class="ui left icon input">
							<i class="calendar icon"></i><input type="text" value="${yeMap.get('documentEnd')}" readonly></div></div></td>
						<td></td>
					</tr>
				</table>
			</c:if>
		</div>
		<div class="hru segment" style="width:98%; height:300px;"></div>
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function(){
			$('#pay3').show();

			$("#start1").calendar({
				type:'date',
				endCalendar : $('#end1')
			});
			$("#end1").calendar({
				type:'date',
				startCalendar : $('#start1')
			});

			$("#start2").calendar({
				type:'date',
				endCalendar : $('#end2')
			});
			$("#end2").calendar({
				type:'date',
				startCalendar : $('#start2')
			});

			$("#start3").calendar({
				type:'date',
				endCalendar : $('#end3')
			});
			$("#end3").calendar({
				type:'date',
				startCalendar : $('#start3')
			});
			
			$("#store1").click(function(){
				$("#storeYEForm").submit();
			});
			var current = new Date();
			var month = new String(current.getMonth()+1);
			if(month>3){
				$(".ui.calendar").addClass("disabled");
			}
			
		});
	</script>
</body>
</html>







