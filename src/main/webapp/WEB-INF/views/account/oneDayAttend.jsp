<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/>
<style>
	#impact{
		color:#0F4C81;
		font-weight:bold;
	}
	.searchArea{
		padding-top:7px;
	}
	.hru.segment.oneDay{
		margin-bottom:10px;
	}
	.odInput{
		width:75px;
	}
</style>
</head>
<body>
<jsp:include page="../common/menubar.jsp"/>

<div class="outer"><!--outer Area -->
<div class="contentArea" style="margin-bottom:120px;"> <!-- 내용부분! -->
	
	<div class="hru segment oneDay">
	<h1 style="color:black">일일 근태조회</h1>
				<div class="searchArea" style="height:70px; ">
					<table align="center" cellspacing="10px">
						<tr>
							<td>
							<h3>근무일자 </h3>
							</td>
							<td>
								<div class="ui calendar" id="date_calendar">
								  <div class="ui input left icon">
								    <i class="calendar icon"></i>
								    <input type="text" id="searchDate" name="oneDay" placeholder="근무일자 선택" readonly>
								  </div>
								</div>
							<td>
							<td><h3>사업장 </h3></td>
							<td>
								<select class="ui dropdown group" id="groupSelect">
								<option value="">사업장</option>
									<c:forEach var="i" begin="0" end="${glist.size() - 1}">
										<option value="${glist[i].groupCode}"><c:out value="${glist[i].groupName}"/></option>
									</c:forEach>
								</select>
								<select class="ui dropdown team" name="teamCode" id="teamSelect">
								 <option value="">부서</option>
								</select>
							</td>
							<td>
								<button class="ui blue button" id="searchButton">검색하기</button>
							</td>
						</tr>
					</table>
				</div><!-- searchArea end -->
			</div><!-- 검색영역 끝 -->
			
			<!-- 추가 설명부분 -->
			<div class="hru segment oneDay">
				<div class="ui message">
					<ul class="list">
						<li>일근태가 생성되지 않은 사원에 대해서는 <label id="impact">[인사정보등록]</label>에서 재직구분과 재직 일자를 확인하세요.</li>
						<li>근무시간, 휴일, 연장, 지각, 조퇴를 계산하기 위해서는 <label id="impact">[인사/급여/급여기준정보설정]</label>에서 근태에 대한 시각이 정확하게 입력되었는지 확인하세요. </li>
					</ul>
				</div>
			</div><!-- 추가 설명부분 끝! -->
			
			<!-- 월 근태 테이블 -->
			<div class="hru segment oneDay">
				<h3 id="oneDayTitle">
				<!-- <i class="check square icon" style="margin:5px auto;"></i>사원별 일근태 현황
				<button style="float:right; font-weight:bold" class="ui basic button">
				  <i style="color:#1f6650"class="download icon"></i>
				  엑셀 다운로드
				</button> -->
				</h3>
				<table class="hru board2" id="oneDayTable" border="1"></table>

			</div><!-- 월 근태 테이블 끝 -->
</div>
</div>

	<script>
		/* 상단 메뉴바 */
		$(function() {
			$("#pay1").show();
		});	
		
		/* 드롭다운 */
		$(function(){
			$(".ui.dropdown.group").dropdown();
			$(".ui.dropdown.team").dropdown();
		});
		
		/* 데이트피커 */
		$('#date_calendar').calendar({
		    type: 'date',
		    formatter: {
		    	date: function (date, settings) {
		    	      if (!date) return '';
		    	      var day = date.getDate();
		    	      var day2;
		    	      var month = date.getMonth() + 1;
		    	      var month2;
		    	      if(month < 10){
		    	    	  month2 = '0' + month;
		    	      }else {
		    	    	  month2 = month;
		    	      }
		    	      if(day < 10){
		    	    	  day2 = '0' + day;
		    	      }else {
		    	    	  day2 = day;
		    	      }
		    	      var year = date.getFullYear();
		    	      return year + '-' + month2 + '-' + day2;
		    	}
		    }
		});
		
		/* 사원별 일근태 수정버튼 */
		function timeModify(){
			$("input[name='attend']").attr("readonly", false);
			$("input[name='leave']").attr("readonly", false);
			$("input[name='dayOff']").attr("readonly", false);
			$("input[name='over']").attr("readonly", false);
			$("input[name='night']").attr("readonly", false);
			$("input[name='late']").attr("readonly", false);
			$("input[name='early']").attr("readonly", false);
			$("input[name='total']").attr("readonly", false);
		}
		
		$(function(){
			$("#groupSelect").change(function(){
				$("#teamSelect").html("<option value=''>부서</option>");
				$("#teamSelect").parent().find(".text").text("부서");
				
				console.log("Test");
				<c:forEach var="i" begin="0" end="${tlist.size()-1}">
				
				var teamCode = "${tlist[i].teamCode}";
				var teamName = "${tlist[i].teamName}";
				var groupCode = "${tlist[i].groupCode}";
				
				if(groupCode == $(this).val()){
					var $option = $("<option value='teamCode'>").text(teamName).val(teamCode);
					$("#teamSelect").append($option);
				}
			</c:forEach>
				$(".ui.dropdown team").dropdown();
			});
		});
		
		$("#searchDate").change(function(){
			console.log($("#searchDate").val());
		})
		
		$("#searchButton").click(function(){
			if($("#teamSelect").val() == ""){
				Swal.fire(
						  '검색 불가',
						  '해당 조건으로 검색할 수 없습니다.',
						  'warning'
				)
			} else{
				var teamCode = $("#teamSelect").val();
				var groupSelect = $("#groupSelect").val();
				var oneDay = $("#searchDate").val();
				
				$("#oneDayTable").empty();
				$("#oneDayTitle").empty();
				$.ajax({
					url:"selectDayAttendance.att",
					type:"post",
					data:{'teamCode' : teamCode, 'groupSelect' : groupSelect, 'oneDay' : oneDay},
					success:function(data){
						console.log(data);
						var oList = data.oList;
						if(oList.length > 0){
							$("#oneDayTitle").append("<i class='check square icon' style='margin:5px auto;'></i>사원별 일근태 현황");
							var $tbody = $("<tbody>");
							var $thead = $("<thead><tr><th>근무일자</th><th>사원번호</th><th>성명</th><th>직급</th><th>부서</th><th>출근시각</th><th>퇴근시각</th><th>연장근로</th><th>야간근로</th><th>휴일근로</th><th>총 근무시간</th></tr></thead>");
							$("#oneDayTable").append($thead);
							console.log(typeof(oList));
							$.each(oList, function(index, i){
								
								var $tr1 = $("<tr>");
								var $startTime = $('<input class="odInput" type="text" name="over" style="text-align:center" readonly>').val(i.startTime);
								var $endTime = $('<input class="odInput" type="text" name="over" style="text-align:center" readonly>').val(i.endTime);
								var $extendTime = $('<input class="odInput" type="text" name="over" style="text-align:center" readonly>').val(i.extendTime);
								var $nightTime = $('<input class="odInput" type="text" name="over" style="text-align:center" readonly>').val(i.nightTime);
								var $weekendTime = $('<input class="odInput" type="text" name="over" style="text-align:center" readonly>').val(i.weekendTime);
								var $totalTime = $('<input class="odInput" type="text" name="over" style="text-align:center" readonly>').val(i.totalTime);
								
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.workDate))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.mId))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.mName))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.position))));
								$tbody.append($tr1.append($('<td>').append($('<label>').text(i.team))));
								$tbody.append($tr1.append($('<td>').append($('<div class="ui input">').append($startTime))));
								$tbody.append($tr1.append($('<td>').append($('<div class="ui input">').append($endTime))));
								$tbody.append($tr1.append($('<td>').append($('<div class="ui input">').append($extendTime))));
								$tbody.append($tr1.append($('<td>').append($('<div class="ui input">').append($nightTime))));
								$tbody.append($tr1.append($('<td>').append($('<div class="ui input">').append($weekendTime))));
								$tbody.append($tr1.append($('<td>').append($('<div class="ui input">').append($totalTime))));
								$("#oneDayTable").append($tbody);
							})
						}else {
							Swal.fire(
									  '정보 없음',
									  '일일 근태정보가 없습니다.',
									  'warning'
							)
						}
					},error:function(status){
						console.log("실패!");
					}
					
				})
			}
		});
		

	</script>
</body>
</html>