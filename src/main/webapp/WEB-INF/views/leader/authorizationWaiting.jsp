<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hru</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	#searchArea2{
		padding-top: 10px;
	
	}
	.iconArea{
		padding-left: 150px;
	
	}
	.icon .ban{
		color: red;
	
	}
	#appTable tbody>tr:hover{
		cursor:pointer;
		background:#eee;
	}
	.download:hover{
		cursor:pointer;
		text-decoration:underline;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment">
			<h1>결재대기</h1>
			<div class="ui divider"></div>
			<div class="hru segment">
				<div class="ui message">
					<ul class="list">
						<li>회원님께 상신이 되어진 결재 대기 목록을 오래된 순으로 조회하실 수 있습니다.</li>
						<li>반려/승인 처리가 완료되면, 결재내역 메뉴에서 해당 내역을 확인, 검색 등을 하실 수 있습니다.</li>
					</ul>
				</div>
			
			</div>
			<%-- <div class="hru segment">
				<div class="searchArea" style="padding-top:10px; padding-bottom:10px; height:100%">
					<form action="" method="get">
					<table align="center" cellspacing="10px">
						<tr>
							<td><label class="hru label" style="margin-right:0;">기안자</label></td>
							<td><div class="ui input"><input type="text" placeholder="이름입력"></div></td>
							<td><label class="hru label">부서</label>
								<c:if test="${ !empty tlist }">
								<select class="ui compact dropdown">
								<c:forEach var="i" begin="0" end="${tlist.size()-1}">
									<option value="${i}"><c:out value="${tlist.get(i).get('teamName')}"/></option>
								</c:forEach>
							</select></c:if></td>
							<td><label class="hru label" style="margin-right:0;">기안날짜</label></td>
							<td><div class="ui calendar">
									<div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="기안날짜"></div></div></td>
						</tr>
						<tr>
							<td><label class="hru label">문서제목</label></td>
							<td colspan="4"><div class="ui rigth icon input small"><input type="text" size="78"><i class="search icon"></i></div></td>
						</tr>
						<tr align="center">
							<td colspan="5"><button class="ui blue button left icon" style="width:200px;"><i class="search icon"></i>검색하기</button></td>
						</tr>
					</table>
					</form>
				</div><!-- searchArea end -->
			</div> --%>
				<div class="hru segment">
				<table class="hru board2" border="1" id="appTable">
					<thead>
						<tr>
							<th style="width:100px;">결재번호</th>
							<th style="width:150px;">종류</th>
							<th>제목</th>
							<th style="width:200px;">기안 날짜</th>
							<th style="width:150px;">기안자</th>
							<th style="width:150px;">기안 부서</th>
							<th style="width:150px;">승인 여부</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${ !empty alist }">
						<c:forEach var="c" begin="0" end="${alist.size()-1}">
						<tr class="applist" onclick="showDetail(this);">
							<td><c:out value="${alist.get(c).get('approvalId')}"/></td>
							<td><c:out value="${alist.get(c).get('approvalName')}"/></td>
							<td><c:out value="${alist.get(c).get('approvalTitle')}"/></td>
							<td><fmt:formatDate value="${alist.get(c).get('approvalDate')}" pattern="yyyy-MM-dd hh:mm" /></td>
							<td><c:out value="${alist.get(c).get('mName')}"/></td>
							<td><c:out value="${alist.get(c).get('tName')}"/></td>
							<td><c:out value="대기중"/></td>
						</tr>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
			</div>
		</div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
	
	<script>
			$(function(){
				$("#leader1").show();
				$(".ui.dropdown").dropdown();
				$('.ui.calendar').calendar({
					type : 'date'
				});
				
			});
			


			function showDetail(num){
				var firstChild = num.firstChild;
				//console.log(firstChild.nextSibiling);
				if(firstChild.nodeType==3){
					var approvalId = firstChild.nextSibling.innerText;
				} else{
					var approvalId = firstChild.innerText;
				}
				//console.log(approvalId);
				$("#approvalMenu").children().eq(0).click();
				
				var $table1 = $("#appDetail1");
				$table1.html("");
				var $table2 = $("#appDetail2");
				$table2.html('');
				var $table3 = $("#appDetail3");
				$table3.html('');
				
				$.ajax({
					url:"showApprovalDetail.app",
					type:"POST",
					data:{approvalId:approvalId},
					success:function(data){
						//console.log(data);
						
						
						var date = moment(new Date(data.app.approvalDate.time)).format('YYYY-MM-DD HH:mm');
						
						var $th1 = $("<th width='150px'>").text("기안자");
						var $td1 = $("<td width='320px;'>").text(data.app.mName);
						var $th2 = $("<th width='150px'>").text("기안일자");							
						var $td2 = $("<td>").text(date);
						$table1.append($("<tr>").append($th1).append($td1).append($th2).append($td2));
						
						var $th3 = $("<th width='150px'>").text("결재종류");
						var category ="";
						switch(data.app.approvalCode){
							case 1 : category="신규사원등록"; break;
							case 2 : category="인사계획"; break;
							case 3 : category="인사발령"; break;
							case 4 : category="인사평가계획"; break;
							case 5 : category="월근태 마감"; break;
							case 6 : category="월급여 마감"; break;
							case 7 : category="야특근 신청"; break;
							case 8 : category="휴가신청"; break;
							case 9 : category="퇴직신청"; break;
							case 10 : category="인사평가 마감"; break;
							case 11 : category="휴가 취소신청"; break;
							case 12 : category="제증명신청"; break;
							case 13 : category="사원 일괄등록"; break;
						}
						var $td3 = $("<td>").text(category);
						var $th4 = $("<th>").text("첨부파일");
						if(data.app.attach != null){
							var attid = data.app.attach.attachmentId;
							var $inputAtt = $("<input type='hidden'>").val(attid);
							var $td4 = $("<td class='download' onclick='downloadAtt(this)'>").text(data.app.attach.originName).append($inputAtt);
							
						} else{
							var $td4 = $("<td>").text("");
						}
						$table1.append($("<tr>").append($th3).append($td3).append($th4).append($td4));
						
						var $th5 = $("<th>").text("결재 제목");
						var $td5 = $("<td colspan='3'>").text(data.app.approvalTitle);
						$table1.append($("<tr>").append($th5).append($td5));
							
						
						
						$("#successForm").find("input[type=hidden]").remove();
						$table2.before($("<input type='hidden' name='approvalId'>").val(data.app.approvalId));
						$table2.append($("<tr>").append($("<th rowspan='4'>").text("결재")).append($("<th width='210px'>").text("본인")).append($("<th width='210px'>").text("1차결재")).append($("<th width='210px'>").text("2차결재")).append($("<th width='210px'>").text("3차결재")));
						
						var $tr1 = $("<tr>");
						var $tr2 = $("<tr style='height:90px;'>");
						var $tr3 = $("<tr>");
						var $tr4 = $("<tr>").append($("<th>").text("의견"));
						
						//본인
						$tr1.append($("<td>").text(data.app.mName));
						$tr2.append($("<td>").append($("<i class='check circle blue icon huge' style='color:#0F4C81'>")).append($("<input type='hidden' id='requester'>").val(data.app.mid)));
						$tr3.append($("<td>").text(date));
						$tr4.append($("<td>").text("-"));
						
						//1,2,3차 결재자
						var ahlist = data.app.ahlist;
						for(var i=0; i<ahlist.length; i++){
							$inputId = $("<input type='hidden' name='apphisId'>").val(ahlist[i].apphisId);
							$inputF = $("<input type='hidden' name='finalYn'>").val(ahlist[i].finalYn);
							$inputM = $("<input type='hidden' name='mid'>").val(ahlist[i].mid);
							$inputR = $("<input type='hidden' name='result'>").val(ahlist[i].result);								

							$tr1.append($("<td>").text(ahlist[i].mName));
							var $result = $("<td>");
							switch(ahlist[i].result){
								case 1 : $result.html(""); break;
								case 2 : $result.html(""); break;
								case 3 : $result.append($("<i class='check circle blue icon huge' style='color:#0F4C81'>")); break;
								case 4 : $result.append($("<i class='times circle blue icon huge' style='color:#c93b30'>")); break;
							}
							$result.append($inputId).append($inputF).append($inputM).append($inputR);
							$tr2.append($result);
							if(ahlist[i].apphisDate != null){
								var ahisDate = moment(new Date(ahlist[i].apphisDate.time)).format('YYYY-MM-DD HH:mm');
							} else{
								var ahisDate = "-";
							}
							$tr3.append($("<td>").text(ahisDate));
							//console.log(ahlist[i].refuseReason);
							if(ahlist[i].refuseReason != ""){
								$tr4.append($("<td>").text(ahlist[i].refuseReason));									
							} else{
								$tr4.append($("<td>").text("-"))
							}
						}
						for(var i=0; i<3-ahlist.length; i++){
							$tr1.append($("<td>"));
							$tr2.append($("<td>"));
							$tr3.append($("<td>"));
							$tr4.append($("<td>"));
							
						}
						
						$table2.append($tr1).append($tr2).append($tr3).append($tr4);
						
						//품의 내용 출력하는 부분!
						$table3.append($("<thead>"));
						$t3head = $("#appDetail3 thead");
						if(data.app.attach != null){
							var attid2 = data.app.attach.attachmentId;
							var $inputAtt2 = $("<input type='hidden'>").val(attid2);
							var $td42 = $("<td class='download' onclick='downloadAtt(this)'>").text(data.app.attach.originName).append($inputAtt2);
							
							$t3head.append($("<tr>").append($("<th width='150px;'>").text("결재종류")).append($("<td colspan='2'>").text(category)).append($("<th width='150px'>").text("첨부파일")).append($td42));
						} else{
							$t3head.append($("<tr>").append($("<th width='150px;'>").text("결재종류")).append($("<td colspan='2'>").text(category)).append($("<th width='150px'>").text("첨부파일")).append($("<td>").text("")));
						}
						$t3head.append($("<tr>").append($("<th>").text("결재제목")).append($("<td colspan='4'>").text(data.app.approvalTitle)))
						
						$table3.append($("<tbody>"));
						var $t3body = $("#appDetail3 tbody");
						$t3body.html("<tr><td colspan='5'><h3><i class='check square icon'></i>품의내용</h3></td></tr>");
						
						//신입사원 입사에대한 품의일 경우,
						if(data.tempMember != null){
							var $tempTh1 = $("<th>").text("이름");
							var $tempTh2 = $("<th>").text("주민번호");
							var $tempTh3 = $("<th>").text("국적");
							var $tempTh4 = $("<th>").text("이메일");
							var $tempTh5 = $("<th>").text("입사일");
							$t3body.append($("<tr>").append($tempTh1).append($tempTh2).append($tempTh3).append($tempTh4).append($tempTh5));
							var $tempTd1 = $("<td>").text(data.tempMember.tempName);
							var $tempTd2 = $("<td>").text(data.tempMember.tempSsn + "******");
							var $tempTd3 = $("<td>").text(data.tempMember.tempCountry);
							var $tempTd4 = $("<td>").text(data.tempMember.tempEmail);
							var tempDate = moment(new Date(data.tempMember.tempDate.time)).format('YYYY-MM-DD');
							var $tempTd5 = $("<td>").text(tempDate);
							$t3body.append($("<tr>").append($tempTd1).append($tempTd2).append($tempTd3).append($tempTd4).append($tempTd5));
							
							var $tempTh6 = $("<th>").text("부서");
							var $tempTh7 = $("<th>").text("직위/직급");
							var $tempTh8 = $("<th>").text("직종");
							var $tempTh9 = $("<th>").text("직책");
							var $tempTh10 = $("<th>").text("계약형태");
							$t3body.append($("<tr>").append($tempTh6).append($tempTh7).append($tempTh8).append($tempTh9).append($tempTh10));
							var $tempTd6 = $("<td>").text(data.tempMember.teamName);
							var $tempTd7 = $("<td>").text(data.tempMember.positionName);
							var $tempTd8 = $("<td>").text(data.tempMember.jobName);
							var $tempTd9 = $("<td>").text(data.tempMember.rName);
							var $tempTd10 = $("<td>").text(data.tempMember.enterName);
							$t3body.append($("<tr>").append($tempTd6).append($tempTd7).append($tempTd8).append($tempTd9).append($tempTd10));
							
							var $tempTh11 = $("<th colspan='5'>").text("급여형태/급여액");
							var $tempTd11 = $("<td>").text(data.tempMember.salaryType);
							var $tempTd12 = $("<td style='text-align:right'>").text(data.tempMember.salaryAmount);
							var $tempTd13 = $("<td colspan='3'>").text("원");
							$t3body.append($("<tr>").append($tempTh11));
							$t3body.append($("<tr>").append($tempTd11).append($tempTd12).append($tempTd13));
							
							
						} else if(data.vhmap != null){ //휴가신청
							//console.log(data.vhmap);
							var $tempTh1 = $("<th>").text("휴가종류");
							var $tempTh2 = $("<th>").text("휴가시작");
							var $tempTh3 = $("<th>").text("휴가종료");
							var $tempTh4 = $("<th>").text("휴가일수");
							var $tempTh5 = $("<th>").text("사유");
							$t3body.append($("<tr>").append($tempTh1).append($tempTh2).append($tempTh3).append($tempTh4).append($tempTh5));
							var $tempTd1 = $("<td>").text(data.vhmap.vacationName);
							var startDate = moment(new Date(data.vhmap.vacationStart.time)).format('YYYY-MM-DD');
							var endDate = moment(new Date(data.vhmap.vacationStart.time)).add(data.vhmap.vacationNum-1, 'days').format('YYYY-MM-DD');
							var $tempTd2 = $("<td>").text(startDate);
							var $tempTd3 = $("<td>").text(endDate);
							var $tempTd4 = $("<td>").text(data.vhmap.vacationNum);
							var $tempTd5 = $("<td>").text(data.vhmap.reason);
							$t3body.append($("<tr>").append($tempTd1).append($tempTd2).append($tempTd3).append($tempTd4).append($tempTd5));
						} else if(data.movMap!=null){ //인사발령
							var $tempTh11 = $("<th>").text("사원명");
							var $tempTd11 = $("<td>").text(data.movMap.mName + "(사번: " + data.movMap.mid + ")");
							var $tempTd111 = $("<td>");
							switch(data.movMap.category){
								case 1 : $tempTd111.text("입사"); break;
								case 2 : $tempTd111.text("직무이동"); break;
								case 3 : $tempTd111.text("부서이동"); break;
								case 4 : $tempTd111.text("조직책임자 임명"); break;
								case 5 : $tempTd111.text("진급"); break;
							}
							var $tempTh12 = $("<th>").text("발령 구분/일자");
							var $tempTd12 = $("<td>").text(data.movMap.moveDate);
							$t3body.append($("<tr>").append($tempTh11).append($tempTd11).append($tempTh12).append($tempTd111).append($tempTd12));
							
							var $tempTh13 = $("<th>").text("발령 사유");
							var $tempTd13 = $("<td colspan='4'>").text(data.movMap.reason);
							$t3body.append($("<tr>").append($tempTh13).append($tempTd13));
							var $titleTd = $("<td colspan='5'>").html("<label class='hru label'>발령 전</label>");
							$t3body.append($("<tr>").append($titleTd));
							var $tempTh1 = $("<th>").text("부서");
							var $tempTh2 = $("<th>").text("직군");
							var $tempTh3 = $("<th>").text("직책");
							var $tempTh4 = $("<th>").text("직급");
							var $tempTh5 = $("<th>").text("계약형태");
							$t3body.append($("<tr>").append($tempTh1).append($tempTh2).append($tempTh3).append($tempTh4).append($tempTh5));
							var $tempTd1 = $("<td>").text(data.movMap0.teamName);
							var $tempTd2 = $("<td>").text(data.movMap0.jobName);
							var $tempTd3 = $("<td>").text(data.movMap0.rName);
							var $tempTd4 = $("<td>").text(data.movMap0.positionName);
							var $tempTd5 = $("<td>").text(data.movMap0.enterName);
							$t3body.append($("<tr>").append($tempTd1).append($tempTd2).append($tempTd3).append($tempTd4).append($tempTd5));
							
							var $titleTd2 = $("<td colspan='5'>").html("<label class='hru label'>발령 후</label>");
							$t3body.append($("<tr>").append($titleTd2));
							var $tempTh6 = $("<th>").text("부서");
							var $tempTh7 = $("<th>").text("직군");
							var $tempTh8 = $("<th>").text("직책");
							var $tempTh9 = $("<th>").text("직급");
							var $tempTh10 = $("<th>").text("계약형태");
							$t3body.append($("<tr>").append($tempTh6).append($tempTh7).append($tempTh8).append($tempTh9).append($tempTh10));
							var $tempTd6 = $("<td>").text(data.movMap.teamName);
							var $tempTd7 = $("<td>").text(data.movMap.jobName);
							var $tempTd8 = $("<td>").text(data.movMap.rName);
							var $tempTd9 = $("<td>").text(data.movMap.positionName);
							var $tempTd10 = $("<td>").text(data.movMap.enterName);
							$t3body.append($("<tr>").append($tempTd6).append($tempTd7).append($tempTd8).append($tempTd9).append($tempTd10));
							
							
						} else if(data.evalist!=null){ //평가계획
							var list = data.evalist;
							
							var $tempTh1 = $("<th>").text("부서");
							var $tempTd1 = $("<td colspan='2'>").text(list[0].teamName);
							var $tempTh2 = $("<th>").text("평가년도");
							var $tempTd2 = $("<td>").text(list[0].evalYaer);
							$t3body.append($("<tr>").append($tempTh1).append($tempTd1).append($tempTh2).append($tempTd2));
							$t3body.append($("<tr>"));

							var $tempTh3 = $("<th>").text("카테고리");
							var $tempTh4 = $("<th colspan='2'>").text("평가내용");
							var $tempTh5 = $("<th>").text("평가항목");
							var $tempTh6 = $("<th>").text("가중치");
							$t3body.append($("<tr>").append($tempTh3).append($tempTh4).append($tempTh5).append($tempTh6));
							for(var i=0; i<list.length; i++){
								var $tempTh7 = $("<th colspan='5'>").text("평가항목" + (i+1) + ": " + list[i].evalName);
								$t3body.append($("<tr>").append($tempTh7));

								var $tempTd3 = $("<td>").text(list[i].category);
								
								var content = list[i].evalContent.split("#");
								//console.log(content);
								var content2 = "";
								var growth = ["A", "B", "C", "D", "E"];
								for(var k=0; k<content.length; k++){
									content2 += growth[k] + ": " + content[k] + "<br>";
								}
								if(list[i].category=="단방향"){
									var $tempTd4 = $("<td colspan='2'>").html(content2);									
								} else{
									var $tempTd4 = $("<td colspan='2'>").html(list[i].evalContent);									
								}
								var $tempTd5 = $("<td>").text(list[i].type);
								var $tempTd6 = $("<td>").text(list[i].evalWeight + "%");
								$t3body.append($("<tr>").append($tempTd3).append($tempTd4).append($tempTd5).append($tempTd6));
							}
						} else if(data.smap!=null){
							var smap = data.smap;
							var $tempTh1 = $("<th>").text("종류");
							var $tempTh2 = $("<th colspan='2'>").text("날짜");
							var $tempTh3 = $("<th>").text("시작시간");
							var $tempTh4 = $("<th>").text("종료시간");
							$t3body.append($("<tr>").append($tempTh1).append($tempTh2).append($tempTh3).append($tempTh4));
							
							var $tempTd1 = $("<td>").text(smap.category);
							var $tempTd2 = $("<td colspan='2'>").text(smap.sattDate);
							var $tempTd3 = $("<td>").text(smap.startDate);
							var $tempTd4 = $("<td>").text(smap.endDate);
							$t3body.append($("<tr>").append($tempTd1).append($tempTd2).append($tempTd3).append($tempTd4));
						}  else if(data.hrplan!=null){
							var hrplan = data.hrplan.tplist;
							console.log(hrplan);
							
							
							var $tempTh1 = $("<th style='width:180px;'>").text("사업장/부서");
							var $tempTh2 = $("<th style='width:150px;'>").text("조직책임자");
							var $tempTh3 = $("<th style='width:100px;'>>").text("인원수");
							var $tempTh4 = $("<th style='width:150px;'>>").text("충원예정 / 방출예정");
							var $tempTh5 = $("<th>").text("내용");
							$t3body.append($("<tr>").append($tempTh1).append($tempTh2).append($tempTh3).append($tempTh4).append($tempTh5));
							for(var i=0; i<hrplan.length; i++){
								var $tempTd1 = $("<td>").append(hrplan[i].gName + "/" +hrplan[i].teamName);
								var $tempTd2 = $("<td>").append(hrplan[i].lname);
								var $tempTd3 = $("<td>").append(hrplan[i].peopleNum);
								var $tempTd4 = $("<td>").append("(+)" + hrplan[i].addNum + " / (-)" + hrplan[i].outNum);
								var $tempTd5 = $("<td>").append(hrplan[i].detail);
								$t3body.append($("<tr>").append($tempTd1).append($tempTd2).append($tempTd3).append($tempTd4).append($tempTd5));
							}
							
						} else if(data.evaluation!=null){
							console.log(data.evaluation);
							var eval = data.evaluation;
							var ed = eval.edlist;

							var $tempTh1 = $("<th style='width:150px;'>").text("사원명(사번)");
							var $tempTh2 = $("<th style='width:230px;'>").text("평가연도");
							var $tempTh3 = $("<th style='width:220px;'>").text("평가점수(총점)");
							var $tempTh4 = $("<th style='width:220px;'>").text("진급대상자");
							var $tempTh5 = $("<th>").text("연봉인상률");
							$t3body.append($("<tr>").append($tempTh1).append($tempTh2).append($tempTh3).append($tempTh4).append($tempTh5));
							var $tempTd1 = $("<td>").text(eval.mName + "(" + eval.mid + ")");
							var $tempTd2 = $("<td>").text(eval.evaluationYear + "년");
							var $tempTd3 = $("<td>").text(eval.totalScore + " (점)");
							var $tempTd4 = $("<td>").text(eval.levelupYn);
							var $tempTd5 = $("<td>").text(eval.salaryRate + " (%)");
							$t3body.append($("<tr>").append($tempTd1).append($tempTd2).append($tempTd3).append($tempTd4).append($tempTd5));
							
							var $titleTd1 = $("<td colspan='5'>").html("<label class='hru label'>1. 양방향 평가내용</label>");
							$t3body.append($("<tr>").append($titleTd1));
							
							var $tempTh6 = $("<th>").text("항목");
							var $tempTh7 = $("<th>").text("목표");
							var $tempTh8 = $("<th>").text("피평가자의견");
							var $tempTh9 = $("<th>").text("평가자의견");
							var $tempTh10 = $("<th>").text("평가점수");
							$t3body.append($("<tr>").append($tempTh6).append($tempTh7).append($tempTh8).append($tempTh9).append($tempTh10));
							
							for(var i=0; i<ed.length; i++){
								if(ed[i].category==1){
									var $tempTd6 = $("<td>").text(ed[i].evalName);
									var $tempTd7 = $("<td>").append($("<textarea rows='3' cols='25' style='resize:none; background:#eee;' readonly>").text(ed[i].edContent));
									var $tempTd8 = $("<td>").append($("<textarea rows='3' cols='25' style='resize:none; background:#eee;' readonly>").text(ed[i].ropinion));
									var $tempTd9 = $("<td>").append($("<textarea rows='3' cols='25' style='resize:none; background:#eee;' readonly>").text(ed[i].gopinion));
									var $tempTd10 = $("<td>").text(ed[i].evalScore + "/" + ed[i].evalWeight);
									
									$t3body.append($("<tr>").append($tempTd6).append($tempTd7).append($tempTd8).append($tempTd9).append($tempTd10));
								}
							}
							
							var $titleTd2 = $("<td colspan='5'>").html("<label class='hru label'>2. 단방향 평가내용</label>");
							$t3body.append($("<tr>").append($titleTd2));
							
							var $tempTh11 = $("<th>").text("종류");
							var $tempTh12 = $("<th colspan='2'>").text("평가항목");
							var $tempTh13 = $("<th colspan='2'>").text("평가점수");
							$t3body.append($("<tr>").append($tempTh11).append($tempTh12).append($tempTh13));
							
							for(var i=0; i<ed.length; i++){
								if(ed[i].category ==2){
									switch(ed[i].type){
									case 1 : var $tempTd11 = $("<td>").text("업적"); break;
									case 2 : var $tempTd11 = $("<td>").text("능력"); break;
									case 3 : var $tempTd11 = $("<td>").text("태도"); break;
									}
									var $tempTd12 = $("<td colspan='2'>").text(ed[i].evalName);
									var $tempTd13 = $("<td colspan='2'>").text(ed[i].evalScore + "/" + ed[i].evalWeight);
									
									$t3body.append($("<tr>").append($tempTd11).append($tempTd12).append($tempTd13));
								}
							}
							
							
							
						} else if(data.monlist!=null){
							var monlist = data.monlist;
							//console.log(monlist);

							var $tempTh1 = $("<th colspan='2'>").text("해당조직");
							var $tempTh2 = $("<th colspan='3'>").text("근무년월");
							$t3body.append($("<tr>").append($tempTh1).append($tempTh2));
							var $tempTd1 = $("<td colspan='2'>").text(monlist[0].teamName);
							var $tempTd2 = $("<td colspan='3'>").text(String(monlist[0].mattDate).substring(0,4) + "년 " + String(monlist[0].mattDate).substring(5,7) + "월");
							$t3body.append($("<tr>").append($tempTd1).append($tempTd2));
							$t3body.append($("<tr>").append($("<td colspan='5'>").append($("<label class='hru label'>").text("월근태 내용"))));
							
							var $tempTh3 = $("<th>").text("이름(직급)");
							var $tempTh4 = $("<th colspan='1' style='width:400px;'>").text("평일근무 | 휴일근무 | 결근일 | 연차사용 (일수)");
							var $tempTh5 = $("<th colspan='3' style='width:300px;'>").text("연장 | 야간 | 휴일1 | 휴일2 (시간)");
							$t3body.append($("<tr>").append($tempTh3).append($tempTh4).append($tempTh5));
							for(var i=0; i<monlist.length; i++){
								var $tempTd3 = $("<td>").text(monlist[i].mName + "(" + monlist[i].positionName.substring(0,2) + ")");
								var $tempTd4 = $("<td colspan='1'>").text(monlist[i].weekdayNum + "일 | " + monlist[i].holidayNum + "일 | " + monlist[i].noattNum + "일 | " + monlist[i].vacationNum + "일");
								var $tempTd5 = $("<td colspan='3'>").text(monlist[i].workMore + "시간 | " + monlist[i].workNight + "시간 | " + monlist[i].workHolidayOne + "시간 | " + monlist[i].workHolidayTwo + "시간");
								$t3body.append($("<tr>").append($tempTd3).append($tempTd4).append($tempTd5));
							}
							
						} else{
							var $tempTd1 = $("<td colspan='5' style='padding:50px; text-align:center;'>").html('<h2 style="color:lightgray"><label><i class="exclamation circle icon"></i>상세내용이 없습니다. 첨부파일 참고바랍니다.</label></h2>');
							$t3body.append($("<tr>").append($tempTd1));
						}
						
						
						
						$("#approve").prop("disabled",false);
						$("#nonono").prop("disabled", false);
						
						$("#approve").click(function(){
							console.log($("#requester").val());
							Swal.fire({
								  title: '결재 승인하시겠습니까?',
								  text: "승인 전 결재 내용을 한번 더 확인해주세요.",
								  icon: 'warning',
								  showCancelButton: true,
								  confirmButtonText: '승인',
								  cancelButtonText: '취소'
								}).then((result) => {
								  if (result.value) {
									  var tmid = 0;
									  var content = "";
									  if($("#appDetail2").find("tr").eq(2).find("td").eq(1).find("input[name=result]").val()=='1'){//1차 결재자
										  if($("#appDetail2").find("tr").eq(2).find("td").eq(1).find("input[name=finalYn]").val()=='Y'){
											  tmid = $("#requester").val(); //결재끝 --> 기안자에게
											  content = "결재완료 : ";
										  } else{
											  tmid = $("#appDetail2").find("tr").eq(2).find("td").eq(2).find("input[name=mid]").val(); //2차 결재자에게
											  content = "결재요청 : ";
										  }
										  
									  } else if($("#appDetail2").find("tr").eq(2).find("td").eq(2).find("input[name=result]").val()=='1'){
										  if($("#appDetail2").find("tr").eq(2).find("td").eq(2).find("input[name=finalYn]").val()=='Y'){
											  tmid = $("#requester").val(); //결재끝 --> 기안자에게
											  content = "결재완료 : ";
										  } else{
											  tmid = $("#appDetail2").find("tr").eq(2).find("td").eq(3).find("input[name=mid]").val(); //3차 결재자에게
											  content = "결재요청 : ";
										  }
										  
									  } else if($("#appDetail2").find("tr").eq(2).find("td").eq(3).find("input[name=result]").val()=='1'){
										  tmid = $("#requester").val(); //결재끝 --> 기안자에게
										  content = "결재완료 : ";
									  }
									  content += $("#appDetail1").children().eq(2).find("td").text();
									  
									  send(tmid, content);
									  $("#successForm").submit();
									  
								  }
								});
							
						});
						$("#nonono").click(function(){
							var num = $("#successForm").find("input[type=hidden]").eq(0).val();
							console.log(num);
							Swal.fire({
								title: '반려사유를 입력해주세요.',
								input: 'text',
								confirmButtonText: '반려',
								confirmButtonColor: '#d33',
								inputAttributes: {
									autocapitalize: 'off'
								},
								showCancelButton: true,
								cancelButtonText: '취소',
							}).then((result) => {
								if(result.value){
									console.log(result.value);
									var tmid = $("#requester").val(); //반려 --> 기안자에게
									var content = "결재반려 : ";
									content += $("#appDetail1").children().eq(2).find("td").text();
									send(tmid, content);
									
									location.href="updateRefuse.app?num=" + num + "&reason=" + result.value;
								} 								
							});
							
						});
						
					},
					error:function(status){
						console.log(status);
					}
				});
				$("#approvalModal2").modal('show');

			}

			function downloadAtt(num){
				var id = num.firstChild;
				if(id.nodeType==3){
					id = id.nextSibling.value;
				} else{
					id = id.value;
				}
				location.href="fileDownload.me?attid=" +  id;
				
			}
	</script>	
	
</body>
</html>