<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hru</title>
<style>
	#searchArea2{
		padding-top: 10px;
	
	}
	.iconArea{
		padding-left: 150px;
	
	}
	.icon .ban{
		color: red;
	
	}

</style>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment">
	<h1 style="color:black">결재내역</h1>
	<hr>
	<br><br>
				<div class="searchArea" id="searchArea2">
					<form action="" method="get">
					<table align="center" >
						<tr align="left">
							<th colspan="2" style="padding: 30px;">기안자</th>
							<td style="padding-right: 150px;"><div class="ui mini input">
							<input type="text" id="wirterC" ></div></td>
							<th colspan="2">부서</th>
							<td>
								<div class="ui compact selection dropdown" style="width:80%">
									<input type="hidden" name="category"><i class="dropdown icon"></i>
										<div class="default text">부서</div>
									<div class="menu">
										<div class="item" data-value="1">영업1팀</div>
										<div class="item" data-value="2">영업2팀</div>
										<div class="item" data-value="3">영업3팀</div></div>
								</div>
					
							</td>
				
							<td>
								<button class="ui blue button">검색하기</button>
							</td>
						</tr>
						<tr>
							<th colspan="2">문서제목</th>
							<td><div class="ui mini input">
							<input type="text" id="textC" ></div></td>
							<th colspan="2">기안날짜</th>
							<td>
								<div class="ui calendar" id="Date1">
								  <div class="ui input left icon">
								    <i class="calendar icon"></i>
								    <input type="text" placeholder="Date/Time">
								  </div>
								</div>
						
						</tr>
					</table>
					</form>
			</div>
			<br>
					<table align="right">
						<tr>
							<td><i class="green check circle icon"></i>완료</td>
							<td><i class="ellipsis horizontal icon"></i>대기중</td>
							<td><i class="red ban icon"></i>반려</td>
						</tr>
					</table>
					<br><br>
		<div class="hru segment">
			
				<table class="hru board2" border="1">
					<thead>
						<tr>
							<th>No</th>
							<th>서식함</th>
							<th>문서 제목</th>
							<th>기안자</th>
							<th>기안 날짜</th>
							<th>기안 부서</th>
							<th>승인 여부</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="c" begin="1" end="10">
						<tr>
							<td>${c}</td>
							<td>영업</td>
							<td>영업비밀${c}</td>
							<td>김진호</td>
							<td>2019-01-01</td>
							<td>영업1팀</td>
							<td><i class="large green check circle icon"></i></td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="pagingArea">
					<div class="paging" style="margin-right:15px;"><i class="angle double left icon"></i></div>
					<c:forEach var="p" begin="1" end="10">
						<div class="paging">${p}</div>
					</c:forEach>
					<div class="paging" style="margin-left:15px;"><i class="angle double right icon"></i></div>
				</div>
			</div>
		
		</div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	
	<script>
			$(function(){
				$(".ui.selection.dropdown").dropdown();
				$('#standard_calendar').calendar();
			});
	</script>	
	
</body>
</html>