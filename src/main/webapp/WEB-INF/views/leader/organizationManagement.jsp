<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>조직관리</title>
<style>
	.searchArea{
		padding-right: 150;
	}

	.table5	{
		width:100%;
		text-align:center;
	}
	
	#div7:hover{
		cursor:pointer;
		background:#eee;
	}
	#div6{
		padding-left:30px; 
		padding-right:30px;
		padding-bottom: 10px;
	}
	.img {
	width:200px;
	height:170px;
	overflow:hidden;
	object-fit:cover;
	}


</style>

<jsp:include page="../common/importlib.html"/>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<h1>조직관리</h1>
			<div class="ui divider"></div>
			<br>

			<div class="searchArea" style="padding-top:10px; padding-bottom:10px; height:100%">
					<form action="searchTeamMember.au" method="POST">
					<table align="center" cellspacing="10px">
						<tr>
							<td>
							<input type="hidden" name="teamCode" value="${ memlist.get(c).get('teamCode') }">
							</td>
							<td>
							<label class="hru label">검색</label>
								<select class="ui compact dropdown" name="category">
								<option value="0">사원명</option>
								<option value="1">직위</option>
							</select>
							</td>
							
							<td><div class="ui input" style="margin-right:10px;"><input type="text" placeholder="입력하세요" name="searchVal"></div>
							<button class="ui blue button left icon" style="width:200px;" type="submit"><i class="search icon"></i>검색하기</button></td>
						</tr>
					</table>
					</form>
				</div><!-- searchArea end -->
				<br>
			<div class="ui divider"></div>
				<div style="height: 100%">
					<table class="table5" id="table5">
					<c:if test="${memlist.size() > 0}">
						<c:forEach var="c" begin="0" end="${memlist.size()-1}" varStatus="status">					
						<c:if test="${c%3==0}"><tr></c:if>
							<td onclick="location.href='memberInfo.hr?mid=' + ${memlist.get(c).get('mid')}">
								<div class="ui horizontal cards" align="left" id="div6" >
								  <div class="card" id="div7">  
								    <div class="img">
								    <c:if test="${ memlist.get(c).get('changeName') != null }">
										<img class="img" src="resources/uploadFiles/${ memlist.get(c).get('changeName') }">
								    </c:if>
									<c:if test="${ memlist.get(c).get('changeName') = null} ">
										<img class="img" src="resources/uploadFiles/defaultProfile.png">
									</c:if>
								    </div>
								   
								    <div class="content">
								      <div class="header">
								      	<input type="hidden" value="${ memlist.get(c).get('mid') }" name="mid">
								      	<c:out value="${memlist.get(c).get('mName') }"/>
								      </div>
								      <div class="meta">
								        <a>
							
								         직위 : <input type="hidden" value="${ memlist.get(c).get('ppCode') }" name="ppCode">
								        <c:out value="${memlist.get(c).get('ppName') }"/>
								        </a>
								      </div>
								      <div>
								      <a>
								       	사업장 :  <input type="hidden" value="${ memlist.get(c).get('groupCode') }" name="groupCode">
								      <c:out value="${memlist.get(c).get('groupName')}"/>
								      </a>
								      </div>
								      <div class="description">
								        <a>
								     	  부서명 :
								        <c:out value="${memlist.get(c).get('teamName') }"/>
								        <input type="hidden" value="${ memlist.get(c).get('teamCode') }" name="teamCode">
								        </a>
								      </div>
								      <div>
								      	<a>
								      	입사일 : 
								      	<c:out value="${memlist.get(c).get('enterDate') }"/>
								      	</a>
								      </div>
								       <div>
								      	<a>
								      	PHONE : 
								      	<c:out value="${memlist.get(c).get('phone') }"/>
								      	</a>
								      </div>
								    </div>
								    
								  </div>
								</div>
								<input type="hidden" value="1" name="teamCode">
								</td>
							<c:if test="${c%3==2}"></tr></c:if>
							</c:forEach>
						</c:if>						
					</table>
					
			</div> 
			
			<br><br>

							
		
		
		
		
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	
	<script>
		$(function(){
			$("#leader2").show();
			$(".ui.dropdown").dropdown();
		})
		
		
/* 		function infoMemeber(fmid){
         $.ajax({
            url:"showMemberLeader.hr",
            type:"POST",
            data:{'fmid':fmid},
            success:function(data){
            	console.log(data)
               var access = data.access;
               if(access == "Y"){
                  location.href="memberInfo.hr?mid=" + fmid;
               }else{
                  Swal.fire(
                          '접근불가',
                          '접근 권한이 없는 사원입니다.',
                          'error'
                        )
               }
            }
         });
      } */
		
	</script>

</body>
</html>