<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	.hru.board2 tbody>tr:not(.noeval):hover{
		cursor:pointer;
		background:#eee;
	}
	#approvalLine tr:hover{
		background:white;
		cursor:default;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment">
			<h1>인사평가</h1>
			<div class="ui divider"></div>
			<div class="searchArea">
					<table class="searchTable" align="center" cellspacing="5px">
						<tr>
							<td><label class="hru label">해당연도:</label></td>
							<td><div id="yearCalendar" class="ui calendar" style="margin-right:30px;">
								<div class="ui left icon input"><i class="calendar icon"></i><input type="text" placeholder="연도선택" readonly size="10"></div></div></td>
							<td><label class="hru label">사원정보:</label>
							<select class="ui compact dropdown" id="ppCode">
								<c:forEach var="i" begin="0" end="${plist.size()-1}">
									<option value="${plist.get(i).get('ppCode')}"><c:out value="${plist.get(i).get('ppName')}"/></option>
								</c:forEach>
								</select>					
							</td>
							<td><div class="ui right icon input" style="margin-right:30px;">
									<i class="search icon"></i><input type="text" size="20" placeholder="사원이름" id="mName"></div>
							</td>
							<td>
								<button type="button" class="ui left icon blue button" onclick="searchFunction();"><i class="search icon"></i>검색하기</button>
							</td>
						</tr>
					</table>
			</div><!-- searchArea end -->	
		</div><!-- segment1 끝! -->
		<div class="hru segment">
			<table class="hru board2" border="1" id="evalTable">
					<thead>
						<tr>
							<th style="width:200px;">해당연도</th>
							<th style="width:180px;">사원번호</th>
							<th style="width:230px;">사원명</th>
							<th style="width:200px;">직급</th>
							<th style="width:250px;">평가결과(백분율)</th>
							<th>진행상황</th>
							<th>평가자</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${!empty evalist}">
						<c:forEach var="e" begin="0" end="${evalist.size()-1}">
						<c:if test="${evalist.get(e).mid != loginUser.mid}">
						<tr class="eval-tr">
							<td><input type="hidden" value="${evalist.get(e).evaluationId}"><c:out value="${evalist.get(e).evaluationYear}"/></td>
							<td><c:out value="${evalist.get(e).mid}"/></td>
							<td><c:out value="${evalist.get(e).mName}"/></td>
							<td><c:out value="${evalist.get(e).ppName}"/></td>
							<td><c:out value="${evalist.get(e).totalScore}"/></td>
							<td><c:if test="${evalist.get(e).approvalId==0}">
								<c:if test="${evalist.get(e).approvalYn=='P'}">목표수립완료</c:if>
								<c:if test="${evalist.get(e).approvalYn=='N'}">목표수립중</c:if></c:if>
							<c:if test="${evalist.get(e).approvalId!=0}">
								<c:if test="${evalist.get(e).approvalYn=='P'}">평가완료(결재중)</c:if>
								<c:if test="${evalist.get(e).approvalYn=='Y'}">평가완료</c:if></c:if></td>
							<td><c:if test="${loginUser.mid != 1}"><c:out value="${loginUser.mName}"/></c:if></td>
						</tr>
						</c:if>
						</c:forEach>
						</c:if>
						<c:if test="${empty evalist}">
						<tr class='noeval'><td colspan="7">
							<h2 style="color:lightgray; padding:50px; text-align:center;"><label><i class="exclamation circle icon"></i><c:out value="${msg}"/></label></h2>
						</td></tr>
						</c:if>
					</tbody>
			</table>
			<%-- <div class="pagingArea">
				<div class="paging" style="margin-right:15px;"><i class="angle double left icon"></i></div>
				<c:forEach var="p" begin="1" end="10">
					<div class="paging">${p}</div>
				</c:forEach>
				<div class="paging" style="margin-left:15px;"><i class="angle double right icon"></i></div>
			</div> --%>
		</div>
		<div class="hru segment"></div>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<jsp:include page="../common/evaluation.jsp"/><!-- 평가모달 이거 포함해야 됨! -->
	<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
	<script>
		$(function(){
			$(".ui.compact.dropdown").dropdown();
			$("#yearCalendar").calendar({
				type : 'year',
				initialDate : new Date()
			});
			
			$('#request').click(function(){
				$("#approvalModal").modal('hide');

				//console.log("눌렸음!");
				$("#title2").val($("#title").val());
				$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
				$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
				$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
				
			    $("#evaluationForm").attr("action", "updateEvalwithApp.eval");
				$('#evaluationForm').submit();
				var content = "결재요청 : " + $("#title").val();
				var tmid = $("#firstApp").find("input[type=hidden]").val();
				if(tmid!="" && tmid!=null && tmid!=0){
					send(tmid, content);
				}
			});
			
			$(".eval-tr").click(function(){
				var evaluationId = $(this).find("input[type=hidden]").val();
				//console.log(evaluationId);
				showEvalModal(evaluationId);
			});
		});
		
		
		function showEvalContent(num){
			var selected = num.childNodes[1].value;
			//console.log(selected);
			var evalContent = selected.split("#");
			console.log(evalContent);
			var content = "";
			var score = ['A(100%)', 'B(80%)', 'C(60%)', 'D(40%)', 'E(20%)'];
			for(var i=0;i<evalContent.length; i++){
				content += "<div align='left'>" + score[i] + ": " + evalContent[i] + "</div>";
			}
			Swal.fire({
				title: '평가기준',
				html: content,
				width : 500
			});
		}
		
		
		function completeEval(){ //평가완료일때 --> 결재로 넘어가야 함
			console.log($("#complete").val());
			var type = $("#complete").val();
			
			if(type==1){
				Swal.fire({
					  title: '저장하시겠습니까?',
					  text: "목표수립은 한번 저장하면, 변경할 수 없습니다.",
					  icon: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: '저장하기',
					  cancelButtonText : '취소하기',
					}).then((result) => {
					  if (result.value) {
					    //location.href = "";
					    $("#evaluationForm").attr("action", "updateEvalPlan.eval");
					    $("#evaluationForm").submit();
					  }
				});
			} else if(type==2){
				var rop = $(".ropinion");
				var each = $(".each");
				var scoreYn = 0;
				var yn = 0;
				for(var i=0; i<rop.length; i++){
					if(rop[i].value==""){
						yn = 1;
					}
				}
				for(var i=0; i<each.length; i++){
					if(each[i].value=="" || each[i].value==0){
						scoreYn = 1;
					}
				}
				if(yn==1){
					alert("피평가자 의견이 입력되어야 합니다.");					
				} else if(scoreYn==1){
					alert("점수가 다 입력되지 않았습니다.");
				} else{
					Swal.fire({
						  title: '평가 완료',
						  text: "전자결재로 연결하시겠습니까?",
						  icon: 'warning',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: '결재요청',
						  cancelButtonText : '다시작성',
						}).then((result) => {
						  if (result.value) {
							  
							  approvalFunction();
							  
							  //$("#evaluationForm").attr("action", "updateEvalwithApp.eval");
							  //$("#evaluationForm").submit();
						  }
					});
				}
			}
		}
		
		function approvalFunction(){
			var approvalCode =10;
			$("#attachArea").parent().addClass("disabled");
			
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					
					$("#appType").val("인사평가 마감");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thirdName = data.appBasic.mlist[2].mName;
									var thirdMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
				},
				error : function(status){
					console.log(status);
				}
			});
		}
		
		function searchFunction(){
			var mName = $("#mName").val();
			var ppCode = $("#ppCode").val();
			var year = $("#yearCalendar").find("input[type=text]").val();
			//console.log("이름 : " + mName + " / 직급코드 : " + ppCode + " / 연도 : " + year);
			
			
			$.ajax({
				url : "search.eval",
				type : "POST",
				data : {
					mName : mName,
					ppCode : ppCode,
					year : year
				},
				success : function(data){
					//console.log(data);
					var evalist = data.evalist;
					var etbody = $("#evalTable tbody");
					etbody.html("");
					
					for(var i=0; i<evalist.length; i++){
						var $td1 = $("<td>").append($("<input type='hidden'>").val(evalist[i].evaluationId)).append(evalist[i].evaluationYear);
						var $td2 = $("<td>").text(evalist[i].mid);
						var $td3 = $("<td>").text(evalist[i].mName);
						var $td4 = $("<td>").text(evalist[i].ppName);
						var $td5 = $("<td>").text(evalist[i].totalScore);
						
						if(evalist[i].approvalId==0 && evalist[i].approvalYn == 'N'){
							var $td6 = $("<td>").text("목표수립중");
						} else if(evalist[i].approvalId==0 && evalist[i].approvalYn == 'P'){
							var $td6 = $("<td>").text("목표수립완료");
						} else if(evalist[i].approvalId!=0 && evalist[i].approvalYn == 'P'){
							var $td6 = $("<td>").text("평가완료(결재중)");
						} else if(evalist[i].approvalId!=0 && evalist[i].approvalYn == 'Y'){
							var $td6 = $("<td>").text("평가완료");
						} else {
							var $td6 = $("<td>");
						}
						var $td7 = $("<td>");
						
						etbody.append($("<tr class='eval-tr' onclick='trClick(this)'>").append($td1).append($td2).append($td3).append($td4).append($td5).append($td6).append($td7));
					}
					
				},
				error : function(status){
					console.log(status);
				}
			});
		}
		

		function trClick(num){
			var evaluationId = num.firstChild.firstChild.value;
			//console.log(num.firstChild);
			
			//console.log(evaluationId);
			showEvalModal(evaluationId);
		}
		
		function showEvalModal(evaluationId){
			$.ajax({
				url : "selectEvaluationDetail.eval",
				type : "post",
				data : {
					evaluationId : evaluationId
				},
				success : function(data){
					console.log(data.eval);
					var evaluation = data.eval;
					var edlist = data.edlist;
					var $twoWay = $("#twoWay");
					$twoWay.html("");
					var $oneWayBody = $("#oneWay tbody");
					$oneWayBody.html("");
					var $idInput = $("<input type='hidden'>").attr("name","evaluationId").val(evaluation.evaluationId);
					$("#evaluationModal").find(".header").find("h2").html("인사평가").append("(" + evaluation.mName + ")");
					
					$("*[data-tab=twoWay]").addClass("active");
					$("*[data-tab=oneWay]").removeClass("active");
					$("#totalScore").html("").val("").append($idInput);
					
					for(var i=0; i<edlist.length; i++){
						if(edlist[i].category==1 && edlist[i].edContent == ""){ //양방향 평가일 경우 & 평가항목이 빈칸일 경우(목표수립)
							var $table = $("<table class='hru evaluation'>");
							var $tBody = $("<tbody>");
							
							var $th4 = $("<th colspan='4'>").html("* "+ edlist[i].evalName + " <span style='color:gray;'> (" + edlist[i].evalContent + ") </span>");
							var $tr0 = $("<tr>").append($th4);
							$tBody.append($tr0);
							
							var edId = "edlist[" + i + "].edId";
							var $input11 = $("<input type='hidden'>").val(edlist[i].edId).attr("name",edId);
							
							var $th1 = $("<th colspan='2'>").text("목표수립").append($input11);
							var $th2 = $("<th>").text("피평가자 의견");
							var $th3 = $("<th>").text("평가자 의견");
							var $tr1 = $("<tr>").append($th1).append($th2).append($th3);
							$tBody.append($tr1);
							
							var evalContent = "edlist[" + i + "].evalContent";
							var $td1 = $("<td>").append($("<textarea rows='5' cols='30' style='resize:none'>").attr("name",evalContent)); //EVAL_CONTENT
							var $td2 = $("<td>").text(">>");
							var $td3 = $("<td>").append($("<textarea rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].ropinion));
							var $td4 = $("<td>").append($("<textarea rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].gopinion));
							var $tr2 = $("<tr>").append($td1).append($td2).append($td3).append($td4)
							$tBody.append($tr2);
							
							var $td5 = $("<td colspan='5' style='text-align:right'>");
							var $label1 = $("<label class='hru label'>").text("가중치(%) : "); //EVAL_WEIGHT
							$td5.append($label1).append($("<div class='ui disabled input small' style='margin-right:20px;'>").append($("<input type='text' size='5' readonly>").val(edlist[i].evalWeight)));
							var $label2 = $("<label class='hru label'>").text("평가자점수 : ");
							$td5.append($label2).append($("<div class='ui disabled input small' style='margin-right:20px;'>").append("<input type='text' size='5' readonly>"));
							var $label3 = $("<label class='hru label'>").text("점수 : ");
							$td5.append($label3).append($("<div class='ui disabled input small' style='margin-right:20px;'>").append("<input type='text' size='5' readonly>"));
							var $tr3 = $("<tr>").append($td5);
							$tBody.append($tr3);
							
							$table.append($tBody);
							$twoWay.append($table).append($("<div class='ui divider'>"));
							

							
						} else if(edlist[i].category==1 && edlist[i].edContent != ""){ //양방향 평가일 경우 & 평가항목이 있을 경우(평가기간)
							var $table = $("<table class='hru evaluation'>");
							var $tBody = $("<tbody>");
							
							var $th4 = $("<th colspan='4'>").html("* "+ edlist[i].evalName + " <span style='color:gray;'> (" + edlist[i].evalContent + ") </span>");
							var $tr0 = $("<tr>").append($th4);
							$tBody.append($tr0);
							
							var edId = "edlist[" + i + "].edId";
							var $input11 = $("<input type='hidden'>").val(edlist[i].edId).attr("name",edId);
							
							var $th1 = $("<th colspan='2'>").text("목표수립").append($input11);
							var $th2 = $("<th>").text("피평가자 의견");
							var $th3 = $("<th>").text("평가자 의견");
							var $tr1 = $("<tr>").append($th1).append($th2).append($th3);
							$tBody.append($tr1);
							
							
							var $td1 = $("<td>").append($("<textarea rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].edContent)); //EVAL_CONTENT
							var $td2 = $("<td>").text(">>");
							var $td3 = $("<td>").append($("<textarea rows='5' class='ropinion' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].ropinion));
			
							if(edlist[i].ropinion == ""){ //피평가자 내용이 아직 빈칸이면... 
								var $td4 = $("<td>").append($("<textarea class='gopinion' rows='5' cols='30' style='resize:none; background:#eee;' readonly>").text(edlist[i].gopinion));
							} else{ //피평가자 내용이 채워졌으면...
								var gopinion = "edlist[" + i +"].gopinion";
								var $td4 = $("<td>").append($("<textarea class='gopinion' rows='5' cols='30' style='resize:none;'>").text(edlist[i].gopinion).attr("name", gopinion));							
							}
							var $tr2 = $("<tr>").append($td1).append($td2).append($td3).append($td4)
							$tBody.append($tr2);
							
							var $td5 = $("<td colspan='5' style='text-align:right'>");
							var $label1 = $("<label class='hru label'>").text("가중치(%) : "); //EVAL_WEIGHT
							$td5.append($label1).append($("<div class='ui disabled input small' style='margin-right:20px;'>").append($("<input type='text' size='5' readonly>").val(edlist[i].evalWeight)));
							var $label2 = $("<label class='hru label'>").text("평가자점수 : ");
							$td5.append($label2).append($("<div class='ui right labeled input small' style='margin-right:20px;'>").append($("<input class='tscore' type='text' size='3'>").val(100*edlist[i].evalScore/edlist[i].evalWeight)).append($("<div class='ui basic label'>").text("%")));
							var evalScore = "edlist[" + i +"].evalScore";
							var $label3 = $("<label class='hru label'>").text("점수 : ");
							$td5.append($label3).append($("<div class='ui input small' style='margin-right:20px;'>").append($("<input class='each' type='text' size='5' value='0' readonly>").attr("name", evalScore).val(edlist[i].evalScore)));
							var $tr3 = $("<tr>").append($td5);
							$tBody.append($tr3);
							
							$table.append($tBody);
							$twoWay.append($table).append($("<div class='ui divider'>"));
							
						} else if(edlist[i].category==2){
							switch(edlist[i].type){
							case 1 : var $td1 = $("<td>").text("업적"); break;
							case 2 : var $td1 = $("<td>").text("능력"); break;
							case 3 : var $td1 = $("<td>").text("태도"); break;
							}
							var edId = "edlist[" + i + "].edId";
							var $input11 = $("<input type='hidden'>").val(edlist[i].edId).attr("name",edId);
							$td1.append($input11);
							
							var etext = "edlist[" + i + "].evalScore";
							var radio = "radio" + i;
							var $td2 = $("<td class='etitle' style='cursor:pointer;' onclick='showEvalContent(this);'>").text(edlist[i].evalName).append($("<input type='hidden'>").val(edlist[i].evalContent));
							var $td3 = $("<td>").append($("<div class='ui input'>").append($("<input type='radio' value='100'>").attr('name', radio)));
							var $td4 = $("<td>").append($("<div class='ui input'>").append($("<input type='radio' value='80'>").attr('name', radio)));
							var $td5 = $("<td>").append($("<div class='ui input'>").append($("<input type='radio' value='60'>").attr('name', radio)));
							var $td6 = $("<td>").append($("<div class='ui input'>").append($("<input type='radio' value='40'>").attr('name', radio)));
							var $td7 = $("<td>").append($("<div class='ui input'>").append($("<input type='radio' value='20'>").attr('name', radio)));							
							var $td8 = $("<td>").append($("<div class='ui disabled input small'>").append($("<input type='text' size='5' readonly>").val(edlist[i].evalWeight)));
							var $td9 = $("<td>").append($("<div class='ui input small'>").append($("<input type='text' size='5' class='each' value='0' readonly>").attr("name", etext).val(edlist[i].evalScore)));
							var $tr1 = $("<tr class='evall'>").append($td1).append($td2).append($td3).append($td4).append($td5).append($td6).append($td7).append($td8).append($td9);
							$oneWayBody.append($tr1);

							var st = 100*edlist[i].evalScore/edlist[i].evalWeight;
							console.log($('input:radio[name='+ radio +']:input[value=' + st + ']'));
							$('input:radio[name='+ radio +']:input[value=' + st + ']').attr("checked", true);
						}
					}
					
					if(evaluation.approvalYn=='N' && evaluation.approvalId == 0){ //목표수립단계
						$("#complete").text("저장").val(1).prop("disabled", false);
						$("input[name='salaryRate']").val(evaluation.salaryRate).prop("readonly", true);
						$("#totalScore").val(evaluation.totalScore);
						
					} else if(evaluation.approvalYn=='P' && evaluation.approvalId == 0){ //목표수립완료
						$("#complete").text("평가마감").val(2).prop("disabled", false);
					} else if(evaluation.approvalYn=='P' && evaluation.approvalId != 0){ //결재요청중
						$("#complete").prop("disabled", true);
						$(".tscore").prop("readonly", true);
						$(".gopinion").prop("readonly", true);
						$("input[name='salaryRate']").val(evaluation.salaryRate).prop("readonly", true);
						$("#totalScore").val(evaluation.totalScore);
					
					} else{ //평가 완료
						$("#complete").text("평가완료").prop("disabled", true);
						$("#totalScore").val(evaluation.totalScore);
						$("input[name='salaryRate']").val(evaluation.salaryRate).prop("readonly", true);
					}
					
					$('#evaluationModal').modal('show');
					if(evaluation.approvalId == 0){
				        $(".tscore").on("propertychange change keyup paste input", function(){
				        	var score = $(this).val()*$(this).parent().parent().find("input").eq(0).val()/100;
				        	//console.log(score);
				        	$(this).parent().parent().find("input").eq(2).val(score);
				        	
				        	var eachInput = $(".each");
				        	var total = 0;
				        	for(var i=0; i<eachInput.length; i++){
				        		total += Number(eachInput[i].value);
				        	}
				        	$("#totalScore").val(total);
				        	
				        });
				        $(".evall").find("input[type=radio]").click(function(){
				        	//console.log($(this).val());
				        	var weight = $(this).parent().parent().parent().find("td").eq(7).find("input[type=text]").val()/100;
				        	$(this).parent().parent().parent().find("td").eq(8).find("input[type=text]").val($(this).val()*weight);
				        	
				        	var eachInput = $(".each");
				        	var total = 0;
				        	for(var i=0; i<eachInput.length; i++){
				        		total += Number(eachInput[i].value);
				        	}
				        	//console.log(total);
				        	$("#totalScore").val(total);
				        });
					}
				},
				error : function(status){
					console.log(status);
				}
			});
		}
	</script>
</body>
</html>










