<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<link rel="stylesheet" type="text/css" href="resources/js/orgchart/jquery.orgchart.css">
<script type="text/javascript" src="resources/js/orgchart/jquery.orgchart.js"></script>
<style>
	.hru.segment{
		width:100%;
	}
	#chart-container{
		width:100%;
		/* border:1px solid lightgray; */
		border-radius:10px;
		text-align:center;
	}
    .orgchart { 
    	background: #fff; 
    	margin-right:auto;
    	margin-left:auto;
    }
    .orgchart .content{
    	font-size:1.1em !important;
    	height: 100% !important;
    }
    
    .orgchart .title{
    	font-size:1.2em !important;
    }
    .orgchart .topLine, .orgchart .rightLine, .orgchart .leftLine{
    	border-color:lightgray !important;
    }
    .orgchart .lines .downLine {
    	background-color:lightgray !important;
    }
    
    .orgchart td.left, .orgchart td.right, .orgchart td.top { border-color: #aaa; }
    .orgchart td>.down { background-color: #aaa; }
    .orgchart .middle-level .title { background-color: #3886ad; }
    .orgchart .middle-level .content { border-color: #3886ad; }
    .orgchart .third-level .title { background-color: #459ead; }
    .orgchart .third-level .content { border-color: #459ead; }
    .orgchart .third-level2 .title { background-color: purple; }
    .orgchart .third-level2 .content { border-color: purple; }
 
    .orgchart .second-menu-icon {
      transition: opacity .5s;
      opacity: 0;
      right: -5px;
      top: -5px;
      z-index: 2;
      position: absolute;
    }
    .orgchart .second-menu-icon::before { background-color: rgba(68, 157, 68, 0.5); }
    .orgchart .second-menu-icon:hover::before { background-color: #449d44; }
    .orgchart .node:hover .second-menu-icon { opacity: 1; }
    .orgchart .node .second-menu {
      display: none;
      position: absolute;
      top: 0;
      right: -70px;
      border-radius: 35px;
      box-shadow: 0 0 10px 1px #999;
      background-color: #fff;
      z-index: 1;
    }
    .orgchart .node .second-menu .avatar {
      width: 60px;
      height: 60px;
      border-radius: 30px;
      float: left;
      margin: 5px;
    }
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment" style="width:99%;">
			<h1><i class="check square icon"></i>조직도</h1>
		<div class="ui divider"></div>
			<div id="chart-container" style="overflow:hidden;"></div>
		</div>
		
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>

    $(function() {
    	let datasource = ${job};
    	console.log(datasource);
    	let datasource1 = {
    			'id' : '1',
    			'name' : 'CEO',
    			'title' : '김진호 대표이사',
    			'children' : [
    				{ 'id' :'2', 'name':'본사', 'title':'서울특별시', 'className':'middle-level',
    					'children' : [
    						{'id' : '5', 'name':'인사팀', 'title':'이도훈<br>박종현<br>이범희', 'className':'third-level'},
    						{'id' : '6', 'name':'개발팀', 'title':'권연주<br>최주혜<br>이순신<br>안동환', 'className':'third-level'},
    						{'id' : '7', 'name':'기획팀', 'title':'', 'className':'third-level'},
    						{'id' : '9', 'name':'홍보팀', 'title':'', 'className':'third-level'},
    						{'id' : '8', 'name':'영업팀', 'title':'', 'className':'third-level'}
    					]
    				},
    				{ 'id' :'3', 'name':'부산점', 'title':'부산광역시', 'className':'middle-level',
    					'children' : [
    						{'id' : '5', 'name':'영업2팀', 'title':'', 'className':'third-level2'},
    						{'id' : '6', 'name': '영업3팀', 'title':'', 'className':'third-level2'}
    					]
    					
    				}
    			]
    	}
    	
        $('#chart-container').orgchart({
            'data' : datasource,
            'visibleLevel': 3,
            'nodeContent': 'title'
            
            /* ,
            'nodeID': 'id',
            'createNode': function($node, data) {
              var secondMenuIcon = $('<i>', {
                'class': 'oci oci-info-circle second-menu-icon',
                click: function() {
                  console.log("클릭!!");
                }
              });
              var secondMenu = '<div class="second-menu"><img class="avatar" src="resources/images/dohun.jpg">';
              $node.append(secondMenuIcon).append(secondMenu);
            } */
          });

  });
  
	</script>
</body>
</html>