<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<h1 style="color:black">결재대기</h1>
		<hr>
		<br>
		<div class="editArea" style="width:100%;">
					<form action="" method="post">
					<table class="hru table">
						<thead>
							<tr  class="table-title">
								<th colspan="6" class="table-title"><c:out value="* 확인후 결재승인"/></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>결재번호</th>
								<td colspan="2" style="padding-top: 50" align="center"><div class="ui mini input">
									<c:out value="1"></c:out></div></td>
								<th>기안일자</th>
								<td colspan="2" align="center"><div class="ui mini input">
									<c:out value="2020-01-02"/></div></td>
							</tr>
							<tr>
								<th>기안자</th>
								<td colspan="2" align="center"><div class="ui mini input">
									<c:out value="김진호"/></div></td>
								<th>기안부서</th>
								<td colspan="2" align="center"><div class="ui mini input">
								<c:out value="영업1팀"/></div></td>
							</tr>
							</tbody>
					</table>
					<br><br>
						<table class="hru table">
							<tr>
								<th style="width:24%">문서제목</th>
								<td colspan="2" align="center"><div class="ui mini input"><c:out value="올해 유튜브 광고관련 건의 문의 내용 입니다 확인후 연락부탁드립니다"/></div></td>
								
							</tr>
					</table>
					
					<br><br>
					<table class="hru table">
							<tr>
								<th style="width:24%;">서류종류</th>
								<td colspan="2" align="center"><div class="ui mini input"><c:out value="견적서"/></div></td>
							
							</tr>
							<tr>
								<th style="width:24%">서류매수</th>
								<td colspan="2" align="center"><div class="ui mini input"><c:out value="1"/></div></td>
							
							</tr>
					</table>
					
					<br>
						<table class="hru table" height="150px;" >
							<tr>
								<th style="width:24%;">제출 사유</th>
								<td colspan="2"  align="center" height="45%"><div class="ui mini input">
								<c:out value="1. 유튜브 광고에 대해 문의 드렸습니다 요즘 뜨고 있는 대세 유튜버 쭈헤이 유튜버에게 광고 문의 하시죠"/>
								<br>
								<c:out value="2. 유튜브의  커다란 별이 될것임이 확실합니다!!!!!! "/>
								</div></td>
								
							</tr>		
					</table>
						<br>
						<table class="hru table">
							<tr>
								<th style="width:24%;">서류매수</th>
								<td colspan="2"><div class="ui mini input"><c:out value="유튜브 광고 문의 관련 pdf"/></div></td>
							</tr>
						</table>
					<div class="buttonArea">
						<button class="ui blue button mini" id="success">결재승인</button>
						<button class="ui blue button mini" style="display:none;" id="complete">완료</button>
						<button class="ui gray button mini" id="cancel">취소</button>
					</div>
					</form>
				</div>
		
		
		
		
		
		
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->

</body>
</html>