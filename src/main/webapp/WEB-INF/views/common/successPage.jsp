<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
	<script>
		$(function(){
			var successCode = "${successCode}";
			var path = "";
			if(successCode == 1){
				alert("권한부여가 성공적으로 변경되었습니다!");
				path = "selectList.au";
			} else{
				alert("성공입니다!");
				path = "showMemberView.temp";
			}
			
			location.href=path;
		});
	</script>
</body>
</html>