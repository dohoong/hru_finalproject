<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<div class="ui modal" id="evaluationModal" style="padding:10px; width:1000px; background:#eee;">
	<i class="close icon"></i>
	<div class="header" align="center" style="background:#eee;">
		<h2>인사 평가</h2>
	</div>
	<form id="evaluationForm" method="POST">
	<div class="hru segment" style="margin-bottom:0">
		<div class="ui top attached tabular menu" id="evaluationMenu">
			<a class="active item" type="button" data-tab="twoWay">양방향평가</a>
			<a class="item" type="button" data-tab="oneWay">단방향평가</a>
		</div>
		
		<div class="ui bottom attached active tab segment" data-tab="twoWay" id="twoWay">
			<c:forEach var="e" begin="1" end="3">
			<table class="hru evaluation">
				<tr>
					<th colspan="3">목표수립</th>
					<th>피평가자 의견</th>
					<th>평가자 의견</th>
				</tr>
				<tr>
					<th>항목${e}</th>
					<td><textarea rows="5" cols="30" style="resize:none"></textarea></td>
					<td>>></td>
					<td><textarea rows="5" cols="30" style="resize:none"></textarea></td>
					<td><textarea rows="5" cols="30" style="resize:none"></textarea></td>
				</tr>
				<tr>
					<td colspan="5" style="text-align:right"><label class="hru label">가중치(%):</label>
									<div class="ui input small" style="margin-right:20px;"><input type="text" size="5" readonly value="30"></div>
						<label class="hru label">평가자점수:</label>
							<div class="ui input small" style="margin-right:20px;"><input type="number" size="5" min="0" max="100" class="score"></div>
						<label class="hru label">점수:</label>
							<div class="ui input small" style="margin-right:20px;"><input type="text" size="5" readonly value="0"></div>
					</td>
				</tr>
			</table>
			<div class="ui divider"></div>
			</c:forEach>
			<div style="text-align:right">
				<label class="hru label">평가점수 : </label>
				<div class="ui input small" style="margin-right:20px;"><input type="text" size="5" readonly></div>
				<button class="ui blue button">평가완료</button>
			</div>
		</div>
		<div class="ui bottom attached tab segment" data-tab="oneWay" id="oneWay">
			<table class="hru board2" style="width:100%;">
				<thead>
					<tr>
						<th rowspan="2" style="width:100px;">카테고리</th>
						<th rowspan="2" style="">평가항목</th>
						<th colspan="5">평가점수</th>
						<th rowspan="2" style="width:100px;">가중치(%)</th>
						<th rowspan="2" style="width:100px;">점수(점)</th>
					</tr>
					<tr>
						<th style="width:35px;">A</th>
						<th style="width:35px;">B</th>
						<th style="width:35px;">C</th>
						<th style="width:35px;">D</th>
						<th style="width:35px;">E</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="t" begin="1" end="5">
					<tr class="evall">
						<td>업적</td>
						<td class="etitle">계획, 지시에 의해 부과된 업무를 정해진 기간 내에 달성 하였는가?</td>
						<td><div class="ui input"><input type="radio" name="eval${t}" value="100"></div></td>
						<td><div class="ui input"><input type="radio" name="eval${t}" value="80"></div></td>
						<td><div class="ui input"><input type="radio" name="eval${t}" value="60"></div></td>
						<td><div class="ui input"><input type="radio" name="eval${t}" value="40"></div></td>
						<td><div class="ui input"><input type="radio" name="eval${t}" value="20"></div></td>
						<td><div class="ui input small"><input type="text" size="5" value="10" readonly></div></td>
						<td><div class="ui input small"><input type="text" size="5" value="0" readonly></div></td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div style="text-align:right" id="bottomContent">
			<label class='hru label'>진급대상자 여부 : </label>
			<select class="ui compact dropdown" name="levelupYn" style='margin-right:20px;'>
				<option value="N" selected>NO</option>
				<option value="Y">YES</option>
			</select>
			<label class="hru label">연봉 인상률 : </label>
			<div class="ui right labeled input small" style="margin-right:20px;"><input type="text" size="3" name="salaryRate"><div class='ui basic label'>%</div></div>
			<label class="hru label">평가점수 : </label>
			<div class="ui disabled input small" style="margin-right:20px;"><input type="text" size="5" id="totalScore" name="totalScore" value="0" readonly></div>
			<button class="ui blue button" type="button" id="complete" onclick="completeEval();">평가완료</button>
			
			<input type="hidden" name="title" value="제목" id="title2">
			<input type="hidden" name="firstApproval" id="firstApp2">
			<input type="hidden" name="secondApproval" id="secondApp2">
			<input type="hidden" name="thirdApproval" id="thirdApp2">
		</div>
	</div>
	</form>
</div>
<script>
	$(function(){
		$(".ui.compact.dropdown").dropdown();
        $(".score").on("propertychange change keyup paste input", function(){
        	var score = $(this).val()*$(this).parent().parent().find("input").eq(0).val()/100;
        	//console.log(score);
        	$(this).parent().parent().find("input").eq(2).val(score);
        });
        $(".evall").find("input[type=radio]").click(function(){
        	//console.log($(this).val());
        	var weight = $(this).parent().parent().parent().find("td").eq(7).find("input[type=text]").val()/100;
        	$(this).parent().parent().parent().find("td").eq(8).find("input[type=text]").val($(this).val()*weight);
        	
        });
        $(".etitle").hover(function(){
        	$(this).css("cursor","pointer");
        }).click(function(){
        	var msg = "본 항목의 평가기준은 다음과 같습니다.\nA(80~100):매우 성실하다.\nB(60~80):대체로 성실하다.\nC(40~60):보통 성실하다.\nD(20~40):별로 성실하지않다.\nE(0~20):매우 불성실하다.";
        	alert(msg);
        });
        
		
		$("#evaluationMenu").find(".item").tab();

	});
</script>










