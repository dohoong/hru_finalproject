<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>  
<head>
<meta charset="UTF-8">
<style>
	.leftMenu{
		width:5%;
		background:#0F4C81;
		min-height:100%;
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
		position:fixed;
		top:0;
		left:0;
		z-index:999;
	}
	.logoArea{
		height:50px;
	}
	.menuArea{
		height:85%;
	}
	.mainMenu{
		padding-left:20px;
		margin-top:20px;
		overflow:hidden;
	}
	.mainMenu>.title:hover{
		cursor:pointer;
	}
	.menu-div{
		display:inline-block;
		width:80%;
		color:white;
		font-size:1.5em;
	}
	.menu-div span{
		display:none;
	}
	.submenu{
		margin-bottom:5px;
		margin-left:25px;
		padding-left:5px;
		color:#ededed;
	}
	.submenu:hover{
		border-left:4px solid lightgray;
		cursor:pointer;
	}
	.hru-area{
		float:bottom;
	}
	.pimg{
		height:150px;
		width:150px;
		overflow:hidden;
		object-fit:cover;
	}
	#circle{
		position:absolute;
		right:115px;
		top:14px;
		visibility:hidden;
	}
@media all and (max-width:1280px) {
	.alermArea{
		display:none;
	}
	.mainMenu{
		padding-left:15px;
	}
}
</style>
</head>
<body>
	<c:set var="contextPath" value="${ pageContext.servletContext.contextPath }"/>
	<div class="leftMenu">
		<div class="logoArea"></div>
		<div class="menuArea" id="menu">
			<div class="ui accordion mainMenu">
				<div class="title">
					<div class="ui left icon menu-div"><i class="server icon"></i><span class="menu-first" id="system">시스템관리</span></div>
						<i class="dropdown inverted icon" style="display:none"></i>
				</div>   
				<div class="content">
					<div class="submenu" id="systemsub1" onclick="location.href='showCompanyInfo.st'">기초 환경설정</div>
					<div class="submenu" id="systemsub2" onclick="location.href='showOrganizationManageMent.st'">기준정보</div>
					<div class="submenu" id="systemsub3" onclick="location.href='selectList.au'">권한관리</div>
					<div class="submenu" id="systemsub4" onclick="location.href='showStatisticalData.st'">통계자료</div>
				</div>
			</div>
			<div class="ui accordion mainMenu">
				<div class="title">
					<div class="ui left icon menu-div"><i class="users icon"></i><span class="menu-first" id="hr"> 인사관리</span></div>
						<i class="dropdown inverted icon" style="display:none"></i>
				</div>
				<div class="content">
					<div class="submenu" id="hrsub1" onclick="location.href='showMemberInsert.me'">신규사원등록</div>
					<div class="submenu" id="hrsub2" onclick="location.href='showMemberList.hr'">사원관리</div>
					<div class="submenu" id="hrsub3" onclick="location.href='showTeamList.au'">조직관리</div>
					<div class="submenu" id="hrsub4" onclick="location.href='showEnterpriseMemberMain.hr'">인사기획</div>
				</div>
			</div>
			<div class="ui accordion mainMenu">
				<div class="title">
					<div class="ui left icon menu-div"><i class="won sign icon"></i><span class="menu-first" id="account"> 급여관리</span></div>
						<i class="dropdown inverted icon" style="display:none"></i>
				</div>
				<div class="content">
					<div class="submenu" id="paysub1" onclick="location.href='showOneDayAttend.att'">근태관리</div>
					<div class="submenu" id="paysub2" onclick="location.href='showWageBasicInformation.salary'">급여관리</div>
					<div class="submenu" id="paysub3" onclick="location.href='showYearEndAccount.me'">정산관리</div>
				</div>
			</div>
			<div class="ui accordion mainMenu">
				<div class="title">
					<div class="ui left icon menu-div"><i class="sitemap icon"></i><span class="menu-first" id="leader"> 조직책임자</span></div>
						<i class="dropdown inverted icon" style="display:none"></i>
				</div>
				<div class="content">
					<div class="submenu" id="leadersub1" onclick="location.href='showApprovalWaiting.app'">결재내역</div>
					<div class="submenu" id="leadersub2" onclick="location.href='showOrganizationManagement.me'">조직관리</div>
					<div class="submenu" id="leadersub3" onclick="location.href='showEvaluationList.eval'">인사평가</div>
				</div>
			</div>
			<div class="ui accordion mainMenu">
				<div class="title">
					<div class="ui left icon menu-div"><i class="id badge icon"></i><span onclick="location.href='showMemberView.me'" id="member"> 사원</span></div>
						<i class="dropdown inverted icon" style="display:none"></i>
				</div>
				<div class="content">
					<div class="submenu" id="membersub1" onclick="location.href='showInfoMember.me'">인적사항</div>
					<div class="submenu" id="membersub2" onclick="location.href='showInfoAttendance.att'">근태조회</div>
					<div class="submenu" id="membersub3" onclick="location.href='showInfoSalary.temp'">셀프서비스</div>
					<div class="submenu" id="membersub4" onclick="location.href='showSocializingMain.temp'">게시판</div>
				</div>
			</div>
		</div><!-- 메뉴영역 끝 -->
		<div class="hru-logo">
			<%-- <img src="${ contextPath }/resources/images/HRU.png" width="60px;"> --%>
		</div>
		
	</div> <!-- 왼쪽영역 끝 -->
	<div class="upperMenu"> 
			<div class="thirdMenu">
				<table cellspacing="20px">	
					<tr style="display:none;" id="system1"><!-- (시스템)기초 환경설정 -->
						<td><a href="showCompanyInfo.st">회사정보</a></td>
						<td><a href="showSalaryAttitudeAdministration.salary">급여/근태기준</a></td>
						<td><a href="showApprovalLine.app">결재라인</a></td>
					</tr>
					<tr style="display:none;" id="system2"><!-- (시스템)기준정보 -->
						<td><a href="showOrganizationManageMent.st">조직관리</a></td>
						<td><a href="showPositionAdministration.st">직급관리</a></td>
						<td><a href="showCalendarAdministration.temp">달력관리</a></td>
						<td><a href="showVacationAdministration.att">휴가관리</a></td>
						<td><a href="showConnectionManagement.st">접속관리</a></td>
					</tr>
					<tr style="display:none;" id="hr1"><!-- (인사)신규사원등록 -->
						<td><a href="showMemberInsert.me">신규사원등록</a></td>
						<td><a href="showNewMemberManagement.me">신규사원관리</a></td>
					</tr>
					<tr style="display:none;" id="hr2"><!-- (인사)조직관리 -->
						<td><a href="showTeamList.au">조직조회</a></td>
						<td><a href="showAuthorityMember.au">담당자설정</a></td>
						<td><a href="showOrganizationChart.app">조직도</a></td>
					</tr>
					<tr style="display:none;" id="hr3"><!-- (인사)인사 기획 -->
						<td><a href="showEnterpriseMember.hr">인사계획</a></td>
						<td><a href="showEvaluateMember.hr">인사평가</a></td>
						<td><a href="showMemberMovement.mov">인사발령</a></td>
					</tr>
					<tr style="display:none;" id="pay1"><!-- (급여근태)근태관리 -->
						<td><a href="showOneDayAttend.att">일일근태</a></td>
						<td><a href="showAttendanceManage.att">월근태</a></td>
						<td><a href="showMonthAttend.att">월 근태현황</a></td>
						<td><a href="showAttendSearch.att">기타 근무조회</a></td>
					</tr>
					<tr style="display:none;" id="pay2"><!-- (급여근태)급여관리 -->
						<td><a href="showWageBasicInformation.salary">급여 기본정보</a></td>
						<td><a href="showinsertWage.salary">급여 입력</a></td>
						<td><a href="showMonthlyWageStatus.salary">급여 지급현황</a></td>
						<td><a href="showWageStatusAndEmail.salary">이메일 발송</a></td>
						<td><a href="quitMoneyResult.salary">퇴직금 산정</a></td>
						<td><a href="showRetiringMember.salary">퇴직사원관리</a></td>
					</tr>
					<tr style="display:none;" id="pay3"><!-- (급여근태)정산관리 -->
						<td><a href="showYearEndAccount.me">연말정산 기초</a></td>
						<td><a href="showYearEndDocument.me">공제증명서류</a></td>
						<td><a href="showYearEndEtcDocument.me">기타서류</a></td>
					</tr>
					<tr style="display:none;" id="leader1"> <!-- (조직책임자)결재내역 -->
						<td><a href="showApprovalWaiting.app">결재대기</a></td>
						<td><a href="showApprovalList.app">결재내역</a></td>
					</tr>
					<tr style="display:none;" id="leader2"> <!-- (조직책임자)조직관리 -->
						<td><a href="showOrganizationManagement.me">조직관리</a></td>
						<td><a href="showOrganizationChart.app">조직도</a></td>
					</tr>
					<tr style="display:none;" id="member1"> <!-- (사원)인적사항 -->
						<td><a href="showInfoMember.me">인적사항</a></td>
						<td><a href="showInfoCareer.me">경력정보</a></td>
						<td><a href="showInfoPersonalHR.mov">인사평가</a></td>
					</tr>
					<tr style="display:none;" id="member2"> <!-- (사원)근태조회 -->
						<td><a href="showInfoAttendance.att">근태관리</a></td>
						<td><a href="showInfoVacation.att">휴가관리</a></td>
					</tr>
					<tr style="display:none;" id="member3"> <!-- (사원)셀프서비스 -->
						<td><a href="showInfoSalary.temp">급여관리</a></td>
						<td><a href="showCertificateManagement.att">제증명 관리</a></td>
						<td><a href="showInfoQuitMoney.me">퇴직금조회</a></td>
						<td><a href="showYearEndCalculator.me">연말정산</a></td>
					</tr>
					<tr style="display:none;" id="member4"> <!-- (사원)게시판 -->
						<td><a href="showSocializingMain.temp">친목도모</a></td>
						<td><a href="selectPbList.bo">건의사항</a></td>
					</tr>
				</table>
			</div>
		<div class="userMenu">
				<div class="alermArea"><i class="user secret large icon" onclick="location.href='showExample.temp'" style="display:none;"></i></div>
	            <div class="alermArea"><i class="home large icon" onclick="location.href='showMemberView.me'"></i></div>
	            <div class="alermArea"><i class="archive large icon" onclick="location.href='showApprovalRequestList.app'"></i></div>
	            <div class="alermArea"><i class="alarm large icon" id="alarm" onclick="readAlarm();"></i><div class="ui red circular mini label" id="circle"></div></div>
	            <div id="showInfo" style="color:gray; display:inline-block;">
				<c:if test="${ empty loginUser }">
					<c:out value="로그아웃 되었습니다."/>
					<%-- 로그아웃되었을 때, 로그인페이지로 돌려줌 --%>
					<script>
					$(function(){
						Swal.fire({
							  icon: 'warning',
							  title: 'WARNING',
							  text: '로그인이 필요합니다!'
							}).then((result) => {
								location.href="logout.me"; 
							});
					});
					</script>
				</c:if>
				<c:if test="${ !empty loginUser }">
					<div><c:out value="${loginUser.mhi.ppname} "/><c:out value="${ loginUser.mName }"/></div>
				</c:if>
				</div>
			</div><!-- 사용자 정보영역 끝 -->
			
	</div><!-- 상단 메뉴영역 끝 -->
	<div class="ui horizontal card" id="myInfo" style="display:none;">
		<div class="ui image pimg">
			<c:if test="${loginUser.mhi.att.changeName != null}">
				<img class="pimg" src="resources/uploadFiles/${loginUser.mhi.att.changeName}" >
			</c:if>
			<c:if test="${loginUser.mhi.att.changeName == null}">
				<img class="pimg" src="resources/images/defaultProfile.png">
			</c:if>
		</div>
		<div class="content">
			<c:if test="${!empty loginUser }">
			<div class="header"><c:out value="${loginUser.mName}"/></div>
			<div class="meta"><c:out value="${loginUser.mhi.ppname}"/> / <c:out value="${loginUser.mhi.tname}"/></div>
			<div class="description">${loginUser.email}</div>
			<div style="margin-top:20px; float:right;">
				<button class="ui blue button" id="commuteBtn">출근/퇴근</button>
				<button class="ui button" onclick="logout();">로그아웃</button>
			</div>
			</c:if>
		</div>
	</div>
	<script>
		$(function(){
			$(".leftMenu span").hide();
			$(".dropdown.inverted.icon").hide();
			$(".leftMenu").css({
				"width":"5%"
			});
			
			$(".menu-first").click(function(){	//메인메뉴 눌렀을 때, 권한체크하는 ajax
				var configId = $(this).attr("id");
				$(".ui.accordion").accordion({
					event : false,
					selector: {
						trigger: '.title .dropdown'
					}
				});	
				$.ajax({
					url : "checkMember.au",
					type : "post",
					data : {id : configId},
					success : function(data){
						//console.log(data);
						//console.log(data.authorityYn);
						
						if(data.authorityYn == "Y"){
							switch(configId){
								case "system" : location.href='showSystemView.temp'; break;
								case "hr" : location.href='showHRView.temp'; break;
								case "account" : location.href='showAccountView.temp'; break;
								case "leader" : location.href='showLeaderView.temp'; break;
							}
						} else{
							Swal.fire(
									  '접근불가',
									  '권한이 없습니다.',
									  'error'
							);
						}
						
					},
					error : function(status){
						console.log(status);
					}
				});
			}); 
			$(".title .dropdown").click(function(){ //더보기 눌렀을 때, 권한체크하는 ajax
				$(".title .dropdown").accordion('close others');
				
				var num = $(this).index();
				
				var configId = $(this).parent().find("span").attr("id");
				$.ajax({
					url : "checkMember.au",
					type : "post",
					data : {id : configId},
					success : function(data){
						if(data.authorityYn == "Y"){
							$(".ui.accordion").accordion({
								selector: {
									trigger: '.title .dropdown'
								}
							});

						} else{
							$(".title .dropdown").accordion('close (num)');
							Swal.fire(
									  '접근불가',
									  '권한이 없습니다.',
									  'error'
							);
						}
					},
					error : function(status){
						console.log(status);
					}
				});
				
			});
			
			
			$(".leftMenu").mouseenter(function(){
				$(this).stop().animate({"width":"18%"}, 400);
				$(".leftMenu span").stop().delay(300).fadeIn(800);
				$(".dropdown.inverted.icon").stop().delay(300).fadeIn(800);
			}).mouseleave(function(){
				$(".ui.accordion").accordion('close', 0);
				$(".leftMenu span").stop().hide();
				$(".dropdown.inverted.icon").stop().hide();
				$(this).stop().animate({"width":"5%"}, 500);
			});
			
			/* $("#test").click(function(){
				$('body')
				  .toast({
					position: 'bottom right',
				    displayTime: 0,
				    title: 'Message',
				    message: '테스트용 토스트입니다!'
				  });
			}); */
			
			//상단메뉴에 대한 부분!
			$(".ui.horizontal.card").hide();

			$(".alarm.large.icon").mouseenter(function() {
				$(this).transition('tada');
			});
			$("#showInfo").hover(function(){
				$(this).css("cursor", "pointer");
			}).click(function(){
				$(".ui.horizontal.card").show();
				$(".ui.horizontal.card").mouseleave(function(){
					$(this).hide();
				});
			});
			
			
			//시스템관리 서브메뉴 페이지 이동
			$("#systemsub1").click(function(){ //기초환경설정
				$(".thirdMenu").find("tr").hide();
				$("#system1").show();
			});
			$("#systemsub2").click(function(){ //기준정보
				$(".thirdMenu").find("tr").hide();
				$("#system2").show();
			});
			$("#systemsub3").click(function(){ //권한관리
				$(".thirdMenu").find("tr").hide();
			});
			$("#systemsub4").click(function(){ //통계자료
				$(".thirdMenu").find("tr").hide();

			});
			//인사관리 서브메뉴 페이지 이동
			$("#hrsub1").click(function(){ //신규사원등록
				$(".thirdMenu").find("tr").hide();
				$("#hr1").show();
			});
			$("#hrsub2").click(function(){ //사원관리
				$(".thirdMenu").find("tr").hide();
			});
			$("#hrsub3").click(function(){ //부서관리
				$(".thirdMenu").find("tr").hide();
				$("#hr2").show();
			});
			$("#hrsub4").click(function(){ //인사기획
				$(".thirdMenu").find("tr").hide();
				$("#hr3").show();
			});
			
			//급여, 근태관리 서브메뉴 페이지 이동
			$("#paysub1").click(function(){ //근태관리
				$(".thirdMenu").find("tr").hide();
				$("#pay1").show();
			});
			$("#paysub2").click(function(){ //급여관리
				$(".thirdMenu").find("tr").hide();
				$("#pay2").show();
			});
			$("#paysub3").click(function(){ //정산관리
				$(".thirdMenu").find("tr").hide();
				$("#pay3").show();
			});
			
			//조직책임자 서브메뉴 페이지 이동
			$("#leadersub1").click(function(){ //결재내역
				$(".thirdMenu").find("tr").hide();
				$("#leader1").show();
			});
			$("#leadersub2").click(function(){ //조직관리
				$(".thirdMenu").find("tr").hide();
				$("#leader2").show();
			});
			$("#leadersub3").click(function(){ //조직관리
				$(".thirdMenu").find("tr").hide();
			});
			
			//사원 서브메뉴 페이지 이동
			$("#membersub1").click(function(){
				$(".thirdMenu").find("tr").hide();
				$("#member1").show();
			});
			$("#membersub2").click(function(){
				$(".thirdMenu").find("tr").hide();
				$("#member2").show();
			});
			$("#membersub3").click(function(){
				$(".thirdMenu").find("tr").hide();
				$("#member3").show();
			});
			$("#membersub4").click(function(){
				$(".thirdMenu").find("tr").hide();
				$("#member4").show();
			});
			$("#membersub5").click(function(){
				$(".thirdMenu").find("tr").hide();
				$("#member5").show();
			});
			$("#membersub6").click(function(){
				$(".thirdMenu").find("tr").hide();
				$("#member6").show();
			});
		});
	
		//웹소켓 관련
        var ws;
        var wsAddress = "ws://";
        var cnt = "${alarmCnt}";
        var mid = "${loginUser.mid}";
       	$(function(){
       		var link = document.location.href.split('/');
       		wsAddress += link[2] + "/hru/echo.so";
       		openSocket();
       		if(cnt>0){
            	$("#circle").css("visibility", "visible").text(cnt);
            	$("#alarm").css("color", "orangered");
       		} else{
            	$("#circle").css("visibility", "hidden").text(0);
       		}
       	});
		//로그아웃하는 함수
        function logout(){
        	ws.close();
        	location.href='logout.me';
        }
       	//웹소켓 연결하는 함수
        function openSocket(){
            if(ws!==undefined && ws.readyState!==WebSocket.CLOSED)
            {
                writeResponse("이미 연결중입니다.");
                return;
            } 
            
            //웹소켓 객체 만드는 코드
            ws = new WebSocket(wsAddress);
            
            ws.onopen=function(event){
                if(event.data===undefined) return;
                writeResponse(event.data);
            };
            ws.onmessage=function(event){
                writeResponse(event.data);
            };
            ws.onclose=function(event){
                writeResponse("Connection closed");
            }
        }
       	
       	//메시지 보내는 코드
        function send(tmid, content){
            //var content = "쪽지 : " + document.getElementById("messageinput").value;
            var smid = "${loginUser.mid}";
            
            var sendMessage = {
            	    type: "message",
            	    content: content,
            	    tmid: tmid,
            	    smid: smid
            	  };
			ws.send(JSON.stringify(sendMessage));
            
        }
       	
       	//서버로부터 온 메시지 읽기
        function writeResponse(text){
        	var takeMessage = JSON.parse(text);
       		//console.log(takeMessage);
        	//var alarmDate = new Date(takeMessage.alarmDate);
			//var tformat = moment(time).format('YYYY-MM-DD HH:mm');
        	var tmid = takeMessage.tmid;
        	var alarmId = takeMessage.alarmId;
        	
        	
        	if(mid==tmid && text != "Connection closed"){ //받는사람이 나일때
	        	$("#alarm").css("color", "orangered").transition('tada').transition('tada').transition('tada').transition('tada');
      			$.ajax({
      				url : "alarmCountUp.app",
      				type : "get",
      				data : {
      					cnt : cnt,
      					alarmId : alarmId
      				},
      				success : function(data){
      					//console.log(cnt);
      					if(cnt!="" && cnt!=0){
		      				$("#circle").text(++cnt);
      					} else{
      						cnt = 1;
		      				$("#circle").css("visibility", "visible").text(cnt);
      					}
      		        	openToast(data.alarm.alarmId, data.alarm.alarmDate, data.alarm.content);
      				}
      			});	
        	}
        }
       	
       	//알람표시 눌렀을 때
        function readAlarm(){
        	$.ajax({
        		url:"selectListAlarm.app",
        		type:"post",
        		success:function(data){
        			var alarm = data.alist;
        			for(var i=0; i<alarm.length; i++){
        				openToast(alarm[i].alarmId, alarm[i].alarmDate, alarm[i].content);
        			}
        		
        		},
        		error:function(status){
        			console.log(status);
        		}
        	});
        }
       	
       	function openToast(alarmId, alarmDate, content){
       		var category = content.split(" : ");
       		var toastColor;
       		//console.log(category[0]);
       		if(category[0]=='쪽지'){
       			toastColor = 'purple';
       		} else if(category[0]=='결재요청' || category[0]=='결재완료'){
       			toastColor = 'blue';
       		} else if(category[0]=='결재반려'){
       			toastColor = 'red';
       		} else{
       			toastColor = 'white';			
       		}
       		var random = Math.random()+1;
       		$('body').toast({
  				position: 'bottom right',
  			    displayTime: 2000*random,
  			    class : toastColor,
  			 	className: {
  		        toast: 'ui message'
  		   		},
  			    title: content,
  			    message: alarmDate,
				showProgress: 'top',
  			    classActions: 'basic right',
  			    actions:	[{
  			        text: 'OK!',
  			        class: toastColor,
  			        click: function() {
  			        	$.ajax({
  			        		url : "updateAlarm.app",
  			        		type : "post",
  			        		data : {
  			        			alarmId : alarmId,
  			        			cnt : cnt
  			        		},
  			        		success:function(data){
  			        			//console.log(data);
  			        			$("#circle").text(--cnt);
  			        			if(cnt<=0){
  			        	        	$("#alarm").css("color", "gray").transition('stop');
  			        	        	$("#circle").css("visibility", "hidden").text(0);
  			        			}
  			        			
  			        		}
  			        	});
  			        }
  			    }]
  			  });
       	
       	}
       	
       	//출퇴근 버튼
       	$("#commuteBtn").click(function(){
       		$.ajax({
       			url:"commuteCheck.att",
       			type:"post",
       			success:function(data){
       				console.log("출근/퇴근 aja성공");
       				console.log(data);
       				if(data.result == 1){
       					//출근
       					Swal.fire(
							  '출근 완료',
							  '출근 체크가 완료되었습니다.',
							  'success'
					)
       				}else{
       					//퇴근
       					Swal.fire(
							  '퇴근 완료',
							  '퇴근 체크가 완료되었습니다.',
							  'success'
					)
       				}
       			},
       			error:function(status){
       				console.log("fail!");
       			}
       			
       		});
       	});
       	
        
	</script>
</body>
</html>