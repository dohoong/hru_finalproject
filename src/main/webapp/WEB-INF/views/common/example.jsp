<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>hru</title>
<jsp:include page="../common/importlib.html"/>
<style>
.easterEgg:hover{
	cursor:pointer;
}
@keyframes twink
{
    0% { color: red; }
    25% { color: yellow; }
    50% { color: green; }
    75% { color: blue; }
    100% { color: violet; }
}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/>
	<div class="outer"><!--outer Area -->
		<div class="contentArea"> <!-- 내용부분! -->
			<div class="hru segment">
				<button class="ui blue button" id="test">토스트테스트!</button>
				<button class="ui button" onclick="location.href='showExcelTest.app'">엑셀테스트</button>
				<button class="ui red button" onclick="location.href='showWebSocket.so'">웹소켓테스트</button>
				<button class="ui button" onclick="location.href='showTestCalendar.temp'">TEST</button>
				<button class="ui purple button" onclick="location.href='showOrganizationChart.app'">조직도!</button>
				<button class="ui button" onclick="location.href='showCountUpTest.temp'">카운트업 테슽</button>
			</div>
		
			<div class="hru segment">
				<h1><i class="check square icon"></i>내 정보</h1>
				<table class="hru table">
					<tr>
						<th>사원번호</th>
						<td><c:out value="20180202"/></td>
						<th>이름</th>
						<td><label class="easterEgg" style="animation: twink 2s infinite;"><c:out value="이도훈"/></label></td>
					</tr>
					<tr>
						<th>사업장</th>
						<td colspan="3"><c:out value="오렌지레드 컴퍼니"/></td>
					</tr>
					<tr>
						<th>부서</th>
						<td><c:out value="인사팀"/></td>
						<th>직급</th>
						<td><c:out value="대리"/></td>
					</tr>
				</table>
			</div> <!-- 내정보 끝 -->
			<div class="hru segment">
				<h1><i class="check square icon"></i>캘린더</h1>
				<div style="height:600px; width:100%; border:1px solid lightgray; border-radius:10px;">
					<table align="center">
						<tr>
							<td><img src="resources/images/dohoon.jpg" class="ui centered small images transition hidden" id="img1" style="margin:0 auto"></td>
							<td><img src="resources/images/dohoon.jpg" class="ui centered small images transition hidden" id="img2" style="margin:0 auto"></td>
							<td><img src="resources/images/dohoon.jpg" class="ui centered small images transition hidden" id="img3" style="margin:0 auto"></td>
						</tr>
						<tr>
							<td><img src="resources/images/dohoon.jpg" class="ui centered small images transition hidden" id="img4" style="margin:0 auto"></td>
							<td><img src="resources/images/dohoon.jpg" class="ui centered small images transition hidden" id="img5" style="margin:0 auto"></td>
							<td><img src="resources/images/dohoon.jpg" class="ui centered small images transition hidden" id="img6" style="margin:0 auto"></td>
						</tr>
						<tr>
							<td><img src="resources/images/dohoon.jpg" class="ui centered small images transition hidden" id="img7" style="margin:0 auto"></td>
							<td><img src="resources/images/dohoon.jpg" class="ui centered small images transition hidden" id="img8" style="margin:0 auto"></td>
							<td><img src="resources/images/dohoon.jpg" class="ui centered small images transition hidden" id="img9" style="margin:0 auto"></td>
						</tr>
					</table>
					
					
					<script>		
					$(".easterEgg").click(function(){
						$('.images').transition({
							animation : 'zoom',
						    reverse   : 'auto', // default setting
						    interval  : 200,
						    onComplete : function(){
						    	$('.images').transition({
								    animation : 'zoom',
								    reverse   : 'auto', // default setting
								    interval  : 200,
								    onComplete : function(){
								    	$('.images') .transition({
								    		animation : 'tada',
										    reverse   : 'auto', // default setting
										    interval  : 200
								    	}).transition({
										    animation : 'fly down',
										    reverse   : 'auto', // default setting
										    interval  : 200,
										    onComplete : function() {
										    	$('.images') .transition({
										    		animation : 'fly up',
												    reverse   : 'auto', // default setting
												    interval  : 200
										    	}).transition({
												    animation : 'fly down',
												    reverse   : 'auto', // default setting
												    interval  : 200
												});
										    }
										});
								    }
								});
						    }
						});
					});
					</script>
				</div>
			</div><!-- 캘린더 끝 -->
			<div class="ui horizontal segments">
				<div class="ui segment"><h1><i class="check square icon"></i>공지사항</h1>
					<table class="hru board">
						<thead>
							<tr>
								<th>작성일자</th>
								<th>제목</th>
								<th>작성자</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>2019-12-01</td>
								<td>인사평가 기간입니다. 얼른 작성하세요.</td>
								<td>인사팀장</td>
							</tr>
							<tr>
								<td>2019-01-01</td>
								<td>공지사항 게시판이 열렸습니다.</td>
								<td>대표이사</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="ui segment" style="width:400px;"><h1><i class="check square icon"></i>전자결재</h1>
					<table class="hru board">
						<thead>
							<tr>
								<th>기안일자</th>
								<th>제목</th>
								<th>종류</th>
								<th>상태</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>2019-12-22</td>
								<td>연차결재 신청합니다.</td>
								<td>연차신청</td>
								<td>결재중</td>
							</tr>
							<tr>
								<td>2019-12-19</td>
								<td>인사평가 목표 작성완료하였습니다.</td>
								<td>인사평가 계획</td>
								<td>결재완료</td>
							</tr>
						</tbody>
					</table>
					<div class="pagingArea">
						<div class="paging" style="margin-right:15px;"><i class="angle double left icon"></i></div>
						<c:forEach var="p" begin="1" end="10">
							<div class="paging">${p}</div>
						</c:forEach>
						<div class="paging" style="margin-left:15px;"><i class="angle double right icon"></i></div>
					</div>
				</div>
			</div><!-- 공지사항/전자결재 끝 -->
			<div class="hru segment">
				<h1><i class="check square icon"></i>부서관리</h1>
				<div class="scrollArea" style="width:50%;">
					<table class="hru scroll table">
						<thead>
							<tr>
								<th>부서코드</th>
								<th>부서명</th>
								<th>부서장</th>
								<th>사용여부</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="t" begin="1" end="10">
								<tr class="scroll-tr">
									<td>${t}</td>
									<td>인사팀${t}</td>
									<td>이도훈${t}</td>
									<td>Y</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="editArea" style="width:40%;">
					<form action="" method="post">
					<table class="hru table">
						<thead>
							<tr  class="table-title">
								<th colspan="3" class="table-title">수 정</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>부서코드</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="code" readonly></div></td>
							</tr>
							<tr>
								<th>부서명</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="name" readonly></div></td>
							</tr>
							<tr>
								<th>조직책임자</th>
								<td colspan="2"><div class="ui mini input">
									<input type="text" id="leader" readonly></div></td>
							</tr>
							<tr>
								<th>사용여부</th>
								<td><div class="ui radio checkbox"><input type="radio" id="use" name="yn"><label for="use"> 사용</label></div></td>
								<td><div class="ui radio checkbox"><input type="radio" id="nouse" name="yn"><label for="nouse"> 비사용</label></div></td>
							</tr>
						</tbody>
					</table>
					</form>
					<div class="buttonArea">
						<button class="ui blue button mini" id="add">추가</button>
						<button class="ui blue button mini" style="display:none;" id="complete">완료</button>
						<button class="ui blue button mini" id="modify">수정</button>
						<button class="ui blue button mini" style="display:none;" id="modiComplete">완료</button>
						<button class="ui gray button mini" id="delete">삭제</button>
					</div>
				</div>
				<script>
					$(function(){
						$(".scroll-tr").click(function(){
							//console.log($(this).find("td").eq(0).text());
							$("#code").val($(this).find("td").eq(0).text()).attr("readonly",true);
							$("#name").val($(this).find("td").eq(1).text()).attr("readonly",true);
							$("#leader").val($(this).find("td").eq(2).text()).attr("readonly",true);
							if($(this).find("td").eq(3).text() == "Y"){
								$("#use").prop("checked", true);
								//console.log("Y");
							} else{
								$("#nouse").prop("checked", true);
								//console.log("N");
							}
							$("#add").show();
							$("#complete").hide();
							
						});
						$("#add").click(function(){
							$("#code").val("").attr("readonly",false);
							$("#name").val("").attr("readonly",false);
							$("#leader").val("").attr("readonly",false);
							$("#add").hide();
							$("#complete").show();
							$("#use").prop("checked", false);
							$("#nouse").prop("checked", false);
						});
						$("#modify").click(function(){
							$("#code").attr("readonly",false);
							$("#name").attr("readonly",false);
							$("#leader").attr("readonly",false);
							$("#modify").hide();
							$("#modiComplete").show();							
						});
					});
				</script>
			</div><!-- 부서관리 끝 -->
			<div class="hru segment">
				<div class="searchArea">
					<form action="" method="get">
					<table align="center" cellspacing="10px">
						<tr>
							<td><h3>검색어 </h3></td>
							<td>
								<div class="ui compact selection dropdown">
									<input type="hidden" name="category"><i class="dropdown icon"></i>
										<div class="default text">카테고리</div>
									<div class="menu">
										<div class="item" data-value="1">사원명</div>
										<div class="item" data-value="2">부서명</div>
										<div class="item" data-value="3">직급명</div></div>
								</div>						
							</td>
							<td>
								<div class="ui icon input"><input type="text" size="30" placeholder="Search..."><i class="search icon"></i></div>
							</td>
							<td width="200px">
								<div class="ui checkbox"><input type="checkbox" id="include"><label for="include">퇴직자 포함</label></div></td>
							<td>
								<button class="ui blue button">검색하기</button>
							</td>
						</tr>
					</table>
					</form>
				</div><!-- searchArea end -->
				<script>
					$(function(){
						$(".ui.selection.dropdown").dropdown();
					});
				</script>
			</div><!-- 검색영역 끝 -->
			<div class="hru segment">
				<table class="hru board2" border="1">
					<thead>
						<tr>
							<th>No</th>
							<th>지급기준일</th>
							<th>급여작업구분</th>
							<th>실지급금액</th>
							<th>급여명세서</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="pay" begin="1" end="10">
						<tr>
							<td>${pay}</td>
							<td>2019-12-24</td>
							<td>정상급여</td>
							<td>4,809,300</td>
							<td><i class="ui icon print"></i></td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="pagingArea">
					<div class="paging" style="margin-right:15px;"><i class="angle double left icon"></i></div>
					<c:forEach var="p" begin="1" end="10">
						<div class="paging">${p}</div>
					</c:forEach>
					<div class="paging" style="margin-left:15px;"><i class="angle double right icon"></i></div>
				</div>
			</div><!-- 테이블영역 끝! -->
			<div class="hru segment">
				<div class="ui top attached tabular menu">
 					<a class="active item" data-tab="first">경력/학력정보</a>
 					<a class="item" data-tab="second">자격/어학정보</a>
					<a class="item" data-tab="third">인사정보</a></div>
				<div class="ui bottom attached active tab segment" data-tab="first">
					<h1><i class="check square icon"></i>경력정보</h1>
	 				<table class="hru board2" border="1">
						<thead>
							<tr>
								<th>회사명</th>
								<th>입사날짜</th>
								<th>퇴사날짜</th>
								<th>업무내용</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="c" begin="1" end="3">
							<tr>
								<td>회사명${c}</td>
								<td>2019-01-01</td>
								<td>2019-12-30</td>
								<td>업무내용${c}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<div class="buttonArea">
						<button class="ui blue button mini" id="add">추가</button>
						<button class="ui blue button mini" id="modify">수정</button>
						<button class="ui gray button mini" id="delete">삭제</button>
					</div>
 				</div><!-- 경력/학력 정보 끝 -->
				<div class="ui bottom attached tab segment" data-tab="second">
					자격/어학정보</div><!-- 자격/어학 정보 끝 -->
				<div class="ui bottom attached tab segment" data-tab="third">
					인사정보</div><!-- 인사정보 끝 -->
			<script>
				$(function(){
					$(".menu .item").tab();
				});
			</script>
			</div><!-- 인적사항  끝! -->
			<div class="hru segment">
				<div class="ui form" style="/* width:1000px; */">
					<div class="two fields">
						<div class="field">
							<label>Start date</label>
							<div class="ui calendar" id="rangestart"><div class="ui input left icon">
									<i class="calendar icon"></i> <input type="text" placeholder="Start">
								</div>
							</div>
						</div>
						<div class="field">
							<label>End date</label>
							<div class="ui calendar" id="rangeend"><div class="ui input left icon">
									<i class="calendar icon"></i> <input type="text" placeholder="End">
								</div>
							</div>
						</div>
					</div>
				</div>
				<script>
					$("#rangestart").calendar({
						type:'date',
						endCalendar : $('#rangeend')
					});
					$("#rangeend").calendar({
						type:'date',
						startCalendar : $('#rangestart')
					});
				</script>
			</div><!-- 포멘틱 캘린더 끝! -->
			
			<div class="hru segment">
			<!-- 월단위 선택 캘린더 -->
				<div class="ui calendar" id="month_year_calendar">
					<div class="ui input left icon">
					<i class="calendar icon"></i>
					<input type="text" placeholder="근무년월 선택">
				</div>
				</div>
				<script>
				$('#month_year_calendar').calendar({
				    type: 'month'
				  });
				</script>
			</div>
			
			<div class="hru segment">
			<i class="exclamation circle icon" id="exclamation"></i>
				<div class="ui message">
					<ul class="list">
						<li>사업장과 부서, 근무 년월을 선택시, 해당 소속된 사원리스트가 표시됩니다.</li>
						<li>간단한 사원정보가 표시되고 사원을 눌렀을 때 그 사원의 월근태가 아래 목록에 표시됩니다.</li>
					</ul>
				</div>
				
				<div>
				<div class="ui right pointing inverted yellow label" style="color:white;">TIP</div>
				<span>토요일이 정상근무인 경우에는 '토요근무산정방법'을 정상근무로 선택한 후 토요근무시간을 입력해야 합니다.</span>
				</div>
			</div><!-- 추가 설명부분 끝! -->
			
			<div class="hru segment">
				<h1><i class="check square icon"></i>월근태 현황</h1>
				<table class="hru board2" border="1">
					<thead>
						<tr>
							<th rowspan="2">사원번호</th>
							<th rowspan="2">성명</th>
							<th rowspan="2">부서</th>
							<th rowspan="2">직급</th>
							<c:forEach var="day" begin="1" end="16">
								<th>${day}</th>
							</c:forEach>
							<th rowspan="2">비고</th>
						</tr>
						<tr>
							<c:forEach var="day" begin="17" end="32">
								<c:if test="${day<=31}">
									<th>${day}</th>
								</c:if>
								<c:if test="${day>31}">
									<th></th>
								</c:if>
							</c:forEach>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="2">125031</td>
							<td rowspan="2">이도훈</td>
							<td rowspan="2">개발팀</td>
							<td rowspan="2">대리</td>
							<c:forEach var="day" begin="1" end="16">
								<td><i class="ui circle grey icon"></i></td>
							</c:forEach>
							<td rowspan="2"></td>
						</tr>
						<tr>
							<c:forEach var="day" begin="17" end="32">
								<c:if test="${day<=31}">
									<td><i class="ui circle grey icon"></i></td>
								</c:if>
								<c:if test="${day>31}">
									<td></td>
								</c:if>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div><!-- 월근태현황 끝! -->
			<div class="hru segment">
				<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
				<button class="ui blue button" onclick="return approvalFunction();">결재요청모달보기</button>
				<button class="ui blue button" id="showApproval2" >결재내용모달보기</button>
			<script>

			function approvalFunction(){
				var approvalCode = 1; 
				$.ajax({
					url : "selectOneBasic.app",
					type : "post",
					data : {approvalCode : approvalCode},
					success : function(data){
						
						$("#appType").val("신규사원등록");
						var usermid = "${loginUser.mid}"; //기안자사번
						var yesApp = 0;
						
						for(var i=0; i<data.appBasic.mlist.length; i++){
							if(data.appBasic.mlist[i].mid == usermid){
								yesApp = 1;
							}
						}
						
						if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
							
							if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
								var firstName = data.appBasic.mlist[0].mName;
								var firstMid = data.appBasic.mlist[0].mid;
								var $input = $("<input type='hidden' name='first'>").val(firstMid);
								$("#firstApp").text(firstName).append($input);

								if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
									var secondName = data.appBasic.mlist[1].mName;
									var secondMid = data.appBasic.mlist[1].mid;
									var $input = $("<input type='hidden' name='second'>").val(secondMid);
									$("#secondApp").text(secondName).append($input);
									
									if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
										var thidName = data.appBasic.mlist[2].mName;
										var thidMid = data.appBasic.mlist[2].mid;
										var $input = $("<input type='hidden' name='third'>").val(thirdMid);
										$("#thirdApp").text(thirdName).append($input);
									}
								}
							}
						}
						$("#approvalModal").modal('show');
					},
					error : function(status){
						console.log(status);
					}
				});
				
				
				return false;
			}
				$(function(){

					$('#request').click(function(){
						$("#approvalModal").modal('hide');
						location.href="test.app";

					});
					
					
					
					$("#showApproval").click(function(){
						$("#approvalModal").modal('show');
					});

					$("#showApproval2").click(function(){
						$("#approvalModal2").modal('show');
					});
				});
			</script>
			</div><!-- 결재모달 불러오기 끝! -->

			<div class="hru segment">
				<jsp:include page="../common/evaluation.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
				<button class="ui blue button" id="showEvaluation">인사평가내역보기</button>
				<script>
					$(function(){
						$('#showEvaluation').click(function(){
							$('#evaluationModal').modal('show');
						});
					});
				</script>
			</div>

			
			<h1><i class="check square icon"></i>첨부파일 😉</h1><!-- 첨부파일 시작 -->
			<div class="ui icon input" id="FileDiv">
				<input type="text" id="fileBtn" placeholder="파일명" style="width:400px" readonly>
				<i class="upload icon"></i>
				<input type="file" id="inputFile" name="inputFile">
			<script>
				$("#fileBtn").click(function() {
					$(this).parent().find("input:file").click();
				});
	
				$("input:file[name='inputFile']").on('change', function(e) {
					var name = null;
					if(e.target.files[0] != null) {
						console.log(e.target.files[0]);
						name = e.target.files[0].name;
					}
					$("input:text[id='fileBtn']").val(name);
				});
			</script>
			</div><!-- 첨부파일 끝! -->
			

		</div><!-- contentArea End -->
		
	</div>
	
</body>
</html>