<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<div class="ui modal" id="approvalModal" style="padding:10px; width:1000px;">
	<i class="close icon"></i>
	<div class="header" align="center">
		<h2>전자결재</h2>
	</div>
	<div class="hru segment">
		<table class="hru table">
			<tr>
				<th width="150px;">*기안자</th>
				<td><c:out value="${loginUser.mName}"/></td>
				<th width="150px;">*기안일자</th>
				<td><div class="ui calendar" id="date_calendar">
					<div class="ui input left icon small"><i class="calendar icon"></i>
					<input type="text" placeholder="Date" readonly></div></div></td>
			</tr>
			<tr>
				<th>*결재종류</th>
				<td><div class="ui input small"><input type="text" id="appType" readonly></div></td>
				<th>첨부파일</th>
				<td><div class="ui input right icon small" id="attachDiv"><input type="text" id="attachArea" readonly><i class="upload icon"></i></div>
					<!-- <input type="file" id="attach" style="display:none"> -->
				</td>
			</tr>
			<tr>
				<th>*결재 제목</th>
				<td colspan="3"><div class="ui input small"><input type="text" size="100" id="title"></div></td>
			</tr>
		</table>
	</div>
	<div class="hru segment" style="margin-bottom:0;">
		<h3><i class="check square icon"></i>결재라인</h3>
		<table class="hru board2" id="approvalLine">
			<tr>
				<th rowspan="4" style="">결재</th>
				<th width="210px">본인</th>
				<th width="210px">1차결재</th>
				<th width="210px">2차결재</th>
				<th width="210px">3차결재</th>
			</tr>
			<tr>
				<td><c:out value="${loginUser.mName}"/></td>
				<td id="firstApp"><input type="hidden" value="0"></td>
				<td id="secondApp"><input type="hidden" value="0"></td>
				<td id="thirdApp"><input type="hidden" value="0"></td>
			</tr>
			<tr style="height:90px;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td id="firstDate"></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<div class="buttonArea" style="text-align:center">
			<button class="ui blue button" id="request">결재요청</button>
			<button class="ui gray button" onclick="closeModal();">취소하기</button>
		</div>
	</div>
</div>
<div class="ui modal" id="approvalModal2" style="padding:10px; width:1000px; background:#eee;">
	<i class="close icon"></i>
	<div class="header" align="center" style="background:#eee">
		<h2>전자결재</h2>
	</div>
	<div class="hru segment" style="margin-bottom:0">
		<div class="ui top attached tabular menu" id="approvalMenu">
			<a class="active item" data-tab="status">결재상태</a>
			<a class="item" data-tab="content">품의내용</a>
		</div>
		<div class="ui bottom attached active tab segment" data-tab="status">
			<table class="hru table" id="appDetail1">
				<tr>
					<th width="150px;">*기안자</th>
					<td>최주혜</td>
					<th width="150px;">*기안일자</th>
					<td><div class="ui calendar" id="date_calendar">
							<div class="ui input left icon small">
								<i class="calendar icon"></i> <input type="text" placeholder="Date" value="2020-01-02" readonly>
							</div>
						</div></td>
				</tr>
				<tr>
					<th>*결재종류</th>
					<td><div class="ui input small">
							<input type="text" value="연차사용" readonly>
						</div></td>
					<th>첨부파일</th>
					<td><div class="ui input right icon small">
							<input type="text" id="attachArea2" readonly><i class="download icon"></i>
						</div></td>
				</tr>
				<tr>
					<th>*결재 제목</th>
					<td colspan="3"><div class="ui input small"><input type="text" size="100" readonly></div></td>
				</tr>
			</table>
			<h3><i class="check square icon"></i>결재라인</h3>
			<form action='updateSuccess.app' method='POST' id='successForm'>
			<table class="hru board2" id="appDetail2">
				<tr>
					<th rowspan="4" style="">결재</th>
					<th width="210px">본인</th>
					<th width="210px">1차결재</th>
					<th width="210px">2차결재</th>
					<th width="210px">3차결재</th>
				</tr>
				<tr>
					<td>최주혜</td>
					<td>홍길동</td>
					<td>이도훈</td>
					<td>권연주</td>
				</tr>
				<tr style="height:90px;">
					<td><i class="check circle blue icon huge" style="color:#0F4C81"></i></td>
					<td><i class="check circle blue icon huge" style="color:#0F4C81"></i></td>
					<td><i class="times circle blue icon huge" style="color:#c93b30"></i></td>
					<td></td>
				</tr>
				<tr>
					<td>2020-01-02</td>
					<td>2020-01-03</td>
					<td>2020-01-06</td>
					<td></td>
				</tr>
				<tr>
					<th>의견</th>
					<td>-</td>
					<td>-</td>
					<td>내용이 부실해요</td>
					<td></td>
				</tr>
			</table>
			</form>
			<div class="buttonArea" style="text-align:center">
			<button class="ui blue button" id="approve" disabled>승인하기</button>
			<button class="ui red button" id="nonono" disabled>반려하기</button>
		</div>
		</div><!-- 결재상태 끝 -->
		<div class="ui bottom attached tab segment" data-tab="content">
			<table class="hru table" id="appDetail3">
				<tr>
					<th width="150px;">*결재종류</th>
					<td><div class="ui input small">
							<input type="text" value="연차사용" readonly>
						</div></td>
					<th width="150px;">첨부파일</th>
					<td><div class="ui input right icon small">
							<input type="text" id="attachArea3" readonly><i class="download icon"></i>
						</div></td>
				</tr>
				<tr>
					<th>*결재 제목</th>
					<td colspan="3"><div class="ui input small"><input type="text" size="100" readonly></div></td>
				</tr>
			</table>
		</div><!-- 결재내용 끝 -->





	</div>

</div>
<script>
	$(function(){
		var date = new Date();
		//console.log(date);
		var year  = date.getFullYear();
        var month = date.getMonth() + 1; // 0부터 시작하므로 1더함 더함
        if(month < 10){ month = "0" + month;}
        var day   = date.getDate();
        if(day < 10){day = "0" + day;}
        
		$('#date_calendar').find("input").val(year + "-" + month + "-" + day);
		$("#approvalLine").find("tr").eq(3).find("td").eq(0).text(year + "-" + month + "-" + day);
		$("#firstDate").text(year + "-" + month + "-" + day);
		
		$("#attachArea").click(function(){
			$("#attach").click();
		});
		
		$("#approvalMenu").find(".item").tab();

	});
	
	function closeModal(){
		$("#approvalModal").modal('hide');
	}
	
	function textchange(){
		console.log($("#attach").val());
		$("#attachArea").val($("#attach").val().substring(12));
	}
</script>










