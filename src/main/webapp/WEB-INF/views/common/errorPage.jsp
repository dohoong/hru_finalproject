<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<script>
		$(function(){
			var msg = "${msg}";
			
			Swal.fire({
				  icon: 'error',
				  title: 'OOPS!',
				  text: '아이디와 비밀번호를 확인해주세요!'
				}).then((result)=>{
					if(result.value){
						location.href = "index.jsp";
					}
				});
		});
	</script>
</body>
</html>