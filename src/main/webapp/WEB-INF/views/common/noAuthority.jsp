<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		$(function(){
			//alert("접근권한이 없습니다.");
			
			Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: '접근권한이 없습니다.'
			}).then((result) => {
				location.href="showMemberView.me";
			});
		});
	</script>
</body>
</html>