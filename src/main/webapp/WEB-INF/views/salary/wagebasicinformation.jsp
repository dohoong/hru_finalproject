<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	.leftMenu{
		height:1100px;
	}
	.searchArea{
		padding-top: 7px;
		height: 70px;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
				<div class="hru segment">
				<h3><i class="check square icon"></i>급여기본정보 관리</h3>
				<br>
				<div class="searchArea">
				 	<table align="center" cellspacing="10px">
						<tr>
							<td><h3>* 부서&nbsp;&nbsp;</h3></td>
							<td>
								<select class="ui dropdown" id="selectCategory" name="searchTeamCode">
									<option value="" selected disabled hidden>카테고리</option>
									<c:forEach items="${tlist}" var="i" varStatus="status" >
										<option value="${i.teamCode}">${i.teamName}</option>
									</c:forEach>
                           	 	</select>			
							</td>
							<td>
								<h3> 성명&nbsp;&nbsp;</h3></td>
								<td><div class="ui icon input"><input type="text" name="searchContent" id="searchName" size="30" placeholder="검색.."><i class="search icon"></i></div>
							</td>
							<td width="100px"></td>
							<td>
								<button class="ui blue button" type="button" onclick="return searchResult();">검색하기</button>
							</td>
						</tr>
					</table>
				</div><!-- searchArea end -->
			</div><!-- 검색영역 끝 -->	
			<table style="width: 100%;">
			<tr>
			<td style="width: 25%; vertical-align:top;">
			<div class="hru segment" style="display: inline-block; width:100%;" ><!-- 검색 결과 사원 리스트 start -->
				<h3><i class="check square icon"></i>기본정보</h3>
				<div class="scrollArea" style="width: 100%; display: inline-block;" >
					<table class="hru scroll table" style="margin-bottom: 400px;" id="searchTable">
						<thead>
							<tr>
								<th>사원번호</th>
								<th>성명</th>
								<th>직급</th>
								<th>부서</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div><!-- 검색 결과 사원 리스트 end -->
			</td>
			<td style="width: 75%; padding: 10px; vertical-align:top; heigth:100%" >
			<div class="hru segment" style="display: inline-block; width: 100%; overflow: hidden; heigth:100%"><!-- 사원 상세정보 start -->
				<h3><i class="check square icon"></i>상세정보</h3>
				<div class="ui top attached tabular menu" style="width: 100%">
 					<a class="active item" data-tab="first">기본정보</a>
 					<a class="item" data-tab="second" onclick="familyInfo();">부양가족</a>
					<a class="item" data-tab="third" onclick="salaryInfo()">급여정보</a>
				</div>	
				<div class="ui bottom attached active tab segment" data-tab="first"><!-- 기본정보 start  -->
				<form action="" method="post" id="MemberBasicInfo">
					<table class="hru table" style="margin-bottom: 75px;">
						<tbody>
							<tr>
								<th>이름 : </th>
								<td>
									<div class="ui mini input">
										<input type="hidden" id="MemberId"name="mid" >
										<input type="text" id="name" name="name" readonly>
									</div>
								</td>
								<th>입사일 : </th>
								<td>
									<div class="ui calendar" id="joincompany" >
										<div class="ui input left icon">
										<i class="calendar icon"></i> <input type="text" id ="enterdate" name="enterDay" placeholder="Date" disabled="disabled">
										</div>
									</div>
								</td>
								<th>퇴사일 : </th>
								<td>
									<div class="ui calendar" id="departurecompany" >
										<div class="ui input left icon">
										<i class="calendar icon"></i> <input type="text" id="outdate" placeholder="Date" disabled="disabled">
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th>핸드폰 번호 : </th>
								<td>
									<div class="ui mini input">
										<input type="text" id="phone" name="phone">
									</div>
								</td>
								<th>이메일 : </th>
								<td colspan="3">
									<div class="ui mini input">
										<input type="text" id="email" name="email" >
									</div>
								</td>
							</tr>
							<tr>
								<th>은행 : </th>
								<td>
									<select class="ui mini dropdown" id="selectbank" name="bank">
										<option value="0" selected disabled hidden>카테고리</option>
										<option value="1">신한은행</option>
										<option value="2">국민은행</option>
										<option value="3">농협</option>
										<option value="4">IBK</option>
										<option value="5">카카오뱅크</option>
										<option value="6">우리은행</option>
	                           	 	</select>			
								</td>
								<th>계좌번호 : </th>
								<td>
									<div class="ui mini input">
										<input type="text" id="accountnumber" name="accountNumber" >
									</div>
								</td>
								<th>예금주 : </th>
								<td>
									<div class="ui mini input">
										<input type="text" id="accountName" name="accountName" readonly="readonly">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<i class="exclamation circle icon" id="exclamation"></i> 월급지급날짜 기준으로 5일 전부터는 계좌를 변경할 수 없습니다.
					<div style="height:80px;"></div>
					<button class="ui blue button mini" id="add" style="float: right;" type="button" onclick="return saveInfo()">저장</button>
					<br>
					<Br>	
					</form>
				</div><!-- 기본정보 end -->
				<div class="ui bottom attached tab segment" data-tab="second" ">
					<table class="hru board2" id="familyTable"><!-- 부양가족  start-->
						<thead>
							<tr>
								<th>성명</th>
								<th>주민등록번호</th>	
								<th>동거여부</th>
								<th>관계</th>
								<th>소득공제 부양여부</th>
								<th>건강보험 피부양자</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div><!-- 부양가족 end -->
				<div class="ui bottom attached tab segment" data-tab="third"><!-- 급여정보 start  -->
					<table class="hru board2" id="salaryInfoTable">
						<thead>
							<tr>
								<th>성명</th>
								<th>급여형태</th>
								<th>은행</th>
								<th>계좌번호</th>
								<th>금액(연봉)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								
							</tr>
						</tbody>
					</table>
				</div><!-- 급여정보 end -->	
			</div><!-- 사원 상세정보 end -->	
			
			</td>	
			</tr>
		</table>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		/* memberId 전역변수 초기화 */
		var memberId = 0;
		
		/* 검색 사원 기본정보 수정 && 저장 */
		function saveInfo(){
			var name = $("#name").val();
			var enterDay = $("#enterdate").val();
			var phone = $("#phone").val();
			var email = $("#email").val();
			var bank = $("#selectbank").val();
			var accountNumber = $("#accountnumber").val();
			var accountName = $("#accountName").val();
			$("#MemberId").val(memberId);
			
			if(memberId == 0 && name == "" && enterDay == ""){
				alert("사원을 선택해주세요");
			}else if(phone=="" && email==""){
				alert("필수정보를 입력해주세요");
			}else if(bank != null && (accountNumber == "" || accountName == "")){
				alert("계좌정보를 모두 입력하세요");
			}else{
				var action = document.forms.MemberBasicInfo;
				$("#MemberBasicInfo").attr("action","UpdateMemberBasicInfo.salary");
				$("#MemberBasicInfo").submit();
			}
			
		}
		
		function searchResult(){
			
			var searchTeamName = $("#selectCategory option:selected").text();
			var searchContent = $("#searchName").val();
			
			var form = {
					searchTeamName:searchTeamName,
					searchContent:searchContent
			};
			/* 사원 검색 ajax */
			$.ajax({
				url:"searchResult.salary",
				type:"post",	
				data: form,
				success:function(data){
					/* console.log(data)
					console.log(typeof(data.mList.length));
					console.log(data.mList[0].mid); */
					
					var length = data.mList.length;
					
					$tableBody=$("#searchTable tbody");
					$tableBody.html('');
					
					for(var i=0; i<length; i++){
						
						var $tr = $("<tr onclick='clickTr(this)'>");
						var $mid = $("<td class='MemberId'>").text(data.mList[i].mid);
						var $mName = $("<td>").text(data.mList[i].mName);
						var $positionName = $("<td>").text(data.mList[i].positionName);
						var $teamName = $("<td>").text(data.mList[i].teamName);
						
						$tr.append($mid);
						$tr.append($mName);
						$tr.append($positionName);
						$tr.append($teamName);
						
						$tableBody.append($tr);
					}
				},
				error:function(status){
					console.log(status);
				}
			});
			/* return false; */
		}
		
		/* 사원정보 ajax */
		function clickTr(select){
			
			/* 사원 mid 가져오기  (검색 목록 테이블의 첫 번째 인자값 가져오기) */
			/* 
				console.log(select.firstChild.innerText);
				console.log($(select).find("td:eq(0)").text());
			 */
			var mid = $(select).find("td:eq(0)").text();
			console.log(mid);
			memberId = mid;
			var result = {
				mid:mid
			}
			/* 사원 기본정보  불러오기 ajax */
			$.ajax({
				url:"selectMemberBasicInfo.salary",
				type:"post",	
				data: result,
				success:function(data){
					var mid = data.bMap.mid;
					var mName = data.bMap.mName;
					var enterdate = data.bMap.enter_date;
					var outdate = data.bMap.out_date;
					var email = data.bMap.email;
					var phone = data.bMap.phone;
					var bankName = data.bMap.bankName;
					var accountnumber = data.bMap.account_number;
					
					
						
					$("#name").val(mName);
					$("#enterdate").val(enterdate.substring(0,10));
					
					if(outdate =='1999-12-31 00:00:00'){
						$("#outdate").attr("disabled" , true);
					}else{
						$("#outdate").val(outdate.substring(0,10));
						$("#outdate").attr("readonly", true);
					}
					$("#phone").val(phone);
					$("#email").val(email);
					$("#accountnumber").val(accountnumber);
					$("#accountName").val(mName);
					
					var maxsize = $("#selectbank option").length;
					for(var i = 1; i<maxsize; i++ ){
						if(bankName == selectbank.options[i].text){
							var num = selectbank.options[i].value;
							console.log(num);
							$("#selectbank").val(num).prop("selected",true);
						}
					}					
					$(".ui.dropdown").dropdown();
				},
				error:function(status){
					console.log(status);
				}
			})
		}
		/* 가족정보 ajax */
		function familyInfo(){
			
			var mid = memberId;
			var result = {
					mid:mid
			}
			$.ajax({
				url:"selectMemberFamilyInfo.salary",
				type:"post",	
				data: result,
				success:function(data){
					var length = data.fList.length;
					
					$tableBody = $("#familyTable tbody");
					$tableBody.html('');
					
					for(var i=0; i<length; i++){
						
						var $tr = $("<tr>");
						var $mName = $("<td style='font-size:0.9em'>").text(data.fList[i].fName);
						var $mSsn = $("<td style='font-size:0.9em'>").text(data.fList[i].fSsn);
						var $together;
						var $category;
						var $deduction;
						var $insurance;
						
						
						if(data.fList[i].deduction == 'N'){
							$deduction = $("<td>").append($("<div class='ui checkbox'>").append("<input type='checkbox'  disabled='disabled'>").append("<label for='include'>"));
						}else if(data.fList[i].deduction == 'Y'){
							$deduction = $("<td>").append($("<div class='ui checkbox'>").append("<input type='checkbox' checked='checked' disabled='disabled'>").append("<label for='include'>"));
						}
						
						if(data.fList[i].insurance =='N'){
							$insurance = $("<td>").append($("<div class='ui checkbox'>").append("<input type='checkbox' disabled='disabled'>").append("<label for='include'>"));
						}else if(data.fList[i].insurance == 'Y'){
							$insurance = $("<td>").append($("<div class='ui checkbox'>").append("<input type='checkbox' checked='checked' disabled='disabled'>").append("<label for='include'>"));
						}
						
						if(data.fList[i].together == 'N'){
							$together = $("<td>").append($("<div class='ui checkbox'>").append("<input type='checkbox' disabled='disabled'>").append("<label for='include'>"));
						}else if(data.fList[i].together == 'Y'){
							$together = $("<td>").append($("<div class='ui checkbox'>").append("<input type='checkbox' checked='checked' disabled='disabled'>").append("<label for='include'>"));
						}
						if(data.fList[i].category == 1){
							$category = $("<td style='font-size:0.9em'>").text("배우자");
						}else if(data.fList[i].category == 2){
							$category = $("<td style='font-size:0.9em'>").text("자녀");
						}else if(data.fList[i].category == 3){
							$category = $("<td style='font-size:0.9em'>").text("형제자매");
						}else if(data.fList[i].category == 4){
							$category = $("<td style='font-size:0.9em'>").text("직계족손");
						}else if(data.fList[i].category == 5){
							$category = $("<td style='font-size:0.9em'>").text("직계비속");
						}
						$tr.append($mName);
						$tr.append($mSsn);
						$tr.append($together);
						$tr.append($category);
						$tr.append($deduction);
						$tr.append($insurance);
						
						$tableBody.append($tr);
					}
				},
				error:function(data){
					console.log("에러입니다.")
				}
			})
		}
		/* 사원급여정보 ajax */
		function salaryInfo(){
			var mid = memberId;
			var result = {
					mid:mid
			}
			$.ajax({
				url:"selectMemberSalaryInfo.salary",
				type:"post",	
				data: result,
				success:function(data){
					var type = data.sMap.type;
					
					$tableBody = $("#salaryInfoTable tbody");
					$tableBody.html('');
					
					var $tr = $("<tr>");
					var $mName = $("<td style='font-size:0.9em'>").text(data.sMap.mName);
					var $type;
					if(type == 1){
						$type = $("<td style='font-size:0.9em'>").text('연봉');
					}else if (type==2){
						$type = $("<td style='font-size:0.9em'>").text('월급');
					}else{
						$type = $("<td style='font-size:0.9em'>").text('시급');
					}
					var $bankName=$("<td style='font-size:0.9em'>").text(data.sMap.bankName);
					var $accountNumber = $("<td style='font-size:0.9em'>").text(data.sMap.accountNumber);
					var $amount = $("<td style='font-size:0.9em'>").text(data.sMap.amount)
					
					$tr.append($mName);
					$tr.append($type);
					$tr.append($bankName);
					$tr.append($accountNumber);
					$tr.append($amount);
					
					$tableBody.append($tr);
				}
			})
		}
		
		$(function(){
			$(".ui.dropdown").dropdown();
			$(".menu .item").tab();
			$(".ui.dropdown").dropdown();
			$("#pay2").show();
			$("#joincompany").calendar({
				type:'date',
				endCalendar : $('#rangeend'),
				formatter: {
				      date: function (date, settings) {
				        if (!date) return '';
				        var day = date.getDate();
				        var month = date.getMonth() + 1;
				        var year = date.getFullYear();
				        return year + '-' + month + '-' + day;
				      }
				}
			});
			$("#departurecompany").calendar({
				type:'date',
				endCalendar : $('#rangeend'),
				formatter: {
				      date: function (date, settings) {
				        if (!date) return '';
				        var day = date.getDate();
				        var month = date.getMonth() + 1;
				        var year = date.getFullYear();
				        return year + '-' + month + '-' + day;
				      }
				}
			});
		});
	</script>
</body>
</html>