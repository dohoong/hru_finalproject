<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	.searchArea{
		padding-top: 7px;
		height: 70px;
	}
	.hru.scroll.table{
		font-size:0.9em;
	}
	.hru.board2 {
		font-size:0.9em;
	}
	div.hru.segment{
		margin-bottom: 0px; 
	}
	.hru.board2 td {
		height:38px;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
			<div class="hru segment"><!-- hru segment start -->
				<h3><i class="check square icon"></i>월별 급여 지급 현황</h3>
				<br>
					<div class="searchArea"><!-- searchArea start -->
							<table align="center" cellspacing="10px">
								<tr>
									<td><h3>* 근무 년도 </h3></td>
									<td>
										<div class="ui calendar" id="year_first_calendar">
										  <div class="ui input left icon">
										    <i class="calendar icon"></i>
										    <input type="text"  readonly="readonly" placeholder="근무 년도 선택" id="searchDate">
										  </div>
										</div>
										
									</td>
									<td width="100px"></td>
									<td><h3>* 부서 &nbsp;&nbsp;</h3></td>
									<td>
										<select class="ui dropdown" id="searchTeamCode" name="searchTeamCode">
											<option value="" selected disabled hidden>카테고리</option>
											<c:forEach items="${tlist}" var="i" varStatus="status" >
												<option value="${i.teamCode}">${i.teamName}</option>
											</c:forEach>
		                           	 	</select>			
									</td>
									<td width="250px"></td>
									<td>
										<button class="ui blue button" onclick="return searchResult();">검색하기</button>
									</td>
								</tr>
							</table>
					</div><!-- searchArea end -->
			</div><!-- hru segment end -->
			<table style="width: 100%">
				<tr>
					<td style="width: 23%; vertical-align: top;" ><!-- 검색결과 사원 리스트 start  -->
						<div class="hru segment" style="display: inline-block; width: 100%">
							<h3><i class="check square icon"></i>사원목록</h3>
							<div class="scrollArea" style="width:100%; display: inline-block;">
								<table class="hru scroll table" style="margin-bottom:200px;" id="searchTable">
									<thead>
										<tr>
											<th>사원번호</th>
											<th>성명</th>
											<th>직급</th>
											<th>부서</th>
										</tr>
									</thead>
									<tbody>
									
									</tbody>
								</table>
							</div>
						</div>
					</td><!-- 검색결과 사원 리스트 end  -->
					<td style="width: 77%; padding: 10px; vertical-align: top; height: 100%"><!-- 사원 월별 지급 내역 조회  start-->
						<div class="hru segment">
							<h3><i class="check square icon"></i>지급 내역</h3>
							<table class="hru board2" style="overflow-x:scroll; padding: 10px;" id="yearSalaryList">
								<thead>
									<tr>
										<th><c:out value="순번"></c:out></th>
										<th><c:out value="항목명"></c:out></th>
										<th><c:out value="계"></c:out></th>
										<th><c:out value="1월"></c:out></th>
										<th><c:out value="2월"></c:out></th>
										<th><c:out value="3월"></c:out></th>
										<th><c:out value="4월"></c:out></th>
										<th><c:out value="5월"></c:out></th>
										<th><c:out value="6월"></c:out></th>
										<th><c:out value="7월"></c:out></th>
										<th><c:out value="8월"></c:out></th>
										<th><c:out value="9월"></c:out></th>
										<th><c:out value="10월"></c:out></th>
										<th><c:out value="11월"></c:out></th>
										<th><c:out value="12월"></c:out></th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
						</div>
					</td><!-- 사원 월별 지급 내역 조회 end -->
				</tr>
			</table>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		//검색 결과를 가져와야함
		//조건 1.팀 코드에 알맞는 인원 -> 퇴사한 인원도 가져와도 상관없다. -> 급여지급현황을 보는 것이기 때문
		//-> 검색 년도는 사원을 선택했을때의 조건 
		function searchResult(){
			//검색 년도
			var searchDay = $("#searchDate").val();
			//현재 년도 
			var date = new Date();
			var year = date.getFullYear();	
			var teamName = $("#searchTeamCode option:selected").text();
			
			if(searchDay > year){
				alert("해당 목록을 조회할 수 없습니다.");
			}else{
				var form = {
						teamName:teamName
				};
				$.ajax({
					url:"selectMonthlyWageMember.salary",
					type:"post",
					data: form,
					success:function(data){
						//console.log(data);
						var length = data.mList.length;
						
						$tableBody=$("#searchTable tbody");
						$tableBody.html('');
						for(var i=0; i<length; i++){
							
							var $tr = $("<tr onclick='clickTr(this)'>");
							var $mid = $("<td class='MemberId'>").text(data.mList[i].mid);
							var $mName = $("<td>").text(data.mList[i].mName);
							var $positionName = $("<td>").text(data.mList[i].positionName);
							var $teamName = $("<td>").text(data.mList[i].teamName);
							
							$tr.append($mid);
							$tr.append($mName);
							$tr.append($positionName);
							$tr.append($teamName);
							
							$tableBody.append($tr);
						}
					},
					error:function(status){
						
					}
				})
			}
		}
		/* 사원을 클릭했을 때 사원의 지급내역 조회 */
		function clickTr(select){
			var mid = $(select).find("td:eq(0)").text();
			var searchDay = $("#searchDate").val();
			
			var form = {
					mid:mid,
					searchDay:searchDay
			}
			$.ajax({
				url:"selectMemberYearSalary.salary",
				type:"post",
				data: form,
				success:function(data){
					
					$tableBody = $("#yearSalaryList tbody");
					$tableBody.html('');
					var arrayList = new Array(data.myList.myList0 ,data.myList.myList1 ,data.myList.myList2
							,data.myList.myList3,data.myList.myList4,data.myList.myList5,data.myList.myList6,
							data.myList.myList7,data.myList.myList8,data.myList.myList9,data.myList.myList10,
							data.myList.myList11,data.myList.myList12,data.myList.myList13)
					/* 지급 합계 */
					var giveTotal= 0;
					/* 공제 합계 */
					var discountTotal = 0;
					//공제
					var discount1 = 0;
					var discount2 = 0;
					var discount3 = 0;
					var discount4 = 0;
					var discount5 = 0;
					var discount6 = 0;
					var discount7 = 0;
					var discount8 = 0;
					var discount9 = 0;
					var discount10 = 0;
					var discount11 = 0;
					var discount12 = 0;
					//지급액
					var give1 = 0;
					var give2 = 0;
					var give3 = 0;
					var give4 = 0;
					var give5 = 0;
					var give6 = 0;
					var give7 = 0;
					var give8 = 0;
					var give9 = 0;
					var give10 = 0;
					var give11 = 0;
					var give12 =0;
					//실지급액
					var result1 = 0;
					var result2 = 0;
					var result3 = 0;
					var result4 = 0;
					var result5 = 0;
					var result6 = 0;
					var result7 = 0;
					var result8 = 0;
					var result9 = 0;
					var result10 = 0;
					var result11 = 0;
					var result12 = 0;
					//총 14번을 반복 
					for(var number = 0; number < 14; number++){
						
						var $trOne = $("<tr>");
						var $numberOne = $("<td>").text(number+1);
						var $NameOne =0;
						//
						for(var k=0; k< arrayList[number].length; k++){
							var date = String(arrayList[number][k].stdDate).substring(4,6);
							console.log(date)						
							switch(date){
							case "01": 
								if(number < 8 && date =="01"){
									give1 += arrayList[number][k].sdetailMoney;
									result1 += arrayList[number][k].sdetailMoney;
								}else{
									discount1 +=arrayList[number][k].sdetailMoney;
									result1 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "02" : 
								if(number < 8 && date =="02"){
									give2 += arrayList[number][k].sdetailMoney;
									result2 += arrayList[number][k].sdetailMoney;
								}else{
									discount2 +=arrayList[number][k].sdetailMoney;
									result2 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "03" :
								if(number < 8 && date =="03"){
									give3 += arrayList[number][k].sdetailMoney;
									result3 += arrayList[number][k].sdetailMoney;
								}else{
									discount3 +=arrayList[number][k].sdetailMoney;
									result3 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "04" :
								if(number < 8 && date =="04"){
									give4+= arrayList[number][k].sdetailMoney;
									result4 += arrayList[number][k].sdetailMoney;
								}else{
									discount4 +=arrayList[number][k].sdetailMoney;
									result4 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "05" : 
								if(number < 8 && date =="05"){
									give5 += arrayList[number][k].sdetailMoney;
									result5 += arrayList[number][k].sdetailMoney;
								}else{
									discount5 +=arrayList[number][k].sdetailMoney;
									result5 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "06" :
								if(number < 8 && date=="06"){
									give6 += arrayList[number][k].sdetailMoney;
									result6 += arrayList[number][k].sdetailMoney;
								}else{
									discount6 +=arrayList[number][k].sdetailMoney;
									result6 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "07" :
								if(number < 8 && date =="07"){
									give7 += arrayList[number][k].sdetailMoney;
									result7 += arrayList[number][k].sdetailMoney;
								}else{
									discount7 +=arrayList[number][k].sdetailMoney;
									result7 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "08" :
								if(number < 8 && date=="08"){
									give8 += arrayList[number][k].sdetailMoney;
									result8 += arrayList[number][k].sdetailMoney;
								}else{
									discount8 +=arrayList[number][k].sdetailMoney;
									result8 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "09" :
								if(number < 8 && date =="09"){
									give9 += arrayList[number][k].sdetailMoney;
									result9 += arrayList[number][k].sdetailMoney;
								}else{
									discount9 +=arrayList[number][k].sdetailMoney;
									result9 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "10":
								if(number < 8 && date=="10"){
									give10 += arrayList[number][k].sdetailMoney;
									//console.log(arrayList[number][k].sdetailMoney);
									result10 += arrayList[number][k].sdetailMoney;
								}else{
									discount10 +=arrayList[number][k].sdetailMoney;
									result10 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "11":
								if(number < 8 && date == "11"){
									give11 += arrayList[number][k].sdetailMoney;
									result11 += arrayList[number][k].sdetailMoney;
								}else{
									discount11 +=arrayList[number][k].sdetailMoney;
									//console.log(arrayList[number][k].sdetailMoney);
									//console.log(discount11);
									result11 -= arrayList[number][k].sdetailMoney;
								}
								break;
							case "12": 
								if(number < 8 && date == "12"){
									give12 += arrayList[number][k].sdetailMoney;
									result12 += arrayList[number][k].sdetailMoney;
								}else if(number >= 8 && date == "12"){
									discount12 +=arrayList[number][k].sdetailMoney;
									//console.log(arrayList[number][k].sdetailMoney);
									result12 -= arrayList[number][k].sdetailMoney;
								}
								break;
							default :
								continue;
							}
						}
						//console.log(result11);
						
						/* console.log("give11 : " + give11)
						console.log("give12 : "+ give12) 
						console.log("discount11 : " + discount11)
						console.log("discount12 : " + discount12) */
						switch(number){
							case 0: $NameOne = $("<td>").text("기본급"); break;
							case 1: $NameOne = $("<td>").text("상여"); break;
							case 2: $NameOne = $("<td>").text("시점상여"); break;
							case 3: $NameOne = $("<td>").text("차량보조"); break;
							case 4: $NameOne = $("<td>").text("식대"); break;
							case 5: $NameOne = $("<td>").text("잔업수당"); break;
							case 6: $NameOne = $("<td>").text("야간수당"); break;
							case 7: $NameOne = $("<td>").text("휴일수당"); break;
							case 8: $NameOne = $("<td>").text("국민연금"); break;
							case 9: $NameOne = $("<td>").text("건강보험"); break;
							case 10: $NameOne = $("<td>").text("장기요양보험"); break;
							case 11: $NameOne = $("<td>").text("고용보험"); break;
							case 12: $NameOne = $("<td>").text("소득세"); break;
							case 13: $NameOne = $("<td>").text("지방소득세"); break;
						}
						
						var total = 0;
						
						for(var i=0; i<arrayList[number].length; i++){
							
							total+= arrayList[number][i].sdetailMoney;
						}
						var $TotalOne = $("<td>").text(numberFormat(total));
						
						if(number <8){
							giveTotal += total;
						}else{
							discountTotal += total;
						}
						
						$trOne.append($numberOne);
						$trOne.append($NameOne);
						$trOne.append($TotalOne);
						
						var searchYear = 0;
						if(searchDay == 2019){
							searchYear = 201900
						}else if(searchDay == 2020){
							searchYear = 202000
						}else if(searchDay == 2021){
							searchYear = 202100
						}
						/* 1월 ~ 12월 반복 */
						for(var j=0; j<12; j++){
							
							var tdfirst = $("<td>").text(0);
							
							for(var z=0; z<arrayList[number].length; z++){
								
								if(j+1 == arrayList[number][z].stdDate-searchYear){
									
									tdfirst.text(numberFormat(arrayList[number][z].sdetailMoney));				
								}else{
									
								}
							}
							$trOne.append(tdfirst);
						}
						$tableBody.append($trOne);
					}
					
					
					
					/* 지급합계 */
					var $trTwo =  $("<tr style='background :#f0f0f0'>");
					var $numberTwo = $("<td colspan='2'>").text("지급계");
					var $giveTotal = $("<td>").text(numberFormat(giveTotal));
					var $give1 = $("<td>").text(numberFormat(give1));
					var $give2 = $("<td>").text(numberFormat(give2));
					var $give3 = $("<td>").text(numberFormat(give3));
					var $give4 = $("<td>").text(numberFormat(give4));
					var $give5 = $("<td>").text(numberFormat(give5));
					var $give6 = $("<td>").text(numberFormat(give6));
					var $give7 = $("<td>").text(numberFormat(give7));
					var $give8 = $("<td>").text(numberFormat(give8));
					var $give9 = $("<td>").text(numberFormat(give9));
					var $give10 = $("<td>").text(numberFormat(give10));
					var $give11 = $("<td>").text(numberFormat(give11));
					var $give12 = $("<td>").text(numberFormat(give12));
					
					$trTwo.append($numberTwo);
					$trTwo.append($giveTotal);
					$trTwo.append($give1);
					$trTwo.append($give2);
					$trTwo.append($give3);
					$trTwo.append($give4);
					$trTwo.append($give5);
					$trTwo.append($give6);
					$trTwo.append($give7);
					$trTwo.append($give8);
					$trTwo.append($give9);
					$trTwo.append($give10);
					$trTwo.append($give11);
					$trTwo.append($give12);
					
					$tableBody.append($trTwo);

					/* 공제합계 */
					var $trThree = $("<tr style='background :#f0f0f0'>");
					var $numberThree = $("<td colspan='2'>").text("공제계");
					var $discountTotal = $("<td>").text(numberFormat(discountTotal));
					var $discount1 = $("<td>").text(numberFormat(discount1));
					var $discount2 = $("<td>").text(numberFormat(discount2));
					var $discount3 = $("<td>").text(numberFormat(discount3));
					var $discount4 = $("<td>").text(numberFormat(discount4));
					var $discount5 = $("<td>").text(numberFormat(discount5));
					var $discount6 = $("<td>").text(numberFormat(discount6));
					var $discount7 = $("<td>").text(numberFormat(discount7));
					var $discount8 = $("<td>").text(numberFormat(discount8));
					var $discount9 = $("<td>").text(numberFormat(discount9));
					var $discount10 = $("<td>").text(numberFormat(discount10));
					var $discount11 = $("<td>").text(numberFormat(discount11));
					var $discount12 = $("<td>").text(numberFormat(discount12));
					
					
					$trThree.append($numberThree);
					$trThree.append($discountTotal);
					$trThree.append($discount1);
					$trThree.append($discount2);
					$trThree.append($discount3);
					$trThree.append($discount4);
					$trThree.append($discount5);
					$trThree.append($discount6);
					$trThree.append($discount7);
					$trThree.append($discount8);
					$trThree.append($discount9);
					$trThree.append($discount10);
					$trThree.append($discount11);
					$trThree.append($discount12);
					
					$tableBody.append($trThree);
					
					
					/* 실지급액 */
					var result = giveTotal - discountTotal;
					
					var $trResult = $("<tr style='background :#f0f0f0'>");
					var $numberResult = $("<td colspan='2'>").text("실 지급액 계");
					var $salaryResult = $("<td>").text(numberFormat(result));
					var $result1 = $("<td>").text(numberFormat(result1));
					var $result2 = $("<td>").text(numberFormat(result2));
					var $result3 = $("<td>").text(numberFormat(result3));
					var $result4 = $("<td>").text(numberFormat(result4));
					var $result5 = $("<td>").text(numberFormat(result5));
					var $result6 = $("<td>").text(numberFormat(result6));
					var $result7 = $("<td>").text(numberFormat(result7));
					var $result8 = $("<td>").text(numberFormat(result8));
					var $result9 = $("<td>").text(numberFormat(result9));
					var $result10 = $("<td>").text(numberFormat(result10));
					var $result11 = $("<td>").text(numberFormat(result11));
					var $result12 = $("<td>").text(numberFormat(result12));
					
					$trResult.append($numberResult);
					$trResult.append($salaryResult);
					$trResult.append($result1);
					$trResult.append($result2);
					$trResult.append($result3);
					$trResult.append($result4);
					$trResult.append($result5);
					$trResult.append($result6);
					$trResult.append($result7);
					$trResult.append($result8);
					$trResult.append($result9);
					$trResult.append($result10);
					$trResult.append($result11);
					$trResult.append($result12);
					
					
					$tableBody.append($trResult);
					
				},
				error:function(status){
					
				}
			})		
		}
		function numberFormat(inputNumber) {
			   return inputNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}
		$(function(){
			$("#pay2").show();
			
			$('#year_first_calendar').calendar({
			    startMode: 'year'
			    ,type: 'year'
			  });
			
			$(".ui.dropdown").dropdown();
		});
	</script>
</body>
</html>