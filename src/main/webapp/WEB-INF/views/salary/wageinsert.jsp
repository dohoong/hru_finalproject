<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	.leftMenu{
		height:1500px;
	}
	.searchArea{
		padding-top: 7px;
		height: 70px;
	}
	.atInput{
		width:60px;
		margin-right: 10px;
	}
</style>
</head>	
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
		<div class="hru segment"><!-- hru segment start -->
		<h3><i class="check square icon"></i>급여입력</h3>
		<br>
			<div class="searchArea"><!-- searchArea start -->
					<table align="center" cellspacing="10px">
						<tr>
							<td><h3>* 근무년월 </h3></td>
							<td>
								<div class="ui calendar" id="month_year_calendar">
									<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" readonly="readonly" placeholder="근무년월 선택" id="searchWorking">
									</div>
								</div>
							</td>
							<td width="100px"></td>
							<td><h3>* 부서 &nbsp;&nbsp;</h3></td>
							<td>
								<select class="ui dropdown" id="selectCategory" name="searchTeamCode">
									<option value="" selected disabled hidden>카테고리</option>
									<c:forEach items="${tlist}" var="i" varStatus="status" >
										<option value="${i.teamCode}">${i.teamName}</option>
									</c:forEach>
                           	 	</select>			
							</td>
							<td width="250px"></td>
							<td>
								<button class="ui blue button" onclick="return searchResult();">검색하기</button>
							</td>
						</tr>
					</table>
			</div><!-- searchArea end  -->
		</div><!-- hru segment end -->
		<table style="width:100%">
		<tr>
			<td style="width:25%; vertical-align: top">
				<div class="hru segment" style="display: inline-block; width: 100%"><!-- 검색 결과 사원리스트 start -->
					<h3><i class="check square icon"></i>사원목록</h3>
					<div class="scrollArea" style="width:100%; display: inline-block;">
						<table class="hru scroll table" style="margin-bottom:200px;" id="searchTable">
							<thead>
								<tr>
									<th>사원번호</th>
									<th>성명</th>
									<th>직급</th>
									<th>부서</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div><!-- 검새결과 사원 리스트 end -->
			</td>
			<td style="width: 75%; padding 10px; vertical-align:top; height: 100%">
				<div class="hru segment" style="display: inline-block; width: 100%; overflow: hidden; heigth:100%"><!-- 사원 상세정보 start -->
					<h3><i class="check square icon"></i>지급 / 공제내역</h3>
					<div class="ui top attached tabular menu" style="width: 100%">
	 					<a class="active item" data-tab="first">사원별</a>
	 					<a class="item" data-tab="second" onclick="TeamTotalMonthSalary()" >전체</a>
					</div>
					<div class="ui bottom attached active tab segment" data-tab="first"><!-- 기본정보 start  -->	
							<h4><i class="check square icon"></i>월 근태일수</h4>
							<div class="searchArea" style="height:70px;" id="monthAt">
								<table align="center" cellspacing="10px">
									<tr>
										<td><h3>총 근무일수 </h3></td>
										<td><div class="ui input">
			  								<input class="atInput" id="allworkingDay" type="text" value="" readonly="readonly" style="text-align: center;"></div>
										</td>
										<td><h3>평일 근무일수 </h3></td>
										<td><div class="ui input">
			  								<input  class="atInput" id="weekend" type="text" value="" readonly="readonly" style="text-align: center;"></div>
										</td>
										<td><h3>휴일 근무일수 </h3></td>
										<td><div class="ui input">
			  								<input  class="atInput" id="holiday" type="text" value="" readonly="readonly" style="text-align: center;"></div>
										</td>
										<td><h3>휴가 사용일수 </h3></td>
										<td><div class="ui input">
			  								<input class="atInput" id="vacation" type="text" value="" readonly="readonly" style="text-align: center;"></div>
										</td>
										<td><h3>결근 일수 </h3></td>
										<td><div class="ui input">
			  								<input class="atInput" id="absence" type="text" value="" readonly="readonly" style="text-align: center;"></div>
										</td>	
									</tr>
							</table>
						</div>
						<br><br>
						<div style="text-align: center;">
							<div style="display: inline-block; width: 45%; vertical-align: top" >
								<table class="hru board2" id="MemberSalaryTable" >
									<thead>
										<tr>
											<th>지급항목</th>
											<th>금액</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><c:out value="기본급"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="basicSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="상여"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="plusSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="시점상여"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="SeemSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="식대"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="mealSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
													<input type="hidden" id="mealId">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="차량보조"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="carSalary"	 style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
													<input type="hidden" id="carId">
												</div>
											</td>
										</tr>
										
										<tr>
											<td><c:out value="잔업수당"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="extensionSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="야근수당"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="nightSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="휴일수당"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="holidaySalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										
										<tr style="background: #f0f0f0">
											<td><c:out value="지급항목계"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="memberSalaryTotal" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div style="display: inline-block; width: 45%">
								<table class="hru board2" id="MemberTaxTable" >
									<thead>
										<tr>
											<th>공제항목</th>
											<th>금액</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><c:out value="국민연금	"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="NationalPersion" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="건강보험"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="healthInsurance" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="장기요양보험"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="CareInsurance" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="고용보험"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="EmployeeInsurance" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="소득세"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="incomeTax" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="지방소득세"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="localIncomeTax" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr><td></td><td></td></tr>
										<tr><td></td><td></td></tr>
										<tr style="background: #f0f0f0">
											<td><c:out value="공제항목계"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="MemberDeduction" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="	">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<br>
								<div class="searchArea" style="height:70px; float: right; vertical-align: middle;" id="monthAt" >
									<table align="right" cellspacing="10px">
										<tr>
											<td>
												<h3>실 지급금액 계  &nbsp;:&nbsp;	 </h3>
											</td>
											<td>
												<div class="ui input" style="float: right;">
													<input type="text" id="Membertotal" value="" readonly="readonly" style="text-align: center;">
												</div>
											</td>
										</tr>
									</table>
								</div>
							</div>
							
							
						</div>
						<br>
						<div>    
							<button id="MemberModify" style="margin-left: 88%" type="button" class="ui blue button" onclick="return SalaryModifiy()">수정</button> 
							<div style="margin-left:80%">
								<button id="MemberSave"  class="ui blue button" style="display: none" onclick="return save()">저장</button>
								<button id="MemberCancel" type="button" class="ui button" style="display:none" onclick="return cancel()">취소</button> 
							</div>
						</div>
						
					</div>
					<div class="ui bottom attached tab segment" data-tab="second"><!-- 기본정보 start  -->	
					<form action="MonthSalaryApproval.salary" method="post" id="TeamMonthSalary" enctype="multipart/form-data">
							<h4><i class="check square icon"></i>전체 지급/공제 내역 확인</h4>
						<br>
						<div style="text-align: center;">
							<div style="display: inline-block; width: 45%; vertical-align: top" >
								<table class="hru board2" >
									<thead>
										<tr>
											<th>지급항목</th>
											<th>금액</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><c:out value="기본급"></c:out></td>
											<td>
												<div class="ui mini input">
														<input  id="TotalbasicSalary" name="TotalbasicSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="상여"></c:out></td>
											<td>
												<div class="ui mini input">
														<input  id="TotalplusSalary" name="TotalplusSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="시점상여"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalSeemSalary" name="TotalSeemSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="식대"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalMealSalary" name="TotalMealSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="차량보조"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalCarSalary" name="TotalCarSalary"style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="잔업수당"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalextensionSalary" name="TotalextensionSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="야근수당"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalnightSalary" name="TotalnightSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="휴일수당"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalholidaySalary" name="TotalholidaySalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr style="background: #f0f0f0">
											<td><c:out value="지급항목계"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalTeamSalary" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							
							<div style="display: inline-block; width: 45%">
								<table class="hru board2" >
									<thead>
										<tr>
											<th>공제항목</th>
											<th>금액</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><c:out value="국민연금	"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalNationalPersion" name="TotalNationalPersion" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="건강보험"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalhealthInsurance" name="TotalhealthInsurance" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="장기요양보험"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalCareInsurance" name="TotalCareInsurance" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="고용보험"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalEmployeeInsurance" name="TotalEmployeeInsurance" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>	
											</td>
										</tr>
										<tr>
											<td><c:out value="소득세"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalincomeTax"  name="TotalincomeTax" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr>
											<td><c:out value="지방소득세"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotallocalIncomeTax" name="TotallocalIncomeTax" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="">
												</div>
											</td>
										</tr>
										<tr><td></td><td></td></tr>
										<tr><td></td><td></td></tr>
										<tr style="background: #f0f0f0">
											<td><c:out value="공제항목계"></c:out></td>
											<td>
												<div class="ui mini input">
													<input  id="TotalTeamDeduction" style="text-align: center; font-weight:; font-size: 1.3em;" type="text" readonly="readonly" value="	">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<br>
								<div class="searchArea" style="height:70px; float: right; vertical-align: middle;" id="monthAt" >
									<table align="right" cellspacing="10px">
										<tr>
											<td>
												<h3>실 지급금액 계  &nbsp;:&nbsp;	 </h3>
											</td>
											<td>
												<div class="ui input" style="float: right;">
													<input type="text" id="departmentTotal"  name="departmentTotal" value="" readonly="readonly" style="text-align: center;">
												</div>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<br>
						<div style="margin-left: 88%">
							<input type="hidden" name="MonthSalarySearchDate" id="MonthSalarySearchDate" >
							<input type="hidden" name="MonthSalaryTeamCode" id="MonthSalaryTeamCode">
							<input type="hidden" name="title" value="제목" id="title2">
							<input type="hidden" name="firstApproval" id="firstApp2">
							<input type="hidden" name="secondApproval" id="secondApp2">
							<input type="hidden" name="thirdApproval" id="thirdApp2">
							<input type="file"id="attach" name="appFile" onchange="textchange();">
							<button class="ui blue button" onclick="return approvalFunction();">전자결재</button>
						</div>
						</form>
					</div>
				</div>
			</td>
			</tr>
		</table>
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<jsp:include page="../common/approval.jsp"/><!-- 결재모달쓰려면 이거 포함해야 됨! -->
	<script>
	
		/* memberId 전역변수 초기화 */
		var memberId = 0;
		
		
		/* 전자결제 ajax */	
		function approvalFunction(){
			var approvalCode = 6; //신규사원등록에 대한 결재코드
			//console.log("전자결재");
			$.ajax({
				url : "selectOneBasic.app",
				type : "post",
				data : {approvalCode : approvalCode},
				success : function(data){
					//console.log(data);
					$("#appType").val("월급여마감");
					var usermid = "${loginUser.mid}"; //기안자사번
					var yesApp = 0;
					for(var i=0; i<data.appBasic.mlist.length; i++){
						if(data.appBasic.mlist[i].mid == usermid){
							yesApp = 1;
						}
					}
					
					if(data.appBasic.status == 'Y' && yesApp != 1 && usermid != 1){
						
						if(data.appBasic.firstApproval!=0){ //1차 결재자가 있을 때,
							var firstName = data.appBasic.mlist[0].mName;
							var firstMid = data.appBasic.mlist[0].mid;
							var $input = $("<input type='hidden' name='first'>").val(firstMid);
							$("#firstApp").text(firstName).append($input);

							if(data.appBasic.secondApproval!=0){ //2차 결재자가 있을 때,
								var secondName = data.appBasic.mlist[1].mName;
								var secondMid = data.appBasic.mlist[1].mid;
								var $input = $("<input type='hidden' name='second'>").val(secondMid);
								$("#secondApp").text(secondName).append($input);
								
								if(data.appBasic.thirdApproval!=0){ //3차 결재자가 있을 때,
									var thirdName = data.appBasic.mlist[2].mName;
									var thirdMid = data.appBasic.mlist[2].mid;
									var $input = $("<input type='hidden' name='third'>").val(thirdMid);
									$("#thirdApp").text(thirdName).append($input);
								}
							}
						}
					}
					$("#approvalModal").modal('show');
					
				},
				error : function(status){
					console.log(status);
				}
			});
			return false;
		}
		
		/* 결제 요청 버튼 눌렀을 때!! */
		$('#request').click(function(){
			var searchDay = $("#searchWorking").val();
			var searchDaySplit = searchDay.split('-');	
			var resultDay = searchDaySplit[0].concat(searchDaySplit[1]); 
			var teamCode = $("#selectCategory option:selected").val();
			
			//console.log("눌렸음!");
			
			$("#title2").val($("#title").val());
			$("#firstApp2").val($("#firstApp").find("input[type=hidden]").val());
			$("#secondApp2").val($("#secondApp").find("input[type=hidden]").val());
			$("#thirdApp2").val($("#thirdApp").find("input[type=hidden]").val());
			$("#MonthSalaryTeamCode").val(teamCode);
			$("#MonthSalarySearchDate").val(resultDay);
			
			$('#TeamMonthSalary').submit();
			
			var content = "결재요청 : " + $("#title").val();
			var tmid = $("#firstApp").find("input[type=hidden]").val();
			if(tmid!="" && tmid!=null && tmid!=0){
				send(tmid, content);
			}
			
		});
		/* 급여입력 - 전체 클릭 ajax */
		function TeamTotalMonthSalary(){
			var searchDay = $("#searchWorking").val();
			var searchDaySplit = searchDay.split('-');	
			var resultDay = searchDaySplit[0].concat(searchDaySplit[1]); 
			var teamCode = $("#selectCategory option:selected").val();
			var result = {
					teamCode:teamCode,
					resultDay:resultDay
			} 
			$.ajax({
				url:"searchTeamSalary.salary",
				type:"post",
				data: result,
				success:function(data){
					
					$("#TotalbasicSalary").val(numberFormat(data.stList[0].sdetailMoney));
					$("#TotalplusSalary").val(numberFormat(data.stList[1].sdetailMoney));
					$("#TotalSeemSalary").val(numberFormat(data.stList[2].sdetailMoney));
					$("#TotalMealSalary").val(numberFormat(data.stList[3].sdetailMoney));
					$("#TotalCarSalary").val(numberFormat(data.stList[4].sdetailMoney));
					$("#TotalextensionSalary").val(numberFormat(data.stList[5].sdetailMoney));
					$("#TotalnightSalary").val(numberFormat(data.stList[6].sdetailMoney));
					$("#TotalholidaySalary").val(numberFormat(data.stList[7].sdetailMoney));
					
					var totalTeamSalary =0;
					for(var i = 0; i<8; i++){
						totalTeamSalary += data.stList[i].sdetailMoney;
					}
					$("#TotalTeamSalary").val(numberFormat(totalTeamSalary));
					
					
					$("#TotalNationalPersion").val(numberFormat(data.stList[8].sdetailMoney));
					$("#TotalhealthInsurance").val(numberFormat(data.stList[9].sdetailMoney));
					$("#TotalCareInsurance").val(numberFormat(data.stList[10].sdetailMoney));
					$("#TotalEmployeeInsurance").val(numberFormat(data.stList[11].sdetailMoney));
					$("#TotalincomeTax").val(numberFormat(data.stList[12].sdetailMoney));
					$("#TotallocalIncomeTax").val(numberFormat(data.stList[13].sdetailMoney));
					
					var totalTeamDeduction = 0;
					for(var j=8; j<14; j++){
						totalTeamDeduction += data.stList[i].sdetailMoney;
					}
					$("#TotalTeamDeduction").val(numberFormat(totalTeamDeduction));
					
					var departmentTotal = totalTeamSalary - totalTeamDeduction;
					
					$("#departmentTotal").val(numberFormat(departmentTotal));
				},
				error:function(status){
					console.log(status);
				}
			});
			
			
		}
		/* searchResult에서 해줘야할 기능
		1. 월급여테이블 , 지급상세테이블 데이터 인서트
		   - 월급여테이블을 Insert하기 위해서는 해당 부서의 팀원들의 memberId가 필요
		   - 해당 사원들의 월근태현황을 모두 가져온다.
		   - 급여기준 테이블의 급여기준을 가져온다.
		   - 월근태현황과 급여기준을 통해 월급여테이블에 데이터를 인서트한다.
		   - 인서트를 완료하면 해당 월급여테이블의 지급상세테이블에 데이터들을 인서트한다.
		   - 이와 같은 로직을 부서테이블을 반복한다.
		2. 사원 목록 조회
		   - 인서트가 완료되면 해당 검색 조건에 맞는 사원들을 조회한다.
		*/
		function searchResult(){
			var searchDay = $("#searchWorking").val();
			var searchDaySplit = searchDay.split('-');	
			var searchTeamName = $("#selectCategory option:selected").text();
			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth()+1;
			var date = date.getDate();
			var yearMonth = year+"-"+month;
			//검색한날짜 (number)
			var resultDay = searchDaySplit[0].concat(searchDaySplit[1]); 
			
			var yearMonthDate = new Date(year , month , 01);
			var searchDayDate = new Date(searchDaySplit[0] , searchDaySplit[1] , 01);
			
			//미래의 급여입력 불가
			if(yearMonthDate > searchDayDate && (date >=1 && date <= 31) && searchTeamName != "카테고리"){
				var form = {
						resultDay:resultDay,
						searchTeamName:searchTeamName
					};
					$.ajax({
						url:"searchInsertSalary.salary",
						type:"post",
						data: form,
						success:function(data){
							var length = data.mList.length;
							
							$tableBody=$("#searchTable tbody");
							$tableBody.html('');
							for(var i=0; i<length; i++){
								
								var $tr = $("<tr onclick='clickTr(this)'>");
								var $mid = $("<td class='MemberId'>").text(data.mList[i].mid);
								var $mName = $("<td>").text(data.mList[i].mName);
								var $positionName = $("<td>").text(data.mList[i].positionName);
								var $teamName = $("<td>").text(data.mList[i].teamName);
								
								$tr.append($mid);
								$tr.append($mName);
								$tr.append($positionName);
								$tr.append($teamName);
								
								$tableBody.append($tr);
							}
						},
						error:function(status){
							console.log(status);
						}
					});
			}else{
				alert("해당 목록을 조회할 수 없습니다.");
			}
		}
		/* 사원정보 ajax (급여 정보) */
		function clickTr(select){
			/* 사원 mid 가져오기  (검색 목록 테이블의 첫 번째 인자값 가져오기) */
			var mid = $(select).find("td:eq(0)").text();
			var searchDay = $("#searchWorking").val();
			var searchDaySplit = searchDay.split('-');	
			var resultDay = searchDaySplit[0].concat(searchDaySplit[1]); 
			console.log(resultDay);
			console.log(mid);
			memberId = mid;
			var result = {
					resultDay:resultDay,
					mid:mid
				}
			/* 사원 지급/공제내역 불러오기 ajax */
			$.ajax({
				url :"selectMemberMonthSalary.salary",
				type:"post",
				data: result,
				success:function(data){
					console.log(data);
					if(data.atList == "" && data.msList == ""){
						//사원 월 근태일수
						$("#allworkingDay").val(0);
						$("#weekend").val(0);
						$("#holiday").val(0);
						$("#vacation").val(0);
						$("#absence").val(0);
						
						//지급내역
						$("#basicSalary").val(0);
						$("#plusSalary").val(0);
						$("#SeemSalary").val(0);
						$("#mealSalary").val(0);
						$("#carSalary").val(0);
						$("#extensionSalary").val(0);
						$("#nightSalary").val(0);
						$("#holidaySalary").val(0);
						$("#memberSalaryTotal").val(0);
						
						//공제내역
						$("#NationalPersion").val(0);
						$("#healthInsurance").val(0);
						$("#EmployeeInsurance").val(0);
						$("#CareInsurance").val(0);
						$("#incomeTax").val(0);
						$("#localIncomeTax").val(0);
						$("#MemberDeduction").val(0);
						
						$("#Membertotal").val(0);
					}else{
						
					var totalDay = data.atList[0].totalNum;		//총근무일수
					var weekDay = data.atList[0].weekdayNum;	//평일근무일수
					var holiday = data.atList[0].holidayNum;	//휴일근무일수
					var vacation = data.atList[0].vacationNum;	//휴가사용일수
					var noatt = data.atList[0].noattNum;		//결근일수
					var basicSalary = numberFormat(data.msList[0].sdetailMoney);
					var plusSalary = numberFormat(data.msList[1].sdetailMoney);
					var SeemSalary = numberFormat(data.msList[2].sdetailMoney);
					var mealSalary = numberFormat(data.msList[3].sdetailMoney);
					var carSalary = numberFormat(data.msList[4].sdetailMoney);
					var extensionSalary = numberFormat(data.msList[5].sdetailMoney);
					var nightSalary = numberFormat(data.msList[6].sdetailMoney);
					var holidaySalary = numberFormat(data.msList[7].sdetailMoney);
					var memberSalaryTotal = 0  
					
					//console.log("sdIdMeal : " + data.msList[3].sdId);
					//console.log("sdIdCar : " +data.msList[4].sdId);
					
					$("#mealId").val(data.msList[3].sdId);
					$("#carId").val(data.msList[4].sdId);
					
					
					for(var i=0; i<8; i++){
						memberSalaryTotal += data.msList[i].sdetailMoney;
					}
					memberSalaryTotal = numberFormat(memberSalaryTotal);
					
					var NationalPersion = numberFormat(data.msList[8].sdetailMoney);
					var healthInsurance = numberFormat(data.msList[9].sdetailMoney);
					var CareInsurance = numberFormat(data.msList[10].sdetailMoney);
					var EmployeeInsurance = numberFormat(data.msList[11].sdetailMoney);
					var incomeTax = numberFormat(data.msList[12].sdetailMoney);
					var localIncomeTax = numberFormat(data.msList[13].sdetailMoney);
					var MemberDeduction = 0
					
					for(var i=8; i<14; i++){
						MemberDeduction += data.msList[i].sdetailMoney;
					}
					MemberDeduction = numberFormat(MemberDeduction);
					
					var MemberTotal = numberFormat(removeComma(memberSalaryTotal) - removeComma(MemberDeduction));
					
					//사원 월 근태일수
					$("#allworkingDay").val(totalDay);
					$("#weekend").val(weekDay);
					$("#holiday").val(holiday);
					$("#vacation").val(vacation);
					$("#absence").val(noatt);
					
					//지급내역
					$("#basicSalary").val(basicSalary);
					$("#plusSalary").val(plusSalary);
					$("#SeemSalary").val(SeemSalary);
					$("#mealSalary").val(mealSalary);
					$("#carSalary").val(carSalary);
					$("#extensionSalary").val(extensionSalary);
					$("#nightSalary").val(nightSalary);
					$("#holidaySalary").val(holidaySalary);
					$("#memberSalaryTotal").val(memberSalaryTotal);
					
					//공제내역
					$("#NationalPersion").val(NationalPersion);
					$("#healthInsurance").val(healthInsurance);
					$("#EmployeeInsurance").val(EmployeeInsurance);
					$("#CareInsurance").val(CareInsurance);
					$("#incomeTax").val(incomeTax);
					$("#localIncomeTax").val(localIncomeTax);
					$("#MemberDeduction").val(MemberDeduction);
					
					$("#Membertotal").val(MemberTotal);
					}
					
				},
				error:function(status){
					console.log(status);
				}
			})
		}
		//수정 버튼 클릭 ( 사원  )
		function SalaryModifiy(){
			$("#MemberModify").hide();
			$("#MemberSave").show();
			$("#MemberCancel").show();
			$("#mealSalary").attr("readonly" , false).css({"background-color": "#f0f0f0"});
			$("#carSalary").attr("readonly" , false).css({"background-color": "#f0f0f0"});	//차량보조 수정 가능 
		}
		/* 비과세 항목 : 식대 */
		$("#mealSalary").change(function(){
			var salaryTotal = 0;
			if($("#mealSalary").val() > 100000){
				alert("식대는 10만원을 초과할 수 없습니다.")
				$("#mealSalary").val(0);
			}else{
				var basicSalary = removeComma($("#basicSalary").val());
				var plusSalary = removeComma($("#plusSalary").val())
				var SeemSalary = removeComma($("#SeemSalary").val())
				var mealSalary =  removeComma($("#mealSalary").val())
				var carSalary =  removeComma($("#carSalary").val())
				var extensionSalary =  removeComma($("#extensionSalary").val())
				var nightSalary =  removeComma($("#nightSalary").val())
				var holidaySalary =  removeComma($("#holidaySalary").val())
				
				salaryTotal = basicSalary + plusSalary + SeemSalary + mealSalary + carSalary + 
				extensionSalary + nightSalary + holidaySalary;
				
				
				$("#memberSalaryTotal").val(numberFormat(salaryTotal));
				var MemberDeduction = $("#MemberDeduction").val();
				var MemberTotal = salaryTotal - removeComma(MemberDeduction);
				
				$("#Membertotal").val(numberFormat(MemberTotal));
			}
		})
		/* 비과세 항목 : 차랑보조 */
		$("#carSalary").change(function(){
			var salaryTotal = 0;
			if($("#carSalary").val() > 200000){
				alert("차량유지비는 20만원을 초과할수 없습니다.")
				$("#carSalary").val(0);
			}else{
				if($("#carSalary").val() == ""){
					alert("숫자를 입력해주세요.")
					$("#carSalary").focus();
				}else{
					var basicSalary = removeComma($("#basicSalary").val());
					var plusSalary = removeComma($("#plusSalary").val())
					var SeemSalary = removeComma($("#SeemSalary").val())
					var mealSalary =  removeComma($("#mealSalary").val())
					var carSalary =  removeComma($("#carSalary").val())
					var extensionSalary =  removeComma($("#extensionSalary").val())
					var nightSalary =  removeComma($("#nightSalary").val())
					var holidaySalary =  removeComma($("#holidaySalary").val())
					
					salaryTotal = basicSalary + plusSalary + SeemSalary + mealSalary + carSalary + 
					extensionSalary + nightSalary + holidaySalary;
					
					
					$("#memberSalaryTotal").val(numberFormat(salaryTotal));
					var MemberDeduction = $("#MemberDeduction").val();
					var MemberTotal = salaryTotal - removeComma(MemberDeduction);
					
					$("#Membertotal").val(numberFormat(MemberTotal));
				}
			}
		})
		//수정 - 저장 버튼 클릭 ( 사원  )
		function save(){
			if($("#carSalary").val() !="" && $("#mealSalary").val() !=""){
				
				var mid = memberId;
				var mealSalary = removeComma($("#mealSalary").val())
				var carSalary = removeComma($("#carSalary").val())
				var membertotal = removeComma($("#Membertotal").val())
				
				var searchDay = $("#searchWorking").val();
				var searchDaySplit = searchDay.split('-');	
				var resultDay = searchDaySplit[0].concat(searchDaySplit[1]); 
				
				var mealId = $("#mealId").val();
				var carId = $("#carId").val();
				
				//console.log(mealId);
				//console.log(carId);
				
				var result = {
						mid:mid,
						mealSalary:mealSalary,
						carSalary:carSalary,
						membertotal:membertotal,
						resultDay:resultDay,
						carId:carId,
						mealId:mealId
					}
				$.ajax({
					url :"updateMemberMonthSalary.salary",
					type:"post",
					data : result,
					success:function(data){
						console.log(data.result);
						
						if(data.result == 1){
							alert("수정이 완료되었습니다.");
							window.location.href = "showinsertWage.salary"
						}
					},
					error:function(status){
						console.log(status);
					}
				})
			}else{
				alert("숫자를 입력해주세요")
			}
			
		}
		
		
		//취소 버튼 클릭 ( 사원  )
		function cancel(){
			$("#MemberModify").show();
			$("#MemberSave").hide();
			$("#MemberCancel").hide();
			$("#mealSalary").attr("readonly" , true).css({"background-color": "white"});	//시점상여 수정 불가
			$("#carSalary").attr("readonly" , true).css({"background-color": "white"});	//차량보조 수정 불가 
		}
		$(function(){
			$(".ui.dropdown").dropdown();
			$(".menu .item").tab();
			$("#pay2").show();
			$('#month_year_calendar').calendar({
			    type: 'month',
			    formatter: {
			    	date: function (date, settings) {
			    	      if (!date) return '';
			    	      var month = date.getMonth() + 1;
		                   var month2;
		                   if(month < 10){
		                      month2 = '0' + month;
		                   }else {
		                      month2 = month;
		                   }
			    	      var year = date.getFullYear();
			    	      return year + '-' + month2;
			    	}
			    },
			    text: {
			        monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
			    }
			 });
		});
		
		function numberFormat(inputNumber) {
			   return inputNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
		
		function removeComma(str)
		{
			n = parseInt(str.replace(/,/g,""));
			return n;
		}
	</script>
</body>
</html>