<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
	.searchArea{
		padding-top: 7px;
		height: 70px;
	}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	
	<div class="outer" id="outer" ><!-- outer area -->
		<div id="loader" class="ui loader"></div>
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
			<div class="hru segment"><!-- hru segment start -->
			<h3><i class="check square icon"></i>급여 이체 현황 및 이메일 발송</h3>
			<br>
				<div class="searchArea"><!-- searchArea start -->
					
						<table align="center" cellspacing="10px">
						<tr>
							<td><h3>* 지급 년월 </h3></td>
								<td>
									<div class="ui calendar" id="month_year_calendar">
										<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="text" readonly="readonly" placeholder="근무년월 선택" id="searchDate">
										</div>
									</div>
								</td>
							<td width="50px"></td>
							<td><h3>* 부서 &nbsp;&nbsp;</h3></td>
							<td>
								<select class="ui dropdown" id="selectTeam" name="searchTeamCode">
									<option value="" selected disabled hidden>카테고리</option>
									<c:forEach items="${tlist}" var="i" varStatus="status" >
										<option value="${i.teamCode}">${i.teamName}</option>
									</c:forEach>
                           	 	</select>			
							</td>
							<td width="50px"></td>
							<td><h3>&nbsp; 성명 &nbsp;&nbsp;</h3></td>
							<td><div class="ui icon input"><input type="text" id="name" size="30" placeholder="Search..."><i class="search icon"></i></div>
							<td width="100px"></td>
							<td>
								<button class="ui blue button" onclick="return searchList()">검색하기</button>
							</td>
						</tr>
					</table>
					
				</div><!-- searchArea end -->
			</div><!-- hru segment end -->

			<div class="hru segment"><!-- hru segment start -->
				<button id="emailButton" onclick="emailClick();"style="float:right; font-weight:bold; margin-bottom:10px;" class="ui basic button">
			 		 <i class="envelope open text icon"></i>E-mail 발송</button>
			<table class="hru board2" id="searchResult">
				<thead>
					<tr>
						<th><div class="ui checkbox"><input type="checkbox" id="include"><label for="include">  </label></div></th>
						<th><c:out value="사원번호"></c:out></th>
						<th><c:out value="성명"></c:out></th>
						<th><c:out value="부서"></c:out></th>
						<th><c:out value="직급"></c:out></th>
						<th><c:out value="지급일자"></c:out></th>
						<th><c:out value="실지급액"></c:out></th>
						<th><c:out value="이메일"></c:out></th>
						<th><c:out value="은행명"></c:out></th>
						<th><c:out value="계좌번호"></c:out></th>
					</tr>
				</thead>
				<tbody>
				
				</tbody>
			</table>
			</div><!-- hru segment end -->
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	
	<script>
		/* 이메일 전송 버튼 */
		function emailClick(){
			var checkbox = $("input[name=emailcheck]:checked");
			var tdArr = new Array();
			for(var i=0; i<checkbox.length; i++){
				var tr = checkbox.parent().parent().parent().eq(i);
				var td = tr.children();
				
				
				var mid = td.eq(1).text();
				var name = td.eq(2).text();
				var teamName = td.eq(3).text();
				var giveDate =td.eq(5).text();
				var email = td.eq(7).text();
				
				var result = {
						mid:mid,
						name:name,
						teamName:teamName,
						giveDate:giveDate,
						email:email
				};
				tdArr.push(result);
			}
			console.log(tdArr);
			
			$("#outer").css("opacity","0.5");
			$("#loader").addClass("active");
			/* 사원 이메일 전송 ajax */
			$.ajax({
				url:"sendEmail.salary",
				type:"post",
				traditional : true,
				data: {
					jsonData:JSON.stringify(tdArr)
				},
				success:function(data){
					if(data.result == 1){
						$("#outer").css("opacity","1.0");
						$("#loader").removeClass("active");
						Swal.fire(
	                             '급여명세서',
	                             '이메일 전송이 완료되었습니다.',
	                             'success'
	                     )
					}
				},
				error:function(status){
				}
			})
			
			
			
		}
		/* 검색조건으로 사원을 검색한다. */
		function searchList(){
			
			var searchDate = $("#searchDate").val();
 			var teamName = $("#selectTeam option:selected").text();
			var name = $("#name").val();
			
			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth()+1;
			var date = date.getDate();
			var yearMonthDate = new Date(year , month , 01);
			var searchDateSplit = searchDate.split('-');
			var searchDayDate = new Date(searchDateSplit[0] , searchDateSplit[1]-1 , 01);
			//YYYY-MM-DD 형식으로 변환 (타입 : STRING) -> 검색 조건
			var resultDate = dateToYYYYMMDD(searchDayDate);
			if(teamName =='카테고리' || searchDate == ""){
				alert("지급 년월과 부서를 선택해주세요")
			}else{
				if(yearMonthDate <= searchDayDate){
					alert("해당 조건으로 검색이 불가능합니다.")
				}else{
					var form = {
							resultDate:resultDate,
							teamName:teamName,
							name:name
					}
					
					/* 검색 가능 */
					$.ajax({
						url:"selectMonthSalaryEmail.salary",
						type:"post",
						data: form,
						success:function(data){
							var length = data.mList.length;
							
							$tableBody=$("#searchResult tbody");
							$tableBody.html('');
							for(var i=0; i<length; i++){
								var $tr =$("<tr>");
								var $check =$("<td>").append($("<div class='ui checkbox'>").append("<input type='checkbox' name='emailcheck'>").append("<label for='include'>"));
								var $mId = $("<td>").text(data.mList[i].mid);
								var $mName = $("<td>").text(data.mList[i].mName);
								var $teamName = $("<td>").text(data.mList[i].teamName);
								var $sDate = $("<td>").text(data.mList[i].sDate);
								var $pName = $("<td>").text(data.mList[i].pName);
								var $Money = $("<td>").text(data.mList[i].salaryMoney);
								var $email = $("<td>").text(data.mList[i].email);
								var $bank = $("<td>").text(data.mList[i].bank);
								var $accountNumber = $("<td>").text(data.mList[i].accountNumber);
								
								$tr.append($check);
								$tr.append($mId);
								$tr.append($mName);
								$tr.append($teamName);
								$tr.append($pName);
								$tr.append($sDate);
								$tr.append($Money);
								$tr.append($email);
								$tr.append($bank);
								$tr.append($accountNumber);
								
								$tableBody.append($tr);
							}
							
						},
						error:function(status){
							console.log(status);
						}
					})
					
				}
			}
		}
	
		
		function dateToYYYYMMDD(date){
		    function pad(num) {
		        num = num + '';
		        return num.length < 2 ? '0' + num : num;
		    }
		    return date.getFullYear() + '-' + pad(date.getMonth()+1) + '-' + pad(date.getDate());
		}
		
		
		$(function(){
			$("#pay2").show();
			$(".ui.dropdown").dropdown();
			$("#include").click(function(){
				if($("#include").prop("checked")){
					$("input[type=checkbox]").prop("checked",true);
				}else{
					$("input[type=checkbox]").prop("checked",false);
				}
			})
			$('#month_year_calendar').calendar({
			    type: 'month',
			    formatter: {
			    	date: function (date, settings) {
			    	      if (!date) return '';
			    	      var month = date.getMonth() + 1;
		                   var month2;
		                   if(month < 10){
		                      month2 = '0' + month;
		                   }else {
		                      month2 = month;
		                   }
			    	      var year = date.getFullYear();
			    	      return year + '-' + month2;
			    	}
			    },
			    text: {
			        monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
			    }
			 });
		});
		

	</script>
</body>
</html>