<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html"/> <!-- 상대경로 잘 지정해야함! -->
<style>
.searchArea{
		padding-top: 7px;
		height: 70px;
	} 
	.hru.scroll.table{
		font-size:0.9em;
	}
	.hru.board2 {
		font-size:0.9em;
	}
	.hru.segment.quitMoney{
		padding:0px;
		margin-bottom:20px;
	}	
	.hru.board2 td {
		padding: 10px;
		text-align: inherit;
	}
	

</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp"/> <!-- 상대경로 잘 지정해야함! -->
	<jsp:include page="../common/approval.jsp"/>
	<div class="outer"><!-- outer area -->
		<div class="contentArea"><!-- 내용부분! -->
		<!-- 여기에 내용부분 작성! -->
			<div class="hru segment"><!-- segment 시작 -->
			<h3><i class="check square icon"></i>퇴직금 산정</h3>
			<br>
				<div class="searchArea"><!-- 검색영역 시작 -->
						<table align="center" cellspacing="10px">
							<tr>
								<td><h3>* 퇴사 신청 년월 </h3></td>
								<td>
									<div class="ui calendar" id="month_year_calendar">
										<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="text" readonly="readonly" placeholder="근무년월 선택" id="searchDate">
										</div>
									</div>
								</td>
								<td width="50px"></td>
								<td><h3>* 부서 &nbsp;&nbsp;</h3></td>
								<td>
									<select class="ui dropdown" id="selectTeam" name="searchTeamCode">
										<option value="" selected disabled hidden>카테고리</option>
										<c:forEach items="${tlist}" var="i" varStatus="status" >
											<option value="${i.teamCode}">${i.teamName}</option>
										</c:forEach>
	                           	 	</select>			
								</td>
								<td width="50px"></td>
								<td><h3>&nbsp; 성명 &nbsp;&nbsp;</h3></td>
								<td><div class="ui icon input"><input id="searchName" type="text" size="30" placeholder="Search..."><i class="search icon"></i></div>
								<td width="100px"></td>
								<td>
									<button class="ui blue button" onclick="searchResult();">검색하기</button>
								</td>
							</tr>
						</table>
				</div><!-- 검색영역 종료 -->
			</div><!-- segment 종료 -->
			<table style="width: 100%">
				<tr>
					<td style="width:25%; vertical-align: top;">
						<div class="hru segment" style="display: inline-block; width: 100%">
							<h3><i class="check square icon"></i>퇴직신청 사원 목록</h3>
							<div class="scrollArea" style="width: 100%; display: inline-block;">
								<table class="hru scroll table" id="MemberTable">
									<thead>
										<tr>
											<th>사원번호</th>
											<th>성명</th>
											<th>직급</th>
											<th>부서</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</td>
					<td style="width:75%; padding : 10px; vertical-align:top;">
						<div class="hru segment"><!-- 사원 상세정보 start -->
							<h3><i class="check square icon"></i>사원 기본 정보</h3>
								<!-- 사원 기본 정보  테이블 -->
								<div class="hru segment quitMoney">
									<table class="hru board2" id="quitMemberInfo">
										<tr>
											<th>성명</th>
											<td colspan="3"><c:out value=""/></td>
											<th>사원번호</th>
											<td colspan="3"><c:out value=""/></td>
										</tr>
										<tr>
											<th>입사일</th>
											<td><div class="ui small icon input">
												  <input type="text" value="" readonly>
												</div></td>
											<th>퇴사일</th>
											<td><div class="ui small icon input">
												  <input type="text" id="quitDate" value="" readonly>
												</div></td>
											<th>퇴직금 지급일</th>
											<td><div class="ui small icon input">
												  <input type="text" value="" readonly>
												</div></td>
											<th>근속기간</th>
											<td><div class="ui small icon input">
												  <input type="text" value="" readonly>
												</div></td>
										</tr>
										
									</table>
									<br>
									<button class="ui mini blue button"onclick="return approvalFunction();"style="float: right;">계산하기</button>
								</div>
								<!-- 퇴직전 3개월 임금총액 계산내역 -->
								
								<div class="hru segment quitMoney">
									<h3><i class="check square icon"></i>퇴직전 3개월 임금총액 계산내역</h3>
									<table class="hru board2" border="1" align="center">
										<thead>
											<tr>
												<th>퇴직금 근속기간</th>
												<th>일수</th>
												<th>기본급</th>
												<th>기타제수당</th>
											</tr>
										</thead>
										<tbody align="center">
											<tr>
												<td><div class="ui small icon input">
												 	 <input id="startOne"type="text" value="" readonly> 
													</div> ~ 
													<div class="ui small icon input">
												 	 <input id="endOne" type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
												 	 <input id="periodOne" type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
													  <input id="basicSalaryOne"type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
													  <input id="plusSalaryOne" type="text" value="" readonly>
													</div>
												</td>
											</tr>
											<tr>
												<td><div class="ui small icon input">
												 	 <input id="startTwo" type="text" value="" readonly>
												 	 ~ 
												 	 <input id="endTwo"type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
												 	 <input id="periodTwo" type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
													  <input id="basicSalaryTwo" type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
													  <input id="plusSalaryTwo" type="text" value="" readonly>
													</div>
												</td>
											</tr>
											<tr>
												<td><div class="ui small icon input">
												 	 <input id="startThree"type="text" value="" readonly>
												 	 ~ 
												 	 <input id="endThree" type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
												 	 <input  id="periodThree" type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
													  <input id="basicSalaryThree" type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
													  <input id="plusSalaryThree" type="text" value="" readonly>
													</div>
												</td>
											</tr>
											<tr>
												<td>계</td>
												<td><div class="ui small icon input">
												 	 <input  id="dataTotal" name ="dateTotal"type="text" value="" readonly>
													</div>
												</td>
												<td><div class="ui small icon input">
													  <input id="basicTotal" name="basicTotal" type="text" value="" readonly>
													</div></td>
												<td><div class="ui small icon input">
													  <input id="plusTotal" type="text" value="" readonly>
													</div></td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- 퇴직전 1년간 상여내역 -->
								<div class="hru segment quitMoney"> 
									<h3><i class="check square icon"></i>퇴직전 1년간 상여내역</h3>
									<table class="hru table">
										<tr>
											<th>연차수당</th>
											<td><div class="ui small icon input">
													  <input id="vacationSalary"type="text" value="" readonly>
												</div></td>
											<th>상여금</th>
											<td><div class="ui small icon input">
													  <input id="plusSalary" name ="plusSalary" type="text" value="" readonly>
												</div></td>
										</tr>
									</table>
								</div>
								<div class="hru segment quitMoney"> 
									<h3><i class="check square icon"></i>퇴직금 계산</h3>
									<table class="hru table">
										<tr>
											<th>일평균 임금</th>
											<td><div class="ui small icon input">
													  <input id="dateAvgSalary"type="text" value="" readonly>
												</div></td>
											<th>일통상 임금</th>
											<td><div class="ui small icon input">
													  <input id="ordinarySalary"type="text" value="" readonly>
												</div></td>
											<th>퇴직금</th>
											<td><div class="ui small icon input">
													  <input id="quitSalary" type="text" value="" readonly>
												</div></td>
										</tr>
										<tr>
											<th>퇴직소득세</th>
											<td><div class="ui small icon input">
													  <input id="quitTax"type="text" value="" readonly>
												</div></td>
											<th>퇴직지방소득세</th>
											<td><div class="ui small icon input">
													  <input id="quitTax2"type="text" value="" readonly>
												</div></td>
											<th>차인지급액</th>
											<td><div class="ui small icon input">
													  <input id="quitResult" type="text" name = "quitResult" value="" readonly>
												</div></td>
										</tr>
									</table>
								</div>
								<div class="hru segment quitMoney"> 
									<h3><i class="check square icon"></i>퇴직 정산</h3>
										<table class="hru table">
											<tr>
												<th>보수월액</th>
												<td><div class="ui small icon input">
														  <input type="text" id="basicMonthSalary" value="" readonly>
													</div></td>
												<th>납부 건강보험료</th>
												<td><div class="ui small icon input">
														  <input type="text" id="paymentHealthTax" value="" readonly>
													</div></td>
												<th>납부 장기요양 보험료</th>
												<td><div class="ui small icon input">
														  <input type="text" id="paymentLongHealthTax" readonly>
													</div></td>
											</tr>
											<tr>
												<th>건강 보험료 총액</th>
												<td><div class="ui small icon input">
														  <input type="text" id="paymentHealthTaxTotal" readonly>
													</div></td>
												<th>장기 요양보험료 총액</th>
												<td><div class="ui small icon input">
														  <input type="text" id="paymentLongHealthTaxTotal" readonly>
													</div></td>
												<th>퇴직 정산금</th>
												<td><div class="ui small icon input">
														  <input type="text" id="realResult" name="realResult" readonly>
													</div></td>
											</tr>
										</table>
								</div>
								<br>
									<button class="ui  primary basic button" onclick="return QuitEnd()" style="float: right;">마감</button>
								
							</div><!-- 퇴직금 조회 끝-->
						</td>
					</tr>
				</table>
			
		</div><!-- 내용 끝 -->
	</div><!-- outer end -->
	<script>
		
		var memberId = 0;
		function QuitEnd(){
			Swal.fire({
				  title: '퇴직 정산을 마감하시겠습니까?',
				  text: "퇴직 정산 마감시 사원이 퇴직 처리 됩니다.",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'yes'
				}).then((result) => {
				  if (result.value) {
					var mid = memberId;
					var quitDate = $("#quitDate").val();
					var quitResult = removeComma($("#quitResult").val())
					var realResult = removeComma($("#realResult").val())
						
					console.log("quit:" + quitDate);
					location.href = "updateQuitEnd.salary?mid="+mid+"&quitResult="+quitResult+"&realResult="+realResult+"&quitDate="+quitDate;
					  
				    Swal.fire(
				      '마감완료!',
				      '퇴직금 마감이 완료되었습니다.',
				      'success'
				    )
				  }
				})
		
			
		}
		function searchResult(){
			var searchDate = $("#searchDate").val();
			var teamName = $("#selectTeam option:selected").text();
			var searchName = $("#searchName").val();
			
			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth()+1;
			var date = date.getDate();
			var yearMonthDate = new Date(year , month , 01);
			var searchDateSplit = searchDate.split('-');
			var searchDayDate = new Date(searchDateSplit[0] , searchDateSplit[1]-1 , 01);
			//YYYY-MM-DD 형식으로 변환 (타입 : STRING) -> 검색 조건
			var resultDate = dateToYYYYMMDD(searchDayDate);
			if(teamName =='카테고리' || searchDate == ""){
				alert("지급 년월과 부서를 선택해주세요")
			}else{
				if(yearMonthDate <= searchDayDate){
					alert("해당 조건으로 검색이 불가능합니다.")
				}else{
					/* ajax 실행 */
						var form = {
								resultDate:resultDate,
								teamName:teamName,
								name:name
						}
					$.ajax({
						url:"selectQuitMemberList.salary",
						type:"post",
						data:form,
						success:function(data){
							var length = data.qList.length;
							console.log(data);
							$tableBody= $("#MemberTable tbody");
							$tableBody.html('');
							
							for(var i=0; i<length; i++){
								var $tr = $("<tr onclick='clickTr(this)'>");
								var $mid = $("<td class='MemberId'>").text(data.qList[i].mid);
								var $mName = $("<td>").text(data.qList[i].mName);
								var $positionName = $("<td>").text(data.qList[i].jobName);
								var $teamName = $("<td>").text(data.qList[i].teamName);
								
								$tr.append($mid);
								$tr.append($mName);
								$tr.append($positionName);
								$tr.append($teamName);
								
								$tableBody.append($tr);
							}
						}
					});
				}
			}
		}
		/* 계산하기 클릭했을 때   퇴직금 계산 */
		function approvalFunction(){
			var employee = $("#employeeDate").val();
			var mid = memberId;
			$.ajax({
				url : "selectMemberQuitMoneySalary.salary",
				type : "post",
				data : {mid:mid},
				success : function(data){
					//console.log(data);	
					var totalDate = 0;
					var salaryMoney = 0;
					var plusMoney = 0;
						
					for(var i=0; i< data.tMoneyList.length; i++){
						 totalDate += data.tMoneyList[i].minus;
						 salaryMoney += data.tMoneyList[i].money;
						 plusMoney +=data.tMoneyList[i].plus;
					}
					
					$("#startOne").val(data.tMoneyList[0].firstDate);
					$("#endOne").val(data.tMoneyList[0].lastDate);
					$("#periodOne").val(data.tMoneyList[0].minus);
					$("#basicSalaryOne").val(numberFormat(data.tMoneyList[0].money));
					$("#plusSalaryOne").val(numberFormat(data.tMoneyList[0].plus));
					
					$("#startTwo").val(data.tMoneyList[1].firstDate);
					$("#endTwo").val(data.tMoneyList[1].lastDate);
					$("#periodTwo").val(data.tMoneyList[1].minus);
					$("#basicSalaryTwo").val(numberFormat(data.tMoneyList[1].money));
					$("#plusSalaryTwo").val(numberFormat(data.tMoneyList[1].plus));
					
					$("#startThree").val(data.tMoneyList[2].firstDate);
					$("#endThree").val(data.tMoneyList[2].lastDate);
					$("#periodThree").val(data.tMoneyList[2].minus);
					$("#basicSalaryThree").val(numberFormat(data.tMoneyList[2].money));
					$("#plusSalaryThree").val(numberFormat(data.tMoneyList[2].plus));
					
					$("#dataTotal").val(numberFormat(totalDate));
					$("#basicTotal").val(numberFormat(salaryMoney));
					$("#plusTotal").val(numberFormat(plusMoney));
					
					
					$("#vacationSalary").val(numberFormat(data.plusSalary.vacation));
					$("#plusSalary").val(numberFormat(data.plusSalary.plus));
					
					
					$("#dateAvgSalary").val(numberFormat(data.qSalary.avgSalary));
					$("#ordinarySalary").val(numberFormat(data.qSalary.ordinarySalary));
					$("#quitSalary").val(numberFormat(data.qSalary.quitSalary));
					
					
					//퇴직급여
					var quitSalary = data.qSalary.quitSalary;
					//근속년수
					var employeeDate = parseInt(employee / 365) + 1;
					//근속연수공제
					var employeeDiscount = 0;
					//근속연수 공제 계산식
					if(employeeDate < 5){
						employeeDiscount = 300000 * employeeDate;
					}else if(employeeDate >5 && employeeDate <= 10){
						employeeDiscount = 1500000 + (500000 * (employeeDate-5));
					}else if(employeeDate >10 && employeeDate <= 20){
						employeeDiscount = 4000000 + (800000 * (employeeDate-10));
					}else{
						employeeDiscount = 12000000 + (1200000 * (employeeDate-20));
					}
					//근속 1년당 산출세액
					var employeeDiscountResult = quitSalary - employeeDiscount;
					//console.log("근속1년당 산출세액 : " + employeeDiscountResult);
					//환산급여 (산출세약 * 12 / 정상근속연수)
					var ChangeSalary = (employeeDiscountResult * 12) / employeeDate;
					//환산급여 공제 요율 
					var ChangeDiscount = 0;
					//console.log("환산급여 :" + ChangeSalary);
					//환산급여 공제 (35% ~ 100%  계산)
					if(ChangeSalary <= 8000000){
						ChangeDiscount = ChangeSalary;
					}else if(ChangeSalary > 8000000 && ChangeSalary <= 70000000){
						ChangeDiscount = 8000000 + ((ChangeSalary - 8000000) * 0.6);
					}else if(ChangeSalary > 70000000 && ChangeSalary <= 100000000){
						ChangeDiscount = 45200000 + ((ChangeSalary - 70000000) * 0.55);
					}else if(ChangeSalary > 100000000 && ChangeSalary <= 300000000){
						ChangeDiscount = 61700000 + ((ChangeSalary - 100000000) * 0.45);
					}else{
						ChangeDiscount = 151700000 + ((ChangeSalary - 300000000) * 0.35);
					}
					//퇴직소득 과세표준
					var QuitSalaryTax = ChangeSalary - ChangeDiscount;
					//console.log("퇴직소득 과세표준  : " +  QuitSalaryTax);
					//소득세율 계산
					var getTax = 0;
					if(QuitSalaryTax <= 12000000){
						getTax = QuitSalaryTax * 0.06;
					}else if(QuitSalaryTax <= 46000000){
						getTax = (QuitSalaryTax * 0.15) - 1080000;
					}else if(QuitSalaryTax <= 88000000){
						getTax = (QuitSalaryTax * 0.24) - 52200000;
					}else if(QuitSalaryTax <= 150000000){
						getTax = (QuitSalaryTax * 0.35) - 14900000;
					}else if(QuitSalaryTax <= 300000000){
						getTax = (QuitSalaryTax * 0.38) - 19400000;
					}else if(QuitSalaryTax <= 500000000){
						getTax = (QuitSalaryTax * 0.40) - 25400000;
					}else if(QuitSalaryTax >= 500000000){
						getTax = (QuitSalaryTax * 0.42) - 35400000;
					}
					
					//console.log("환산 산출세액 : " + getTax);
					//퇴직 소득 산출 세액
					var resultTax = 0;
					//지방소득세 
					var resultTax2 = 0;
					resultTax = Math.floor((getTax * employeeDate)/12/ 10) * 10 ;
					resultTax2 = Math.floor(resultTax * 0.1/10)*10 ;
					
					//console.log("소득세 : " + resultTax);
					//console.log("지방소득세 : " + resultTax2);	
					
					$("#quitTax").val(numberFormat(resultTax));
					$("#quitTax2").val(numberFormat(resultTax2));
					$("#quitResult").val(numberFormat(quitSalary- (resultTax + resultTax2)))
					
					var MonthlyAmount = 0;
					for(var j=0; j<4; j++){
						for(var i=0; i< data.tMoneyList.length; i++){
							MonthlyAmount += data.tMoneyList[i].money;
							MonthlyAmount += data.tMoneyList[i].plus;
						}
						MonthlyAmount += data.plusSalary.plus;
					}
					
					MonthlyAmount = Math.floor( MonthlyAmount / 12);
					//보수월액
					$("#basicMonthSalary").val(numberFormat(MonthlyAmount));
					//납부한 건강보험료
					$("#paymentHealthTax").val(numberFormat(85860 * 12));
					//납부한 장기건강보험료
					$("#paymentLongHealthTax").val(numberFormat(8800 * 12))
					//건강보험료 총액
					$("#paymentHealthTaxTotal").val(numberFormat(86400 * 12));
					//장기요양보험료 총액
					$("#paymentLongHealthTaxTotal").val(numberFormat(8930 * 12));
					
					var minusResult = ((85860 * 12) + (8800 * 12)) - ((86400 * 12) + (8930 * 12));
					
					$("#realResult").val(numberFormat(quitSalary- (resultTax + resultTax2) + minusResult));
				},
				error: function(status){
					console.log(status);
				}
			})
		}
		
		function numberFormat(inputNumber) {
			   return inputNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
		function removeComma(str)
		{
			n = parseInt(str.replace(/,/g,""));
			return n;
		}
		/* 사원을 클릭했을 때 사원의 기본정보 조회 */
		function clickTr(select){
			var mid = $(select).find("td:eq(0)").text();
			var searchDay = $("#searchDate").val();
			memberId = mid;
			
			$.ajax({
				url:"selectMemberInfo.salary",
				type:"post",
				data:{mid: mid},
				success:function(data){
					console.log(data);
					
					$tableBody= $("#quitMemberInfo tbody");
					$tableBody.html('');
					
					var $tr1 = $("<tr>");
					var $name1 = $("<th>").text("성명");
					var $name2 = $("<td>").attr("colspan",3).text(data.qList.mName);
					var $mid1 = $("<th>").text("사원번호");
					var $mid2 = $("<td>").attr("colspan",3).text(data.qList.mid);
					
					var $tr2 = $("<tr>");
					var $indate1 =  $("<th>").text("입사일");
					var $indate2 =  $("<td>").append($("<div class='ui small icon input'>").append($("<input type='text'>").val(data.qList.enterDate)));
					var $outdate1 = $("<th>").text("퇴사일");
					var $outdate2 = $("<td>").append($("<div class='ui small icon input'>").append($("<input id='quitDate' type='text'>").val(data.qList.quitDate)));
					var $pay1 =  $("<th>").text("퇴직금 지급일");
					var $pay2 =  $("<td>").append($("<div class='ui small icon input'>").append($("<input type='text'>").val(data.qList.plusMonth)));
					var $em1 = $("<th>").text("근속기간");
					var $em2 = $("<td>").append($("<div class='ui small icon input '>").append($("<input id='employeeDate' type='text'>").val(data.qList.employeeDate)));
					
					$tr1.append($name1);
					$tr1.append($name2);
					$tr1.append($mid1);
					$tr1.append($mid2);
					
					$tr2.append($indate1);
					$tr2.append($indate2);
					$tr2.append($outdate1);
					$tr2.append($outdate2);
					$tr2.append($pay1);
					$tr2.append($pay2);
					$tr2.append($em1);
					$tr2.append($em2);
					
					$tableBody.append($tr1);
					$tableBody.append($tr2);
					
				},error:function(status){
					
					console.log(status);
					
				}
			})
		}
		
		function dateToYYYYMMDD(date){
		    function pad(num) {
		        num = num + '';
		        return num.length < 2 ? '0' + num : num;
		    }
		    return date.getFullYear() + '-' + pad(date.getMonth()+1) + '-' + pad(date.getDate());
		}
		$(function(){
			$("#pay2").show();
			$(".ui.dropdown").dropdown();
			$('#month_year_calendar').calendar({
			    type: 'month',
			    formatter: {
			    	date: function (date, settings) {
			    	      if (!date) return '';
			    	      var month = date.getMonth() + 1;
		                   var month2;
		                   if(month < 10){
		                      month2 = '0' + month;
		                   }else {
		                      month2 = month;
		                   }
			    	      var year = date.getFullYear();
			    	      return year + '-' + month2;
			    	}
			    },
			    text: {
			        monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
			    }
			 });
			
		})
	</script>
</body>
</html>