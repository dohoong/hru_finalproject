<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HRU</title>
<jsp:include page="../common/importlib.html" />
<style>
.searchArea {
	padding-top: 7px;
}
</style>
</head>
<body>
	<jsp:include page="../common/menubar.jsp" />
	<div class="outer">
		<!--outer Area -->
		<div class="contentArea">
			<!-- 내용부분! -->
			<div class="hru segment">
				<h3><i class="check square icon"></i>퇴직사원 관리</h3>
				<div class="searchArea" style="height: 70px;">
						<table align="center" cellspacing="10px">
							<tr>
								<td><h3>* 퇴직 년월</h3></td>
								<td style="width: 20px;"></td>
								<td>
									<div class="ui calendar" id="month_year_calendar">
										<div class="ui input left icon">
											<i class="calendar icon"></i> 
											<input type="text" placeholder="근무년월 선택" id="sDate" readonly="readonly">
										</div>
									</div>
								</td>
								<td style="width: 50px;"></td>
								<td><h3>*부서</h3></td>
								<td style="width: 20px;"></td>
								<td>
									<select class="ui dropdown" id="selectTeam" name="searchTeamCode">
										<option value="" selected disabled hidden>카테고리</option>
										<c:forEach items="${tlist}" var="i" varStatus="status" >
											<option value="${i.teamCode}">${i.teamName}</option>
										</c:forEach>
                           	 		</select>			
								</td>
								<td style="width: 50px;"></td>
								<td>
									<button class="ui blue button" onclick="return search();">검색하기</button>
								</td>
							</tr>
						</table>
				</div>
				<!-- searchArea end -->
			</div>
			<!-- 서서서브 메뉴 -->
			<div class="ui top attached tabular menu">
				<a class="active item" data-tab="first">지급내역</a> 
				<a class="item" data-tab="second" onclick="secondfunction()">신고여부</a>
			</div>
			<!-- 서서서브 메뉴 끝 -->
			<div class="ui bottom attached active tab segment" data-tab="first">
				<div class="hru segment">
					<table class="hru board2" border="1" id="resultTable">
						<thead>
							<tr>
								<th>사원번호</th>
								<th>성명</th>
								<th>직급</th>
								<th>부서</th>
								<th>입사일</th>
								<th>퇴사일</th>
								<th>근속일수</th>
								<th>지급 퇴직금</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>

			<div class="ui bottom attached tab segment" data-tab="second">
				<div class="hru segment">
					<table class="hru board2" border="1" id="reportMember">
						<thead>
							<tr>
								<th>사원번호</th>
								<th>사원명</th>
								<th>직급</th>
								<th>퇴직일자</th>
								<th>4대보험 상실신고</th>
								<th>등록마감</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script>
		var gDate = 0;
		var gTeam ="";
	
		function secondfunction(){
			var resultDate = gDate;
			var teamName = gTeam;
			
			var form = {
					resultDate:resultDate,
					teamName:teamName
			}
			$.ajax({
				url:"selectRetirementMemberInfo.salary",
				type:"post",
				data: form,
				success:function(data){
					console.log(data);
					
					$tableBody = $("#reportMember tbody");
					$tableBody.html('');
					
					var length = data.mList.length;
					
					for(var i=0; i<length; i++){
						
						var $tr = $("<tr>");
						var $mid= $("<td>").text(data.mList[i].mid);
						var $mName = $("<td>").text(data.mList[i].mName);
						var $pName = $("<td>").text(data.mList[i].ppName);
						var $quitDate = $("<td>").text(data.mList[i].out_Date);
						var $insurance = $("<td>").append($("<button class='ui blue button mini' id='insurance' onclick='changefunction()'>").text(data.mList[i].insurance));
						var $endDate = $("<td>").text(data.mList[i].endDate);
						
						$tr.append($mid);
						$tr.append($mName);
						$tr.append($pName);
						$tr.append($quitDate);
						$tr.append($insurance);
						$tr.append($endDate);
						
						$tableBody.append($tr);
						
					}
				},
				error:function(data){
					console.log(data)
				}
						
			})
		}
		function changefunction(){
			
			var result = $("#insurance").text();
			
			if(result == 'N'){
				$("#insurance").text("Y");
			}else if(result =='Y'){
				$("#insurance").text("N");
			}
			
		}
	
		
		function search(){
			var sDate = $("#sDate").val();
			var teamName = $("#selectTeam option:selected").text();
			
			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth()+1;
			var date = date.getDate();
			var yearMonthDate = new Date(year , month , 01);
			
			var searchDateOne = sDate.split('-');
			var searchDateTwo = new Date(searchDateOne[0] , searchDateOne[1]-1 , 01 );
			
			var resultDate = dateToYYYYMMDD(searchDateTwo);
			gTeam = teamName;
			gDate = resultDate;
			if(teamName =='카테고리' || sDate == ""){
			
				alert("해당 조건으로 검색이 불가능합니다.");
			}else{
				if(yearMonthDate <= resultDate){
					alert("해당 조건으로 검색이 불가능합니다.");
				}else{
					var form = {
							teamName:teamName,
							resultDate:resultDate
					}
					
					/* 검색 가능 */
						$.ajax({
						url:"selectRetirementMember.salary",
						type:"post",
						data: form,
						success:function(data){
							console.log(data);
							
							$tableBody = $("#resultTable tbody");
							$tableBody.html('');
							
							var length = data.mList.length;
							
							for(var i=0; i<length; i++){
								
								var $tr = $("<tr>");
								var $mid= $("<td>").text(data.mList[i].mid);
								var $mName = $("<td>").text(data.mList[i].mName);
								var $pName = $("<td>").text(data.mList[i].pName);
								var $teamName = $("<td>").text(data.mList[i].teamNAME);
								var $enterDate = $("<td>").text(data.mList[i].enterDate);
								var $quitDate = $("<td>").text(data.mList[i].quitDate);
								var $minusDate = $("<td>").text(data.mList[i].minusDate);
								var $quitMoney = $("<td>").text(numberFormat(data.mList[i].quitMoney));
								
								$tr.append($mid);
								$tr.append($mName);
								$tr.append($pName);
								$tr.append($teamName);
								$tr.append($enterDate);
								$tr.append($quitDate);
								$tr.append($minusDate);
								$tr.append($quitMoney);
								
								$tableBody.append($tr);
								
							}
						},
						error:function(data){
							console.log(data)
						}
							
					});
				}
			}
			
		}
	
	
	
		$(function() {
			/* 상단메뉴 */
			$("#pay2").show();
			/* 드롭다운 */
			$(".ui.dropdown").dropdown();
			/* 서서서브메뉴 */
			$(".menu .item").tab();
			$('#month_year_calendar').calendar(
					{
						type : 'month',
						formatter : {
							date : function(date, settings) {
								if (!date)
									return '';
								var month = date.getMonth() + 1;
								var year = date.getFullYear();
								return year + '-' + month;
							}
						},
						text : {
							monthsShort : [ '1월', '2월', '3월', '4월', '5월', '6월',
									'7월', '8월', '9월', '10월', '11월', '12월' ]
						}
					});
			});
	
		function numberFormat(inputNumber) {
			   return inputNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
	
		function dateToYYYYMMDD(date){
		    function pad(num) {
		        num = num + '';
		        return num.length < 2 ? '0' + num : num;
		    }
		    return date.getFullYear() + '-' + pad(date.getMonth()+1) + '-' + pad(date.getDate());
		}
		
	</script>

</body>
</html>